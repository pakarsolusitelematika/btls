//gcc -O3 -fPIC -shared -o pem-ecdsa.so -I. pem-ecdsa/pem-ecdsa.c -lgnutls
#include <gnutls/gnutls.h>
#include <gnutls/x509.h>
#include "src/server_keys/module.h"
#include <pthread.h>
#include <stdio.h>
#include <string.h>

#define MAXKEYS 128

struct key
{
	int used;
	int allocated;
	uint8_t pubkey[160];
	size_t pubkeylen;
	uint16_t scheme;	//0x403 or 0x503.
	gnutls_x509_privkey_t key;
};

static struct key keys[MAXKEYS];
static pthread_mutex_t key_lock = PTHREAD_MUTEX_INITIALIZER;

//Return 1 on success, 0 on failure. Don't use errcode 1, it is reserved for out of key slots.
static int load_key_into_slot(const uint8_t* data, size_t datalen, uint32_t* errcode, struct key* key)
{
	if(gnutls_x509_privkey_init(&key->key) < 0) {
		*errcode = 2;			//Can't allocate key.
		return 0;
	}

	gnutls_datum_t _data = {.data = (uint8_t*)data, .size = datalen};
	if(gnutls_x509_privkey_import(key->key, &_data, GNUTLS_X509_FMT_PEM)) {
		*errcode = 3;			//Key import failed.
		gnutls_x509_privkey_deinit(key->key);
		return 0;
	}
	gnutls_datum_t x, y;
	gnutls_ecc_curve_t crv;
	if(gnutls_x509_privkey_export_ecc_raw(key->key, &crv, &x, &y, NULL) < 0) {
		*errcode = 4;			//Key export failed.
		gnutls_x509_privkey_deinit(key->key);
		return 0;
	}
	size_t xoff = (x.size & 1);	//Nasty hack to handle padding zeros.
	size_t yoff = (y.size & 1);
	size_t raw_pubkey_size = x.size+y.size-xoff-yoff;
	uint8_t raw_pubkey[raw_pubkey_size];
	memcpy(raw_pubkey, x.data + xoff, x.size - xoff);
	memcpy(raw_pubkey + x.size - xoff, y.data + yoff, y.size - yoff);
	gnutls_free(x.data);
	gnutls_free(y.data);

	uint16_t scheme;
	uint8_t prefix[27];
	size_t prefixlen;
	if(crv == GNUTLS_ECC_CURVE_SECP256R1 && raw_pubkey_size == 64) {
		scheme = 0x403;
		memcpy(prefix, "\x30\x59\x30\x13\x06\x07\x2a\x86\x48\xce\x3d\x02\x01\x06\x08\x2a\x86\x48\xce\x3d"
			"\x03\x01\x07\x03\x42\x00\x04", prefixlen = 27);
	} else if(crv == GNUTLS_ECC_CURVE_SECP384R1 && raw_pubkey_size == 96) {
		scheme = 0x503;
		memcpy(prefix, "\x30\x76\x30\x10\x06\x07\x2a\x86\x48\xce\x3d\x02\x01\x06\x05\x2b\x81\x04\x00\x22"
			"\x03\x62\x00\x04", prefixlen = 24);
	} else if(crv == GNUTLS_ECC_CURVE_SECP521R1 && raw_pubkey_size == 132) {
		scheme = 0x603;
		memcpy(prefix, "\x30\x81\x9b\x30\x10\x06\x07\x2a\x86\x48\xce\x3d\x02\x01\x06\x05\x2b\x81\x04\x00"
			"\x23\x03\x81\x86\x00\x04", prefixlen = 26);
	} else {
		*errcode = 5;			//Unknown curve.
		gnutls_x509_privkey_deinit(key->key);
		return 0;
	}
	memcpy(key->pubkey, prefix, prefixlen);
	memcpy(key->pubkey+prefixlen, raw_pubkey, raw_pubkey_size);
	key->pubkeylen = prefixlen + raw_pubkey_size;
	key->scheme = scheme;
	//Success.
	return 1;
}

uint64_t btls_mod_get_key_handle2(const uint8_t* data, size_t datalen, uint32_t* errcode)
{
	//Reserve ID.
	unsigned id = 0;
	pthread_mutex_lock(&key_lock);
	while(id < MAXKEYS && keys[id].allocated) id++;
	if(id < MAXKEYS) keys[id].allocated = 1;
	pthread_mutex_unlock(&key_lock);
	if(id == MAXKEYS) {
		*errcode = 1;			//Out of key slots.
		return 0xFFFFFFFFFFFFFFFFULL;	//Bad handle.
	}
	if(!load_key_into_slot(data, datalen, errcode, keys + id))
		goto out_error;
	//Success.
	keys[id].used = 1;
	return id;
out_error:
	keys[id].allocated = 0;
	return 0xFFFFFFFFFFFFFFFFULL;	//Bad handle.
}

void btls_mod_request_sign(uint64_t handle, struct btls_signature_reply reply, uint16_t algorithm,
        const uint8_t* data, size_t datalen)
{
	if(handle >= MAXKEYS || !keys[handle].used) {
		//Invalid handle.
		reply.finish(reply.context, NULL, 0);
		return;
	}
	struct key* key = keys + handle;
	if(algorithm != key->scheme) {
		//Invalid algorithm.
		char buf[512];
		sprintf(buf, "Requested unsupported signature algorithm %04x", algorithm);
		reply.log(reply.context, buf);
		reply.finish(reply.context, NULL, 0);
		return;
	}
	//6 is SHA-256, 7 is SHA-384.
	unsigned char sigbuffer[1024];
	size_t sigbufferlen = sizeof(sigbuffer);
	gnutls_datum_t _data = {.data = (uint8_t*)data, .size = datalen};
	int r = gnutls_x509_privkey_sign_data(key->key, key->scheme / 256 + 2, 0, &_data, sigbuffer, &sigbufferlen);
	if(r < 0) {
		char buf[512];
		sprintf(buf, "Error in signing data: %s", gnutls_strerror(r));
		reply.log(reply.context, buf);
		reply.finish(reply.context, NULL, 0);
		return;
	}
	reply.finish(reply.context, sigbuffer, sigbufferlen);
}

void btls_mod_request_pubkey(uint64_t handle, struct btls_pubkey_reply reply)
{
	if(handle >= MAXKEYS || !keys[handle].used) {
		//Invalid handle.
		return;
	}
	struct key* key = keys + handle;
	reply.finish(reply.context, key->pubkey, key->pubkeylen, &key->scheme, 1);
}
