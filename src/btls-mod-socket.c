//Compile using:
//
// gcc -O3 -std=c99 -shared -pie -fPIC -o btls-mod-socket.so btls-mod-socket.c -pthread -Wl,-E -Iihashlib ihashlib/hashes/sha256.c curve25519_donna/curve25519-donna-c64.o
//
//The key file can be dumped by:
//
// /path/to/btls-mod-socket.so <socketfile>
//
//Where <socketfile> is the socket to listening daemon.
//
//This writes <socketfile>.pub and <socketfile>.key. The latter is key wrapper to pass to btls functions as key
//to load.
//
#define _XOPEN_SOURCE 500
#define _POSIX_C_SOURCE 201112L
#include "module.h"
#include <string.h>
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <limits.h>
#include <netdb.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
//From ihashlib.
#include "hashes/sha256.h"

const uint8_t x25519_base[32] = {9};

typedef uint8_t u8;
int curve25519_donna(u8 *mypublic, const u8 *secret, const u8 *basepoint);

void x25519(uint8_t shared[32], const uint8_t secret[32], const uint8_t public[32])
{
	curve25519_donna(shared, secret, public);
}

void sha256_oneshot(uint8_t output[32], const uint8_t* dbuf, size_t dsize)
{
	uint8_t state[sha_256.state_size];
	sha_256.init(state);
	sha_256.update(state, dbuf, dsize);
	sha_256.final(state, output);
}

void hmac_sha256_oneshot(uint8_t output[32], const uint8_t* key, size_t keylen, const uint8_t* dbuf, size_t dsize)
{
	uint8_t _key[64] = {0};
	if(keylen > 64) {
		sha256_oneshot(_key, key, keylen);
	} else {
		memcpy(_key, key, keylen);
	}
	uint8_t state[sha_256.state_size];
	sha_256.init(state);
	for(size_t i = 0; i < 64; i++) _key[i] ^= 0x36;
	sha_256.update(state, _key, 64);
	sha_256.update(state, dbuf, dsize);
	sha_256.final(state, output);
	sha_256.init(state);
	for(size_t i = 0; i < 64; i++) _key[i] ^= (0x36 ^ 0x5C);
	sha_256.update(state, _key, 64);
	sha_256.update(state, output, 32);
	sha_256.final(state, output);
}

#define MAX_KEY_STRUCTS 1024

struct key_struct
{
	const char* name;
	uint8_t* pubkey;
	size_t pubkeylen;
	uint16_t* schemes;
	size_t schemecount;
	int needs_auth;
	uint8_t authorization[32];	//X25519 private key.
	uint8_t auth_id[32];		//X25519 public key.
};

struct key_struct* valid_key_structs[MAX_KEY_STRUCTS];
size_t valid_key_struct_count;

struct sign_request
{
	struct key_struct* handle;
	struct btls_signature_reply reply;
	uint16_t algorithm;
	unsigned char data[1024];
	size_t datalen;
};

int is_tcp_socket(const char* socketname)
{
	//No '/' and exactly one ':', or part before last ':' is in square brackets.
	size_t begin_bracket = strlen(socketname);
	size_t end_bracket = strlen(socketname);
	size_t colon = strlen(socketname);
	size_t ccount = 0;
	for(const char* i = socketname; *i; i++) {
		if(*i == '/') return 0;
		if(*i == '[') begin_bracket = i - socketname;
		if(*i == ']') end_bracket = i - socketname;
		if(*i == ':') { colon = i - socketname; ccount++; }
	}
	if(ccount == 0) return 0;
	if(ccount > 1 && (colon != end_bracket + 1 || begin_bracket != 0))
		return 0;
	return 1;
}

int connect_to_tcp_socket(const char* sockname, const char* what, char* errbuf)
{
	char name[512];
	char port[512];
	if(strlen(sockname) > sizeof(name) - 1) {
		sprintf(errbuf, "Error %s: Ridiculous TCP target\n", what);
		return -1;
	}
	const char* split = sockname;
	for(const char* i = sockname; *i; i++) {
		if(*i == ':') split = i;
	}
	memcpy(name, sockname, split - sockname);
	name[split - sockname] = 0;
	strcpy(port, split + 1);

	struct addrinfo hints;
	struct addrinfo* result = NULL;
	memset(&hints, 0, sizeof(hints));
	hints.ai_socktype = SOCK_STREAM;
	int r = getaddrinfo(name, port, &hints, &result);
	if(r) {
		sprintf(errbuf, "Error %s: Can't look up address: %s\n", what, gai_strerror(errno));
		return -1;
	}
	if(!result) {
		sprintf(errbuf, "Error %s: Can't look up address: getaddrinfo succeeded but no addresses!\n", what);
		return -1;
	}
	int fd = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if(fd < 0) {
		sprintf(errbuf, "Error %s: Can't create socket: %s\n", what, strerror(errno));
		freeaddrinfo(result);
		return -1;
	}
	if(connect(fd, result->ai_addr, result->ai_addrlen) < 0) {
		sprintf(errbuf, "Error %s: Can't connect to socket (%s): %s\n", what, sockname, strerror(errno));
		close(fd);
		freeaddrinfo(result);
		return -1;
	}
	freeaddrinfo(result);
	return fd;
}

int connect_to_socket(const char* socketname, const char* what, char* errbuf)
{
	if(is_tcp_socket(socketname))
		return connect_to_tcp_socket(socketname, what, errbuf);
	int fd = socket(AF_UNIX, SOCK_STREAM, 0);
	if(fd < 0) {
		sprintf(errbuf, "Error %s: Can't create socket: %s\n", what, strerror(errno));
		return -1;
	}
	struct sockaddr_un sun;
	sun.sun_family = AF_UNIX;
	strcpy(sun.sun_path, socketname);
	if(connect(fd, (struct sockaddr*)&sun, sizeof(sun)) < 0) {
		sprintf(errbuf, "Error %s: Can't connect to socket (unix %s): %s\n", what, socketname,
			strerror(errno));
		close(fd);
		return -1;
	}
	return fd;
}

void* signature_thread(void* _v)
{
	char errbuf[2048];
	errbuf[0] = 0;
	struct sign_request* v = (struct sign_request*)_v;
	int fd = connect_to_socket(v->handle->name, "signing", errbuf);
	if(fd < 0) goto out_error;	//Closes socket on error.

	unsigned char reqbuf[1024+5+32];
	unsigned char sesskey[32];
	if(v->datalen > sizeof(reqbuf)-5-32) {
		sprintf(errbuf, "Error signing: Data to sign too big");
		goto out_fd;
	}
	if(v->handle->needs_auth) {
		reqbuf[0] = 2;		//Authenticate.
		memcpy(reqbuf + 1, v->handle->auth_id, 32);
		size_t i = 0;
		size_t tlen = 33;
		while(i < tlen) {
			ssize_t r = write(fd, reqbuf + i, tlen - i);
			if(r < 0) {
				sprintf(errbuf, "Error signing: Can't write auth request: %s", strerror(errno));
				goto out_fd;
			}
			i += r;
		}
		i = 0;
		while(i < 32) {
			ssize_t r = read(fd, reqbuf + i, 32 - i);
			if(r == 0) {
				sprintf(errbuf, "Error signing: Remote end hung up without sending auth response");
				goto out_fd;
			}
			if(r < 0) {
				sprintf(errbuf, "Error signing: read error: %s", strerror(errno));
				goto out_fd;
			}
			i += r;
		}
		x25519(sesskey, v->handle->authorization, reqbuf);
	}
	size_t extra = 0;
	reqbuf[0] = 1;
	reqbuf[1] = v->algorithm >> 8;
	reqbuf[2] = v->algorithm;
	reqbuf[3] = v->datalen >> 8;
	reqbuf[4] = v->datalen;
	memcpy(reqbuf+5, v->data, v->datalen);
	if(v->handle->needs_auth) {
		unsigned char* macbuf = reqbuf + 5 + v->datalen;
		size_t macdlen = macbuf - reqbuf;
		hmac_sha256_oneshot(macbuf, sesskey, sizeof(sesskey), reqbuf, macdlen);
		extra += 32;
	}
	size_t i = 0;
	size_t tlen = v->datalen+5+extra;
	while(i < tlen) {
		ssize_t r = write(fd, reqbuf + i, tlen - i);
		if(r < 0) {
			sprintf(errbuf, "Error signing: Can't write signature request: %s", strerror(errno));
			goto out_fd;
		}
		i += r;
	}
	i = 0;
	while(i < 2) {
		ssize_t r = read(fd, reqbuf + i, 2 - i);
		if(r == 0) {
			sprintf(errbuf, "Error signing: Remote end hung up without sending signature");
			goto out_fd;
		}
		if(r < 0) {
			sprintf(errbuf, "Error signing: read error: %s", strerror(errno));
			goto out_fd;
		}
		i += r;
	}
	size_t resplen = reqbuf[0] * 256 + reqbuf[1];
	if(resplen > sizeof(reqbuf)) {
		sprintf(errbuf, "Error signing: signature too big");
		goto out_fd;
	}
	i = 0;
	while(i < resplen) {
		ssize_t r = read(fd, reqbuf + i, resplen - i);
		if(r == 0) {
			sprintf(errbuf, "Error signing: Remote end hung up without sending signature");
			goto out_fd;
		}
		if(r < 0) {
			sprintf(errbuf, "Error signing: read error: %s", strerror(errno));
			goto out_fd;
		}
		i += r;
	}
	v->reply.finish(v->reply.context, reqbuf, resplen);
	close(fd);
	free(v);	//Free the context allocated by another thread.
	return NULL;
out_fd:
	close(fd);
out_error:
	if(errbuf[0]) v->reply.log(v->reply.context, errbuf);
	v->reply.finish(v->reply.context, NULL, 0);
	free(v);	//Free the context allocated by another thread.
	return NULL;
}

uint64_t btls_mod_get_key_handle(const char* name, uint32_t* errorcode)
{
	int needs_auth = 0;
	uint8_t authorization[32];
	uint8_t auth_id[32];
	for(size_t i = 0; i < valid_key_struct_count; i++) {
		if(!strcmp(valid_key_structs[i]->name, name))
			return (uint64_t)(size_t)valid_key_structs[i];
	}
	if(valid_key_struct_count == MAX_KEY_STRUCTS) { *errorcode = 7; return 0; }	//Can't load anymore.
	struct key_struct* newkey = NULL;
	char pubname[strlen(name)+5];
	char socktmp[512];
	const char* sockname = name;
	strcpy(pubname, name);
	strcat(pubname, ".pub");
	int fd = open(pubname, O_RDONLY);
	if(fd < 0) {*errorcode = 1; goto out; }
	unsigned char keybuf[4096];
	ssize_t keysize = read(fd, keybuf, sizeof(keybuf));
	close(fd);
	if(keysize < 0) {*errorcode = 2; goto out; }

	struct stat s;
	if(stat(name, &s) < 0) {*errorcode = 3; goto out; }
	if(S_ISREG(s.st_mode)) {
		//Read the TCP socket from within.
		FILE* fp = fopen(name, "r");
		socktmp[0] = 0;
		fgets(socktmp, sizeof(socktmp)-1, fp);
		size_t l = strlen(socktmp);
		while(l > 0 && (socktmp[l-1] == '\r' || socktmp[l-1] == '\n'))
			l--;
		socktmp[l] = 0;
		sockname = socktmp;
		if(!is_tcp_socket(socktmp)) {*errorcode = 10; goto out; }
		fclose(fp);
	} else if(!S_ISSOCK(s.st_mode)) {*errorcode = 9; goto out; }

	if(keysize < 2) {*errorcode = 5; goto out; }
	unsigned schemecount = keybuf[0] * 256 + keybuf[1];
	unsigned pubkeyoff = 2 + 2 * schemecount;
	if(keysize < pubkeyoff + 2) {*errorcode = 5; goto out; }
	unsigned pubkeylen = keybuf[pubkeyoff+0] * 256 + keybuf[pubkeyoff+1];
	if(keysize != pubkeyoff + 2 + pubkeylen) {*errorcode = 5; goto out; }


	strcpy(pubname, name);
	strcat(pubname, ".auth");
	fd = open(pubname, O_RDONLY);
	if(fd < 0 && errno != ENOENT) {*errorcode = 7; goto out; }
	if(fd >= 0) {
		needs_auth = 1;
		unsigned char keybuf[4096];
		ssize_t keysize = read(fd, keybuf, sizeof(keybuf));
		close(fd);
		if(keysize < 0) {*errorcode = 8; goto out; }
		sha256_oneshot(authorization, keybuf, keysize);
		authorization[0] &= 0xF8;
		authorization[31] &= 0x7F;
		authorization[31] |= 0x40;
		//Expand authorization using X25519 with standard base to yield auth_id.
		x25519(auth_id, authorization, x25519_base);
		close(fd);
	}

	//OK, the key looks valid.
	char* nname = malloc(strlen(sockname) + 1);
	uint8_t* pubkey = malloc(pubkeylen);
	uint16_t* schemes = malloc(2 * schemecount);
	newkey = malloc(sizeof(struct key_struct));
	if(!nname || !pubkey || !schemes || !newkey) {
		free(nname);
		free(pubkey);
		free(schemes);
		free(newkey);
		newkey = NULL;
		*errorcode = 6;
		goto out;
	}
	strcpy(nname, sockname);
	memcpy(pubkey, keybuf + pubkeyoff + 2, pubkeylen);
	for(unsigned i = 0; i < schemecount; i++) {
		schemes[i] = keybuf[2 * i + 2] * 256 + keybuf[2 * i + 3];
	}
	//Fill the structure.
	newkey->name = nname;
	newkey->pubkey = pubkey;
	newkey->pubkeylen = pubkeylen;
	newkey->schemes = schemes;
	newkey->schemecount = schemecount;
	newkey->needs_auth = needs_auth;
	memcpy(newkey->auth_id, auth_id, sizeof(auth_id));
	memcpy(newkey->authorization, authorization, sizeof(authorization));
	//Ok.
	goto out;
out:
	if(newkey) valid_key_structs[valid_key_struct_count++] = newkey;
	return (uint64_t)(size_t)newkey;
}

void btls_mod_request_sign(uint64_t handle, struct btls_signature_reply reply, uint16_t algorithm,
	const uint8_t* data, size_t datalen)
{
	struct key_struct* key = NULL;
	for(size_t i = 0; i < valid_key_struct_count; i++) {
		uint64_t chandle = (uint64_t)(size_t)valid_key_structs[i];
		if(chandle == handle) key = valid_key_structs[i];
	}
	if(!key) goto out_failed;
	//Make a request.
	struct sign_request* req = calloc(sizeof(struct sign_request), 1);
	if(!req) goto out_failed;
	if(datalen > sizeof(req->data)) goto out_failed_req;
	req->algorithm = algorithm;
	req->handle = key;
	req->reply = reply;
	memcpy(req->data, data, datalen);
	req->datalen = datalen;
	//Okay, start a thread for it.
	pthread_t thread_id;
	if(pthread_create(&thread_id, NULL, signature_thread, req)) goto out_failed_req;
	pthread_detach(thread_id);	//Just try to detach it.
	return;		//The signature request is running in another thread
out_failed_req:
	free(req);
out_failed:
	//We have to reply, even in case of failure.
	reply.finish(reply.context, NULL, 0);
	return;
}

void btls_mod_request_pubkey(uint64_t handle, struct btls_pubkey_reply reply)
{
	struct key_struct* key = NULL;
	for(size_t i = 0; i < valid_key_struct_count; i++) {
		uint64_t chandle = (uint64_t)(size_t)valid_key_structs[i];
		if(chandle == handle) key = valid_key_structs[i];
	}
	if(key) {
		reply.finish(reply.context, key->pubkey, key->pubkeylen, key->schemes, key->schemecount);
	}
}

static char* canonicalize_path(const char* input)
{
	if(!input) {
		fprintf(stderr, "Can't canonicalize NULL!\n");
		return NULL;
	}
	char* outpath = realpath(input, NULL);
	if(!outpath) {
		fprintf(stderr, "Can't canonicalize '%s': %s\n", input, strerror(errno));
		return NULL;
	}
	return outpath;
}

static int dump_pubkey(const char* sockname, uint8_t** pubkey, size_t* pubkeylen)
{
	char errbuf[2048];
	int fd = connect_to_socket(sockname, "dumping", errbuf);
	if(fd < 0) {
		fputs(errbuf, stderr);
		return -1;
	}
	errno = 0;
	char tmp = 0;
	if(write(fd, &tmp, 1) < 1) {
		fprintf(stderr, "Error dumping: Can't write to socket '%s': %s", sockname, strerror(errno));
		return -1;
	}
	//Now, read the response.
	uint8_t tmp_pubkey[65537];
	size_t i = 0;
	while(i < sizeof(tmp_pubkey)) {
		ssize_t r = read(fd, tmp_pubkey + i, sizeof(tmp_pubkey) - i);
		if(r < 0) {
			fprintf(stderr, "Error dumping: Can't read from socket '%s': %s", sockname, strerror(errno));
			return -1;
		}
		if(r == 0)
			break;
		i += r;
	}
	close(fd);
	if(i == sizeof(tmp_pubkey)) {
		fprintf(stderr, "Error dumping: Public key too large from '%s'", sockname);
		return -1;
	}
	if(i < 2) goto invalid_pubkey;
	size_t schemes = tmp_pubkey[0] * 256 + tmp_pubkey[1];
	size_t puboff = 2 * schemes + 2;
	if(i < puboff + 2) goto invalid_pubkey;
	size_t keylen = tmp_pubkey[puboff+0] * 256 + tmp_pubkey[puboff+1];
	if(i != puboff + 2 + keylen) goto invalid_pubkey;
	uint8_t* key = tmp_pubkey + puboff + 2;
	if(keylen < 2) goto invalid_pubkey;				//0x30 <length>
	if(key[0] != 0x30) goto invalid_pubkey;				//SEQUENCE.
	if(key[1] < 0x80) {
		if(keylen != 2 + key[1]) goto invalid_pubkey;		//Length <128 matches.
	} else if(key[1] == 0x80) goto invalid_pubkey;			//Invalid in DER.
	else if(key[1] == 0x81) {
		if(keylen < 3) goto invalid_pubkey;
		if(key[2] < 128) goto invalid_pubkey;			//Overlong.
		if(keylen != 3 + key[2]) goto invalid_pubkey;		//Length >=128, <256 matches.
	} else if(key[1] == 0x82) {
		if(keylen < 4) goto invalid_pubkey;
		if(key[2] == 0) goto invalid_pubkey;				//Overlong.
		if(keylen != 4 + key[2] * 256 + key[3]) goto invalid_pubkey;	//Length >=256, <65535 matches.
	} else goto invalid_pubkey;						//Too big.

	*pubkey = malloc(i);
	if(!*pubkey) {
		fprintf(stderr, "Error dumping '%s': Out of memory!", sockname);
		return -1;
	}
	memcpy(*pubkey, tmp_pubkey, i);
	*pubkeylen = i;
	//Ok.
	return 0;
invalid_pubkey:
	fprintf(stderr, "Error dumping: Invalid public key from '%s'", sockname);
	return -1;
}

//This is main()!
int main(int argc, char** argv)
{
	if(argc < 2) {
		fprintf(stderr, "Syntax: %s <keysocket>\n", argv[0]);
		return 1;
	}
	char* argv0 = canonicalize_path(argv[0]);
	char* name = canonicalize_path(argv[1]);
	char socktmp[512];
	const char* sockname = name;
	if(!argv0 || !name) return 1;

	struct stat s;
	if(stat(argv0, &s) < 0) {
		if(errno == ENOENT)
			fprintf(stderr, "'%s' does not exist.\n", argv0);
		else
			fprintf(stderr, "Error stating '%s': %s\n", argv0, strerror(errno));
		return 1;
	}
	if(stat(name, &s) < 0) {
		if(errno == ENOENT)
			fprintf(stderr, "'%s' does not exist.\n", name);
		else
			fprintf(stderr, "Error stating '%s': %s\n", name, strerror(errno));
		return 1;
	}
	if(S_ISREG(s.st_mode)) {
		//Read the TCP socket from within.
		FILE* fp = fopen(name, "r");
		socktmp[0] = 0;
		fgets(socktmp, sizeof(socktmp)-1, fp);
		size_t l = strlen(socktmp);
		while(l > 0 && (socktmp[l-1] == '\r' || socktmp[l-1] == '\n'))
			l--;
		socktmp[l] = 0;
		sockname = socktmp;
		if(!is_tcp_socket(socktmp)) {
			fprintf(stderr, "'%s' does not point to TCP socket\n", name, strerror(errno));
			return 1;
		}
		fclose(fp);
	} else if(!S_ISSOCK(s.st_mode)) {
		fprintf(stderr, "'%s' is not a socket\n", name, strerror(errno));
		return 1;
	}
	uint8_t* dumped_pubkey;
	size_t pubkey_len;
	if(dump_pubkey(sockname, &dumped_pubkey, &pubkey_len) < 0) return 1;

	//Dump the public key we got.
	char tmp[strlen(name)+5];
	strcpy(tmp, name);
	strcat(tmp, ".pub");
	FILE* pubk = fopen(tmp, "wb");
	size_t i = 0;
	while(i < pubkey_len) {
		size_t r = fwrite(dumped_pubkey + i, 1, pubkey_len - i, pubk);
		if(r == 0) {
			if(ferror(pubk)) {
				fprintf(stderr, "Error writing '%s'\n", tmp);
				return 1;
			}
		}
		i += r;
	}
	fflush(pubk);
	fclose(pubk);
	fprintf(stderr, "Wrote %s\n", tmp);

	//Dump the keyfile.
	strcpy(tmp, name);
	strcat(tmp, ".key");
	pubk = fopen(tmp, "w");
	fprintf(pubk, "%s %s\n", argv0, name);
	fflush(pubk);
	if(ferror(pubk)) {
		fprintf(stderr, "Error writing '%s'\n", tmp);
		return 1;
	}
	fclose(pubk);
	fprintf(stderr, "Wrote %s\n", tmp);

	uint8_t authorization[32];
	uint8_t auth_id[32];
	strcpy(tmp, name);
	strcat(tmp, ".auth");
	int fd = open(tmp, O_RDONLY);
	if(fd < 0 && errno != ENOENT) {
		fprintf(stderr, "Error opening '%s'\n", tmp);
		return 1;
	}
	if(fd >= 0) {
		unsigned char keybuf[4096];
		ssize_t keysize = read(fd, keybuf, sizeof(keybuf));
		close(fd);
		if(keysize < 0) {
			fprintf(stderr, "Error reading '%s'\n", tmp);
			return 1;
		}
		sha256_oneshot(authorization, keybuf, keysize);
		authorization[0] &= 0xF8;
		authorization[31] &= 0x7F;
		authorization[31] |= 0x40;
		//Expand authorization using X25519 with standard base to yield auth_id.
		x25519(auth_id, authorization, x25519_base);
		for(size_t i = 0; i < 32; i++) {
			fprintf(stdout, "%02x", auth_id[i]);
		}
		fprintf(stdout, "\n");
		close(fd);
	}
	return 0;
}
