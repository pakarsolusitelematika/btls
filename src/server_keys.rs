//!Types related to server keys.
//!
//!This module contains the various types related to server keys. This includes:
//!
//! * The base trait for server keys: `KeyPair`.
//! * Key pairs loaded from files, or file-like things: `LocalKeyPair`.
//!
//!It also contains auxiliary type `SubjectPublicKeyInfo`.

pub use btls_aux_keypair::{CsrParams, KeyPair, SubjectPublicKeyInfo, SupportedSchemes};
pub use btls_aux_keypair_local::{KeyLoadingError, KeyReadError, LocalKeyPair, ModuleError};
