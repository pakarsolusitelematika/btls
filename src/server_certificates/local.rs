//#![allow(unused_variables)]
//#![allow(dead_code)]
use super::{Certificate, CertificateChain, CertificateLookupCriteria, KeyStoreOperations, OcspResponse,
	StapledSctList, StapledSct};
use ::certificate::{CertificateError, CertificateIssuer, CertificateValidationError, extract_spki, OcspDumpedScts,
	OcspSanityCheckResults, OcspValidationError, ParsedCertificate, sanity_check_certificate_chain_load,
	validate_scts};
use ::client_certificates::TrustedLog;
use ::logging::Logging;
use ::server_certificates::{CertificateLookup, CertificateSigner};
use ::stdlib::{Arc, AsRef, AtomicOrdering, Box, BTreeMap, Cow, Deref, DerefMut, Display, fence, FmtError, Formatter,
	Hash, Hasher, IoRead, max, min, Range, RwLock, String, swap, ToOwned, Vec};
use ::system::{File, Path};
use ::utils::subslice_to_range;

use btls_aux_assert::AssertFailed;
use btls_aux_futures::{create_future,FutureCallback,FutureReceiver,FutureSender};
use btls_aux_hash::{checksum, ChecksumOutput};
use btls_aux_keyconvert::{decode_pem, DecodingError, looks_like_pem};
use btls_aux_keypair::{CsrParams, KeyPair, SubjectPublicKeyInfo};
use btls_aux_serialization::{ASN1_SEQUENCE, Asn1Error, Source};
use btls_aux_signature_algo::{SIGALGO_ECDSA_MASK as ECDSA_SIG_MASK, SIGALGO_TLS13_MASK as TLS13_SIG_MASK,
	TLS_SIG_ALGOS, WrappedRecognizedSignatureAlgorithm as WSSigAlgo};
use btls_aux_time::{TimeInterface, TimeNow, Timestamp};

///Error in loading a certificate.
#[derive(Clone,Debug,PartialEq,Eq)]
pub enum CertificateLoadError
{
	//Assertion failed.
	AssertionFailed(AssertFailed),

	///Certificate chain empty
	CertificateChainEmpty,
	///Certificate is missing valid header.
	ValidHeaderMissing(usize),
	///Certificate has junk after end.
	JunkAfterEndOfCert(usize),
	///Error parsing certificate.
	CertParseError(usize, CertificateError),
	///No corresponding private key available.
	NoPrivateKeyAvailable,
	///The private key does not support any known algorithm.
	NoKnownAlgorithms,
	///Error sanity-checking certificate chain.
	SanityCheckFailed(CertificateValidationError),
	///Error sanity-checking OCSP.
	OcspSanityCheckFailed(OcspValidationError),
	///Looks like PEM but isn't?
	LooksLikePem(DecodingError),
	///Error reading certificate.
	ReadError(String),
	///Error splitting certificate.
	CertSplitError(usize, Asn1Error),
	///Certificate handle overflow.
	HandleOverflowed,
	///Error opening certificate.
	OpenError(String),
	///Effective certificate lifetime is zero.
	NoEffectiveCertificateLifetime,

	///Certificate is not DER.
	NotDer(Asn1Error),
	///Error parsing EE public key.
	EePublicKey(Asn1Error),
	#[doc(hidden)]
	#[allow(dead_code)]
	Hidden__
}

impl From<AssertFailed> for CertificateLoadError
{
	fn from(x: AssertFailed) -> CertificateLoadError
	{
		CertificateLoadError::AssertionFailed(x)
	}
}

impl Display for CertificateLoadError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::CertificateLoadError::*;
		match self {
			&AssertionFailed(ref x) => fmt.write_fmt(format_args!("Assertion failed: {}", x)),
			&CertificateChainEmpty => fmt.write_str("Certificate chain is empty!"),
			&ValidHeaderMissing(i) if i == 0 => fmt.write_str("EE Certificate missing valid header"),
			&ValidHeaderMissing(i) => fmt.write_fmt(format_args!("CA Certificate #{} missing valid \
				header", i)),
			&JunkAfterEndOfCert(i) if i == 0 => fmt.write_str("EE Certificate has junk after end"),
			&JunkAfterEndOfCert(i) => fmt.write_fmt(format_args!("CA Certificate #{} has junk after end",
				i)),
			&CertParseError(i, ref x) if i == 0 => fmt.write_fmt(format_args!("EE Certificate does not \
				parse: {}", x)),
			&CertParseError(i, ref x) => fmt.write_fmt(format_args!("CA Certificate #{} does not \
				parse: {}", i, x)),
			&NoPrivateKeyAvailable => fmt.write_str("No corresponding private key available"),
			&NoKnownAlgorithms => fmt.write_str("Private key does not support any known algorithms"),
			&NoEffectiveCertificateLifetime => fmt.write_str("Effective certificate lifetime is zero"),
			&SanityCheckFailed(ref x) => fmt.write_fmt(format_args!("Certificate chain sanity check \
				failed: {}", x)),
			&OcspSanityCheckFailed(ref x) => fmt.write_fmt(format_args!("OCSP response sanity check \
				failed: {}", x)),
			&LooksLikePem(ref x) => fmt.write_fmt(format_args!("Looks like PEM but isn't: {}", x)),
			&HandleOverflowed => fmt.write_str("Certificate handle overflowed"),
			&ReadError(ref x) => fmt.write_fmt(format_args!("Read error: {}", x)),
			&CertSplitError(i, ref x) if i == 0 => fmt.write_fmt(format_args!("EE certificate can't be \
				split: {}", x)),
			&CertSplitError(i, ref x) => fmt.write_fmt(format_args!("CA certificate #{} can't be \
				split: {}", i, x)),
			&OpenError(ref x) => fmt.write_fmt(format_args!("Open error: {}", x)),
			&NotDer(ref x) => fmt.write_fmt(format_args!("Certificate is not DER: {}", x)),
			&EePublicKey(ref x) => fmt.write_fmt(format_args!("Error parsing EE public key: {}", x)),
			&Hidden__ => fmt.write_str("Hidden__")
		}
	}
}

macro_rules! try_log
{
	($sink:expr, $x:expr) => {{
		match $x {
			Ok(x) => x,
			Err(x) => {
				$sink.message(x.clone());
				return Err(x);
			}
		}
	}};
}

macro_rules! logerr
{
	($sink:expr, $x:expr) => {{
		let x: Cow<'static, str> = cow!($x);
		$sink.message(x.clone());
		return Err(x);
	}};
	($sink:expr, $x:expr, $($p:expr),*) => {{
		let x: Cow<'static, str> = cow!($x,$($p),*);
		$sink.message(x.clone());
		return Err(x);
	}}
}

static LONG_ECDSA_P256_K_OID: [u8;19] = [6, 7, 42, 134, 72, 206, 61, 2, 1, 6, 8, 42, 134, 72, 206, 61, 3, 1, 7];
static LONG_ECDSA_P384_K_OID: [u8;16] = [6, 7, 42, 134, 72, 206, 61, 2, 1, 6, 5, 43, 129, 4, 0, 34];
static LONG_ECDSA_P521_K_OID: [u8;16] = [6, 7, 42, 134, 72, 206, 61, 2, 1, 6, 5, 43, 129, 4, 0, 35];
static ED25519_KEY_OID: [u8; 5] = [6, 3, 43, 101, 112];
static ED448_KEY_OID: [u8; 5] = [6, 3, 43, 101, 113];


//An internal function.
#[doc(hidden)]
pub fn convert_chain_to_der<'a>(chain: &'a [u8], label: &str) -> Result<Cow<'a, [u8]>, CertificateLoadError>
{
	if looks_like_pem(chain, label) {
		Ok(Cow::Owned(decode_pem(chain, label, true).map_err(|y|
			CertificateLoadError::LooksLikePem(y))?))
	} else {
		Ok(Cow::Borrowed(chain))
	}
}

//Adapter that adds a algorithm into signature.
struct AddAlgorithmAdapter
{
	//The algorithm to add.
	algorithm: u16,
	//Sender to send the resolved future on.
	resender: Option<FutureSender<Result<(u16, Vec<u8>), ()>>>
}

impl AddAlgorithmAdapter
{
	fn adapt(&self, val: Vec<u8>) -> (u16, Vec<u8>)
	{
		(self.algorithm, val)
	}
}

impl FutureCallback<Result<Vec<u8>, ()>> for AddAlgorithmAdapter
{
	fn on_settled(&mut self, val: Result<Vec<u8>, ()>) -> Result<Vec<u8>, ()>
	{
		let mut tmp = None;
		swap(&mut self.resender, &mut tmp);
		tmp.map(|resender|resender.settle(val.map(|x|self.adapt(x))));
		Err(())	//Nobody ever reads this.
	}
}


//A certificate that has been selected for signing.
#[derive(Clone)]
struct SelectedCertificate
{
	//The keypair that does the signing.
	keypair: Arc<Box<KeyPair+Send+Sync>>,
	//The TLS algorithm number to use.
	tls_algorithm: u16,
	//Is ECDSA-type algorithm (according to TLS 1.2 rules)?
	ecdsa_type: bool,
	//The certificate chain to send.
	certificate: CertificateChain,
	//The OCSP staple to send.
	ocsp_staple: OcspResponse,
	//The SCT list to send.
	sct_list: StapledSctList,
}

impl CertificateSigner for SelectedCertificate
{
	fn sign(&self, data: &[u8]) -> FutureReceiver<Result<(u16, Vec<u8>), ()>>
	{
		//The KeyPair sign method returns future for Vec<u8>, but this function needs to return future for
		//(u16, Vec<u8>). Therefore we need an adapter to add the algorithm.
		let (with_algo_sender, with_algo_receiver) = create_future::<Result<(u16, Vec<u8>), ()>>();
		let algo = self.tls_algorithm;
		let ctx = AddAlgorithmAdapter{algorithm: algo, resender: Some(with_algo_sender)};
		//Start the signing process.
		let mut will_be_signature = self.keypair.sign(data, algo);
		//Now, register the adapter we just created.
		if let Some(mut ctx) = will_be_signature.settled_cb(Box::new(ctx)) {
			//Ok, the signing is already complete, Forward the value.
			let _ = match will_be_signature.read() {
				Ok(x) => ctx.on_settled(x),
				_ => return FutureReceiver::from(Err(())),
			};
		}
		//Will be chained here when ready.
		with_algo_receiver
	}
	fn is_ecdsa(&self) -> bool
	{
		self.ecdsa_type
	}
	fn get_certificate(&self) -> CertificateChain
	{
		self.certificate.clone()
	}
	fn get_ocsp(&self) -> OcspResponse
	{
		self.ocsp_staple.clone()
	}
	fn get_scts(&self) -> StapledSctList
	{
		self.sct_list.clone()
	}
}

//A keyring of keys.
#[derive(Clone)]
struct Keyring
{
	map: BTreeMap<ChecksumOutput, Arc<Box<KeyPair+Send+Sync>>>,
	names: BTreeMap<String, ChecksumOutput>
}

impl Keyring
{
	//Create a new keyring.
	fn new() -> Keyring
	{
		Keyring{map:BTreeMap::new(), names:BTreeMap::new()}
	}
	//Add a new key to the keyring.
	fn add(&mut self, key: Arc<Box<KeyPair+Send+Sync>>, name: &str)
	{
		let chksum = checksum((key.get_public_key().0).deref());
		self.map.insert(chksum, key);
		self.names.insert(name.to_owned(), chksum);
	}
	//Look up a key from keyring, using the public key as lookup key.
	fn lookup(&self, spki: &[u8]) -> Option<Arc<Box<KeyPair+Send+Sync>>>
	{
		let chksum = checksum(spki);
		self.map.get(&chksum).map(|x|x.clone())
	}
	fn enumerate_private_keys(&self) -> Vec<(String, &'static str)>
	{
		let mut list = Vec::new();
		for i in self.names.iter() {
			if let Some(x) = self.map.get(i.1) {
				list.push((i.0.to_owned(), x.get_key_type()));
			}
		}
		list
	}
	fn get_public_key(&self, privkey: &str) -> Option<SubjectPublicKeyInfo>
	{
		let checksum = match self.names.get(privkey) {
			Some(x) => x,
			None => return None
		};
		self.map.get(checksum).map(|x|x.get_public_key())
	}
	fn sign_csr(&self, privkey: &str, csr_params: &CsrParams) -> FutureReceiver<Result<Vec<u8>, ()>>
	{
		let checksum = match self.names.get(privkey) {
			Some(x) => x,
			None => return FutureReceiver::from(Err(()))
		};
		let key = match self.map.get(checksum) {
			Some(x) => x,
			None => return FutureReceiver::from(Err(()))
		};
		key.sign_csr(csr_params)
	}
}

//A certificate available for signing.
#[derive(Clone)]
struct AvailableCertificate
{
	//The keypair for certificate.
	keypair: Arc<Box<KeyPair+Send+Sync>>,
	//The certificate chain to send.
	certificate: CertificateChain,
	//The OCSP staple to send.
	ocsp_staple: OcspResponse,
	//The SCT list to send.
	sct_list: StapledSctList,
	//Mask of available algorithms, using SIGALGO_* constants in bitmask.
	algorithms: u64,
	//The algorithms needed by the chain, using SIGALGO_* constants in bitmask.
	need_chain_algs: u64,
	//Priority boost to apply.
	prio_boost: u64,
	//The validity time of the certificate.
	cert_validity: Range<Timestamp>,
	//The validity time of the OCSP staple.
	ocsp_validity: Range<Timestamp>,
	//Logger to use for messages.
	log: Arc<Box<Logging+Send+Sync>>,
	//The certificate requires OCSP?
	needs_ocsp: bool,
	//The certificate requires SCT?
	needs_sct: bool,
	//OCSP has at least one stapled SCT?
	have_ocsp_sct: bool,
	//Certificate has at least one stapled SCT?
	have_cert_sct: bool,
	//The range for issuer in the EE certificate.
	ee_issuer: Range<usize>,
	//The range for serial in the EE certificate.
	ee_serial: Range<usize>,
	//Name of certificate to use in messages
	call_me_as: Arc<String>,
	//The list of SANs from the certificate.
	san_list: Arc<Vec<Vec<u8>>>,
	//Self-signed flag.
	selfsigned: bool,
}

impl AvailableCertificate
{
	//Create a new available certificate. OCSP and SCT staples are initially empty.
	fn new<Time:TimeInterface>(keyring: &Keyring, chain: &[Certificate], call_me_as: &str,
		log: Arc<Box<Logging+Send+Sync>>, time_now: &Time) ->
		Result<AvailableCertificate, CertificateLoadError>
	{
		use self::CertificateLoadError::*;
		//Empty certificates are not allowed.
		fail_if!(chain.len() == 0, CertificateChainEmpty);
		//Parse the certificate chain.
		let mut keypair = None;
		let mut certificates = Vec::with_capacity(chain.len());
		let mut need_chain_algs = 0;
		let mut prio_boost = 0;
		let mut issuer_range = 0..0;
		let mut serial_range = 0..0;
		let mut cert_validity = Timestamp::negative_infinity()..Timestamp::positive_infinity();
		let mut needs_ocsp = false;
		let mut needs_sct = false;
		let mut have_cert_sct = false;
		let mut selfsigned = false;
		let mut cert_refs = Vec::with_capacity(chain.len());
		let mut san_list = Vec::new();
		for i in chain.iter().enumerate() {
			let content: &[u8] = ((i.1).0).deref();
			cert_refs.push(content);
			let mut src = Source::new(content);
			let cert = match src.read_asn1_value(ASN1_SEQUENCE, |_|()) {
				Ok(x) => x.outer,
				Err(_) => fail!(ValidHeaderMissing(i.0)),
			};
			fail_if!(!src.at_end(), JunkAfterEndOfCert(i.0));
			let parsed = match ParsedCertificate::from(cert).map_err(|x|x) {
				Ok(x) => x,
				Err(x) => fail!(CertParseError(i.0, x))
			};
			//Grab the algorithm. If not known, default to 1<<63, which is invalid.
			let alg = WSSigAlgo::from(parsed.signature.algorithm).and_then(|x|
				x.unwrap_if_allowed(0).ok()).and_then(|x|x.sigalgo_constant()).unwrap_or(
				1u64 << 63);
			if i.0 == 0 {
				//Is selfsigned?
				selfsigned = parsed.issuer.same_as(parsed.subject);
				//The EE certificate is handled a bit specially.
				//Look up the private key to use.
				keypair = keyring.lookup(parsed.pubkey);
				san_list = parsed.dnsnames.iter().map(|x|x.to_owned()).collect();
				//The issuer and serial are taken from here.
				issuer_range = subslice_to_range(cert, parsed.issuer.0).map_err(|_|
					assert_failure!("Certificate issuer not from certificate???"))?;
				serial_range = subslice_to_range(cert, parsed.serial_number).map_err(|_|
					assert_failure!("Certificate serial not from certificate???"))?;
				//Has stapled SCTs?
				have_cert_sct = parsed.scts.iter().next().is_some();
				//Compute priority boost according to the algorithm.
				let mut src = Source::new(parsed.pubkey);
				let algo2 = src.read_asn1_value(ASN1_SEQUENCE, |_|()).map(|x|x.value).
					and_then(|mut x|x.read_asn1_value(ASN1_SEQUENCE, |_|()).map(|x|x.raw_p));
				prio_boost = if algo2 == Ok(&ED25519_KEY_OID) {
					5u64	//+5 for Ed25519 keys.
				} else if algo2 == Ok(&ED448_KEY_OID) {
					4u64	//+4 for Ed448 keys.
				} else if algo2 == Ok(&LONG_ECDSA_P256_K_OID) {
					3u64	//+3 for ECDSA P-256 keys.
				} else if algo2 == Ok(&LONG_ECDSA_P384_K_OID) {
					2u64	//+2 for ECDSA P-384 keys.
				} else if algo2 == Ok(&LONG_ECDSA_P521_K_OID) {
					1u64	//+1 for ECDSA P-521 keys.
				} else {
					0
				} << 56;
			}
			//Don't count algorithms of self-signed certificates for need-to-know.
			if !parsed.issuer.same_as(parsed.subject) { need_chain_algs |= alg; }
			certificates.push(i.1.clone());
			cert_validity.start = max(cert_validity.start, parsed.not_before);
			cert_validity.end = min(cert_validity.end, parsed.not_after);
		}
		fail_if!(cert_validity.start > cert_validity.end, NoEffectiveCertificateLifetime);
		let keypair = match keypair {
			Some(x) => x,
			None => fail!(NoPrivateKeyAvailable)
		};
		let _algorithms = keypair.get_schemes().to_owned();
		let mut algorithms = 0;
		for i in _algorithms.iter() {
			for j in TLS_SIG_ALGOS.iter().enumerate() {
				//TLS_SIG_ALGOS contains at most 63 entries.
				if *j.1 == *i { algorithms |= 1u64 << (j.0 as u32); }
			}
		}
		fail_if!(algorithms == 0, NoKnownAlgorithms);
		let kp_pubkey = keypair.get_public_key();
		let kp_pubk: &[u8] = (kp_pubkey.0).deref().deref();
		//There is at least one certificate, so slices are OK.
		match sanity_check_certificate_chain_load(cert_refs[0], &cert_refs[1..], kp_pubk, time_now,
			&mut needs_ocsp, &mut needs_sct, true) {
			Ok(_) => (),
			Err(x) => fail!(SanityCheckFailed(x))
		};
		Ok(AvailableCertificate {
			keypair: keypair.clone(),
			certificate: CertificateChain(Arc::new(certificates)),
			ocsp_staple: OcspResponse(None),
			sct_list: StapledSctList(None),
			algorithms: algorithms,
			need_chain_algs: need_chain_algs,
			prio_boost: prio_boost,
			cert_validity: cert_validity,
			//No OCSP. Note, the endpoints are in wrong order.
			ocsp_validity: Timestamp::positive_infinity()..Timestamp::negative_infinity(),
			log: log,
			needs_ocsp: needs_ocsp,
			needs_sct: needs_sct,
			have_ocsp_sct: false,		//No OCSP.
			have_cert_sct: have_cert_sct,
			ee_issuer: issuer_range,
			ee_serial: serial_range,
			call_me_as: Arc::new(call_me_as.to_owned()),
			san_list: Arc::new(san_list),
			selfsigned: selfsigned,
		})
	}
	//Read certificate chain from file.
	fn read_certifcate_chain<R:IoRead>(file: &mut R) -> Result<Vec<Certificate>, CertificateLoadError>
	{
		let mut _chain = Vec::new();
		file.read_to_end(&mut _chain).map_err(|x|CertificateLoadError::ReadError(format!("{}", x)))?;
		let chain = convert_chain_to_der(&_chain, " CERTIFICATE")?;
		fail_if!(chain.len() == 0, CertificateLoadError::CertificateChainEmpty);
		let mut cchain = Vec::<Certificate>::new();
		let mut chainsrc = Source::new(chain.deref());
		//Split the certificates in chain.
		while !chainsrc.at_end() {
			let cert = match chainsrc.read_asn1_value(ASN1_SEQUENCE, |_|()) {
				Ok(x) => x.outer,
				Err(_) => fail!(CertificateLoadError::ValidHeaderMissing(cchain.len()))
			};
			cchain.push(Certificate(Arc::new(cert.to_owned())));
		}
		Ok(cchain)
	}
	//Try adding a SCT response.
	fn try_add_sct(&mut self, sct: Arc<Vec<u8>>, _call_sct_as: &str) -> Result<(), CertificateLoadError>
	{
		//sct_list is immutable, so we have to create a new one to add the SCT.
		let old_sct_list = self.sct_list.clone();
		let mut ret = match old_sct_list {
			StapledSctList(Some(x)) => {
				//The entry is appended. The length can't be near overflow.
				let mut ret = Vec::with_capacity(x.len()+1);
				ret.extend_from_slice(&x);
				ret
			},
			StapledSctList(None) => {
				//This becomes the only entry.
				Vec::with_capacity(1)
			}
		};
		//Add the new entry. Only one write is performed, so update can't be interrupted.
		ret.push(StapledSct(sct));
		self.sct_list = StapledSctList(Some(Arc::new(ret)));
		Ok(())
	}
	//Read a SCT response from file.
	fn read_sct<R:IoRead>(file: &mut R) -> Result<Arc<Vec<u8>>, CertificateLoadError>
	{
		let mut _data = Vec::new();
		file.read_to_end(&mut _data).map_err(|x|CertificateLoadError::ReadError(format!("{}", x)))?;
		Ok(Arc::new(_data))
	}
	//Try setting OCSP response.
	fn try_set_ocsp<Time:TimeInterface>(&mut self, ocsp: Option<Arc<Vec<u8>>>, _call_ocsp_as: &str,
		current_time: &Time) -> Result<(), CertificateLoadError>
	{
		let staple_data = match &ocsp {
			&Some(ref x) => x,
			&None => {
				//We can clear the OCSP staple at any time.
				//We assert memory fences to ensure that update to structures can't be
				//interrupted by a panic.
				fence(AtomicOrdering::SeqCst);
				//We don't have OCSP anymore. Note, that endpoints are in the wrong order.
				self.have_ocsp_sct = false;
				self.ocsp_validity = Timestamp::positive_infinity()..Timestamp::negative_infinity();
				self.ocsp_staple = OcspResponse(None);
				fence(AtomicOrdering::SeqCst);
				return Ok(());
			}
		};
		//There has to be at least one certificate, the constructor checks this.
		//Also, slice the issuer and serial out of the EE cert.
		let ee_cert = (self.certificate.0)[0].0.deref();
		let issuer = CertificateIssuer(&ee_cert[self.ee_issuer.clone()]);
		let serial = &ee_cert[self.ee_serial.clone()];
		//Sanity-check the OCSP response.
		let ocsp_validity = match OcspSanityCheckResults::from(staple_data.deref(), issuer, serial,
			current_time) {
			Ok(x) => {
				x.not_before..x.not_after
			},
			Err(x) => fail!(CertificateLoadError::OcspSanityCheckFailed(x)),
		};
		let ocsp_scts = OcspDumpedScts::from(staple_data.deref()).map(|x|x.list.len() > 0).unwrap_or(
			false);
		let response = OcspResponse(Some(staple_data.clone()));
		//We assert memory fences to ensure that update to structures can't be interrupted by a
		//panic.
		fence(AtomicOrdering::SeqCst);
		self.ocsp_validity = ocsp_validity;
		self.have_ocsp_sct = ocsp_scts;
		self.ocsp_staple = response;
		fence(AtomicOrdering::SeqCst);
		Ok(())
	}
	//Read a OCSP response from file.
	fn read_ocsp<R:IoRead>(file: &mut R) -> Result<Option<Arc<Vec<u8>>>, CertificateLoadError>
	{
		let mut _data = Vec::new();
		file.read_to_end(&mut _data).map_err(|x|CertificateLoadError::ReadError(format!("{}", x)))?;
		Ok(Some(Arc::new(_data)))
	}
	//Make a signer using specified algorithm.
	fn signer_with_algo<Time:TimeInterface>(&self, algorithm: u16, current_time: &Time) -> SelectedCertificate
	{
		let time = current_time.get_time();
		let valid_ocsp = time >= self.ocsp_validity.start && time < self.ocsp_validity.end;
		let ecdsa_type = TLS_SIG_ALGOS.iter().enumerate().filter(|x|*(x.1) == algorithm).map(|x|
			(1 << x.0) & ECDSA_SIG_MASK != 0).next().unwrap_or(true);
		SelectedCertificate {
			keypair: self.keypair.clone(),
			tls_algorithm: algorithm,
			ecdsa_type: ecdsa_type,
			certificate: self.certificate.clone(),
			//We have to drop OCSP staple if it isn't valid.
			ocsp_staple: if valid_ocsp { self.ocsp_staple.clone() } else { OcspResponse(None) },
			sct_list: self.sct_list.clone()
		}
	}
	//Is the certificate any good at all?
	fn is_useable(&self, sig_flags: u64, ee_flags: u64, tls13: bool, selfsigned_ok: bool,
		allow_unknown_algo: bool, timenow: Timestamp) -> bool
	{
		let mut ok = true;
		//No selfsigneds unless asked. Non-selfsigneds are OK even if asked.
		ok &= selfsigned_ok || !self.selfsigned;
		//Compute intersection of available signature algorithms and check there is at least one valid
		//algorithm. If TLS 1.3 is in use, mask algorithms that can't be used with TLS 1.3.
		let tls_mask = if tls13 { TLS13_SIG_MASK } else { !0u64 };
		ok &= ee_flags & self.algorithms & tls_mask != 0;
		//Supported CA signature algorithms? We skip this check if arbitrary algorithms are allowed in the
		//chain. Also, self-signed roots are not counted, because need_chain_algs does not accumulate those.
		//Note that we check that complement of sig_flags does not contain any algorithms from
		//need_chain_algs, as this is equivalent to check that sig_flags contains all algorithms in
		//need_chain_algs.
		ok &= allow_unknown_algo || self.need_chain_algs & !sig_flags == 0;
		//If certificate needs OCSP, it must be within validity.
		ok &= !self.needs_ocsp || timenow.in_range(&self.ocsp_validity);
		//If certificate needs SCT, we have to have at least one stapled one, OCSP with one or cerificate
		//has to have one.
		ok &= !self.needs_sct || self.sct_list.0.is_some() || self.have_ocsp_sct ||self.have_cert_sct;
		//Check not-yet-valid or expired.
		ok &= timenow.in_range(&self.cert_validity);
		//All checks done.
		ok
	}
	//Score certificate, assuming it is usable.
	fn score_certificate(&self) -> u64
	{
		const TIME_BIAS: i64 = 1i64 << 55;
		let ts_score = min(max(self.cert_validity.end.into_inner(), -TIME_BIAS), TIME_BIAS) + TIME_BIAS;
		self.prio_boost.saturating_add(ts_score as u64)
	}
	//If the certificate isn't suitable, returns None. Otherwise returns score (bigger is better) for this
	//certificate.
	fn is_good(&self, sig_flags: u64, ee_flags: u64, tls13: bool, selfsigned_ok: bool,
		allow_unknown_algo: bool, timenow: Timestamp) -> Option<u64>
	{
		if self.is_useable(sig_flags, ee_flags, tls13, selfsigned_ok, allow_unknown_algo, timenow) {
			//Assume valid.
			Some(self.score_certificate())
		} else {
			None
		}
	}
}

//A handle for AvailableCertificate
#[derive(Clone)]
struct SharedAvailableCertificate
{
	inner: Arc<RwLock<AvailableCertificate>>,
	call_me_as: Arc<String>,
}

impl SharedAvailableCertificate
{
	fn new(cert: AvailableCertificate) -> SharedAvailableCertificate
	{
		let call_me_as = cert.call_me_as.clone();
		SharedAvailableCertificate{inner: Arc::new(RwLock::new(cert)), call_me_as: call_me_as}
	}
	fn with_read_access<F,T>(&self, cb: F) -> T where F: FnOnce(&AvailableCertificate) -> T
	{
		//We overrule poisons, because the updates can't be interrupted by a panic.
		match self.inner.read() {
			Ok(x) => cb(x.deref()),
			Err(x) => cb(x.into_inner().deref()),
		}
	}
	fn with_write_access<F,T>(&self, cb: F) -> T where F: FnOnce(&mut AvailableCertificate) -> T
	{
		//We overrule poisons, because the updates can't be interrupted by a panic.
		match self.inner.write() {
			Ok(mut x) => cb(x.deref_mut()),
			Err(x) => cb(x.into_inner().deref_mut()),
		}
	}
}

fn remove_from_vec2(x: &mut Vec<u64>, item: u64)
{
	let mut idx = 0;
	while idx < x.len() {
		if x[idx] == item {
			x.swap_remove(idx);
		} else {
			idx += 1;	//Array index so in-range.
		}
	}
}

#[derive(Clone)]
struct CertificateJar
{
	contents: BTreeMap<u64, SharedAvailableCertificate>,
	name_index: BTreeMap<Vec<u8>, Vec<u64>>,
	wild_name_index: BTreeMap<Vec<u8>, Vec<u64>>,
	all_index: Vec<u64>,
	private_keys: Keyring,
	next_id: u64,
	log: Arc<Box<Logging+Send+Sync>>,
	ct_logs: Vec<TrustedLog>,
}

impl CertificateJar
{
	fn lookup_certificate(&self, id: u64) -> Option<SharedAvailableCertificate>
	{
		self.contents.get(&id).map(|x|x.clone())
	}
	fn delete_certificate(&mut self, id: u64)
	{
		//TODO: Optimize this.
		for i in self.name_index.iter_mut() { remove_from_vec2(i.1, id); }
		for i in self.wild_name_index.iter_mut() { remove_from_vec2(i.1, id); }
		remove_from_vec2(&mut self.all_index, id);
		self.contents.remove(&id);
	}
	fn add_certificate(&mut self, acert: AvailableCertificate) -> Result<u64, CertificateLoadError>
	{
		let san_list = acert.san_list.clone();
		let handle = SharedAvailableCertificate::new(acert);
		//We need to use the names to construct the maps.
		{
			//Allocate ID number. Hopefully these don't grow too large...
			fail_if!(self.next_id.checked_add(1).is_none(), CertificateLoadError::HandleOverflowed);
			let id = self.next_id;
			self.next_id = self.next_id + 1;
			self.contents.insert(id, handle);
			self.all_index.push(id);
			for i in san_list.iter() {
				let sni = i;
				//Insert wildcard entries for wildcard certs, and normal entries for non-wildcard
				//ones.
				if sni.len() >= 2 && &sni[..2] == b"*." {
					let sniwild = &sni[2..];
					self.wild_name_index.entry(sniwild.to_owned()).or_insert_with(||Vec::new()).
						push(id);
				} else {
					self.name_index.entry(sni.to_owned()).or_insert_with(||Vec::new()).push(id);
				};
			}
			//TODO: Insert whatever locator entries exist.
			return Ok(id);
		}
	}
	fn select_best_among(&self, certs: &[u64], best_cert: &mut Option<SharedAvailableCertificate>,
		best_score: &mut u64, sig_flags: u64, ee_flags: u64, tls13: bool, selfsigned_ok: bool,
		allow_unknown_algo: bool)
	{
		let timenow = TimeNow.get_time();
		for cnum in certs.iter() {
			if let Some(y) = self.contents.get(cnum) {
				let score = y.with_read_access(|x|x.is_good(sig_flags, ee_flags, tls13,
					selfsigned_ok, allow_unknown_algo, timenow));
				if let Some(score) = score {
					if score > *best_score {
						*best_cert = Some(y.clone());
						*best_score = score;
					}
				}
			}
		}
	}
	fn select_best_certificate(&self, refname: Option<&str>, sig_flags: u64, ee_flags: u64, tls13: bool,
		selfsigned_ok: bool, allow_unknown_algo: bool) -> Option<SharedAvailableCertificate>
	{
		let mut best_cert = None;
		let mut best_score = 0;
		if let Some(sni) = refname {
			let sni = sni.as_bytes();
			let sniwild = sni.splitn(2, |x|*x==b'.').nth(1).unwrap_or(&[]);
			if let Some(x) = self.name_index.get(sni) {
				self.select_best_among(x, &mut best_cert, &mut best_score, sig_flags,
					ee_flags, tls13, selfsigned_ok, allow_unknown_algo);
			}
			if let Some(x) = self.wild_name_index.get(sniwild) {
				self.select_best_among(x, &mut best_cert, &mut best_score, sig_flags,
					ee_flags, tls13, selfsigned_ok, allow_unknown_algo);
			}
		} else {
			self.select_best_among(&self.all_index, &mut best_cert, &mut best_score, sig_flags,
				ee_flags, tls13, selfsigned_ok, allow_unknown_algo);
		}
		//If none was found, and allow_unknown_algo is not set, search again with it set.
		if !allow_unknown_algo && best_cert.is_none() {
			self.select_best_certificate(refname, sig_flags, ee_flags, tls13, selfsigned_ok, true)
		} else {
			best_cert
		}
	}
	fn lookup(&self, criteria: &CertificateLookupCriteria) -> Result<Box<CertificateSigner+Send>, ()>
	{
		let best_cert = match self.select_best_certificate(criteria.sni, criteria.sig_flags,
			criteria.ee_flags, criteria.tls13, criteria.selfsigned_ok, false) {
			Some(x) => x,
			None => {
				self.log.message(cow!("Failed to find suitable certificate for `{}`",
					criteria.sni.unwrap_or("<default>")));
				return Err(());
			}
		};
		best_cert.with_read_access(|cert| {
			let available_algos = cert.algorithms & criteria.ee_flags &
				if criteria.tls13 { TLS13_SIG_MASK } else { !0u64 };
			let mut algoidx = 0usize;
			let mut tmp = available_algos;
			while tmp > 0 {
				algoidx += 1;		//This will be at most 64
				tmp >>= 1;
			}
			fail_if!(algoidx == 0, ());	//No suitable certificate found.
			algoidx -= 1;
			let algo = TLS_SIG_ALGOS.get(algoidx).map(|x|*x).ok_or(())?;
			Ok(Box::new(cert.signer_with_algo(algo, &TimeNow)) as Box<CertificateSigner+Send+'static>)
		})
	}
	pub fn add_trusted_log(&mut self, log: &TrustedLog)
	{
		self.ct_logs.push(log.clone());
		self.log.message(cow!("Added new trusted CT log: {}", log.name));
	}
}

///Handle for local certificate.
///
///This handle is used for referencing local certificates, for adding or updating staple data.
#[derive(Clone)]
pub struct LocalServerCertificateHandle
{
	jar: Arc<RwLock<CertificateJar>>,
	id: u64,
	log: Arc<Box<Logging+Send+Sync>>,
}


impl PartialEq for LocalServerCertificateHandle
{
	fn eq(&self, another: &Self) -> bool
	{
		if self.jar.deref() as *const _ != another.jar.deref() as *const _ { return false; }
		if self.id != another.id { return false; }
		true
	}
}

impl Eq for LocalServerCertificateHandle {}

impl Hash for LocalServerCertificateHandle
{
	fn hash<H:Hasher>(&self, state: &mut H)
	{
		state.write_usize(self.jar.deref() as *const _ as usize);
		state.write_u64(self.id);
	}
}

impl LocalServerCertificateHandle
{
	///Clear OCSP stapling for certificate.
	///
	///# Parameters:
	///
	/// * `self`: The certificate to update.
	///
	///# Failures:
	///
	/// * If operation fails, it returns cause of failure as string.
	pub fn clear_ocsp_staple(&mut self) -> Result<(), Cow<'static, str>>
	{
		let cert = match self.jar.read() {
			Ok(x) => {
				match x.lookup_certificate(self.id) {
					Some(x) => x,
					None => logerr!(self.log, "Stale certificate handle")
				}
			},
			Err(_) => logerr!(self.log, "Failed to lock certificate jar")
		};
		cert.with_write_access(|crt|{
			crt.try_set_ocsp(None, "", &TimeNow).map_err(|_|cow!("Failed to clear OCSP staple"))
		})
	}
	///Add or update OCSP stapling for certificate.
	///
	///# Parameters:
	///
	/// * `self`: The certificate to update.
	/// * `staple`: The OCSP staple data to associate.
	/// * `name`: Name of OCSP staple to use in error messages.
	///
	///# Failures:
	///
	/// * If operation fails, it returns cause of failure as string.
	///
	///# Notes:
	///
	/// * The data is in raw ASN.1 form.
	pub fn set_ocsp_staple<R:IoRead>(&mut self, staple: &mut R, name: &str) -> Result<(), Cow<'static, str>>
	{
		let cert = match self.jar.read() {
			Ok(x) => {
				match x.lookup_certificate(self.id) {
					Some(x) => x,
					None => logerr!(self.log, "Stale certificate handle")
				}
			},
			Err(_) => logerr!(self.log, "Failed to lock certificate jar")
		};
		let response = match AvailableCertificate::read_ocsp(staple) {
			Ok(x) => x,
			Err(x) => logerr!(self.log, "Can't set OCSP staple `{}` for `{}`: {}", name,
				cert.call_me_as, x),
		};
		cert.with_write_access(|crt|{
			match crt.try_set_ocsp(response, name, &TimeNow) {
				Ok(_) => Ok(()),
				Err(x) => logerr!(self.log, "Can't set OCSP staple `{}` for `{}`: {}", name,
					crt.call_me_as, x),
			}
		})
	}
	///Add or update OCSP stapling for certificate from file.
	///
	///# Parameters:
	///
	/// * `self`: The certificate to update.
	/// * `staple`: The name of file to load OCSP staple data from.
	///
	///# Failures:
	///
	/// * If operation fails, it returns cause of failure as string.
	///
	///# Notes:
	///
	/// * The data is in raw ASN.1 form.
	pub fn set_ocsp_staple_file<P:AsRef<Path>>(&mut self, staple: P) -> Result<(), Cow<'static, str>>
	{
		let staple = staple.as_ref();
		let name = format!("{}", staple.display());
		let mut _staple = match File::open(staple) {
			Ok(x) => x,
			Err(x) => logerr!(self.log, "Can't open OCSP staple `{}`: {}", name, x)
		};
		//This has its own logging.
		self.set_ocsp_staple(&mut _staple, &name)
	}
	//No longer present.
	#[doc(hidden)]
	pub fn set_ocsp_staple_file_multishot<P:AsRef<Path>>(&mut self, _staple: P)
	{
		self.log.message(cow!("OCSP multishot is not supported"));
	}
	//No longer present.
	#[doc(hidden)]
	pub fn get_ocsp_last_multishot_error(&self) -> Result<(), (u64, Cow<'static, str>)>
	{
		Err((1, cow!("OCSP multishot is not supported")))
	}
	///Add or update SCT stapling for certificate.
	///
	///# Parameters:
	///
	/// * `self`: The certificate to update.
	/// * `staple`: The SCT staple data to associate.
	/// * `name`: Name of staple to use in error messages.
	///
	///# Failures:
	///
	/// * If operation fails, it returns cause of failure as string.
	///
	///# Notes:
	///
	/// * The data is in the raw `SerializedSct` format.
	/// * Call this (or `.add_sct_staple_file()`) multiple times if you have multiple SCTs to send.
	pub fn add_sct_staple<R:IoRead>(&mut self, staple: &mut R, name: &str) -> Result<(), Cow<'static, str>>
	{
		let cert = match self.jar.read() {
			Ok(x) => {
				match x.lookup_certificate(self.id) {
					Some(x) => x,
					None => logerr!(self.log, "Stale certificate handle")
				}
			},
			Err(_) => logerr!(self.log, "Failed to lock certificate jar")
		};
		let response = match AvailableCertificate::read_sct(staple) {
			Ok(x) => x,
			Err(x) => logerr!(self.log, "Can't add SCT staple `{}` for `{}`: {}", name,
				cert.call_me_as, x),
		};
		//Grab the EE certficate and issuer key.
		let (eecert, isskey): (Vec<u8>, Vec<u8>) = cert.with_read_access(|crt|{
			let eecert = ((crt.certificate.0)[0].0).deref().clone();
			let isskey = if (crt.certificate.0).len() > 0 {
				let rcrt: &[u8] = ((crt.certificate.0)[1].0).deref();
				match extract_spki(rcrt) {
					Ok(x) => x.to_owned(),
					Err(_) => Vec::new()
				}
			} else {
				Vec::new()
			};
			(eecert, isskey)
		});
		if isskey.len() == 0 {
			logerr!(self.log, "Can't add SCT staple `{}` for `{}`: No issuer key available", name,
				cert.call_me_as);
		}
		match self.jar.read() {
			Ok(x) => {
				let mut sct_list = Vec::new();
				sct_list.push((false, response.deref().clone()));
				let mut unsuccessful = true;
				let mut dummy = false;
				match validate_scts(&eecert, &isskey, &mut sct_list, &mut unsuccessful, &mut dummy,
					&x.ct_logs, None) {
					Ok(_) => (),
					Err(x) => logerr!(self.log, "Can't add SCT staple `{}` for `{}`: \
						SCT failed sanity check: {}", name, cert.call_me_as, x)
				};
				if unsuccessful {
					logerr!(self.log, "Can't add SCT staple `{}` for `{}`: \
						SCT was issued by unknown log", name, cert.call_me_as);
				}
			},
			Err(_) => logerr!(self.log, "Failed to lock certificate jar")
		};
		cert.with_write_access(|crt|{
			match crt.try_add_sct(response, name) {
				Ok(_) => Ok(()),
				Err(x) => logerr!(self.log, "Can't add SCT staple `{}` for `{}`: {}", name,
					crt.call_me_as, x),
			}
		})
	}
	///Add or update SCT stapling for certificate from file.
	///
	///# Parameters:
	///
	/// * `self`: The certificate to update.
	/// * `staple`: The name of file to load SCT staple data from.
	///
	///# Failures:
	///
	/// * If operation fails, it returns cause of failure as string.
	///
	///# Notes:
	///
	/// * The data is in the raw `SerializedSct` format.
	/// * Call this (or `.add_sct_staple()`) multiple times if you have multiple SCTs to send.
	pub fn add_sct_staple_file<P:AsRef<Path>>(&mut self, staple: P) -> Result<(), Cow<'static, str>>
	{
		let staple = staple.as_ref();
		let mut _staple = match File::open(staple) {
			Ok(x) => x,
			Err(x) => logerr!(self.log, "Can't open SCT staple `{}`: {}", staple.display(), x)
		};
		let name = format!("{}", staple.display());
		self.add_sct_staple(&mut _staple, &name)
	}
	///Delete the certificate from store.
	///
	///# Parameters:
	///
	/// * `self`: The certificate to delete.
	pub fn delete(&self)
	{
		match self.jar.write() {
			Ok(mut x) => x.delete_certificate(self.id),
			Err(_) => self.log.message(cow!("Failed to lock certificate jar"))
		};
	}
}

///A certificate lookup that uses local certificates and associated private keys.
///
///This structure acts as Certificate lookup ([`CertificateLookup`]) that uses the added private keys and
///certificates to resolve certificate requests.
///
///The private keys are added by `.add_privkey()` method.
///
///The certificates (after you have added the corresponding private key, can be added using `.add()` and
///`.add_file()` methods.
///
///If you need OCSP or SCT stapling, save the handle resulting from adding the certificate to variable, and call
///its methods to set the OCSP or SCT stapling.
///
///[`CertificateLookup`]: trait.CertificateLookup.html
#[derive(Clone)]
pub struct LocalServerCertificateStore(Arc<RwLock<CertificateJar>>, Arc<Box<Logging+Send+Sync>>);

impl LocalServerCertificateStore
{
	///Get local handle for remote handle.
	///
	///# Parameters:
	///
	/// * `self`: The store to get the handle in.
	/// * `handle`: The remote handle.
	///
	///# Returns:
	///
	/// * If the handle is also valid in the local store, the handle in this store wrapped in `Some()`.
	/// * Otherwise, `None`.
	///
	///# Note:
	///
	/// * Only meaningful if one of the stores is cloned from the other (or if both stores are cloned from
	///the same store).
	pub fn remote_to_local_handle(&self, handle: &LocalServerCertificateHandle) ->
		Option<LocalServerCertificateHandle>
	{
		//Note, we need to check for the case where handle is already local in order to avoid relocking
		//a lock, which may not work.
		if self.0.deref() as *const _ == handle.jar.deref() as *const _ {
			return Some(handle.clone());
		}
		//The two stores are different.
		let log = self.1.clone();
		if let Ok(x) = self.0.read() {
			let local = match x.contents.get(&handle.id) {
				Some(x) => x,
				None => return None
			};
			if let Ok(y) = handle.jar.read() {
				let remote = match y.contents.get(&handle.id) {
					Some(x) => x,
					None => return None
				};
				//The handles must point to the same certificate.
				if local.deref() as *const _ == remote.deref() as *const _ {
					return Some(LocalServerCertificateHandle{
						jar: self.0.clone(),
						id: handle.id,
						log: log
					});
				}
			}
		};
		return None;
	}
	///Deep-clone the certifcate store. The new store will be duplicate of the old one.
	///
	///# Parameters:
	///
	/// * `self`: The store to clone.
	///
	///# Returns:
	///
	/// * The cloned store.
	///
	///# Notes:
	///
	/// * The certificates are shared between the stores. However, any removals or inserts only affect store
	///thos are done in.
	/// * Any keys added also only affect the store that is done in.
	pub fn deep_clone(&self) -> LocalServerCertificateStore
	{
		let log = self.1.clone();
		let newjar = match self.0.read() {
			Ok(x) => x.clone(),
			Err(x) => x.into_inner().clone()
		};
		LocalServerCertificateStore(Arc::new(RwLock::new(newjar)), log)
	}
	///Create a new certificate store.
	///
	///# Parameters:
	///
	/// * `log`: The log to use.
	///
	///# Returns:
	///
	/// * New certificate store.
	pub fn new<L:Logging+Send+Sync+'static>(log: L) -> LocalServerCertificateStore
	{
		let _log = Arc::new(Box::new(log) as Box<Logging+Send+Sync>);
		let _store = CertificateJar {
			contents: BTreeMap::new(),
			name_index: BTreeMap::new(),
			wild_name_index: BTreeMap::new(),
			all_index: Vec::new(),
			private_keys: Keyring::new(),
			next_id: 0,
			log:_log.clone(),
			ct_logs: Vec::new(),
		};
		LocalServerCertificateStore(Arc::new(RwLock::new(_store)), _log.clone())
	}
	///Add a private key into store.
	///
	///# Parameters:
	///
	/// * `self`: The certificate lookup to add private key to.
	/// * `privkey`: The private key.
	/// * `name`: The name of key to use in error messages.
	///
	///# Failures:
	///
	/// * If operation fails, it returns cause of failure as string.
	///
	///# Notes:
	///
	/// * This variant adds arbitrary instance of [`KeyPair`] trait, so you can use it to add private keys that
	///the library or application using it never see.
	///
	///[`KeyPair`]: types/advanced/trait.KeyPair.html
	pub fn add_privkey<K: KeyPair+Send+Sync+'static>(&mut self, privkey: K, name: &str) ->
		Result<(), Cow<'static, str>>
	{
		{
			let mut inner = match self.0.write() {
				Ok(x) => x,
				Err(_) => logerr!(self.1, "Internal error: Can't lock certificate store")
			};
			inner.private_keys.add(Arc::new(Box::new(privkey)), name);
		}
		self.1.message(cow!("Loaded private key `{}`", name));
		Ok(())
	}
	///Enumerate names of available private keys, and their types
	pub fn enumerate_private_keys(&self) -> Vec<(String, &'static str)>
	{
		let inner = match self.0.read() {
			Ok(x) => x,
			Err(_) => return Vec::new()
		};
		inner.private_keys.enumerate_private_keys()
	}
	///Get public key.
	pub fn get_public_key(&self, privkey: &str) -> Option<SubjectPublicKeyInfo>
	{
		let inner = match self.0.read() {
			Ok(x) => x,
			Err(_) => return None
		};
		inner.private_keys.get_public_key(privkey)
	}
	///Sign a CSR with private key.
	pub fn sign_csr(&self, privkey: &str, csr_params: &CsrParams) -> FutureReceiver<Result<Vec<u8>, ()>>
	{
		let inner = match self.0.read() {
			Ok(x) => x,
			Err(_) => return FutureReceiver::from(Err(()))
		};
		inner.private_keys.sign_csr(privkey, csr_params)
	}
	///Add a certificate to store.
	///
	///# Parameters:
	///
	/// * `self`: The certificate lookup to add certificate to.
	/// * `chain`: The certificate chain.
	/// * `name`: The name of chain to use in error messages.
	///
	///# Return value:
	///
	/// * The handle for the certificate. This is used to provode/update stapling data.
	///
	///# Failures:
	///
	/// * If operation fails, it returns cause of failure as string.
	///
	///# Notes:
	///
	/// * The private key corresponding to the first certificate has to be added before.
	/// * If the certificate is not self-signed, the second certificate in chain must sign the first.
	/// * This can be used to add certificates from slices, since `&[u8]: Read` (but note the `&mut` qualifier
	///on the Read type).
	/// * Currently DER and PEM formats are supported (multiple certificates are just concatenated).
	pub fn add<R:IoRead>(&mut self, chain: &mut R, name: &str) ->
		Result<LocalServerCertificateHandle, Cow<'static, str>>
	{
		let cchain = match AvailableCertificate::read_certifcate_chain(chain) {
			Ok(x) => x,
			Err(x) => logerr!(self.1, "Can't add certificate `{}`: {}", name, x)
		};
		let acert = {
			let inner = match self.0.read() {
				Ok(x) => x,
				Err(_) => logerr!(self.1, "Can't add certificate `{}`: Internal error: Can't \
					lock certificate store", name),
			};
			match AvailableCertificate::new(&inner.private_keys, &cchain, name, inner.log.clone(),
				&TimeNow) {
				Ok(x) => x,
				Err(x) => logerr!(self.1, "Can't add certificate `{}`: {}", name, x)
			}
		};
		let id = {
			let mut inner = match self.0.write() {
				Ok(x) => x,
				Err(_) => logerr!(self.1, "Can't add certificate `{}`: Internal error: Can't \
					lock certificate store", name),
			};
			match inner.add_certificate(acert) {
				Ok(x) => x,
				Err(x) => logerr!(self.1, "Can't add certificate `{}`: {}", name, x),
			}
		};
		self.1.message(cow!("Loaded certificate `{}` as #{}", name, id));
		Ok(LocalServerCertificateHandle {jar:self.0.clone(), id:id, log:self.1.clone()})
	}
	///Load a certificate from file.
	///
	///# Parameters:
	///
	/// * `self`: The certificate lookup to add certificate to.
	/// * `chain`: The filename of certificate chain file to load.
	///
	///# Return value:
	///
	/// * The handle for the certificate. This is used to provode/update stapling data.
	///
	///# Failures:
	///
	/// * If operation fails, it returns cause of failure as string.
	///
	///# Notes:
	///
	/// * The private key corresponding to the first certificate has to be added before.
	/// * If the certificate is not self-signed, the second certificate in chain must sign the first.
	/// * Currently DER and PEM formats are supported (multiple certificates are just concatenated).
	pub fn add_file<P: AsRef<Path>>(&mut self, chain: P) ->
		Result<LocalServerCertificateHandle, Cow<'static, str>>
	{
		let chain = chain.as_ref();
		let mut _chain = match File::open(chain) {
			Ok(x) => x,
			Err(x) => logerr!(self.1, "Can't open certificate `{}`: {}", chain.display(),
				x)
		};
		let name = format!("{}", chain.display());
		//Add has its own logging.
		self.add(&mut _chain, &name)
	}
	///Add a trusted CT log.
	///
	///# Parameters:
	///
	/// * `self`: The jar to manipulate.
	/// * `log`: The trusted log to add.
	///
	///# Notes:
	///
	/// * Trusted logs have to be added for dedicated SCT staples, in order to sanity-check them.
	pub fn add_trusted_log(&mut self, log: &TrustedLog)
	{
		match self.0.write() {
			Ok(mut x) => x.add_trusted_log(log),
			Err(x) => x.into_inner().add_trusted_log(log)
		};
	}
	//This is internal method, but needs to be exposed, since directory certs use it.
	#[doc(hidden)]
	pub fn get_logger(&self) -> Arc<Box<Logging+Send+Sync>> { self.1.clone() }
}

impl CertificateLookup for LocalServerCertificateStore
{
	fn lookup(&self, criteria: &CertificateLookupCriteria) ->
		FutureReceiver<Result<Box<CertificateSigner+Send>, ()>>
	{
		let inner = match self.0.read() {
			Ok(x) => x,
			Err(_) => {
				self.1.message(cow!("Internal error: Can't lock certificate store"));
				return FutureReceiver::from(Err(()))
			}
		};
		FutureReceiver::from(match inner.lookup(criteria) {
			Ok(x) => Ok(x),
			Err(_) => {
				self.1.message(cow!("Failed to find suitable certificate for `{}`",
					criteria.sni.unwrap_or("<default>")));
				Err(())
			}
		})
	}
	fn clone_self(&self) -> Box<CertificateLookup+Send>
	{
		Box::new(self.clone())
	}
}

impl KeyStoreOperations for LocalServerCertificateStore
{
	fn enumerate_private_keys(&self) -> Vec<(String, &'static str)>
	{
		self.enumerate_private_keys()
	}
	fn get_public_key(&self, privkey: &str) -> Option<SubjectPublicKeyInfo>
	{
		self.get_public_key(privkey)
	}
	fn sign_csr(&self, privkey: &str, csr_params: &CsrParams) -> FutureReceiver<Result<Vec<u8>, ()>>
	{
		self.sign_csr(privkey, csr_params)
	}
}
