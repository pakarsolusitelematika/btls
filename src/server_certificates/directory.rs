use super::{CertificateLookup, CertificateLookupCriteria, CertificateSigner, KeyStoreOperations,
	LocalServerCertificateHandle, LocalServerCertificateStore};
use ::certificate::ParsedCertificate;
use ::client_certificates::{LogHashFunction, TrustedLog};
use ::server_keys::LocalKeyPair;
use ::logging::Logging;
use ::stdlib::{Arc, Box, BTreeMap, Cow, Deref, FromStr, IoRead, Mutex, Ordering, OsString, String, swap, ToOwned,
	UNIX_EPOCH, Vec};
use ::system::{Builder, File, Path, PathBuf, read_dir};

use btls_aux_futures::FutureReceiver;
use btls_aux_keyconvert::{Base64Decoder, decode_pem, looks_like_pem};
use btls_aux_keypair::{CsrParams, SubjectPublicKeyInfo};
use btls_aux_serialization::is_single_asn1_structure;


type Logger = Arc<Box<Logging+Send+Sync+'static>>;

fn clone_cow(x: &Cow<'static, str>) -> Cow<'static, str>
{
	match x {
		&Cow::Borrowed(ref x) => Cow::Borrowed(x),
		&Cow::Owned(ref x) => Cow::Owned(x.clone())
	}
}

struct CertificateInfo
{
	handle: LocalServerCertificateHandle,
	ocsp_timestamp: Option<i64>,
	scts_seen: BTreeMap<OsString, ()>,
	ocsp_loaded: bool,
}

impl CertificateInfo
{
	fn read_file_and_decode_pem(path: &Path, logger: Logger, allow_multi: bool) -> Result<Vec<u8>, ()>
	{
		let mut handle = match File::open(&path) {
			Ok(x) => x,
			Err(x) => {
				logger.message(cow!("Can't open `{}`: {}", path.display(), x));
				return Err(())
			}
		};
		let mut content = Vec::new();
		match handle.read_to_end(&mut content) {
			Ok(_) => (),
			Err(x) => {
				logger.message(cow!("Can't read `{}`: {}", path.display(), x));
				return Err(())
			}
		};
		//PEM-decode if needed.
		let content = if looks_like_pem(&content, " CERTIFICATE") {
			match decode_pem(&content, " CERTIFICATE", allow_multi) {
				Ok(x) => x,
				Err(x) => {
					logger.message(cow!("Can't decode `{}` as PEM: {}", path.display(), x));
					return Err(())
				}
			}
		} else {
			content
		};
		//This won't triger above if the EE cert is not PEM.
		if !allow_multi && !is_single_asn1_structure(&content) {
			logger.message(cow!("Expected `{}` to contain one certificate", path.display()));
			return Err(());
		}
		Ok(content)
	}
	fn new(path: &Path, logger: Logger, mut inner: LocalServerCertificateStore) -> Result<CertificateInfo, ()>
	{
		let ee_path = path.join("certificate");
		let issuer_path = path.join("issuer");
		let end_entity = Self::read_file_and_decode_pem(&ee_path, logger.clone(), false)?;
		let issuer = Self::read_file_and_decode_pem(&issuer_path, logger.clone(), true)?;

		//Parse the EE certificate check if it is selfsigned.
		let eecert = match ParsedCertificate::from(&end_entity) {
			Ok(x) => x,
			Err(x) => {
				logger.message(cow!("Can't parse `{}` as certificate: {}", ee_path.display(), x));
				return Err(());
			}
		};
		//For selfsigned certificates, the issuer must be empty.
		if eecert.issuer.same_as(eecert.subject) && issuer.len() != 0 {
			logger.message(cow!("Issuer chain `{}` not empty for self-signed certificate",
				issuer_path.display()));
			return Err(());
		}
		//Contcatenate the EE cert and issuer chain.
		let mut certdata = Vec::new();
		certdata.extend_from_slice(&end_entity);
		certdata.extend_from_slice(&issuer);
		let mut certdata: &[u8] = &certdata;
		//There &mut &[u8] is a Read, so &mut &mut &[u8] is a &mut Read.
		let handle = match inner.add(&mut &mut certdata, &format!("{}", path.display())) {
			Ok(x) => x,
			Err(_) => return Err(())	//Call already prints an error.
		};
		let mut certinfo = CertificateInfo {
			handle: handle,
			ocsp_timestamp: None,
			scts_seen: BTreeMap::new(),
			ocsp_loaded: false,
		};
		//On the first round, do update in order to load OCSP and SCTs.
		certinfo.update(path, logger);
		Ok(certinfo)
	}
	fn process_sct(&mut self, sctpath: &Path)
	{
		let fname = match sctpath.file_name() {
			Some(x) => x,
			None => return		//Invalid filename.
		};
		if self.scts_seen.contains_key(fname) { return; }	//Seen this one.
		self.scts_seen.insert(fname.to_owned(), ());
		//This already prints errors.
		let _ = self.handle.add_sct_staple_file(sctpath);
	}
	fn update(&mut self, path: &Path, logger: Logger)
	{
		let mut seen_ocsp = false;
		let mut ocsp_timestamp = None;
		let mut itr = match read_dir(path) {
			Ok(x) => x,
			Err(x) => {
				logger.message(cow!("Can't read directory `{}`: {}", path.display(), x));
				return;
			}
		};
		while let Some(dir_entry) = itr.next() {
			let fpath = match dir_entry {
				Ok(x) => x.path(),
				Err(x) => {
					logger.message(cow!("Can't read directory `{}`: {}", path.display(), x));
					return;
				}
			};
			//We are interested about file called 'ocsp'.
			if fpath.file_name().and_then(|x|x.to_str()) == Some("ocsp") {
				//This is OCSP staple.
				seen_ocsp = true;
				//Get metadata on OCSP.
				let ocsp_metadata = match fpath.metadata() {
					Ok(x) => x,
					Err(x) => {
						logger.message(cow!("Can't get metadata for `{}`: {}",
							fpath.display(), x));
						return;
					}
				};
				let ocsp_modify = match ocsp_metadata.modified() {
					Ok(x) => Some(match x.duration_since(UNIX_EPOCH) {
						Ok(y) => y.as_secs() as i64,
						Err(y) => -(y.duration().as_secs() as i64)
					}),
					Err(_) => None
				};
				//ocsp_modify == None means unknown. We reload in that case.
				if self.ocsp_timestamp == ocsp_modify && ocsp_modify.is_some() {
					continue;
				}
				ocsp_timestamp = ocsp_modify;
				//This already logs error if any.
				match self.handle.set_ocsp_staple_file(&fpath) {
					Ok(_) => self.ocsp_loaded = true,
					Err(_) => ()
				};
			}
			//We are interested in '.sct' files.
			match fpath.extension() {
				Some(x) => if x.to_str() == Some("sct") { self.process_sct(&fpath); },
				None => (),
			};
		}
		if !seen_ocsp && self.ocsp_loaded {
			//No OCSP response, clear it. This already prints errors.
			let _ = self.handle.clear_ocsp_staple();
			self.ocsp_loaded = false;
		}
		self.ocsp_timestamp = ocsp_timestamp;
	}
}

fn join_links(src: &Path, dest: &Path) -> PathBuf
{
	if dest.is_absolute() { return dest.to_path_buf(); }
	let src_m1 = match src.parent() {
		Some(x) => x,
		None => return dest.to_path_buf()	//Hope for the best...
	};
	src_m1.join(dest)
}

struct DirectoryManager
{
	old_seen: BTreeMap<PathBuf, PathBuf>,
	logs_seen: BTreeMap<PathBuf, ()>,
	cert_handles: BTreeMap<PathBuf, CertificateInfo>,
	directory: PathBuf,
	store: LocalServerCertificateStore,
	logger: Logger,
}

impl DirectoryManager
{
	fn new(directory: &Path, mut inner: LocalServerCertificateStore) ->
		Result<DirectoryManager, Cow<'static, str>>
	{
		let logger = inner.get_logger();
		let mut keys_loaded = 0usize;
		let directory = match directory.canonicalize() {
			Ok(x) => x,
			Err(x) => fail!(cow!("Can't canonicalize directory path: {}", x))
		};
		let mut itr = match read_dir(&directory) {
			Ok(x) => x,
			Err(x) => fail!(cow!("Can't read directory: {}", x))
		};
		while let Some(dir_entry) = itr.next() {
			let fpath = match dir_entry {
				Ok(x) => x.path(),
				Err(x) => fail!(cow!("Can't read directory: {}", x))
			};

			//We are only interested in '.key' files (and ctlog files).
			match fpath.extension() {
				Some(x) => if x.to_str() != Some("key") { continue; },
				None => continue
			};
			//Okay, try to load this.
			let fstem = match fpath.file_stem() {
				Some(x) => x,
				None => continue	//Can't happen: Invalid filename.
			};
			let keyname = fstem.to_string_lossy();
			let key = match LocalKeyPair::new_file(&fpath) {
				Ok(x) => x,
				Err(x) => {
					logger.message(cow!("{}", x));	//Message already has prefix.
					continue;
				}
			};
			match inner.add_privkey(key, keyname.deref()) {
				Ok(_) => (),
				Err(x) => {
					logger.message(cow!("{}", x));	//Message already has prefix.
					continue;
				}
			};
			//Okay, loaded a key. The keys take memory, so this is enough not to overflow.
			keys_loaded += 1;
		}
		if keys_loaded == 0 {
			fail!(cow!("No private keys found or could be loaded"));
		}
		let mut  mgr = DirectoryManager {
			old_seen: BTreeMap::new(),
			cert_handles: BTreeMap::new(),
			logs_seen: BTreeMap::new(),
			directory: directory,
			store: inner,
			logger: logger,
		};
		//The first pass on certificates is the same as update.
		mgr.update();
		Ok(mgr)
	}
	fn get_path(&self) -> PathBuf
	{
		self.directory.clone()
	}
	fn scan_files_into_array(&self) -> Result<(BTreeMap<PathBuf, PathBuf>, Vec<PathBuf>), ()>
	{
		let mut seen = BTreeMap::<PathBuf, PathBuf>::new();
		let mut newlogs = Vec::new();
		let logger = self.store.get_logger();
		let mut itr = match read_dir(&self.directory) {
			Ok(x) => x,
			Err(x) => {
				logger.message(cow!("Can't read directory: {}", x));
				return Err(());	//This is too unreliable to act upon.
			}
		};
		while let Some(dir_entry) = itr.next() {
			let fpath = match dir_entry {
				Ok(x) => x.path(),
				Err(x) => {
					logger.message(cow!("Can't read directory: {}", x));
					return Err(());	//This is too unreliable to act upon.
				}
			};
			//We are only interested in '.crt' and 'ctlog' files.
			match fpath.extension() {
				Some(x) => {
					if x.to_str() == Some("ctlog") && !self.logs_seen.contains_key(&fpath) {
						//Okay, this is a new log, load it.
						newlogs.push(fpath.to_path_buf());
					}
					if x.to_str() != Some("crt") { continue; }
				},
				None => continue	//Just ignore this.
			};
			//Okay, try to load the target.
			let slinktype = match fpath.symlink_metadata() {
				Ok(x) => x.file_type(),
				Err(x) => {
					logger.message(cow!("Can't read (symlink) metadata for `{}`: {}",
						fpath.display(), x));
					continue;
				}
			};
			let target = if slinktype.is_symlink() {
				//This is a symbolic link.
				match fpath.read_link() {
					Ok(x) => join_links(&fpath, &x),
					Err(x) => {
						logger.message(cow!("Can't read symlink target for `{}`: {}",
							fpath.display(), x));
						continue;
					}
				}
			} else if slinktype.is_file() {
				//This is a regular file.
				let mut fp = match File::open(&fpath) {
					Ok(x) => x,
					Err(x) => {
						logger.message(cow!("Can't open file `{}`: {}", fpath.display(),
							x));
						continue;
					}
				};
				let mut content = String::new();
				match fp.read_to_string(&mut content) {
					Ok(_) => (),
					Err(x) => {
						logger.message(cow!("Can't read file `{}`: {}", fpath.display(),
							x));
						continue;
					}
				};
				if content.len() == 0 {
					logger.message(cow!("File `{}` is not valid reference: File is empty",
						fpath.display()));
					continue;
				}
				let mut dclone = self.directory.clone();
				dclone.push(&content);
				dclone
			} else {
				//Other, error.
				logger.message(cow!("'{}' is not a symbolic link nor regular file",
					fpath.display()));
				continue;
			};
			//Ok, now the target is in variable target. Check it is a directory.
			match target.metadata() {
				Ok(x) => if x.file_type().is_dir() {
						//Record this.
						seen.insert(fpath, target);
						continue;
					} else {
						logger.message(cow!("`{}` (target of `{}`) is not a directory",
							target.display(), fpath.display()));
						continue;
				},
				Err(x) => {
					logger.message(cow!("Can't read metadata for `{}` (target of `{}`): {}",
						target.display(), fpath.display(), x));
					continue;
				}
			}
		}
		Ok((seen, newlogs))
	}
	fn call_with_pair(name: &Path, old: Option<&Path>, new: Option<&Path>, logger: Logger,
		inner: LocalServerCertificateStore, cert_handles: &mut BTreeMap<PathBuf, CertificateInfo>) -> bool
	{
		match (old, new) {
			(Some(_old), Some(_new)) => {
				if _old == _new {
					//Reread entry.
					match cert_handles.get_mut(name) {
						Some(x) => x.update(_new, logger.clone()),
						None => return true	//The entry is still there.
					};
				} else {
					//Full reload of entry.
					let old_handle = cert_handles.get(name).map(|x|x.handle.clone());
					let new_handle = match CertificateInfo::new(_new, logger.clone(),
						inner.clone()) {
						Ok(x) => x,
						Err(_) => {
							//The old entry is still deleted.
							if let Some(h) = old_handle { h.delete(); }
							cert_handles.remove(name);
							return false;
						}
					};
					//Swap the entry.
					match cert_handles.get_mut(name) {
						Some(x) => *x = new_handle,
						None => {
							new_handle.handle.delete();
							return false;	//This should have existed!
						}
					};
					if let Some(h) = old_handle { h.delete(); }
				}
			},
			(Some(_old), None) => {
				//Entry got deleted.
				match cert_handles.remove(name) {
					Some(x) => x.handle.delete(),
					None => return true	//Should not happen. False is only for new cert
								//failures.
				};
			},
			(None, Some(_new)) => {
				//New entry got created.
				match CertificateInfo::new(_new, logger.clone(), inner.clone()) {
					Ok(x) => cert_handles.insert(name.to_path_buf(), x),
					Err(_) => return false
				};
			},
			(None, None) => {
				//Should not happen.
			}
		}
		return true;
	}
	fn process_pairs(&mut self, seen: &BTreeMap<PathBuf, PathBuf>, logger: Logger) -> BTreeMap<PathBuf, ()>
	{
		let mut to_delete = BTreeMap::<PathBuf, ()>::new();
		let mut itr_old = self.old_seen.iter();
		let mut itr_new = seen.iter();
		let mut entry_old = itr_old.next();
		let mut entry_new = itr_new.next();
		loop {
			//The step flags are:
			//Bit 0: Old entry should be stepped.
			//Bit 1: New entry should be stepped.
			//Bit 2: If set with bit 1, the new entry should be deleted.
			let step = if let &(Some((ref eold, ref trgold)), Some((ref enew, ref trgnew))) =
				&(entry_old, entry_new) {
				//Both old and new lists still have entries.
				match eold.cmp(enew) {
					//<. This means that new is ahead of old. We should process this as
					//deletion, since there is no replacement.
					Ordering::Less => { Self::call_with_pair(eold, Some(trgold), None,
						logger.clone(), self.store.clone(), &mut self.cert_handles); 1u32 },
					//=. This means new and old are equal. We should process this as update.
					//If entry should not exist, then delete it.
					Ordering::Equal => if Self::call_with_pair(eold, Some(trgold), Some(trgnew),
						logger.clone(), self.store.clone(), &mut self.cert_handles) { 3u32 }
						else { 7u32 },
					//>. This means new is behind old. We should process this as add.
					//If entry should not exist, then delete it.
					Ordering::Greater => if	Self::call_with_pair(eold, None, Some(trgnew),
						logger.clone(), self.store.clone(), &mut self.cert_handles) { 2u32 }
						else { 6u32 },
				}
			} else if let &Some((ref eold, ref trgold)) = &entry_old {
				//Only the old list still has entries. We should process this as deletion, as
				//there can be no replacement.
				Self::call_with_pair(eold, Some(trgold), None, logger.clone(), self.store.clone(),
					&mut self.cert_handles);
				1u32
			} else if let &Some((ref eold, ref trgnew)) = &entry_new {
				//Only the new list still has entries. We should process this as addition, as
				//there can be no old entry. If entry should not exist, then delete it.
				if Self::call_with_pair(eold, None, Some(trgnew), logger.clone(), self.store.clone(),
					&mut self.cert_handles) { 2u32 } else { 6u32 }
			} else {
				break;					//All entries processed.
			};
			//This is only valid if new entry is valid.
			if let (6, &Some((ref wname, _))) = (step & 6, &entry_new) {
				to_delete.insert(wname.to_path_buf(), ());
			}
			if (step & 1) != 0 { entry_old = itr_old.next(); }
			if (step & 2) != 0 { entry_new = itr_new.next(); }
		}
		to_delete
	}
	fn process_log(&mut self, name: &Path)
	{
		let mut filehandle = match File::open(name) {
			Ok(x) => x,
			Err(x) => return self.logger.message(cow!("Can't add CT log file `{}`: Can't open: {}",
				name.display(), x))
		};
		let mut content = String::new();
		match filehandle.read_to_string(&mut content) {
			Ok(_) => (),
			Err(x) => return self.logger.message(cow!("Can't add CT log file `{}`: Can't read: {}",
				name.display(), x))
		};
		let mut parts = content.split('\n').collect::<Vec<_>>();
		while parts.len() > 0 && parts[parts.len()-1] == "" { parts.pop(); }
		let (lname, b64key, v2info) = if parts.len() == 2 {
			//v1 log.
			(parts[0].to_owned(), parts[1], "")
		} else if parts.len() == 3 {
			//v2 log.
			(parts[0].to_owned(), parts[1], parts[2])
		} else {
			return self.logger.message(cow!("Can't add CT log file `{}`: Unrecognized format",
				name.display()));
		};
		//base64 decode the key.
		let mut key = Vec::new();
		let mut decoder = Base64Decoder::new();
		match decoder.data(b64key, &mut key).and_then(|_|decoder.end(&mut key)) {
			Ok(_) => (),
			Err(x) => return self.logger.message(cow!("Can't add CT log file `{}`: Can't decode \
				public key: {}", name.display(), x))
		}
		let (v2_id, v2_hash) = if v2info != "" {
			//Decode v2_id and v2_hash
			let v2parts = v2info.split("/").collect::<Vec<_>>();
			if v2parts.len() != 2 {
				return self.logger.message(cow!("Can't add CT log file `{}`: Unrecognized format",
					name.display()));
			}
			let hash = if v2parts[1] == "sha256" {
				LogHashFunction::Sha256
			} else {
				return self.logger.message(cow!("Can't add CT log file `{}`: Unrecognized hash `{}`",
					name.display(), v2parts[1]));
			};
			let error = format!("Can't add CT log file `{}`: Unrecognized OID `{}`", name.display(),
				v2parts[0]);
			let mut oid = Vec::new();
			let _oidparts = v2parts[0].split(".").collect::<Vec<_>>();
			let mut oidparts = Vec::new();
			for i in _oidparts.iter() {
				oidparts.push(match u64::from_str(i) {
					Ok(x) => x,
					Err(_) => return self.logger.message(Cow::Owned(error))
				});
			}
			if oidparts.len() < 2 || oidparts[0] > 2 || (oidparts[0] < 2 && oidparts[1] > 39) {
				return self.logger.message(Cow::Owned(error));
			}
			//These are in range since oidparts.len() >= 2 by above.
			oidparts[0] = (40 * oidparts[0]).saturating_add(oidparts[1]);
			if oidparts[0].wrapping_add(1) == 0 { return self.logger.message(Cow::Owned(error)); }
			oidparts.remove(1);	//The first two components are collapsed to one.
			for i in oidparts.iter() {
				let val = *i;
				for j in 0..9 {
					if val >= 1u64 << (63 - 7 * j) {
						oid.push((val >> (63 - 7 * j)) as u8 | 128);
					}
				}
				oid.push(val as u8 & 127);
			}
			(oid, hash)
		} else {
			(Vec::new(), LogHashFunction::Sha256)
		};
		//Construct and add.
		let newlog = TrustedLog {
			name: lname,
			key: key,
			v2_id: v2_id,
			v2_hash: v2_hash,
			expiry: None,		//These logs always have no expiry
		};
		self.store.add_trusted_log(&newlog);	//This logs.
		self.logs_seen.insert(name.to_path_buf(), ());
	}
	fn update(&mut self)
	{
		let (mut seen, newlogs) = match self.scan_files_into_array() {
			Ok((x, y)) => (x, y),
			Err(_) => return	//Used to signal directory too unreliable to scan.
		};
		//Add all new logs found.
		for log in newlogs.iter() {
			self.process_log(&log);
		}
		let logger2 = self.logger.clone();
		let to_delete = self.process_pairs(&seen, logger2);
		//Remove certs that failed to load, so we will retry next iteration.
		for i in to_delete.iter() {
			seen.remove(i.0);
		}
		//Finally, make this seen the base for next iteration.
		swap(&mut seen, &mut self.old_seen);
	}
}

#[cfg(target_os = "linux")]
#[allow(unsafe_code)]
mod inotify
{
	use super::Logger;
	use libc::{c_char, c_int, poll, pollfd, POLLIN, read, O_CLOEXEC, O_NONBLOCK};
	use ::stdlib::{Cow, CString, Duration, IoError, IoErrorKind, IoResult, min, transmute};
	use ::system::{Path, sleep};
	use btls_aux_time::{TimeNow, TimeInterface};
	use ::std::os::unix::ffi::OsStrExt;
	pub struct InotifyObj
	{
		fd: i32,
		debug: Logger,
	}

	#[repr(C)]
	struct InotifyHeader
	{
		wd: c_int,
		mask: u32,
		cookie: u32,
		len: u32
	}

	extern {
		fn inotify_init1(flags: c_int) -> c_int;
		fn inotify_add_watch(fd: c_int, pathname: *const c_char, mask: u32) -> c_int;
	}

	impl InotifyObj
	{
		pub fn new<P:AsRef<Path>>(p: P, log: Logger) -> IoResult<InotifyObj>
		{
			let p = p.as_ref();
			let fd = match unsafe { inotify_init1(O_CLOEXEC | O_NONBLOCK) } {
				x if x < 0 => return Err(IoError::last_os_error()),
				x => x
			};
			let cpath = match CString::new(p.as_os_str().as_bytes()) {
				Ok(x) => x,
				Err(_) => return Err(IoError::new(IoErrorKind::Other, "Given path contains NUL"))
			};
			//We are interested in delete and moves.
			match unsafe { inotify_add_watch(fd, cpath.as_ptr(), 0x2C0) } {
				x if x < 0 => return Err(IoError::last_os_error()),
				_ => ()
			};
			log.message(cow!("Certificate directory inotify poller armed"));
			Ok(InotifyObj{
				fd: fd,
				debug: log
			})
		}
		pub fn wait(&mut self)
		{
			let clocknow = TimeNow.get_time();
			let timeout = clocknow + 600;
			let mut buf = [0;16384];
			let mut got_break = false;
			while !got_break {
				let left = TimeNow.get_time().delta(timeout) * 1000;
				if left <= 0 { return; }
				let mut pfd = [pollfd{
					fd: self.fd,
					events: POLLIN,
					revents: 0
				}];
				match unsafe {poll(pfd.as_mut_ptr(), 1, left as c_int)} {
					x if x < 0 => {
						let errmsg = IoError::last_os_error();
						self.debug.message(cow!("Error polling for inotify: {}", errmsg));
						sleep(Duration::from_millis(left as u64));
						return;
					},
					_ => (),
				};
				//If not readable, loop. In case of timeout, we break at start of loop.
				if pfd[0].revents & POLLIN == 0 { continue; }
				//We read all events, until WouldBlock.
				loop {
					let r = match unsafe { read(self.fd, buf.as_mut_ptr() as *mut _,
						buf.len()) } {
						x if x < 0 => {
							let errmsg = IoError::last_os_error();
							if errmsg.kind() == IoErrorKind::WouldBlock { break; }
							self.debug.message(cow!("Error reading for inotify: {}",
								errmsg));
							break;
						},
						x => x
					};
					//r is supposed to be smaller than buf.len().
					let nbuf = &buf[..min(r as usize, buf.len())];
					let mut idx = 0;
					while idx < nbuf.len() {
						let mut hdr = [0u8; 16];
						if nbuf.len() - idx < 16 {
							self.debug.message(cow!("Got malformed inotify record"));
							break;
						}
						hdr.copy_from_slice(&nbuf[idx..][..16]);
						let hdr: InotifyHeader = unsafe { transmute::<_,_>(hdr) };
						let nameidx = idx + 16;
						if nbuf.len() - nameidx < hdr.len as usize {
							self.debug.message(cow!("Got malformed inotify record"));
							break;
						}
						//Find first zero. Read is valid as nbuf at least nameidx + hdr.len.
						let mut zidx = 0;
						while zidx < hdr.len as usize && nbuf[nameidx + zidx] != 0 {
							zidx = zidx + 1;
						}
						if zidx == hdr.len as usize {
							self.debug.message(cow!("Got malformed inotify record"));
							break;
						}
						let name = &buf[nameidx..][..zidx];	//nameidx + zidx was read.
						//We are interested of events that are for .ctlog or .crt files,
						//and are moves or deletes.
						let is_ctlog = name.len() > 6 && &name[name.len()-6..] ==
							".ctlog".as_bytes();
						let is_crt = name.len() > 4 && &name[name.len()-4..] ==
							".crt".as_bytes();
						let is_signal = name == "rescan".as_bytes();
						if (is_ctlog || is_crt || is_signal) && hdr.mask & 0x2C0 != 0 {
							//Bingo.
							got_break = true;
						}
						idx = idx.saturating_add(16).saturating_add(hdr.len as usize);
					}
				}
			}
			self.debug.message(cow!("Rescanning certificate directory due to activity"));
		}
	}
}

#[cfg(not(target_os = "linux"))]
mod inotify
{
	use super::Logger;
	use ::stdlib::{Duration, IoResult};
	use ::system::{Path, sleep};
	pub struct InotifyObj;

	impl InotifyObj
	{
		pub fn new<P:AsRef<Path>>(_: P, _l: Logger) -> IoResult<InotifyObj>
		{
			Ok(InotifyObj)
		}
		pub fn wait(&mut self)
		{
			//Wait 5 min.
			sleep(Duration::from_millis(300000));
		}
	}
}

use self::inotify::InotifyObj;


///Certificate store loaded from a directory
///
///# Upon constructing the object:
///
/// * Files with name `*.key` are scanned. Found ones are passed to `LocalKeyPair` for loading. At least 1
///successfully loaded key must exist.
/// * Files with name `*.crt` are scanned. Those that are symbolic links that point to directory are loaded as
///an certificate.
///
///# Periodically, files with name `*.crt` are rescanned.
///
/// * If existing `*.crt` file disappears, the corresponding certificate is deleted.
/// * If new `*.crt` file that is symbolic link to directory appears, it is loaded.
/// * If existing `*.crt` file changes destination of symbolic link, the old certificate is unloaded and new one
///is loaded in its place.
/// * If `ocsp` file within certificate directory changes, the OCSP response is updated.
/// * If new `*.sct` file within certificate directory appears, it is loaded as SCT response.
///
/// The certificate directories have the following files:
///
/// * `certificate`: The certificate itself. Can only have one certificate. Required.
/// * `issuer`: The issuer certificate. Can have multiple certificates. Required.
/// * `ocsp`: OCSP staple response. Optional
/// * `*.sct`: SCT responses.
///
///# Periodically, files with name '*.ctlog' are rescanned.
///
/// * If a new file with '*.ctlog' extension appears, it is loaded as a trusted log.
/// * The file needs to contain 2 or 3 lines,
///   * First line is log name
///   * second being base64 (not base64url!) encoding of the SPKI-format log public key.
///   * Optional third line being `<oid>/<hash>` (only needed for v2-capable, a.k.a. 6962bis logs)
///     * `<oid>` is textual-form OID of the log.
///     * `<hash>` is the hash function, currently only `sha256` is supported.
///
///# Inotify support
///
///If btls is built with feature `inotify`, then rescan is automatically triggered if a `*.ctlog` or `*.crt`
///file is renamed or deleted. Creation or modification does not count: You are expected to only create or
///modify such files by renaming them (the standard rename-as-atomic-overwrite trick).
///
///# Notes:
///
/// * All symbolic links to directories in the same directory can be replaced by regular files holding just
///the name of directory.
/// * Due to race conditions, only replace symbolic links with symbolic links and regular file references with
///regular file references while this code is running.
///
///This struct implements trait `CertificateLookup`, so it can be used as certificate lookup.
///
///Yes, really: The `::new()` function is all there is to this class.
#[derive(Clone)]
pub struct CertificateDirectory
{
	inner: LocalServerCertificateStore,
	logger: Logger,
	manager: Arc<Mutex<DirectoryManager>>,
}

impl CertificateDirectory
{
	///Create a new store that loads certificates from specified directory.
	///
	///# Parameters:
	///
	/// * `path`: The path to directory to load.
	/// * `log`: The logger to use for certificate logging messages.
	pub fn new<L:Logging+Send+Sync+'static, P:AsRef<Path>>(path: P, log: L) ->
		Result<CertificateDirectory, Cow<'static, str>>
	{
		let inner = LocalServerCertificateStore::new(log);
		let logger = inner.get_logger();
		let directory = path.as_ref();
		let (ipath, state) = match DirectoryManager::new(directory, inner.clone()) {
			Ok(x) => {
				let ipath = x.get_path();
				(ipath, Arc::new(Mutex::new(x)))
			},
			Err(x) => {
				let errmsg: Cow<'static, str> = cow!("Can't load certificates from directory `{}`: \
					{}", directory.display(), x);
				logger.message(clone_cow(&errmsg));
				fail!(errmsg);
			}
		};
		let mut inotifyobj = match InotifyObj::new(&ipath, logger.clone()) {
			Ok(x) => x,
			Err(x) => {
				let errmsg: Cow<'static, str> = cow!("Can't create inotify for `{}`: {}",
					directory.display(), x);
				logger.message(clone_cow(&errmsg));
				fail!(errmsg);
			}
		};
		let state2 = state.clone();
		let logger2 = logger.clone();
		let b = Builder::new();
		let b = b.name(String::from("directory update thread"));
		match b.spawn(move || {		//Drop the handle to floor (and we can't report errors).
			let _state = Arc::downgrade(&state);
			drop(state);	//Dispose the strong reference.
			loop {
				let strongref = match _state.upgrade() {
					Some(x) => x,
					None => break	//The store is gone, exit loop.
				};
				match strongref.lock() {
					Ok(mut x) => x.update(),
					Err(_) => logger2.message(cow!("Internal error: Can't lock certificate \
						manager"))
				};
				inotifyobj.wait();
			}
		}) {
			Ok(_) => (),
			Err(x) => {
				let errmsg = cow!("Internal error: Can't start directory updater thread: {}", x);
				logger.message(clone_cow(&errmsg));
				fail!(errmsg);
			}
		}
		Ok(CertificateDirectory {
			inner: inner,
			logger: logger,
			manager: state2,
		})
	}
	///Enumerate names of available private keys, and their types
	pub fn enumerate_private_keys(&self) -> Vec<(String, &'static str)>
	{
		self.inner.enumerate_private_keys()
	}
	///Sign a CSR with private key.
	pub fn sign_csr(&self, privkey: &str, csr_params: &CsrParams) -> FutureReceiver<Result<Vec<u8>, ()>>
	{
		self.inner.sign_csr(privkey, csr_params)
	}
}

impl CertificateLookup for CertificateDirectory
{
	fn lookup(&self, criteria: &CertificateLookupCriteria) ->
		FutureReceiver<Result<Box<CertificateSigner+Send>, ()>>
	{
		self.inner.lookup(criteria)
	}
	fn clone_self(&self) -> Box<CertificateLookup+Send>
	{
		Box::new(self.clone())
	}
}


impl KeyStoreOperations for CertificateDirectory
{
	fn enumerate_private_keys(&self) -> Vec<(String, &'static str)>
	{
		self.inner.enumerate_private_keys()
	}
	fn get_public_key(&self, privkey: &str) -> Option<SubjectPublicKeyInfo>
	{
		self.inner.get_public_key(privkey)
	}
	fn sign_csr(&self, privkey: &str, csr_params: &CsrParams) -> FutureReceiver<Result<Vec<u8>, ()>>
	{
		self.inner.sign_csr(privkey, csr_params)
	}
}
