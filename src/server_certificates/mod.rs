//!Types related to server certificate store.
//!
//!This module contains the various types related to server certificate store. This includes:
//!
//! * The base trait for certificate selection: `CertificateLookup`.
//! * The base trait for signable certificate: `CertificateSigner`.
//! * Store of signable certificates loaded from files: `LocalServerCertificateStore`.
//! * Store of signable certificates loaded from a directory (with automatic managment): `CertificateDirectory`.
//!
//!It also contains the certificate-related types: `Certificate`, `CertificateChain`, `OcspResponse`, `StapledSct`
//!and `StapledSctList`, plus `SIGALGO_*` constants.

use ::stdlib::{Arc, Box, String, Vec};

use btls_aux_futures::FutureReceiver;
use btls_aux_keypair::{CsrParams, SubjectPublicKeyInfo};
pub use btls_aux_signature_algo::{SIG_ECDSA_SHA256, SIG_ECDSA_SHA384, SIG_ECDSA_SHA512, SIG_ED25519, SIG_ED448,
	SIG_RSA_PKCS1_SHA256, SIG_RSA_PKCS1_SHA384, SIG_RSA_PKCS1_SHA512, SIG_RSA_PSS_SHA256, SIG_RSA_PSS_SHA384,
	SIG_RSA_PSS_SHA512, SIGALGO_RSA_PKCS1_SHA256, SIGALGO_RSA_PKCS1_SHA384, SIGALGO_RSA_PKCS1_SHA512,
	SIGALGO_RSA_PSS_SHA256, SIGALGO_RSA_PSS_SHA384, SIGALGO_RSA_PSS_SHA512, SIGALGO_ECDSA_SHA256,
	SIGALGO_ECDSA_SHA384, SIGALGO_ECDSA_SHA512, SIGALGO_ED25519, SIGALGO_ED448, SIGALGO_RSA_MASK,
	SIGALGO_ECDSA_MASK, SIGALGO_TLS13_MASK};

mod local;
pub use self::local::{LocalServerCertificateHandle, LocalServerCertificateStore, convert_chain_to_der};
mod directory;
pub use self::directory::CertificateDirectory;

///A single certificate
#[derive(Clone)]
pub struct Certificate(pub Arc<Vec<u8>>);
///A certificate chain.
#[derive(Clone)]
pub struct CertificateChain(pub Arc<Vec<Certificate>>);
///A single OCSP response.
#[derive(Clone)]
pub struct OcspResponse(pub Option<Arc<Vec<u8>>>);
///A single stapled SCT.
#[derive(Clone)]
pub struct StapledSct(pub Arc<Vec<u8>>);
///A list of stapled SCTs
#[derive(Clone)]
pub struct StapledSctList(pub Option<Arc<Vec<StapledSct>>>);




///A certificate chain, plus signer for the associated private key.
///
///This trait is for objects that represent the results of certificate selection. It provodes signing
///capability with certificate private key and the certificate info (along with assoicated info).
///
///You don't have to understand the internals of this trait unless you are changing the way
///certificate lookup or certificate signing works.
pub trait CertificateSigner
{
	///Sign a message.
	///
	///# Parameters:
	///
	/// * `self`: The signer.
	/// * `data`: The raw data to sign.
	///
	///# Returns:
	///
	/// * A future for:
	///   * The TLS SignatureScheme value.
	///   * The actual signature.
	///   * Or on failure, `()`.
	///
	///# Notes:
	///
	/// * Because signing can involve contacting an external signer, the return value is a future
	///   and not a direct value. If the future has not settled, the handshake will be paused
	///   until it is.
	fn sign(&self, data: &[u8]) -> FutureReceiver<Result<(u16, Vec<u8>), ()>>;
	///Is ECDSA (also EdDSA)?
	///
	///# Parameters:
	///
	/// * `self`: The signer.
	///
	///# Returns:
	///
	/// * `true` if signature scheme is ECDSA or EdDSA, false otherwise.
	fn is_ecdsa(&self) -> bool;
	///Get the certificate chain.
	///
	///# Parameters:
	///
	/// * `self`: The signer.
	///
	///# Returns:
	///
	/// * The associated certificate chain.
	fn get_certificate(&self) -> CertificateChain;
	///Get OCSP staple for certificate.
	///
	///# Parameters:
	///
	/// * `self`: The signer.
	///
	///# Returns:
	///
	/// * The associated OCSP stapling response. Empty if none.
	fn get_ocsp(&self) -> OcspResponse
	{
		//Disable OCSP stapling by default.
		dummy_ocsp_response()
	}
	///Get SCT staples for certificate
	///
	///# Parameters:
	///
	/// * `self`: The signer.
	///
	///# Returns:
	///
	/// * The associated SCT stapling response.
	///
	///# Notes:
	///
	/// * If list of SCTs is empty, no SCT staple gets sent.
	fn get_scts(&self) -> StapledSctList
	{
		//Disable CT by default.
		dummy_sct_response()
	}
}

///Lookup criteria for certificates.
pub struct CertificateLookupCriteria<'a>
{
	///The SNI name, or None if client sent none and there is no default.
	pub sni: Option<&'a str>,
	///Bitwise OR of signature algorithms (SIGALGO_*) usable for CA signatures.
	pub sig_flags: u64,
	///Bitwise OR of signature algorithms (SIGALGO_*) usable for server key signatures.
	pub ee_flags: u64,
	///True if TLS 1.3 is in use.
	pub tls13: bool,
	///Selfsigned certificates are accepted.
	pub selfsigned_ok: bool,
	///Not used.
	pub __dummy: (),
}

///Key storage operations.
///
///This trait contains operations on key stores.
pub trait KeyStoreOperations
{
	///Enumerate names of available private keys, and their types
	///
	///# Parameters:
	///
	/// * `self`: The key container.
	///
	///# Returns:
	///
	/// * A list of pairs, each pair contains:
	///   * The first element: Name of the key.
	///   * The second element: Type of the key.
	fn enumerate_private_keys(&self) -> Vec<(String, &'static str)>;
	///Get public key.
	///
	///
	///# Parameters:
	///
	/// * `self`: The key container.
	/// * `privkey`: Name of the private key.
	///
	///# Returns:
	///
	/// * If specified private key is found in container, `Some(x)`, where `x` is the public key.
	/// * If specified private key was not found, `None`.
	fn get_public_key(&self, privkey: &str) -> Option<SubjectPublicKeyInfo>;
	///Sign a CSR with private key.
	///
	///# Parameters:
	///
	/// * `self`: The key container.
	/// * `privkey`: Name of the private key.
	/// * `csr_params`: The parameters to write into CSR.
	///
	///# Returns:
	///
	/// * A future resolving into the CSR, or `Err()` if error occured signing the CSR.
	fn sign_csr(&self, privkey: &str, csr_params: &CsrParams) -> FutureReceiver<Result<Vec<u8>, ()>>;
}

///Certificate lookup.
///
///This trait is for objects that perform certificate lookup (select the certificate used).
///
///The implementations of this trait provoded by the library are:
///
/// * [`LocalServerCertificateStore`]: Load key and certificate files and use those.
///
///You don't have to understand the internals of this trait unless you are changing the way
///certificate lookup or certificate signing works.
///
///[`LocalServerCertificateStore`]: struct.LocalServerCertificateStore.html
pub trait CertificateLookup
{
	///Look up a certificate.
	///
	///# Parameters:
	///
	/// * `self`: The certificate lookup.
	/// * `criteria': The criteria to use in selecting certificate.
	///
	///# Returns:
	///
	/// * Future with:
	///   * Certificate signer.
	///   * Or on failure, `()`.
	///
	///# Notes:
	///
	/// * Because certificate selection can involve contacting an external signer, the return
	///   value is a future and not a direct value. If the future has not settled, the
	///   handshake will be paused until it is.
	fn lookup(&self, conditions: &CertificateLookupCriteria) ->
		FutureReceiver<Result<Box<CertificateSigner+Send>, ()>>;
	///Clone this object.
	///
	///# Parameters:
	///
	/// * `self`: The certificate lookup.
	///
	///# Returns:
	///
	/// * Clone of the lookup.
	///
	///# Notes:
	///
	/// * The reason this exists instead of just requiring Clone is that the latter is not
	///   possible if CertificateLookup is to be object-safe.
	fn clone_self(&self) -> Box<CertificateLookup+Send>;
}


fn dummy_ocsp_response() -> OcspResponse
{
	OcspResponse(None)
}

fn dummy_sct_response() -> StapledSctList
{
	StapledSctList(None)
}
