//!Types related to logging
//!
//!This module contains the following types:
//!
//! * The base trait for logging: `Logging`.
//! * Null log implementation that just discards all messages: `NullLog`.
//! * Log implementation that just prints the messages to stderr: `StderrLog`.
//!
//!Additionally, this module contains the `DEBUG_*` constants.

use ::stdlib::{Cow, from_u32, IoWrite, String};
use ::system::stderr;

///Null Log.
///
///This is a null implementation of [`Logging`] trait. The messages are simply just discarded.
///
///[`Logging`]: trait.Logging.html
pub struct NullLog;

impl Logging for NullLog
{
	fn message(&self, _x: Cow<'static, str>)
	{
		//Do nothing.
	}
}

///Stderr Log.
///
///This is a simple implementation of [`Logging`] trait. The messages are simply printed to stderr.
///
///[`Logging`]: trait.Logging.html
pub struct StderrLog;

impl Logging for StderrLog
{
	fn message(&self, x: Cow<'static, str>)
	{
		//Just discard errors.
		let _ = writeln!(stderr(), "{}", x);
	}
}

///Debugging: Log handshake messages.
pub const DEBUG_HANDSHAKE_MSGS: u64 = 1;
///Debugging: Log Aborts.
pub const DEBUG_ABORTS: u64 = 2;
///Debugging: Log crypto calculations.
pub const DEBUG_CRYPTO_CALCS: u64 = 4;
///Debugging: Log Handshake message contents.
pub const DEBUG_HANDSHAKE_DATA: u64 = 8;
///Debugging: Log Handshake events.
pub const DEBUG_HANDSHAKE_EVENTS: u64 = 16;
///Debugging: Log TLS extensions.
pub const DEBUG_TLS_EXTENSIONS: u64 = 32;

#[doc(hidden)]
#[cfg(feature = "enable-key-debug")]
pub const DEBUG_MASK: u64 = 0x3f;
#[doc(hidden)]
#[cfg(not(feature = "enable-key-debug"))]
pub const DEBUG_MASK: u64 = 0x3b;	//DEBUG_CRYPTO_CALCS is disabled.


///Logging callback.
///
///This trait models an logging callback
pub trait Logging
{
	///Log a message.
	///
	///# Parameters:
	///
	/// * `msg`: The message to log.
	fn message(&self, msg: Cow<'static, str>);
}

///Output string as hexes.
pub fn bytes_as_hex(x: &[u8]) -> String
{
	let mut out = String::new();
	const HEXES: [char;16] = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'];
	for i in x.iter() {
		out.push(HEXES.get((i >> 4) as usize).map(|x|*x).unwrap_or('?'));
		out.push(HEXES.get((i & 15) as usize).map(|x|*x).unwrap_or('?'));
	}
	out
}

fn push_sepline(target: &mut String, lenlen: usize)
{
	target.push('+');
	for _ in 0..lenlen { target.push('-'); }	//Space for offset.
	target.push('+');
	for _ in 0..48 { target.push('-'); }		//Hexes.
	target.push('+');
	for _ in 0..16 { target.push('-'); }		//ASCII bytes.
	target.push('+');
}

///Output string as hexes block.
pub fn bytes_as_hex_block(x: &[u8], lpfx: &str) -> String
{
	//Calculate length of length.
	let lenlen = if x.len() < 16 {
		0
	} else {
		let mut l = x.len().saturating_sub(1);
		let mut lenlen = 1;
		while l > 15 {
			lenlen = lenlen + 1;
			l = l / 16;
		}
		lenlen
	};

	const HEXES: [char;16] = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'];
	let mut y = String::new();
	for i in lpfx.chars() { y.push(i); }
	push_sepline(&mut y, lenlen);
	y.push('\n');
	for i in lpfx.chars() { y.push(i); }
	for i in 0..(x.len() + 15) / 16 {
		if i != 0 {
			y.push('\n');
			for i in lpfx.chars() { y.push(i); }
		}
		y.push('|');
		for j in 0..lenlen {
			let offset = i << 4;
			let hnum = lenlen - 1 - j;
			y.push(HEXES.get(((offset >> (hnum << 2)) & 15) as usize).map(|x|*x).unwrap_or('?'));
		}
		y.push('|');
		for j in 0..16 {
			let off = i << 4 | j;
			match x.get(off) {
				Some(z) => {
					y.push(HEXES.get((*z >> 4) as usize).map(|x|*x).unwrap_or('?'));
					y.push(HEXES.get((*z & 15) as usize).map(|x|*x).unwrap_or('?'));
				},
				None => {
					y.push(' ');
					y.push(' ');
				}
			}
			y.push(' ');
		}
		y.push('|');
		for j in 0..16 {
			let off = i << 4 | j;
			match x.get(off) {
				Some(z) => {
					if *z >= 32 && *z <= 126 {
						y.push(from_u32(*z as u32).unwrap_or('.'));
					} else {
						y.push('.');
					}
				},
				None => {
					y.push(' ');
				}
			}
		}
		y.push('|');
	}
	if x.len() != 0 {
		y.push('\n');
		for i in lpfx.chars() { y.push(i); }
	}
	push_sepline(&mut y, lenlen);
	y
}

///Print human-readable name of handshake type
pub fn explain_hs_type(htype: u8) -> Cow<'static, str>
{
	match htype {
		0 => cow!("hello_request"),
		1 => cow!("client_hello"),
		2 => cow!("server_hello"),
		3 => cow!("hello_verify_request"),
		4 => cow!("new_session_ticket"),
		6 => cow!("hello_retry_request"),
		8 => cow!("encrypted_extensions"),
		11 => cow!("certificate"),
		12 => cow!("server_key_exchange"),
		13 => cow!("certificate_request"),
		14 => cow!("server_hello_done"),
		15 => cow!("certificate_verify"),
		16 => cow!("client_key_exchange"),
		20 => cow!("finished"),
		21 => cow!("certificate_url"),
		22 => cow!("certificate_status"),
		23 => cow!("supplemental_data"),
		24 => cow!("key_update"),
		x => cow!("<unknown #{}>", x),
	}
}
