//This module is internal.
#![allow(missing_docs)]
pub use core::char::from_u32;
pub use core::cmp::{Eq, max, min, Ord, Ordering, PartialEq, PartialOrd};
pub use core::convert::{AsRef, From};
pub use core::default::Default;
pub use core::fmt::{Debug, Display, Error as FmtError, Formatter, Result as FmtResult};
pub use core::hash::{Hash, Hasher};
pub use core::iter::FromIterator;
pub use core::marker::PhantomData;
pub use core::mem::{size_of, swap, transmute, zeroed};
pub use core::ops::{Add, Deref, DerefMut, Range, RangeFrom, RangeFull, RangeTo};
pub use core::ptr::{null, write_volatile};
pub use core::slice::{from_raw_parts, Iter as SliceIter};
pub use core::str::{from_utf8, FromStr};
pub use core::sync::atomic::{AtomicBool, fence, Ordering as AtomicOrdering};


pub use super::std::borrow::{Cow, ToOwned};
pub use super::std::boxed::Box;
pub use super::std::collections::BTreeMap;
pub use super::std::error::Error;
pub use super::std::ffi::{CStr, CString, OsStr, OsString};
pub use super::std::io::{Error as IoError, ErrorKind as IoErrorKind, Read as IoRead, Result as IoResult,
	Write as IoWrite};
pub use super::std::string::String;
pub use super::std::sync::{Arc, Mutex, Once, ONCE_INIT, RwLock, Weak};
pub use super::std::time::{Duration, UNIX_EPOCH};
pub use super::std::vec::Vec;

macro_rules! cow
{
	($x:expr) => { Cow::Borrowed($x) };
	($x:expr, $($p:tt)*) => { Cow::Owned(format!($x, $($p)*)) };
}
