use ::stdlib::{Display, FmtError, Formatter, min, Vec};

pub struct Queue
{
	//The queue backing data.
	backing: Vec<u8>,
	//The queue read pointer.
	read: usize,
	//The queue write pointer.
	write: usize,
	//EOF written.
	eof_written: bool,
}

///Error from queue read operation.
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub enum QueueReadError
{
	///Queue has been EOF'd.
	Eof,
	///Amount to discard too big.
	NotEnoughData(usize),
	#[doc(hidden)]
	Hidden__
}

impl Display for QueueReadError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::QueueReadError::*;
		match self {
			&Eof => fmt.write_str("Buffer already at EOF"),
			&NotEnoughData(x) => fmt.write_fmt(format_args!("Amount to discard too big (remain={})",
				x)),
			&Hidden__ => fmt.write_str("Hidden__"),
		}
	}
}

///Error from queue write operation.
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum QueueWriteError
{
	///Queue has been EOF'd.
	Eof,
	///Queue would become too big.
	TooBig,
	#[doc(hidden)]
	Hidden__
}

impl Display for QueueWriteError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::QueueWriteError::*;
		match self {
			&Eof => fmt.write_str("Queue already EOF'd"),
			&TooBig => fmt.write_str("Queue would become too big"),
			&Hidden__ => fmt.write_str("Hidden__"),
		}
	}
}

fn round_up_to_power_of_2(x: usize) -> usize
{
	let mut out = 1;
	while out < x {
		out = out << 1;
		if out == 0 { out = out.wrapping_sub(1); }
	}
	out
}

impl Queue
{
	//Create a new queue.
	pub fn new() -> Queue
	{
		Queue{
			backing: Vec::new(),
			read: 0,
			write: 0,
			eof_written: false
		}
	}
	//Read from queue. If EOF, returns None, otherwise size read (lesser of available data and buffer size).
	pub fn read(&mut self, buf: &mut [u8]) -> Option<usize>
	{
		let amt = match self.peek(buf) { Some(x) => x, None => return None };
		match self.discard(amt) { Ok(_) => Some(amt), Err(_) => None }
	}
	//Similar as read, but don't remove data.
	pub fn peek(&self, buf: &mut [u8]) -> Option<usize>
	{
		if self.eof_written && self.read == self.write { return None; }
		if self.read <= self.write {
			//The buffer is in one segment. Read and write are valid indices.
			let data = &self.backing[self.read..self.write];
			let maxc = min(data.len(), buf.len());
			(&mut buf[..maxc]).copy_from_slice(&data[..maxc]);
			Some(maxc)
		} else {
			//The buffer is in two segments. Read and write are valid indices.
			let data1 = &self.backing[self.read..];
			let data2 = &self.backing[..self.write];
			let maxc1 = min(data1.len(), buf.len());
			let maxc2 = min(data2.len(), buf.len().saturating_sub(maxc1));
			(&mut buf[..maxc1]).copy_from_slice(&data1[..maxc1]);
			(&mut buf[maxc1..][..maxc2]).copy_from_slice(&data2[..maxc2]);
			//At most buf.len() by above.
			Some(maxc1 + maxc2)
		}
	}
	//Discard data without storing it anywhere.
	pub fn discard(&mut self, amount: usize) -> Result<(), QueueReadError>
	{
		use self::QueueReadError::*;
		fail_if!(self.eof_written && self.read == self.write, Eof);
		if self.read <= self.write {
			fail_if!(self.write - self.read < amount, NotEnoughData(amount));
			self.read += amount;
		} else {
			//self.read < self.backing.len(),
			let mut amount = amount;
			fail_if!(self.backing.len() - self.read + self.write < amount, NotEnoughData(amount));
			if self.backing.len() - self.read > amount {
				self.read += amount;
			} else {
				amount -= self.backing.len() - self.read;
				self.read = amount;
			}
		}
		Ok(())
	}
	//Write to queue. Returns Err(()) if EOF.
	pub fn write(&mut self, buf: &[u8]) -> Result<usize, QueueWriteError>
	{
		fail_if!(self.eof_written, QueueWriteError::Eof);
		if buf.len() == 0 { return Ok(0); }
		//Allocate space for write.
		let current = self.buffered();
		let newneed = current.saturating_add(buf.len()).saturating_add(1);
		fail_if!(newneed.wrapping_add(1) == 0, QueueWriteError::TooBig);
		if newneed > self.backing.len() {
			//We need to reallocate backing memory.
			let newneed = round_up_to_power_of_2(newneed);
			let oldsize = self.backing.len();
			self.backing.resize(newneed, 0);
			let newsize = self.backing.len();
			//If read is before write, then the buffer is in one segment, not needing adjustment.
			if self.read > self.write {
				//See which is shorter:
				//1) Copying 0..write to oldsize..
				//2) Copying read...oldsize to end of backing.
				let size1 = self.write;
				let size2 = oldsize - self.read;
				if size1 <= size2 && newsize - oldsize >= size1 {
					//Copy 0..write to oldsize.. Oldsize must be bigger.
					for i in 0..self.write {
						self.backing[oldsize + i] = self.backing[i];
					}
					self.write += oldsize;
					if self.write == newsize { self.write = 0; }
				} else {
					//Copy read..oldsize to end of backing.
					let size = oldsize - self.read;
					for i in 0..size {
						self.backing[newsize - i - 1] = self.backing[oldsize - i - 1];
					}
					self.read += newsize - oldsize;
				}
			}
		}

		if self.backing.len() - self.write <= buf.len() {
			//Buffer is in two parts and we need both.
			let maxc1 = self.backing.len() - self.write;
			(&mut self.backing[self.write..]).copy_from_slice(&buf[..maxc1]);
			let remaining = buf.len() - maxc1;
			(&mut self.backing[..remaining]).copy_from_slice(&buf[maxc1..]);
			self.write = remaining;
		} else {
			//Buffer is in one part, or we only need one part.
			(&mut self.backing[self.write..][..buf.len()]).copy_from_slice(buf);
			self.write += buf.len();
		}
		Ok(buf.len())
	}
	//Count amount of data in buffer.
	pub fn buffered(&self) -> usize
	{
		if self.read <= self.write {
			self.write - self.read
		} else {
			self.backing.len() - self.read + self.write
		}
	}
	//Abort immediately from receive side.
	pub fn abort(&mut self)
	{
		self.eof_written = true;
		self.backing.clear();
		self.read = 0;
		self.write = 0;
	}
	//Signal EOF after this data.
	pub fn send_eof(&mut self)
	{
		self.eof_written = true;
	}
	//EOF sent?
	pub fn eof_sent(&self) -> bool
	{
		self.eof_written
	}
}

#[test]
fn basic_write_freespace()
{
	use std::iter::repeat;
	let mut queue = Queue {
		backing: repeat(0u8).take(128).collect(),
		read: 24,
		write: 56,
		eof_written: false,
	};
	queue.write(&[2, 6, 2, 5, 2, 3]).unwrap();
	assert_eq!(queue.write, 62);
	assert_eq!(queue.buffered(), 38)
}

#[test]
fn basic_write_freespace_warped()
{
	use std::iter::repeat;
	let mut queue = Queue {
		backing: repeat(0u8).take(128).collect(),
		read: 24,
		write: 126,
		eof_written: false,
	};
	queue.write(&[2, 6, 2, 5, 2, 3]).unwrap();
	assert_eq!(queue.write, 4);
}

#[test]
fn write_realloc_case_a()
{
	use std::iter::repeat;
	let mut queue = Queue {
		backing: repeat(0u8).take(128).collect(),
		read: 12,
		write: 10,
		eof_written: false,
	};
	//This is supposed to copy the initial segment.
	queue.write(&[2, 6, 2, 5, 2, 3]).unwrap();
	assert_eq!(queue.backing.len(), 256);
	assert_eq!(queue.write, 144);
	assert_eq!(queue.read, 12);
	assert_eq!(queue.buffered(), 132);
}

#[test]
fn write_realloc_case_b()
{
	use std::iter::repeat;
	let mut queue = Queue {
		backing: repeat(0u8).take(128).collect(),
		read: 120,
		write: 119,
		eof_written: false,
	};
	//This is supposed to copy the final segment.
	queue.write(&[2, 6, 2, 5, 2, 3]).unwrap();
	assert_eq!(queue.backing.len(), 256);
	assert_eq!(queue.write, 125);
	assert_eq!(queue.read, 248);
	assert_eq!(queue.buffered(), 133);
}

#[test]
fn write_realloc_case_n()
{
	use std::iter::repeat;
	let mut queue = Queue {
		backing: repeat(0u8).take(128).collect(),
		read: 1,
		write: 126,
		eof_written: false,
	};
	//This is supposed to copy the final segment.
	queue.write(&[2, 6, 2, 5, 2, 3]).unwrap();
	assert_eq!(queue.backing.len(), 256);
	assert_eq!(queue.write, 132);
	assert_eq!(queue.read, 1);
	assert_eq!(queue.buffered(), 131);
}

#[test]
fn write_sequence()
{
	let mut sbuf = [0u8; 256];
	let mut dbuf = [0u8; 256];
	for i in 0..256 { sbuf[i] = i as u8; }
	let mut queue = Queue {
		backing: Vec::new(),
		read: 0,
		write: 0,
		eof_written: false,
	};
	assert_eq!(queue.buffered(), 0);
	queue.write(&sbuf[0..31]).unwrap();
	assert_eq!(queue.backing.len(), 32);
	assert_eq!(queue.write, 31);
	assert_eq!(queue.read, 0);
	assert_eq!(queue.buffered(), 31);
	assert_eq!(queue.read(&mut dbuf[0..5]).unwrap(), 5);
	assert_eq!(queue.backing.len(), 32);
	assert_eq!(queue.write, 31);
	assert_eq!(queue.read, 5);
	assert_eq!(queue.buffered(), 26);
	queue.write(&mut sbuf[31..40]).unwrap();
	assert_eq!(queue.backing.len(), 64);
	assert_eq!(queue.write, 40);
	assert_eq!(queue.read, 5);
	assert_eq!(queue.buffered(), 35);
	queue.write(&mut sbuf[40..64]).unwrap();
	assert_eq!(queue.backing.len(), 64);
	assert_eq!(queue.write, 0);
	assert_eq!(queue.read, 5);
	assert_eq!(queue.buffered(), 59);
	queue.write(&mut sbuf[64..68]).unwrap();
	assert_eq!(queue.backing.len(), 64);
	assert_eq!(queue.write, 4);
	assert_eq!(queue.read, 5);
	assert_eq!(queue.buffered(), 63);
	queue.write(&mut sbuf[68..69]).unwrap();
	assert_eq!(queue.backing.len(), 128);
	assert_eq!(queue.write, 69);
	assert_eq!(queue.read, 5);
	assert_eq!(queue.buffered(), 64);
	queue.write(&mut sbuf[69..127]).unwrap();
	assert_eq!(queue.backing.len(), 128);
	assert_eq!(queue.write, 127);
	assert_eq!(queue.read, 5);
	assert_eq!(queue.buffered(), 122);
	assert_eq!(queue.read(&mut dbuf[5..120]).unwrap(), 115);
	assert_eq!(queue.backing.len(), 128);
	assert_eq!(queue.write, 127);
	assert_eq!(queue.read, 120);
	assert_eq!(queue.buffered(), 7);
	queue.write(&mut sbuf[127..227]).unwrap();
	assert_eq!(queue.backing.len(), 128);
	assert_eq!(queue.write, 99);
	assert_eq!(queue.read, 120);
	assert_eq!(queue.buffered(), 107);
	queue.write(&mut sbuf[227..256]).unwrap();
	assert_eq!(queue.eof_sent(), false);
	queue.send_eof();
	assert_eq!(queue.eof_sent(), true);
	assert_eq!(queue.write(&mut sbuf[256..256]), Err(QueueWriteError::Eof));
	assert_eq!(queue.backing.len(), 256);
	assert_eq!(queue.write, 128);
	assert_eq!(queue.read, 248);
	assert_eq!(queue.buffered(), 136);
	assert_eq!(queue.read(&mut dbuf[120..256]).unwrap(), 136);
	assert_eq!(queue.backing.len(), 256);
	assert_eq!(queue.write, 128);
	assert_eq!(queue.read, 128);
	assert_eq!(queue.buffered(), 0);
	assert_eq!(queue.read(&mut dbuf[256..256]), None);
	assert_eq!(queue.eof_sent(), true);
	assert_eq!(&sbuf[..], &dbuf[..]);
}
