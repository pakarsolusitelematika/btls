//!This module contains definitions of feature flags.

use stdlib::{Display, String, FmtError, Formatter};

#[doc(hidden)]
pub const CONFIG_PAGES: usize = 1;

///Enable TLS 1.2 support.
///
///This flag resides in page 0.
pub const FLAGS0_ENABLE_TLS12: u64 = 0x1;
///Enable TLS 1.3 support.
///
///This flag resides in page 0.
pub const FLAGS0_ENABLE_TLS13: u64 = 0x2;
///Require EMS.
///
///This causes handshake attempts where the other side does not support either Extended Master Secret nor TLS 1.3
///to fail. Useful if you are using exporters.
///
///This flag resides in page 0.
pub const FLAGS0_REQUIRE_EMS: u64 = 0x4;
///Require Certificate Transparency.
///
///If this is set, attempts to handshake with servers that don't support Certificate Transparency will fail.
///
///This flag is ignored on server side.
///
///This flag resides in page 0.
pub const FLAGS0_REQUIRE_CT: u64 = 0x8;
///Don't send any shares.
///
///This is meant for debugging only. Don't use otherwise.
///
///This flag resides in page 0.
pub const FLAGS0_DEBUG_NO_SHARES: u64 = 0x10;
///Assume hardware-accelerated AES.
///
///This flag resides in page 0.
pub const FLAGS0_ASSUME_HW_AES: u64 = 0x20;
///Require Online Certificate Status Protocol stapling.
///
///If this is set, attempts to handshake with servers that don't send OCSP staple will fail.
///
///This flag is ignored on server side.
///
///This flag resides in page 0.
pub const FLAGS0_REQUIRE_OCSP: u64 = 0x40;
///Allow bad cryptographic algorithms
///
///This flag is ignored on client side.
///
///This flag resides in page 0.
pub const FLAGS0_ALLOW_BAD_CRYPTO: u64 = 0x80;

///Default flags for page 0.
pub const DEFAULT_FLAGS: [u64; CONFIG_PAGES] = [FLAGS0_ENABLE_TLS12];

///Look up a flag by name.
///
///# Parameters:
///
/// * `flagname`: Name of the flag to look up.
///
///# Returns:
///
/// * If the flag exists, Some(page, mask), where page is the page number and mask is the flag bit in the page.
/// * If the flag does not exist, None.
///
///# Notes:
///
/// The currently known flag names are:
/// * tls12 -> FLAGS0_ENABLE_TLS12
/// * tls13 -> FLAGS0_ENABLE_TLS13
/// * require-ems -> FLAGS0_REQUIRE_EMS
/// * require-ocsp -> FLAGS0_REQUIRE_OCSP
/// * require-ct -> FLAGS0_REQUIRE_CT
/// * debug-no-shares -> FLAGS0_DEBUG_NO_SHARES
/// * assume-hw-aes -> FLAGS0_ASSUME_HW_AES
/// * allow-bad-crypto -> FLAGS0_ALLOW_BAD_CRYPTO
///
pub fn lookup_flag(flagname: &str) -> Option<(usize, u64)>
{
	if flagname == ENABLE_TLS12_NAME { return Some((0, FLAGS0_ENABLE_TLS12)); }
	if flagname == ENABLE_TLS13_NAME { return Some((0, FLAGS0_ENABLE_TLS13)); }
	if flagname == REQUIRE_EMS_NAME { return Some((0, FLAGS0_REQUIRE_EMS)); }
	if flagname == REQUIRE_OCSP_NAME { return Some((0, FLAGS0_REQUIRE_OCSP)); }
	if flagname == REQUIRE_CT_NAME { return Some((0, FLAGS0_REQUIRE_CT)); }
	if flagname == DEBUG_NO_SHARES_NAME { return Some((0, FLAGS0_DEBUG_NO_SHARES)); }
	if flagname == ASSUME_HW_AES { return Some((0, FLAGS0_ASSUME_HW_AES)); }
	if flagname == ALLOW_BAD_CRYPTO_NAME { return Some((0, FLAGS0_ALLOW_BAD_CRYPTO)); }
	None
}

///Error from setting flags from admin config.
#[derive(Copy,Clone,Debug)]
pub struct ConfigFlagsError<'a>
{
	///The errorneous entry.
	pub entry: ConfigFlagsEntry<'a>,
	///The more precise error.
	pub kind: ConfigFlagsErrorKind<'a>,
}

impl<'a> Display for ConfigFlagsError<'a>
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		fmt.write_fmt(format_args!("Entry#{}({}): {}", self.entry.entrynum.saturating_add(1),
			self.entry.entry_str, self.kind))
	}
}

///Error from setting flags from admin config.
#[derive(Copy,Clone,Debug)]
pub enum ConfigFlagsErrorKind<'a>
{
	///Unrecognized setting.
	UnrecognizedSetting(&'a str),
	///Setting has no effect.
	NoEffect(&'a str),
	///Setting requires argument.
	ArgumentRequired(&'a str),
	///Setting has no argument.
	NoArgument(&'a str),
	///Error setting the setting.
	Error(&'a str),
}

impl<'a> Display for ConfigFlagsErrorKind<'a>
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::ConfigFlagsErrorKind::*;
		match self {
			&UnrecognizedSetting(x) => fmt.write_fmt(format_args!("Unrecognized setting '{}'", x)),
			&NoEffect(x) => fmt.write_fmt(format_args!("Setting '{}' to this value has no effect", x)),
			&ArgumentRequired(x) => fmt.write_fmt(format_args!("Setting '{}'  requires an argument", x)),
			&NoArgument(x) => fmt.write_fmt(format_args!("Setting '{}' has no argument", x)),
			&Error(x) => fmt.write_fmt(format_args!("Error processing: {}", x)),
		}
	}
}


#[doc(hidden)]
pub static ENABLE_TLS12_NAME: &'static str = "tls12";
#[doc(hidden)]
pub static ENABLE_TLS13_NAME: &'static str = "tls13";
#[doc(hidden)]
pub static REQUIRE_EMS_NAME: &'static str = "require-ems";
#[doc(hidden)]
pub static REQUIRE_CT_NAME: &'static str = "require-ct";
#[doc(hidden)]
pub static REQUIRE_OCSP_NAME: &'static str = "require-ocsp";
#[doc(hidden)]
pub static DEBUG_NO_SHARES_NAME: &'static str = "debug-no-shares";
#[doc(hidden)]
pub static ASSUME_HW_AES: &'static str = "assume-hw-aes";
#[doc(hidden)]
pub static ALLOW_BAD_CRYPTO_NAME: &'static str = "allow-bad-crypto";

#[doc(hidden)]
pub fn set_combine_flags_set(_page: usize, newmask: u64, oldmask: u64) -> u64
{
	oldmask | newmask
}

#[doc(hidden)]
pub fn set_combine_flags_clear(_page: usize, newmask: u64, oldmask: u64) -> u64
{
	oldmask & !newmask
}

#[doc(hidden)]
#[derive(Copy,Clone,Debug)]
//Value for config flag.
pub enum ConfigFlagsValue<'a>
{
	//Disabled.
	Disabled,
	//Enabled.
	Enabled,
	//Explicit
	Explicit(&'a str),
}

#[doc(hidden)]
#[derive(Copy,Clone,Debug)]
//Entry info for config flag.
pub struct ConfigFlagsEntry<'a>
{
	//Ordinal of the entry
	pub entrynum: usize,
	//The whole entry.
	pub entry_str: &'a str,
}


#[doc(hidden)]
pub fn split_config_str<T, F>(objself: &mut T, config: &str, mut cb: F) where F: FnMut(&mut T, &str,
	ConfigFlagsValue, ConfigFlagsEntry)
{
	let _config = config.as_bytes();
	let mut idx = 0;
	let mut itemstart = 0;
	let mut escape = false;
	let mut ordinal = 0;
	while idx < _config.len() {
		if !escape {
			if _config.get(idx).map(|x|*x).unwrap_or(0) == 92 {
				//Backslash escape.
				escape = true;
			} else if _config.get(idx).map(|x|*x).unwrap_or(0) == 44 {
				//Split here.
				let item = &config[itemstart..idx];
				call_with_config_item(objself, item, &mut cb, ordinal);
				ordinal = ordinal.wrapping_add(1);
				itemstart = idx.wrapping_add(1);
			} else {
				//Not special.
			}
		} else {
			escape = false;
		}
		idx = idx.wrapping_add(1);
	}
	if itemstart < config.len() {
		let item = &config[itemstart..];
		call_with_config_item(objself, item, &mut cb, ordinal);
	}
}

#[doc(hidden)]
pub fn call_with_config_item<T, F>(objself: &mut T, item: &str, mut cb: &mut F, ordinal: usize)
	where F: FnMut(&mut T, &str, ConfigFlagsValue, ConfigFlagsEntry)
{
	let mut itemback = String::new();
	let aitem = if item.find('\\') != None {
		let mut escape = false;
		for i in item.chars() {
			if i == '\\' && !escape {
				escape = true;
			} else {
				itemback.push(i);
				escape = false;
			}
		}
		&itemback
	} else {
		item
	};
	if let Some(eqpos) = aitem.find('=') {
		cb(objself, &aitem[0..eqpos], ConfigFlagsValue::Explicit(&aitem[eqpos+1..]), ConfigFlagsEntry{
			entrynum:ordinal, entry_str:aitem});
	} else {
		//Boolean flag.
		let fchar = aitem.chars().next();
		if fchar == Some('+') {
			cb(objself, &aitem[1..], ConfigFlagsValue::Enabled, ConfigFlagsEntry{entrynum:ordinal,
				entry_str:aitem});
		} else if fchar == Some('-') || fchar == Some('!') {
			cb(objself, &aitem[1..], ConfigFlagsValue::Disabled, ConfigFlagsEntry{entrynum:ordinal,
				entry_str:aitem});
		} else {
			cb(objself, aitem, ConfigFlagsValue::Enabled, ConfigFlagsEntry{entrynum:ordinal,
				entry_str:aitem});
		}
	}
}
