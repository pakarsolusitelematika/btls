mod internal
{
use std::iter::repeat;

pub struct CircularBuffer
{
	data: Vec<u8>,		//Backing buffer.
	read_ptr: usize,	//Read pointer. The buffer is not truly circular, always <= write_ptr.
	write_ptr: usize,	//Write pointer
}

impl CircularBuffer
{
	pub fn new(size: usize) -> CircularBuffer
	{
		CircularBuffer {
			data: repeat(0u8).take(size).collect(),
			read_ptr: 0,
			write_ptr: 0,
		}
	}
	pub fn space_remaining(&self) -> usize
	{
		self.data.len() - self.write_ptr + self.read_ptr
	}
	pub fn data_remaining(&self) -> usize
	{
		self.write_ptr - self.read_ptr
	}
	pub fn get_write<'a>(&'a mut self, size: usize) -> Result<&'a mut [u8], ()>
	{
		if size > self.space_remaining() { return Err(()); }
		if size > self.data.len() - self.write_ptr {
			//Gotta compact the buffer.
			let count = self.data_remaining();
			for i in 0..count {
				self.data[i] = self.data[i + self.read_ptr];
			}
			self.write_ptr -= self.read_ptr;
			self.read_ptr = 0;
		}
		Ok(&mut self.data[self.write_ptr..][..size])
	}
	pub fn put_write<'a>(&mut self, size: usize) -> Result<(), ()>
	{
		if size > self.space_remaining() { return Err(()); }
		self.write_ptr = self.write_ptr.wrapping_add(size);
		Ok(())
	}
	pub fn get_read<'a>(&'a mut self, size: usize) -> Result<&'a mut [u8], ()>
	{
		if size > self.data_remaining() { return Err(()); }
		Ok(&mut self.data[self.read_ptr..][..size])
	}
	pub fn put_read<'a>(&mut self, size: usize) -> Result<(), ()>
	{
		if size > self.data_remaining() { return Err(()); }
		self.read_ptr = self.read_ptr.wrapping_add(size);
		Ok(())
	}
}

}

pub use self::internal::*;
