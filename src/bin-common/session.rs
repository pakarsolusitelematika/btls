use btls::{Session, RxAppdataHandler, TxIrqHandler};

use std::io::{ErrorKind, Read as IoRead, stdin, stdout, stderr, Result as IoResult, Write as IoWrite};
use std::mem::swap;
use std::thread::spawn;
use std::panic::{catch_unwind, UnwindSafe, AssertUnwindSafe};
use std::sync::mpsc::{channel as mpsc_channel, Receiver as MpscReceiver, Sender as MpscSender};
use std::sync::Arc;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering as MemOrder;

#[path="./circbuf.rs"]
mod circbuf;

use self::circbuf::CircularBuffer;

pub trait TryClone: Sized
{
	fn try_clone_trait(&self) -> IoResult<Self>;
}

struct StdoutSink;

impl RxAppdataHandler for StdoutSink
{
	fn received_data(&mut self, buf: &[u8])
	{
		let len = buf.len();
		let mut ptr = 0;
		while ptr < len {
			let amt = stdout().write(&buf[ptr..len]).unwrap();
			ptr += amt;
		}
	}
}

struct TxIrq
{
	flag: Arc<AtomicBool>,
	sender: MpscSender<()>,
}

impl TxIrqHandler for TxIrq
{
	fn wants_transmit(&mut self)
	{
		if !self.flag.load(MemOrder::SeqCst) {
			let _ = self.sender.send(());
			self.flag.store(true, MemOrder::SeqCst);
		}
	}
}

macro_rules! handle_write_error
{
	($x:expr, $lost:expr) => {
		if $x.kind() == ErrorKind::BrokenPipe {
			writeln!(stderr(), "<Peer has gone away>").unwrap();
			$lost.store(true, MemOrder::SeqCst);
			break;
		} else if $x.kind() == ErrorKind::WouldBlock {
			0usize
		} else if $x.kind() == ErrorKind::Interrupted {
			0usize
		} else {
			$lost.store(true, MemOrder::SeqCst);
			writeln!(stderr(), "<Error writing to peer: {}>", $x).unwrap();
			break;
		}
	}
}

fn stdin_to_tls_task<Socktype:IoRead+IoWrite+TryClone+Send+UnwindSafe+'static,
	Sess:Session+Clone+Send+UnwindSafe+'static>(mut wsession: Sess, mut wsock: Socktype,
	irq_receiver: MpscReceiver<()>, irq_flag: Arc<AtomicBool>, exit_flag: Arc<AtomicBool>,
	connection_lost: Arc<AtomicBool>)
{
	let mut eofd = false;
	let mut eof_transmit = false;
	let mut fatal = true;
	let mut wbuf = [0; 65536];
	let mut sbuf = [0; 32768];
	let mut wbuf_range = 0usize..0usize;
	let mut sbuf_range = 0usize..0usize;
	while wsession.aborted_by().is_none() {
		let _ = irq_receiver.try_recv();	//Clear the queue.
		//If buffers are empty, fill source buffer again.
		if wbuf_range.start == wbuf_range.end && sbuf_range.start == sbuf_range.end && !eofd {
			sbuf_range = 0..0;
			sbuf_range.end = match stdin().read(&mut sbuf) {
				Ok(0) => { eofd = true; 0 },
				Ok(x) => x,
				Err(x) => {
					if x.kind() == ErrorKind::WouldBlock {
						0usize
					} else if x.kind() == ErrorKind::Interrupted {
						0usize
					} else {
						writeln!(stderr(), "<Error reading from stdin: {}>", x).unwrap();
						break;
					}
				}
			};
		}
		//If there is something to transmit and output buffer is empty, do it.
		if wbuf_range.start == wbuf_range.end && (sbuf_range.start < sbuf_range.end ||
			(eofd && !eof_transmit)) {
			if wsession.aborted_by().is_some() { break; }
			//If EOF flag is set, send the EOF and mark sent.
			if sbuf_range.start == sbuf_range.end && eofd && !eof_transmit {
				wsession.send_eof();
				eof_transmit = true;
			}
			irq_flag.store(false, MemOrder::SeqCst);	//Clear the IRQ flag before write.
			let (dout, din) = match wsession.zerolatency_write(&mut wbuf, &sbuf[sbuf_range.clone()]) {
				Ok((x, y)) => (x, y),
				Err(x) => {
					writeln!(stderr(), "<Error in zerolatency_write: {}>", x).unwrap();
					break;
				}
			};
			sbuf_range.start = sbuf_range.start + din;
			wbuf_range = 0..dout;
		}
		//If there is something in the write buffer, try writing it out to wire. Even if TLS has already
		//aborted.
		if wbuf_range.start < wbuf_range.end {
			let amt = match wsock.write(&wbuf[wbuf_range.clone()]) {
				Ok(x) => x,
				Err(x) => handle_write_error!(x, connection_lost),
			};
			wbuf_range.start = wbuf_range.start + amt;
			//If we have reached end of write buffer and eof_transmit is set, we have sent all the
			//data that will be sent.
			if wbuf_range.start == wbuf_range.end && eof_transmit {
				fatal = false;
				writeln!(stderr(), "<EOF sent>").unwrap();
				break;
			}
		}
	}
	if fatal || wsession.aborted_by().is_some() { return; }
	//Stick around in order to send possible alerts from reverse direction.
	//The irq_receiver receive waits for event.
	while irq_receiver.recv().is_ok() {
		if exit_flag.load(MemOrder::SeqCst) { break; }		//Finally exit.
		if !irq_flag.load(MemOrder::SeqCst) { continue; }	//Spurious
		irq_flag.store(false, MemOrder::SeqCst);		//Clear the IRQ flag.
		if wsession.aborted_by().is_some() { break; }
		//Use write_tls, as it is easier than zerolatency writes here.
		match wsession.write_tls(&mut wsock) {
			Ok(_) => (),
			Err(x) => {handle_write_error!(x, connection_lost);}
		};
	}
}

fn tls_to_stdout_task<Socktype:IoRead+IoWrite+TryClone+Send+UnwindSafe+'static,
	Sess:Session+Clone+Send+UnwindSafe+'static>(mut rsession: Sess, mut rsock: Socktype,
	connection_lost: Arc<AtomicBool>)
{
	rsession.set_appdata_rx_fn(Some(Box::new(StdoutSink)));
	let mut buffer = CircularBuffer::new(65536);
	let mut eof_ack = false;
	while rsession.aborted_by().is_none() {
		//The TLS library should always be ready for read, so no need to check wants_read.
		//Read from socket into circular bufer.
		let amount = {
			let wslice = buffer.get_write(16384 + 2048 + 5).unwrap();
			match rsock.read(wslice) {
				Ok(0) => {
					writeln!(stderr(), "<Peer has gone away>").unwrap();
					connection_lost.store(true, MemOrder::SeqCst);
					break;
				},
				Ok(x) => x,
				Err(x) => {
					if x.kind() == ErrorKind::BrokenPipe {
						writeln!(stderr(), "<Peer has gone away>").unwrap();
						connection_lost.store(true, MemOrder::SeqCst);
						break;
					} else if x.kind() == ErrorKind::WouldBlock {
						0usize
					} else if x.kind() == ErrorKind::Interrupted {
						0usize
					} else {
						writeln!(stderr(), "<Error reading from peer: {}>",x).unwrap();
						connection_lost.store(true, MemOrder::SeqCst);
						break;
					}
				}
			}
		};
		if amount == 0 { continue; }
		buffer.put_write(amount).unwrap();
		//Pass the data to TLS.
		while buffer.data_remaining() > 0 {
			let hlen = {
				let hdrlen = buffer.data_remaining();
				let hslice = buffer.get_read(hdrlen).unwrap();
				rsession.get_record_size(hslice)
			};
			if let Some(pktlen) = hlen {
				if pktlen > 16384 + 2048 + 5 {
					//Just submit some garbage to TLS in order to crash the connection.
					let garbage = buffer.get_read(5).unwrap();
					let _ = rsession.submit_tls_record(garbage);
				}
				if buffer.data_remaining() < pktlen { break; }
				{
					let rslice = buffer.get_read(pktlen).unwrap();
					let _ = rsession.submit_tls_record(rslice);
				}
				buffer.put_read(pktlen).unwrap();
			} else {
				break;
			}
		}
		if rsession.is_eof() && !eof_ack {
			writeln!(stderr(), "<End of data from peer>").unwrap();
			eof_ack = true;
		}
	}
}

pub fn handle_peer<Socktype:IoRead+IoWrite+TryClone+Send+UnwindSafe+'static,
	Sess:Session+Clone+Send+UnwindSafe+'static>(
	mut session: Sess, mut sock: Socktype, side: &str)
{
	let (irq_tx, irq_rx) = mpsc_channel();
	let irq_flag = Arc::new(AtomicBool::new(false));
	let exit_flag = Arc::new(AtomicBool::new(false));
	let exit_flag2 = exit_flag.clone();

	let irq = TxIrq{sender: irq_tx.clone(), flag: irq_flag.clone()};
	session.set_tx_irq(Box::new(irq));

	let (chan_tx1, chan_rx) = mpsc_channel();
	let chan_tx2 = chan_tx1.clone();
	let _chan_tx_backup = chan_tx1.clone();

	let connection_lost = Arc::new(AtomicBool::new(false));
	let connection_lost1 = connection_lost.clone();
	let connection_lost2 = connection_lost.clone();

	while !session.handshake_completed() {
		while irq_rx.try_recv().is_ok() {
			irq_flag.store(false, MemOrder::SeqCst);	//Clear IRQ before write.
			let _ = session.write_tls(&mut sock);
		}
		if session.wants_read() {
			let _ = session.read_tls(&mut sock);
		}
		if let Some(x) = session.aborted_by() {
			panic!("{} abort: {}", side, x);
		}
	}
	while irq_rx.try_recv().is_ok() {
		irq_flag.store(false, MemOrder::SeqCst);	//Clear IRQ before write.
		let _ = session.write_tls(&mut sock);
	}
	let rsession = session.clone();
	let wsession = session.clone();
	let rsock = sock.try_clone_trait().unwrap();
	let wsock = sock.try_clone_trait().unwrap();
	let conninfo = session.connection_info();
	writeln!(stderr(), "Handshake complete, crypto parameters:").unwrap();
	writeln!(stderr(), " - TLS version:              {}", conninfo.version_str).unwrap();
	writeln!(stderr(), " - Encryption:               {}", conninfo.protection_str).unwrap();
	writeln!(stderr(), " - PRF:                      {}", conninfo.hash_str).unwrap();
	writeln!(stderr(), " - Key Exchange:             {}", conninfo.exchange_group_str).unwrap();
	writeln!(stderr(), " - Server Signature:         {}", conninfo.signature_str).unwrap();
	writeln!(stderr(), " - Triple Handshake attack:  {}", if conninfo.ems_available { "Fixed" } else {
		"VULERNABLE" }).unwrap();
	writeln!(stderr(), " - OCSP stapling:            {}", if conninfo.validated_ocsp_shortlived {
		"Short-lived certificate" } else if conninfo.validated_ocsp { "Yes" } else { "No" }).unwrap();
	writeln!(stderr(), " - Certificate Transparency: {}", if conninfo.validated_ct { "Yes" } else { "No" }).
		unwrap();
	let r = spawn(move ||{
		match catch_unwind(AssertUnwindSafe(move ||{
			tls_to_stdout_task(rsession, rsock, connection_lost1);
		})) {
			Ok(_) => (),
			Err(_) => writeln!(stderr(), "<Read thread paniced>").unwrap()
		};
		chan_tx1.send(0).unwrap();
	});
	let w = spawn(move ||{
		match catch_unwind(AssertUnwindSafe(move ||{
			stdin_to_tls_task(wsession, wsock, irq_rx, irq_flag, exit_flag2, connection_lost2);
		})) {
			Ok(_) => (),
			Err(_) => writeln!(stderr(), "<Write thread paniced>").unwrap()
		};
		chan_tx2.send(1).unwrap();
	});
	let mut r = Some(r);
	let mut w = Some(w);
	let mut joined = 0u32;
	while (joined & 3) != 3 {
		let x = match chan_rx.recv() { Ok(x) => x, Err(_) => break };
		if x == 0 {
			//Kick the writer, so it exits.
			exit_flag.store(true, MemOrder::SeqCst);
			let _ = irq_tx.send(());
			let mut tmp = None;
			swap(&mut tmp, &mut r);
			if let Some(_tmp) = tmp { let _ = _tmp.join(); }
			joined |= 1;
		}
		if x == 1 {
			let mut tmp = None;
			swap(&mut tmp, &mut w);
			if let Some(_tmp) = tmp { let _ = _tmp.join(); }
			joined |= 2;
		}
	}
	let lost = connection_lost.load(MemOrder::SeqCst);
	writeln!(stderr(), "Session disconnected by: {}", match session.aborted_by() {
		Some(x) => format!("{}", x),
		None if lost => format!("Connection to peer lost"),
		None => format!("No cause???")
	}).unwrap();
}
