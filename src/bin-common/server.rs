use btls::ServerConfiguration;
use btls::{ClientCertificateReceived, ClientCertificateInfo, ChecksumFunction};
use btls::logging::StderrLog;
use btls::server_certificates::CertificateLookup;

use std::env::var;
use std::io::{Write as IoWrite, stderr};
use std::convert::AsRef;

#[path="./session.rs"]
mod session;
use self::session::handle_peer;
pub use self::session::{TryClone};
use super::transport::TransportListener;

#[derive(Clone)]
struct CertificateCallback
{
}

impl ClientCertificateReceived for CertificateCallback
{
	fn on_certificate_auth(&mut self, info: ClientCertificateInfo) -> Result<(), ()>
	{
		let spki_hash = info.get_public_key_hash(ChecksumFunction::Sha256);
		let ee_hash = info.get_ee_certificate_hash(ChecksumFunction::Sha256);
		let name = info.get_subject();
		if spki_hash.is_none() {
			writeln!(stderr(), "Client did not send a certificate.").unwrap();
			return Ok(());
		}
		writeln!(stderr(), "Client sent a certificate:").unwrap();
		writeln!(stderr(), "OCSP:             {}", if info.is_ocsp_stapled() { "Yes" } else { "No" }).
			unwrap();
		writeln!(stderr(), "CT:               {}", if info.is_sct_stapled() { "Yes" } else { "No" }).
			unwrap();
		if let Some(spki_hash) = spki_hash {
			let mut x = String::new();
			for i in spki_hash.as_ref().iter() { x = x + &format!("{:02x}", i); }
			writeln!(stderr(), "Key hash:         {}", x).unwrap();
		} else {
			writeln!(stderr(), "Key hash:         N/A").unwrap();
		}
		if let Some(ee_hash) = ee_hash {
			let mut x = String::new();
			for i in ee_hash.as_ref().iter() { x = x + &format!("{:02x}", i); }
			writeln!(stderr(), "Certificate hash: {}", x).unwrap();
		} else {
			writeln!(stderr(), "Certificate hash: N/A").unwrap();
		}
		if let Some(name) = name {
			let mut x = String::new();
			for i in name.iter() { x = x + &format!("{:02x}", i); }
			writeln!(stderr(), "Subject:          {}", x).unwrap();
		} else {
			writeln!(stderr(), "Subject:          N/A").unwrap();
		}
		Ok(())
	}
	fn clone_self(&self) -> Box<ClientCertificateReceived+Send>
	{
		Box::new(self.clone()) as Box<ClientCertificateReceived+Send>
	}
}

pub fn do_it<Lookup:CertificateLookup+Send+Sync+'static>(acceptsock: TransportListener, store: Lookup)
{
	let mut server_config = ServerConfiguration::from(&store as &CertificateLookup);
	if let Ok(settings) = var("BTLS_SETTINGS") {
		server_config.config_flags(&settings, |error|{writeln!(stderr(), "{}", error).unwrap();});
	}
	server_config.set_ask_client_cert(Box::new(CertificateCallback{}));
	server_config.set_debug(!0, Box::new(StderrLog));
	if let Ok(acmepath) = var("ACME_CHALLENGE_PATH") {
		server_config.set_acme_dir(&acmepath);
	}
	server_config.create_acme_challenge("test1.acme.invalid", None).unwrap();
	server_config.create_acme_challenge("test2.acme.invalid", Some("response.acme.invalid")).unwrap();
	writeln!(stderr(), "Waiting for connection...").unwrap();
	let mut sock = None;
	while sock.is_none() {
		let (sock2, _) = match acceptsock.accept() {
			Ok(x) => x,
			Err(x) => {
				writeln!(stderr(), "Can't accept connection: {}", x).unwrap();
				continue;
			}
		};
		sock = Some(sock2);
	}
	let sock = sock.unwrap();
	writeln!(stderr(), "Received connection...").unwrap();
	let server_session = server_config.make_session().unwrap();
	handle_peer(server_session, sock, "Server");
}
