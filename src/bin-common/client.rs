use btls::ClientConfiguration;
use btls::{ClientCertificateCallback, ClientCertificateCriteria};
use btls::futures::{create_future, FutureCallback, FutureReceiver, FutureSender};
use btls::server_keys::{KeyPair, LocalKeyPair};
use btls::server_certificates::{CertificateSigner, Certificate, CertificateChain};
use btls::client_certificates::{HostSpecificPin, LogHashFunction, TrustAnchor, TrustedLog};
use btls::logging::StderrLog;

use std::env::var;
use std::io::{Read as IoRead, Write as IoWrite, stderr};
use std::fs::File;
use std::sync::Arc;
use std::mem::swap;
use std::panic::UnwindSafe;

extern crate btls_aux_serialization;
extern crate btls_aux_signature_algo;
use self::btls_aux_serialization::{ASN1_SEQUENCE, Source};
use self::btls_aux_signature_algo::SignatureType;

//Adapter that adds a algorithm into signature.
struct AddAlgorithmAdapter
{
	//The algorithm to add.
	algorithm: u16,
	//Sender to send the resolved future on.
	resender: Option<FutureSender<Result<(u16, Vec<u8>), ()>>>
}

impl AddAlgorithmAdapter
{
	fn adapt(&self, val: Vec<u8>) -> (u16, Vec<u8>)
	{
		(self.algorithm, val)
	}
}

impl FutureCallback<Result<Vec<u8>, ()>> for AddAlgorithmAdapter
{
	fn on_settled(&mut self, val: Result<Vec<u8>, ()>) -> Result<Vec<u8>, ()>
	{
		let mut tmp = None;
		swap(&mut self.resender, &mut tmp);
		tmp.map(|resender|resender.settle(val.map(|x|self.adapt(x))));
		Err(())	//Nobody ever reads this.
	}
}


//A certificate that has been selected for signing.
#[derive(Clone)]
struct SelectedCertificate
{
	//The keypair that does the signing.
	keypair: Arc<Box<KeyPair+Send+Sync>>,
	//The TLS algorithm number to use.
	tls_algorithm: u16,
	//The certificate chain to send.
	certificate: CertificateChain,
}

impl CertificateSigner for SelectedCertificate
{
	fn sign(&self, data: &[u8]) -> FutureReceiver<Result<(u16, Vec<u8>), ()>>
	{
		//The KeyPair sign method returns future for Vec<u8>, but this function needs to return future for
		//(u16, Vec<u8>). Therefore we need an adapter to add the algorithm.
		let (with_algo_sender, with_algo_receiver) = create_future::<Result<(u16, Vec<u8>), ()>>();
		let algo = self.tls_algorithm;
		let ctx = AddAlgorithmAdapter{algorithm: algo, resender: Some(with_algo_sender)};
		//Start the signing process.
		let mut will_be_signature = self.keypair.sign(data, algo);
		//Now, register the adapter we just created.
		if let Some(mut ctx) = will_be_signature.settled_cb(Box::new(ctx)) {
			//Ok, the signing is already complete, Forward the value.
			let _ = match will_be_signature.read() {
				Ok(x) => ctx.on_settled(x),
				_ => return FutureReceiver::from(Err(())),
			};
		}
		//Will be chained here when ready.
		with_algo_receiver
	}
	fn is_ecdsa(&self) -> bool
	{
		true		//Does not matter for TLS 1.3.
	}
	fn get_certificate(&self) -> CertificateChain
	{
		self.certificate.clone()
	}
	//We don't implement get_ocsp nor get_scts. These get replaced by defaults.
}

#[derive(Clone)]
struct ClientCertificate
{
}

impl ClientCertificateCallback for ClientCertificate
{
	fn on_certificate_request(&mut self, criteria: ClientCertificateCriteria) ->
		Option<Box<CertificateSigner+Send>>
	{
		if let Ok(certname) = var("BTLS_CLIENTCERT") {
			let keyfile = format!("{}.key", certname);
			let certfile = format!("{}.crt", certname);
			let kp = match LocalKeyPair::new_file(keyfile) {
				Ok(x) => x,
				Err(x) => {
					writeln!(stderr(), "Can't load private key: {}", x).unwrap();
					return None;
				}
			};
			let mut algo = 0;
			for i in kp.get_schemes().iter() {
				if let Some(x) = SignatureType::by_tls_id(*i) {
					if x.sigalgo_const() & criteria.ee_algorithms != 0 {
						algo = *i;
						break;
					}
				}
			}
			if algo == 0 {
				writeln!(stderr(), "Can't find acceptable signature algorithm").unwrap();
				return None;
			}
			let mut certificates = Vec::new();
			let mut fp = match File::open(certfile) {
				Ok(x) => x,
				Err(x) => {
					writeln!(stderr(), "Can't open certificate file: {}", x).unwrap();
					return None;
				}
			};
			let mut certfile = Vec::new();
			match fp.read_to_end(&mut certfile) {
				Ok(_) => (),
				Err(x) => {
					writeln!(stderr(), "Can't read certificate file: {}", x).unwrap();
					return None;
				}
			};
			let mut certfile = Source::new(&certfile);
			while !certfile.at_end() {
				let ent = match certfile.read_asn1_value(ASN1_SEQUENCE, |x|x) {
					Ok(x) => x,
					Err(x) => {
						writeln!(stderr(), "Can't read a certificate: {}", x).unwrap();
						return None;
					}
				};
				certificates.push(Certificate(Arc::new(ent.outer.to_owned())));
			}
			Some(Box::new(SelectedCertificate{
				keypair: Arc::new(Box::new(kp)),
				tls_algorithm: algo,
				certificate: CertificateChain(Arc::new(certificates))
			}))
		} else {
			None
		}
	}
	fn clone_self(&self) -> Box<ClientCertificateCallback+Send>
	{
		Box::new(self.clone()) as Box<ClientCertificateCallback+Send>
	}
}

#[path="./session.rs"]
mod session;
use self::session::{handle_peer};
pub use self::session::{TryClone};

pub fn do_it<Socktype:IoRead+IoWrite+TryClone+Send+UnwindSafe+'static>(sock: Socktype, reference: Option<String>,
	trust_anchors: Vec<TrustAnchor>, pins: Vec<HostSpecificPin>)
{
	let google_aviator = include_bytes!("google-aviator.ctkey");
	let google_pilot = include_bytes!("google-pilot.ctkey");
	let google_rocketeer = include_bytes!("google-rocketeer.ctkey");
	let digicert = include_bytes!("digicert.ctkey");

	let google_aviator = TrustedLog {
		name: "Google Aviator".to_owned(),
		expiry: None,
		key: (&google_aviator[..]).to_owned(),
		v2_hash: LogHashFunction::Sha256,
		v2_id: Vec::new(),	//No V2 id.
	};
	let google_pilot = TrustedLog {
		name: "Google Pilot".to_owned(),
		expiry: None,
		key: (&google_pilot[..]).to_owned(),
		v2_hash: LogHashFunction::Sha256,
		v2_id: Vec::new(),	//No V2 id.
	};
	let google_rocketeer = TrustedLog {
		name: "Google Rocketeer".to_owned(),
		expiry: None,
		key: (&google_rocketeer[..]).to_owned(),
		v2_hash: LogHashFunction::Sha256,
		v2_id: Vec::new(),	//No V2 id.
	};
	let digicert = TrustedLog {
		name: "Digicert".to_owned(),
		expiry: None,
		key: (&digicert[..]).to_owned(),
		v2_hash: LogHashFunction::Sha256,
		v2_id: Vec::new(),	//No V2 id.
	};
	let nocheck = &reference.clone().unwrap() == "no-checking.invalid";
	if trust_anchors.len() == 0 && pins.len() == 0 && !nocheck {
		panic!("Need a target, reference and at least one trust anchor");
	}
	let mut client_config = ClientConfiguration::from(&trust_anchors[..]);
	if let Ok(settings) = var("BTLS_SETTINGS") {
		client_config.config_flags(&settings, |error|{writeln!(stderr(), "{}", error).unwrap();});
	}
	client_config.set_client_certificate(Box::new(ClientCertificate{}));
	client_config.set_debug(!0, Box::new(StderrLog));
	client_config.add_ct_log(&google_aviator);
	client_config.add_ct_log(&google_pilot);
	client_config.add_ct_log(&google_rocketeer);
	client_config.add_ct_log(&digicert);
	let client_session = if nocheck {
		client_config.make_session_insecure_nocheck().unwrap()
	} else {
		client_config.make_session_pinned(&reference.clone().unwrap(), &pins).unwrap()
	};
	handle_peer(client_session, sock, "Client");
}
