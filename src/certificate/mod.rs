use ::stdlib::Vec;

use btls_aux_keypair::KeyPair;
use btls_aux_signature_algo::{SignatureAlgorithm, SignatureType, WrappedRecognizedSignatureAlgorithm};
use btls_aux_serialization::{ASN1_BIT_STRING, ASN1_SEQUENCE, Sink};

mod ocsp;
pub use self::ocsp::{OcspDumpedScts, OcspExtensionsError, OcspResponseError, OcspSanityCheckResults,
	OcspValidationError, OcspValidationResult, ResponseStatusError};
mod parse;
pub use self::parse::{CertificateError, CertificateExtensionError, CertificateIssuer, CertificateSubject,
	CertificateTimestampError, CertificateToplevelError, CertificateValidityError, CertificateVersionError,
	extract_spki, NameConstraintsError, NameListError, ParsedCertificate, TranslateAsn1Oid};
mod sct;
pub use self::sct::{SctError, validate_scts};
mod timestamp;
pub use self::timestamp::{DecodeTsForUser, interpret_timestamp, TimestampParseError};
mod validation;
pub use self::validation::{CertificateValidationError, sanity_check_certificate_chain,
	sanity_check_certificate_chain_load, MajorCertProblem, TrustAnchorValue, validate_chain,
	validate_chain_client, validate_ee_spki};

///Flag for TLS feature 5: MUST-STAPLE.
///
///If this feature is present, then the server MUST present OCSP staple for the certificate to be considered
///valid.
pub const CFLAG_MUST_STAPLE: u32 = 1;
///Flag for TLS feature 23: MUST-EMS.
///
///If this feature is present, the server MUST negotiate Extended Master Secret, or TLS 1.3.
pub const CFLAG_MUST_EMS: u32 = 2;
///Flag for TLS feature 40: MUST-TLS13.
///
///If this feature is present and client offers TLS 1.3, the server MUST accept TLS 1.3.
pub const CFLAG_MUST_TLS13: u32 = 4;
///Flag for TLS feature 65281: MUST-RENEGO
///
///This does nothing: Safe renegotiation is always required.
pub const CFLAG_MUST_RENEGO: u32 = 8;
///Flag for TLS feature 18: MUST-CT
///
///If this feature is present, the server MUST present at least one validatable SCT, using certificate, OCSP
///or dedicated certificate transparency TLS extension.
pub const CFLAG_MUST_CT: u32 = 16;
///Flag for unknown TLS feature.
///
///This flag is ignored on client side (since whatever it is, we don't support it, and TLS features can never
///demand support on client side), and causes failure on server side if set (since it presents an obligation we
///do not know about).
pub const CFLAG_UNKNOWN: u32 = 0x80000000;

///Check that specified reference name is in specified set (also considering wildcards).
///
///# Parameters:
///
/// * `set`: The set of names (given as iterator).
/// * `refname`: The specified reference name to match.
///
///# Returns:
///
/// * True if reference name is found from the set.
/// * False otherwise.
pub fn name_in_set2<'a,T:Iterator<Item=&'a [u8]>>(set: T, refname: &[u8]) -> bool
{
	for i in set { if names_match(i, refname) { return true; } }
	return false;
}

///Check if (possibly wildcard) pattern matches reference name.
///
///# Parameters:
///
/// * `i`: The name or pattern to match.
/// * `refname`: The specified reference name to match.
///
///# Returns:
///
/// * True if the name matches the pattern.
/// * False otherwise.
fn names_match(i: &[u8], refname: &[u8]) -> bool
{
	//Non-wildcard case: The names have to match case insenstively (ASCII case insensitive only, the other
	//(locale dependent!) characters are not case-mapped.
	if matches_ascii_case_insensitive(refname, i) { return true; }
	if &i[..2] == b"*." {
		//Wildcard. In this case, remove the '*.' from pattern, and leftmost label from reference name,
		//and compare.
		let suffix = &i[2..];
		let refsuffix = refname.splitn(2, |x|*x==b'.').nth(1).unwrap_or(&[]);
		if suffix.len() > 0 && matches_ascii_case_insensitive(suffix, refsuffix) { return true; }
	}
	return false;
}

///Check if two names match ASCII case insensitively.
///
///The behaviour of this function does not depend on locale. Any non-ASCII characters are matched as-is.
///
///# Parameters:
///
/// * `a`: The first string.
/// * `b`: The second string.
///
///# Return value:
///
/// * `true` if the strings match.
/// * `false` otherwise.
fn matches_ascii_case_insensitive(a: &[u8], b: &[u8]) -> bool
{
	//Map both strings to lowercase (ASCII alphabets only) and then compare.
	let itera = a.iter().map(|x| if *x >= 0x41 && *x <= 0x5A { *x + 32 } else { *x });
	let iterb = b.iter().map(|x| if *x >= 0x41 && *x <= 0x5A { *x + 32 } else { *x });
	itera.eq(iterb)
}

#[doc(hidden)]		//Debugging function.
pub fn sign_certificate(tbs: &[u8], key: &KeyPair, alg_id: u16) -> Result<Vec<u8>, ()>
{
	//Get the signature algorithm OID we are supposed to use.
	let oid = SignatureType::by_tls_id(alg_id).and_then(|x|WrappedRecognizedSignatureAlgorithm::from(
		SignatureAlgorithm::Tls(x))).and_then(|x|x.unwrap_if_allowed(0).ok()).and_then(|x|x.get_oid()).
		ok_or(())?;
	//Grab the signature.
	let fsig = key.sign(tbs, alg_id);
	while !fsig.settled() {};	//Wait until settled.
	let sig = fsig.read().map_err(|_|())??;
	//Serialize the result. It is SEQUENCE of three components: The TBS (which is assumed to have ASN.1
	//header), the algorithm and then finally the signature as BIT STRING (0 bits stripped).
	let mut vec = Vec::new();
	vec.asn1_fn(ASN1_SEQUENCE, |x|{
		x.write_slice(tbs)?;
		x.asn1_fn(ASN1_SEQUENCE, |y|y.write_slice(oid))?;
		x.asn1_fn(ASN1_BIT_STRING, |y|{
			y.write_u8(0)?;			//No bits to discard: Octet aligned.
			y.write_slice(&sig)?;
			Ok(())
		})?;
		Ok(())
	})?;
	Ok(vec)
}


#[cfg(test)]
fn names_match_u(i: &str, refname: &str) -> bool { names_match(i.as_bytes(), refname.as_bytes()) }

#[test]
fn basic_matching()
{
	assert!(names_match_u("foo.example.org", "foo.example.org"));		//Basic.
	assert!(names_match_u("*.example.org", "foo.example.org"));		//Wildcard.
	assert!(!names_match_u("foo.example.org", "bar.example.org"));		//Basic mismatch first label.
	assert!(!names_match_u("foo.example.org", "foo.example.com"));		//Basic mismatch last lablel.
	assert!(!names_match_u("foo.example.org", "*.example.org"));		//Wildcard wrong way around
	assert!(!names_match_u("*.example.org", "bar.foo.example.com"));	//Multiple labels from wildcard.
	assert!(!names_match_u("*.example.org", "example.com"));		//No labels from wildcard.
	assert!(!names_match_u("foo.example.org", "foo.example.orgx"));		//Extra characters.
	assert!(!names_match_u("*.example.org", "foo.example.orgx"));		//Extra characters (wildcard).
	assert!(!names_match_u("foo.example.orgx", "foo.example.org"));		//Extra characters.
	assert!(!names_match_u("*.example.orgx", "foo.example.org"));		//Extra characters (wildcard).
}

#[test]
fn case_insensitive_matching()
{
	assert!(names_match_u("foo.ExAmPlE.oRg", "foo.example.org"));	//Basic case-insenstive.
	assert!(names_match_u("foo.ExAmPlE.oRg", "foo.eXaMpLe.OrG"));	//Basic case-insenstive.
	assert!(names_match_u("*.ExAmPlE.oRg", "foo.example.org"));	//Wildcard case-insenstive
	assert!(names_match_u("*.ExAmPlE.oRg", "foo.eXaMpLe.OrG"));	//Wildcard case-insenstive
	assert!(!names_match_u("foo.ExAmPlE.oRg", "foo.Fxample.org"));	//Middle labels don't match.
	assert!(!names_match_u("föö.example.org", "fÖÖ.example.org"));	//Non-ASCII is not case-mapped.
	assert!(!names_match_u("x-o.example.org", "xMo.example.org"));	//2D and 4D is not mapped.
	assert!(!names_match_u("x-o.example.org", "x\ro.example.org"));	//2D and 0D is not mapped.
	assert!(!names_match_u("x\ro.example.org", "x-o.example.org"));	//2D and 0D is not mapped.
	assert!(!names_match_u("xMo.example.org", "x-o.example.org"));	//2D and 4D is not mapped.
	assert!(!names_match_u("fo\u{e1}.example.org", "foA!.example.org"));	//High bits aren't lost.
	assert!(!names_match_u("fo\u{c1}.example.org", "fo\u{e1}.example.org"));	//81 and A1 aren't mapped.
	assert!(!names_match(b"fo\xc1.example.org", b"fo\xe1.example.org"));	//C1 and E1 aren't mapped.
}
