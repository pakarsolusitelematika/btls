use super::TranslateAsn1Oid;
use ::client_certificates::{LogHashFunction, TrustedLog};
use ::common::{TLS_LEN_SCT2_CERT_LEN, TLS_LEN_SCT2_LOGID, TLS_LEN_SCT_CERT_LEN, TLS_LEN_SCT_EXTENSIONS,
	TLS_LEN_SCT_PRECERT_LEN};
use ::logging::Logging;
use ::messages::{DigitallySigned, DigitallySignedReadError as DSRE};
use ::stdlib::{Arc, Box, BTreeMap, Display, FmtError, Formatter, Vec};

use btls_aux_hash::{ChecksumFunction, HashFunctionError};
use btls_aux_serialization::{AnyAsn1Tag, ASN1_SEQUENCE, ASN1_CTX3_C, Asn1Error, Sink, SlicingExt, Source,
	Asn1Tlv};
use btls_aux_signature::SignatureBlock;


//Translate error case into buffer overflow message. This can't actually happen, since we are using Vec,
//which can't overflow capacity.
macro_rules! try_buffer
{
	($exp:expr) => { $exp.map_err(|_|SctError::TbsTooBig)? }
}

///Error parsing/validating SCT.
#[derive(Clone,PartialEq,Eq,Debug)]
pub enum SctError
{
	///TBS serialization too big.
	TbsTooBig,
	///Can't read certificate toplevel for SCT extraction.
	CertificateToplevelError(Asn1Error),
	///Can't read certificate TBS for SCT extraction.
	CertificateTbsError(Asn1Error),
	///Scratch buffer not big enough.
	SctScratchNotBigEnough,
	///Can't read entry from TBS.
	TbsCertEntryReadFailed(Asn1Error),
	///Can't read extensions inner SEUQENCE from TBS.
	TbsCertInnertExtractFailed(Asn1Error),
	///Can't read extension SEUQENCE from TBS.
	TbsCertExtExtractFailed(Asn1Error),
	///SCT signature validation failed.
	VSctSignatureFailed,
	///SHA-256 is not 32 bytes?
	Sha256IsNot32Bytes,
	///SCT is empty.
	VSctEmpty,
	///SCT is missing log ID.
	VSctNoLogId,
	///SCT is missing timestamp.
	VSctNoTimestamp,
	///SCT is missing extensions block.
	VSctNoExtensions,
	///SCT is missing signature.
	VSctNoSignature,
	///Hash function failure.
	HashFailure(HashFunctionError),
	#[allow(dead_code)]
	#[doc(hidden)]
	Hidden__
}

impl Display for SctError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::SctError::*;
		match self {
			&TbsTooBig => fmt.write_str("TBS too big"),
			&CertificateToplevelError(ref x) => fmt.write_fmt(format_args!("Can't read Certificate \
				top level: {}", x)),
			&CertificateTbsError(ref x) => fmt.write_fmt(format_args!("Can't read Certificate \
				TBS: {}", x)),
			&SctScratchNotBigEnough => fmt.write_str("SCT length scratch buffer not big enough"),
			&TbsCertEntryReadFailed(x) => fmt.write_fmt(format_args!("Failed to read entry from \
				TBSCertificate: {}", x)),
			&TbsCertInnertExtractFailed(x) => fmt.write_fmt(format_args!("Failed to extract inner \
				extensions from TBSCertificate: {}", x)),
			&TbsCertExtExtractFailed(x) => fmt.write_fmt(format_args!("Failed to extract extension from \
				TBSCertificate: {}", x)),
			&VSctSignatureFailed => fmt.write_str("Rx SCT: Failed to validate signature"),
			&Sha256IsNot32Bytes => fmt.write_str("Internal error: Sha256 is not 32 bytes?"),
			&VSctEmpty => fmt.write_str("SCT corrupt: Empty"),
			&VSctNoLogId => fmt.write_str("SCT corrupt: No log ID"),
			&VSctNoTimestamp => fmt.write_str("SCT corrupt: No timestamp"),
			&VSctNoExtensions => fmt.write_str("SCT corrupt: No extensions"),
			&VSctNoSignature => fmt.write_str("SCT corrupt: No signature"),
			&HashFailure(ref x) => fmt.write_fmt(format_args!("Failed to invoke hash function: {}", x)),
			//&RCertParse(ref x) => fmt.write_fmt(format_args!("Parse responder certificate: {}", x)),
			&Hidden__ => fmt.write_str("Hidden__")
		}
	}
}

//Grab TBSCertificate from inside certificate.
fn extract_tbs_certificate<'b>(eecert: &'b [u8]) -> Result<&'b [u8], SctError>
{
	//The TBS certificate is the first ASN.1 node inside the top-level SEQUENCE.
	let mut msg = Source::new(eecert);
	msg.read_asn1_value(ASN1_SEQUENCE, |x|SctError::CertificateToplevelError(x))?.call(|z|{
		Ok(z.read_asn1_value(ASN1_SEQUENCE, |x|SctError::CertificateTbsError(x))?.outer)
	})
}

//Write the ASN.1 sequence header.
fn write_asn1_sequence_header<'b>(length: usize, scratch: &'b mut [u8], tbyte: u8) -> Result<&'b [u8], SctError>
{
	//Need at least 5 bytes of space for scratch.
	fail_if!(scratch.len() < 5, SctError::SctScratchNotBigEnough);
	//Scratch.len() >= 5 by above check, so 0 is in-range.
	scratch[0] = tbyte;
	let scratch_len: usize = match length {
		0...127 => 2,
		128...255 => 3,
		256...65535 => 4,
		65536...16777215 => 5,
		_ => fail!(SctError::TbsTooBig)
	};
	//Scratch.len() >= 5 by above check, so 1 and scratch_len-1<=5-1<=4<5 are in-range.
	if scratch_len > 2 { scratch[1] = scratch_len as u8 + 126; }
	if scratch_len > 4 { scratch[scratch_len - 3] = (length >> 16) as u8; }
	if scratch_len > 3 { scratch[scratch_len - 2] = (length >> 8) as u8; }
	scratch[scratch_len - 1] = length as u8;
	//scratch_len is at most 5, and scratch.len() is at least 5, so this is in bounds.
	let scratch_5b = &scratch[..scratch_len];
	Ok(scratch_5b)
}

//Reconstruct a TBSCertificate from pieces, first being part after toplevel header and before extensions, the
//second-fourth being parts of extensions. Can output up to 7 parts, Scratch needs to be at least 15 bytes.
fn reconstruct_tbs<'b>(p1: &'b [u8], p2: &'b [u8], p3: &'b [u8], p4: &'b [u8], scratch: &'b mut [u8]) ->
	Result<[&'b [u8];7], SctError>
{
	fail_if!(scratch.len() < 15, SctError::SctScratchNotBigEnough);
	//There is space for 3x5 bytes, so splits are valid.
	let (scratch1, scratch) = scratch.split_at_mut(5);
	let (scratch2, scratch) = scratch.split_at_mut(5);
	let (scratch3, _) = scratch.split_at_mut(5);

	//Lengths are maximum of 2^24, so this won't overflow.
	let exts_len = p2.len() + p3.len() + p4.len();
	let scratch3 = write_asn1_sequence_header(exts_len, scratch3, 48)?;	//48 => SEQUENCE.
	let oexts_len = exts_len + scratch3.len();
	let scratch2 = write_asn1_sequence_header(oexts_len, scratch2, 163)?;	//163 => CONTEXT 3
	let tbs_len = oexts_len + scratch2.len() + p1.len();
	let scratch1 = write_asn1_sequence_header(tbs_len, scratch1, 48)?;	//48 => SEQUENCE
	Ok([scratch1, p1, scratch2, scratch3, p2, p3, p4])
}

static SCT_V1_EXT: [u8;12] = [6, 10, 43, 6, 1, 4, 1, 214, 121, 2, 4, 2];
static SCT_V2_EXT: [u8;5] = [6, 3, 43, 101, 75];

//Extract pre extensions data from TBS.
fn split_extensions_from_tbs<'b>(tbs_cert: &'b [u8]) -> Result<(&'b [u8], Option<&'b [u8]>), SctError>
{
	let mut src = Source::new(tbs_cert);
	let mut ext_inner_payload = None;
	let mut pre_extensions: &'b [u8] = &[];
	let seq: Asn1Tlv = src.read_asn1_value(ASN1_SEQUENCE, |x|SctError::CertificateTbsError(x))?;
	seq.call(|mut x|{
		let success: Result<(), SctError> = Ok(());	//Hack to help type inference work.
		pre_extensions = x.as_slice();		//In case no extensions.
		while !x.at_end() {
			let tmppos = x.position();
			let tlv = x.read_asn1_value(AnyAsn1Tag, |x|SctError::TbsCertEntryReadFailed(x))?;
			if tlv.tag == ASN1_CTX3_C {
				ext_inner_payload = Some(tlv.value);
				pre_extensions = x.as_slice().slice_np(..tmppos).unwrap_or(&[]);
				break;
			}
		}
		success
	})?;
	if let Some(mut ext_inner_payload) = ext_inner_payload {
		//Got extensions, read them.
		let extensions = ext_inner_payload.read_asn1_value(ASN1_SEQUENCE, |x|
			SctError::TbsCertInnertExtractFailed(x))?.raw_p;
		Ok((pre_extensions, Some(extensions)))
	} else {
		//No extensions
		Ok((tbs_cert, None))
	}
}

fn find_v1_and_v2_sct_extensions<'b>(extensions: &[u8]) ->
	Result<(Option<(usize, usize)>, Option<(usize, usize)>), SctError>
{
	let mut extensions = Source::new(extensions);
	let mut split1 = None;
	let mut split2 = None;
	//Note that extensions does not have SEQUENCE header.
	while !extensions.at_end() {
		let pos = extensions.position();
		let (extension, contentlen) = extensions.read_asn1_value(ASN1_SEQUENCE, |x|
			SctError::TbsCertExtExtractFailed(x)).map(|x|
			(x.raw_p, x.outer.len()))?;
		if extension.slice_np(..SCT_V1_EXT.len()).unwrap_or(&[]) == SCT_V1_EXT {
			split1 = Some((pos, contentlen));
		}
		if extension.slice_np(..SCT_V2_EXT.len()).unwrap_or(&[]) == SCT_V2_EXT {
			split2 = Some((pos, contentlen));
		}
	}
	Ok((split1, split2))
}

//Construct v1 precert. The cert is in maximum 7 pieces. Scratch needs to be at least 15 bytes.
fn reconstruct_v1_precert<'b>(tbs_cert: &'b [u8], scratch: &'b mut [u8]) -> Result<[&'b [u8];7], SctError>
{
	//This is the TBSCertificate from inside certificate, without the 1.3.6.1.4.1.11129.2.4.2 extension.
	let (pre_extensions, extensions) = split_extensions_from_tbs(tbs_cert)?;
	let extensions = if let Some(x) = extensions {
		x
	} else {
		//No extensions to remove, return the cert as-is.
		return Ok([tbs_cert, &[], &[], &[], &[], &[], &[]]);
	};
	let (v1, _) = find_v1_and_v2_sct_extensions(extensions)?;
	let (end1, start2) = if let Some(v) = v1 {
		(v.0, v.0.saturating_add(v.1))
	} else {
		(0, 0)
	};
	let exts_begin = extensions.slice_np(..end1).unwrap_or(&[]);
	let exts_end = extensions.slice_np(start2..).unwrap_or(&[]);
	reconstruct_tbs(pre_extensions, exts_begin, exts_end, &[], scratch)
}

//Construct v1 precert. The cert is in maximum 7 pieces. Scratch needs to be at least 15 bytes.
fn reconstruct_v2_precert<'b>(tbs_cert: &'b [u8], scratch: &'b mut [u8]) -> Result<[&'b [u8];7], SctError>
{
	//This is the TBSCertificate without the 1.3.6.1.4.1.11129.2.4.2 or
	//1.3.101.75 extensions
	let (pre_extensions, extensions) = split_extensions_from_tbs(tbs_cert)?;
	let extensions = if let Some(x) = extensions {
		x
	} else {
		//No extensions to remove, return the cert as-is.
		return Ok([tbs_cert, &[], &[], &[], &[], &[], &[]]);
	};
	let (v1, v2) = find_v1_and_v2_sct_extensions(extensions)?;
	let (end1, start2, end2, start3) = match (v1, v2) {
		(Some(x), Some(y)) if x.0 < y.0 => (x.0, x.0.saturating_add(x.1), y.0, y.0.saturating_add(y.1)),
		(Some(x), Some(y)) => (y.0, y.0.saturating_add(y.1), x.0, x.0.saturating_add(x.1)),
		(Some(x), None) => (0, 0, x.0, x.0.saturating_add(x.1)),
		(None, Some(x)) => (0, 0, x.0, x.0.saturating_add(x.1)),
		(None, None) => (0, 0, 0, 0)
	};
	let exts_begin = extensions.slice_np(..end1).unwrap_or(&[]);
	let exts_mid = extensions.slice_np(start2..end2).unwrap_or(&[]);
	let exts_end = extensions.slice_np(start3..).unwrap_or(&[]);
	reconstruct_tbs(pre_extensions, exts_begin, exts_mid, exts_end, scratch)
}

fn validate_sct<'a>(eecert: &[u8], issuer_key: &[u8], log: &TrustedLog, ts: u64, extensions: &[u8],
	signature: DigitallySigned<'a>, is_precert: bool, version: u32) -> Result<bool, SctError>
{
	use self::SctError::*;
	if let Some(expired_at) = log.expiry {
		//Non-precert SCTs from expired logs are always invalid.
		if !is_precert { return Ok(false); }
		if expired_at < 0 { return Ok(false); }		//EEh....
		//Precert SCTs are invalid if they are newer than expiry.
		if ts / 1000 > (expired_at as u64) { return Ok(false); }
	}
	let hash_algo = match version {
		1 => ChecksumFunction::Sha256,
		2 => match log.v2_hash {
			LogHashFunction::Sha256 => ChecksumFunction::Sha256
		},
		_ => return Ok(false),		//Can't validate.
	};
	let issuer_key_hash = hash_algo.calculate(issuer_key).map_err(|x|HashFailure(x))?;
	let issuer_key_hash = issuer_key_hash.as_ref();
	let tbs_cert = extract_tbs_certificate(eecert)?;

	//No need to check sigscheme compatiblity with public key, as signature validation already checks this.
	let mut tbs = Vec::new();
	match version {
		1 => {
			try_buffer!(tbs.write_u16(0));	//Version and signature type (both 0).
			try_buffer!(tbs.write_u64(ts));
			if is_precert {
				let mut buf = [0u8; 15];
				try_buffer!(tbs.write_u16(1));
				try_buffer!(tbs.write_slice(issuer_key_hash));
				let tmp = reconstruct_v1_precert(tbs_cert, &mut buf)?;
				tbs.vector_fn(TLS_LEN_SCT_PRECERT_LEN, |x|{
					for i in tmp.iter() { x.write_slice(i)?; }
					Ok(())
				}).map_err(|_|TbsTooBig)?;
			} else {
				try_buffer!(tbs.write_u16(0));
				tbs.vector_fn(TLS_LEN_SCT_CERT_LEN, |x|{
					//This is full cert, unlike others.
					x.write_slice(eecert)?;
					Ok(())
				}).map_err(|_|TbsTooBig)?;
			}
			try_buffer!(tbs.write_u16(extensions.len() as u16));
			try_buffer!(tbs.write_slice(extensions));
		},
		2 => {
			try_buffer!(tbs.write_u8(if is_precert { 2 } else { 1 }));
			try_buffer!(tbs.write_u64(ts));
			tbs.vector_fn(TLS_LEN_SCT2_LOGID, |x|{
				x.write_slice(issuer_key_hash)?;
				Ok(())
			}).map_err(|_|TbsTooBig)?;
			let mut delay_err = None;
			tbs.vector_fn(TLS_LEN_SCT2_CERT_LEN, |x|{
				if is_precert {
					let mut buf = [0u8; 15];
					let tmp = reconstruct_v2_precert(tbs_cert, &mut buf).map_err(|x|{
						delay_err = Some(x);
					})?;
					for i in tmp.iter() { x.write_slice(i)?; }
				} else {
					//Just write the TBSCertificate as whole.
					x.write_slice(tbs_cert)?;
				}
				Ok(())
			}).map_err(|_|TbsTooBig)?;
			if let Some(err) = delay_err { fail!(err); }
			try_buffer!(tbs.write_u16(extensions.len() as u16));
			try_buffer!(tbs.write_slice(extensions));
		},
		_ => return Ok(false),		//Can't validate.
	}
	SignatureBlock::from3(signature).verify(&log.key, &tbs, 0).map_err(|_|SctError::VSctSignatureFailed)?;
	Ok(true)
}

///Validate a list of SCTs.
///
///# Parameters:
///
/// * `eecert`: The EE certificate to validate against.
/// * `issuer_key`: The key of EE certificate issuer.
/// * `staple_scts`: Vector of stapled SCTs. The boolean is precertificate flag (false if not a precertificate,
///true if precertificate) and the vector is raw SCT data. On success, this is cleared.
/// * `require_sct_unsat`: If at least one SCT was successfully validated, this is forced to false.
/// * `validated_one`: If at least one SCT was successfully validated, this is forced to true.
/// * `trusted_logs`: List of trusted logs.
/// * `debugger`: The debugger interface.
///
///# Failures:
///
/// * If any of the SCTs fails to validate (as opposed to just being unvalidatable), fails with appropriate error.
pub fn validate_scts(eecert: &[u8], issuer_key: &[u8], staple_scts: &mut Vec<(bool, Vec<u8>)>,
	require_sct_unsat: &mut bool, validated_one: &mut bool, trusted_logs: &[TrustedLog],
	debugger: Option<(u64, Arc<Box<Logging+Send+Sync>>)>) -> Result<(), SctError>
{
	let mut at_least_one = false;
	{
		//Build the log id maps.
		let mut v1_map = BTreeMap::<[u8;32], &TrustedLog>::new();
		let mut v2_map = BTreeMap::<&[u8], &TrustedLog>::new();
		let mut checked_id = BTreeMap::<(u32, &[u8]), usize>::new();
		for i in trusted_logs.iter() {
			let mut key = [0u8;32];
			let h = ChecksumFunction::Sha256.calculate(&i.key).map_err(|x|SctError::HashFailure(x))?;
			//SHA-256 is 32 bytes.
			fail_if!(h.as_ref().len() != key.len(), SctError::Sha256IsNot32Bytes);
			key.copy_from_slice(h.as_ref());
			v1_map.insert(key, i);
			if i.v2_id.len() > 0 {
				v2_map.insert(&i.v2_id, i);
			}
		}

		let mut sct_idx = 0usize;
		for i in staple_scts.iter() {
			let mut _sct_idx = sct_idx;
			sct_idx = sct_idx.saturating_add(1);
			let mut sct = Source::new(&i.1);
			//The first byte of SCT is the type byte.
			let stype = sct.read_u8(SctError::VSctEmpty)?;
			let (version, preflag) = match stype {
				0 => (1, i.0),
				3 if i.0 == false => (2, false),
				4 if i.0 == true => (2, true),
				_ => {
					debug!(HANDSHAKE_EVENTS debugger, "SCT#{} has unknown SCT type {}",
						_sct_idx, stype);
					continue		//Ignore unknown SCTs.
				},
			};
			let log = match version {
				1 => {
					let id = sct.read_slice(32, SctError::VSctNoLogId)?;
					//Check for duplicate.
					if let Some(dup_idx) = checked_id.get(&(1, id)) {
						debug!(HANDSHAKE_EVENTS debugger, "SCT#{} is duplicate of SCT#{}",
							_sct_idx, dup_idx);
						continue;
					};
					checked_id.insert((1, id), _sct_idx);
					//Find log.
					match v1_map.get(id) {
						Some(x) => x,
						None => {
							debug!(HANDSHAKE_EVENTS debugger, "SCT#{} is from unknown \
								v1 log {}", _sct_idx, DumpAsHex(id));
							continue	//Unknown log.
						}
					}
				},
				2 => {
					let id = sct.read_slice(2..127, SctError::VSctNoLogId)?;
					//Check for duplicate.
					if let Some(dup_idx) = checked_id.get(&(2, id)) {
						debug!(HANDSHAKE_EVENTS debugger, "SCT#{} is duplicate of SCT#{}",
							_sct_idx, dup_idx);
						continue;
					};
					checked_id.insert((2, id), _sct_idx);
					//Find log.
					match v2_map.get(id) {
						Some(x) => x,
						None => {
							debug!(HANDSHAKE_EVENTS debugger, "SCT#{} is from unknown \
								v2 log {}", _sct_idx, TranslateAsn1Oid(id));
							continue	//Unknown log.
						}
					}
				}
				_ => continue,		//Can't happen.
			};
			let timestamp = sct.read_u64(SctError::VSctNoTimestamp)?;
			let extensions = sct.read_slice(TLS_LEN_SCT_EXTENSIONS, SctError::VSctNoExtensions)?;
			let signature = match DigitallySigned::parse(&mut sct) {
				Ok(x) => x,
				Err(DSRE::CantParse) => fail!(SctError::VSctNoSignature),
				Err(DSRE::BogusAlgorithm(_)) => continue,	//Unvalidable.
			};
			let status = validate_sct(eecert, issuer_key, log, timestamp, extensions, signature,
				preflag, version)?;
			if status {
				debug!(HANDSHAKE_EVENTS debugger, "Got valid SCT#{} from '{}'", _sct_idx, log.name);
				at_least_one = true;
			}
		}
	}
	staple_scts.clear();
	if at_least_one { *require_sct_unsat = false; }		//SCT received.
	if at_least_one { *validated_one = true; }		//SCT received.
	Ok(())
}

struct DumpAsHex<'a>(&'a [u8]);

impl<'a> Display for DumpAsHex<'a>
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		for i in self.0 { fmt.write_fmt(format_args!("{:02x}", i))?; }
		Ok(())
	}
}

#[test]
fn idp_aalto_fi_precert_reconstruct()
{
	let cert = include_bytes!("idp.aalto.fi.der");
	let precert = include_bytes!("idp.aalto.fi.der.precert");
	let mut scratch = [0u8; 15];
	let tbs_cert = extract_tbs_certificate(&cert[..]).unwrap();
	let pieces = reconstruct_v1_precert(tbs_cert, &mut scratch).unwrap();
	let mut reconstructed: Vec<u8> = Vec::new();
	for i in pieces.iter() { reconstructed.extend(*i); }
	assert_eq!(&reconstructed, &&precert[..]);
}
