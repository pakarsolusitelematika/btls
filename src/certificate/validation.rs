use super::{CertificateError, CertificateIssuer, CertificateSubject, CFLAG_MUST_CT, CFLAG_MUST_EMS,
	CFLAG_MUST_RENEGO, CFLAG_MUST_STAPLE, CFLAG_MUST_TLS13, CFLAG_UNKNOWN, DecodeTsForUser, extract_spki,
	name_in_set2, ParsedCertificate};
use ::client_certificates::HostSpecificPin;
use ::stdlib::{BTreeMap, Deref, Display, FmtError, Formatter, min, Range, String, ToOwned, Vec};

use btls_aux_assert::AssertFailed;
use btls_aux_hash::HashFunction;
use btls_aux_serialization::Source;
use btls_aux_signature_algo::WrappedRecognizedSignatureAlgorithm;
use btls_aux_signature::{check_sig_algorithm_supported, SignatureBlock};
use btls_aux_time::{TimeInterface, Timestamp};

///Variant for the TLS certificate error classification.
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub enum MajorCertProblem
{
	///TLS alert bad_certificate
	Bad,
	///TLS alert unsupported_certificate
	Unsupported,
	///TLS alert certificate_expired
	Expired,
	///TLS alert bad_certificate_status_response
	Ocsp,
	///TLS alert certificate_unknown
	Unspecified,
	///TLS alert unknown_ca
	UnknownCa,
	///TLS alert internal_error
	InternalError,
}

///Error in validating certificate.
#[derive(Clone,Debug,PartialEq,Eq)]
pub enum CertificateValidationError
{
	///Assertion failed!
	AssertionFailed(AssertFailed),
	///Conflicting keys.
	ConflictingKeys,
	///Error extracting SPKI keys.
	EeSpkiExtraction(CertificateError),
	///Path length constraint violated.
	PathLenViolation,
	///OCSP stapling is required.
	OcspStapleRequred,
	///SCT stapling is required.
	SctStapleRequred,
	///EMS support is required.
	EmsRequired,
	///TLS 1.3 is required.
	Tls13Required,
	///Secure renegotiation is required.
	SecureRenegoRequired,
	///Certificate requires unknown features.
	RequireUnknownFeatures,
	///Certificate has unmet feature requirements.
	UnmetFeatures(u32),
	///Error parsing CA certificate.
	CaCertParseError(usize, CertificateError),
	///CA did not issue EE certificate.
	DidNotIssueEeCert(usize),
	///CA key is kill-listed.
	CaKillisted(usize),
	///CA keys collide with EE keys.
	CaCantHaveEeKeys(usize),
	///CA signature algorithm mismatch.
	CaSignatureMismatch(usize),
	///CA not yet valid.
	CaNotYetValid(usize, Timestamp),
	///CA expired.
	CaExpired(usize, Timestamp),
	///CA is not marked as a CA.
	CaIsNotCa(usize),
	///CA certificate not valid for TLS.
	CaNotValidForTls(usize),
	///CA certificate is superfluous.
	CaSuperfluous(usize),
	///Name constraints violated.
	NameConstraintViolation(usize, String),
	///CA uses bad signature algorithm.
	CaBadSignatureAlgorithm(usize),
	///Error parsing EE certificate.
	EeCertParseError(CertificateError),
	///EE key is kill-listed.
	EeKillisted,
	///EE signature algorithm mismatch.
	EeSignatureMismatch,
	///EE certificate not valid for TLS.
	EeNotValidForTls,
	///EE certificate is a CA.
	EeCertificateCa,
	///EE certificate not valid for name.
	EeNotValidForName(String, String),
	///EE certificate not yet valid.
	EeNotYetValid(Timestamp),
	///EE certificate expired.
	EeExpired(Timestamp),
	///EE certificate is self-signed.
	SelfSigned,
	///EE certificate has bad signature algorithm.
	EeBadSignatureAlgorithm,
	///No EE issuer certificate found.
	NoIssuerCert,
	///No certificates match pins.
	NoCertifcatesMatchPins,
	///Certificate not signed by any known CA.
	CaUnknown,
	///Signature verification failed.
	SignatureFailure(usize),
	///Public key mismatch against provoded one.
	PublicKeyMismatch,
	///Looping certificate chain.
	LoopingCertificateChain(usize),
	#[doc(hidden)]
	Hidden__
}

impl CertificateValidationError
{
	///Get the problem class.
	///
	/// # Parameters:
	///
	/// * `self`: The validation error.
	///
	/// # Returns:
	///
	/// The major problem class.
	pub fn problem_class(&self) -> MajorCertProblem
	{
		use self::CertificateValidationError::*;
		use self::MajorCertProblem::*;
		match self {
			&AssertionFailed(_) => InternalError,
			&ConflictingKeys => Bad,
			&EeSpkiExtraction(_) => Bad,
			&PathLenViolation => Bad,
			&OcspStapleRequred => Ocsp,
			&SctStapleRequred => Ocsp,
			&EmsRequired => Bad,
			&Tls13Required => Bad,
			&SecureRenegoRequired => Bad,
			&RequireUnknownFeatures => Unsupported,
			&UnmetFeatures(_) => Unsupported,
			&CaCertParseError(_, _) => Unsupported,
			&DidNotIssueEeCert(_) => Bad,
			&CaKillisted(_) => UnknownCa,
			&CaCantHaveEeKeys(_) => Bad,
			&CaSignatureMismatch(_) => Bad,
			&CaNotYetValid(_, _) => Expired,
			&CaExpired(_, _) => Expired,
			&CaIsNotCa(_) => Bad,
			&CaNotValidForTls(_) => Bad,
			&CaSuperfluous(_) => Unsupported,
			&NameConstraintViolation(_, _) => Bad,
			&CaBadSignatureAlgorithm(_) => Unsupported,
			&EeCertParseError(_) => Unsupported,
			&EeKillisted => UnknownCa,
			&EeSignatureMismatch => Bad,
			&EeNotValidForTls => Bad,
			&EeCertificateCa => Bad,
			&EeNotValidForName(_, _) => Bad,
			&EeNotYetValid(_) => Expired,
			&EeExpired(_) => Expired,
			&SelfSigned => Unsupported,
			&EeBadSignatureAlgorithm => Unsupported,
			&NoIssuerCert => Unsupported,
			&NoCertifcatesMatchPins => UnknownCa,
			&CaUnknown => UnknownCa,
			&SignatureFailure(_) => Bad,
			&PublicKeyMismatch => Bad,	//Should never happen.
			&LoopingCertificateChain(_) => Unsupported,
			&Hidden__ => Unspecified,	//Should never happen.
		}
	}
}

impl From<AssertFailed> for CertificateValidationError
{
	fn from(x: AssertFailed) -> CertificateValidationError
	{
		CertificateValidationError::AssertionFailed(x)
	}
}

impl Display for CertificateValidationError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::CertificateValidationError::*;
		match self {
			&AssertionFailed(ref x) => fmt.write_fmt(format_args!("Assertion failed: {}", x)),
			&ConflictingKeys => fmt.write_str("Certificate chain contains conflicting public keys"),
			&EeSpkiExtraction(ref x) => fmt.write_fmt(format_args!("Extracting EE SPKI: {}", x)),
			&PathLenViolation => fmt.write_str("Path length constraints violated"),
			&OcspStapleRequred => fmt.write_str("Certificate requires OCSP stapling"),
			&SctStapleRequred => fmt.write_str("Certificate requires SCT stapling"),
			&EmsRequired => fmt.write_str("Certificate requires EMS support"),
			&Tls13Required => fmt.write_str("Certificate requires TLS 1.3 support"),
			&SecureRenegoRequired => fmt.write_str("Certificate requires secure renegotiation support"),
			&RequireUnknownFeatures => fmt.write_str("Certificate requires unknown features"),
			&UnmetFeatures(x) => fmt.write_fmt(format_args!("Certificate has unknown unmet feature \
				flags {:x}", x)),
			&CaCertParseError(x, ref y) => fmt.write_fmt(format_args!("Error parsing CA#{}: {}",
				x, y)),
			&DidNotIssueEeCert(x) => fmt.write_fmt(format_args!("CA#{} did not issue the EE cert", x)),
			&CaKillisted(x) => fmt.write_fmt(format_args!("CA#{} is explicitly killisted", x)),
			&CaCantHaveEeKeys(x) => fmt.write_fmt(format_args!("CA#{} shares public key with EE cert",
				x)),
			&CaSignatureMismatch(x) => fmt.write_fmt(format_args!("CA#{} signature algorithms mismatch",
				x)),
			&CaNotYetValid(x, y) => fmt.write_fmt(format_args!("CA#{} not yet valid (begins at {})", x,
				DecodeTsForUser(y, true))),
			&CaExpired(x, y) => fmt.write_fmt(format_args!("CA#{} expired (expired at {})", x,
				DecodeTsForUser(y, true))),
			&CaIsNotCa(x) => fmt.write_fmt(format_args!("CA#{} is not a CA", x)),
			&CaNotValidForTls(x) => fmt.write_fmt(format_args!("CA#{} can not issue TLS server certs",
				x)),
			&CaSuperfluous(x) => fmt.write_fmt(format_args!("CA#{} is superfluous", x)),
			&NameConstraintViolation(x, ref y) => fmt.write_fmt(format_args!("CA#{} name constraints \
				don't allow '{}'", x, y)),
			&CaBadSignatureAlgorithm(x) => fmt.write_fmt(format_args!("CA#{} uses unacceptable \
				signature algorithm", x)),
			&EeCertParseError(ref y) => fmt.write_fmt(format_args!("Error parsing EE certificate: {}",
				y)),
			&EeKillisted => fmt.write_str("EE certificate is explicitly killisted"),
			&EeSignatureMismatch => fmt.write_str("EE Certificate signature algorithms mismatch"),
			&EeNotValidForTls => fmt.write_str("EE Certificate is not valid for TLS server certificate"),
			&EeCertificateCa => fmt.write_str("EE certificate can not be CA certificate"),
			&EeNotValidForName(ref x, ref y) => fmt.write_fmt(format_args!("EE Certificate not valid \
				for name '{}' (got: {})", x, y)),
			&EeNotYetValid(x) => fmt.write_fmt(format_args!("EE certificate not yet valid (begins at \
				{})", DecodeTsForUser(x, true))),
			&EeExpired(x) => fmt.write_fmt(format_args!("EE Certificate expired (expired at {})",
				DecodeTsForUser(x, true))),
			&SelfSigned => fmt.write_str("Self-signed EE certificates are not allowed"),
			&EeBadSignatureAlgorithm => fmt.write_str("EE Certificate uses unacceptable signature \
				algorithm"),
			&NoIssuerCert => fmt.write_str("Issuer certificate missing"),
			&NoCertifcatesMatchPins => fmt.write_str("No certificates matching given pins found"),
			&CaUnknown => fmt.write_str("Certificate issued by unknown CA"),
			&SignatureFailure(x) => fmt.write_fmt(format_args!("Signature on certifcate #{} doesn't \
				verify", x)),
			&PublicKeyMismatch => fmt.write_str("Certificate public key does not match keypair"),
			&LoopingCertificateChain(x) => fmt.write_fmt(format_args!("CA Certificate #{} forms a \
				apparent looping chain", x)),
			&Hidden__ => fmt.write_str("Hidden__"),
		}
	}
}

struct TrustPathEntry<T:Sized>
{
	//The index of representative (first certificate for the same subject.
	representative: usize,
	//Link from this certificate is OK?
	link_ok: bool,
	//Representative of the issuer.
	issuer_representative: usize,
	//More data.
	more: T,
}

//Build a trust path.
//
// * vertices: Information about the vertices available for trust paths. Only the base entries are used, the
//             more fields don't matter.
// * pin_set: Set of pinned certficates (if empty, assume all certificates are pinned).
// * ta_set: Set of trust anchor certificates.
// * scratch: Scratch memory. Must have number of entries at least 7 times to that of vertices. The memory for
//            returned path is sliced off this buffer.
//
//Returns array of node indices on trust path (at least 1), or () on failure if trust path can't be built.
fn build_trust_path2<'a, T:Sized>(vertices: &[TrustPathEntry<T>], pin_set: &[usize], ta_set: &[usize],
	scratch_memory: &'a mut [usize]) -> Result<&'a [usize], ()>
{
	//The scratch memory needs to be at least 7 times that of vertices. The scratch memory is organized into
	//6 subarrays:
	//- path
	//- path2 (alternate path)
	//- flags
	//- queue
	//- rlist
	//- pathbuf (2x normal in size).
	fail_if!(scratch_memory.len() / 7 < vertices.len(), ());
	let (path, scratch_memory) = scratch_memory.split_at_mut(vertices.len());
	let (path2, scratch_memory) = scratch_memory.split_at_mut(vertices.len());
	let (flags, scratch_memory) = scratch_memory.split_at_mut(vertices.len());
	let (queue, scratch_memory) = scratch_memory.split_at_mut(vertices.len());
	let (rlist, pathbuf) = scratch_memory.split_at_mut(vertices.len());
	const FLAG_IN_QUEUE: usize = 1;
	const FLAG_TRUST_ANCHOR: usize = 2;
	const FLAG_PINNED: usize = 4;
	const FLAG_REPRESENTATIVE: usize = 8;
	const FLAG_IN_PATH: usize = 16;
	const FLAG_FORK: usize = 32;
	let invalid_node = usize::max_value();
	for i in 0..vertices.len() {
		//0 <= i < scratch.len() by loop condition, and thus i is valid index.
		path[i] = invalid_node;
		path2[i] = invalid_node;
		flags[i] = 0;
		queue[i] = 0;
		rlist[i] = 0;
	}
	//There must be at least one vertex.
	fail_if!(vertices.len() == 0, ());
	//qfirst and qlast are always in bounds for vertex index, and thus also scratch index.
	let mut qfirst = 0;
	let mut qlast = 0;
	//Set representative flags for those vertices that are representatives.
	for i in vertices.iter().enumerate() {
		if  i.1.representative == i.0 {
			//0 <= i.0 < vertices.len() <= scratch.len(), and thus i.0 is valid scratch index.
			flags[i.0] |= FLAG_REPRESENTATIVE;
		}
	}
	//- The representative must be the first element in equivalence releation equivalence class. From this
	//  it follows thar representatives must be valid vertex indices and thus valid scratch indices.
	//- 0 must be in its own equivalence class, which can't contain any other vertices.
	//- Issuer representatives must be valid representatives and must be valid vertex indices and thus valid
	//  scratch indices.
	for i in vertices.iter().enumerate() {
		fail_if!(i.1.representative > i.0, ());
		fail_if!(i.1.representative == 0 && i.0 != 0, ());
		fail_if!(i.1.issuer_representative >= vertices.len(), ());
		//0 <= i.1.representative <= i.0 < vertices.len() <= scratch.len(), thus this is valid scratch index.
		fail_if!(flags[i.1.representative] & FLAG_REPRESENTATIVE == 0, ());
		//0 <= i.1.issuer_representative < vertices.len() <= scratch.len(), thus this is valid scratch index.
		fail_if!(flags[i.1.issuer_representative] & FLAG_REPRESENTATIVE == 0, ());
	}

	//All TAs and Pins must be valid vertex indices.
	for i in ta_set.iter() { fail_if!(*i >= vertices.len(), ()); }
	for i in pin_set.iter() { fail_if!(*i >= vertices.len(), ()); }
	//Build circular lists for equivalence classes.
	for i in 0..vertices.len() {
		//0 <= i < vertices.len() and thus i is valid vertex index.
		let rep = vertices[i].representative;
		//0 <= i < vertices.len() <= scratch.len() and thus i is valid scratch index.
		//0 <= rep <= i < vertices.len() <= scratch.len() and thus rep is a valid scratch index. That
		//0 <= rep <= i follows from check on representatives above.
		//
		//This maintains the invariant that all next indices are valid vertex indices, because the only
		//possible values a loop iteration can leave in rlist[i] and rlist[rep] are the values past iterations
		//have left, which are 0 <= x < i < vertices.len() or which are 0 <= i < vertices.len().  In case
		//rep == i, the value left will be 0 <= i < vertices.len().
		//
		//The purpose here is to insert rep as next node after i (which is the representative of the class).
		//Note that 1) this works properly if i==rep, writing rlist[i]=i, as desired for the class
		//representative. 2) This keeps the list as circular list.
		rlist[i] = rlist[rep];			//Next after this is next after rep.
		rlist[rep] = i;				//Insert link.
	}
	//Add sets.
	if pin_set.len() > 0 {
		for i in pin_set.iter() {
			//The pin indices were checked to be < vertex.len() <= scratch.len() and thus valid
			//scratch indices.
			flags[*i] |= FLAG_PINNED;
		}
	} else {
		//If no pins, assume ALL pinned.
		for i in flags.iter_mut() { *i |= FLAG_PINNED; }
	}
	for i in ta_set.iter() {
		//The trust anchor indices were checked to be < vertex.len() <= scratch.len() and thus valid
		//scratch indices.
		flags[*i] |= FLAG_TRUST_ANCHOR;
	}

	//Start chain-building from node 0. This node can't have other representations, and 0 <= 0 < vertices.len()
	//< scratch.len() and thus 0 is valid scratch index.
	path[0] = 0;
	flags[0] |= FLAG_IN_QUEUE;

	//qlast is always valid scratch index. Thus qlast + 1 is at least 1 and at most scratch.len() and thus can
	//be represented. Thus the result of modulo with vertices.len() (which is always nonzero) is at least 0 and
	//always less than vertices.len() <= scratch.len(). Thus the new value fo qlast is a valid vertex index
	//and thus scratch index. Also, since 0 <= 0 < vertices.len(), the written 0 is valid vertex index.
	queue[qlast] = 0;
	qlast = (qlast + 1) % vertices.len();

	//This is always true if there is something in the queue, because vertex#0 can be in queue only on the
	//very first round, since it is only added above, and no other round can add it.
	while qfirst != qlast {
		//qfirst is always valid scratch index. Thus qfirst + 1 is at least 1 and at most scratch.len() and
		//thus can be represented. Thus the result of modulo with vertices.len() (which is always nonzero)
		//is at least 0 and always less than vertices.len() <= scratch.len(). Thus the new value fo qfirst
		//is a valid vertex index and thus scratch index. And since values in queue are always valid vertex
		//indices, the value of investigate will be valid vertex index and thus valid scratch index.
		let investigate = queue[qfirst];
		qfirst = (qfirst + 1) % vertices.len();

		//We popped investigate off the queue (it was in queue, and thus is a valid vertex index and thus
		//valid scratch index. Mark it no longer being in queue, so it can be re-inserted later if needed.
		flags[investigate] &= !FLAG_IN_QUEUE;
		//If the link off vertex to investigate is not ok, ignore the vertex and don't build further off it.
		//investigate is valid vertex index since it was in the queue. Note that this does not prevent
		//building into this vertex, since path is set before vertex was added to list. Also, self-signed
		//nodes (those with representative equal to issuer representative are never followed. Note that
		//vertex can have differening representatives but link broken.
		if !vertices[investigate].link_ok || vertices[investigate].issuer_representative ==
			vertices[investigate].representative { continue; }
		//investigate is off queue and thus valid vertex index. Grab the issuer representative for this
		//vertex, so we can loop over all instances of that representative.
		let issuer_idx = vertices[investigate].issuer_representative;
		//Invariant: target is valid vertex index and scratch index. The initial value is off
		//issuer_representative of an vertex, and thus valid vertex index (and thus valid scratch index).
		let mut target = issuer_idx;
		loop {
			//We only update path if there is no existing one, or this path has a pin, but
			//the known one doesn't. By invariants above both target and investigate are valid vertex
			//and thus scratch indices.
			if path[target] == invalid_node || (flags[target] & FLAG_PINNED == 0 &&
				flags[investigate] & FLAG_PINNED != 0) {
				//Link target into this path. Both target and investigate are valid vertex and
				//scratch indices by invariants above. The new node becomes path-pinned if it was
				//already, or if this node is path-pinned. If we are pinning the node, we save the
				//former path, so we can build the loop properly.
				if path[target] != invalid_node && path[target] != investigate {
					//This vertex must have be gotten pinned and was relinked. Save the former
					//path. And mark this as fork. Vertex can never be visited more than twice,
					//because the only way to revisit it sets the pinned flag, which is also
					//required for revisit, so this ever happens once.
					flags[target] |= FLAG_FORK;
					path2[target] = path[target];
				}
				path[target] = investigate;
				flags[target] |= flags[investigate] & FLAG_PINNED;
				if flags[target] & FLAG_IN_QUEUE == 0 {
					//Target isn't in queue yet, add it. By invariants above, target is valid
					//vertex/scratch index.
					//qlast is always valid scratch index. Thus qlast + 1 is at least 1 and at
					//most scratch.len() and thus can be represented. Thus the result of modulo
					//with vertices.len() (which is always nonzero) is at least 0 and always
					//less than vertices.len() <= scratch.len(). Thus the new value fo qlast is
					//a valid vertex index and thus scratch index.
					flags[target] |= FLAG_IN_QUEUE;
					queue[qlast] = target;
					qlast = (qlast + 1) % vertices.len();
				}
			}
			//By invariants above, rlist values are always valid vertex/scratch indices, and thus the
			//invariant that target will be one is kept.
			target = rlist[target];
			//If we walk back to representative, we have completed full round across list represeted
			//vertices and can move to next one.
			if target == issuer_idx { break; }
		}
	}

	//Now, check all TAs. If we have a valid path with pinned flag, retrun that.
	for i in ta_set.iter() {
		let mut pathidx = 0usize;
		//*i is index in ta set, and we checked all are valid vertex and thus scratch indices above.
		//Find a TA that is part of a path and transitively pinned.
		if path[*i] != invalid_node && flags[*i] & FLAG_PINNED != 0 {
			//Invariant: nidx is valid vertex index (and thus scratch index).
			let mut nidx = *i;
			*pathbuf.get_mut(pathidx).ok_or(())? = nidx;
			pathidx = pathidx.checked_add(1).ok_or(())?;
			//The paths constructed above always start at 0, and at are acyclic if path is followed only
			//once for each fork.
			while nidx != 0 {
				//Path of connected vertex points to valid vertex. We ever visit vertices twice,
				//and paths can only be updated on visit, so first following path and then path2
				//(if distinct, that is signaled by forked flag) will backtrace our steps, eventually
				//finding way back to root node 0. We have to trace the loop, or we won't validate
				//the signatures required to prove that the pin is valid.
				let forked_already = FLAG_IN_PATH|FLAG_FORK;
				nidx = if flags[nidx] & forked_already == forked_already {
					path2[nidx]
				} else {
					//Mark this visited once, so we can follow alternate path on revisit.
					flags[nidx] |= FLAG_IN_PATH;
					path[nidx]
				};
				*pathbuf.get_mut(pathidx).ok_or(())? = nidx;
				pathidx = pathidx.checked_add(1).ok_or(())?;
			}
			//pathidx must be at most the number of elements in pathbuf, as all stores up to pathidx-1
			//were successful. But we need to reverse pathbuf, as it needs to start at 0, not end at 0.
			//Also, pathidx is at least 1 here, as the first add is unconditionally there.
			{
				let half = pathidx/2;
				for i in 0..half {
					//0 <= i < pathidx/2, so in-range for path buffer. And pathidx-1-i is
					//pathidx-1>=pathidx-1-i>=pathidx-1-pathidx/2>=pathidx/2-1, so in-range.
					let j = pathidx-1-i;
					pathbuf[i] ^= pathbuf[j];
					pathbuf[j] ^= pathbuf[i];
					pathbuf[i] ^= pathbuf[j];
				}
			}
			return Ok(&pathbuf[..pathidx]);
		}
	}
	//We would have found a path if it existed, but didn't, so there is no path.
	Err(())
}

//min function, where None means infinite.
fn min_inf(a: Option<u64>, b: Option<u64>) -> Option<u64>
{
	match (a, b) {
		(Some(_a), Some(_b)) => Some(min(_a, _b)),
		(Some(_a), None) => Some(_a),
		(None, Some(_b)) => Some(_b),
		(None, None) => None,
	}
}

//Is signature algorithm known?
fn known_signature_algorithm<'a>(algo: SignatureBlock<'a>) -> bool
{
	WrappedRecognizedSignatureAlgorithm::from(algo.algorithm).map(|x|x.unwrap_if_allowed(0).ok()).is_some()
}

//Is signature algorithm allowed?
fn allowed_signature_algorithm<'a>(algo: SignatureBlock<'a>) -> bool
{
	check_sig_algorithm_supported(algo.algorithm, 0)
}

///A trust anchor
#[derive(Clone)]
pub struct TrustAnchorValue
{
	///The backing memory.
	backing: Vec<u8>,
	///The SubjectPublicKeyInfo range for the trust anchor. This includes the leading SEQUENCE header.
	spki: Range<usize>,
	///The set of allowed names. If any names are present here, only these names are allowed. All subnames
	///of given name are allowed.
	allowed_names: Range<usize>,
	///The set of disallowed names. If name is present in both allowed_names and disallowed_names, then
	///disallowed_names takes precendence. All subnames of given name are disallowed.
	disallowed_names: Range<usize>,
}

fn check_range(backing: &[u8], range: &Range<usize>) -> Result<Range<usize>, ()>
{
	fail_if!(range.end < range.start, ());
	fail_if!(range.end > backing.len(), ());
	fail_if!(range.start > backing.len(), ());
	Ok(range.clone())
}

impl TrustAnchorValue
{
	///Create a new TrustAnchorValue.
	pub fn new(backing: Vec<u8>, spki: Range<usize>, allowed_names: Range<usize>,
		disallowed_names: Range<usize>) -> TrustAnchorValue
	{
		TrustAnchorValue{
			backing: backing,
			spki: spki,
			allowed_names: allowed_names,
			disallowed_names: disallowed_names,
		}
	}
	///Get the SPKI, or None if invalid.
	pub fn get_spki<'a>(&'a self) -> Option<&'a [u8]>
	{
		check_range(&self.backing, &self.spki).map(|x|&self.backing[x]).ok()
	}
	///Get the allowed names, or None if invalid.
	pub fn get_allowed_names<'a>(&'a self) -> Option<&'a [u8]>
	{
		check_range(&self.backing, &self.allowed_names).map(|x|&self.backing[x]).ok()
	}
	///Get the disallowed names, or None if invalid.
	pub fn get_disallowed_names<'a>(&'a self) -> Option<&'a [u8]>
	{
		check_range(&self.backing, &self.disallowed_names).map(|x|&self.backing[x]).ok()
	}
}

//Check if name matches given postfix.
fn name_matches_postfix(name: &[u8], postfix: &[u8]) -> bool
{
	//If postfix begins with a '.', strip that (. is always 1 byte).
	let postfix = if postfix.starts_with(b".") { &postfix[1..] } else { postfix };
	//If the raw string postfixes don't match, the name just won't match.
	if !name.ends_with(postfix) { return false; }
	//Check trivial match.
	if name.len() == postfix.len() { return true; }
	//Now, the character just before postfix has to be '.'. Begnning of string would be valid too, but we
	//handled that above. Also, we know name is strictly longer than postfix. But to handle unicode correctly,
	//we need to index the string as bytes, not characters.
	let start_pos = name.len() - postfix.len() - 1;
	name[start_pos] == 46
}

//Check name constraints. If allow exists, the name must be on that. And the name must not be on disallow list.
fn check_name_constraints<'a,T1:Iterator<Item=&'a [u8]>+Clone,T2:Iterator<Item=&'a [u8]>+Clone>(name: &[u8],
	allow: T1, disallow: T2) -> bool
{
	if allow.clone().next().is_some() {
		if allow.filter(|x|name_matches_postfix(name, x)).next().is_none() {
			return false;
		}
	}
	if disallow.filter(|x|name_matches_postfix(name, x)).next().is_some() {
		return false;
	}
	true
}

//This is a macro due to lifetime issues...
macro_rules! add_key {
	($map:expr; $name:expr, $key:expr) => {
		if $map.contains_key(&$name) {
			let spki = $map.get(&$name).ok_or(assert_failure!("Bad key map (contains_key, but no \
				entry to get)"))? as &[u8];
			fail_if!(spki != $key, ConflictingKeys);
		} else {
			$map.insert($name, $key);
		};
	}
}

//Do checks on EE certificate.
//
//The following checks are done:
// - The two signature algorithms in certificate match up.
// - The certificate is valid for TLS server authentication (not AnyEku!)
// - The certificate is not CA certificate.
// - The certificate is in its validity period.
// - The certificate is not self-signed.
// - The certificate signature algorithm is trusted (if strict_algo=true).
// - icert_count > 0 (if strict_algo=true).
//
//It also does the follows:
//
// - Sets bits corresponding to TLS features required in feature_flags_present.
// - Adds the issuer to issuers_seen.
// - Adds the (subject,key) to keys_map, checking that there is no existing conflicting entry.
//
//It most notably does NOT check:
// - That the EE key is not kill-listed.
// - That the SAN in certificate matches the reference name.
//
fn ee_certificate_checks<'b>(eecert: &ParsedCertificate<'b>, time_now: &TimeInterface, strict_algo: bool,
	icert_count: usize, feature_flags_present: &mut u32, issuers_seen: &mut BTreeMap<CertificateIssuer<'b>, ()>,
	keys_map: &mut BTreeMap<CertificateSubject<'b>, &'b [u8]>, allow_not_yet_valid: bool, is_client: bool) ->
	Result<(), CertificateValidationError>
{
	use self::CertificateValidationError::*;

	let now = time_now.get_time();
	//The signature algorithms of EE cert must match up.
	fail_if!(Some(eecert.sig_algo1) != eecert.signature.algorithm.unwrap_x509(), EeSignatureMismatch);
	//The certificate needs to have appropriate EKU.
	if is_client {
		fail_if!(!eecert.use_tls_client, EeNotValidForTls);
	} else {
		fail_if!(!eecert.use_tls_serv, EeNotValidForTls);
	}
	//The certificate must not be a CA.
	fail_if!(eecert.is_ca, EeCertificateCa);
	//The EE cert must be in validity window.
	fail_if!(!allow_not_yet_valid && now < eecert.not_before, EeNotYetValid(eecert.not_before));
	fail_if!(now > eecert.not_after, EeExpired(eecert.not_after));
	//Self-signeds are NOT allowed. Note that if EE SPKI is trusted, the entiere certificate check will be
	//skipped. Also, if RPK is used on server, this sanity check will be skipped.
	//This holds even for client certificates. Note that if we fail validation of client certificate, that
	//just withholds the name, it does not distrupt the connection.
	fail_if!(eecert.issuer.same_as(eecert.subject), SelfSigned);
	//If the EE cert is not self-signed, it must have acceptable signature algorithm (if checked) and its
	//issuer certificate must be in chain.
	fail_if!(strict_algo && !known_signature_algorithm(eecert.signature), EeBadSignatureAlgorithm);
	fail_if!(icert_count == 0 && strict_algo, NoIssuerCert);
	*feature_flags_present |= eecert.req_flags;
	//Add the issuer and key for the EE certificate.
	issuers_seen.insert(eecert.issuer, ());
	add_key!(keys_map; eecert.subject, eecert.pubkey);
	Ok(())
}

//Do checks on CA certificate.
//
//The following checks are done:
// - The two signature algorithms in certificate match up.
// - The certificate is valid for TLS server authentication or AnyEku.
// - The certificate is a CA certificate.
// - The certificate is in its validity period.
// - The certificate signature algorithm is trusted if not self-signed (if strict_algo=true).
// - The first CA entry (index=0) is the issuer of the EE certificate.
// - All EE certificate SANs satisfy the name constraints.
// - Some earlier certificate has been signed by this certificate.
// - The issuer has not been seen as subject already (prevent looping "chains").
//
//It also does the follows:
//
// - Sets bits corresponding to TLS features required in feature_flags_present.
// - Adds the issuer to issuers_seen.
// - Adds the (subject,key) to keys_map, checking that there is no existing conflicting entry.
//
//It most notably does NOT check:
// - That the CA key is not kill-listed.
//
fn ca_certificate_checks<'b>(icert: ParsedCertificate<'b>, eecert: &ParsedCertificate<'b>, index: usize,
	time_now: &TimeInterface, strict_algo: bool, feature_flags_present: &mut u32,
	issuers_seen: &mut BTreeMap<CertificateIssuer<'b>, ()>,
	keys_map: &mut BTreeMap<CertificateSubject<'b>, &'b [u8]>, icerts: &mut Vec<ParsedCertificate<'b>>,
	allow_not_yet_valid: bool, is_client: bool) -> Result<(), CertificateValidationError>
{
	use self::CertificateValidationError::*;

	let now = time_now.get_time();
	//The feature flags accumulate through the chain.
	*feature_flags_present |= icert.req_flags;
	//CA#0 must issue EE.
	fail_if!(index == 0 && !eecert.issuer.same_as(icert.subject), DidNotIssueEeCert(index));
	//There must not be known key for issuer, if not self-signed.
	fail_if!(!icert.issuer.same_as(icert.subject) && keys_map.contains_key(&icert.issuer.as_subject()),
		LoopingCertificateChain(index));
	//CA must not share keys with EE cert.
	fail_if!(icert.pubkey == eecert.pubkey, CaCantHaveEeKeys(index));
	//The signature algorithms must be consistent.
	fail_if!(Some(icert.sig_algo1) != icert.signature.algorithm.unwrap_x509(), CaSignatureMismatch(index));
	//The certificate must be in validity window.
	fail_if!(!allow_not_yet_valid && now < icert.not_before, CaNotYetValid(index, icert.not_before));
	fail_if!(now > icert.not_after, CaExpired(index, icert.not_after));
	//The certificate must be a CA.
	fail_if!(!icert.is_ca, CaIsNotCa(index));
	//The certificate needs to have appropriate EKUs.
	if is_client {
		fail_if!(!icert.use_tls_client && !icert.use_any, CaNotValidForTls(index));
	} else {
		fail_if!(!icert.use_tls_serv && !icert.use_any, CaNotValidForTls(index));
	}
	//Name constraints must allow all names in EE cert.
	for j in eecert.dnsnames.iter() {
		fail_if!(!check_name_constraints(j, icert.names_allow.iter(), icert.names_disallow.iter()),
			NameConstraintViolation(index, String::from_utf8_lossy(j).into_owned()));
	}
	//If requested, check that signature algorithm is allowed. Note that self-signed certificates are always
	//ignored for this check.
	fail_if!(!icert.issuer.same_as(icert.subject) && strict_algo &&
		!known_signature_algorithm(icert.signature), CaBadSignatureAlgorithm(index));

	//There needs to be a certificate signed by this certificate.
	fail_if!(!issuers_seen.contains_key(&icert.subject.as_issuer()), CaSuperfluous(index));
	//The keys must be unique per issuers.
	add_key!(keys_map; icert.subject, icert.pubkey);
	//Insert issuer to list of issuers seen.
	issuers_seen.insert(icert.issuer, ());
	//Insert certificate to list of certificates.
	icerts.push(icert);
	//OK.
	Ok(())
}

//Hashes a key and checks if it is on specified killist.
fn key_on_killist(key: &[u8], killist: &BTreeMap<[u8; 32], ()>) -> bool
{
	let mut ctx = HashFunction::Sha256.make_context();
	ctx.input(key);
	//Assume key in killist if computing the key hash fails.
	let hash = match ctx.output() { Ok(x) => x, Err(_) => return true };
	let _hash = hash.as_ref();
	killist.contains_key(_hash)
}

//Check a set of names against name constraints. Return if all are allowed.
fn check_name_constraints2<'a,T0:Iterator<Item=&'a [u8]>+Clone,T1:Iterator<Item=&'a [u8]>+Clone,
	T2:Iterator<Item=&'a [u8]>+Clone>(names: T0, allow: T1, disallow: T2) -> bool
{
	for i in names {
		if !check_name_constraints(i, allow.clone(), disallow.clone()) { return false; }
	}
	true
}

#[derive(Clone)]
struct DecayVectorToSlice<'a>(Source<'a>);

impl<'a> Iterator for DecayVectorToSlice<'a>
{
	type Item=&'a [u8];
	fn next(&mut self) -> Option<&'a [u8]> {
		self.0.read_slice(0..65535, ()).ok()
	}
}

//Check if specified name is a valid trust anchor.
fn is_valid_trust_anchor<'a, T0:Iterator<Item=&'a [u8]>+Clone>(dnsnames: T0, name: &[u8],
	trust_anchors: &'a BTreeMap<Vec<u8>, TrustAnchorValue>, killist: &BTreeMap<[u8; 32], ()>) -> bool
{
	match trust_anchors.get(name) {
		Some(x) => {
			let spki = match x.get_spki() { Some(y) => y, None => return false};
			let allowed_names = match x.get_allowed_names() { Some(y) => y, None => return false};
			let disallowed_names = match x.get_disallowed_names() { Some(y) => y, None => return false};
			!key_on_killist(spki, killist) && check_name_constraints2(dnsnames, DecayVectorToSlice(
				Source::new(allowed_names)), DecayVectorToSlice(Source::new(disallowed_names)))
		}
		None => false
	}
}

struct CertificateEntry<'a>
{
	//Key for this certificate.
	key: &'a [u8],
	//The entiere certificate.
	entiere: &'a [u8],
	//The TBS.
	tbs: &'a [u8],
	//The signature.
	signature: SignatureBlock<'a>,
	//Issuer of the certificate.
	issuer: CertificateIssuer<'a>,
}


fn validate_certificate_after_sanity_check<'a,'b>(eecert: &ParsedCertificate<'b>,
	intermediates: &[ParsedCertificate<'b>], keys_map: &mut BTreeMap<CertificateSubject<'b>, &'a [u8]>,
	seen: &BTreeMap<CertificateIssuer<'b>, ()>,
	trust: (&'a BTreeMap<Vec<u8>, TrustAnchorValue>, &[HostSpecificPin]), killist: &BTreeMap<[u8; 32], ()>,
	_valid_until: Timestamp, _need_features: u32) -> Result<(), CertificateValidationError>
{
	use self::CertificateValidationError::*;
	let trust_anchors = trust.0;
	let host_pins = trust.1;
	//Do we have at least one pin, so pinning should be activated?
	let at_least_one_pin = host_pins.iter().filter(|x|x.pin).next().is_some();
	//Add the missing keys found from trust anchor list into keys map, checking that the keys are unique within
	//a name.
	for i in seen.keys() {
		let j: CertificateIssuer<'b> = *i;
		let rawissuer = i.as_raw_issuer();
		if trust_anchors.contains_key(rawissuer) {
			let ta_spki = &trust_anchors.get(rawissuer).ok_or(assert_failure!("Bad trust anchor map \
				(contains_key, but no entry to get)"))?.get_spki().ok_or(assert_failure!("Bad \
				trust anchor (bad spki range)"))?;
			add_key!(keys_map; j.as_subject(), &ta_spki[..]);
		}
	}
	let mut _trust_anchors: Vec<usize> = Vec::new();
	let mut _pins: Vec<usize> = Vec::new();
	let mut certificates: Vec<TrustPathEntry<CertificateEntry>> = Vec::new();
	let mut representatives_name: BTreeMap<CertificateSubject, usize> = BTreeMap::new();

	//Build map of subject representatives for the certificates.
	certificates.push(TrustPathEntry{
		representative: 0,				//The EE certificate subject is always #0.
		//This must be allowed, not just known, because this signature may be checked.
		link_ok: allowed_signature_algorithm(eecert.signature),
		issuer_representative: usize::max_value(),
		more: CertificateEntry {
			key: eecert.pubkey,
			entiere: eecert.entiere,
			tbs: eecert.tbs,
			signature: eecert.signature,
			issuer: eecert.issuer,
		}
	});
	representatives_name.insert(eecert.subject, 0);

	for i in intermediates.iter().enumerate() {
		let cert_num = i.0.checked_add(1).ok_or(assert_failure!("Too many certificates in chain"))?;
		if !representatives_name.contains_key(&i.1.subject) {
			representatives_name.insert(i.1.subject, cert_num);
		}
		if is_valid_trust_anchor(i.1.dnsnames.iter(), i.1.subject.as_raw_subject(), trust_anchors,
			killist) {
			_trust_anchors.push(cert_num);
		}
		certificates.push(TrustPathEntry{
			representative: *representatives_name.get(&i.1.subject).ok_or(
				assert_failure!("No representative for subject"))?,
			//This must be allowed, not just recognized, because this signature may be checked.
			link_ok: allowed_signature_algorithm(i.1.signature),
			issuer_representative: usize::max_value(),
			more: CertificateEntry {
				key: i.1.pubkey,
				entiere: i.1.entiere,
				tbs: i.1.tbs,
				signature: i.1.signature,
				issuer: i.1.issuer,
			}
		});
	}

	//Make entries for trust anchors too.
	let mut next = intermediates.len().checked_add(1).ok_or(assert_failure!("Too many certificates in chain"))?;
	for i in seen.keys() {
		if !representatives_name.contains_key(&i.as_subject()) {
			representatives_name.insert(i.as_subject(), next);
			let rawissuer = i.as_raw_issuer();
			let key = if is_valid_trust_anchor(eecert.dnsnames.iter(), rawissuer, trust_anchors,
				killist) {
				_trust_anchors.push(next);
				trust_anchors.get(rawissuer).ok_or(assert_failure!("Bad trust anchor map \
					(contains_key, but no entry to get)"))?.get_spki().ok_or(assert_failure!(
					"Bad trust anchor (bad spki)"))?
			} else {
				&[]	//No key for extra non-TA entries.
			};
			certificates.push(TrustPathEntry{
				representative: next,
				link_ok: false,
				issuer_representative: next,	//These are self-signed.
				more: CertificateEntry {
					key: key,
					entiere: &[],		//The signature is never verified.
					tbs: &[],
					signature: SignatureBlock::from_x509(&[], &[]),
					issuer: *i,		//Always self-signed.
				}
			});
			next = next.checked_add(1).ok_or(assert_failure!("Too many certificates in chain"))?;
		}
	}
	//Compute issuer representatives.
	for i in certificates.iter_mut().enumerate() {
		i.1.issuer_representative = *representatives_name.get(&i.1.more.issuer.as_subject()).ok_or(
			assert_failure!("No representative for issuer"))?;
	}
	//Deal with pinning.
	for i in host_pins.iter() {
		for j in certificates.iter().enumerate() {
			if j.0 == 0 && i.ca_flag == Some(true) { continue; }	//EE, need CA.
			if j.0 != 0 && i.ca_flag == Some(false) { continue; }	//CA, need EE.
			let matched_data = if i.spki { j.1.more.key } else { j.1.more.entiere };
			if i.data.matches(matched_data) {
				if i.pin { _pins.push(j.0); }
				if i.ta { _trust_anchors.push(j.0); }
			}
		}
	}
	//We have pins, but set none on graph. This definitely means we failed pinning.
	fail_if!(at_least_one_pin && _pins.len() == 0, NoCertifcatesMatchPins);
	//Build the trust path.
	let mut scratch = vec![0usize; certificates.len().saturating_mul(7)];
	let path = build_trust_path2(&certificates, &_pins, &_trust_anchors, &mut scratch).map_err(|_|CaUnknown)?;
	//Validate signatures on the trust path. Note that 0-length path is an error: Such path can not be valid.
	//The returned path from path building is guaranteed to have all indices valid for vertices and length of
	//at least 1. Also, i+1 is at most path.len()-1, and thus fits into usize.
	for i in 0..path.len()-1 {
		let this_ent = &certificates[path[i]].more;
		let next_ent = &certificates[path[i+1]].more;
		//Entries like that are used in synthethized entries. And since these don't have the trust
		//anchor flag set, they should not be considered valid endpoints of the certificate chain,
		//but just in case.
		fail_if!(next_ent.key.len() == 0, CaUnknown);
		this_ent.signature.verify(next_ent.key, this_ent.tbs, 0).map_err(|_|SignatureFailure(path[i]))?;
	}
	//All checks passed.
	Ok(())
}


//Check path length constraints.
fn check_pathlen_constraints<'b>(icerts: &[ParsedCertificate<'b>],
	issuers_seen: &BTreeMap<CertificateIssuer<'b>, ()>) -> Result<(), CertificateValidationError>
{
	use self::CertificateValidationError::*;

	//Check all path length constraints are satisfied. We do this as follows:
	// - Compute minimum path length constraint for each subject.
	// - Pseudo-topologically sort the issuer graph (if it is DAG, it is topologically sorted, if it is not,
	//   it has back-edges).
	// - Walk the sorted graph in order. If we encounter a node with path length constraint of 0, or node
	//   with backedge and non-infinite constraint, fail. Otherwise cap all forward targets to constraint in
	//   this node minus 1.
	//
	//This computes if it is possible to construct a certificate path that violates path length constraints.
	let mut issuer_ids: BTreeMap<CertificateIssuer, usize> = BTreeMap::new();
	let mut min_pathlen: Vec<Option<u64>> = Vec::with_capacity(issuers_seen.len());
	let mut issued_for: BTreeMap<usize, BTreeMap<usize, ()>> = BTreeMap::new();
	let mut issued_for_rev: BTreeMap<usize, BTreeMap<usize, ()>> = BTreeMap::new();
	for i in issuers_seen.keys() {
		let next_id = min_pathlen.len();
		issuer_ids.insert(*i, next_id);
		min_pathlen.push(None);
	}
	for i in icerts.iter() {
		//Every CA subject is guaranteed to be in issuers_seen (we actually check this above).
		let entry = min_pathlen.get_mut(*issuer_ids.get(&i.subject.as_issuer()).ok_or(assert_failure!(
			"No issuer subject id"))?).ok_or(assert_failure!("No issuer path length entry"))?;
		*entry = min_inf(*entry, i.max_path_len);
		//Build graph of who issued to who.
		let issued_by = *issuer_ids.get(&i.issuer).ok_or(assert_failure!("No issuer subject id"))?;
		let issued_to = *issuer_ids.get(&i.subject.as_issuer()).ok_or(assert_failure!(
			"No subject subject id"))?;

		if !issued_for.contains_key(&issued_by) { issued_for.insert(issued_by, BTreeMap::new()); }
		if !issued_for.contains_key(&issued_to) { issued_for.insert(issued_to, BTreeMap::new()); }
		issued_for.get_mut(&issued_by).ok_or(assert_failure!("No issued_for entry"))?.insert(issued_to, ());

		if !issued_for_rev.contains_key(&issued_by) { issued_for_rev.insert(issued_by, BTreeMap::new()); }
		if !issued_for_rev.contains_key(&issued_to) { issued_for_rev.insert(issued_to, BTreeMap::new()); }
		issued_for_rev.get_mut(&issued_to).ok_or(assert_failure!("No issued_for_rev entry"))?.insert(
			issued_by, ());
	}
	let mut issuers_toposort_order: Vec<usize> = Vec::new();
	let mut reverse_toposort_order = BTreeMap::new();
	toposort(&issued_for_rev, &mut issuers_toposort_order)?;
	for i in issuers_toposort_order.iter().enumerate() {
		reverse_toposort_order.insert(*i.1, i.0);
	}
	for i in issuers_toposort_order.iter().enumerate() {
		let pathlen = *min_pathlen.get(*i.1).ok_or(assert_failure!("No issuer path length entry"))?;
		for j in issued_for.get(i.1).ok_or(assert_failure!("No issued_for entry"))?.keys() {
			let pathlen = match pathlen {
				Some(0) => fail!(PathLenViolation),	//0 remaining => can't issue new CA.
				Some(x) => Some(x.saturating_sub(1)),	//Can't be 0, because that would have tripped
									//the above case.
				None => None
			};
			let _j = *reverse_toposort_order.get(j).ok_or(assert_failure!("No reverse_toposort entry"))?;
			if _j > i.0 {
				let entry = min_pathlen.get_mut(_j).ok_or(assert_failure!("No issuer path length \
					entry"))?;
				*entry = min_inf(*entry, pathlen);
			} else if pathlen.is_some() {
				//Backedge with non-infinite length => Would be infinitely loopable.
				fail!(PathLenViolation);
			}
		}
	}
	Ok(())
}

///Sanity-check a certificate chain. The following checks are done:
///
/// - Eecert must have SAN for the reference (if any).
/// - The two signature algorithm slots match up.
/// - No certificate may be expired or not yet valid.
/// - eecert must have use_tls_serv and !is_ca
/// - Every non-eecert must have (use_tls_serv | use_any) and is_ca.
/// - Every certificate (except the first) must sign some previous certificate.
/// - Every EE SAN satisfies all certificate name constraints.
/// - Subject name must be key for subject public key (also checked versus initial values of keys_map)
/// - EE cert must have unique public key.
/// - The first CA cert must have issued the EE cert.
/// - Path length constraints are satisfied.
/// - If strict_algo is set, every signature is with approved signature method.
///
///Also, keys_map is updated and issuers_seen is populated with certificate issuers seen.
///
///The parameters passed to callback are in order:
/// - Parsed EE certificate
/// - Parsed intermediate certificates.
/// - Borrow of keys_map.
/// - Set of issuer names seen.
/// - Specified argument to sanity_check_certificate_chain.
/// - Expiry time of the certificate
/// - The required flags for certificate.
///
///# Parameters:
///
/// * `_eecert`: The EE certificate.
/// * `_icerts`: The intermediate certificates.
/// * `reference`: The reference name (if any).
/// * `time_now`: Current time.
/// * `strict_algo`: If true, strictly check algorithms.
/// * `feature_flags`: The feature flags that have been met.
/// * `callback`: The callback to call after validation.
/// * `cbparam`: The extra parameter to pass to callback.
/// * `need_ocsp_flag`: Set if certificate requires OCSP.
/// * `need_sct_flag`: Set if certificate requires SCT.
/// * `is_client`: True to run client certificate checks.
///
///# Return value:
///
/// * The return value of the callback.
///
///# Failures:
///
/// * If the validation fails, returns an appropriate error.
pub fn sanity_check_certificate_chain<'b, T, F, P>(_eecert: &'b [u8], _icerts: &[&'b [u8]], reference: Option<&str>,
	time_now: &TimeInterface, strict_algo: bool, mut feature_flags: u32, mut callback: F, cbparam: P,
	killist: &BTreeMap<[u8; 32], ()>, need_ocsp_flag: &mut bool, need_sct_flag: &mut bool, is_client: bool) ->
	Result<T, CertificateValidationError> where F: FnMut(&ParsedCertificate<'b>, &[ParsedCertificate<'b>],
	&mut BTreeMap<CertificateSubject<'b>, &'b [u8]>, &BTreeMap<CertificateIssuer<'b>, ()>, P,
	&BTreeMap<[u8; 32], ()>, Timestamp, u32) -> Result<T, CertificateValidationError>
{
	use self::CertificateValidationError::*;
	let mut keys_map: BTreeMap<CertificateSubject<'b>, &'b [u8]> = BTreeMap::new();
	let mut issuers_seen: BTreeMap<CertificateIssuer<'b>, ()> = BTreeMap::new();
	let mut feature_flags_present: u32 = 0;
	let eecert: ParsedCertificate<'b> = ParsedCertificate::from(_eecert).map_err(|x|EeCertParseError(x))?;
	let mut valid_until = Timestamp::positive_infinity();	//Maximum i64.

	//If EE cert contains SCTs, assert the feature.
	if !eecert.scts.empty() { feature_flags |= CFLAG_MUST_CT; }

	//The basic checks on EE.
	ee_certificate_checks(&eecert, time_now, strict_algo, _icerts.len(), &mut feature_flags_present,
		&mut issuers_seen, &mut keys_map, false, is_client)?;
	valid_until = valid_until.first(eecert.not_after);
	//Check EE on killist.
	fail_if!(key_on_killist(eecert.pubkey, killist), EeKillisted);
	//If reference was given, the certificate must have that in SAN (either explicitly or via a wildcard).
	if let Some(reference) = reference {
		if !name_in_set2(eecert.dnsnames.iter(), reference.as_bytes()) {
			let mut names = String::new();
			let mut first = true;
			for i in eecert.dnsnames.iter() {
				if !first { names.push_str(", "); }
				names.push_str(String::from_utf8_lossy(i).deref());
				first = false;
			}
			fail!(EeNotValidForName(reference.to_owned(), names));
		}
	}

	let mut icerts = Vec::new();
	for i in _icerts.iter().enumerate() {
		let icert = ParsedCertificate::from(i.1).map_err(|x|CaCertParseError(i.0, x))?;
		//Check CA on killist (we check this first, because ca_certificate_checks consumes the parsed
		//certificate.
		fail_if!(key_on_killist(icert.pubkey, killist), CaKillisted(i.0));
		//Basic checks.
		ca_certificate_checks(icert, &eecert, i.0, time_now, strict_algo, &mut feature_flags_present,
			&mut issuers_seen, &mut keys_map, &mut icerts, false, is_client)?;
		valid_until = valid_until.first(eecert.not_after);
	}

	//Check features. We set a flag if certificate is must-staple, because if OCSP is available isn't known yet.
	//The same with SCT stapling.
	if feature_flags_present & CFLAG_MUST_STAPLE != 0 { *need_ocsp_flag = true; }
	if feature_flags_present & CFLAG_MUST_CT != 0 { *need_sct_flag = true; }
	let features_unmet = feature_flags_present & !feature_flags;
	if features_unmet != 0 {
		fail_if!(features_unmet & CFLAG_MUST_STAPLE != 0, OcspStapleRequred);
		fail_if!(features_unmet & CFLAG_MUST_CT != 0, SctStapleRequred);
		fail_if!(features_unmet & CFLAG_MUST_EMS != 0, EmsRequired);
		fail_if!(features_unmet & CFLAG_MUST_TLS13 != 0, Tls13Required);
		fail_if!(features_unmet & CFLAG_MUST_RENEGO != 0, SecureRenegoRequired);
		fail_if!(features_unmet & CFLAG_UNKNOWN != 0, RequireUnknownFeatures);
		fail!(UnmetFeatures(features_unmet));
	}

	check_pathlen_constraints(&icerts, &issuers_seen)?;
	callback(&eecert, &icerts, &mut keys_map, &issuers_seen, cbparam, killist, valid_until,
		feature_flags_present)
}

///Do Load-time sanity-check a certificate chain. Does the same checks as sanity_check_certificate_chain, except the
///following:
/// - The signature algorithm check is unconditionally done.
/// - Reference SAN check is never done.
/// - There is no callback for postcheck.
/// - Of feature flags, only unknown ones are checked.
/// - Killist is not checked.
/// - The public key is checked against the given one.
///
///# Parameters:
///
/// * `_eecert`: The EE certificate.
/// * `_icerts`: The intermediate certificates.
/// * `pubkey`: The public key of the certificate.
/// * `time_now`: Current time.
/// * `need_ocsp_flag`: Set if certificate requires OCSP.
/// * `need_sct_flag`: Set if certificate requires SCT.
/// * `selfsigned_ok`: If true, accept self-signed.
///
///# Failures:
///
/// * If the sanity check fails, returns an appropriate error.
pub fn sanity_check_certificate_chain_load<'b>(_eecert: &'b [u8], _icerts: &[&'b [u8]], pubkey: &[u8],
	time_now: &TimeInterface, need_ocsp_flag: &mut bool, need_sct_flag: &mut bool, selfsigned_ok: bool) ->
	Result<(), CertificateValidationError>
{
	use self::CertificateValidationError::*;
	let mut keys_map: BTreeMap<CertificateSubject<'b>, &'b [u8]> = BTreeMap::new();
	let mut issuers_seen: BTreeMap<CertificateIssuer<'b>, ()> = BTreeMap::new();
	let mut feature_flags_present: u32 = 0;
	let eecert: ParsedCertificate<'b> = ParsedCertificate::from(_eecert).map_err(|x|EeCertParseError(x))?;

	//Check the public key given.
	fail_if!(pubkey != eecert.pubkey, PublicKeyMismatch);

	if eecert.issuer.same_as(eecert.subject) && selfsigned_ok {
		//Self-signed, and self-signed OK, return Ok.
		return Ok(());
	}

	//The basic checks on EE (strict_algo is always true).
	ee_certificate_checks(&eecert, time_now, true, _icerts.len(), &mut feature_flags_present,
		&mut issuers_seen, &mut keys_map, true, false)?;

	let mut icerts = Vec::new();
	for i in _icerts.iter().enumerate() {
		let icert = ParsedCertificate::from(i.1).map_err(|x|CaCertParseError(i.0, x))?;
		//Basic checks (strict_algo is always true).
		ca_certificate_checks(icert, &eecert, i.0, time_now, true, &mut feature_flags_present,
			&mut issuers_seen, &mut keys_map, &mut icerts, true, false)?;
	}

	//Check features. We set a flag if certificate is must-staple.
	if feature_flags_present & CFLAG_MUST_STAPLE != 0 { *need_ocsp_flag = true; }
	if feature_flags_present & CFLAG_MUST_CT != 0 && eecert.scts.empty() { *need_sct_flag = true; }
	fail_if!(feature_flags_present & CFLAG_UNKNOWN != 0, RequireUnknownFeatures);

	check_pathlen_constraints(&icerts, &issuers_seen)?;
	Ok(())
}


fn toposort_helper(rev_map: &BTreeMap<usize, BTreeMap<usize, ()>>, order: &mut Vec<usize>,
	visited: &mut BTreeMap<usize, ()>, index: usize) -> Result<(), CertificateValidationError>
{
	if visited.contains_key(&index) { return Ok(()); }	//Don't visit again.
	visited.insert(index, ());
	for i in rev_map.get(&index).ok_or(assert_failure!("Broken link (no rev_map entry found)"))?.keys() {
		toposort_helper(rev_map, order, visited, *i)?;
	}
	order.push(index);
	Ok(())
}

fn toposort(rev_map: &BTreeMap<usize, BTreeMap<usize, ()>>, order: &mut Vec<usize>) ->
	Result<(), CertificateValidationError>
{
	let mut visited: BTreeMap<usize, ()> = BTreeMap::new();
	for i in rev_map.keys() {
		toposort_helper(rev_map, order, &mut visited, *i)?;
	}
	Ok(())
}


///Validate EE SPKI against host pins.
///
///# Parameters:
///
/// * `ee_spki`: The EE SPKI to validate.
/// * `host_pins`: The host pins in effect.
///
///# Failures:
///
/// * If the verification fails, `()`.
pub fn validate_ee_spki(ee_spki: &[u8], host_pins: &[HostSpecificPin]) -> Result<(), ()>
{
	let any_pin = {
		let mut any_pin = false;
		for i in host_pins.iter() { any_pin |= i.pin; }
		any_pin
	};
	//Check host pins against EE SPKI.
	for i in host_pins.iter() {
		//If pin is a CA pin, it does not match.
		if i.ca_flag == Some(true) { continue; }
		//If pin is not a TA, it does not match.
		if !i.ta { continue; }
		//If pin is not a pin and there are pins, it does not match.
		if !i.pin && any_pin { continue; }
		//If pin is not for SPKI, it does not match.
		if !i.spki { continue; }
		//Match the data. If match, short-circuit the validation.
		if i.data.matches(ee_spki) { return Ok(()); }
	}
	Err(())
}

///Validate server certificate chain.
///
///# Parameters:
///
/// * `_eecert`: The EE certificate.
/// * `_icerts`: The intermediate certificates.
/// * `reference`: The reference name (if any).
/// * `time_now`: Current time.
/// * `trust_anchors`: The trust anchors to use in validation.
/// * `host_pins`: The host pins in effect.
/// * `killist`: The public key kill list in effect.
/// * `need_ocsp_flag`: Set if certificate requires OCSP.
/// * `need_sct_flag`: Set if certificate requires SCT.
///
///# Return value:
///
/// * If just SPKI was validated, true.
/// * If whole chain was validated, false.
///
///# Failures:
///
/// * If the validation fails, returns an appropriate error.
pub fn validate_chain<'b>(_eecert: &'b [u8], _icerts: &[&'b [u8]], reference: &str,
	time_now: &TimeInterface, feature_flags: u32, trust_anchors: &'b BTreeMap<Vec<u8>, TrustAnchorValue>,
	host_pins: &[HostSpecificPin], killist: &BTreeMap<[u8; 32], ()>, need_ocsp_flag: &mut bool,
	need_sct_flag: &mut bool) -> Result<bool, CertificateValidationError>
{
	match validate_ee_spki(extract_spki(_eecert).map_err(|x|CertificateValidationError::EeSpkiExtraction(x))?,
		host_pins) {
		Ok(_) => return Ok(true),
		Err(_) => ()
	};
	sanity_check_certificate_chain(_eecert, _icerts, Some(reference),  time_now, false, feature_flags,
		validate_certificate_after_sanity_check, (trust_anchors, host_pins), killist, need_ocsp_flag,
		need_sct_flag, false).map(|_|false)
}

///Validate client certificate chain.
///
///# Parameters:
///
/// * `_eecert`: The EE certificate.
/// * `_icerts`: The intermediate certificates.
/// * `time_now`: Current time.
/// * `trust_anchors`: The trust anchors to use in validation.
/// * `killist`: The public key kill list in effect.
/// * `need_ocsp_flag`: Set if certificate requires OCSP.
/// * `need_sct_flag`: Set if certificate requires SCT.
///
///# Return value:
///
/// * If just SPKI was validated, true.
/// * If whole chain was validated, false.
///
///# Failures:
///
/// * If the validation fails, returns an appropriate error.
pub fn validate_chain_client<'b>(_eecert: &'b [u8], _icerts: &[&'b [u8]], time_now: &TimeInterface,
	feature_flags: u32, trust_anchors: &'b BTreeMap<Vec<u8>, TrustAnchorValue>, killist: &BTreeMap<[u8; 32], ()>,
	need_ocsp_flag: &mut bool, need_sct_flag: &mut bool) -> Result<bool, CertificateValidationError>
{
	static BLANK: [HostSpecificPin;0] = [];
	sanity_check_certificate_chain(_eecert, _icerts, None,  time_now, false, feature_flags,
		validate_certificate_after_sanity_check, (trust_anchors, &BLANK[..]), killist, need_ocsp_flag,
		need_sct_flag, true).map(|_|false)
}

#[test]
fn simple_chain_building()
{
	let vertices = [
		TrustPathEntry{representative: 0, link_ok: true, issuer_representative: 1, more:()},
		TrustPathEntry{representative: 1, link_ok: true, issuer_representative: 2, more:()},
		TrustPathEntry{representative: 2, link_ok: false, issuer_representative: 2, more:()},
	];
	let pin_set = [];
	let ta_set = [2];
	let mut scratch = [0usize;21];
	let trustpath = build_trust_path2(&vertices, &pin_set, &ta_set, &mut scratch).unwrap();
	assert_eq!(&trustpath[..], &[0, 1, 2]);
}

#[test]
fn chain_building_two_ta()
{
	let vertices = [
		TrustPathEntry{representative: 0, link_ok: true, issuer_representative: 1, more:()},
		TrustPathEntry{representative: 1, link_ok: true, issuer_representative: 3, more:()},
		TrustPathEntry{representative: 1, link_ok: true, issuer_representative: 4, more:()},
		TrustPathEntry{representative: 3, link_ok: false, issuer_representative: 3, more:()},
		TrustPathEntry{representative: 4, link_ok: false, issuer_representative: 4, more:()},
	];
	let pin_set = [];
	let ta_set = [4];
	let mut scratch = [0usize;35];
	let trustpath = build_trust_path2(&vertices, &pin_set, &ta_set, &mut scratch).unwrap();
	assert_eq!(&trustpath[..], &[0, 2, 4]);
}

#[test]
fn chain_building_two_sigs()
{
	let vertices = [
		TrustPathEntry{representative: 0, link_ok: true, issuer_representative: 1, more:()},
		TrustPathEntry{representative: 1, link_ok: false, issuer_representative: 3, more:()},
		TrustPathEntry{representative: 1, link_ok: true, issuer_representative: 3, more:()},
		TrustPathEntry{representative: 3, link_ok: false, issuer_representative: 3, more:()},
	];
	let pin_set = [];
	let ta_set = [3];
	let mut scratch = [0usize;28];
	let trustpath = build_trust_path2(&vertices, &pin_set, &ta_set, &mut scratch).unwrap();
	assert_eq!(&trustpath[..], &[0, 2, 3]);
}

#[test]
fn chain_building_cut_trust()
{
	let vertices = [
		TrustPathEntry{representative: 0, link_ok: true, issuer_representative: 1, more:()},
		TrustPathEntry{representative: 1, link_ok: false, issuer_representative: 3, more:()},
		TrustPathEntry{representative: 1, link_ok: false, issuer_representative: 3, more:()},
		TrustPathEntry{representative: 3, link_ok: false, issuer_representative: 3, more:()},
	];
	let pin_set = [];
	let ta_set = [3];
	let mut scratch = [0usize;28];
	build_trust_path2(&vertices, &pin_set, &ta_set, &mut scratch).unwrap_err();
}

#[test]
fn chain_building_cut_trust2()
{
	let vertices = [
		TrustPathEntry{representative: 0, link_ok: false, issuer_representative: 1, more:()},
		TrustPathEntry{representative: 1, link_ok: true, issuer_representative: 3, more:()},
		TrustPathEntry{representative: 1, link_ok: true, issuer_representative: 3, more:()},
		TrustPathEntry{representative: 3, link_ok: false, issuer_representative: 3, more:()},
	];
	let pin_set = [];
	let ta_set = [3];
	let mut scratch = [0usize;28];
	build_trust_path2(&vertices, &pin_set, &ta_set, &mut scratch).unwrap_err();
}

#[test]
fn chain_building_cut_trust_ta()
{
	let vertices = [
		TrustPathEntry{representative: 0, link_ok: false, issuer_representative: 1, more:()},
		TrustPathEntry{representative: 1, link_ok: true, issuer_representative: 3, more:()},
		TrustPathEntry{representative: 1, link_ok: true, issuer_representative: 3, more:()},
		TrustPathEntry{representative: 3, link_ok: false, issuer_representative: 3, more:()},
	];
	let pin_set = [];
	let ta_set = [0, 3];
	let mut scratch = [0usize;28];
	let trustpath = build_trust_path2(&vertices, &pin_set, &ta_set, &mut scratch).unwrap();
	assert_eq!(&trustpath[..], &[0]);
}

#[test]
fn simple_chain_building_cross_cert()
{
	let vertices = [
		TrustPathEntry{representative: 0, link_ok: true, issuer_representative: 1, more:()},
		TrustPathEntry{representative: 1, link_ok: true, issuer_representative: 2, more:()},
		TrustPathEntry{representative: 2, link_ok: true, issuer_representative: 3, more:()},
		TrustPathEntry{representative: 3, link_ok: false, issuer_representative: 3, more:()},
	];
	let pin_set = [];
	let ta_set = [2, 3];
	let mut scratch = [0usize;28];
	let trustpath = build_trust_path2(&vertices, &pin_set, &ta_set, &mut scratch).unwrap();
	assert_eq!(&trustpath[..], &[0, 1, 2]);
}

#[test]
fn certbuild_two_by_two_block()
{
	//      34  7
	// 0 12        9
	//      56  8
	let vertices = [
		TrustPathEntry{representative: 0, link_ok: true, issuer_representative: 1, more:()},
		TrustPathEntry{representative: 1, link_ok: true, issuer_representative: 3, more:()},
		TrustPathEntry{representative: 1, link_ok: true, issuer_representative: 5, more:()},
		TrustPathEntry{representative: 3, link_ok: true, issuer_representative: 7, more:()},
		TrustPathEntry{representative: 3, link_ok: true, issuer_representative: 8, more:()},
		TrustPathEntry{representative: 5, link_ok: true, issuer_representative: 7, more:()},
		TrustPathEntry{representative: 5, link_ok: true, issuer_representative: 8, more:()},
		TrustPathEntry{representative: 7, link_ok: true, issuer_representative: 9, more:()},
		TrustPathEntry{representative: 8, link_ok: true, issuer_representative: 9, more:()},
		TrustPathEntry{representative: 9, link_ok: false, issuer_representative: 9, more:()},
	];
	let pin_set = [];
	let ta_set = [9];
	let mut scratch = [0usize;70];
	let trustpath = build_trust_path2(&vertices, &pin_set, &ta_set, &mut scratch).unwrap();
	assert_eq!(&trustpath[..], &[0, 1, 3, 7, 9]);
}

#[test]
fn certbuild_two_by_two_block_pinned()
{
	//      34  7
	// 0 12        9
	//      56  8
	let vertices = [
		TrustPathEntry{representative: 0, link_ok: true, issuer_representative: 1, more:()},
		TrustPathEntry{representative: 1, link_ok: true, issuer_representative: 3, more:()},
		TrustPathEntry{representative: 1, link_ok: true, issuer_representative: 5, more:()},
		TrustPathEntry{representative: 3, link_ok: true, issuer_representative: 7, more:()},
		TrustPathEntry{representative: 3, link_ok: true, issuer_representative: 8, more:()},
		TrustPathEntry{representative: 5, link_ok: true, issuer_representative: 7, more:()},
		TrustPathEntry{representative: 5, link_ok: true, issuer_representative: 8, more:()},
		TrustPathEntry{representative: 7, link_ok: true, issuer_representative: 9, more:()},
		TrustPathEntry{representative: 8, link_ok: true, issuer_representative: 9, more:()},
		TrustPathEntry{representative: 9, link_ok: false, issuer_representative: 9, more:()},
	];
	let pin_set = [8];
	let ta_set = [9];
	let mut scratch = [0usize;70];
	let trustpath = build_trust_path2(&vertices, &pin_set, &ta_set, &mut scratch).unwrap();
	assert_eq!(&trustpath[..], &[0, 1, 4, 8, 9]);
}

#[test]
fn certbuild_two_by_two_block_pinned_notok()
{
	//      34  7
	// 0 12        9
	//      56  8
	let vertices = [
		TrustPathEntry{representative: 0, link_ok: true, issuer_representative: 1, more:()},
		TrustPathEntry{representative: 1, link_ok: true, issuer_representative: 3, more:()},
		TrustPathEntry{representative: 1, link_ok: true, issuer_representative: 5, more:()},
		TrustPathEntry{representative: 3, link_ok: true, issuer_representative: 7, more:()},
		TrustPathEntry{representative: 3, link_ok: false, issuer_representative: 8, more:()},
		TrustPathEntry{representative: 5, link_ok: true, issuer_representative: 7, more:()},
		TrustPathEntry{representative: 5, link_ok: true, issuer_representative: 8, more:()},
		TrustPathEntry{representative: 7, link_ok: true, issuer_representative: 9, more:()},
		TrustPathEntry{representative: 8, link_ok: true, issuer_representative: 9, more:()},
		TrustPathEntry{representative: 9, link_ok: false, issuer_representative: 9, more:()},
	];
	let pin_set = [8];
	let ta_set = [9];
	let mut scratch = [0usize;70];
	let trustpath = build_trust_path2(&vertices, &pin_set, &ta_set, &mut scratch).unwrap();
	assert_eq!(&trustpath[..], &[0, 2, 6, 8, 9]);
}

#[test]
fn certbuild_two_by_two_block_pinned_block1()
{
	//      34  7
	// 0 12        9
	//      56  8
	let vertices = [
		TrustPathEntry{representative: 0, link_ok: true, issuer_representative: 1, more:()},
		TrustPathEntry{representative: 1, link_ok: false, issuer_representative: 3, more:()},
		TrustPathEntry{representative: 1, link_ok: true, issuer_representative: 5, more:()},
		TrustPathEntry{representative: 3, link_ok: true, issuer_representative: 7, more:()},
		TrustPathEntry{representative: 3, link_ok: true, issuer_representative: 8, more:()},
		TrustPathEntry{representative: 5, link_ok: true, issuer_representative: 7, more:()},
		TrustPathEntry{representative: 5, link_ok: true, issuer_representative: 8, more:()},
		TrustPathEntry{representative: 7, link_ok: true, issuer_representative: 9, more:()},
		TrustPathEntry{representative: 8, link_ok: true, issuer_representative: 9, more:()},
		TrustPathEntry{representative: 9, link_ok: false, issuer_representative: 9, more:()},
	];
	let pin_set = [7];
	let ta_set = [9];
	let mut scratch = [0usize;70];
	let trustpath = build_trust_path2(&vertices, &pin_set, &ta_set, &mut scratch).unwrap();
	assert_eq!(&trustpath[..], &[0, 2, 5, 7, 9]);
}

#[test]
fn certbuild_three_certs()
{
	//   1
	// 0 24
	//   3
	let vertices = [
		TrustPathEntry{representative: 0, link_ok: true, issuer_representative: 1, more:()},
		TrustPathEntry{representative: 1, link_ok: true, issuer_representative: 4, more:()},
		TrustPathEntry{representative: 1, link_ok: true, issuer_representative: 4, more:()},
		TrustPathEntry{representative: 1, link_ok: true, issuer_representative: 4, more:()},
		TrustPathEntry{representative: 4, link_ok: false, issuer_representative: 4, more:()},
	];
	let pin_set = [3];
	let ta_set = [4];
	let mut scratch = [0usize;35];
	let trustpath = build_trust_path2(&vertices, &pin_set, &ta_set, &mut scratch).unwrap();
	assert_eq!(&trustpath[..], &[0, 3, 4]);
}

#[test]
fn certbuild_killer()
{
	//       2 - {3} - 1
	//      /
	// 0 - 1
	//      \
	//       4 - (5)
	//3 is pinned, 5 is trust anchor. 2 and 4 are same subject.
	let vertices = [
		TrustPathEntry{representative: 0, link_ok: true, issuer_representative: 1, more:()},
		TrustPathEntry{representative: 1, link_ok: true, issuer_representative: 2, more:()},
		TrustPathEntry{representative: 2, link_ok: true, issuer_representative: 3, more:()},
		TrustPathEntry{representative: 3, link_ok: true, issuer_representative: 1, more:()},
		TrustPathEntry{representative: 2, link_ok: true, issuer_representative: 5, more:()},
		TrustPathEntry{representative: 5, link_ok: false, issuer_representative: 5, more:()},
	];
	let pin_set = [3];
	let ta_set = [5];
	let mut scratch = [0usize;42];
	let trustpath = build_trust_path2(&vertices, &pin_set, &ta_set, &mut scratch).unwrap();
	assert_eq!(&trustpath[..], &[0, 1, 2, 3, 1, 4, 5]);
}

#[cfg(test)]
#[derive(Copy,Clone)]
struct StringAdapter<'a>(&'a [&'a str], usize);

#[cfg(test)]
impl<'a> Iterator for StringAdapter<'a>
{
	type Item = &'a [u8];
	fn next(&mut self) -> Option<&'a [u8]>
	{
		let idx = self.1;
		self.1 += 1;
		if idx >= self.0.len() { return None; }
		Some(self.0[idx].as_bytes())
	}
}

#[test]
fn test_name_constraints_none()
{
	//If no name constraints, anything goes.
	let null: [& str; 0] = [];
	let name: [&str; 3] = ["foo.test", "bar.test", "qux.example"];
	assert!(check_name_constraints2(StringAdapter(&name,0), StringAdapter(&null,0), StringAdapter(&null,0)));
}

#[test]
fn test_name_constraints_disallow()
{
	//If no allow but some disallow, everything not disallowed is allowed.
	let null: [& str; 0] = [];
	let disallows: [& str; 1] = ["mil"];
	let disallowsd: [& str; 1] = [".mil"];
	let name: [&str; 3] = ["foo.test", "bar.test", "qux.example"];
	let name2: [&str; 4] = ["foo.test", "bar.test", "qux.example", "banned.mil"];
	let name3: [&str; 4] = ["foo.test", "bar.test", "qux.example", "notbanned.xmil"];
	let name4: [&str; 1] = ["mil"];
	let name5: [&str; 1] = ["xmil"];
	assert!(check_name_constraints2(StringAdapter(&name,0), StringAdapter(&null,0),
		StringAdapter(&disallows,0)));
	assert!(!check_name_constraints2(StringAdapter(&name2,0), StringAdapter(&null,0),
		StringAdapter(&disallows,0)));
	assert!(check_name_constraints2(StringAdapter(&name3,0), StringAdapter(&null,0),
		StringAdapter(&disallows,0)));
	assert!(!check_name_constraints2(StringAdapter(&name4,0), StringAdapter(&null,0),
		StringAdapter(&disallows,0)));
	assert!(check_name_constraints2(StringAdapter(&name5,0), StringAdapter(&null,0),
		StringAdapter(&disallows,0)));
	assert!(check_name_constraints2(StringAdapter(&name,0), StringAdapter(&null,0),
		StringAdapter(&disallowsd,0)));
	assert!(!check_name_constraints2(StringAdapter(&name2,0), StringAdapter(&null,0),
		StringAdapter(&disallowsd,0)));
	assert!(check_name_constraints2(StringAdapter(&name3,0), StringAdapter(&null,0),
		StringAdapter(&disallowsd,0)));
	assert!(!check_name_constraints2(StringAdapter(&name4,0), StringAdapter(&null,0),
		StringAdapter(&disallowsd,0)));
	assert!(check_name_constraints2(StringAdapter(&name5,0), StringAdapter(&null,0),
		StringAdapter(&disallowsd,0)));
}

#[test]
fn test_name_constraints_allow()
{
	//If just allow, the names listed are allowed
	let null: [& str; 0] = [];
	let allows: [& str; 1] = ["gov"];
	let allowsd: [& str; 1] = [".gov"];
	let name: [&str; 3] = ["a.gov", "b.gov", "c.gov"];
	let name2: [&str; 4] = ["a.gov", "b.gov", "c.gov", "banned.xgov"];
	let name3: [&str; 1] = ["gov"];
	let name4: [&str; 1] = ["xgov"];
	assert!(check_name_constraints2(StringAdapter(&name,0), StringAdapter(&allows,0),
		StringAdapter(&null,0)));
	assert!(!check_name_constraints2(StringAdapter(&name2,0), StringAdapter(&allows,0),
		StringAdapter(&null,0)));
	assert!(check_name_constraints2(StringAdapter(&name3,0), StringAdapter(&allows,0),
		StringAdapter(&null,0)));
	assert!(!check_name_constraints2(StringAdapter(&name4,0), StringAdapter(&allows,0),
		StringAdapter(&null,0)));
	assert!(check_name_constraints2(StringAdapter(&name,0), StringAdapter(&allowsd,0),
		StringAdapter(&null,0)));
	assert!(!check_name_constraints2(StringAdapter(&name2,0), StringAdapter(&allowsd,0),
		StringAdapter(&null,0)));
	assert!(check_name_constraints2(StringAdapter(&name3,0), StringAdapter(&allowsd,0),
		StringAdapter(&null,0)));
	assert!(!check_name_constraints2(StringAdapter(&name4,0), StringAdapter(&allowsd,0),
		StringAdapter(&null,0)));
}

#[test]
fn test_name_constraints_allow_disallow()
{
	let allows: [& str; 1] = ["gov"];
	let allowsd: [& str; 1] = [".gov"];
	let disallows: [& str; 1] = ["banned.gov"];
	let disallowsd: [& str; 1] = [".banned.gov"];
	let name: [&str; 3] = ["a.gov", "b.gov", "c.gov"];
	let name2: [&str; 4] = ["a.gov", "b.gov", "c.gov", "x.banned.gov"];
	let name3: [&str; 1] = ["gov"];
	let name4: [&str; 1] = ["notbanned.gov"];
	let name5: [&str; 1] = ["banned.gov"];
	assert!(check_name_constraints2(StringAdapter(&name,0), StringAdapter(&allows,0),
		StringAdapter(&disallows,0)));
	assert!(!check_name_constraints2(StringAdapter(&name2,0), StringAdapter(&allows,0),
		StringAdapter(&disallows,0)));
	assert!(check_name_constraints2(StringAdapter(&name3,0), StringAdapter(&allows,0),
		StringAdapter(&disallows,0)));
	assert!(check_name_constraints2(StringAdapter(&name4,0), StringAdapter(&allows,0),
		StringAdapter(&disallows,0)));
	assert!(!check_name_constraints2(StringAdapter(&name5,0), StringAdapter(&allows,0),
		StringAdapter(&disallows,0)));
	assert!(check_name_constraints2(StringAdapter(&name,0), StringAdapter(&allowsd,0),
		StringAdapter(&disallowsd,0)));
	assert!(!check_name_constraints2(StringAdapter(&name2,0), StringAdapter(&allowsd,0),
		StringAdapter(&disallowsd,0)));
	assert!(check_name_constraints2(StringAdapter(&name3,0), StringAdapter(&allowsd,0),
		StringAdapter(&disallowsd,0)));
	assert!(check_name_constraints2(StringAdapter(&name4,0), StringAdapter(&allowsd,0),
		StringAdapter(&disallowsd,0)));
	assert!(!check_name_constraints2(StringAdapter(&name5,0), StringAdapter(&allowsd,0),
		StringAdapter(&disallowsd,0)));
}
