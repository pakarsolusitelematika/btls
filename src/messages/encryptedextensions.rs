use super::{Debugger, ExtensionAssignments, ExtensionParseError, parse_extensions};
use ::common::{EXT_ALPN, EXT_RECORD_SIZE_LIMIT, EXT_SERVER_NAME, EXT_SUPPORTED_GROUPS, TlsFailure};
use ::stdlib::from_utf8;

use btls_aux_serialization::Source;

///The TLS EncryptedExtensions message
pub struct TlsEncryptedExtensions<'a>
{
	///The ALPN for the connection.
	pub alpn: Option<&'a str>,
	///The maximum record size.
	pub max_record_size: Option<usize>,
}


#[derive(Copy,Clone,Debug)]
enum TlsEncryptedExtensionsExt
{
	ServerName,
	SupportedGroups,
	Alpn,
	RecordSizeLimit,
}

impl ExtensionAssignments for TlsEncryptedExtensionsExt
{
	fn get_entry(extid: u16, _: bool) -> Option<Self>
	{
		Some(match extid {
			EXT_SERVER_NAME => TlsEncryptedExtensionsExt::ServerName,
			EXT_SUPPORTED_GROUPS => TlsEncryptedExtensionsExt::SupportedGroups,
			EXT_ALPN => TlsEncryptedExtensionsExt::Alpn,
			EXT_RECORD_SIZE_LIMIT => TlsEncryptedExtensionsExt::RecordSizeLimit,
			_ => return None
		})
	}
	fn get_bit(&self) -> u32
	{
		match *self {
			TlsEncryptedExtensionsExt::ServerName =>      0,
			TlsEncryptedExtensionsExt::SupportedGroups => 1,
			TlsEncryptedExtensionsExt::Alpn =>            2,
			TlsEncryptedExtensionsExt::RecordSizeLimit => 3,
		}
	}
}

impl<'a> TlsEncryptedExtensions<'a>
{
	pub fn parse(msg: &'a [u8], debugger: Debugger) -> Result<TlsEncryptedExtensions<'a>, TlsFailure>
	{
		let mut msg = Source::new(msg);
		let mut alpn = None;
		let mut max_record_size = None;
		parse_extensions(&mut msg, false, |etype, mut epayload|{
			debug!(TLS_EXTENSIONS debugger, "EncryptedExtensions: Received extension {:?} ({} bytes).",
				etype, epayload.as_slice().len());
			match etype {
				TlsEncryptedExtensionsExt::ServerName => (),	//Server_name, just ignore this.
				TlsEncryptedExtensionsExt::SupportedGroups => {
					//Just read and ignore the response
					epayload.read_slice(2..65534, TlsFailure::CantParseSupGrp)?;
				},
				TlsEncryptedExtensionsExt::Alpn => {
					//Bonus points for 3(!) nested lengths around value we want...
					let length1 = epayload.read_u16(TlsFailure::CantParseAlpn)?;
					let length2 = epayload.read_u8(TlsFailure::CantParseAlpn)?;
					fail_if!(length2 == 0, TlsFailure::AlpnEmptyProtocol);
					let alpn_raw = epayload.read_remaining();
					//The lengths can't be even near overflow.
					let len_p1 = alpn_raw.len() + 1;
					fail_if!(length1 as usize != len_p1, TlsFailure::AlpnExpectedLen1Val(len_p1,
						length1 as usize));
					fail_if!(length2 as usize != alpn_raw.len(), TlsFailure::AlpnExpectedLen2Val(
						alpn_raw.len(), length2 as usize));
					let _alpn = from_utf8(alpn_raw).map_err(|_|TlsFailure::AlpnNonUtf8Protocol)?;
					alpn = Some(_alpn);
				}
				TlsEncryptedExtensionsExt::RecordSizeLimit => {
					let maxsize = epayload.read_u16(TlsFailure::CantParseRecSizeLim)?;
					fail_if!(maxsize < 64, TlsFailure::BadRecSizeLim(maxsize as usize));
					max_record_size = Some(maxsize as usize);
				},
			}
			Ok(())
		}, |etype, _|fail!(TlsFailure::BogusEeExtension(etype))).map_err(|x|match x {
			ExtensionParseError::CantParse => TlsFailure::CantParseEe,
			ExtensionParseError::Duplicate(y) => TlsFailure::DuplicateEeExtension(y),
			ExtensionParseError::JunkAfter(y) => TlsFailure::JunkAfterEeExtension(y),
			ExtensionParseError::Wrapped(y) => y
		})?;
		fail_if!(!msg.at_end(), TlsFailure::CantParseEe);

		Ok(TlsEncryptedExtensions{
			alpn: alpn,
			max_record_size: max_record_size,
		})
	}
}
