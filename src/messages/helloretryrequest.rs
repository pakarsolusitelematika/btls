use super::{Debugger, ExtensionAssignments, ExtensionParseError, parse_extensions};
use ::common::{Ciphersuite, EXT_COOKIE, EXT_KEY_SHARE, TLS_LEN_COOKIE, TlsFailure, TlsVersion};

use btls_aux_serialization::Source;

///The TLS HelloRetryRequest message
pub struct TlsHelloRetryRequest<'a>
{
	///The version number
	pub version: TlsVersion,
	///Ciphersuite (-draft19 only).
	pub ciphersuite: Option<Ciphersuite>,
	///Key share group.
	pub kex_group: Option<u16>,
	///Cookie.
	pub cookie: Option<&'a [u8]>,
}

#[derive(Copy,Clone,Debug)]
enum TlsHelloRetryRequestExt
{
	KeyShare,
	Cookie,
}

impl ExtensionAssignments for TlsHelloRetryRequestExt
{
	fn get_entry(extid: u16, _: bool) -> Option<Self>
	{
		Some(match extid {
			EXT_KEY_SHARE => TlsHelloRetryRequestExt::KeyShare,
			EXT_COOKIE => TlsHelloRetryRequestExt::Cookie,
			_ => return None
		})
	}
	fn get_bit(&self) -> u32
	{
		match self {
			&TlsHelloRetryRequestExt::KeyShare => 0,
			&TlsHelloRetryRequestExt::Cookie =>   1,
		}
	}
}

impl<'a> TlsHelloRetryRequest<'a>
{
	///Parse HelloRetryRequest message.
	pub fn parse(msg: &'a [u8], debugger: Debugger) -> Result<TlsHelloRetryRequest<'a>, TlsFailure>
	{
		let mut msg = Source::new(msg);
		let version = msg.read_u16(TlsFailure::CantParseHrr)?;
		let version = TlsVersion::by_tls_id(version, false)?;
		let mut kex_group = None;
		let mut cookie = None;
		let mut spurious = true;
		let mut ciphersuite = None;
		if version.draft_code().at_least_19() {
			let csnum = msg.read_u16(TlsFailure::CantParseHrr)?;
			ciphersuite = Some(Ciphersuite::by_tls_id(csnum).ok_or(
				TlsFailure::BogusShCiphersuite(csnum))?);
		}
		let is_tls12 = version.is_tls12();
		let tlsver = if is_tls12 { 2 } else { 3 };

		parse_extensions(&mut msg, is_tls12, |etype, mut epayload|{
			debug!(TLS_EXTENSIONS debugger, "HelloRetryRequest: Received extension {:?} ({} bytes).",
				etype, epayload.as_slice().len());
			spurious = false;
			match etype {
				TlsHelloRetryRequestExt::KeyShare =>
					kex_group = Some(epayload.read_u16(TlsFailure::HrrExpectedKeyShare)?),
				TlsHelloRetryRequestExt::Cookie => {
					cookie = Some(epayload.read_slice(TLS_LEN_COOKIE,
						TlsFailure::HrrExpectedCookie)?);
				},
			}
			Ok(())
		}, |etype, _|fail!(TlsFailure::BogusHrrExtension(etype, tlsver))).map_err(|x|match x {
			ExtensionParseError::CantParse => TlsFailure::CantParseHrr,
			ExtensionParseError::Duplicate(y) => TlsFailure::DuplicateHrrExtension(y),
			ExtensionParseError::JunkAfter(y) => TlsFailure::JunkAfterHrrExtension(y),
			ExtensionParseError::Wrapped(y) => y
		})?;
		fail_if!(!msg.at_end(), TlsFailure::CantParseHrr);
		fail_if!(spurious, TlsFailure::BogusHrrRestart);

		Ok(TlsHelloRetryRequest{
			version: version,
			ciphersuite: ciphersuite,
			kex_group: kex_group,
			cookie: cookie,
		})
	}
}
