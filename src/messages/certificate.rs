use super::{Debugger, ExtensionAssignments, ExtensionParseError, parse_extensions, SctList};
use ::common::{CERTTYPE_RPK, CERTTYPE_X509, EXT_CT, EXT_STATUS_REQUEST, EXT_SERVER_CERTIFICATE_TYPE,
	TLS_LEN_CERT_CONTEXT, TLS_LEN_CERT_ENTRY, TLS_LEN_CERT_LIST, TLS_LEN_OCSP_RESPONSE, TlsFailure};
use ::stdlib::Vec;

use btls_aux_serialization::Source;

///The TLS 1.2 certificate message
pub struct Tls12Certificate<'a>
{
	///The certificate chain.
	pub chain: Vec<&'a [u8]>
}

impl<'a> Tls12Certificate<'a>
{
	pub fn parse(msg: &'a [u8], _debugger: Debugger, rpk: bool) -> Result<Tls12Certificate<'a>, TlsFailure>
	{
		let mut chain = Source::new(msg);
		let mut certs = Vec::new();
		chain.read_source_fn(TLS_LEN_CERT_LIST, true, |chain|{
			let cert = if rpk {
				//RPKs are whole "chain".
				chain.read_remaining()
			} else {
				chain.read_slice(TLS_LEN_CERT_ENTRY, TlsFailure::CantParseCertTls12)?
			};
			fail_if!(cert.len() == 0,  TlsFailure::CertIntermediateEmpty);
			certs.push(cert);
			Ok(())
		}, TlsFailure::CantParseCertTls12, ())?;
		fail_if!(!chain.at_end(), TlsFailure::CantParseCertTls12);
		Ok(Tls12Certificate {
			chain: certs
		})
	}
}

//The TLS 1.3 certificate message
pub struct Tls13Certificate<'a>
{
	///Is just RPK?
	pub is_rpk: bool,
	///Certificate context.
	pub context: &'a [u8],
	///Stapled OCSP response for EE cert, if any.
	pub stapled_ocsp: Option<&'a [u8]>,
	///Type of stapled OCSP response (0 if no OCSP response).
	pub stapled_ocsp_type: u8,
	///Vector of stapled SCTs (for EE cert).
	pub stapled_scts: Vec<&'a [u8]>,
	///The certificate chain.
	pub chain: Vec<&'a [u8]>
}

#[derive(Copy,Clone,Debug)]
enum TlsCertificateExt
{
	StatusRequest,
	CertificateTransparency,
	ServerCertificateType,
}

impl ExtensionAssignments for TlsCertificateExt
{
	fn get_entry(extid: u16, _: bool) -> Option<Self>
	{
		Some(match extid {
			EXT_STATUS_REQUEST => TlsCertificateExt::StatusRequest,
			EXT_CT => TlsCertificateExt::CertificateTransparency,
			EXT_SERVER_CERTIFICATE_TYPE => TlsCertificateExt::ServerCertificateType,
			_ => return None
		})
	}
	fn get_bit(&self) -> u32
	{
		match self {
			&TlsCertificateExt::StatusRequest =>            0,
			&TlsCertificateExt::CertificateTransparency =>  1,
			&TlsCertificateExt::ServerCertificateType =>    2,
		}
	}
}


impl<'a> Tls13Certificate<'a>
{
	pub fn parse(msg: &'a [u8], debugger: Debugger, no_server_type: bool) ->
		Result<Tls13Certificate<'a>, TlsFailure>
	{
		let mut chain = Source::new(msg);
		let mut certs = Vec::new();
		let mut is_rpk = false;
		let mut is_ee = true;
		let mut stapled_ocsp = None;
		let mut stapled_ocsp_type = 0;
		let mut stapled_scts = Vec::new();
		let context = chain.read_slice(TLS_LEN_CERT_CONTEXT, TlsFailure::CantParseCertTls13)?;
		chain.read_source_fn(TLS_LEN_CERT_LIST, true, |chain|{
			let cert = chain.read_slice(TLS_LEN_CERT_ENTRY, TlsFailure::CantParseCertTls13)?;
			fail_if!(cert.len() == 0, TlsFailure::CertIntermediateEmpty);
			certs.push(cert);
			parse_extensions(chain, false, |etype, epayload|{
				debug!(TLS_EXTENSIONS debugger, "Certificate: Received extension {:?} ({} bytes) \
					for certificate #{}.", etype, epayload.as_slice().len(), certs.len().
					saturating_sub(1));
				match etype {
					TlsCertificateExt::StatusRequest => {
						let stype = epayload.read_u8(TlsFailure::CantParseOcsp)?;
						let data = epayload.read_slice(TLS_LEN_OCSP_RESPONSE,
							TlsFailure::CantParseOcsp)?;
						if is_ee {
							debug!(HANDSHAKE_EVENTS debugger, "Received stapled OCSP \
								response.");
							stapled_ocsp_type = stype;
							stapled_ocsp = Some(data);
						} else {
							debug!(HANDSHAKE_EVENTS debugger, "Warning: Server sent CA \
								OCSP!");
						}
					},
					TlsCertificateExt::CertificateTransparency => {
						let scts = SctList::new(epayload.read_remaining()).map_err(|_|
							TlsFailure::CantParseSct)?;
						if is_ee {
							debug!(HANDSHAKE_EVENTS debugger, "Received {} new SCTs \
								via TLS extension.", scts.iter().count());
							stapled_scts.extend(scts.iter());
						} else {
							//We don't support CA CT.
							debug!(HANDSHAKE_EVENTS debugger, "Unimplemented: Server \
								Sent CA CT. Ignored.");
						}
					},
					TlsCertificateExt::ServerCertificateType if !no_server_type => {
						if is_ee {
							let ctype = epayload.read_u8(
								TlsFailure::CantParseSCertType)?;
							if ctype == CERTTYPE_RPK {
								is_rpk = true;
								debug!(HANDSHAKE_EVENTS debugger, "Server sent \
									just its public key");
							} else if ctype == CERTTYPE_X509 {
								//Default.
							} else {
								fail!(TlsFailure::BogusSCertType(ctype));
							};
						} else {
							fail!(TlsFailure::SCertTypeInNotFirstCert);
						}
					},
					TlsCertificateExt::ServerCertificateType => {
						debug!(HANDSHAKE_EVENTS debugger, "Ignoring server_cert_type in \
							certificate");
					}
				}
				Ok(())
			}, |etype, _|fail!(TlsFailure::BogusCertExtension(etype))).map_err(|x|match x {
				ExtensionParseError::CantParse => TlsFailure::CantParseCertTls13,
				ExtensionParseError::Duplicate(y) => TlsFailure::DuplicateCertExtension(y),
				ExtensionParseError::JunkAfter(y) => TlsFailure::JunkAfterCertExtension(y),
				ExtensionParseError::Wrapped(y) => y
			})?;
			is_ee = false;
			Ok(())
		}, TlsFailure::CantParseCertTls13, ())?;
		fail_if!(!chain.at_end(), TlsFailure::CantParseCertTls13);
		Ok(Tls13Certificate {
			is_rpk: is_rpk,
			context: context,
			stapled_ocsp: stapled_ocsp,
			stapled_ocsp_type: stapled_ocsp_type,
			stapled_scts: stapled_scts,
			chain: certs
		})
	}
}
