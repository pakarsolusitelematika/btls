use super::{Debugger, ExtensionAssignments, ExtensionParseError, parse_extensions};
use ::common::{BAD_CIPHERSUITES, Ciphersuite, CERTTYPE_RPK, CERTTYPE_X509, CS_FALLBACK_FIX_HACK,
	CS_RENEGO_FIX_HACK, EXT_ALPN, EXT_CT, EXT_EMS, EXT_KEY_SHARE, EXT_RECORD_SIZE_LIMIT, EXT_RENEGO_INFO,
	EXT_SERVER_CERTIFICATE_TYPE, TLS_LEN_COMPRESS_LIST, EXT_SERVER_NAME, EXT_SIGNATURE_ALGORITHMS,
	EXT_STATUS_REQUEST, EXT_SUPPORTED_GROUPS, EXT_SUPPORTED_VERSIONS, TLS_LEN_ALPN_ENTRY, TLS_LEN_ALPN_LIST,
	TLS_LEN_CIPHERSUITE_LIST, TLS_LEN_SCERTT_LIST, TLS_LEN_SNI_ENTRY, TLS_LEN_SNI_LIST, TLS_LEN_TLS13_PUBKEY,
	TLS_LEN_TLS13_PUBKEY_LIST, TlsFailure, TlsVersion};
use ::stdlib::{from_utf8, max};

use btls_aux_dhf::Dhf;
use btls_aux_serialization::Source;
use btls_aux_signature_algo::SignatureType;

///List of ciphersuites.
#[derive(Copy,Clone)]
pub struct CiphersuiteList<'a>(&'a [u8]);
///Iterator in list of ciphersuites.
pub struct CiphersuiteListIterator<'a>(Source<'a>);

///List of ALPN values.
#[derive(Copy,Clone)]
pub struct AlpnList<'a>(&'a [u8]);
///Iterator in list of ALPN values.
pub struct AlpnListIterator<'a>(Source<'a>);

///List of Key Shares.
#[derive(Copy,Clone)]
pub struct KeyShareList<'a>(&'a [u8]);
///Iterator in list of Key shares.
pub struct KeyShareListIterator<'a>(Source<'a>);


impl<'a> CiphersuiteList<'a>
{
	fn blank() -> CiphersuiteList<'a>
	{
		CiphersuiteList(&[])
	}
	fn new(mut src: &mut Source<'a>, generic_error: TlsFailure, renego_hack: &mut bool,
		fallback_hack: &mut bool, bad_ciphers: &mut bool) -> Result<CiphersuiteList<'a>, TlsFailure>
	{
		let slice = src.as_slice();
		while !src.at_end() {
			let cs = src.read_u16(generic_error.clone())?;
			fail_if!(cs == 0, TlsFailure::Ciphersuite0);
			*bad_ciphers |= (&BAD_CIPHERSUITES[..]).binary_search(&cs).is_ok();
			*renego_hack |= cs == CS_RENEGO_FIX_HACK;
			*fallback_hack |= cs == CS_FALLBACK_FIX_HACK;
		}
		Ok(CiphersuiteList(slice))
	}
	pub fn iter(&self) -> CiphersuiteListIterator<'a>
	{
		CiphersuiteListIterator(Source::new(self.0))
	}
}

impl<'a> Iterator for CiphersuiteListIterator<'a>
{
	type Item = Ciphersuite;
	fn next(&mut self) -> Option<Ciphersuite>
	{
		while !self.0.at_end() {
			let cs = match self.0.read_u16(()) { Ok(x) => x, Err(_) => return None };
			if let Some(_cs) = Ciphersuite::by_tls_id(cs) {
				return Some(_cs);
			}
		}
		None
	}
}

impl<'a> AlpnList<'a>
{
	///Create a new ALPN list.
	fn new(mut src: Source<'a>) -> Result<AlpnList<'a>, TlsFailure>
	{
		let slice = src.as_slice();
		while !src.at_end() {
			//Don't care about the names, just don't be empty.
			src.read_slice(TLS_LEN_ALPN_ENTRY, TlsFailure::CantParseAlpn)?;
		}
		Ok(AlpnList(slice))
	}
	///Make iterator (yielding `&'a str`) out of ALPN list.
	pub fn iter(&self) -> AlpnListIterator<'a>
	{
		AlpnListIterator(Source::new(self.0))
	}
}

impl<'a> Iterator for AlpnListIterator<'a>
{
	type Item = &'a str;
	fn next(&mut self) -> Option<&'a str>
	{
		while !self.0.at_end() {
			let alp = match self.0.read_slice(TLS_LEN_ALPN_ENTRY, ()) {
				Ok(x) => x,
				Err(_) => return None
			};
			match from_utf8(alp) { Ok(x) => return Some(x), Err(_) => continue };
		}
		None
	}
}

impl<'a> KeyShareList<'a>
{
	fn new(mut src: Source<'a>) -> Result<KeyShareList<'a>, TlsFailure>
	{
		let slice = src.as_slice();
		while !src.at_end() {
			//Just check share is not empty.
			src.read_u16(TlsFailure::CantParseKeyShare)?;
			let share = src.read_slice(TLS_LEN_TLS13_PUBKEY, TlsFailure::CantParseKeyShare)?;
			fail_if!(share.len() == 0, TlsFailure::CantParseKeyShare);
		}
		Ok(KeyShareList(slice))
	}
	pub fn iter(&self) -> KeyShareListIterator<'a>
	{
		KeyShareListIterator(Source::new(self.0))
	}
}

impl<'a> Iterator for KeyShareListIterator<'a>
{
	type Item = (Dhf, &'a [u8]);
	fn next(&mut self) -> Option<(Dhf, &'a [u8])>
	{
		while !self.0.at_end() {
			let group = match self.0.read_u16(()) { Ok(x) => x, Err(_) => return None };
			let share = match self.0.read_slice(TLS_LEN_TLS13_PUBKEY, ()) {
				Ok(x) => x,
				Err(_) => return None
			};
			let group = match Dhf::by_tls_id(group) { Some(x) => x, None => continue };
			return Some((group, share))
		}
		None
	}
}

///The TLS ClientHello message
pub struct TlsClientHello<'a>
{
	pub client_version: TlsVersion,
	pub random: [u8; 32],
	pub ciphersuites: CiphersuiteList<'a>,
	pub fallback_hack: bool,
	pub renego_ok: bool,
	pub certtypes: u8,
	pub host_name: Option<&'a str>,
	pub supported_groups_mask: Option<u64>,
	pub signature_algorithms_mask: Option<u64>,
	pub tls12_supported: bool,
	pub tls13_variant: Option<u8>,
	pub sct_offered: bool,
	pub ocsp_offered: bool,
	pub ems_offered: bool,
	pub record_size_limit: Option<u16>,
	pub key_shares: Option<KeyShareList<'a>>,
	pub alpn_list: Option<AlpnList<'a>>,
	pub bad_ciphers: bool,
}


#[derive(Copy,Clone,Debug)]
enum TlsClientHelloExt
{
	ServerName,
	StatusRequest,
	SupportedGroups,
	SignatureAlgorithms,
	ApplicationLayerProtocolNegotiation,
	SignedCertificateTimestamp,
	ServerCertificateType,
	ExtendedMasterSecret,
	KeyShare,
	SupportedVersions,
	RenegotiationInfo,
	RecordSizeLimit,
}

impl ExtensionAssignments for TlsClientHelloExt
{
	fn get_entry(extid: u16, _: bool) -> Option<Self>
	{
		Some(match extid {
			EXT_SERVER_NAME => TlsClientHelloExt::ServerName,
			EXT_STATUS_REQUEST => TlsClientHelloExt::StatusRequest,
			EXT_SUPPORTED_GROUPS => TlsClientHelloExt::SupportedGroups,
			EXT_SIGNATURE_ALGORITHMS => TlsClientHelloExt::SignatureAlgorithms,
			EXT_ALPN => TlsClientHelloExt::ApplicationLayerProtocolNegotiation,
			EXT_CT => TlsClientHelloExt::SignedCertificateTimestamp,
			EXT_SERVER_CERTIFICATE_TYPE => TlsClientHelloExt::ServerCertificateType,
			EXT_EMS => TlsClientHelloExt::ExtendedMasterSecret,
			EXT_KEY_SHARE => TlsClientHelloExt::KeyShare,
			EXT_SUPPORTED_VERSIONS => TlsClientHelloExt::SupportedVersions,
			EXT_RENEGO_INFO => TlsClientHelloExt::RenegotiationInfo,
			EXT_RECORD_SIZE_LIMIT => TlsClientHelloExt::RecordSizeLimit,
			_ => return None
		})
	}
	fn get_bit(&self) -> u32
	{
		match *self {
			TlsClientHelloExt::ServerName =>                          0,
			TlsClientHelloExt::StatusRequest =>                       1,
			TlsClientHelloExt::SupportedGroups =>                     2,
			TlsClientHelloExt::SignatureAlgorithms =>                 3,
			TlsClientHelloExt::ApplicationLayerProtocolNegotiation => 4,
			TlsClientHelloExt::SignedCertificateTimestamp =>          5,
			TlsClientHelloExt::ServerCertificateType =>               6,
			TlsClientHelloExt::ExtendedMasterSecret =>                7,
			TlsClientHelloExt::KeyShare =>                            8,
			TlsClientHelloExt::SupportedVersions =>                   9,
			TlsClientHelloExt::RenegotiationInfo =>                  10,
			TlsClientHelloExt::RecordSizeLimit =>                    11,
		}
	}
}

impl<'a> TlsClientHello<'a>
{
	fn ext_server_name(epayload: &mut Source<'a>, host_name: &mut Option<&'a str>) -> Result<(), TlsFailure>
	{
		let mut seen_hostname = false;
		epayload.read_source_fn(TLS_LEN_SNI_LIST, true, |payload|{
			let ntype = payload.read_u8(TlsFailure::CantParseServerName)?;
			//This is limit-0 instead of limit-1, because 1-limit only appiles to host_name.
			let nval = payload.read_slice(TLS_LEN_SNI_ENTRY, TlsFailure::CantParseServerName)?;
			match ntype {
				0 => {
					//host_name.
					fail_if!(seen_hostname, TlsFailure::CantParseServerName);
					seen_hostname = true;
					fail_if!(nval.len() < 1 || nval.len() > 255, TlsFailure::ServerNameBadHost);
					*host_name = Some(from_utf8(nval).map_err(|_|TlsFailure::ServerNameBadHost)?);
				},
				_ => (),	//Skip unknown.
			}
			Ok(())
		}, TlsFailure::CantParseServerName, ())?;
		Ok(())
	}
	///Parse HelloRetryRequest message.
	pub fn parse(msg: &'a [u8], debugger: Debugger) -> Result<TlsClientHello<'a>, TlsFailure>
	{
		let mut renego_ok = false;
		let mut fallback_hack = false;
		let mut record_size_limit = None;
		let mut ems_offered = false;
		let mut ocsp_offered = false;
		let mut sct_offered = false;
		let mut tls12_supported = true;		//We only accept at least TLS 1.2 ClientVersion.
		let mut tls13_variant = None;
		let mut certtypes = 1;			//Assume X.509 OK.
		let mut supported_groups_mask = None;
		let mut signature_algorithms_mask = None;
		let mut host_name = None;
		let mut supported_groups_src = None;
		let mut key_shares_src = None;
		let mut alpn_list = None;
		let mut key_shares = None;
		let mut bad_ciphers = false;

		let mut msg = Source::new(msg);
		//Client version
		let client_version = msg.read_u16(TlsFailure::CantParseCh)?;
		let client_version = TlsVersion::by_tls_id(client_version, true)?;
		//Random.
		let mut random = [0u8; 32];
		msg.read_array(&mut random, TlsFailure::CantParseCh)?;
		//Session id (max 32 bytes).
		msg.read_slice(..32, TlsFailure::CantParseCh)?;
		//Cipher suites.
		let ciphersuites = msg.read_source_fn(TLS_LEN_CIPHERSUITE_LIST, false, |ciphersuites|{
			CiphersuiteList::new(ciphersuites, TlsFailure::CantParseCh, &mut renego_ok,
				&mut fallback_hack, &mut bad_ciphers)
		}, TlsFailure::CantParseCh, CiphersuiteList::blank())?;
		//Compression methods. There MUST be method 0 (NULL) supported. Furthermore, in TLS 1.3, this
		//MUST be [0].
		let compressions = msg.read_slice(TLS_LEN_COMPRESS_LIST, TlsFailure::CantParseCh)?;
		let not_tls13_compliant = compressions != [0];
		let supports_null = {
			let mut found = false;
			for i in compressions { found |= *i == 0 }
			found
		};
		fail_if!(!supports_null, TlsFailure::ClientDoesNotSupportUncompressed);
		//Extensions.
		parse_extensions(&mut msg, false, |etype, mut epayload|{
			debug!(TLS_EXTENSIONS debugger, "ClientHello: Received extension {:?} ({} bytes).",
				etype, epayload.as_slice().len());
			match etype {
				TlsClientHelloExt::ServerName => Self::ext_server_name(&mut epayload,
					&mut host_name)?,
				TlsClientHelloExt::StatusRequest => {
					ocsp_offered = epayload.read_remaining() == &[1, 0, 0, 0, 0];
				},
				TlsClientHelloExt::SupportedGroups => {
					let mut tmp = 0;
					let mut payload = epayload.read_source(2..65534,
						TlsFailure::CantParseSupGrp)?;
					supported_groups_src = Some(payload.clone());
					while !payload.at_end() {
						let grp = payload.read_u16(TlsFailure::CantParseSupGrp)?;
						let grp = match Dhf::by_tls_id(grp) {
							Some(x) => x,
							None => continue,
						};
						tmp |= grp.dhfalgo_const();
					}
					supported_groups_mask = Some(tmp);
				},
				TlsClientHelloExt::SignatureAlgorithms => {
					let mut tmp = 0;
					let mut payload = epayload.read_source(2..65534,
						TlsFailure::CantParseSigAlgo)?;
					while !payload.at_end() {
						let sigalgo = payload.read_u16(TlsFailure::CantParseSigAlgo)?;
						let sigalgo = match SignatureType::by_tls_id(sigalgo) {
							Some(x) => x,
							None => continue,
						};
						tmp |= sigalgo.sigalgo_const();
					}
					signature_algorithms_mask = Some(tmp);
				},
				TlsClientHelloExt::ApplicationLayerProtocolNegotiation => {
					let payload = epayload.read_source(TLS_LEN_ALPN_LIST,
						TlsFailure::CantParseAlpn)?;
					alpn_list = Some(AlpnList::new(payload)?)
				},
				TlsClientHelloExt::SignedCertificateTimestamp => {
					sct_offered = true;
				},
				TlsClientHelloExt::ServerCertificateType => {
					certtypes = 0;		//X.509 might not be OK.
					let mut payload = epayload.read_source(TLS_LEN_SCERTT_LIST,
						TlsFailure::CantParseSCertType)?;
					while !payload.at_end() {
						match payload.read_u8(TlsFailure::CantParseSCertType)? {
							CERTTYPE_X509 => certtypes |= 1,
							CERTTYPE_RPK => certtypes |= 2,
							_ => (),	//Ignore unknown.
						};
					}
				},
				TlsClientHelloExt::ExtendedMasterSecret => {
					ems_offered = true;
				},
				TlsClientHelloExt::KeyShare => {
					let payload = epayload.read_source(TLS_LEN_TLS13_PUBKEY_LIST,
						TlsFailure::CantParseKeyShare)?;
					key_shares_src = Some(payload.clone());
					key_shares = Some(KeyShareList::new(payload)?);
				},
				TlsClientHelloExt::SupportedVersions => {
					tls12_supported = false;	//Might not actually be.
					let mut maxvariant = 0;
					let mut payload = epayload.read_source(2..254,
						TlsFailure::CantParseSupVer)?;
					while !payload.at_end() {
						let ver = payload.read_u16(TlsFailure::CantParseSupVer)?;
						match ver {
							0x0303 => tls12_supported = true,
							0x0304 => maxvariant = max(maxvariant, 255),
							_ => ()		//Ignore unknown.
						};
						for i in TlsVersion::supported_tls13().iter() {
							if ver == i.to_code() {
								maxvariant = max(maxvariant, i.draft_code().
									into_inner());
							}
						}
					}
					if maxvariant > 0 { tls13_variant = Some(maxvariant); }
				},
				TlsClientHelloExt::RenegotiationInfo => {
					fail_if!(epayload.read_remaining() != &[0],
						TlsFailure::RenegotiationAttackDetected);
					renego_ok = true;
				},
				TlsClientHelloExt::RecordSizeLimit => {
					let maxsize = epayload.read_u16(TlsFailure::CantParseRecSizeLim)?;
					fail_if!(maxsize < 64, TlsFailure::BadRecSizeLim(maxsize as usize));
					record_size_limit = Some(maxsize);
				},
			}
			Ok(())
		}, |ekind, epayload|{
			debug!(TLS_EXTENSIONS debugger, "ClientHello: Received unknown extension {} ({} bytes).",
				ekind, epayload.as_slice().len());
			Ok(())
		}).map_err(|x|match x {
			ExtensionParseError::CantParse => TlsFailure::CantParseCh,
			ExtensionParseError::Duplicate(y) => TlsFailure::DuplicateChExtension(y),
			ExtensionParseError::JunkAfter(y) => TlsFailure::JunkAfterChExtension(y),
			ExtensionParseError::Wrapped(y) => y
		})?;
		fail_if!(!msg.at_end(), TlsFailure::CantParseCh);

		fail_if!(not_tls13_compliant && tls13_variant.is_some(), TlsFailure::BogusChCompressionMethodsTls13);
		fail_if!(tls13_variant.is_some() && !key_shares.is_some(), TlsFailure::Tls13NoKeyShare);

		//If key_shares is present, supported_groups has to be too. Furthermore, those need to have subset
		//relationship.
		fail_if!(key_shares_src.is_some() && supported_groups_src.is_none(),
			TlsFailure::KeySharesNotSubsetSupGrp);
		if let (Some(mut ssrc), Some(mut ksrc)) = (supported_groups_src, key_shares_src) {
			while !ksrc.at_end() {
				let group = ksrc.read_u16(TlsFailure::CantParseKeyShare)?;
				ksrc.read_slice(TLS_LEN_TLS13_PUBKEY, TlsFailure::CantParseKeyShare)?;
				//Scanning ssrc forward, we should find this group.
				while !ssrc.at_end() {
					let group2 = ssrc.read_u16(TlsFailure::CantParseSupGrp)?;
					if group == group2 { break; }
				}
				fail_if!(ssrc.at_end(), TlsFailure::KeySharesNotSubsetSupGrp);
			}
		}

		Ok(TlsClientHello {
			client_version: client_version,
			random: random,
			ciphersuites: ciphersuites,
			fallback_hack: fallback_hack,
			renego_ok: renego_ok,
			host_name: host_name,
			certtypes: certtypes,
			supported_groups_mask: supported_groups_mask,
			signature_algorithms_mask: signature_algorithms_mask,
			tls12_supported: tls12_supported,
			tls13_variant: tls13_variant,
			sct_offered: sct_offered,
			ocsp_offered: ocsp_offered,
			ems_offered: ems_offered,
			record_size_limit: record_size_limit,
			alpn_list: alpn_list,
			key_shares: key_shares,
			bad_ciphers: bad_ciphers,
		})
	}
}
