use ::common::{TLS_LEN_EXTENSION, TLS_LEN_EXTENSIONS, TLS_LEN_SCT_ENTRY, TLS_LEN_SCT_LIST, TLS_LEN_SIGNATURE,
	TlsFailure};
use ::logging::Logging;
use ::stdlib::{Arc, Box};

use btls_aux_serialization::Source;
use btls_aux_signature::{FromDigitallySigned, SignatureBlock};
use btls_aux_signature_algo::SignatureType;


mod certificate;
pub use self::certificate::{Tls12Certificate, Tls13Certificate};
mod certificaterequest;
pub use self::certificaterequest::Tls13CertificateRequest;
mod certificatestatus;
pub use self::certificatestatus::TlsCertificateStatus;
mod certificateverify;
pub use self::certificateverify::TlsCertificateVerify;
mod clienthello;
pub use self::clienthello::{AlpnList, AlpnListIterator, KeyShareList, TlsClientHello};
mod clientkeyexchange;
pub use self::clientkeyexchange::TlsClientKeyExchange;
mod encryptedextensions;
pub use self::encryptedextensions::TlsEncryptedExtensions;
mod helloretryrequest;
pub use self::helloretryrequest::TlsHelloRetryRequest;
mod keyupdate;
pub use self::keyupdate::TlsKeyUpdate;
mod serverhello;
pub use self::serverhello::TlsServerHello;
mod serverkeyexchange;
pub use self::serverkeyexchange::TlsServerKeyExchange;

type Debugger = Option<(u64, Arc<Box<Logging+Send+Sync>>)>;

///TLS DigitallySigned structure
#[derive(Copy,Clone,Debug)]
pub struct DigitallySigned<'a>
{
	///The algorithm.
	pub algorithm: SignatureType,
	///The signature value.
	pub signature: &'a [u8],
}

///Read error for DigitallySigned structure.
#[derive(Copy,Clone,Debug)]
pub enum DigitallySignedReadError
{
	///Can't parse.
	CantParse,
	///Bogus algorithm.
	BogusAlgorithm(u16),
}

impl<'a> FromDigitallySigned<'a> for DigitallySigned<'a>
{
	fn get_algorithm(self) -> SignatureType { self.algorithm }
	fn get_payload(self) -> &'a [u8] { self.signature }
}

impl<'a> DigitallySigned<'a>
{
	pub fn parse(msg: &mut Source<'a>) -> Result<DigitallySigned<'a>, DigitallySignedReadError>
	{
		let (sigtype, sigdata) = {
			let sigalgo_id = msg.read_u16(DigitallySignedReadError::CantParse)?;
			let sigalgo = match SignatureType::by_tls_id(sigalgo_id) {
				Some(y) => y,
				None => fail!(DigitallySignedReadError::BogusAlgorithm(sigalgo_id))
			};
			let signature = msg.read_slice(TLS_LEN_SIGNATURE, DigitallySignedReadError::CantParse)?;
			(sigalgo, signature)
		};
		Ok(DigitallySigned {
			algorithm: sigtype,
			signature: sigdata,
		})
	}
	///Validate a signature.
	pub fn verify(self, key: &[u8], tbs: &[u8], flags: u32) -> Result<(), ()>
	{
		SignatureBlock::from3(self).verify(key, tbs, flags)
	}
}

///List of SCTs.
#[derive(Copy,Clone)]
pub struct SctList<'a>(&'a [u8]);
///Iterator in list of SCTs.
pub struct SctListIterator<'a>(Source<'a>);

impl<'a> SctList<'a>
{
	pub fn empty(&self) -> bool
	{
		self.0.len() == 0
	}
	pub fn blank() -> SctList<'a>
	{
		SctList(&[])
	}
	pub fn new(data: &'a[u8]) -> Result<SctList<'a>, ()>
	{
		let mut tsrc = Source::new(data);
		let mut src = tsrc.read_source(TLS_LEN_SCT_LIST, ())?;
		fail_if!(!tsrc.at_end(), ());
		let slice = src.as_slice();
		while !src.at_end() {
			//Don't care about the contents, just don't be empty.
			src.read_slice(TLS_LEN_SCT_ENTRY, ())?;
		}
		Ok(SctList(slice))
	}
	pub fn iter(&self) -> SctListIterator<'a>
	{
		SctListIterator(Source::new(self.0))
	}
}

impl<'a> Iterator for SctListIterator<'a>
{
	type Item = &'a [u8];
	fn next(&mut self) -> Option<&'a [u8]>
	{
		self.0.read_slice(TLS_LEN_SCT_ENTRY, ()).ok()
	}
}

///Trait of extension recognizer.
pub trait ExtensionAssignments: Sized
{
	///Get the extension entry corresponding to extension id, or None if none is known.
	fn get_entry(ext_id: u16, tls12: bool) -> Option<Self>;
	///Get the extension flag bit number corresponding to extension id.
	fn get_bit(&self) -> u32;
}

///Error from extension parsing.
#[derive(Clone)]
pub enum ExtensionParseError
{
	///The message can't be parsed,
	CantParse,
	///Duplicate extension.
	Duplicate(u16),
	///Junk after extension.
	JunkAfter(u16),
	///Wrapped error from extension parser.
	Wrapped(TlsFailure)
}

fn parse_extensions<'a,EKindType:ExtensionAssignments,Process,Unknown>(data: &mut Source<'a>, tls12: bool,
	mut process: Process, mut unknown: Unknown) -> Result<(), ExtensionParseError> where
	Process: FnMut(EKindType, &mut Source<'a>) -> Result<(), TlsFailure>, Unknown: FnMut(u16, &mut Source<'a>) ->
	Result<(), TlsFailure>
{
	let mut present = 0u64;
	data.read_source_fn(TLS_LEN_EXTENSIONS, true, |extensions|{
		let ekind = extensions.read_u16(ExtensionParseError::CantParse)?;
		let mut epayload = extensions.read_source(TLS_LEN_EXTENSION, ExtensionParseError::CantParse)?;
		let ekind2 = match EKindType::get_entry(ekind, tls12) {
			Some(x) => x,
			None => return unknown(ekind, &mut epayload).map_err(|x|ExtensionParseError::Wrapped(x))
		};
		let mask = 1 << (ekind2.get_bit() & 63);
		fail_if!(present & mask != 0, ExtensionParseError::Duplicate(ekind));
		present |= mask;
		process(ekind2, &mut epayload).map_err(|x|ExtensionParseError::Wrapped(x))?;
		fail_if!(!epayload.at_end(), ExtensionParseError::JunkAfter(ekind));
		Ok(())
	}, ExtensionParseError::CantParse, ())
}
