use ::common::{TLS_LEN_TLS12_PUBKEY, TlsFailure};

use btls_aux_serialization::Source;

///The TLS CertificateStatus message
pub struct TlsClientKeyExchange<'a>
{
	///The public key
	pub pubkey: &'a [u8]
}

impl<'a> TlsClientKeyExchange<'a>
{
	pub fn parse(msg: &'a [u8]) -> Result<TlsClientKeyExchange<'a>, TlsFailure>
	{
		let mut msg = Source::new(msg);
		let pubkey = {
			let pubkey = msg.read_slice(TLS_LEN_TLS12_PUBKEY, TlsFailure::CantParseCke)?;
			fail_if!(pubkey.len() == 0, TlsFailure::CantParseCke);
			fail_if!(!msg.at_end(), TlsFailure::CantParseCke);
			pubkey
		};
		Ok(TlsClientKeyExchange {
			pubkey: pubkey,
		})
	}
}
