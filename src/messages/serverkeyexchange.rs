use super::{DigitallySigned, DigitallySignedReadError as DSRE};
use ::common::{TLS_LEN_TLS12_PUBKEY, TlsFailure};

use btls_aux_dhf::Dhf;
use btls_aux_serialization::Source;

///The TLS CertificateStatus message
pub struct TlsServerKeyExchange<'a>
{
	///The response type.
	pub group: Dhf,
	///The response payload.
	pub pubkey: &'a [u8],
	///Raw share for signature.
	pub raw_share: &'a [u8],
	///Signature.
	pub signature: DigitallySigned<'a>,
}

impl<'a> TlsServerKeyExchange<'a>
{
	pub fn parse(msg: &'a [u8]) -> Result<TlsServerKeyExchange<'a>, TlsFailure>
	{
		let mut msg = Source::new(msg);
		let (group, pubkey, raw_share, signature) = {
			let marker = msg.slice_marker();
			//Must be 0x03, signinfing named group.
			match msg.read_u8(TlsFailure::CantParseSke)? {
				3 => (),
				x => fail!(TlsFailure::BogusSkeCurveType(x)),
			};
			let group = msg.read_u16(TlsFailure::CantParseSke)?;
			let group = match Dhf::by_tls_id(group) {
				Some(y) => y,
				None => fail!(TlsFailure::BogusSkeCurve(group))
			};
			let pubkey = msg.read_slice(TLS_LEN_TLS12_PUBKEY, TlsFailure::CantParseSke)?;
			fail_if!(pubkey.len() == 0, TlsFailure::CantParseSke);
			let raw_share = marker.commit(&mut msg);

			//Decode the signature.
			let signature = match DigitallySigned::parse(&mut msg) {
				Ok(x) => x,
				Err(DSRE::CantParse) => fail!(TlsFailure::CantParseSke),
				Err(DSRE::BogusAlgorithm(x)) => fail!(TlsFailure::BogusSkeSignatureAlgo(x)),
			};
			fail_if!(!msg.at_end(), TlsFailure::CantParseSke);
			(group, pubkey, raw_share, signature)
		};
		Ok(TlsServerKeyExchange {
			group: group,
			pubkey: pubkey,
			raw_share: raw_share,
			signature: signature,
		})
	}
}
