use super::{Debugger, ExtensionAssignments, ExtensionParseError, parse_extensions};
use ::common::{EXT_CT, EXT_SIGNATURE_ALGORITHMS, EXT_STATUS_REQUEST, TLS_LEN_CERT_CONTEXT, TlsFailure};

use btls_aux_serialization::Source;
use btls_aux_signature_algo::SignatureType;

///The TLS 1.3 CertificateRequest message
pub struct Tls13CertificateRequest<'a>
{
	pub context: &'a [u8],
	pub status_request: bool,
	pub sct_request: bool,
	pub signature_algorithms: u64,
}

#[derive(Copy,Clone,Debug)]
enum TlsCertificateRequestExt
{
	SignatureAlgorithms,
	StatusRequest,
	SignedCertificateTimestamp,
}

impl ExtensionAssignments for TlsCertificateRequestExt
{
	fn get_entry(extid: u16, _: bool) -> Option<Self>
	{
		Some(match extid {
			EXT_SIGNATURE_ALGORITHMS => TlsCertificateRequestExt::SignatureAlgorithms,
			EXT_STATUS_REQUEST => TlsCertificateRequestExt::StatusRequest,
			EXT_CT => TlsCertificateRequestExt::SignedCertificateTimestamp,
			_ => return None
		})
	}
	fn get_bit(&self) -> u32
	{
		match *self {
			TlsCertificateRequestExt::StatusRequest =>              0,
			TlsCertificateRequestExt::SignedCertificateTimestamp => 1,
			TlsCertificateRequestExt::SignatureAlgorithms =>        2,
		}
	}
}

impl<'a> Tls13CertificateRequest<'a>
{
	pub fn parse(msg: &'a [u8], debugger: Debugger) -> Result<Tls13CertificateRequest<'a>, TlsFailure>
	{
		let mut msg = Source::new(msg);
		let mut status_request = false;
		let mut sct_request = false;
		let mut signature_algorithms_mask = 0;
		let context = msg.read_slice(TLS_LEN_CERT_CONTEXT, TlsFailure::CantParseCr)?;
		//Everything else is a extension block! Do rough pass (it is offer, so we can se whatever).
		parse_extensions(&mut msg, false, |etype, epayload|{
			debug!(TLS_EXTENSIONS debugger, "CertificateRequest: Received extension {:?} ({} bytes).",
				etype, epayload.as_slice().len());
			match etype {
				TlsCertificateRequestExt::StatusRequest => status_request = true,
				TlsCertificateRequestExt::SignedCertificateTimestamp => sct_request = true,
				TlsCertificateRequestExt::SignatureAlgorithms => {
					let mut payload = epayload.read_source(2..65534,
						TlsFailure::CantParseSigAlgo)?;
					while !payload.at_end() {
						let sigalgo = payload.read_u16(TlsFailure::CantParseSigAlgo)?;
						let sigalgo = match SignatureType::by_tls_id(sigalgo) {
							Some(x) => x,
							None => continue,
						};
						signature_algorithms_mask |= sigalgo.sigalgo_const();
					}
				},
			}
			Ok(())
		}, |ekind, epayload|{
			debug!(TLS_EXTENSIONS debugger, "CertificateRequest: Received unknown extension {} \
				({} bytes).", ekind, epayload.as_slice().len());
			Ok(())
		}).map_err(|x|match x {
			ExtensionParseError::CantParse => TlsFailure::CantParseCr,
			ExtensionParseError::Duplicate(y) => TlsFailure::DuplicateCertReqExtension(y),
			ExtensionParseError::JunkAfter(y) => TlsFailure::JunkAfterCertReqExtension(y),
			ExtensionParseError::Wrapped(y) => y
		})?;
		fail_if!(!msg.at_end(), TlsFailure::CantParseCr);
		Ok(Tls13CertificateRequest {
			context: context,
			status_request: status_request,
			sct_request: sct_request,
			signature_algorithms: signature_algorithms_mask,
		})
	}
}
