use super::{RecordDeprotector, RecordProtector, TlsExtractor};
use ::common::{CLIENT_FINISHED_LABEL, Debugger, HkdfLabel, SERVER_FINISHED_LABEL, Tls13Draft, TlsFailure};
use ::logging::bytes_as_hex_block;
use ::stdlib::{Box, Deref, DerefMut};
use ::utils::slice_eq_ct;

use btls_aux_aead::ProtectorType;
use btls_aux_dhf::{Dhf, DhfOutput, DiffieHellmanKey};
use btls_aux_hash::{HashFunction, HashInputBlock, HashOutput, MAX_HASH_INPUT, MAX_HASH_OUTPUT};
use btls_aux_securebuf::{SecStackBuffer, wipe_buffer};

const MS_LEN: usize = 48;
const RANDOM_LEN: usize = 32;
const MAX_KEY_CHUNK: usize = 44;	//32 bytes key material + 12 bytes IV.

//These traits are not exported outside record module, and the set protector thingies take these.
pub struct ProtectorWrapper(Box<RecordProtector+Send>);
pub struct DeprotectorWrapper(Box<RecordDeprotector+Send>);
pub struct ExtractorWrapper(Box<TlsExtractor+Send>);

impl ProtectorWrapper { pub fn into_inner(self) -> Box<RecordProtector+Send> { self.0 }}
impl DeprotectorWrapper { pub fn into_inner(self) -> Box<RecordDeprotector+Send> { self.0 }}
impl ExtractorWrapper { pub fn into_inner(self) -> Box<TlsExtractor+Send> { self.0 }}

macro_rules! create_protector {
	($debug:expr, $prot:expr, $keyblock:expr, $keylen:expr, $aes_iv:expr) =>  {{
		sanity_check!($keylen <= $keyblock.len(), "Key block too small ({}, need {})", $keyblock.len(),
			$keylen);
		let key = &$keyblock[..$keylen];
		debug!(CRYPTO_CALCS $debug, "Creating protector:\n>type: {:?}\n>key:\n{}", $prot,
			bytes_as_hex_block(key, ">>"));
		let error = assert_failure!("Can't create record protector");
		match $prot {
			ProtectorType::Aes128Gcm => {
				Box::new(Protector::new_aes128(key).map_err(|_|error.clone())?)
			},
			ProtectorType::Aes256Gcm => {
				Box::new(Protector::new_aes256(key).map_err(|_|error.clone())?)
			},
			ProtectorType::Chacha20Poly1305 => {
				Box::new(Protector::new_chacha20(key).map_err(|_|error.clone())?)
			},
		}
	}}
}

macro_rules! create_deprotector {
	($debug:expr, $prot:expr, $keyblock:expr, $keylen:expr, $aes_iv:expr) =>  {{
		sanity_check!($keylen <= $keyblock.len(), "Key block too small ({}, need {})", $keyblock.len(),
			$keylen);
		let key = &$keyblock[..$keylen];
		debug!(CRYPTO_CALCS $debug, "Creating deprotector:\n>type: {:?}\n>key:\n{}", $prot,
			bytes_as_hex_block(key, ">>"));
		let error = assert_failure!("Can't create record deprotector");
		match $prot {
			ProtectorType::Aes128Gcm => {
				Box::new(Deprotector::new_aes128(key).map_err(|_|error.clone())?)
			},
			ProtectorType::Aes256Gcm => {
				Box::new(Deprotector::new_aes256(key).map_err(|_|error.clone())?)
			},
			ProtectorType::Chacha20Poly1305 => {
				Box::new(Deprotector::new_chacha20(key).map_err(|_|error.clone())?)
			},
		}
	}}
}


struct ExtractorNotAllowed;

impl TlsExtractor for ExtractorNotAllowed
{
	fn extract(&self, _label: &str, _context: Option<&[u8]>, _buffer: &mut [u8]) -> Result<(), TlsFailure>
	{
		sanity_failed!("TLS Extractors require TLS 1.3 or extended master secret");
	}
}

///TLS 1.2 premaster secret.
pub struct Tls12PremasterSecret
{
	//The premaster secret can be compressed to one hash input block.
	secret: HashInputBlock,
	protector: ProtectorType,
	prf: HashFunction,
	is_server: bool,
	is_ems: bool,			//Needed by exporters.
}

impl Tls12PremasterSecret
{
	pub fn new(premaster_secret: DiffieHellmanSharedSecret, protector: ProtectorType, prf: HashFunction,
		is_server: bool, is_ems: bool, debug: Debugger) -> Result<Tls12PremasterSecret, TlsFailure>
	{
		let premaster_secret = premaster_secret.secret.as_ref();
		debug!(CRYPTO_CALCS debug, "Setting TLS 1.2 Pre Master Secret:\n{}", bytes_as_hex_block(
			premaster_secret.as_ref(), ">"));
		//If the PMS is longer than hash, apply a trick to compress it. This works because PMS only
		//appears as HMAC key, which is prehashed if too long.
		let secret = if premaster_secret.len() > MAX_HASH_INPUT {
			let pms2 = prf.oneshot(|x|x.input(premaster_secret)).map_err(|x|assert_failure!("Hash \
				function internal error: {}", x))?;
			HashInputBlock::from2(pms2.as_ref()).map_err(|(x,y)|assert_failure!("Hashing gave ouput too \
				long for {} input ({}, limit is {})", prf.name(), x, y))?
		} else {
			HashInputBlock::from2(premaster_secret).map_err(|(x,y)|assert_failure!("MAX_HASH_INPUT({}) \
				is larger than itself({})???", x, y))?
		};
		debug!(CRYPTO_CALCS debug, "TLS 1.2 Pre Master Secret after compression:\n{}", bytes_as_hex_block(
			secret.as_ref(), ">"));
		Ok(Tls12PremasterSecret{
			secret: secret,
			protector: protector,
			prf: prf,
			is_server: is_server,
			is_ems: is_ems
		})
	}
}

///TLS 1.2 master secret.
pub struct Tls12MasterSecret
{
	secret: [u8; MS_LEN],
	client_random: [u8; RANDOM_LEN],
	server_random: [u8; RANDOM_LEN],
	protector: ProtectorType,
	prf: HashFunction,
	is_server: bool,
	is_ems: bool,			//Needed by exporters.
}

impl Drop for Tls12MasterSecret
{
	fn drop(&mut self)
	{
		wipe_buffer(&mut self.secret);
	}
}

impl Tls12MasterSecret
{
	pub fn new(premaster_secret: Tls12PremasterSecret, client_random: &[u8; 32], server_random: &[u8; 32],
		post_cke_hash: HashOutput, debug: Debugger) -> Result<Tls12MasterSecret, TlsFailure>
	{
		sanity_check!(post_cke_hash.as_ref().len() == premaster_secret.prf.output_length(), "Handshake \
			hash length is not expected ({}, expected {})", post_cke_hash.as_ref().len(),
			premaster_secret.prf.output_length());
		let mut ret = Tls12MasterSecret{
			secret: [0;MS_LEN],
			client_random: *client_random,
			server_random: *server_random,
			protector: premaster_secret.protector,
			prf: premaster_secret.prf,
			is_server: premaster_secret.is_server,
			is_ems: premaster_secret.is_ems
		};
		Self::derive_ms(debug, &mut ret.secret, premaster_secret.secret.as_ref(), client_random,
			server_random, premaster_secret.prf, post_cke_hash, premaster_secret.is_ems)?;
		Ok(ret)
	}
	pub fn get_deprotector(&self, debug: Debugger) -> Result<DeprotectorWrapper, TlsFailure>
	{
		use super::tls12::Deprotector;
		let mut keyblock = [0; MAX_KEY_CHUNK];
		let mut keyblock = SecStackBuffer::new(&mut keyblock, MAX_KEY_CHUNK).unwrap();	//Can't fail.
		let tlen = self.compute_key_chunk(&mut keyblock, !self.is_server)?;
		Ok(DeprotectorWrapper(create_deprotector!(debug, self.protector, keyblock, tlen, 4)))
	}
	pub fn get_protector(&self, debug: Debugger) -> Result<ProtectorWrapper, TlsFailure>
	{
		use super::tls12::Protector;
		let mut keyblock = [0; MAX_KEY_CHUNK];
		let mut keyblock = SecStackBuffer::new(&mut keyblock, MAX_KEY_CHUNK).unwrap();	//Can't fail.
		let tlen = self.compute_key_chunk(&mut keyblock, self.is_server)?;
		Ok(ProtectorWrapper(create_protector!(debug, self.protector, keyblock, tlen, 4)))
	}
	pub fn get_exporter(&self, _debug: Debugger) -> Result<ExtractorWrapper, TlsFailure>
	{
		//Extractors are not allowed in TLS 1.2 non-EMS.
		if !self.is_ems { return Ok(ExtractorWrapper(Box::new(ExtractorNotAllowed))); }
		Ok(ExtractorWrapper(Box::new(super::tls12::Extractor::new(&self.secret, &self.client_random,
			&self.server_random, self.prf)?)))
	}
	pub fn generate_finished(&self, hs_hash: &HashOutput, debug: Debugger) -> Result<HashOutput, TlsFailure>
	{
		let label = if self.is_server { SERVER_FINISHED_LABEL } else { CLIENT_FINISHED_LABEL };
		self.compute_finished(label, hs_hash, debug)
	}
	pub fn check_finished(&self, hs_hash: &HashOutput, debug: Debugger, finished: &[u8]) ->
		Result<(), TlsFailure>
	{
		//These are swapped, since it is checking peer's result.
		let (label, side) = if self.is_server {
			(CLIENT_FINISHED_LABEL, "Client")
		} else {
			(SERVER_FINISHED_LABEL, "Server")
		};
		let mac = self.compute_finished(label, hs_hash, debug.clone())?;
		fail_if!(!slice_eq_ct(&mac.as_ref(), finished), TlsFailure::FinishedMacFailed);
		debug!(HANDSHAKE_EVENTS debug, "{} MAC check OK", side);
		Ok(())
	}
	fn compute_finished(&self, label: &str, hash_sofar: &HashOutput, debug: Debugger) ->
		Result<HashOutput, TlsFailure>
	{
		sanity_check!(hash_sofar.as_ref().len() == self.prf.output_length(), "Finished hash length is not \
			expected ({}, expected {})", hash_sofar.as_ref().len(), self.prf.output_length());
		const FINISHED_LEN: usize = 12;
		let mut out = [0; FINISHED_LEN];
		let mut out = SecStackBuffer::new(&mut out, FINISHED_LEN).unwrap();	//Can't fail.
		self.prf.tls_prf(&self.secret, label, |x|{x.input(hash_sofar.as_ref());}, &mut out).map_err(|x|
			assert_failure!("Hash function internal error: {}", x))?;
		debug!(CRYPTO_CALCS debug, "Calculating TLS 1.2 finished:\n>label: '{}'\n>master secret:\n{}\
			\n>hash:\n{}\n>result:\n{}", label, bytes_as_hex_block(self.secret.as_ref(), ">>"),
			bytes_as_hex_block(hash_sofar.as_ref(), ">>"), bytes_as_hex_block(out.as_ref(), ">>"));
		Ok(HashOutput::from2(out.deref()).map_err(|(x,y)|assert_failure!("TLS 1.2 Finished too long \
			({}, limit is {})", x, y))?)
	}
	//Derive MS from PMS.
	fn derive_ms(debug: Debugger, ms_out: &mut [u8; 48], pms: &[u8], client_random: &[u8; 32],
		server_random: &[u8; 32], prf: HashFunction, hs_hash: HashOutput,
		ems_enabled: bool) -> Result<(), TlsFailure>
	{
		//sanity_check!(self.nontrivial_kex, "Interlock no nontrivial key exchange");
		let label = if ems_enabled {"extended master secret"} else {"master secret"};
		prf.tls_prf(pms, label,  |x| {
			if ems_enabled {
				x.input(hs_hash.as_ref());
			} else {
				x.input(client_random);
				x.input(server_random);
			}
		}, ms_out).map_err(|x|assert_failure!("Hash function internal error: {}", x))?;
		debug!(CRYPTO_CALCS debug, "Calculating TLS 1.2 Master Secret:\n>premaster_secret:\n{}\n\
			>label: '{}'\n>hash:\n{}\n>client_random:\n{}\n>server_random:\n{}\n>result:\n{}",
			bytes_as_hex_block(pms, ">>"), label, bytes_as_hex_block(hs_hash.as_ref(), ">>"),
			bytes_as_hex_block(client_random, ">>"), bytes_as_hex_block(server_random, ">>"),
			bytes_as_hex_block(ms_out, ">>"));
		Ok(())
	}
	fn compute_key_chunk(&self, keyblock: &mut [u8], server_write: bool) -> Result<usize, TlsFailure>
	{
		//sanity_check!(self.nontrivial_kex, "Interlock no nontrivial key exchange");
		let mut masterblock = [0; 2*MAX_KEY_CHUNK];	//Currently largest possible is 88 bytes (2*32+2*12)
		let mut masterblock = SecStackBuffer::new(&mut masterblock, 2*MAX_KEY_CHUNK).unwrap();	//Can't fail.
		let keysize = match self.protector {
			ProtectorType::Aes128Gcm => 16usize,
			ProtectorType::Aes256Gcm => 32usize,
			ProtectorType::Chacha20Poly1305 => 32usize,
		};
		let ivsize = match self.protector {
			ProtectorType::Aes128Gcm => 4usize,
			ProtectorType::Aes256Gcm => 4usize,
			ProtectorType::Chacha20Poly1305 => 12usize,
		};
		let blocklen = keysize + ivsize;	//Should be reasonable.
		let masterlen = blocklen * 2;		//Should be reasonable.
		let func = self.prf;
		//We use the length extension of the TLS PRF.
		func.tls_prf(self.secret.as_ref(), "key expansion", |x| {
			x.input(&self.server_random);
			x.input(&self.client_random);
		}, &mut masterblock[..]).map_err(|x|assert_failure!("Hash function internal error: {}", x))?;
		sanity_check!(masterlen <= masterblock.len(), "Masterblock too small ({}, need {})",
			masterblock.len(), masterlen);
		sanity_check!(blocklen <= keyblock.len(), "Keyblock too small ({}, need {})", keyblock.len(),
			blocklen);
		let t = &masterblock[..masterlen];
		let (client_write_key, t) = t.split_at(keysize);
		let (server_write_key, t) = t.split_at(keysize);
		let (client_write_iv, server_write_iv) = t.split_at(ivsize);
		let write_key = if server_write { server_write_key } else { client_write_key };
		let write_iv = if server_write { server_write_iv } else { client_write_iv };
		(&mut keyblock[..keysize]).copy_from_slice(write_key);
		(&mut keyblock[keysize..blocklen]).copy_from_slice(write_iv);
		Ok(blocklen)
	}
}

fn rachet_common(key: &mut HashOutput, func: HashFunction, debug: Debugger, draft: Tls13Draft, is_tx: bool) ->
	Result<(), TlsFailure>
{
	//Yes, it apparently is "application traffic secret", for both directions (ladders are disjoint,
	//so this is no crypto problem). Also, this one uses hdkf-expand-label with empty context in the
	//spec.
	let hlen = func.output_length();
	sanity_check!(hlen <= MAX_HASH_OUTPUT, "MAX_HASH_OUTPUT too small ({}, need {})", MAX_HASH_OUTPUT,
		hlen);
	let mut tmpkey = [0;MAX_HASH_OUTPUT];
	let mut tmpkey = SecStackBuffer::new(&mut tmpkey, hlen).unwrap();	//Can't fail.
	func.tls13_hkdf_expand_label(key.as_ref(), HkdfLabel::KeyUpdate.get(draft), &[],
		tmpkey.deref_mut()).map_err(|x|assert_failure!("Internal error in PRF hash: {}", x))?;
	debug!(CRYPTO_CALCS debug, "Bumping traffic secret:\n>dir: {}\n>old:\n{}\n>new:\n{}", if is_tx { "tx" }
		else { "rx" }, bytes_as_hex_block(key.as_ref(), ">>"), bytes_as_hex_block(tmpkey.deref(), ">>"));
	//Update the key, as tls13_hkdf_expand_label can't directly return a hash.
	*key = HashOutput::from2(tmpkey.deref()).map_err(|(x,y)|assert_failure!("TLS 1.3 updated key too \
		long for {}, ({}, limit is {})", func.name(), x, y))?;
	Ok(())
}

fn compute_finished(basekey: &HashOutput, hs_hash: &HashOutput, prf: HashFunction, debug: Debugger,
	draft: Tls13Draft, is_tx: bool) -> Result<HashOutput, TlsFailure>
{
	let hlen = prf.output_length();
	sanity_check!(hlen <= MAX_HASH_OUTPUT, "MAX_HASH_OUTPUT too small ({}, need {})", MAX_HASH_OUTPUT,
		hlen);
	let mut finished_key = [0;MAX_HASH_OUTPUT];
	let mut finished_key = SecStackBuffer::new(&mut finished_key, hlen).unwrap();	//Can't fail.
	prf.tls13_hkdf_expand_label(basekey.as_ref(), HkdfLabel::Finished.get(draft),
		&[], &mut finished_key).map_err(|x|assert_failure!("Hash function internal error: {}", x))?;
	let ret = prf.hmac(&mut finished_key, |x| {x.input(hs_hash.as_ref());}).map_err(|x|assert_failure!(
		"Internal error in PRF hash: {}", x))?;
	debug!(CRYPTO_CALCS debug, "Calculating TLS 1.3 finished:\n>direction: {}\n>traffic secret:\n{}\
		\n>hash:\n{}\n>result:\n{}", if is_tx { "outgoing" } else { "incoming" }, bytes_as_hex_block(
		basekey.as_ref(), ">>"), bytes_as_hex_block(hs_hash.as_ref(), ">>"),
		bytes_as_hex_block(ret.as_ref(), ">>"));
	Ok(ret)
}

fn compute_enc_keys(keyblock: &mut [u8], from_secret: &HashOutput, protection: ProtectorType,
	func: HashFunction, draft: Tls13Draft) -> Result<usize, TlsFailure>
{
	let keysize = match protection {
		ProtectorType::Aes128Gcm => 16,
		ProtectorType::Aes256Gcm => 32,
		ProtectorType::Chacha20Poly1305 => 32,
	};
	let ivsize = match protection {
		ProtectorType::Aes128Gcm => 12,
		ProtectorType::Aes256Gcm => 12,
		ProtectorType::Chacha20Poly1305 => 12,
	};

	let blocklen = keysize + ivsize;	//Should be reasonable, and at least keysize.
	sanity_check!(blocklen <= keyblock.len(), "Keyblock too small ({}, need {})", keyblock.len(), blocklen);
	{
		func.tls13_hkdf_expand_label(from_secret.as_ref(), HkdfLabel::Key.get(draft), &[],
			&mut keyblock[..keysize]).map_err(|x|assert_failure!("Hash function internal error: \
			{}", x))?;
	}
	{
		func.tls13_hkdf_expand_label(from_secret.as_ref(), HkdfLabel::Iv.get(draft), &[],
			&mut keyblock[keysize..blocklen]).map_err(|x|assert_failure!("Hash function \
			internal error: {}", x))?;
	}
	Ok(blocklen)
}

//The server_write=TRUE means server->client direction keys.
fn compute_tls13_traffic_secret(master_key: &HashOutput, hs_hash: &HashOutput, prf: HashFunction, hfinal: bool,
	server_write: bool, debug: Debugger, draft: Tls13Draft) -> Result<HashOutput, TlsFailure>
{
	let label = HkdfLabel::Traffic(server_write, hfinal);
	let label = label.get(draft);
	let masterkey = prf.tls13_derive_secret(master_key.as_ref(), label, hs_hash).map_err(|x|
		assert_failure!("Error calculating master secret: {}", x))?;
	debug!(CRYPTO_CALCS debug, "Calculating TLS 1.3 traffic secret:\n>label: '{}'\n>traffic secret:\n{}\
		\n>hash:\n{}\n>result:\n{}", label.0, bytes_as_hex_block(master_key.as_ref(), ">>"),
		bytes_as_hex_block(hs_hash.as_ref(), ">>"), bytes_as_hex_block(masterkey.as_ref(), ">>"));
	Ok(masterkey)
}

pub fn mix_tls13_key(existing_key: &HashOutput, incoming_key: &[u8], prf: HashFunction, kname: &str, is_dhe: bool,
	draft: Tls13Draft, debug: Debugger) -> Result<HashOutput, TlsFailure>
{
	//Only one of the two set_pms calls is affected by this.
	let chain_key: HashOutput = if draft.at_least_19() && is_dhe {
		//Derive-secret step was added before zero block.
		let empty_hash = prf.oneshot(|_|{}).map_err(|x|assert_failure!("Internal error in hash \
			function: {}", x))?;
		let hash = prf.tls13_derive_secret(existing_key.as_ref(), HkdfLabel::DerivedSecret.get(draft),
			&empty_hash).map_err(|x|assert_failure!("Hash function internal error: {}", x))?;
		debug!(CRYPTO_CALCS debug, "Pre-mix expansion:\n>old key:\n{oldkey}\n\
			>result:\n{keyout}", oldkey=bytes_as_hex_block(existing_key.as_ref(), ">>"),
			keyout=bytes_as_hex_block(hash.as_ref(), ">>"));
		hash
	} else {
		//Copy the key, because we can't borrow premaster_secret properly.
		HashOutput::from2(existing_key.as_ref()).map_err(|(x, y)|assert_failure!("Hash function {} has \
			output size too big ({}, limit {})", prf.name(), x, y))?
	};
	let newres = prf.hkdf_extract(incoming_key, chain_key.as_ref()).map_err(|x|
		assert_failure!("Internal error in PRF hash: {}", x))?;
	debug!(CRYPTO_CALCS debug, "Mixing TLS 1.3 key:\n>keyname: {kname}\n>old key:\
		\n{oldkey}\n>key in:\n{keyin}\n>result:\n{keyout}", kname=kname,
			oldkey=bytes_as_hex_block(chain_key.as_ref(), ">>"),
			keyin=bytes_as_hex_block(incoming_key, ">>"),
			keyout=bytes_as_hex_block(newres.as_ref(), ">>"));
	Ok(newres)
}

///DH shared result.
pub struct DiffieHellmanSharedSecret
{
	secret: DhfOutput,
	group: Dhf,
}

impl DiffieHellmanSharedSecret
{
	pub fn new(private_key: &mut Box<DiffieHellmanKey+Send>, peer_public: &[u8]) ->
		Result<DiffieHellmanSharedSecret, TlsFailure>
	{
		let group = private_key.group();
		let secret = private_key.agree(peer_public).map_err(|x|TlsFailure::DiffieHellmanAgreeError(
			group.to_tls_id(), x))?;
		Ok(DiffieHellmanSharedSecret {
			secret: secret,
			group: group
		})
	}
	pub fn get_group(&self) -> Dhf
	{
		self.group
	}
}

///TLS 1.3 handshake secret.
pub struct Tls13HandshakeSecret
{
	secret: HashOutput,
	protector: ProtectorType,
	prf: HashFunction,
	is_server: bool,
	draft: Tls13Draft,
}

impl Tls13HandshakeSecret
{
	pub fn new_dhe(dhe_secret: DiffieHellmanSharedSecret, protector: ProtectorType, prf: HashFunction,
		is_server: bool, draft: Tls13Draft, debug: Debugger) -> Result<Tls13HandshakeSecret, TlsFailure>
	{
		let dhe_secret = dhe_secret.secret.as_ref();
		let hashlen = prf.output_length();
		sanity_check!(dhe_secret.as_ref().len() >= 32, "Diffie-Hellman output too short ({}, minimum 32)",
			dhe_secret.as_ref().len());
		sanity_check!(hashlen <= MAX_HASH_OUTPUT, "MAX_HASH_OUTPUT too small ({}, need {})",
			MAX_HASH_OUTPUT, hashlen);
		let zeroes = [0;MAX_HASH_OUTPUT];
		let zeroes = &zeroes[..hashlen];
		//Can't fail, 0 <= <anything positive>.
		let secret = HashOutput::from2(&[]).unwrap();
		let secret = mix_tls13_key(&secret, zeroes, prf, "dummy PSK", false, draft, debug.clone())?;
		let secret = mix_tls13_key(&secret, dhe_secret.as_ref(), prf, "DH result", true, draft, debug)?;
		Ok(Tls13HandshakeSecret {
			secret: secret,
			protector: protector,
			prf: prf,
			is_server: is_server,
			draft: draft,
		})
	}
	//The hs_hash is after ServerHello.
	pub fn traffic_secret_in(&self, hs_hash: &HashOutput, debug: Debugger) ->
		Result<Tls13TrafficSecretIn, TlsFailure>
	{
		Tls13TrafficSecretIn::new(&self.secret, hs_hash, self.protector, self.prf, self.is_server,
			self.draft, false, debug)
	}
	//The hs_hash is after ServerHello.
	pub fn traffic_secret_out(&self, hs_hash: &HashOutput, debug: Debugger) ->
		Result<Tls13TrafficSecretOut, TlsFailure>
	{
		Tls13TrafficSecretOut::new(&self.secret, hs_hash, self.protector, self.prf, self.is_server,
			self.draft, false, debug)
	}
}

///TLS 1.3 master secret.
pub struct Tls13MasterSecret
{
	secret: HashOutput,
	protector: ProtectorType,
	prf: HashFunction,
	is_server: bool,
	draft: Tls13Draft,
}

impl Tls13MasterSecret
{
	pub fn new(handshake_secret: Tls13HandshakeSecret, debug: Debugger) -> Result<Tls13MasterSecret, TlsFailure>
	{
		let hashlen = handshake_secret.prf.output_length();
		sanity_check!(hashlen <= MAX_HASH_OUTPUT, "MAX_HASH_OUTPUT too small ({}, need {})",
			MAX_HASH_OUTPUT, hashlen);
		let zeroes = [0;MAX_HASH_OUTPUT];
		let zeroes = &zeroes[..hashlen];
		let master_secret = mix_tls13_key(&handshake_secret.secret, zeroes, handshake_secret.prf,
			"mixes done", true, handshake_secret.draft, debug)?;
		Ok(Tls13MasterSecret{
			secret: master_secret,
			protector: handshake_secret.protector,
			prf: handshake_secret.prf,
			is_server: handshake_secret.is_server,
			draft: handshake_secret.draft,
		})
	}
	//The hs_hash is after Server Finished.
	pub fn traffic_secret_in(&self, hs_hash: &HashOutput, debug: Debugger) ->
		Result<Tls13TrafficSecretIn, TlsFailure>
	{
		Tls13TrafficSecretIn::new(&self.secret, hs_hash, self.protector, self.prf, self.is_server,
			self.draft, true, debug)
	}
	//The hs_hash is after Server Finished.
	pub fn traffic_secret_out(&self, hs_hash: &HashOutput, debug: Debugger) ->
		Result<Tls13TrafficSecretOut, TlsFailure>
	{
		Tls13TrafficSecretOut::new(&self.secret, hs_hash, self.protector, self.prf, self.is_server,
			self.draft, true, debug)
	}
	//Exporter. hs_hash is after Server Finished.
	pub fn extractor(&self, hs_hash: &HashOutput, debug: Debugger) -> Result<ExtractorWrapper, TlsFailure>
	{
		make_tls13_extractor(&self.secret, hs_hash, self.prf, self.draft, debug).map(|x|ExtractorWrapper(
			x))
	}
}


///TLS 1.3 traffic secret (input)
pub struct Tls13TrafficSecretIn
{
	secret: HashOutput,
	protector: ProtectorType,
	prf: HashFunction,
	is_server: bool,
	draft: Tls13Draft,
	application: bool,
}

impl Tls13TrafficSecretIn
{
	pub fn new(master_key: &HashOutput, hs_hash: &HashOutput, protector: ProtectorType, prf: HashFunction,
		is_server: bool, draft: Tls13Draft, application: bool, debug: Debugger) ->
		Result<Tls13TrafficSecretIn, TlsFailure>
	{
		sanity_check!(master_key.as_ref().len() == prf.output_length(), "Wrong master key length ({}, \
			expected {})", master_key.as_ref().len(), prf.output_length());
		sanity_check!(hs_hash.as_ref().len() == prf.output_length(), "Wrong handshake hash length ({}, \
			expected {})", hs_hash.as_ref().len(), prf.output_length());
		Ok(Tls13TrafficSecretIn {
			//This is read side, so server_write should be asserted on client.
			secret: compute_tls13_traffic_secret(master_key, hs_hash, prf, application, !is_server,
				debug, draft)?,
			protector: protector,
			prf: prf,
			is_server: is_server,
			draft: draft,
			application: application
		})
	}
	pub fn to_deprotector(&self, debug: Debugger) -> Result<DeprotectorWrapper, TlsFailure>
	{
		use super::tls13::Deprotector;
		let mut keyblock = [0; MAX_KEY_CHUNK];
		let mut keyblock = SecStackBuffer::new(&mut keyblock, MAX_KEY_CHUNK).unwrap();	//Can't fail.
		let tlen = compute_enc_keys(&mut keyblock, &self.secret, self.protector, self.prf, self.draft)?;
		Ok(DeprotectorWrapper(create_deprotector!(debug, self.protector, keyblock, tlen, 12)))
	}
	pub fn check_finished(&self, hs_hash: &HashOutput, finished: &[u8], debug: Debugger) ->
		Result<(), TlsFailure>
	{
		let mac = compute_finished(&self.secret, hs_hash, self.prf, debug.clone(), self.draft, false)?;
		fail_if!(!slice_eq_ct(&mac.as_ref(), finished), TlsFailure::FinishedMacFailed);
		let side = if self.is_server { "Client" } else { "Server" };
		debug!(HANDSHAKE_EVENTS debug, "{} MAC check OK", side);
		Ok(())
	}
	pub fn rachet(&mut self, debug: Debugger) -> Result<(), TlsFailure>
	{
		sanity_check!(self.application, "Only application data keys can be stepped forward");
		rachet_common(&mut self.secret, self.prf, debug, self.draft, false)
	}
}

///TLS 1.3 traffic secret (output)
pub struct Tls13TrafficSecretOut
{
	secret: HashOutput,
	protector: ProtectorType,
	prf: HashFunction,
	draft: Tls13Draft,
	application: bool,
}

impl Tls13TrafficSecretOut
{
	pub fn new(master_key: &HashOutput, hs_hash: &HashOutput, protector: ProtectorType, prf: HashFunction,
		is_server: bool, draft: Tls13Draft, application: bool, debug: Debugger) ->
		Result<Tls13TrafficSecretOut, TlsFailure>
	{
		sanity_check!(master_key.as_ref().len() == prf.output_length(), "Wrong master key length ({}, \
			expected {})", master_key.as_ref().len(), prf.output_length());
		sanity_check!(hs_hash.as_ref().len() == prf.output_length(), "Wrong handshake hash length ({}, \
			expected {})", hs_hash.as_ref().len(), prf.output_length());
		Ok(Tls13TrafficSecretOut {
			//This is write side, so server_write should be asserted on server.
			secret: compute_tls13_traffic_secret(master_key, hs_hash, prf, application, is_server,
				debug, draft)?,
			protector: protector,
			prf: prf,
			draft: draft,
			application: application
		})
	}
	pub fn to_protector(&self, debug: Debugger) -> Result<ProtectorWrapper, TlsFailure>
	{
		use super::tls13::Protector;
		let mut keyblock = [0; MAX_KEY_CHUNK];
		let mut keyblock = SecStackBuffer::new(&mut keyblock, MAX_KEY_CHUNK).unwrap();	//Can't fail.
		let tlen = compute_enc_keys(&mut keyblock, &self.secret, self.protector, self.prf, self.draft)?;
		Ok(ProtectorWrapper(create_protector!(debug, self.protector, keyblock, tlen, 12)))
	}
	pub fn generate_finished(&self, hs_hash: &HashOutput, debug: Debugger) -> Result<HashOutput, TlsFailure>
	{
		compute_finished(&self.secret, hs_hash, self.prf, debug, self.draft, true)
	}
	pub fn rachet(&mut self, debug: Debugger) -> Result<(), TlsFailure>
	{
		sanity_check!(self.application, "Only application data keys can be stepped forward");
		rachet_common(&mut self.secret, self.prf, debug, self.draft, true)
	}
}

//This uses the post-server-finished hash.
fn make_tls13_extractor(master_key: &HashOutput, ems_hash: &HashOutput, prf: HashFunction, draft: Tls13Draft,
	debug: Debugger) -> Result<Box<TlsExtractor+Send>, TlsFailure>
{
	let hashlen = prf.output_length();
	sanity_check!(hashlen <= MAX_HASH_OUTPUT, "MAX_HASH_OUTPUT too small ({}, need {})",
		MAX_HASH_OUTPUT, hashlen);
	let masterkey = prf.tls13_derive_secret(master_key.as_ref(), HkdfLabel::ExporterM.get(draft), ems_hash).
		map_err(|x|assert_failure!("Internal error in PRF hash: {}", x))?;
	debug!(CRYPTO_CALCS debug, "Calculating TLS 1.3 extractor key:\n>master secret:\n{}\n>hash:\n{}\
		\n>result:\n{}", bytes_as_hex_block(master_key.as_ref(), ">>"), bytes_as_hex_block(ems_hash.as_ref(),
		">>"), bytes_as_hex_block(masterkey.as_ref(), ">>"));
	Ok(Box::new(super::tls13::Extractor::new(master_key, prf, draft)?))
}
