use ::{AbortReason, RxAppdataHandler, TxIrqHandler};
use ::common::{HSTYPE_KEY_UPDATE, PROTO_ALERT, PROTO_APPDATA, PROTO_HANDSHAKE, TlsFailure};
use ::logging::{bytes_as_hex_block, explain_hs_type, Logging};
use ::stdlib::{Arc, Box, Cow, IoError, IoErrorKind, IoRead, IoResult, IoWrite, max, min, Range, ToOwned, Vec};
use ::utils::{Queue, QueueWriteError};

//This limit is due to AES. It would be trillions of times bigger for Chacha20.
const MAX_PROTECTOR_USES: u64 = 2000000;
const MAX_PROTECTOR_USES_FIRST: u64 = 1000;

mod keyschedule;
pub use self::keyschedule::{DiffieHellmanSharedSecret, Tls12MasterSecret, Tls12PremasterSecret, Tls13HandshakeSecret,
	Tls13MasterSecret, Tls13TrafficSecretIn, Tls13TrafficSecretOut};
use self::keyschedule::{DeprotectorWrapper, ExtractorWrapper, ProtectorWrapper};
mod tls12;
mod tls13;

//Record protector.
pub trait RecordProtector
{
	//Protect a record" inplace. Returns range into buf.
	fn protect(&mut self, buf: &mut [u8], rtype: u8, range: Range<usize>, pad: usize) ->
		Result<Range<usize>, ()>;
	//Protect record with constrained output. Returns output and input sizes.
	fn protect_coutput(&mut self, out: &mut [u8], inp: &[u8], rtype: u8, max_write_size: usize) ->
		Result<(usize, usize), ()>;
	//Get overhead.
	fn get_overhead(&self) -> usize;
}

//Record deprotector.
pub trait RecordDeprotector
{
	//Deprotect a record inplace.Returns record type and range into buf.
	fn deprotect(&mut self, buf: &mut [u8]) -> Result<(u8, Range<usize>), ()>;
}

//Extractor.
pub trait TlsExtractor
{
	fn extract(&self, label: &str, context: Option<&[u8]>, buffer: &mut [u8]) -> Result<(), TlsFailure>;
}

//Session control.
pub trait SessionController
{
	//Set protector for next message.
	fn set_protector(&mut self, obj: ProtectorWrapper);
	//Set deprotector for next message.
	fn set_deprotector(&mut self, obj: DeprotectorWrapper);
	//Set TLS extractor.
	fn set_extractor(&mut self, obj: ExtractorWrapper);
	//Abort handshake.
	fn abort_handshake(&mut self, fault: TlsFailure);
	//Queue handshake message for sending. These will be sent _before_ send protector changes.
	fn queue_handshake(&mut self, htype: u8, data: &[u8], state: &'static str);
	//Change to TLS 1.2 record version.
	fn set_use_tls12_records(&mut self);
	//Last message sent was KeyUpdate.
	fn last_tx_key_update(&self) -> bool;
	//Ok to send appdata.
	fn assert_hs_rx_finished(&mut self);
	//Set the maximum fragment length.
	fn set_max_fragment_length(&mut self, maxlen: usize);
}

//Control message handler.
pub trait ControlHandler
{
	//Handle received control message (anything except Alert or ApplicationData).
	//
	//Note: If you need to abort connection, use controller.abort_handshake().
	fn handle_control(&mut self, rtype: u8, msg: &[u8], controller: &mut SessionController);
	//Handle request for tx update
	//
	//Note: If you need to abort connection, use controller.abort_handshake().
	fn handle_tx_update(&mut self, controller: &mut SessionController);
}

struct NullProtector(bool);
struct NullExtractor;
struct NullTxIrqReceiver;
struct SliceWrite<'a>(&'a mut [u8], usize);

impl RecordProtector for NullProtector
{
	fn protect(&mut self, buf: &mut [u8], rtype: u8, range: Range<usize>, _pad: usize) ->
		Result<Range<usize>, ()>
	{
		//We need:
		//- At least 5 bytes of space before range start.
		//- Range must be valid (end >= start).
		//- Range must be within buffer.
		//- Range must be at most 16384 bytes in size.
		fail_if!(range.start < 5, ());
		fail_if!(range.end < range.start, ());
		fail_if!(range.end > buf.len(), ());
		let len = range.end - range.start;
		fail_if!(len > 16384, ());
		//By the checks above, this can not overflow, and at least 5 bytes from this can be accessed.
		let hstart = range.start - 5;
		buf[hstart+0] = rtype;
		//Hack, if TLS 1.2, use TLS 1.2 record protocol.
		buf[hstart+1] = 3;
		buf[hstart+2] = if self.0 { 1 } else { 3 };
		buf[hstart+3] = (len >> 8) as u8;
		buf[hstart+4] = len as u8;
		Ok(hstart..range.end)
	}
	fn protect_coutput(&mut self, out: &mut [u8], inp: &[u8], rtype: u8, max_write_size: usize) ->
		Result<(usize, usize), ()>
	{
		//We need at least 5 bytes of output space, otherwise we can't flush anything.
		if out.len() < 5 { return Ok((0, 0)); }
		//The maximum input length is minimum of output length minus five and the maximum packet size.
		//Also, by checks above, at least 5 bytes from out are writeable.
		let len = min(min(MAX_TLS_RECORD_PLAIN, max_write_size), min(out.len() - 5, inp.len()));
		out[0] = rtype;
		//Hack, if TLS 1.2, use TLS 1.2 record protocol.
		out[1] = 3;
		out[2] = if self.0 { 1 } else { 3 };
		out[3] = (len >> 8) as u8;		//Length.
		out[4] = len as u8;
		(&mut out[5..][..len]).copy_from_slice(&inp[..len]);
		Ok((len + 5, len))			//5 bytes of headers.
	}
	fn get_overhead(&self) -> usize
	{
		5
	}
}

impl RecordDeprotector for NullProtector
{
	fn deprotect(&mut self, buf: &mut [u8]) -> Result<(u8, Range<usize>), ()>
	{
		//We need at least 5 bytes of input.
		fail_if!(buf.len() < 5, ());
		let len = (buf[3] as usize) << 8 | (buf[4] as usize);
		let rtype = buf[0];	//Buf.len has to be at least 5, so conains at least 1 byte.
		let rsize = len + 5;	//Len can be at most 65535, not even close to overflow.
		//Check that the size is correct.
		fail_if!(buf.len() != rsize, ());
		Ok((rtype, 5..rsize))
	}
}

impl TlsExtractor for NullExtractor
{
	fn extract(&self, _label: &str, _context: Option<&[u8]>, _buffer: &mut [u8]) -> Result<(), TlsFailure>
	{
		sanity_failed!("Tried to use NULL extractor");
	}
}

impl TxIrqHandler for NullTxIrqReceiver
{
	fn wants_transmit(&mut self)
	{
		//Do nothing.
	}
}

impl<'a> IoWrite for SliceWrite<'a>
{
	fn write(&mut self, data: &[u8]) -> IoResult<usize>
	{
		//The amount to copy is lesser of space available and data available. And structure invariant is
		//that pointer is at most buffer length.
		let tocopy = min(data.len(), self.0.len() - self.1);
		(&mut self.0[self.1..][..tocopy]).copy_from_slice(&data[..tocopy]);
		//This can't overflow, because self.1 + tocopy must fit as size, and thus are bounded by usize.
		self.1 = self.1 + tocopy;
		Ok(tocopy)
	}
	fn flush(&mut self) -> IoResult<()>
	{
		Ok(())
	}
}


struct SessionControllerReceiver
{
	abort: Option<TlsFailure>,
	next_extractor: Option<Box<TlsExtractor+Send>>,
	next_protector: Option<Box<RecordProtector+Send>>,
	next_deprotector: Option<Box<RecordDeprotector+Send>>,
	queued_messages: Vec<(u8, Vec<u8>, &'static str)>,
	use_tls12_records: bool,
	tx_ku_flag: bool,
	hs_rx_finished: bool,
	max_fragment_length: Option<usize>,
}

impl SessionController for SessionControllerReceiver
{
	fn set_protector(&mut self, obj: ProtectorWrapper)
	{
		self.next_protector = Some(obj.into_inner());
	}
	fn set_deprotector(&mut self, obj: DeprotectorWrapper)
	{
		self.next_deprotector = Some(obj.into_inner());
	}
	fn set_extractor(&mut self, obj: ExtractorWrapper)
	{
		self.next_extractor = Some(obj.into_inner());
	}
	fn abort_handshake(&mut self, fault: TlsFailure)
	{
		self.abort = Some(fault);
	}
	fn queue_handshake(&mut self, htype: u8, data: &[u8], state: &'static str)
	{
		self.queued_messages.push((htype, data.to_owned(), state));
	}
	fn set_use_tls12_records(&mut self)
	{
		self.use_tls12_records = true;
	}
	fn last_tx_key_update(&self) -> bool
	{
		self.tx_ku_flag
	}
	fn assert_hs_rx_finished(&mut self)
	{
		self.hs_rx_finished = true;
	}
	fn set_max_fragment_length(&mut self, maxlen: usize)
	{
		self.max_fragment_length = Some(maxlen);
	}
}


//Base session type.
pub struct SessionBase
{
	//Received plaintext data queue. This queue contains the received plaintext data, and gets EOF'd when there
	//will be no more application data.
	read_q: Queue,
	//Sent plaintext data queue. This queue contains the plaintext data to be sent. EOF'ing it causes
	//close_notify alert to be sent once this buffer is empty.
	write_q: Queue,
	//Receive ciphertext queue. This queue contains undecrypted data from socket.
	recv_q: Queue,
	//Send ciphertext queue. This queue contains encrypted data ready to be sent.
	send_q: Queue,
	//Receive high water mark. If the amount of data in read buffer is higher than this, don't do read in
	//showtime.
	high_water_mark: usize,
	//Received TLS abort flag. This is set after remote end aborts the TLS connection. Note that close_notify
	//is not considered an abort.
	remote_tls_abort: bool,
	//TLS abort from local side flag. This is set after local end aborts the TLS connection. Note that
	//close_notify is not considered an abort.
	local_tls_abort: bool,
	//TLS abort to be sent. This gets cleared after the alert has been queued.
	abort_code: Option<u8>,
	//Error message. normally None. Note that error messages are gated behind abort_completed.
	error: Option<AbortReason>,
	//Abort completed flag. This gets set when messages relating to abort have been sent, and causes the
	//failure reason to be disclosed via aborted_by().
	abort_completed: bool,
	//Outgoing EOF flag. This get set when messages relating to EOF have been sent.
	outgoing_eof: bool,
	//Incoming EOF flag. This flag gets set when receiving close_notify.
	incoming_eof: bool,
	//Queued EOF flag. This gets set when do_local_abort() is called with CLOSE_NOTIFY.
	queued_eof: bool,
	//Countdown to outgoing EOF. This is normally None. However, when alert is queued, it is updated to point to
	//the end of the alert. When this hits zero, either abort_completed or outgoing_eof gets set (depending on
	//if local_tls_abort is set).
	bytes_to_outgoing_eof: Option<usize>,
	//All of handshake has been transmitted.
	tx_hs_finished: bool,
	//All of handshake has been received.
	rx_hs_finished: bool,
	//Requesting handshake message receipt.
	rx_hs_request: bool,
	//Requesting message transmit slot.
	tx_request: bool,
	//TX IRQ has been raised already.
	tx_irq_active: bool,
	//Scratch buffer for operations.
	scratch: Vec<u8>,
	//Next received record maybe unencrypted flag. Allows next received record to be unencrypted alert. Auto-
	//resets after next received record.
	maybe_clear_alert: bool,
	//Times protector has been used.
	protector_used: u64,
	//Protector change queued.
	do_change_protector: bool,
	//Protector has been changed at least once.
	protector_changed_once: bool,
	//Number of bytes to flush left.
	flush_remaining: usize,
	//Send threshold (note: Values bigger than 16384 are treated as 16384).
	send_threshold: usize,
	//Nodelay flag.
	nodelay_flag: bool,
	//Internal only flag (disallow all ops).
	internal_only: bool,
	//Last TX was KeyUpdate.
	last_tx_key_update: bool,
	//Appdata RX handler.
	appdata_rx_fn: Option<Box<RxAppdataHandler+Send>>,
	//Protector.
	protector: Box<RecordProtector+Send>,
	//Deprotector.
	deprotector: Box<RecordDeprotector+Send>,
	//Extractor.
	extractor: Box<TlsExtractor+Send>,
	//TX irq.
	tx_irq: Box<TxIrqHandler+Send>,
	//Debugging.
	debug: Option<(u64, Arc<Box<Logging+Send+Sync>>)>,
	//Maximum write size in one fragment. Assumed to be at least 64.
	max_write_size: usize,
	//Is TLS 1.3?
	is_tls13: bool,
}

const MAX_RAW_RECV_BUF: usize = 24 * 1024;	//This needs to be larger than max TLS record size.
const MAX_TLS_PACKET: usize = 18 * 1024;
const MAX_TLS_RECORD_PLAIN: usize = 16384;
const MIN_TLS_OFFSET: usize = 13;		//5 byte header, 8 bytes of boobytrap.

fn is_close_alert(x: u8) -> bool { x == 0 }
fn is_error_alert(x: u8) -> bool { x != 0 }

macro_rules! impl_tls13_cleartext_alert
{
	($selfx:expr, $buffer:expr, $bufferlen:expr) => {{
		//Special unencrypted alert. This happens on TLS 1.3 in serverside if client chokes on ServerHello.
		//This record can only appear immediately after ServerHello has been sent.
		if $selfx.maybe_clear_alert && $bufferlen == 7 && $buffer[0] == PROTO_ALERT {
			let code = $buffer[6];
			$selfx.do_remote_abort(code);	//Always do abort.
			$selfx.read_q.send_eof();
			return Ok(false);
		}
		$selfx.maybe_clear_alert = false;	//Auto-reset one-shot.
	}}
}

macro_rules! impl_rx_packet_tail
{
	($selfx:expr, $buffer:expr, $controller:expr) => {{
		let (rtype, range) = match $selfx.deprotector.deprotect($buffer) {
			Ok((x,y)) => (x, y),
			Err(_) => fail!(TlsFailure::DeprotectionFailed)
		};

		if rtype == PROTO_ALERT {
			//Alerts...
			let (badlen, level, code) = {
				let msg = &$buffer[range];	//Deprotect range is always valid.
				debug!(HANDSHAKE_DATA $selfx.debug, "Received alert message:\n{}",
					bytes_as_hex_block(msg, ">"));
				//If length is invalid, interpret as fatal code 50 alert.
				let (level, code) = if msg.len() == 2 { (msg[0], msg[1]) } else { (2, 50) };
				(msg.len() != 2, level, code)
			};
			//Level is meaningless in TLS 1.3, and unusable in TLS 1.2. Do meaningless check in order
			//to avoid warning.
			if badlen || is_error_alert(code) || level < 1 || level > 2 {
				$selfx.do_remote_abort(code);
			} else {
				debug!(ABORTS $selfx.debug, "EOF alert from remote side");
				$selfx.incoming_eof = true;
			}
			//Shutdown reads anyway: If alert isn't fatal, it is EOF.
			$selfx.read_q.send_eof();
			return Ok(false);
		} else if rtype == PROTO_APPDATA {
			//Application data. This is only allowed if all handshake messages have been received
			//and EOF has not already been received.
			fail_if!(!$selfx.rx_hs_finished, TlsFailure::AppdataBeforeFinished);
			fail_if!($selfx.read_q.eof_sent(), TlsFailure::AppdataAfterEof);
			if let &mut Some(ref mut handler) = &mut $selfx.appdata_rx_fn {
				//Appdata handler is set, call it! The deprotect range is always valid.
				handler.received_data(&$buffer[range]);
				return Ok(true);
			}
			//We can't be here with read_q closed. The deprotect range is always valid.
			match $selfx.read_q.write(&$buffer[range]) {
				Ok(_) => (),
				Err(x) => sanity_failed!("Error writing to application data read queue: {}", x),
			};
		} else {
			//Other: Forward as control message.
			let mut receiver = $selfx.make_session_receiver();
			$selfx.rx_hs_request = false;
			{
				//The deprotect range is always valid.
				$controller.handle_control(rtype, &$buffer[range], &mut receiver);
			}
			if !$selfx.process_session_update(receiver) {
				return Ok(false);
			}
		}
		return Ok(true);
	}}
}

impl SessionBase
{
	//Create a new session base.
	pub fn new(debug: Option<(u64, Arc<Box<Logging+Send+Sync>>)>) -> SessionBase
	{
		SessionBase {
			read_q: Queue::new(),
			write_q: Queue::new(),
			recv_q: Queue::new(),
			send_q: Queue::new(),
			high_water_mark: 4096,
			remote_tls_abort: false,
			local_tls_abort: false,
			abort_code: None,
			error: None,
			abort_completed: false,
			outgoing_eof: false,
			incoming_eof: false,
			queued_eof: false,
			bytes_to_outgoing_eof: None,
			tx_hs_finished: false,
			rx_hs_finished: false,
			rx_hs_request: false,
			tx_request: false,
			tx_irq_active: false,
			scratch: vec![0;MAX_RAW_RECV_BUF],
			maybe_clear_alert: false,
			protector_used: 0,
			do_change_protector: false,
			protector_changed_once: false,
			flush_remaining: 0,
			send_threshold: 16384,
			nodelay_flag: false,
			internal_only: false,
			last_tx_key_update: false,
			appdata_rx_fn: None,
			protector: Box::new(NullProtector(true)),
			deprotector: Box::new(NullProtector(true)),
			extractor: Box::new(NullExtractor),
			tx_irq: Box::new(NullTxIrqReceiver),
			debug: debug,
			max_write_size: MAX_TLS_RECORD_PLAIN,
			is_tls13: true,
		}
	}
	//Send a local abort. If there is already local abort in process, this causes double abort, which finishes
	//instantly, but retains the old abort number (unless it was closure alert).
	pub fn do_local_abort(&mut self, fault: TlsFailure)
	{
		let code = fault.get_alert_num();
		debug!(ABORTS self.debug, "do_local_abort: #{}:{}", code, fault);
		let mut do_double_abort = false;
		if let Some(ref mut x) = self.abort_code {
			debug!(ABORTS self.debug, "do_local_abort: Double abort");
			do_double_abort = true;
			if is_close_alert(*x) { *x = code; }
		} else {
			self.abort_code = Some(code);
		}
		if is_error_alert(code) || do_double_abort {
			//These are not EOFs, so do local abort. Note that this doesn't complete instantly.
			self.local_tls_abort = true;
			self.error = Some(AbortReason::SentAlert(fault));
			self.recv_q.abort();
		} else {
			self.queued_eof = true;
		}
		//Additionally, double aborts complete instantly.
		if do_double_abort {
			debug!(ABORTS self.debug, "Abort completed (double abort)");
			self.abort_completed = true;
		} else {
			//New data, so assert TX IRQ.
			self.maybe_send_tx_irq(false);
		}
	}
	//Abort handshake and set error.
	pub fn abort_handshake(&mut self, fault: TlsFailure)
	{
		//The error must be error alert.
		self.do_local_abort(fault.clone());
		self.error = Some(AbortReason::HandshakeError(fault));
		//Even if the alert gets interpretted as close, turn it into error.
		self.local_tls_abort = true;
		self.queued_eof = false;
	}
	//Set maximum fragment length.
	pub fn set_max_fragment_length(&mut self, maxlen: usize)
	{
		//Floor at 64 bytes, cap at 16384.
		self.max_write_size = min(max(maxlen, 64), 16384);
	}
	//Set RX handshake finished flag.
	pub fn finish_rx_handshake(&mut self)
	{
		self.rx_hs_finished = true;
	}
	//Set TX handshake finished flag.
	pub fn finish_tx_handshake(&mut self)
	{
		self.tx_hs_finished = true;
	}
	//Set Request RX handshake flag.
	pub fn request_rx_handshake(&mut self)
	{
		self.rx_hs_request = true;
	}
	//Change deprotector
	pub fn change_deprotector(&mut self, deprotector: DeprotectorWrapper)
	{
		//Message are synchronous (except for appdata), so we can change the deprotector here.
		self.deprotector = deprotector.into_inner();
	}
	//Change protector
	pub fn change_protector(&mut self, protector: ProtectorWrapper)
	{
		//Message are synchronous (except for appdata), so we can change the protector here.
		self.protector_used = 0;
		self.do_change_protector = false;
		self.protector = protector.into_inner();
	}
	//Change extractor
	pub fn change_extractor(&mut self, extractor: ExtractorWrapper)
	{
		//Message are synchronous (except for appdata), so we can change the extractor here.
		self.extractor = extractor.into_inner();
	}
	//Send handshake message.
	pub fn send_handshake(&mut self, htype: u8, buf: &[u8], state: &str)
	{
		if self.debug.clone().map(|x|x.0).unwrap_or(0) & debug_class!(HANDSHAKE_DATA) == 0 {
			//DEBUG_HANDSHAKE_DATA causes more detailed message, so don't print this here.
			debug!(HANDSHAKE_MSGS self.debug, "[State {}] send_handshake: type {}, length {}", state,
				explain_hs_type(htype), buf.len());
		}
		match self._send_handshake(htype, buf, state) {
			Ok(_) => (),
			Err(x) => self.do_local_abort(x)
		}
	}
	//Set tx_request flag to active. Also, if TX IRQ is not already active, activate if if needed.
	pub fn do_tx_request(&mut self)
	{
		self.tx_request = true;
		//If Interrupt is not already active, activate it if needed.
		self.maybe_send_tx_irq(false);
	}
	//If tx_request is set or wants write, raise the TX IRQ.
	fn maybe_send_tx_irq(&mut self, force: bool)
	{
		if self.tx_request || self.wants_write() {
			if !self.tx_irq_active || force {
				self.tx_irq_active = true;
				self.tx_irq.wants_transmit();
			}
		}
	}
	//Send handshake message.
	fn _send_handshake(&mut self, htype: u8, buf: &[u8], state: &str) -> Result<(), TlsFailure>
	{
		//The plaintext must fit into scratch buffer.
		sanity_check!(self.scratch.len() >= MAX_TLS_RECORD_PLAIN + MIN_TLS_OFFSET,
			 "Max record size overflows scratch buffer ({}, need {})", self.scratch.len(),
			MAX_TLS_RECORD_PLAIN + MIN_TLS_OFFSET);
		sanity_check!(self.max_write_size >= 64, "Max write size must be at least 64");
		sanity_check!(self.max_write_size <= MAX_TLS_RECORD_PLAIN, "Max write size ({}) must be at most {}",
			self.max_write_size, MAX_TLS_RECORD_PLAIN);
		//Handshake messages can be at most 16MB.
		sanity_check!(buf.len() <= 0xFFFFFF, "Handshake message over 16MB ({})", buf.len());
		debug!(HANDSHAKE_DATA self.debug, "[State {}] Sending {}, length {}:\n{}", state, explain_hs_type(
			htype), buf.len(), bytes_as_hex_block(buf, ">"));
		//We assume that MIN_TLS_OFFSET is well below usize limit, and MAX_TLS_RECORD_PLAIN is at least
		//4.
		let min_tls_off_hdr = MIN_TLS_OFFSET + 4;
		let max_seg_hdr = self.max_write_size - 4;
		let mut offset = 0;
		//The buf.len() == 0 is here in order to send empty messages.
		while offset < buf.len() || buf.len() == 0 {
			//64 <= self.max_write_size <= MAX_TLS_RECORD_PLAIN.
			//The calculations won't come even near overflow. And even if they would, we would have
			//already aborted above. Also, we have established that at least MAX_TLS_RECORD_PLAIN
			//octets fit into scratch buffer after the MIN_TLS_OFFSET point.
			let (coffset, maxsize, extra) = if offset == 0 {
				self.scratch[MIN_TLS_OFFSET+0] = htype;
				self.scratch[MIN_TLS_OFFSET+1] = (buf.len() >> 16) as u8;
				self.scratch[MIN_TLS_OFFSET+2] = (buf.len() >> 8) as u8;
				self.scratch[MIN_TLS_OFFSET+3] = buf.len() as u8;
				(min_tls_off_hdr,max_seg_hdr,4)
			} else {
				(MIN_TLS_OFFSET,self.max_write_size,0)
			};
			//Calculate fragment size. It is smaller of maximum size and remaining size.
			let fsize = min(maxsize, buf.len() - offset);
			(&mut self.scratch[coffset..][..fsize]).copy_from_slice(&buf[offset..][..fsize]);
			//The result for addition can be at most MAX_TLS_RECORD_PLAIN, since fsize can be at
			//most MAX_TLS_RECORD_PLAIN if extra is 0, or MAX_TLS_RECORD_PLAIN-4 if extra is 4.
			self.last_tx_key_update = htype == HSTYPE_KEY_UPDATE;
			self.send_encrypted(PROTO_HANDSHAKE, fsize + extra, true);
			offset = offset + fsize;		//Must be valid size, so in usize range.
			//Special: If we got here with empty handshake message, only do one round.
			if buf.len() == 0 { break; }
		}
		self.maybe_send_tx_irq(false);	//There probably is stuff to send now.
		Ok(())
	}
	//Send arbitrary message fragment (must be <16kB and rtype must not be 21 (use do_local_abort), 22 (use
	//send_handshake) nor 23 (use normal write))
	pub fn send_message_fragment(&mut self, rtype: u8, buf: &[u8])
	{
		//The buffer must fit into one TLS record.
		if buf.len() > self.max_write_size || buf.len() > MAX_TLS_RECORD_PLAIN ||
			MIN_TLS_OFFSET + buf.len() > self.scratch.len() {
			self.do_local_abort(TlsFailure::from(assert_failure!("Tried to send fragment too big ({})",
				buf.len())));
			return;
		}
		if rtype == PROTO_ALERT || rtype == PROTO_HANDSHAKE || rtype == PROTO_APPDATA {
			self.do_local_abort(TlsFailure::from(assert_failure!("Tried to send bad fragment rtype {}",
				rtype)));
			return;
		}
		(&mut self.scratch[MIN_TLS_OFFSET..][..buf.len()]).copy_from_slice(buf);
		self.send_encrypted(rtype, buf.len(), true)
	}
	//Set one-shot "next record may be unencrypted alert" flag.
	pub fn next_record_may_be_unencrypted_alert(&mut self)
	{
		self.maybe_clear_alert = true;
	}
	//Use TLS 1.2 record version.
	pub fn set_use_tls12_records(&mut self)
	{
		self.is_tls13 = false;	//Hack to detect TLS 1.3.
		self.protector = Box::new(NullProtector(false));
		self.deprotector = Box::new(NullProtector(false));
	}
	//Set read/write internal only.
	pub fn set_internal_only(&mut self)
	{
		self.internal_only = true;
	}
	fn do_remote_abort(&mut self, code: u8)
	{
		debug!(ABORTS self.debug, "do_remove_abort: code {}", code);
		self.error = Some(AbortReason::RecvAlert(code));
		self.remote_tls_abort = true;
		debug!(ABORTS self.debug, "Abort completed (remote abort)");
		self.abort_completed = true;
		self.read_q.send_eof();		//This will shutdown reads.
	}
	fn read_error_abort(&mut self, message: Cow<'static, str>)
	{
		debug!(ABORTS self.debug, "read_error_abort: {}", message);
		//EOF on input. This is TLS error. Remote aborts complete instantly.
		self.remote_tls_abort = true;
		debug!(ABORTS self.debug, "Abort completed (read error)");
		self.abort_completed = true;
		self.error = Some(AbortReason::ReadError(message));
		self.recv_q.send_eof();
	}
/*	This function is not used anymore.
	fn write_error_abort(&mut self, message: Cow<'static, str>)
	{
		//Do local abort. We can't send alert however (connection has been lost), so complete instantly.
		debug!(ABORTS self.debug, "write_error_abort: {}", message);
		self.local_tls_abort = true;
		debug!(ABORTS self.debug, "Abort completed (write error)");
		self.abort_completed = true;
		self.error = Some(AbortReason::WriteError(message));
		self.send_q.abort();
	}
*/
	fn use_protector(&mut self)
	{
		//This is about tracking uses, so can be saturating add.
		self.protector_used = self.protector_used.saturating_add(1);
		if self.protector_used >= MAX_PROTECTOR_USES_FIRST && !self.protector_changed_once {
			self.do_change_protector = true;
			self.protector_changed_once = true;
		}
		if self.protector_used >= MAX_PROTECTOR_USES { self.do_change_protector = true; }
	}
	fn encrypt_scratch_inplace(&mut self, rtype: u8, range: Range<usize>) -> Result<Range<usize>, ()>
	{
		self.use_protector();
		self.protector.protect(&mut self.scratch, rtype, range, 0)
	}
	fn _process_one_rx_packet_buffer(&mut self, controller: &mut ControlHandler, buffer: &mut [u8]) ->
		Result<bool, TlsFailure>
	{
		//Shut off packet processing on error.
		if self.local_tls_abort || self.remote_tls_abort { return Ok(false); }

		impl_tls13_cleartext_alert!(self, buffer, buffer.len().wrapping_sub(5));

		//Actually receive the data.
		impl_rx_packet_tail!(self, buffer, controller);
	}
	fn _process_one_rx_packet(&mut self, pktlen: usize, controller: &mut ControlHandler) ->
		Result<bool, TlsFailure>
	{
		//Sanity-check the pktlen.
		sanity_check!(pktlen <= 65540, "Packet length received out of whack ({})", pktlen);
		//Shut off packet processing on error.
		if self.local_tls_abort || self.remote_tls_abort { return Ok(false); }

		//Try reading the packet.
		let clen = pktlen + 5;		//Pktlen is at most 18000 or so.
		fail_if!(clen > self.scratch.len(), TlsFailure::RxRecordTooBig(clen));
		let len = self.recv_q.peek(&mut self.scratch[..clen]).ok_or(assert_failure!("No packet to \
			process"))?;
		//Complete packet?
		if len != clen { return Ok(false); }
		//Yes, discard the data.
		self.recv_q.discard(clen).map_err(|x|assert_failure!("Discarding from receive queue failed: {}",
			x))?;

		impl_tls13_cleartext_alert!(self, self.scratch, clen);

		//Actually receive the data. Len = clen, and len is at most size of scratch buffer.
		impl_rx_packet_tail!(self, &mut self.scratch[..clen], controller);
	}
	fn make_session_receiver(&self) -> SessionControllerReceiver
	{
		SessionControllerReceiver {
			abort: None,
			next_extractor: None,
			next_protector: None,
			next_deprotector: None,
			queued_messages: Vec::new(),
			use_tls12_records: false,
			tx_ku_flag: self.last_tx_key_update,
			hs_rx_finished: false,
			max_fragment_length: None,
		}
	}
	fn process_session_update(&mut self, receiver: SessionControllerReceiver) -> bool
	{
		if let Some(x) = receiver.max_fragment_length {
			self.set_max_fragment_length(x);
		}
		for i in receiver.queued_messages {
			self.send_handshake(i.0, &i.1, i.2);
		}
		if let Some(x) = receiver.abort {
			self.abort_handshake(x);
			return false;
		}
		if let Some(x) = receiver.next_extractor {
			self.extractor = x;
		}
		if let Some(x) = receiver.next_protector {
			self.protector_used = 0;
			self.do_change_protector = false;
			self.protector = x;
		}
		if let Some(x) = receiver.next_deprotector {
			self.deprotector = x;
		}
		if receiver.use_tls12_records {
			self.set_use_tls12_records();
		}
		if receiver.hs_rx_finished {
			self.finish_rx_handshake();
		}
		//tx_ku_flag stays constant.
		true
	}
	fn prod_protector_update(&mut self, controller: &mut ControlHandler)
	{
		let mut receiver = self.make_session_receiver();
		controller.handle_tx_update(&mut receiver);
		self.process_session_update(receiver);
	}
	fn process_recv_q(&mut self, controller: &mut ControlHandler) -> Result<(), TlsFailure>
	{
		while self.recv_q.buffered() >= 5 {
			let mut pkthdr = [0; 5];
			match self.recv_q.peek(&mut pkthdr) {
				Some(5) => (),	//Should grab 5 bytes.
				_ => sanity_failed!("Shortread record header without known reason")
			};
			//pkthdr is 5 bytes as per above.
			let pktlen = (pkthdr[3] as usize) << 8 | pkthdr[4] as usize;
			fail_if!(pktlen > MAX_TLS_PACKET, TlsFailure::RxRecordTooBig(pktlen));
			if !self._process_one_rx_packet(pktlen, controller)? { return Ok(()); }
		}
		Ok(())
	}
	fn drain_tx_buffer<W:IoWrite>(&mut self, stream: &mut W) -> IoResult<bool>
	{
		if self.send_q.buffered() > 0 {
			//This is not none because buffered() > 0.
			let len = self.send_q.peek(&mut self.scratch).ok_or(IoError::new(IoErrorKind::Other,
				TlsFailure::from(assert_failure!("Peeking send queue failed"))))?;
			//Record errors from stream writes.
			//Don't abort on write-error.
			let len = stream.write(&mut self.scratch[..len])?;
			self.send_q.discard(len).map_err(|x|IoError::new(IoErrorKind::Other,
				TlsFailure::from(assert_failure!("Discarding from send queue failed: {}", x))))?;
			//Set the outgoing EOF flag or abort completed if we have gotten that far.
			if let Some(ref mut remaining) = self.bytes_to_outgoing_eof {
				*remaining -= min(*remaining, len);
				if *remaining == 0 {
					if self.local_tls_abort {
						debug!(ABORTS self.debug, "Abort completed (sent alert)");
						self.abort_completed = true;
					} else {
						self.outgoing_eof = true;
					}
				}
			}
			return Ok(true);
		}
		return Ok(false);
	}
	fn fill_rx_buffer<R:IoRead>(&mut self, stream: &mut R) -> IoResult<()>
	{
		if self.recv_q.eof_sent() { return Ok(()); }
		//Don't abort on low-level read errors.
		let len = match stream.read(&mut self.scratch)? {
			x if x > 0 => x,
			_ => {
				//But do abort on unexpected EOF.
				self.read_error_abort(cow!("Unexpected end of input"));
				fail!(IoError::new(IoErrorKind::UnexpectedEof, "Unexpected TLS stream EOF"));
			}
		};
		//We assume this can't be EOF'd.
		match self.recv_q.write(&self.scratch[..len]) {
			Ok(_) => Ok(()),
			Err(x) => Err(IoError::new(IoErrorKind::Other, TlsFailure::from(assert_failure!(
				"Error writing to receive queue: {}", x)))),
		}
	}
	fn send_encrypted(&mut self, rtype: u8, plen: usize, do_irq: bool)
	{
		//plen can be at most MAX_TLS_RECORD_PLAIN.
		if plen > self.max_write_size || plen > MAX_TLS_RECORD_PLAIN {
			self.do_local_abort(TlsFailure::from(assert_failure!("Packet length to send out of whack \
				({})", plen)));
			return;
		}
		if rtype != PROTO_HANDSHAKE { self.last_tx_key_update = false; }	//Can't be KeyUpdate.
		//Using this when processing abort causes double abort if it fails.
		//plen + MIN_TLS_OFFSET is smaller than scratch buffer.
		let endoff = plen + MIN_TLS_OFFSET;
		let range = match self.encrypt_scratch_inplace(rtype, MIN_TLS_OFFSET..endoff) {
			Ok(x) => x,
			Err(_) => {
				self.do_local_abort(TlsFailure::from(assert_failure!("Protecting packet failed")));
				return;
			}
		};
		let ret = match self.send_q.write(&self.scratch[range]) {
			Ok(_) => (),
			Err(x) => {
				self.do_local_abort(TlsFailure::from(assert_failure!("Writing to send queue \
					failed: {}", x)));
				return;
			}
		};
		if do_irq { self.maybe_send_tx_irq(false); }
		ret
	}
	fn wants_write(&self) -> bool
	{
		//If abort has finished, we are done.
		if self.abort_completed { return false; }
		//If we have pending TX interrupt, service it.
		if self.tx_irq_active { return true; }
		//If we want to change protector, we want to write it out.
		if self.do_change_protector { return true; }
		//If we have data in ciphertext send buffer, we want to write it out.
		if self.send_q.buffered() > 0 { return true; }
		//If we have EOF'd write queue, we want to write EOF out.
		if self.write_q.eof_sent() && self.write_q.buffered() == 0 && !self.queued_eof { return true; }
		//If we have abort code, we want to write it out.
		if self.abort_code.is_some() { return true; }
		let aborted = self.abort_completed || self.outgoing_eof;
		let buffered = self.write_q.buffered();
		let enough = buffered >= self.max_write_size;				//Full record
		let enough = enough || buffered >= self.send_threshold;			//Over threshold
		let enough = enough || (self.flush_remaining > 0 && buffered > 0);	//Flushing.
		let enough = enough || (self.nodelay_flag && buffered > 0);		//Nodelay
		let enough = enough || (self.write_q.eof_sent() || !aborted);		//EOF.
		//If handshake has been sent and we have data in write queue (or EOF), we want to send out.
		if self.tx_hs_finished && enough { return true; }
		//Otherwise we stay quiescent.
		false
	}
}

impl SessionBase
{
	pub fn set_high_water_mark(&mut self, amount: usize)
	{
		if self.internal_only { return; }
		self.high_water_mark = amount;
	}
	pub fn set_send_threshold(&mut self, threshold: usize)
	{
		if self.internal_only { return; }
		self.nodelay_flag = threshold == 0;
		self.send_threshold = threshold;
		self.maybe_send_tx_irq(false);
	}
	pub fn bytes_available(&self) -> usize
	{
		if self.internal_only { return 0; }
		self.read_q.buffered()
	}
	pub fn bytes_queued(&self) -> usize
	{
		if self.internal_only { return 0; }
		self.write_q.buffered()
	}
	pub fn is_eof(&self) -> bool
	{
		self.read_q.eof_sent() && self.read_q.buffered() == 0
	}
	pub fn wants_read(&self) -> bool
	{
		//If we have abort, don't read more.
		if self.remote_tls_abort || self.local_tls_abort { return false; }
		//If we already got EOF, don't read more.
		if self.recv_q.eof_sent() { return false; }
		//If handshake has been received and amount in read buffer is below high water mark, do read.
		if self.rx_hs_finished && !self.read_q.eof_sent() &&
			self.read_q.buffered() < self.high_water_mark {
			return true;
		};
		//Even if read EOF, we want to listen for ALERT/HANDSHAKE messages.
		if self.rx_hs_finished && self.read_q.eof_sent() { return true; }
		//If rx handshake isn't finished yet and message is requested, hear it.
		if !self.rx_hs_finished && self.rx_hs_request {
			return true;
		}
		return false;
	}
	pub fn set_tx_irq(&mut self, handler: Box<TxIrqHandler+Send>)
	{
		self.tx_irq = handler;
		self.maybe_send_tx_irq(true);
	}
	pub fn set_appdata_rx_fn(&mut self, handler: Option<Box<RxAppdataHandler+Send>>)
	{
		self.appdata_rx_fn = handler;
	}
	fn maybe_set_txirq_for_alert(&mut self)
	{
		//The recv_q processing might have resulted an abort. Assert TX IRQ in that case, so we send the
		//alert.
		if self.abort_code.is_some() && !self.abort_completed {
			self.maybe_send_tx_irq(false);
		}
	}
	pub fn read_tls<R:IoRead>(&mut self, stream: &mut R, controller: &mut ControlHandler) -> IoResult<()>
	{
		if !self.wants_read() { return Ok(()); }
		//We can't be here with complete packet, so on any error, no need to check for complete packets.
		self.fill_rx_buffer(stream)?;
		match self.process_recv_q(controller) {
			Ok(_) => (),
			Err(x) => self.do_local_abort(x)
		};
		self.maybe_set_txirq_for_alert();
		Ok(())
	}
	pub fn submit_tls_record(&mut self, record: &mut [u8], controller: &mut ControlHandler) -> Result<(),
		TlsFailure>
	{
		sanity_check!(self.wants_read(), "No record expected currently");
		match self._process_one_rx_packet_buffer(controller, record) {
			Ok(_) => (),
			Err(x) => self.do_local_abort(x)
		};
		self.maybe_set_txirq_for_alert();
		Ok(())
	}
	fn try_refill_tx(&mut self, force_write: bool, controller: &mut ControlHandler) -> IoResult<()>
	{
		if self.max_write_size > MAX_TLS_RECORD_PLAIN {
			return Err(IoError::new(IoErrorKind::Other, TlsFailure::from(assert_failure!(
				"Max write size ({}) must be at most {}", self.max_write_size,
				MAX_TLS_RECORD_PLAIN))));
		}
		//Check for EOF. Note that other aborts take percedence. Also, don't do this multiple times!
		if self.write_q.eof_sent() && self.write_q.buffered() == 0 && self.abort_code.is_none() &&
			!self.queued_eof {
			self.do_local_abort(TlsFailure::NormalClose);
		}

		let buffered = self.write_q.buffered();
		let enough = buffered >= self.max_write_size;				//Full record
		let aborted = self.abort_completed || self.outgoing_eof;
		//Over threshold
		let enough = enough || (!self.nodelay_flag && buffered >= self.send_threshold);
		let enough = enough || (self.flush_remaining > 0 && buffered > 0);	//Flushing.
		let enough = enough || (self.nodelay_flag && buffered > 0);		//Nodelay
		let enough = enough || (force_write && buffered > 0);			//Forced write.
		let enough = enough || (self.write_q.eof_sent() && !aborted);		//EOF.

		if let Some(acode) = self.abort_code {
			self.scratch[MIN_TLS_OFFSET+0] = if is_close_alert(acode) && !self.is_tls13 { 1 } else { 2 };
			self.scratch[MIN_TLS_OFFSET+1] = acode;
			self.send_encrypted(PROTO_ALERT, 2, false);
			self.write_q.abort();
			//Schedule outgoing_eof or abort_completed to be set when this alert message has been sent.
			self.bytes_to_outgoing_eof = Some(self.send_q.buffered());
			self.abort_code = None;
		} else if self.tx_hs_finished && enough {
			let len = {
				//MAX_TLS_RECORD_PLAIN is guaranteed to fit, and max_write_size is smaller.
				let buf = &mut self.scratch[MIN_TLS_OFFSET..][..self.max_write_size];
				self.write_q.read(buf).ok_or(IoError::new(IoErrorKind::Other,
					TlsFailure::from(assert_failure!("Can't read from write queue"))))?
			};
			//The flush remaining can at most go to 0.
			self.flush_remaining = self.flush_remaining.saturating_sub(len);
			self.send_encrypted(PROTO_APPDATA, len, false);
		}
		if self.tx_hs_finished && self.do_change_protector {
			self.do_change_protector = false;
			self.prod_protector_update(controller);
		}
		Ok(())
	}
	fn try_flush_tx<W:IoWrite>(&mut self, stream: &mut W, zlsub: bool) -> IoResult<bool>
	{
		if self.drain_tx_buffer(stream)? {
			if !zlsub {
				//If zlsub=true, we don't reassert TX IRQ here.
				self.maybe_send_tx_irq(false);	//Do this in case we didn't transmit everything.
			}
			return Ok(true);
		}
		Ok(false)
	}
	pub fn write_tls<W:IoWrite>(&mut self, stream: &mut W, controller: &mut ControlHandler, zlsub: bool) ->
		IoResult<()>
	{
		//If TX IRQ is active, we force flushing of data, to prevent changing send thresholds from
		//effectively clearing the TX IRQ. Also, if we are serving zerolatency write, force flush.
		let force_write = self.tx_irq_active || zlsub;
		self.tx_irq_active = false;	//Disarm TX IRQ.
		self.tx_request = false;	//Disarm TX request.
		if self.try_flush_tx(stream, zlsub)? { return Ok(()); }
		//Nothing to send yet, try refilling the buffer and then sending again.
		self.try_refill_tx(force_write, controller)?;
		self.try_flush_tx(stream, zlsub)?;	//Igore return.
		Ok(())
	}
	pub fn zerolatency_write(&mut self, controller: &mut ControlHandler, output: &mut [u8], input: &[u8]) ->
		Result<(usize, usize), TlsFailure>
	{
		let mut outsize;
		let mut insize = 0;
		{
			//First, do flush of existing data. This never flushes input, so insize stays at 0.
			let mut tmp = SliceWrite(output, 0);
			match self.write_tls(&mut tmp, controller, true) {
				Ok(_) => (),
				Err(x) => {
					//We assume any error can be downcasted to TlsFailure.
					if let Some(y) = x.into_inner() {
						if let Some(z) = y.downcast_ref::<TlsFailure>() {
							fail!(z.clone());
						}
					}
					sanity_failed!("Unknown I/O exception in zerolatency write");
				}
			}
			outsize = tmp.1;
		}
		//Can't write to internal connections or aborted connections.
		//Also can't write if there is anything in write_q or if write_q is EOF'd.
		let cant_write = self.internal_only || self.local_tls_abort || self.remote_tls_abort ||
			!self.tx_hs_finished;
		if !cant_write && input.len() > 0 && self.write_q.buffered() == 0 && !self.write_q.eof_sent() {
			//We might flush some data. No need to check for record protection update, since write_tls
			//already sent that if needed.
			while insize < input.len() {
				let dst = &mut output[outsize..];
				let src = &input[insize..];
				let (outdelta, indelta) = match self.protector.protect_coutput(dst, src,
					PROTO_APPDATA, self.max_write_size) {
					Ok((x, y)) => (x, y),
					Err(_) => {
						self.do_local_abort(TlsFailure::from(assert_failure!("Protecting \
							packet failed")));
						break;
					}
				};
				if outdelta > 0 {
					self.use_protector();
				} else {
					break;	//No more progress
				}
				outsize += outdelta;
				insize += indelta;
			}
		}
		if outsize > 0 {
			//If we sent anything, check if TX IRQ should be reasserted.
			self.maybe_send_tx_irq(false);
		}
		Ok((outsize, insize))
	}
	pub fn estimate_zerolatency_size(&self, mut outsize: usize) -> usize
	{
		let mut ret = 0usize;
		let cant_write = self.internal_only || self.local_tls_abort || self.remote_tls_abort ||
			self.tx_hs_finished;
		if cant_write || self.write_q.buffered() != 0 || self.write_q.eof_sent() ||
			self.send_q.buffered() != 0 { return 0; }
		let overhead = self.protector.get_overhead();
		if self.do_change_protector {
			//We really need saturating_sub here, as outsize might not be big enough.
			outsize = outsize.saturating_sub(overhead + 5);
		}
		let maxunit = overhead + self.max_write_size;	//Overhead should be much smaller than usize max.
		while outsize >= maxunit {
			outsize -= maxunit;		//Doesn't underflow by check above.
			ret += self.max_write_size;
		}
		ret + outsize.saturating_sub(overhead)	//Outsize might be smaller than overhead.
	}
	pub fn can_tx_data(&self) -> bool
	{
		if self.internal_only { return false; }	//Internal connections never complete HS.
		self.tx_hs_finished
	}
	pub fn send_eof(&mut self)
	{
		if self.internal_only { return; }
		self.write_q.send_eof();
		self.maybe_send_tx_irq(false);
	}
	pub fn aborted_by(&self) -> Option<AbortReason>
	{
		if self.incoming_eof && self.outgoing_eof { return Some(AbortReason::MutualEof); }
		if !self.abort_completed { return None; }	//Delay reporting until abort is complete.
		self.error.clone()
	}
	pub fn extractor(&self, label: &str, context: Option<&[u8]>, buffer: &mut [u8]) -> Result<(), TlsFailure>
	{
		self.extractor.extract(label, context, buffer)
	}
	pub fn get_record_size(&self, buffer: &[u8]) -> Option<usize>
	{
		if buffer.len() < 5 { return None }
		let len = (buffer[3] as usize) * 256 + (buffer[4] as usize);
		Some(len + 5)
	}
	pub fn begin_transacted_read<'a>(&self, buffer: &'a mut [u8]) -> Result<&'a [u8], TlsFailure>
	{
		if self.internal_only { return Ok(&[]); }	//Pretend no data.
		let aborted = self.local_tls_abort || self.remote_tls_abort;
		fail_if!(aborted, TlsFailure::TlsSessionError);
		match self.read_q.peek(buffer) {
			Some(x) => Ok(&buffer[..x]),
			None => Ok(&[])
		}
	}
	pub fn end_transacted_read(&mut self, len: usize) -> Result<(), TlsFailure>
	{
		if len == 0 { return Ok(()); }
		sanity_check!(!self.internal_only, "Can't discard data from internal connections");
		let aborted = self.local_tls_abort || self.remote_tls_abort;
		fail_if!(aborted, TlsFailure::TlsSessionError);
		match self.read_q.discard(len) {
			Ok(_) => Ok(()),
			Err(x) => sanity_failed!("Discarding from read queue failed: {}", x)
		}
	}
}

impl IoRead for SessionBase
{
	fn read(&mut self, buf: &mut [u8]) -> IoResult<usize>
	{
		//Can't read from internal connections.
		fail_if!(self.internal_only, IoError::new(IoErrorKind::WouldBlock, "Internal connection, \
			can't read"));
		let aborted = self.local_tls_abort || self.remote_tls_abort;
		match self.read_q.read(buf) {
			Some(x) if x > 0 => Ok(x),
			Some(_) => Err(IoError::new(IoErrorKind::WouldBlock, "Unencrypted RX Buffer empty, \
				try again later")),
			None if !aborted => Ok(0),
			None => Err(IoError::new(IoErrorKind::ConnectionReset, "TLS connection error")),
		}
	}
}

impl IoWrite for SessionBase
{
	fn write(&mut self, buf: &[u8]) -> IoResult<usize>
	{
		if self.internal_only { return Ok(0); }	//Can't write to internal connections.
		let aborted = self.local_tls_abort || self.remote_tls_abort;
		if aborted {
			fail!(IoError::new(IoErrorKind::ConnectionReset, "TLS connection already aborted on error"));
		}
		let ret = match self.write_q.write(buf) {
			Ok(x) => x,
			Err(QueueWriteError::Eof) if !aborted => fail!(IoError::new(IoErrorKind::BrokenPipe,
				"TX EOF has already been signaled")),
			Err(_) => fail!(IoError::new(IoErrorKind::ConnectionReset, "TLS connection error"))
		};
		if ret > 0 { self.maybe_send_tx_irq(false); };
		Ok(ret)
	}
	fn flush(&mut self) -> IoResult<()>
	{
		if self.internal_only { return Ok(()); }	//Can't write to internal connections.
		let aborted = self.local_tls_abort || self.remote_tls_abort;
		if aborted {
			fail!(IoError::new(IoErrorKind::ConnectionReset, "TLS connection already aborted on error"));
		}
		let amount = self.write_q.buffered();
		if self.tx_hs_finished && amount > 0 {
			self.flush_remaining = amount;
			self.maybe_send_tx_irq(false);
		}
		Ok(())
	}
}

#[test]
fn null_protector_basic()
{
	let mut msg = [3;128];
	let msg2 = [3;42-5];
	let mut e = NullProtector(true);
	let mut d = NullProtector(true);
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..42);
	assert_eq!(msg[0], 27);
	assert_eq!(msg[1], 3);
	assert_eq!(msg[2], 1);
	assert_eq!(msg[3], 0);
	assert_eq!(msg[4], 37);
	assert_eq!(d.deprotect(&mut msg[..42]).unwrap(), (27, 5..42));
	assert_eq!(&msg[5..42], &msg2[..]);

}

#[test]
fn null_protector_off()
{
	let mut msg = [3;128];
	let msg2 = [3;42-7];
	let mut e = NullProtector(true);
	let mut d = NullProtector(true);
	assert_eq!(e.protect(&mut msg, 27, 7..42, 0).unwrap(), 2..42);
	assert_eq!(msg[2], 27);
	assert_eq!(msg[3], 3);
	assert_eq!(msg[4], 1);
	assert_eq!(msg[5], 0);
	assert_eq!(msg[6], 35);
	assert_eq!(d.deprotect(&mut msg[2..42]).unwrap(), (27, 5..40));
	assert_eq!(&msg[7..42], &msg2[..]);

}
