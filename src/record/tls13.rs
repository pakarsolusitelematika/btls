use ::common::{HkdfLabel, Tls13Draft, TlsFailure};
use ::record::{RecordDeprotector, RecordProtector, TlsExtractor};
use ::stdlib::{min, Range};

use btls_aux_aead::{DecryptionKey, EncryptionKey, ProtectorType};
use btls_aux_hash::{HashFunction, HashOutput};

pub struct Protector(EncryptionKey, [u8;12], u64, bool);
pub struct Deprotector(DecryptionKey, [u8;12], u64, bool);
pub struct Extractor
{
	secret: HashOutput,
	function: HashFunction,
	draft: Tls13Draft,
}

static AES_128_GCM: ProtectorType = ProtectorType::Aes128Gcm;
static AES_256_GCM: ProtectorType = ProtectorType::Aes256Gcm;
static CHACHA20_POLY1305: ProtectorType = ProtectorType::Chacha20Poly1305;

impl Protector
{
	pub fn new_aes128(key: &[u8]) -> Result<Self, ()>
	{
		fail_if!(key.len() != 28, ());
		let (key_k, key_iv) = key.split_at(16);
		let mut iv = [0; 12];
		iv.copy_from_slice(key_iv);
		Ok(Protector(AES_128_GCM.new_encryptor(key_k)?, iv, 0, false))
	}
	pub fn new_aes256(key: &[u8]) -> Result<Self, ()>
	{
		fail_if!(key.len() != 44, ());
		let (key_k, key_iv) = key.split_at(32);
		let mut iv = [0; 12];
		iv.copy_from_slice(key_iv);
		Ok(Protector(AES_256_GCM.new_encryptor(key_k)?, iv, 0, false))
	}
	pub fn new_chacha20(key: &[u8]) -> Result<Self, ()>
	{
		fail_if!(key.len() != 44, ());
		let (key_k, key_iv) = key.split_at(32);
		let mut iv = [0; 12];
		iv.copy_from_slice(key_iv);
		Ok(Protector(CHACHA20_POLY1305.new_encryptor(key_k)?, iv, 0, false))
	}
}

const TAG_SIZE_OVERHEAD: usize = 16;
const MAX_RECORD_PTXT: usize = 16384;
const MAX_RECORD_CTXT: usize = 16384 + 256;

impl RecordProtector for Protector
{
	fn protect(&mut self, buf: &mut [u8], rtype: u8, range: Range<usize>, pad: usize) -> Result<Range<usize>, ()>
	{
		//If context has been destroyed, fail.
		fail_if!(self.3, ());
		//The range must be valid. There must be at least 5 bytes of space before start for header.
		fail_if!(range.start < 5, ());
		fail_if!(range.end < range.start, ());
		fail_if!(buf.len() < range.end, ());
		//Size of packet data. We have checked that range.end >= range.start.
		let pktsize = range.end - range.start;
		//Check that the record fits into 16384 bytes reserved space.
		fail_if!(pktsize > MAX_RECORD_PTXT, ());
		//Pad the packet by up to pad bytes, but only up to 16384.
		let mut _iv: [u8; 12] = self.1;
		//We never let pktsize + pad go over 16*1024.
		let opktsize = min(MAX_RECORD_PTXT, pktsize.saturating_add(pad));
		//Compute the size of ciphertext. TAG_SIZE_OVERHEAD+1 is at most few hundred and opktsize is at most
		//16384, so this is under 20k, and so fits to usize.
		let extlen = opktsize + TAG_SIZE_OVERHEAD + 1;
		fail_if!(extlen > MAX_RECORD_CTXT, ());
		//extlen must fit after range.start.
		fail_if!(buf.len() - range.start < extlen, ());
		//Range.start is at least 5 by check above, so this is in-range.
		let dstart = range.start - 5;
		//buf.len() >= extlen + range.start, so the latter fits into usize.
		let dend = extlen + range.start;
		//Write header and trailer.
		buf[dstart+0] = 23;	//Outer type is always 23.
		buf[dstart+1] = 3;	//"TLS 1.0"
		buf[dstart+2] = 1;
		buf[dstart+3] = (extlen >> 8) as u8;
		buf[dstart+4] = extlen as u8;
		//Real rtype. pktsize <= opktsize < extlen and index is thus < dend, which is in bounds.
		buf[range.start + pktsize] = rtype;
		//Write padding zeroes. All the indices involved are < dend, since dend = range.start + extlen, and
		//opktsize + 1 <= extlen.
		for i in pktsize..opktsize { buf[range.start + i + 1] = 0; }
		for i in 0..8usize {
			let x = (self.2.wrapping_shr((!(i as u32) & 7) << 3)) as u8;
			_iv[i+4] ^= x;
		}
		//This is in-range, since range.start + extlen = dend, which is known to be valid bound.
		self.0.encrypt(&_iv, &[], &mut buf[range.start..dend], 16).map_err(|_|())?;
		//Increment the RSN. We need to check for overflow.
		self.2 = match self.2.checked_add(1) { Some(x) => x, None => {self.3 = true; self.2 } };
		//Return the range.
		Ok(dstart..dend)
	}
	fn protect_coutput(&mut self, out: &mut [u8], inp: &[u8], rtype: u8, max_write_size: usize) ->
		Result<(usize, usize), ()>
	{
		let hdrsize = 5usize;
		let overhead = hdrsize.saturating_add(TAG_SIZE_OVERHEAD+1);
		if out.len() < overhead { return Ok((0, 0)); }
		//out.len() >= overhead by check above, so difference is positive.
		let len = min(min(MAX_RECORD_PTXT, max_write_size), min(out.len() - overhead, inp.len()));
		//Copy from inp[..len] to out[hdrsize..][..len]. The first is in range, since
		//len <= inp.len() and the latter since len <= out.len() - overhead and so out.len() >=
		//len + overhead > len + hdrsize.
		(&mut out[hdrsize..][..len]).copy_from_slice(&inp[..len]);
		//len + overhead <= out.len() - overhead + overhead = out.len(), so this fits into usize.
		let tsize = len + overhead;
		//len + overhead <= out.len() - overhead + overhead = out.len(), so tsize fits into out.
		//The range start this gives will be 0. Also, by above hdrsize + len fits into usize.
		let outrange = self.protect(&mut out[..tsize], rtype, hdrsize..hdrsize+len, 0)?;
		//Return the range sizes, both are based on 0.
		Ok((outrange.end, len))
	}
	fn get_overhead(&self) -> usize
	{
		//5 byte base header, 1 byte real type, and tag.
		6 + TAG_SIZE_OVERHEAD
	}
}

impl Deprotector
{
	pub fn new_aes128(key: &[u8]) -> Result<Self, ()>
	{
		fail_if!(key.len() != 28, ());
		let (key_k, key_iv) = key.split_at(16);
		let mut iv = [0; 12];
		iv.copy_from_slice(key_iv);
		Ok(Deprotector(AES_128_GCM.new_decryptor(key_k)?, iv, 0, false))
	}
	pub fn new_aes256(key: &[u8]) -> Result<Self, ()>
	{
		fail_if!(key.len() != 44, ());
		let (key_k, key_iv) = key.split_at(32);
		let mut iv = [0; 12];
		iv.copy_from_slice(key_iv);
		Ok(Deprotector(AES_256_GCM.new_decryptor(key_k)?, iv, 0, false))
	}
	pub fn new_chacha20(key: &[u8]) -> Result<Self, ()>
	{
		fail_if!(key.len() != 44, ());
		let (key_k, key_iv) = key.split_at(32);
		let mut iv = [0; 12];
		iv.copy_from_slice(key_iv);
		Ok(Deprotector(CHACHA20_POLY1305.new_decryptor(key_k)?, iv, 0, false))
	}
}

impl RecordDeprotector for Deprotector
{
	fn deprotect(&mut self, buf: &mut [u8]) -> Result<(u8, Range<usize>), ()>
	{
		//If context has been destroyed, fail.
		fail_if!(self.3, ());
		//Iv.
		let mut _iv: [u8; 12] = self.1;
		for i in 0..8usize {
			let x = (self.2.wrapping_shr((!(i as u32) & 7) << 3)) as u8;
			_iv[i+4] ^= x;
		}
		fail_if!(buf.len() < 5, ());
		fail_if!(buf[0] != 23, ());		//Bad outer type.
		//By check above, buf.len() >= 5.
		let psize = buf.len() - 5;
		//These fall in 5 byte header. Check the record size.
		fail_if!(buf[3] != (psize >> 8) as u8, ());
		fail_if!(buf[4] != psize as u8, ());

		//Buf is at least 5 bytes, so slice is in-range.
		let outlen = self.0.decrypt(&_iv, &[], &mut buf[5..]).map_err(|_|())?;
		//outlen <= buf.len() - 5, so outlen + 5 - 1 is a valid index and also fits into usize (to the last
		//byte).
		let mut pktend = outlen + 5 - 1;
		//While in payload and byte zero... pktend must be at least 1 in loop.
		while pktend > 4 && buf[pktend] == 0 { pktend -= 1; }
		//This may be invalid index. However, then it must be 4, which can be read.
		let rtype = buf[pktend];
		//If padding is valid, pktend stays out of header. It might decrement all the way to 5 for empty
		//record.
		fail_if!(pktend < 5, ());
		//Increment the RSN. We have to check for overflow.
		self.2 = match self.2.checked_add(1) { Some(x) => x, None => {self.3 = true; self.2 } };
		//Return range.
		Ok((rtype, 5..pktend))
	}
}

impl Extractor
{
	pub fn new(master_key: &HashOutput, prf: HashFunction, draft: Tls13Draft) -> Result<Self, TlsFailure>
	{
		Ok(Extractor{secret:master_key.clone(), function:prf, draft:draft})
	}
}

impl TlsExtractor for Extractor
{
	fn extract(&self, label: &str, context: Option<&[u8]>, buffer: &mut [u8]) -> Result<(), TlsFailure>
	{
		let context = context.unwrap_or(&[]);	//Defaults to empty.
		//The maximum output is 255 output blocks, or 65535 bytes, whatever is less.
		let max_hkdf = min(255 * self.function.output_length(), 65535);
		sanity_check!(buffer.len() <= max_hkdf, "TLS 1.3 exporter output too long ({}, max {} bytes)",
			buffer.len(), max_hkdf);
		if self.draft.at_least_19() {
			//Draft-19 hashes the context, rederives key on label (supposedly for forward secrecy, but
			//we can't use it) and doesn't have length limit.
			let empty_hash = self.function.oneshot(|_|{}).map_err(|x|assert_failure!("Internal error \
				in hash function: {}", x))?;
			let tmpkey = self.function.tls13_derive_secret(&self.secret.as_ref(), HkdfLabel::Custom(
				label).get(self.draft), &empty_hash).map_err(|x|assert_failure!("Hash function \
				internal error: {}", x))?;
			let hctx = self.function.oneshot(|x|x.input(context)).map_err(|x|assert_failure!(
				"Internal error in hash function: {}", x))?;
			self.function.tls13_hkdf_expand_label(tmpkey.as_ref(), HkdfLabel::ExporterD.get(self.draft),
				hctx.as_ref(), buffer).map_err(|x|assert_failure!("TLS 1.3 extractor: Internal \
				error in hash function: {}", x))?;
		} else {
			//These sanity checks are only meaningful in non-draft-19 context.
			sanity_check!(label.len() <= 246, "TLS 1.3 exporter label too long ({}, max 246 bytes)",
				label.len());
			sanity_check!(context.len() <= 255, "TLS 1.3 exporter context too long ({}, max 255 bytes)",
				context.len());
			self.function.tls13_hkdf_expand_label(&self.secret.as_ref(), HkdfLabel::Custom(label).get(
				self.draft), context, buffer).map_err(|x|assert_failure!("TLS 1.3 extractor: \
				Internal error in hash function: {}", x))?;
		};
		Ok(())
	}
}

#[test]
fn basic_protectors()
{
	let key = [1;44];
	let mut msg = [0;128];
	let msg2 = [0;42-5];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..59);
	assert_eq!(msg[0], 23);
	assert_eq!(msg[1], 3);
	assert_eq!(msg[2], 1);
	assert_eq!(msg[3], 0);
	assert_eq!(msg[4], 54);
	assert_eq!(d.deprotect(&mut msg[..59]).unwrap(), (27, 5..42));
	assert_eq!(&msg[5..42], &msg2[..]);
}

#[test]
fn basic_protectors_off()
{
	let key = [1;44];
	let mut msg = [0;128];
	let msg2 = [0;42-7];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 7..42, 0).unwrap(), 2..59);
	assert_eq!(msg[2], 23);
	assert_eq!(msg[3], 3);
	assert_eq!(msg[4], 1);
	assert_eq!(msg[5], 0);
	assert_eq!(msg[6], 52);
	assert_eq!(d.deprotect(&mut msg[2..59]).unwrap(), (27, 5..40));
	assert_eq!(&msg[7..42], &msg2[..]);
}

#[test]
fn basic_protectors_pad()
{
	let key = [1;44];
	let mut msg = [0;128];
	let msg2 = [0;42-5];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	msg[50] = 255;
	assert_eq!(e.protect(&mut msg, 27, 5..42, 11).unwrap(), 0..70);
	assert_eq!(msg[0], 23);
	assert_eq!(msg[1], 3);
	assert_eq!(msg[2], 1);
	assert_eq!(msg[3], 0);
	assert_eq!(msg[4], 65);
	assert_eq!(d.deprotect(&mut msg[..70]).unwrap(), (27, 5..42));
	assert_eq!(&msg[5..42], &msg2[..]);
}

#[test]
fn basic_protectors_exact()
{
	let key = [1;44];
	let mut msg = [0;59];
	let msg2 = [0;42-5];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..59);
	assert_eq!(msg[0], 23);
	assert_eq!(msg[1], 3);
	assert_eq!(msg[2], 1);
	assert_eq!(msg[3], 0);
	assert_eq!(msg[4], 54);
	assert_eq!(d.deprotect(&mut msg[..59]).unwrap(), (27, 5..42));
	assert_eq!(&msg[5..42], &msg2[..]);
}

#[test]
fn basic_protectors_pad_big()
{
	let key = [1;44];
	let mut msg = [0;20*1024];
	let msg2 = [0;42-5];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	msg[50] = 255;
	assert_eq!(e.protect(&mut msg, 27, 5..42, 32768).unwrap(), 0..16406);
	assert_eq!(msg[0], 23);
	assert_eq!(msg[1], 3);
	assert_eq!(msg[2], 1);
	assert_eq!(msg[3], 0x40);
	assert_eq!(msg[4], 0x11);
	assert_eq!(d.deprotect(&mut msg[..16406]).unwrap(), (27, 5..42));
	assert_eq!(&msg[5..42], &msg2[..]);
}

#[test]
fn basic_protectors_multi()
{
	let key = [1;44];
	let mut msg = [0;128];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..59);
	assert_eq!(d.deprotect(&mut msg[..59]).unwrap(), (27, 5..42));
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..59);
	assert_eq!(d.deprotect(&mut msg[..59]).unwrap(), (27, 5..42));
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..59);
	assert_eq!(d.deprotect(&mut msg[..59]).unwrap(), (27, 5..42));
	assert_eq!(e.2, 3);
	assert_eq!(d.2, 3);
}

#[test]
fn gap_too_small()
{
	let key = [1;28];
	let mut msg = [0;128];
	let mut e = Protector::new_aes128(&key).unwrap();
	e.protect(&mut msg, 27, 4..42, 0).unwrap_err();
	e.protect(&mut msg, 27, 6..42, 0).unwrap();
	let key = [1;44];
	let mut e = Protector::new_chacha20(&key).unwrap();
	e.protect(&mut msg, 27, 4..42, 0).unwrap_err();
	e.protect(&mut msg, 27, 6..42, 0).unwrap();
}

#[test]
fn basic_protectors_desync()
{
	let key = [1;44];
	let mut msg = [0;128];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..59);
	for i in msg.iter_mut() { *i = 0; }
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..59);
	assert_eq!(msg[0], 23);
	assert_eq!(msg[1], 3);
	assert_eq!(msg[2], 1);
	assert_eq!(msg[3], 0);
	assert_eq!(msg[4], 54);
	d.deprotect(&mut msg[..59]).unwrap_err();
}

#[test]
fn basic_protectors_wrap_e()
{
	let key = [1;44];
	let mut msg = [0;128];
	let mut e = Protector::new_chacha20(&key).unwrap();
	e.2 = !0u64;
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..59);
	for i in msg.iter_mut() { *i = 0; }
	e.protect(&mut msg, 27, 5..42, 0).unwrap_err();
}


#[test]
fn basic_protectors_wrap_d()
{
	let key = [1;44];
	let mut msg = [0;128];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut e2 = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	e2.2 = !0u64;
	d.2 = !0u64;
	assert_eq!(e2.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..59);
	assert_eq!(d.deprotect(&mut msg[..59]).unwrap(), (27, 5..42));
	for i in msg.iter_mut() { *i = 0; }
	assert_eq!(e2.2, !0u64);
	assert_eq!(d.2, !0u64);
	e.2 = d.2;
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..59);
	d.deprotect(&mut msg[..59]).unwrap_err();
}

#[test]
fn basic_protectors2()
{
	let key = [1;28];
	let mut msg = [0;128];
	let msg2 = [0;42-5];
	let mut e = Protector::new_aes128(&key).unwrap();
	let mut d = Deprotector::new_aes128(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..59);
	assert_eq!(msg[0], 23);
	assert_eq!(msg[1], 3);
	assert_eq!(msg[2], 1);
	assert_eq!(msg[3], 0);
	assert_eq!(msg[4], 54);
	assert_eq!(d.deprotect(&mut msg[..59]).unwrap(), (27, 5..42));
	assert_eq!(&msg[5..42], &msg2[..]);
}

#[test]
fn basic_protectors3()
{
	let key = [1;44];
	let mut msg = [0;128];
	let msg2 = [0;42-5];
	let mut e = Protector::new_aes256(&key).unwrap();
	let mut d = Deprotector::new_aes256(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..59);
	assert_eq!(msg[0], 23);
	assert_eq!(msg[1], 3);
	assert_eq!(msg[2], 1);
	assert_eq!(msg[3], 0);
	assert_eq!(msg[4], 54);
	assert_eq!(d.deprotect(&mut msg[..59]).unwrap(), (27, 5..42));
	assert_eq!(&msg[5..42], &msg2[..]);
}
