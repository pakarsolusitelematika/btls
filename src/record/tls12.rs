use ::common::TlsFailure;
use ::record::{RecordDeprotector, RecordProtector, TlsExtractor};
use ::stdlib::{min, Range};

use btls_aux_aead::{DecryptionKey, EncryptionKey, ProtectorType};
use btls_aux_hash::HashFunction;
use btls_aux_securebuf::wipe_buffer;

const MS_LEN: usize = 48;

//key, IV, RSN, vulernable to NDA, destroyed.
pub struct Protector(EncryptionKey, [u8;12], u64, bool, bool);
pub struct Deprotector(DecryptionKey, [u8;12], u64, bool, bool);
pub struct Extractor
{
	secret: [u8; MS_LEN],		//TLS 1.2 MasterSecret is always 48 bytes.
	randoms: [u8; 64],
	function: HashFunction,
}

const TAG_LENGTH: usize = 16;
const BASE_HEADER_LENGTH: usize = 5;
const CHACHA_HEADER_LENGTH: usize = BASE_HEADER_LENGTH;
const GCM_HEADER_LENGTH: usize = BASE_HEADER_LENGTH + 8;
const MAX_RECORD_PTXT: usize = 16 * 1024;
const MAX_RECORD_CTXT: usize = 18 * 1024;

static AES_128_GCM: ProtectorType = ProtectorType::Aes128Gcm;
static AES_256_GCM: ProtectorType = ProtectorType::Aes256Gcm;
static CHACHA20_POLY1305: ProtectorType = ProtectorType::Chacha20Poly1305;

impl Protector
{
	pub fn new_aes128(key: &[u8]) -> Result<Self, ()>
	{
		fail_if!(key.len() != 20, ());
		let (key_k, key_iv) = key.split_at(16);
		let mut iv = [0; 12];
		(&mut iv[..4]).copy_from_slice(key_iv);
		Ok(Protector(AES_128_GCM.new_encryptor(key_k)?, iv, 0, true, false))
	}
	pub fn new_aes256(key: &[u8]) -> Result<Self, ()>
	{
		fail_if!(key.len() != 36, ());
		let (key_k, key_iv) = key.split_at(32);
		let mut iv = [0; 12];
		(&mut iv[..4]).copy_from_slice(key_iv);
		Ok(Protector(AES_256_GCM.new_encryptor(key_k)?, iv, 0, true, false))
	}
	pub fn new_chacha20(key: &[u8]) -> Result<Self, ()>
	{
		fail_if!(key.len() != 44, ());
		let (key_k, key_iv) = key.split_at(32);
		let mut iv = [0; 12];
		iv.copy_from_slice(key_iv);
		Ok(Protector(CHACHA20_POLY1305.new_encryptor(key_k)?, iv, 0, false, false))
	}
}

const TAG_SIZE_OVERHEAD: usize = 16;

impl RecordProtector for Protector
{
	fn protect(&mut self, buf: &mut [u8], rtype: u8, range: Range<usize>, _pad: usize) ->
		Result<Range<usize>, ()>
	{
		//The size of header is 13 bytes (8+5) in GCM mode, and 5 bytes in Chacha mode.
		let hdrsize = if self.3 { GCM_HEADER_LENGTH } else { CHACHA_HEADER_LENGTH };
		//Check that range is valid
		fail_if!(range.end < range.start, ());
		fail_if!(buf.len() < range.end, ());
		fail_if!(range.start < hdrsize, ());
		//If destroyed flag is set, fail.
		fail_if!(self.4, ());
		//Check that message is 16384 bytes maximum.
		let pktsize = range.end - range.start;
		fail_if!(pktsize > MAX_RECORD_PTXT, ());

		//Additional size over standard base header. This shouldn't be even near overflow.
		let additional = hdrsize + TAG_SIZE_OVERHEAD - BASE_HEADER_LENGTH;
		//The size of output, without record base header.
		let plen = pktsize + additional;
		fail_if!(plen > MAX_RECORD_CTXT, ());
		//The size of the ciphertext output.
		let extlen = pktsize + TAG_LENGTH;
		//Check that the tag fits. range.start is smaller than buf.len() by checks above.
		fail_if!(buf.len() - range.start < extlen, ());
		let mut _iv: [u8; 12] = self.1;
		let mut _ad: [u8; 13] = [0, 0, 0, 0, 0, 0, 0, 0, rtype, 3, 3, 0, 0];
		//Write the packet plaintext size to the AD.
		_ad[11] = (pktsize >> 8) as u8;
		_ad[12] = pktsize as u8;
		//Write the RSN to AD and IV. The IV one is XORed for Chacha20 mode. In GCM mode, we assume the
		//octets are zeroes, so XOR is the same as store.
		for i in 0..8usize {
			let x = (self.2.wrapping_shr((!(i as u32) & 7) << 3)) as u8;
			_iv[i + 4] ^= x;
			_ad[i] = x;
		}
		//Encrypt the record. We know that buf.len() - range.start >= extlen, so that buf.len() >=
		//range.start + extlen, and the buffer is in bounds.
		self.0.encrypt(&_iv, &_ad, &mut buf[range.start..][..extlen], 16).map_err(|_|())?;
		//Copy hdrsize bytes to just before range.start.
		let hdroff = range.start - hdrsize;	//Positive since range.start >= hdrsize.
		let dend = range.start + extlen;	//Fits by being at most buf.len().
		//Copy the base header from AD.
		(&mut buf[hdroff..][..BASE_HEADER_LENGTH]).copy_from_slice(&_ad[8..]);
		//The length in header is not the same, so correct it.
		buf[hdroff + 3] = (plen >> 8) as u8;
		buf[hdroff + 4] = plen as u8;
		let gcmivoff = hdroff + BASE_HEADER_LENGTH;
		//Copy the used IV for AES-GCM.
		if self.3 { (&mut buf[gcmivoff..][..8]).copy_from_slice(&_iv[4..]); }
		//Increment the RSN, checking for overflows.
		self.2 = match self.2.checked_add(1) { Some(x) => x, None => {self.4 = true; self.2 } };
		//The range of data after protection.
		Ok(hdroff..dend)
	}
	fn protect_coutput(&mut self, out: &mut [u8], inp: &[u8], rtype: u8, max_write_size: usize) ->
		Result<(usize, usize), ()>
	{
		//The size of header is 13 bytes (8+5) in GCM mode, and 5 bytes in Chacha mode.
		let hdrsize = if self.3 { GCM_HEADER_LENGTH } else { CHACHA_HEADER_LENGTH };
		//We assume per-packet overhead is much below maximum usize.
		let overhead = hdrsize + TAG_SIZE_OVERHEAD;
		//If overhead does not fit into output, no data is taken.
		if out.len() < overhead { return Ok((0, 0)); }
		//out.len() >= overhead, so the difference is positive. Compute the maximum amount of plaintext
		//that fits (capping at MAX_RECORD_PTXT).
		let len = min(min(MAX_RECORD_PTXT, max_write_size), min(out.len() - overhead, inp.len()));
		//Copy from inp[..len] to out[hdrsize..][..len]. First is in-range, since len <= inp.len() and the
		//second is because hdrsize <= overhead and len + overhead <= out.len().
		(&mut out[hdrsize..][..len]).copy_from_slice(&inp[..len]);
		//The total size of the encrypted record. This in range because len <= out.len() - overhead, and
		//thus len + overhead <= out.len(), and the last on fits into usize. It also can slice out.
		let tsize = len + overhead;
		let srcdst = &mut out[..tsize];
		//Protect the record. As said above, len + hdrsize <= out.len() and thus fits in usize.
		let outrange = self.protect(srcdst, rtype, hdrsize..hdrsize + len, 0)?;
		Ok((outrange.end, len))
	}
	fn get_overhead(&self) -> usize
	{
		let hdrsize = if self.3 { GCM_HEADER_LENGTH } else { CHACHA_HEADER_LENGTH };
		hdrsize + TAG_SIZE_OVERHEAD
	}
}

impl Deprotector
{
	pub fn new_aes128(key: &[u8]) -> Result<Self, ()>
	{
		fail_if!(key.len() != 20, ());
		let (key_k, key_iv) = key.split_at(16);
		let mut iv = [0; 12];
		(&mut iv[..4]).copy_from_slice(key_iv);
		Ok(Deprotector(AES_128_GCM.new_decryptor(key_k)?, iv, 0, true, false))
	}
	pub fn new_aes256(key: &[u8]) -> Result<Self, ()>
	{
		fail_if!(key.len() != 36, ());
		let (key_k, key_iv) = key.split_at(32);
		let mut iv = [0; 12];
		(&mut iv[..4]).copy_from_slice(key_iv);
		Ok(Deprotector(AES_256_GCM.new_decryptor(key_k)?, iv, 0, true, false))
	}
	pub fn new_chacha20(key: &[u8]) -> Result<Self, ()>
	{
		fail_if!(key.len() != 44, ());
		let (key_k, key_iv) = key.split_at(32);
		let mut iv = [0; 12];
		iv.copy_from_slice(key_iv);
		Ok(Deprotector(CHACHA20_POLY1305.new_decryptor(key_k)?, iv, 0, false,
			false))
	}
}

impl RecordDeprotector for Deprotector
{
	fn deprotect(&mut self, buf: &mut [u8]) -> Result<(u8, Range<usize>), ()>
	{
		//The size of header is 13 bytes (8+5) in GCM mode, and 5 bytes in Chacha mode.
		let hdrsize = if self.3 { GCM_HEADER_LENGTH } else { CHACHA_HEADER_LENGTH };
		//The record must at minimum contain a header and a tag (16 bytes). We can assume that header length
		//and tag length combined are much below usize max.
		fail_if!(buf.len() < hdrsize + TAG_LENGTH, ());
		//If destroyed flag is set, fail.
		fail_if!(self.4, ());
		let mut _iv: [u8; 12] = self.1;
		//Check the length in header. buf.len() is guaranteed to be at least BASE_HEADER_LENGTH by the
		//check above.
		let recsize_check = buf.len() - BASE_HEADER_LENGTH;
		fail_if!(buf[3] != (recsize_check >> 8) as u8, ());
		fail_if!(buf[4] != recsize_check as u8, ());
		fail_if!(recsize_check > MAX_RECORD_CTXT, ());
		//The amount of plaintext payload in record.
		//Assemble the AD. The first eight bytes are the RSN (we don't copy it here), then comes the header,
		//except with the length being amount of plaintext. buf.len() is guaranteed to be at least hdrsize+
		//TAG_LENGTH by check above.
		let mut _ad: [u8; 13] = [0; 13];
		(&mut _ad[8..13]).copy_from_slice(&buf[..5]);
		let plaintext_size = buf.len() - hdrsize - TAG_LENGTH;
		_ad[11] = (plaintext_size >> 8) as u8;
		_ad[12] = plaintext_size as u8;
		fail_if!(plaintext_size > MAX_RECORD_PTXT, ());
		//Assemble the IV assuming we are Chacha mode. If this isn't true, then the wrong data will be
		//overwritten later. Also, fill the RSN filed in AD.
		for i in 0..8usize {
			let x = (self.2.wrapping_shr((!(i as u32) & 7) << 3)) as u8;
			_iv[i+4] ^= x;
			_ad[i] = x;
		}
		//If in GCM mode, overwrite the explicit IV part.
		if self.3 { (&mut _iv[4..]).copy_from_slice(&buf[BASE_HEADER_LENGTH..GCM_HEADER_LENGTH]); }
		//Finally try to decrypt the packet.
		let outlen = self.0.decrypt(&_iv, &_ad, &mut buf[hdrsize..]).map_err(|_|())?;
		//The plaintext data starts at hdrsize and runs for outlen bytes. Now, outlen is at most buf.len()-
		//hdrsize, so this can't overflow usize.
		let pktend = outlen + hdrsize;
		//By the check above, the record has at least one byte, and the first is the record type.
		let rtype = buf[0];
		//Increment the RSN, checking for overflow.
		self.2 = match self.2.checked_add(1) { Some(x) => x, None => {self.4 = true; self.2 } };
		Ok((rtype, hdrsize..pktend))
	}
}

impl Extractor
{
	pub fn new(master_key: &[u8], client_random: &[u8; 32], server_random: &[u8; 32], prf: HashFunction) ->
		Result<Self, TlsFailure>
	{
		sanity_check!(master_key.len() == MS_LEN, "Wrong key length for TLS 1.2 extractor ({}, expected {})",
			master_key.len(), MS_LEN);
		let mut tmp = Ok(Extractor{secret:[0;MS_LEN], randoms:[0;64], function:prf});
		if let &mut Ok(ref mut inner) = &mut tmp {
			inner.secret.copy_from_slice(master_key);
			(&mut inner.randoms[0..32]).copy_from_slice(client_random);
			(&mut inner.randoms[32..64]).copy_from_slice(server_random);
		}
		tmp
	}
}

impl Drop for Extractor
{
	fn drop(&mut self)
	{
		wipe_buffer(&mut self.secret);
	}
}

impl TlsExtractor for Extractor
{
	fn extract(&self, label: &str, context: Option<&[u8]>, buffer: &mut [u8]) -> Result<(), TlsFailure>
	{
		let func = self.function;
		if let Some(ctx) = context {
			let ctxlen = [(ctx.len() >> 8) as u8, ctx.len() as u8];
			func.tls_prf(&self.secret, label, |x| {
				x.input(&self.randoms);
				x.input(&ctxlen);
				x.input(ctx);
			}, buffer).map_err(|x|assert_failure!("Hash function internal error: {}", x))?;
		} else {
			func.tls_prf(&self.secret, label, |x| {
				x.input(&self.randoms);
			}, buffer).map_err(|x|assert_failure!("Hash function internal error: {}", x))?;
		}
		Ok(())
	}
}

#[test]
fn basic_protectors()
{
	let key = [1;44];
	let mut msg = [0;128];
	let msg2 = [0;42-5];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..58);
	assert_eq!(msg[0], 27);
	assert_eq!(msg[1], 3);
	assert_eq!(msg[2], 3);
	assert_eq!(msg[3], 0);
	assert_eq!(msg[4], 53);
	assert_eq!(d.deprotect(&mut msg[..58]).unwrap(), (27, 5..42));
	assert_eq!(&msg[5..42], &msg2[..]);
}

#[test]
fn basic_protectors2()
{
	let key = [1;20];
	let mut msg = [0;128];
	let msg2 = [0;42-13];
	let mut e = Protector::new_aes128(&key).unwrap();
	let mut d = Deprotector::new_aes128(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 13..42, 0).unwrap(), 0..58);
	assert_eq!(msg[0], 27);
	assert_eq!(msg[1], 3);
	assert_eq!(msg[2], 3);
	assert_eq!(msg[3], 0);
	assert_eq!(msg[4], 53);
	assert_eq!(&msg[5..13], &msg2[..8]);
	assert_eq!(d.deprotect(&mut msg[..58]).unwrap(), (27, 13..42));
	assert_eq!(&msg[13..42], &msg2[..]);
}

#[test]
fn basic_protectors3()
{
	let key = [1;36];
	let mut msg = [0;128];
	let msg2 = [0;42-13];
	let mut e = Protector::new_aes256(&key).unwrap();
	let mut d = Deprotector::new_aes256(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 13..42, 0).unwrap(), 0..58);
	assert_eq!(msg[0], 27);
	assert_eq!(msg[1], 3);
	assert_eq!(msg[2], 3);
	assert_eq!(msg[3], 0);
	assert_eq!(msg[4], 53);
	assert_eq!(&msg[5..13], &msg2[..8]);
	assert_eq!(d.deprotect(&mut msg[..58]).unwrap(), (27, 13..42));
	assert_eq!(&msg[13..42], &msg2[..]);
}

#[test]
fn gap_too_small()
{
	let key = [1;20];
	let mut msg = [0;128];
	let mut e = Protector::new_aes128(&key).unwrap();
	e.protect(&mut msg, 27, 12..42, 0).unwrap_err();
	e.protect(&mut msg, 27, 14..42, 0).unwrap();
	let key = [1;44];
	let mut e = Protector::new_chacha20(&key).unwrap();
	e.protect(&mut msg, 27, 4..42, 0).unwrap_err();
	e.protect(&mut msg, 27, 6..42, 0).unwrap();
}

#[test]
fn basic_protectors_desync()
{
	let key = [1;44];
	let mut msg = [0;128];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..58);
	for i in msg.iter_mut() { *i = 0; }
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..58);
	assert_eq!(msg[0], 27);
	assert_eq!(msg[1], 3);
	assert_eq!(msg[2], 3);
	assert_eq!(msg[3], 0);
	assert_eq!(msg[4], 53);
	d.deprotect(&mut msg[..58]).unwrap_err();
}

#[test]
fn basic_protectors_wrap_e()
{
	let key = [1;44];
	let mut msg = [0;128];
	let mut e = Protector::new_chacha20(&key).unwrap();
	e.2 = !0u64;
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..58);
	for i in msg.iter_mut() { *i = 0; }
	e.protect(&mut msg, 27, 5..42, 0).unwrap_err();
}


#[test]
fn basic_protectors_wrap_d()
{
	let key = [1;44];
	let mut msg = [0;128];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut e2 = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	e2.2 = !0u64;
	d.2 = !0u64;
	assert_eq!(e2.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..58);
	assert_eq!(d.deprotect(&mut msg[..58]).unwrap(), (27, 5..42));
	for i in msg.iter_mut() { *i = 0; }
	assert_eq!(e2.2, !0u64);
	assert_eq!(d.2, !0u64);
	e.2 = d.2;
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..58);
	d.deprotect(&mut msg[..58]).unwrap_err();
}

#[test]
fn test_aes_gcm_nonces()
{
	let key = [1;20];
	let mut msg = [0;128];
	let mut e = Protector::new_aes128(&key).unwrap();
	let mut d = Deprotector::new_aes128(&key).unwrap();
	e.2 = 0x01030507090B0D0F;
	d.2 = 0x01030507090B0D0F;
	for i in 4..12 { e.1[i] = 0xFF; }
	assert_eq!(e.protect(&mut msg, 27, 13..42, 0).unwrap(), 0..58);
	assert_eq!(&msg[5..13], &[0xFE, 0xFC, 0xFA, 0xF8, 0xF6, 0xF4, 0xF2, 0xF0][..]);
	assert_eq!(d.deprotect(&mut msg[..58]).unwrap(), (27, 13..42));
}

#[test]
fn test_aes_gcm_nonces2()
{
	let key = [1;20];
	let mut msg = [0;128];
	let mut e = Protector::new_aes128(&key).unwrap();
	let mut d = Deprotector::new_aes128(&key).unwrap();
	e.2 = 0x01030507090B0D0F;
	d.2 = 0x01030507090B0D0E;
	for i in 4..12 { e.1[i] = 0xFF; }
	assert_eq!(e.protect(&mut msg, 27, 13..42, 0).unwrap(), 0..58);
	d.deprotect(&mut msg[..58]).unwrap_err();
}

#[test]
fn test_aes_gcm_nonces3()
{
	let key = [1;20];
	let mut msg = [0;128];
	let mut e = Protector::new_aes128(&key).unwrap();
	let mut d = Deprotector::new_aes128(&key).unwrap();
	e.2 = 0x01030507090B0D0F;
	d.2 = 0x01030507090B0D0F;
	for i in 4..12 { e.1[i] = 0xFF; }
	assert_eq!(e.protect(&mut msg, 27, 13..42, 0).unwrap(), 0..58);
	msg[5] += 1;
	d.deprotect(&mut msg[..58]).unwrap_err();
}

#[test]
fn basic_protectors_multi()
{
	let key = [1;44];
	let mut msg = [0;128];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..58);
	assert_eq!(d.deprotect(&mut msg[..58]).unwrap(), (27, 5..42));
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..58);
	assert_eq!(d.deprotect(&mut msg[..58]).unwrap(), (27, 5..42));
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..58);
	assert_eq!(d.deprotect(&mut msg[..58]).unwrap(), (27, 5..42));
	assert_eq!(e.2, 3);
	assert_eq!(d.2, 3);
}

#[test]
fn basic_protectors_off()
{
	let key = [1;44];
	let mut msg = [0;128];
	let msg2 = [0;42-7];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 7..42, 0).unwrap(), 2..58);
	assert_eq!(msg[2], 27);
	assert_eq!(msg[3], 3);
	assert_eq!(msg[4], 3);
	assert_eq!(msg[5], 0);
	assert_eq!(msg[6], 51);
	assert_eq!(d.deprotect(&mut msg[2..58]).unwrap(), (27, 5..40));
	assert_eq!(&msg[7..42], &msg2[..]);
}

#[test]
fn basic_protectors_exact()
{
	let key = [1;44];
	let mut msg = [0;58];
	let msg2 = [0;42-5];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..58);
	assert_eq!(msg[0], 27);
	assert_eq!(msg[1], 3);
	assert_eq!(msg[2], 3);
	assert_eq!(msg[3], 0);
	assert_eq!(msg[4], 53);
	assert_eq!(d.deprotect(&mut msg[..58]).unwrap(), (27, 5..42));
	assert_eq!(&msg[5..42], &msg2[..]);
}

#[test]
fn basic_protectors_pad()
{
	let key = [1;44];
	let mut msg = [0;58];
	let msg2 = [0;42-5];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 5..42, 10000).unwrap(), 0..58);
	assert_eq!(msg[0], 27);
	assert_eq!(msg[1], 3);
	assert_eq!(msg[2], 3);
	assert_eq!(msg[3], 0);
	assert_eq!(msg[4], 53);
	assert_eq!(d.deprotect(&mut msg[..58]).unwrap(), (27, 5..42));
	assert_eq!(&msg[5..42], &msg2[..]);
}
