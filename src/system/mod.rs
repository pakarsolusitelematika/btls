//This module is internal.
#![allow(missing_docs)]
pub use super::std::fs::{File, read_dir};
pub use super::std::io::stderr;
pub use super::std::path::{Path, PathBuf};
pub use super::std::thread::{Builder, sleep};
pub use super::std::time::SystemTime;

#[cfg(unix)]
pub fn is_executable(x: &Path) -> bool
{
	use std::os::unix::fs::PermissionsExt;
	match x.metadata() {
		Ok(x) => x.permissions().mode() & 0x40 != 0,
		Err(_) => false
	}
}

#[cfg(unix)]
pub fn is_socket(x: &Path) -> bool
{
	use std::os::unix::fs::FileTypeExt;
	match x.metadata() {
		Ok(x) => x.file_type().is_socket(),
		Err(_) => false
	}
}

#[cfg(not(unix))]
pub fn is_executable(_x: &Path) -> bool
{
	false
}

#[cfg(not(unix))]
pub fn is_socket(_x: &Path) -> bool
{
	false
}
