#![allow(improper_ctypes)]
#![forbid(unsafe_code)]
extern crate btls;
use self::btls::test::stderr;
extern crate btls_aux_ecdsa_sign;
use btls_aux_ecdsa_sign::*;
extern crate btls_aux_ed25519_sign;
use btls_aux_ed25519_sign::*;
extern crate btls_aux_ed448_sign;
use btls_aux_ed448_sign::*;
extern crate btls_aux_keyconvert;
use btls_aux_keyconvert::{EcdsaCurve, EcdsaKeyDecode, Ed25519KeyDecode, Ed448KeyDecode, RsaKeyDecode};
extern crate btls_aux_rsa_sign;
use btls_aux_rsa_sign::*;
extern crate btls_aux_serialization;
use btls_aux_serialization::*;
extern crate btls_aux_signature_algo;
use btls_aux_signature_algo::SignatureKeyPair;

use std::env::args_os;
use std::fs::File as StdFile;
use std::io::{Write as IoWrite};
use std::path::Path as StdPath;
use std::process::exit;
use std::str::FromStr;

macro_rules! stderr
{
	($x:expr) => { let _ = writeln!(stderr(), $x); };
	($x:expr, $($a:expr),*) => { let _ = writeln!(stderr(), $x, $($a),*); };
}

macro_rules! abort
{
	($x:expr) => {{ stderr!($x); exit(1); }};
	($x:expr, $($a:expr),*) => {{ stderr!($x, $($a),*); exit(1); }};
}

fn write_integer_sexp_sz<S:Sink>(target: &mut S, val: &[u8]) -> Result<(), ()>
{
	let mut fidx = 0;
	//Strip possible leading zeros.
	while fidx + 1 < val.len() && val[fidx] == 0 { fidx += 1; }
	let val = &val[fidx..];
	write_integer_sexp(target, val)
}

fn write_integer_sexp<S:Sink>(target: &mut S, val: &[u8]) -> Result<(), ()>
{
	write_padded(target, val, val.len())
}

fn write_padded<S:Sink>(sink: &mut S, data: &[u8], datalen: usize) -> Result<(), ()>
{
	let prefix = format!("{}:", datalen);
	for i in prefix.as_bytes().iter() { sink.write_u8(*i).map_err(|_|())?; }
	for _ in data.len()..datalen { sink.write_u8(0).map_err(|_|())?; }
	if data.len() > datalen {
		sink.write_slice(&data[data.len()-datalen..]).map_err(|_|())?;
	} else {
		sink.write_slice(data).map_err(|_|())?;
	}
	Ok(())
}

fn write_rsa_key<S:Sink>(sink: &mut S, n: &[u8], e: &[u8], d: &[u8], p: &[u8], q: &[u8], dp: &[u8], dq: &[u8],
	qi: &[u8]) -> Result<(), ()>
{
	sink.write_u8(40).map_err(|_|())?;
	write_integer_sexp_sz(sink, "rsa".as_bytes())?;	//Not actually integer but close enough...
	write_integer_sexp_sz(sink, n)?;
	write_integer_sexp_sz(sink, e)?;
	write_integer_sexp_sz(sink, d)?;
	write_integer_sexp_sz(sink, p)?;
	write_integer_sexp_sz(sink, q)?;
	write_integer_sexp_sz(sink, dp)?;
	write_integer_sexp_sz(sink, dq)?;
	write_integer_sexp_sz(sink, qi)?;
	sink.write_u8(41).map_err(|_|())?;
	Ok(())
}

fn write_ecdsa_key<S:Sink>(sink: &mut S, crv: &str, d: &[u8], x: &[u8], y: &[u8], bytes: usize) -> Result<(), ()>
{
	sink.write_u8(40).map_err(|_|())?;
	write_integer_sexp(sink, "ecdsa".as_bytes())?;	//Not actually integer but close enough...
	write_integer_sexp(sink, crv.as_bytes())?;	//Not actually integer but close enough...
	write_padded(sink, d, bytes)?;
	write_padded(sink, x, bytes)?;
	write_padded(sink, y, bytes)?;
	sink.write_u8(41).map_err(|_|())?;
	Ok(())
}

fn write_eddsa_key<S:Sink>(sink: &mut S, sub: &str, privkey: &[u8], pubkey: &[u8]) -> Result<(), ()>
{
	sink.write_u8(40).map_err(|_|())?;
	write_integer_sexp(sink, sub.as_bytes())?;	//Not actually integer but close enough...
	write_integer_sexp(sink, privkey)?;		//Not actually integer but close enough...
	write_integer_sexp(sink, pubkey)?;		//Not actually integer but close enough...
	sink.write_u8(41).map_err(|_|())?;
	Ok(())
}

fn generate_rsa_key(keysize: usize) -> Result<Vec<u8>, String>
{
	let mut key = RsaKeyPair::keypair_generate(keysize).map_err(|_|format!("Error generating private key"))?;
	let mut out = Vec::new();
	loop {
		{
			let p = RsaKeyDecode::new(&key).map_err(|_|format!("Error reading private key \
				representation"))?;

			//Nettle seems to generate p and q not caring about if p<q, which OpenSSL doesn't like.
			//So rethrow key if p<q.
			let mut maybe_lt = true;
			let mut lt = false;
			for i in p.p.iter().zip(p.q.iter()) {
				lt = lt || (maybe_lt && (i.0 < i.1));
				maybe_lt = maybe_lt && (i.0 == i.1);
			}
			if !lt {
				if write_rsa_key(&mut out, p.n, p.e, p.d, p.p, p.q, p.dp, p.dq, p.qi).is_err() {
					return Err(format!("Error serializing key"));
				}
				return Ok(out)
			}
		};
		key = RsaKeyPair::keypair_generate(keysize).map_err(|_|format!("Error generating private key"))?;
	}
}



fn generate_ecdsa_key(crvid: EcdsaCurve) -> Result<Vec<u8>, String>
{
	let key = EcdsaKeyPair::keypair_generate(crvid).map_err(|_|format!("Error generating private key"))?;
	let p = EcdsaKeyDecode::new(&key).map_err(|_|format!("Generated bad keypair representation"))?;
	let crvname = p.crv.get_sexp_curve_name();
	let len = p.crv.get_component_bytes();
	let mut out = Vec::new();
	write_ecdsa_key(&mut out, crvname, p.d, p.x, p.y, len).map_err(|_|format!("Error serializing key"))?;
	Ok(out)
}

fn generate_eddsa_key(subtype: &str) -> Result<Vec<u8>, String>
{
	let mut out = Vec::new();
	let error = if subtype == "ed25519" {
		let keypair = Ed25519KeyPair::keypair_generate(()).map_err(|_|format!("Error generating key"))?;
		let parse = Ed25519KeyDecode::new(&keypair).map_err(|_|format!("Error parsing generated key"))?;
		write_eddsa_key(&mut out, subtype, &parse.private, &parse.public)
	} else if subtype == "ed448" {
		let keypair = Ed448KeyPair::keypair_generate(()).map_err(|_|format!("Error generating key"))?;
		let parse = Ed448KeyDecode::new(&keypair).map_err(|_|format!("Error parsing generated key"))?;
		write_eddsa_key(&mut out, subtype, &parse.private, &parse.public)
	} else {
		return Err(format!("Unknown subtype for EdDSA"));
	};
	error.map_err(|_|format!("Error serializing key"))?;
	Ok(out)
}

fn main()
{
	let mut outputf = None;
	let mut algorithm = None;
	for i in args_os().enumerate() {
		if i.0 == 0 { continue; }	//Skip program name.
		if outputf.is_none() {
			outputf = Some(i.1);
		} else if algorithm.is_none() {
			let j = match i.1.into_string() {
				Ok(x) => x,
				Err(x) => {
					println!("Bad argument `{:?}`, skipping", x);
					continue;
				}
			};
			algorithm = Some(j);
		}
	}
	if algorithm.is_none() {
		stderr!("Syntax: mkkey <output> <algorithm>");
		stderr!("");
		stderr!("<algorithm> can be:");
		stderr!("\trsa:      2048-bit RSA");
		stderr!("\trsa<n>:   <n>-bit RSA (2048-4096 in multiples of 8)");
		stderr!("\trsa2048:  2048-bit RSA");
		stderr!("\trsa3072:  3072-bit RSA");
		stderr!("\trsa4096:  4096-bit RSA");
		stderr!("\tecdsa:    256-bit ECDSA");
		stderr!("\tecdsa256: 256-bit ECDSA");
		stderr!("\tecdsa384: 384-bit ECDSA");
		stderr!("\tecdsa521: 521-bit ECDSA");
		stderr!("\ted25519:  Ed25519");
		stderr!("\ted448:    Ed448");
		abort!("Error: missing paramteters");
	}

	let algorithm = algorithm.clone().unwrap();

	let (crv, rsa_size) = if algorithm == "rsa" {
		(Some(0xFFFFFFFDu32), 2048)		//Special.
	} else if algorithm.starts_with("rsa") {
		let size = match u32::from_str(&algorithm[3..]) {
			Ok(x) => x,
			Err(_) => abort!("Error: Invalid rsa key size '{}'", &algorithm[3..])
		};
		if size < 2048 || size > 4096 || size % 8 != 0 {
			abort!("Error: Unsupported RSA key size '{}'", size);
		}
		(Some(0xFFFFFFFDu32), size)		//Special.
	} else if algorithm == "ecdsa" || algorithm == "ecdsa256" {
		(Some(0x80000002u32), 0)
	} else if algorithm == "ecdsa384" {
		(Some(0x80000003u32), 0)
	} else if algorithm == "ecdsa521" {
		(Some(0x80000004u32), 0)
	} else if algorithm == "ed25519" {
		(Some(0xFFFFFFFEu32), 0)	//Special.
	} else if algorithm == "ed448" {
		(Some(0xFFFFFFFFu32), 0)	//Special.
	} else {
		abort!("Error: Unknown algorithm '{}'", algorithm);
	};

	let out = if let Some(0xFFFFFFFDu32) = crv {
		generate_rsa_key(rsa_size as usize)
	} else if let Some(0xFFFFFFFEu32) = crv {
		generate_eddsa_key("ed25519")
	} else if let Some(0xFFFFFFFFu32) = crv {
		generate_eddsa_key("ed448")
	} else if let Some(crvx) = crv {
		let crvobj = match EcdsaCurve::by_id(crvx - 0x80000002u32) {
			Some(x) => x,
			None => abort!("Error: Unknown ECDSA curve")
		};
		generate_ecdsa_key(crvobj)
	} else {
		abort!("Error: Not RSA, ECDSA nor EdDSA key?");
	};
	let out = match out {
		Ok(x) => x,
		Err(x) => abort!("Error: {}", x)
	};
	let fname = outputf.clone().unwrap();
	let fname = StdPath::new(&fname);
	let mut fp = match StdFile::create(fname) {
		Ok(x) => x,
		Err(x) => abort!("Error creating '{}': {}", fname.display(), x)
	};
	match fp.write_all(&out) {
		Ok(_) => (),
		Err(x) => abort!("Error writing '{}': {}", fname.display(), x)
	};
}
