extern crate btls;
use btls::logging::StderrLog;
use btls::server_certificates::LocalServerCertificateStore;
use btls::server_keys::LocalKeyPair;

use std::env::args_os;
use std::io::{Write as IoWrite, stderr};
use std::ops::Deref;
use std::process::exit;

#[path="../bin-common/server.rs"]
mod common;
#[path="../bin-common/transport.rs"]
mod transport;
use transport::{TransportListener,TransportStream};

fn main()
{
	if false { dummy(); }		//Silence some warnings.
	let mut target = None;
	let mut key = None;
	let mut dummy = false;
	let mut files = Vec::new();
	for i in args_os() {
		if !dummy {
			dummy = true;
		} else if target.is_none() {
			target = Some(match i.clone().into_string() {
				Ok(x) => x,
				Err(_) => panic!("Invalid target `{:?}`", i)
			});
		} else if key.is_none() {
			key = Some(i);
		} else {
			let file = i.clone();
			files.push(file);
		}
	}
	if files.len() == 0 {
		panic!("Need target, key and some certificate files");
	}
	let mut store = LocalServerCertificateStore::new(StderrLog);
	let _key = key.clone().unwrap().to_string_lossy().into_owned();
	let keyfile = key.unwrap();
	let privkey = match LocalKeyPair::new_file(&keyfile) {
		Ok(x) => x,
		Err(x) => {
			writeln!(stderr(), "{}", x).unwrap();
			exit(1);
		}
	};
	store.add_privkey(privkey, &_key).unwrap();
	for i in files {
		match store.add_file(&i) {
			Ok(_) => (),
			Err(x) => writeln!(stderr(), "{}", x).unwrap()
		}
	}

	let acceptsock = match TransportListener::bind(target.clone().unwrap().deref()) {
		Ok(x) => x,
		Err(x) => panic!("Can't bind to `{:?}`: {}", target.unwrap(), x)
	};
	common::do_it(acceptsock, store);
}

fn dummy()
{
	let _ = TransportStream::connect("");
}
