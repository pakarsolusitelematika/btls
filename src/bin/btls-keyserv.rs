#[cfg(unix)]
mod internal
{
extern crate btls;
extern crate btls_aux_dhf;
extern crate btls_aux_hash;
extern crate btls_aux_keypair;
use self::btls::server_keys::LocalKeyPair;
use self::btls::test::slice_eq_ct;
use self::btls_aux_dhf::Dhf;
use self::btls_aux_hash::HashFunction;
use self::btls_aux_keypair::KeyPair;

use std::env::args_os;
use std::fs::remove_file;
use std::fs::File;
use std::io::{Read as IoRead, Result as IoResult, stderr, Write as IoWrite};
use std::net::Shutdown;
use std::os::unix::fs::FileTypeExt;
use std::os::unix::net::{UnixListener,UnixStream};
use std::net::{TcpListener,TcpStream};
use std::path::Path;
use std::sync::Arc;
use std::collections::BTreeMap;
use std::process::exit;
use std::thread::Builder;
use std::ffi::OsStr;

trait Acceptor: Sized+Send
{
	type Connection: Socket+Send+'static;
	fn take_connection(&mut self) -> IoResult<<Self as Acceptor>::Connection>;
}


impl Acceptor for UnixListener
{
	type Connection = UnixStream;
	fn take_connection(&mut self) -> IoResult<<Self as Acceptor>::Connection>
	{
		self.accept().map(|(x,_)|x)
	}
}

impl Acceptor for TcpListener
{
	type Connection = TcpStream;
	fn take_connection(&mut self) -> IoResult<<Self as Acceptor>::Connection>
	{
		self.accept().map(|(x,_)|x)
	}
}


trait Socket: IoRead+IoWrite+Sized
{
	fn dispose(&mut self) -> IoResult<()>;
}

impl Socket for UnixStream
{
	fn dispose(&mut self) -> IoResult<()>
	{
		self.shutdown(Shutdown::Both)
	}
}

impl Socket for TcpStream
{
	fn dispose(&mut self) -> IoResult<()>
	{
		self.shutdown(Shutdown::Both)
	}
}

fn serve_thread<SType:Socket>(keypair: &LocalKeyPair, mut sock: SType, allowed: Arc<BTreeMap<[u8;32], ()>>,
	allow_any: bool)
{
	let mut auth_in_use = false;
	let mut auth_id = [0u8; 32];
	let mut auth_key = [0u8; 32];
	let mut cmdbyte = [0];
	match sock.read_exact(&mut cmdbyte) {
		Ok(_) => (),
		Err(x) => {
			writeln!(stderr(), "Can't read input (command): {}", x).unwrap();
			return;
		}
	};
	if cmdbyte[0] == 2 {
		//Authenticate.
		match sock.read_exact(&mut auth_id) {
			Ok(_) => (),
			Err(x) => {
				writeln!(stderr(), "Can't read input (identity): {}", x).unwrap();
				return;
			}
		};
		if allowed.get(&auth_id).is_none() {
			//ID not found in ACL.
			writeln!(stderr(), "Unauthorized signature request").unwrap();
			return;
		}
		let mut auth = match Dhf::X25519.generate_key() {
			Ok(x) => x,
			Err(_) => {
				writeln!(stderr(), "Can't generate auth challenge").unwrap();
				return;
			}
		};
		match auth.agree(&auth_id) {
			Ok(x) => auth_key.copy_from_slice(x.as_ref()),
			Err(_) => {
				writeln!(stderr(), "Peer sent invalid public key").unwrap();
				return;
			}
		}
		auth_in_use = true;
		let tmp = auth.pubkey().unwrap();
		let tmp = tmp.as_ref();
		match sock.write_all(tmp) {
			Ok(_) => (),
			Err(x) => {
				writeln!(stderr(), "Can't write auth response: {}", x).unwrap();
				return;
			}
		};
		//Reread command byte after authentication.
		match sock.read_exact(&mut cmdbyte) {
			Ok(_) => (),
			Err(x) => {
				writeln!(stderr(), "Can't read input (new command): {}", x).unwrap();
				return;
			}
		};
	}
	if cmdbyte[0] == 0 {
		//Capabilities and Public Key.
		let schemes = keypair.get_schemes();
		let spki = keypair.get_public_key().0.clone();
		let mut hdr = Vec::new();
		hdr.push((schemes.len() >> 8) as u8);
		hdr.push(schemes.len() as u8);
		for i in schemes.iter() {
			hdr.push((*i >> 8) as u8);
			hdr.push(*i as u8);
		}
		hdr.push((spki.len() >> 8) as u8);
		hdr.push(spki.len() as u8);
		match sock.write_all(&hdr) {
			Ok(_) => (),
			Err(x) => {
				writeln!(stderr(), "Can't write caps&spki: {}", x).unwrap();
				return;
			}
		};
		match sock.write_all(&spki) {
			Ok(_) => (),
			Err(x) => {
				writeln!(stderr(), "Can't write caps&spki: {}", x).unwrap();
				return;
			}
		};
	} else if cmdbyte[0] == 1 {
		//Sign.
		//Read header.
		let mut hdr = [0; 4];
		match sock.read_exact(&mut hdr) {
			Ok(_) => (),
			Err(x) => {
				writeln!(stderr(), "Can't read input (sign header)): {}", x).unwrap();
				return;
			}
		};
		let algo = (hdr[0] as u16) << 8 | (hdr[1] as u16);
		let schemes = keypair.get_schemes();
		let mut found = false;
		for i in schemes.iter() { found |= *i == algo; }
		if !found {
			writeln!(stderr(), "Got invalid algorithm: {:x}", algo).unwrap();
			return;
		}
		//Read payload.
		let len = (hdr[2] as u16) << 8 | (hdr[3] as u16);
		let mut buf = vec![0;len as usize];
		match sock.read_exact(&mut buf) {
			Ok(_) => (),
			Err(x) => {
				writeln!(stderr(), "Can't read input (sign payload): {}", x).unwrap();
				return;
			}
		};
		//Read mac.
		let mut mac = [0u8; 32];
		if auth_in_use {
			match sock.read_exact(&mut mac) {
				Ok(_) => (),
				Err(x) => {
					writeln!(stderr(), "Can't read input (MAC): {}", x).unwrap();
					return;
				}
			};
			let h = match HashFunction::Sha256.hmac(&auth_key, |input|{
				input.input(&[0x01]);
				input.input(&hdr);
				input.input(&buf);
			}) {
				Ok(x) => x,
				Err(x) => {
					writeln!(stderr(), "Internal error in SHA-256: {}", x).unwrap();
					return;
				}
			};
			if !slice_eq_ct(&mac, h.as_ref()) {
				writeln!(stderr(), "Bad request MAC").unwrap();
				return;
			}
		} else if !allow_any {
			writeln!(stderr(), "Unauthorized signature request").unwrap();
			return;
		}
		let sig = {
			let receiver = keypair.sign(&buf, algo);
			let sig = match receiver.read() {
				Ok(Ok(x)) => x,
				Ok(Err(_)) => {
					writeln!(stderr(), "Error signing input").unwrap();
					return;
				}
				Err(_) => {
					writeln!(stderr(), "Error: Can't wait on local signature").unwrap();
					return;
				}
			};
			writeln!(stderr(), "Signed a message").unwrap();
			sig
		};
		let sighdr = [(sig.len() >> 8) as u8, sig.len() as u8];
		match sock.write_all(&sighdr) {
			Ok(_) => (),
			Err(x) => {
				writeln!(stderr(), "Can't write signature: {}", x).unwrap();
				return;
			}
		};
		match sock.write_all(&sig) {
			Ok(_) => (),
			Err(x) => {
				writeln!(stderr(), "Can't write signature: {}", x).unwrap();
				return;
			}
		};
	}
	sock.dispose().unwrap();
}


pub fn main()
{
	let mut socket = None;
	let mut key = None;
	let mut dummy = false;
	for i in args_os() {
		if !dummy {
			dummy = true;
		} else if socket.is_none() {
			socket = Some(i);
		} else if key.is_none() {
			key = Some(i);
		}
	}
	if key.is_none() {
		writeln!(stderr(), "Syntax: btls-keyserv <socket> <privatekey>").unwrap();
		exit(1);
	}
	let mut acl = key.clone().unwrap();
	acl.push(".acl");
	let key = key.clone().unwrap();

	let keypair = load_key(&key);
	let (allow_any, allowed) = load_acls(&acl);
	let socket = socket.clone().unwrap();

	if is_tcp_socket(&socket) {
		let acceptsock = make_tcp(&socket);
		serve_loop(acceptsock, keypair, allow_any, allowed);
	} else {
		let acceptsock = make_unix(&socket);
		serve_loop(acceptsock, keypair, allow_any, allowed);
	}
}

fn is_tcp_socket(socket: &OsStr) -> bool
{
	//If path fails conversion to UTF-8, it definitely isn't TCP socket.
	if let Some(socket) = socket.to_str() {
		//The name must contain exactly one ':' and must not contain any '/'s.
		if socket.find('/').is_some() { return false; }
		if socket.split(':').count() != 2 {
			//This might be IPv6 address literial.
			let beginning = match socket.rsplitn(2, ':').nth(1) {
				Some(x) => x,
				None => return false	//Definitely not valid.
			};
			//Check that beginning is enclosed in '[]'.
			if !beginning.starts_with("[") { return false; }
			if !beginning.ends_with("]") { return false; }
			return true;
		}
		true
	} else {
		false
	}
}

fn make_tcp(socket: &OsStr) -> TcpListener
{
	let socket = socket.to_str().unwrap();
	let acceptsock = match TcpListener::bind(socket) {
		Ok(x) => x,
		Err(x) => {
			writeln!(stderr(), "Can't bind to `{}`: {}", socket, x).unwrap();
			exit(1);
		}
	};
	acceptsock
}

fn load_key(key: &OsStr) -> LocalKeyPair
{
	let key = Path::new(&key);
	let keypair = match LocalKeyPair::new_file(key) {
		Ok(x) => x,
		Err(x) => {
			writeln!(stderr(), "Can't load private key '{}': {}", key.display(), x).unwrap();
			exit(1);
		}
	};
	keypair
}

fn load_acls(acl: &OsStr) -> (bool, BTreeMap<[u8;32],()>)
{
	let mut allowed = BTreeMap::<[u8;32],()>::new();
	let mut allow_any = false;
	let acl = Path::new(acl);
	let mut aclfile = match File::open(acl) {
		Ok(x) => x,
		Err(x) => {
			writeln!(stderr(), "Can't load ACL file '{}': {}", acl.display(), x).unwrap();
			exit(1);
		}
	};
	let mut acldata = String::new();
	match aclfile.read_to_string(&mut acldata) {
		Ok(_) => (),
		Err(x) => {
			writeln!(stderr(), "Can't read ACL file '{}': {}", acl.display(), x).unwrap();
			exit(1);
		}
	};
	for i in acldata.split('\n') {
		if i == "any" { allow_any = true; }
		if i.len() != 64 { continue; }
		let mut ent = [0u8;32];
		for j in i.chars().enumerate() {
			let cval = j.1 as u32;
			let hval = match cval {
				x@48...57 => x - 48,
				x@65...70 => x - 55,
				x@97...112 => x - 87,
				_ => continue
			} as u8;
			ent[j.0>>1] |= hval << (4 - 4 * (j.0 % 2));
		}
		allowed.insert(ent, ());
	}
	(allow_any, allowed)
}

fn make_unix(socket: &OsStr) -> UnixListener
{
	let socket = Path::new(socket);
	//If the socket already exists and is a socket, delete it. Otherwise if it already exists, error.
	match socket.metadata() {
		Ok(mdata) => {
			let ftype = mdata.file_type();
			if !socket.exists() {
				//Do nothing.
			} else if ftype.is_socket() {
				match remove_file(socket) {
					Ok(_) => (),
					Err(x) => {
						writeln!(stderr(), "Failed to delete existing sokcet '{}': {}",
							socket.display(), x).unwrap();
						exit(1);
					}
				};
			} else {
				writeln!(stderr(), "Socket '{}' already exists and is not asocket",
					socket.display()).unwrap();
				exit(1);
			}
		},
		Err(_) => (),	//Assume does not exist.
	};
	let acceptsock = match UnixListener::bind(socket) {
		Ok(x) => x,
		Err(x) => {
			writeln!(stderr(), "Can't bind to `{}`: {}", socket.display(), x).unwrap();
			exit(1);
		}
	};
	acceptsock
}

fn serve_loop<AType:Acceptor>(mut acceptsock: AType, keypair: LocalKeyPair, allow_any: bool,
	allowed: BTreeMap<[u8;32],()>)
{
	let allowed = Arc::new(allowed);
	writeln!(stderr(), "Waiting for connections...").unwrap();
	loop {
		let sock = match acceptsock.take_connection() {
			Ok(x) => x,
			Err(x) => {
				writeln!(stderr(), "Can't accept connection: {}", x).unwrap();
				continue;
			}
		};
		let b = Builder::new();
		let b = b.name("socket signature".to_owned());
		let key2 = keypair.clone();
		let allowed2 = allowed.clone();
		match b.spawn(move || {
			serve_thread(&key2, sock, allowed2, allow_any);
		}) {
			Ok(_) => (),
			Err(x) => writeln!(stderr(), "Can't spawn thread: {}", x).unwrap()
		}
	}
}

}


#[cfg(not(unix))]
mod internal
{
	use std::io::{stderr, Write as IoWrite};
	pub fn main()
	{
		writeln!(stderr(), "Error: btls-keyserv is only available on Unix.").unwrap()
	}
}

fn main()
{
	internal::main()
}
