extern crate btls;
use btls::logging::StderrLog;
use btls::server_certificates::CertificateDirectory;

use std::env::args_os;
use std::ops::Deref;

#[path="../bin-common/server.rs"]
mod common;
#[path="../bin-common/transport.rs"]
mod transport;
use transport::{TransportListener,TransportStream};

fn main()
{
	if false { dummy(); }		//Silence some warnings.
	let mut target = None;
	let mut cdir = None;
	let mut dummy = false;
	for i in args_os() {
		if !dummy {
			dummy = true;
		} else if target.is_none() {
			target = Some(match i.clone().into_string() {
				Ok(x) => x,
				Err(_) => panic!("Invalid target `{:?}`", i)
			});
		} else if cdir.is_none() {
			cdir = Some(i);
		} else {
			panic!("Expected only two arguments");
		}
	}
	if cdir.is_none() {
		panic!("Need target and certificate directory");
	}
	let store = match CertificateDirectory::new(&cdir.unwrap(), StderrLog) {
		Ok(x) => x,
		Err(x) => panic!("Can't create ceritificate store: {}", x)
	};
	let acceptsock = match TransportListener::bind(target.clone().unwrap().deref()) {
		Ok(x) => x,
		Err(x) => panic!("Can't bind to `{:?}`: {}", target.unwrap(), x)
	};
	common::do_it(acceptsock, store);
}

fn dummy()
{
	let _ = TransportStream::connect("");
}
