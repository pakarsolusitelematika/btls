extern crate btls;
use btls::server_keys::LocalKeyPair;
extern crate btls_aux_hash;
use btls_aux_hash::HashFunction;
extern crate btls_aux_keypair;
use btls_aux_keypair::KeyPair;
use std::char::from_u32;
use std::env::args_os;
use std::process::exit;
use std::path::Path;
use std::ops::Deref;
use std::convert::AsRef;
use std::io::{stderr, Write};

fn hexchar(val: u8) -> char
{
	from_u32((val + if val < 10 { 48 } else { 87 }) as u32).unwrap()
}

fn as_hex(data: &[u8]) -> String
{
	let mut ret = String::new();
	for i in data.iter() {
		ret.push(hexchar(*i >> 4));
		ret.push(hexchar(*i & 15));
	}
	ret
}

fn base64char(val: u8) -> char
{
	from_u32((val.wrapping_add(match val {
		0...25 => 65,
		26...51 => 71,
		52...61 => 252,
		62 => 237,
		63 => 240,
		_ => unreachable!(),
	})) as u32).unwrap()
}

fn as_base64(data: &[u8]) -> String
{
	let mut ret = String::new();
	for i in 0..data.len() / 3 {
		let a = data[3*i+0];
		let b = data[3*i+1];
		let c = data[3*i+2];
		ret.push(base64char(a >> 2));
		ret.push(base64char(((a << 4) | (b >> 4)) & 63));
		ret.push(base64char(((b << 2) | (c >> 6)) & 63));
		ret.push(base64char(c & 63));
	}
	if data.len() % 3 == 1 {
		let a = data[data.len()-1];
		ret.push(base64char(a >> 2));
		ret.push(base64char((a << 4) & 63));
		ret.push('=');
		ret.push('=');
	}
	if data.len() % 3 == 2 {
		let a = data[data.len()-2];
		let b = data[data.len()-1];
		ret.push(base64char(a >> 2));
		ret.push(base64char(((a << 4) | (b >> 4)) & 63));
		ret.push(base64char((b << 2) & 63));
		ret.push('=');
	}
	ret
}

pub fn main()
{
	let mut key = None;
	let mut dummy = false;
	for i in args_os() {
		if !dummy {
			dummy = true;
		} else if key.is_none() {
			key = Some(i);
		}
	}
	if key.is_none() {
		writeln!(stderr(), "Syntax: btls-keyinfo <privatekey>").unwrap();
		exit(1);
	}
	let key = key.unwrap();
	let key = Path::new(&key);
	let keypair = match LocalKeyPair::new_file(key) {
		Ok(x) => x,
		Err(x) => {
			writeln!(stderr(), "Can't load private key '{}': {}", key.display(), x).unwrap();
			exit(1);
		}
	};
	let rawkey = keypair.get_public_key();
	let schemes = keypair.get_schemes();
	let mut supported_schemes = String::new();
	for i in schemes.iter() {
		let name = match *i {
			0x401 => "RSA-PKCS1-SHA256".to_owned(),
			0x501 => "RSA-PKCS1-SHA384".to_owned(),
			0x601 => "RSA-PKCS1-SHA512".to_owned(),
			0x403 => "ECDSA-SHA256".to_owned(),
			0x503 => "ECDSA-SHA384".to_owned(),
			0x603 => "ECDSA-SHA512".to_owned(),
			0x804 => "RSA-PSS-SHA256".to_owned(),
			0x805 => "RSA-PSS-SHA384".to_owned(),
			0x806 => "RSA-PSS-SHA512".to_owned(),
			0x807 => "Ed25519".to_owned(),
			0x808 => "Ed448".to_owned(),
			x => format!("UNKONWN_{:04x}", x)
		};
		if supported_schemes.len() > 0 { supported_schemes.push(' '); }
		supported_schemes.extend(name.chars());
	}
	let pkhash = HashFunction::Sha256.oneshot(|x|x.input(rawkey.0.deref())).unwrap();
	println!("File:     {}", key.display());
	println!("Type:     {}", keypair.get_key_type());
	println!("Supports: {}", supported_schemes);
	println!("SHA-256:  {}", as_hex(pkhash.as_ref()));
	println!("pin:      pin-sha256=\"{}\"", as_base64(pkhash.as_ref()));
}
