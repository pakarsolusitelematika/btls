extern crate btls;
use btls::client_certificates::{HostSpecificPin, TrustAnchor};

use std::env::args_os;
use std::ops::Deref;

#[path="../bin-common/client.rs"]
mod common;
#[path="../bin-common/transport.rs"]
mod transport;
use transport::{TransportListener,TransportStream};

fn main()
{
	if false { dummy(); }	//Silence some warnings.
	let mut target = None;
	let mut dummy = false;
	let mut trust_anchors = Vec::new();
	let mut pins = Vec::new();
	for i in args_os() {
		if !dummy {
			dummy = true;
		} else if target.is_none() {
			target = Some(match i.clone().into_string() {
				Ok(x) => x,
				Err(_) => panic!("Invalid target `{:?}`", i)
			});
		} else {
			let file = i.clone();
			if let Some(ref x) = i.to_str().and_then(|x|{
				if x.starts_with("hash:") { Some(&x[5..]) } else { None }}) {
				let mut hash = [0; 32];
				let mut valid = false;
				for i in x.chars().enumerate() {
					let ch = i.1 as u32;
					let shift = 4 ^ ((i.0 & 1) << 2) as u32;
					if let Some(x) = hash.get_mut(i.0>>1) {
						if ch >= 0x30 && ch <= 0x39 {
							*x |= (ch.saturating_sub(0x30)).wrapping_shl(shift) as u8;
						} else if ch >= 0x41 && ch <= 0x46 {
							*x |= (ch.saturating_sub(0x41-10)).wrapping_shl(shift) as u8;
						} else if ch >= 0x61 && ch <= 0x66 {
							*x |= (ch.saturating_sub(0x61-10)).wrapping_shl(shift) as u8;
						} else {
							break;	//Not valid.
						}
					} else {
						valid = false;
						break;
					}
					valid |= i.0 == 63;	//Completed.
				}
				if !valid { panic!("Bad keyhash (expected 64 hex digits)."); }
				pins.push(HostSpecificPin::trust_server_key_by_sha256(&hash, false, false));
			} else {
				let ta = match TrustAnchor::from_certificate_file(file) {
					Ok(x) => x,
					Err(x) => panic!("Can't load TA `{:?}`: {}", i, x)
				};
				trust_anchors.push(ta);
			}
		}
	}
	let (clientsock, reference) = match TransportStream::connect(target.clone().unwrap().deref()) {
		Ok((x, y)) => (x, y),
		Err(x) => panic!("Can't connect to `{:?}`: {}", target.unwrap(), x)
	};
	common::do_it(clientsock, Some(reference), trust_anchors, pins);
}

fn dummy()
{
	let x = TransportListener::bind("").unwrap();
	let _ = x.accept();
}
