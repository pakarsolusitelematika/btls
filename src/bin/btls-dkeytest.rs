extern crate btls;
extern crate btls_aux_hash;
use btls_aux_hash::HashFunction;
use btls::logging::StderrLog;
use btls::server_certificates::{CertificateDirectory, KeyStoreOperations};
use std::char::from_u32;
use std::env::args_os;
use std::process::exit;
use std::path::Path;
use std::ops::Deref;
use std::convert::AsRef;
use std::io::{stderr, Write};

fn base64char(val: u8) -> char
{
	from_u32((val.wrapping_add(match val {
		0...25 => 65,
		26...51 => 71,
		52...61 => 252,
		62 => 237,
		63 => 240,
		_ => unreachable!(),
	})) as u32).unwrap()
}

fn as_base64(data: &[u8]) -> String
{
	let mut ret = String::new();
	for i in 0..data.len() / 3 {
		let a = data[3*i+0];
		let b = data[3*i+1];
		let c = data[3*i+2];
		ret.push(base64char(a >> 2));
		ret.push(base64char(((a << 4) | (b >> 4)) & 63));
		ret.push(base64char(((b << 2) | (c >> 6)) & 63));
		ret.push(base64char(c & 63));
	}
	if data.len() % 3 == 1 {
		let a = data[data.len()-1];
		ret.push(base64char(a >> 2));
		ret.push(base64char((a << 4) & 63));
		ret.push('=');
		ret.push('=');
	}
	if data.len() % 3 == 2 {
		let a = data[data.len()-2];
		let b = data[data.len()-1];
		ret.push(base64char(a >> 2));
		ret.push(base64char(((a << 4) | (b >> 4)) & 63));
		ret.push(base64char((b << 2) & 63));
		ret.push('=');
	}
	ret
}

pub fn main()
{
	let mut directory = None;
	let mut dummy = false;
	for i in args_os() {
		if !dummy {
			dummy = true;
		} else if directory.is_none() {
			directory = Some(i);
		}
	}
	if directory.is_none() {
		writeln!(stderr(), "Syntax: btls-keyinfo <directory>").unwrap();
		exit(1);
	}
	let directory = directory.unwrap();
	let directory = Path::new(&directory);
	let keydir = match CertificateDirectory::new(directory, StderrLog) {
		Ok(x) => x,
		Err(x) => {
			writeln!(stderr(), "Can't load directory '{}': {}", directory.display(), x).unwrap();
			exit(1);
		}
	};
	let keys = keydir.enumerate_private_keys();
	for i in keys.iter() {
		println!("{} ({}):", i.0, i.1);
		match keydir.get_public_key(&i.0) {
			Some(y) => {
				let h = HashFunction::Sha256.oneshot(|x|x.input(y.0.deref())).unwrap();
				println!("pin-sha256=\"{}\"", as_base64(h.as_ref()));
			},
			None => println!("WARNING: Public key not found!")
		}
	}
}
