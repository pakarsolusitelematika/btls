use ::certificate::TrustAnchorValue;
use ::client_certificates::{TrustedLog, LogHashFunction, TrustAnchor};
use ::features::{ConfigFlagsEntry, ConfigFlagsError, ConfigFlagsErrorKind};
use ::stdlib::{BTreeMap, Cow, FromStr, IoRead, String, ToOwned, Vec};
use ::system::{File, Path};

use btls_aux_keyconvert::{Base64Decoder, decode_pem, looks_like_pem};
use btls_aux_serialization::{ASN1_SEQUENCE, Source};

macro_rules! config_strval
{
	($func:expr, $value:expr, $error_cb: expr, $entry:expr, $target:expr) => {
		if let &::features::ConfigFlagsValue::Explicit(ref val) = &$value {
			return $func(val, $error_cb, $entry, $target);
		} else {
			true
		}
	}
}

//Handle blacklist entry.
pub fn handle_blacklist<F>(value: &str, mut error_cb: &mut F, entry: ConfigFlagsEntry,
	killist: &mut BTreeMap<[u8; 32], ()>) where F: FnMut(ConfigFlagsError)
{
	let mut tmp = [0u8; 32];
	let mut valid = true;
	valid &= value.len() == 64;
	for i in value.chars().enumerate() {
		let val = match i.1 {
			'0' => 0, '1' => 1, '2' => 2, '3' => 3, '4' => 4,
			'5' => 5, '6' => 6, '7' => 7, '8' => 8, '9' => 9,
			'a' => 10, 'b' => 11, 'c' => 12, 'd' => 13, 'e' => 14, 'f' => 15,
			'A' => 10, 'B' => 11, 'C' => 12, 'D' => 13, 'E' => 14, 'F' => 15,
			_ => { valid = false; 0 }
		} as u8;
		if let Some(ptr) = tmp.get_mut(i.0>>1) {
			*ptr |= val << (4 - 4 * (i.0 & 1));
		}
	}
	if valid {
		killist.insert(tmp, ());
	} else {
		error_cb(ConfigFlagsError{entry:entry, kind:ConfigFlagsErrorKind::Error(
			"The arguemnt needs to be 64 hexdigits")})
	}
}

//Handle trust anchor entry.
pub fn handle_trustanchor<F>(value: &str, mut error_cb: &mut F, entry: ConfigFlagsEntry,
	trust_anchors: &mut BTreeMap<Vec<u8>, TrustAnchorValue>) where F: FnMut(ConfigFlagsError)
{
	let p: &Path = value.as_ref();
	let mut f = match File::open(p) {
		Ok(x) => x,
		Err(x) => return error_cb(ConfigFlagsError{entry:entry, kind:ConfigFlagsErrorKind::Error(
			&format!("{}", x))})
	};
	let mut content = Vec::new();
	match f.read_to_end(&mut content) {
		Ok(x) => x,
		Err(x) => return error_cb(ConfigFlagsError{entry:entry, kind:ConfigFlagsErrorKind::Error(
			&format!("{}", x))})
	};
	let data: Cow<[u8]> = if looks_like_pem(&content, " CERTIFICATE") {
		Cow::Owned(match decode_pem(&content, " CERTIFICATE", true) {
			Ok(x) => x,
			Err(x) => return error_cb(ConfigFlagsError{entry:entry,
				kind:ConfigFlagsErrorKind::Error(&format!("Internal error: Can't decode \
				PEM?: {}", x))})
		})
	} else {
		Cow::Borrowed(&content)
	};
	let mut source = Source::new(&data);
	while !source.at_end() {
		let marker = source.slice_marker();
		match source.read_asn1_value(ASN1_SEQUENCE, |x|x) {
			Ok(_) => (),
			Err(x) => return error_cb(ConfigFlagsError{entry:entry,
				kind:ConfigFlagsErrorKind::Error(&format!("Parse error: {}", x))})
		};
		let mut data = marker.commit(&mut source);
		let t = match TrustAnchor::from_certificate(&mut data) {
			Ok(x) => x,
			Err(x) => { error_cb(ConfigFlagsError{entry:entry, kind:ConfigFlagsErrorKind::Error(
				&format!("Can't read cert: {}", x))}); continue }
		};
		let (name, content) = t.to_internal_form();
		trust_anchors.insert(name, content);
	}
}

//Handle ctlog entry.
pub fn handle_ctlog<F>(value: &str, mut error_cb: &mut F, entry: ConfigFlagsEntry,
	trusted_logs: &mut Vec<TrustedLog>) where F: FnMut(ConfigFlagsError)
{
	let p: &Path = value.as_ref();
	let mut f = match File::open(p) {
		Ok(x) => x,
		Err(x) => return error_cb(ConfigFlagsError{entry:entry,
			kind:ConfigFlagsErrorKind::Error(&format!("{}", x))})
	};
	let mut content = String::new();
	match f.read_to_string(&mut content) {
		Ok(x) => x,
		Err(x) => return error_cb(ConfigFlagsError{entry:entry,
			kind:ConfigFlagsErrorKind::Error(&format!("{}", x))})
	};
	for line in content.split('\n').enumerate() {
		if line.1.len() == 0 { continue; }
		let version = line.1.split(':').next();
		let (key, id, hash, expiry, name) = if version == Some("v1") {
			let mut itr = line.1.splitn(4, ':');
			let _ = itr.next();
			let key = itr.next();
			let expiry = itr.next();
			let name = itr.next();
			(key, None, None, expiry, name)
		} else if version == Some("v2") {
			let mut itr = line.1.splitn(6, ':');
			let _ = itr.next();
			let id = itr.next();
			let hash = itr.next();
			let key = itr.next();
			let expiry = itr.next();
			let name = itr.next();
			(key, id, hash, expiry, name)
		} else {
			error_cb(ConfigFlagsError{entry:entry, kind:ConfigFlagsErrorKind::Error(&format!(
				"Unknown log version on line {}", line.0))});
			continue;
		};
		//Key, expiry and name must be present.
		let (key, expiry, name) = if let (Some(x), Some(y), Some(z)) = (key, expiry, name) {
			let mut _key = Vec::new();
			let mut dec = Base64Decoder::new();
			match dec.data(x, &mut _key).and_then(|_|dec.end(&mut _key)) {
				Ok(_) => (),
				Err(_) => {
					error_cb(ConfigFlagsError{entry:entry,
						kind:ConfigFlagsErrorKind::Error(&format!(
						"Bad key on line {}", line.0))});
					continue;
				}
			};
			let _expiry = if y == "" {
				None
			} else {
				Some(match i64::from_str(y) {
					Ok(x) => x,
					Err(_) => {
						error_cb(ConfigFlagsError{entry:entry,
							kind:ConfigFlagsErrorKind::Error(&format!(
							"Bad expiry on line {}", line.0))});
						continue;
					}
				})
			};
			(_key, _expiry, z)
		} else {
			error_cb(ConfigFlagsError{entry:entry, kind:ConfigFlagsErrorKind::Error(&format!(
				"Parse error on line {}", line.0))});
			continue;
		};
		//If id is present, hash needs to be too.
		let (id, hash) = if let (Some(id), Some(hash)) = (id, hash) {
			let _hash = if hash == "sha256" {
				LogHashFunction::Sha256
			} else {
				error_cb(ConfigFlagsError{entry:entry, kind:ConfigFlagsErrorKind::Error(
					&format!("Unknown hash function on line {}", line.0))});
				continue;
			};
			let mut dec = Base64Decoder::new();
			let mut _id = Vec::new();
			match dec.data(id, &mut _id).and_then(|_|dec.end(&mut _id)) {
				Ok(_) => (),
				Err(_) => {
					error_cb(ConfigFlagsError{entry:entry,
						kind:ConfigFlagsErrorKind::Error(&format!(
						"Bad log ID on line {}", line.0))});
					continue;
				}
			};
			(_id, _hash)
		} else if let (None, None) = (id, hash) {
			(Vec::new(), LogHashFunction::Sha256)
		} else {
			error_cb(ConfigFlagsError{entry:entry, kind:ConfigFlagsErrorKind::Error(&format!(
				"Parse error on line {}", line.0))});
			continue;
		};
		let log = TrustedLog {
			name: name.to_owned(),
			key: key,
			v2_id: id,
			v2_hash: hash,
			expiry: expiry
		};
		trusted_logs.push(log);
	}
}

//Handle ctlog entry.
pub fn handle_ocsp_maxvalid<F>(value: &str, mut error_cb: &mut F, entry: ConfigFlagsEntry,
	ocsp_maxvalid: &mut u64) where F: FnMut(ConfigFlagsError)
{
	*ocsp_maxvalid = match u64::from_str(value) {
		Ok(x) => x,
		Err(_) => {
			error_cb(ConfigFlagsError{entry:entry,
				kind:ConfigFlagsErrorKind::Error(&format!(
				"Can't parse ocsp-maxvalid value as number"))});
			return;
		}
	};
}
