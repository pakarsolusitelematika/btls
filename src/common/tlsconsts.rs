use ::stdlib::{Range, RangeTo};

use btls_aux_serialization::{MAX16, MAX24};

pub const HSTYPE_CLIENT_HELLO: u8 = 1;
pub const HSTYPE_SERVER_HELLO: u8 = 2;
pub const HSTYPE_SESSION_TICKET: u8 = 4;
pub const HSTYPE_HELLO_RETRY_REQUEST: u8 = 6;
pub const HSTYPE_ENCRYPTED_EXTENSIONS: u8 = 8;
pub const HSTYPE_CERTIFICATE: u8 = 11;
pub const HSTYPE_SERVER_KEY_EXCHANGE: u8 = 12;
pub const HSTYPE_CERTIFICATE_REQUEST: u8 = 13;
pub const HSTYPE_SERVER_HELLO_DONE: u8 = 14;
pub const HSTYPE_CERTIFICATE_VERIFY: u8 = 15;
pub const HSTYPE_CLIENT_KEY_EXCHANGE: u8 = 16;
pub const HSTYPE_FINISHED: u8 = 20;
pub const HSTYPE_CERTIFICATE_STATUS: u8 = 22;
pub const HSTYPE_KEY_UPDATE: u8 = 24;

pub const PROTO_CCS: u8 = 20;
pub const PROTO_ALERT: u8 = 21;
pub const PROTO_HANDSHAKE: u8 = 22;
pub const PROTO_APPDATA: u8 = 23;

pub const EXT_SERVER_NAME: u16 = 0;
pub const EXT_STATUS_REQUEST: u16 = 5;
pub const EXT_SUPPORTED_GROUPS: u16 = 10;
pub const EXT_SIGNATURE_ALGORITHMS: u16 = 13;
pub const EXT_ALPN: u16 = 16;
pub const EXT_CT: u16 = 18;
pub const EXT_SERVER_CERTIFICATE_TYPE: u16 = 20;
pub const EXT_EMS: u16 = 23;
pub const EXT_KEY_SHARE: u16 = 40;
pub const EXT_COOKIE: u16 = 44;
pub const EXT_SUPPORTED_VERSIONS: u16 = 43;
pub const EXT_RENEGO_INFO: u16 = 65281;
pub const EXT_RECORD_SIZE_LIMIT: u16 = 0x1053;

pub const CS_ECDHE_ECDSA_AES128: u16 = 0xC02B;
pub const CS_ECDHE_ECDSA_AES256: u16 = 0xC02C;
pub const CS_ECDHE_ECDSA_CHACHA20: u16 = 0xCCA9;
pub const CS_ECDHE_RSA_AES128: u16 = 0xC02F;
pub const CS_ECDHE_RSA_AES256: u16 = 0xC030;
pub const CS_ECDHE_RSA_CHACHA20: u16 = 0xCCA8;
pub const CS_TLS13_AES128: u16 = 0x1301;
pub const CS_TLS13_AES256: u16 = 0x1302;
pub const CS_TLS13_CHACHA20: u16 = 0x1303;
pub const CS_RENEGO_FIX_HACK: u16 = 0x00ff;
pub const CS_FALLBACK_FIX_HACK: u16 = 0x5600;

pub const OCSP_SINGLE: u8 = 1;

pub const CERTTYPE_X509: u8 = 0;
pub const CERTTYPE_RPK: u8 = 2;

pub const TLS_LEN_EXTENSION: RangeTo<usize> = ..MAX16;
pub const TLS_LEN_EXTENSIONS: RangeTo<usize> = ..MAX16;
pub const TLS_LEN_ALPN_LIST: Range<usize> = 1..MAX16;
pub const TLS_LEN_ALPN_ENTRY: Range<usize> = 1..255;
pub const TLS_LEN_SNI_LIST: Range<usize> = 2..MAX16;
pub const TLS_LEN_SNI_ENTRY: RangeTo<usize> = ..MAX16;
pub const TLS_LEN_SCT_LIST: Range<usize> = 1..MAX16;
pub const TLS_LEN_SCT_ENTRY: Range<usize> = 1..MAX16;
pub const TLS_LEN_SIGNATURE: RangeTo<usize> = ..MAX16;
pub const TLS_LEN_TLS13_PUBKEY: Range<usize> = 1..MAX16;
pub const TLS_LEN_TLS13_PUBKEY_LIST: RangeTo<usize> = ..MAX16;
pub const TLS_LEN_TLS12_PUBKEY: Range<usize> = 1..255;
pub const TLS_LEN_CERT_LIST: RangeTo<usize> = ..MAX24;
pub const TLS_LEN_CERT_ENTRY: Range<usize> = 1..MAX24;
pub const TLS_LEN_CERT_CONTEXT: Range<usize> = 0..255;
pub const TLS_LEN_OCSP_RESPONSE: Range<usize> = 1..MAX24;
pub const TLS_LEN_OCSP_RESPONDER: RangeTo<usize> = ..MAX16;
pub const TLS_LEN_OCSP_EXTENSIONS: RangeTo<usize> = ..MAX16;
pub const TLS_LEN_COOKIE: Range<usize> = 1..MAX16;
pub const TLS_LEN_CIPHERSUITE_LIST: Range<usize> = 2..MAX16;
pub const TLS_LEN_SCT_PRECERT_LEN: Range<usize> = 1..MAX24;
pub const TLS_LEN_SCT_CERT_LEN: Range<usize> = 1..MAX24;
pub const TLS_LEN_SCT2_CERT_LEN: Range<usize> = 1..MAX24;
pub const TLS_LEN_SCT_EXTENSIONS: RangeTo<usize> = ..MAX16;
pub const TLS_LEN_SUPGRP_LIST: Range<usize> = 1..MAX16;
pub const TLS_LEN_SIGALGO_LIST: Range<usize> = 1..MAX16;
pub const TLS_LEN_SCT2_LOGID: Range<usize> = 1..127;
pub const TLS_LEN_SCERTT_LIST: Range<usize> = 1..255;
pub const TLS_LEN_SUPVER_LIST: Range<usize> = 1..255;
pub const TLS_LEN_COMPRESS_LIST: Range<usize> = 1..255;
