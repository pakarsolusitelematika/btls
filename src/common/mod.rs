use ::TlsConnectionInfo;
use ::logging::{bytes_as_hex_block, Logging};
use ::record::SessionBase;
use ::stdlib::{Arc, Box, BTreeMap, Display, FmtError, Formatter, String, swap, Vec};

use btls_aux_aead::ProtectorType;
use btls_aux_dhf::Dhf;
use btls_aux_hash::{HashFunction, HashFunctionContext, HashOutput};
use btls_aux_serialization::{LengthTrait, Sink};
use btls_aux_signature_algo::SignatureType;
pub use btls_aux_signature_algo::SIGALGO_STRINGS;
use btls_aux_signature_algo::TLS_SIG_ALGOS;

#[macro_use]
mod tlserr;
pub use self::tlserr::{alert_name_as_string, AssertFailed, TlsFailure};

mod ciphersuite;
pub use self::ciphersuite::*;
#[macro_use]
mod config;
pub use self::config::*;
mod state;
pub use self::state::*;
mod tlsconsts;
pub use self::tlsconsts::*;

pub type Debugger = Option<(u64, Arc<Box<Logging+Send+Sync>>)>;

pub const CLIENT_FINISHED_LABEL: &'static str = "client finished";
pub const SERVER_FINISHED_LABEL: &'static str = "server finished";

//A label for TLS 1.3 HKDF.
#[derive(Copy,Clone)]
pub enum HkdfLabel<'a>
{
	//First is server, second is final.
	Traffic(bool, bool),
	ExporterD,
	Finished,
	DerivedSecret,
	Key,
	Iv,
	ExporterM,
	KeyUpdate,
	Custom(&'a str),
}

impl<'a> HkdfLabel<'a>
{
	//Get the label.
	pub fn get(&'a self, draft_version: Tls13Draft) -> (&'a str, bool)
	{
		let d20flag = draft_version.at_least_20();
		let string = match (d20flag, *self) {
			(true,  HkdfLabel::Traffic(false, false)) => "c hs traffic",
			(true,  HkdfLabel::Traffic(false, true )) => "c ap traffic",
			(true,  HkdfLabel::Traffic(true,  false)) => "s hs traffic",
			(true,  HkdfLabel::Traffic(true,  true )) => "s ap traffic",
			(true,  HkdfLabel::DerivedSecret        ) => "derived",
			(true,  HkdfLabel::ExporterM            ) => "exp master",
			(true,  HkdfLabel::KeyUpdate            ) => "traffic upd",
			(false, HkdfLabel::Traffic(false, false)) => "client handshake traffic secret",
			(false, HkdfLabel::Traffic(false, true )) => "client application traffic secret",
			(false, HkdfLabel::Traffic(true,  false)) => "server handshake traffic secret",
			(false, HkdfLabel::Traffic(true,  true )) => "server application traffic secret",
			(_,     HkdfLabel::ExporterD            ) => "exporter",
			(_,     HkdfLabel::Finished             ) => "finished",
			(false, HkdfLabel::DerivedSecret        ) => "derived secret",
			(_,     HkdfLabel::Key                  ) => "key",
			(_,     HkdfLabel::Iv                   ) => "iv",
			(false, HkdfLabel::ExporterM            ) => "exporter master secret",
			(false, HkdfLabel::KeyUpdate            ) => "application traffic secret",
			(_,     HkdfLabel::Custom(x)            ) => x,
		};
		(string, d20flag)
	}
}


pub fn builtin_killist() -> BTreeMap<[u8; 32], ()>
{
	let mut x = BTreeMap::new();
	let builtin_kills = include_str!("builtin-cert-blacklist.txt");
	for i in builtin_kills.lines() {
		let mut y = [0u8; 32];
		let mut valid = false;
		for i in i.chars().enumerate() {
			let ch = i.1 as u32;
			let shift = 4 ^ ((i.0 & 1) << 2) as u32;
			if let Some(x) = y.get_mut(i.0>>1) {
				if ch >= 0x30 && ch <= 0x39 {
					*x |= (ch.saturating_sub(0x30)).wrapping_shl(shift) as u8;
				} else if ch >= 0x41 && ch <= 0x46 {
					*x |= (ch.saturating_sub(0x41-10)).wrapping_shl(shift) as u8;
				} else if ch >= 0x61 && ch <= 0x66 {
					*x |= (ch.saturating_sub(0x61-10)).wrapping_shl(shift) as u8;
				} else {
					break;	//Not valid.
				}
			} else {
				break;
			}
			valid |= i.0 == 63;	//Completed.
		}
		if valid { x.insert(y, ()); }
	}
	x
}

pub struct HandshakeMessage<'a>
{
	hs_type: u8,		//The type of message.
	msg: &'a [u8],		//The message payload.
}

impl<'a> HandshakeMessage<'a>
{
	pub fn new(hs_type: u8, msg: &'a [u8]) -> HandshakeMessage<'a>
	{
		HandshakeMessage {
			hs_type: hs_type,
			msg: msg,
		}
	}
}

pub trait HashMessages
{
	fn add<'a>(&mut self, msg: HandshakeMessage<'a>, debug: Debugger) -> Result<(), TlsFailure>;
}

pub struct HandshakeHash
{
	context: Box<HashFunctionContext+Send>,
}

impl HandshakeHash
{
	pub fn new(prf: HashFunction, debug: Debugger) -> HandshakeHash
	{
		debug!(CRYPTO_CALCS debug, "Setting up handshake hash: {:?}", prf);
		HandshakeHash {
			context: prf.make_context()
		}
	}
	pub fn write_raw(&mut self, raw: &[u8], debug: Debugger) -> Result<(), TlsFailure>
	{
		self.context.input(raw);
		debug!(CRYPTO_CALCS debug, "Including {} bytes of raw data into handshake hash, updated:\n{}",
			raw.len(), bytes_as_hex_block(self.checkpoint()?.as_ref(), ">"));
		Ok(())
	}
	pub fn compress(&mut self, debug: Debugger) -> Result<(), TlsFailure>
	{
		let hashout = self.checkpoint()?;
		self.context.reset();
		let dlen = hashout.as_ref().len();
		let header = [254, (dlen >> 16) as u8, (dlen >> 8) as u8, dlen as u8];
		self.context.input_sg(&[&header, hashout.as_ref()]);
		debug!(CRYPTO_CALCS debug, "Compressing handshake block, updated:\n{}", bytes_as_hex_block(self.
			checkpoint()?.as_ref(), ">"));
		Ok(())
	}
	pub fn checkpoint(&self) -> Result<HashOutput, TlsFailure>
	{
		Ok(self.context.output().map_err(|x|assert_failure!("Internal error in hash function: {}", x))?)
	}
}

impl HashMessages for HandshakeHash
{
	fn add<'a>(&mut self, msg: HandshakeMessage<'a>, debug: Debugger) -> Result<(), TlsFailure>
	{
		let dlen = msg.msg.len();
		let header = [msg.hs_type, (dlen >> 16) as u8, (dlen >> 8) as u8, dlen as u8];
		self.context.input_sg(&[&header, msg.msg]);
		debug!(CRYPTO_CALCS debug, "Including handshake message {} ({} bytes) into handshake hash, \
			updated:\n{}", msg.hs_type, dlen, bytes_as_hex_block(self.checkpoint()?.as_ref(), ">"));
		Ok(())
	}
}

pub enum EarlyHandshakeRep
{
	Plaintext(Vec<u8>),
	Hash(HandshakeHash),
}

impl EarlyHandshakeRep
{
	///Create new empty handshake representation.
	pub fn new() -> EarlyHandshakeRep
	{
		EarlyHandshakeRep::Plaintext(Vec::new())
	}
/*
	///Check if representation is already hashed.
	pub fn is_hashed(&self) -> bool
	{
		match self {
			&EarlyHandshakeRep::Plaintext(_) => false,
			&EarlyHandshakeRep::Hash(_) => true,
		}
	}
*/
	///Convert the early handshake representation to hashed form. If compress is true, the hash is injected
	///indirectly (used for restart handshake in TLS 1.3-draft19+.). Can only be called once.
	pub fn convert_to_hash(&mut self, hash: HashFunction, debug: Debugger, compress: bool) ->
		Result<(), TlsFailure>
	{
		let mut hs_hash = HandshakeHash::new(hash, debug.clone());
		match self {
			&mut EarlyHandshakeRep::Plaintext(ref x) => {
				hs_hash.write_raw(x, debug.clone())?;
				if compress { hs_hash.compress(debug.clone())?; }
			},
			_ => sanity_failed!("Attempted to set PRF twice"),
		};
		let mut hs_hash = EarlyHandshakeRep::Hash(hs_hash);
		swap(&mut hs_hash, self);
		Ok(())
	}
	///Unwrap the early handshake representation as hash. convert_to_hash() must be called before this.
	pub fn unwrap_hash(self) -> Result<HandshakeHash, TlsFailure>
	{
		match self {
			EarlyHandshakeRep::Plaintext(_) => sanity_failed!("PRF not set before calling unwrap_hash"),
			EarlyHandshakeRep::Hash(x) => Ok(x),
		}
	}
}

impl HashMessages for EarlyHandshakeRep
{
	fn add<'a>(&mut self, msg: HandshakeMessage<'a>, debug: Debugger) -> Result<(), TlsFailure>
	{
		match self {
			&mut EarlyHandshakeRep::Plaintext(ref mut client_hello) => {
				let dlen = msg.msg.len();
				let header = [msg.hs_type, (dlen >> 16) as u8, (dlen >> 8) as u8, dlen as u8];
				client_hello.extend_from_slice(&header);
				client_hello.extend_from_slice(msg.msg);
			},
			&mut EarlyHandshakeRep::Hash(ref mut hash) => {
				hash.add(msg, debug)?;
			}
		}
		Ok(())
	}
}

//Emit a handshake message, adding it to the handshake hash.
pub fn emit_handshake<T:HashMessages>(debug: Debugger, htype: u8, sink: &[u8], base: &mut SessionBase,
	hs_hash: &mut T, state: &str) -> Result<(), TlsFailure>
{
	sanity_check!(sink.len() <= 0xFFFFFF, "Handshake message too big to send ({})", sink.len());
	hs_hash.add(HandshakeMessage::new(htype, sink), debug)?;
	base.send_handshake(htype, sink, state);
	Ok(())
}


pub struct CryptoTracker
{
	ems_enabled: bool,
	version: TlsVersion,
	ciphersuite: Option<Ciphersuite>,
	protection: Option<ProtectorType>,
	hash: Option<HashFunction>,
	group: Option<Dhf>,
	signature: Option<SignatureType>,
	validated_ocsp: bool,
	validated_ocsp_shortlived: bool,
	validated_ct: bool,
}

impl CryptoTracker
{
	pub fn new() -> CryptoTracker
	{
		CryptoTracker {
			ems_enabled: false,
			version: TlsVersion::Tls12,
			ciphersuite: None,
			protection: None,
			hash: None,
			group: None,
			signature: None,
			validated_ct: false,
			validated_ocsp: false,
			validated_ocsp_shortlived: false,
		}
	}
	//Set the protection and PRF (these may be seperatedly known, without knowing key exchange yet).
	pub fn set_protection_prf(&mut self, protection: ProtectorType, prf: HashFunction)
	{
		self.protection = Some(protection);
		self.hash = Some(prf);
	}
	//Set the ciphersuite.
	pub fn set_ciphersuite(&mut self, cs: Ciphersuite)
	{
		self.protection = Some(cs.get_protector());
		self.hash = Some(cs.get_prf());
		self.ciphersuite = Some(cs);
	}
	//set the key exchange group.
	pub fn set_kex_group(&mut self, newgrp: Dhf)
	{
		self.group = Some(newgrp);
	}
	//set the signature algorithm.
	pub fn set_signature(&mut self, sigalgo: SignatureType)
	{
		self.signature = Some(sigalgo);
	}
	//Notify that EMS was enabled.
	pub fn enable_ems(&mut self)
	{
		self.ems_enabled = true;
	}
	//Notify that TLS 1.3 was enabled.
	pub fn set_version(&mut self, version: TlsVersion)
	{
		self.version = version;
	}
	//Notify validation of CT.
	pub fn ct_validated(&mut self)
	{
		self.validated_ct = true;
	}
	//Notify validation of OCSP.
	pub fn ocsp_validated(&mut self)
	{
		self.validated_ocsp = true;
	}
	//Notify validation of short-lived certificate.
	pub fn ocsp_shortlived(&mut self)
	{
		self.validated_ocsp = true;
		self.validated_ocsp_shortlived = true;
	}
	pub fn connection_info(&self) -> TlsConnectionInfo
	{
		TlsConnectionInfo {
			version: if self.version.is_tls12() { 3 } else { 4 },
			ems_available: self.ems_enabled || !self.version.is_tls12(),
			ciphersuite: self.ciphersuite.map(|x|x.to_tls_id()).unwrap_or(0),
			kex: self.group.map(|x|x.to_tls_id()),
			signature: self.signature.map(|x|x.tls_id()),
			version_str: self.version.as_string(),
			protection_str: self.protection.map(|x|x.as_string()).unwrap_or(""),
			hash_str: self.hash.map(|x|x.as_string()).unwrap_or(""),
			exchange_group_str: self.group.map(|x|x.as_string()).unwrap_or(""),
			signature_str: self.signature.map(|x|x.as_string()).unwrap_or(""),
			validated_ct: self.validated_ct,
			validated_ocsp: self.validated_ocsp,
			validated_ocsp_shortlived: self.validated_ocsp_shortlived,
		}
	}
}

pub struct NamesContainer
{
	alpn: Option<Option<Arc<String>>>,
	sni: Option<Option<Arc<String>>>,
	server_names: Option<Arc<Vec<String>>>,
	server_names_released: bool,
}

impl NamesContainer
{
	pub fn new() -> NamesContainer
	{
		NamesContainer {
			alpn: None,
			sni: None,
			server_names: None,
			server_names_released: false,
		}
	}
	//Get the ALPN in effect.
	pub fn get_alpn(&self) -> Option<Option<Arc<String>>>
	{
		self.alpn.clone()
	}
	//Get the SNI in effect.
	pub fn get_sni(&self) -> Option<Option<Arc<String>>>
	{
		self.sni.clone()
	}
	//Get the server name list (this won't give anything until OK'd).
	pub fn get_server_names(&self) -> Option<Arc<Vec<String>>>
	{
		if !self.server_names_released { return None; }
		self.server_names.clone()
	}
	//Set the ALPN name.
	pub fn set_alpn(&mut self, protocol: String)
	{
		self.alpn = Some(Some(Arc::new(protocol)));
	}
	//Set the SNI name.
	pub fn set_sni(&mut self, name: String)
	{
		self.sni = Some(Some(Arc::new(name)));
	}
	//Notify that ALPN is known, so if isn't set yet, there is no ALPN.
	pub fn alpn_known(&mut self)
	{
		if self.alpn.is_none() { self.alpn = Some(None); }
	}
	//Notify that SNI is known, so if isn't set yet, there is no SNI.
	pub fn sni_known(&mut self)
	{
		if self.sni.is_none() { self.sni = Some(None); }
	}
	//Set the server name list (does not release it).
	pub fn set_server_names(&mut self, names: Arc<Vec<String>>)
	{
		self.server_names = Some(names);
	}
	//Release the server name list.
	pub fn release_server_names(&mut self)
	{
		self.server_names_released = true;
	}
}

//Format the server signature TBS for TLS 1.2
pub fn format_tbs_tls12(share: &[u8], client_random: &[u8; 32], server_random: &[u8; 32]) ->
	Result<Vec<u8>, TlsFailure>
{
	let mut tbs = Vec::new();
	tbs.extend_from_slice(client_random);
	tbs.extend_from_slice(server_random);
	tbs.extend_from_slice(share);
	Ok(tbs)
}

//Format the server signature TBS for TLS 1.3
pub fn format_tbs_tls13(hash: HashOutput, server: bool) -> Result<Vec<u8>, TlsFailure>
{
	let mut tbs = Vec::new();
	let label = if server {
		"TLS 1.3, server CertificateVerify"
	} else {
		"TLS 1.3, client CertificateVerify"
	};
	let padding = [32;32];
	tbs.extend_from_slice(&padding);
	tbs.extend_from_slice(&padding);
	tbs.extend_from_slice(label.as_bytes());
	tbs.extend_from_slice(&[0]);
	tbs.extend_from_slice(hash.as_ref());
	Ok(tbs)
}

const VERSION_TLS12: u16 = 0x0303;
const VERSION_TLS13: u16 = 0x0304;

#[derive(Copy,Clone)]
pub struct Tls13Draft(u8);

impl Tls13Draft
{
	pub fn into_inner(self) -> u8 { self.0 }
	pub fn at_least_19(&self) -> bool { self.0 >= 19 }
	pub fn at_least_20(&self) -> bool { self.0 >= 20 }
}

///TLS version.
#[derive(Copy,Clone,Debug)]
pub enum TlsVersion
{
	Tls12,
	Tls13,
	Tls13Draft17,
	Tls13Draft18,
	Tls13Draft19,
	Tls13Draft20,
}

impl TlsVersion
{
	pub fn supported_tls13() -> &'static [TlsVersion]
	{
		static RET: [TlsVersion;4] = [TlsVersion::Tls13Draft20, TlsVersion::Tls13Draft19,
			TlsVersion::Tls13Draft18, TlsVersion::Tls13Draft17];
		&RET
	}
	pub fn by_tls_id(ver_id: u16, ch_soft: bool) -> Result<TlsVersion, TlsFailure>
	{
		Ok(match ver_id {
			VERSION_TLS12 => TlsVersion::Tls12,			//TLS 1.2
			VERSION_TLS13 => TlsVersion::Tls13,			//TLS 1.3.
			0x0305...0x03ff if ch_soft => TlsVersion::Tls13,	//Fallback.
			0x7f11 => TlsVersion::Tls13Draft17,			//TLS 1.3 draft 17.
			0x7f12 => TlsVersion::Tls13Draft18,			//TLS 1.3 draft 18.
			0x7f13 => TlsVersion::Tls13Draft19,			//TLS 1.3 draft 19.
			0x7f14 => TlsVersion::Tls13Draft20,			//TLS 1.3 draft 20.
			0x0300 => fail!(TlsFailure::Ssl3NotSupported),
			0x0301 => fail!(TlsFailure::Tls10NotSupported),
			0x0302 => fail!(TlsFailure::Tls11NotSupported),
			x => fail!(TlsFailure::BogusShVersion(x)),
		})
	}
	pub fn is_tls12(&self) -> bool
	{
		match *self {
			TlsVersion::Tls12 => true,
			TlsVersion::Tls13 => false,
			TlsVersion::Tls13Draft17 => false,
			TlsVersion::Tls13Draft18 => false,
			TlsVersion::Tls13Draft19 => false,
			TlsVersion::Tls13Draft20 => false,
		}
	}
	pub fn to_code(&self) -> u16
	{
		match *self {
			TlsVersion::Tls12 => VERSION_TLS12,
			TlsVersion::Tls13 => VERSION_TLS13,
			TlsVersion::Tls13Draft17 => 0x7f11,
			TlsVersion::Tls13Draft18 => 0x7f12,
			TlsVersion::Tls13Draft19 => 0x7f13,
			TlsVersion::Tls13Draft20 => 0x7f14,
		}
	}
	pub fn draft_code(&self) -> Tls13Draft
	{
		match *self {
			TlsVersion::Tls12 => Tls13Draft(0),
			TlsVersion::Tls13 => Tls13Draft(255),
			TlsVersion::Tls13Draft17 => Tls13Draft(17),
			TlsVersion::Tls13Draft18 => Tls13Draft(18),
			TlsVersion::Tls13Draft19 => Tls13Draft(19),
			TlsVersion::Tls13Draft20 => Tls13Draft(20),
		}
	}
	pub fn as_string(&self) -> &'static str
	{
		match *self {
			TlsVersion::Tls12 => "1.2",
			TlsVersion::Tls13 => "1.3",
			TlsVersion::Tls13Draft17 => "1.3-draft17",
			TlsVersion::Tls13Draft18 => "1.3-draft18",
			TlsVersion::Tls13Draft19 => "1.3-draft19",
			TlsVersion::Tls13Draft20 => "1.3-draft20",
		}
	}
}

impl Display for TlsVersion
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		match self.draft_code().into_inner() {
			0 => fmt.write_str("TLS 1.2"),
			255 => fmt.write_str("TLS 1.3"),
			x => fmt.write_fmt(format_args!("TLS 1.3-draft{}", x)),
		}
	}
}

pub struct PrintBitfieldFlags(pub u64, pub &'static [&'static str]);

impl Display for PrintBitfieldFlags
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		let mut next = false;
		for i in 0..63 {
			let bit = (self.0 >> i) & 1 != 0;
			if bit {
				if next {
					fmt.write_str(" ")?;
				}
				if i < self.1.len() {
					fmt.write_str(self.1[i])?;
				} else {
					fmt.write_fmt(format_args!("UNKNOWN{}", i))?;
				}
			}
			next |= bit;
		}
		Ok(())
	}
}


#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub enum KeyExchangeType
{
	Tls12EcdheRsa,
	Tls12EcdheEcdsa,
	Tls13,
}

pub static CIPHERSUITE_STRINGS: [&'static str; 9] = [
	"TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA_256", "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA_384",
	"TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA_256", "TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA_256",
	"TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA_384", "TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA_256",
	"TLS13_AES_128_GCM_SHA_256", "TLS13_AES_256_GCM_SHA_384", "TLS13_CHACHA20_POLY1305_SHA_256",
];

pub static CERTTYPE_STRINGS: [&'static str; 2] = ["X509", "RPK"];
pub static DHGROUP_STRINGS: [&'static str; 4] = ["NIST_P256", "NIST_P384", "X25519", "X448"];

//Translate error case into buffer overflow message. This can't actually happen, since we are using Vec,
//which can't overflow capacity.
macro_rules! try_buffer
{
	($exp:expr) => { $exp.map_err(|_|assert_failure!("TX serialization buffer overflow"))? }
}

//Write a TLS vector of specified length of length, call callback to populate it, returning an error if length limit
//is exceeded
pub fn write_sub_fn<S:Sink,F,L:LengthTrait>(to: &mut S, name: &str, marker: L, mut cb: F) -> Result<(), TlsFailure>
	where F: FnMut(&mut S) -> Result<(), TlsFailure>
{
	let mut result = Ok(());
	to.vector_fn(marker, |ctx|{
		result = cb(ctx);
		if result.is_err() { Err(()) } else { Ok(()) }
	}).map_err(|_|assert_failure!("Element {} too big to send", name))?;
	result
}

//Write an TLS extension with 16bit tag and 16 bit extension, returning an error is length limit is exceeded.
pub fn write_ext_fn<S:Sink,F>(to: &mut S, extnum: u16, name: &str, cb: F) -> Result<(), TlsFailure>
	where F: FnMut(&mut S) -> Result<(), TlsFailure>
{
	try_buffer!(to.write_u16(extnum));
	write_sub_fn(to, name, TLS_LEN_EXTENSION, cb)
}

//Get priority for TLS algo id (<0 means unusable)
fn get_priority(algo: u16) -> i32
{
	if algo == 0 { return -1; }	//Fastpath.
	SignatureType::by_tls_id(algo).map(|x|x.get_priority()).unwrap_or(-1)
}

//Write signature algorithms extension.
pub fn write_signature_algorithms_ext<S:Sink>(x: &mut S) -> Result<(), TlsFailure>
{
	write_ext_fn(x, EXT_SIGNATURE_ALGORITHMS, "signature_algorithms", |x|{
		//64 elements is enough, because we can't have more than 64 signature algorithms, as the bitmask
		//is 64-bit. And 64 is enough because we don't do Pokémon crypto...
		let mut siglist = [0u16; 64];
		for (n, v) in TLS_SIG_ALGOS.iter().enumerate() {
			siglist.get_mut(n).map(|x|*x = *v);	//Ignore overflowing entries.
		}
		siglist.sort_by(|x,y|get_priority(*y).cmp(&get_priority(*x)));
		write_sub_fn(x, "signature algorithms", TLS_LEN_SIGALGO_LIST, |x|{
			for i in siglist.iter() {
				if get_priority(*i) < 0 { break; }
				try_buffer!(x.write_u16(*i));
			}
			Ok(())
		})
	})?;
	Ok(())
}


#[test]
fn test_builtin_killist()
{
	assert_eq!(builtin_killist().len(), 2);
}
