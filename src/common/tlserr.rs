use ::certificate::{CertificateError, CertificateValidationError, MajorCertProblem, OcspValidationError, SctError};
use ::common::Ciphersuite;
use ::stdlib::{AsRef, Cow, Display, Error, FmtError, Formatter, String};
use ::utils::{HsBufferError};

pub use btls_aux_assert::AssertFailed;
use btls_aux_dhf::KeyAgreementError;

macro_rules! tlserr
{
	(CLOSE_NOTIFY) => {0};
	(END_OF_EARLY_DATA) => {1};
	(UNEXPECTED_MESSAGE) => {10};
	(BAD_RECORD_MAC) => {20};
	(DECRYPTION_FAILED) => {21};
	(RECORD_OVERFLOW) => {22};
	(DECOMPRESSION_FAILURE) => {30};
	(HANDSHAKE_FAILURE) => {40};
	(NO_CERTIFICATE) => {41};
	(BAD_CERTIFICATE) => {42};
	(UNSUPPORTED_CERTIFICATE) => {43};
	(CERTIFICATE_REVOKED) => {44};
	(CERTIFICATE_EXPIRED) => {45};
	(CERTIFICATE_UNKNOWN) => {46};
	(ILLEGAL_PARAMETER) => {47};
	(UNKNOWN_CA) => {48};
	(ACCESS_DENIED) => {49};
	(DECODE_ERROR) => {50};
	(DECRYPT_ERROR) => {51};
	(EXPORT_RESTRICTION) => {60};
	(PROTOCOL_VERSION) => {70};
	(INSUFFICIENT_SECURITY) => {71};
	(INTERNAL_ERROR) => {80};
	(INAPPORIATE_FALLBACK) => {86};
	(USER_CANCELED) => {90};
	(NO_RENEGOTIATION) => {100};
	(MISSING_EXTENSION) => {109};
	(UNSUPPORTED_EXTENSION) => {110};
	(CERTIFICATE_UNOBTAINABLE) => {111};
	(UNRECOGNIZED_NAME) => {112};
	(BAD_CERTIFICATE_STATUS_RESPONSE) => {113};
	(BAD_CERTIFICATE_HASH_VALUE) => {114};
	(UNKNOWN_PSK_IDENTITY) => {115};
	(CERTIFICATE_REQUIRED) => {116};
}

///Returns the alert name, or None if not a known alert.
pub fn alert_name_as_string(alert: u8) -> Option<&'static str>
{
	Some(match alert {
		tlserr!(CLOSE_NOTIFY) => "close notify",
		tlserr!(END_OF_EARLY_DATA) => "end of early data",
		tlserr!(UNEXPECTED_MESSAGE) => "unexpected message",
		tlserr!(BAD_RECORD_MAC) => "bad record MAC",
		tlserr!(DECRYPTION_FAILED) => "decryption failed",
		tlserr!(RECORD_OVERFLOW) => "record overflow",
		tlserr!(DECOMPRESSION_FAILURE) => "decompression failure",
		tlserr!(HANDSHAKE_FAILURE) => "handshake failure",
		tlserr!(NO_CERTIFICATE) => "no certificate",
		tlserr!(BAD_CERTIFICATE) => "bad certificate",
		tlserr!(UNSUPPORTED_CERTIFICATE) => "unsupported certificate",
		tlserr!(CERTIFICATE_REVOKED) => "certificate revoked",
		tlserr!(CERTIFICATE_EXPIRED) => "certificate expired",
		tlserr!(CERTIFICATE_UNKNOWN) => "certificate unknown",
		tlserr!(ILLEGAL_PARAMETER) => "illegal parameter",
		tlserr!(UNKNOWN_CA) => "unknown CA",
		tlserr!(ACCESS_DENIED) => "access denied",
		tlserr!(DECODE_ERROR) => "decode error",
		tlserr!(DECRYPT_ERROR) => "decrypt error",
		tlserr!(EXPORT_RESTRICTION) => "export restriction",
		tlserr!(PROTOCOL_VERSION) => "protocol version",
		tlserr!(INSUFFICIENT_SECURITY) => "insufficient security",
		tlserr!(INTERNAL_ERROR) => "internal error",
		tlserr!(INAPPORIATE_FALLBACK) => "inapporiate fallback",
		tlserr!(USER_CANCELED) => "user canceled",
		tlserr!(NO_RENEGOTIATION) => "no renegotiation",
		tlserr!(MISSING_EXTENSION) => "missing extension",
		tlserr!(UNSUPPORTED_EXTENSION) => "unsupported extension",
		tlserr!(CERTIFICATE_UNOBTAINABLE) => "certificate unobtainable",
		tlserr!(UNRECOGNIZED_NAME) => "unrecognized name",
		tlserr!(BAD_CERTIFICATE_STATUS_RESPONSE) => "bad certificate status response",
		tlserr!(BAD_CERTIFICATE_HASH_VALUE) => "bad certificate hash value",
		tlserr!(UNKNOWN_PSK_IDENTITY) => "unknown PSK identity",
		tlserr!(CERTIFICATE_REQUIRED) => "certificate required",
		_ => return None
	})
}

macro_rules! debug_class
{
	(HANDSHAKE_MSGS) => {::logging::DEBUG_HANDSHAKE_MSGS};
	(ABORTS) => {::logging::DEBUG_ABORTS};
	(CRYPTO_CALCS) => {::logging::DEBUG_CRYPTO_CALCS};
	(HANDSHAKE_DATA) => {::logging::DEBUG_HANDSHAKE_DATA};
	(HANDSHAKE_EVENTS) => {::logging::DEBUG_HANDSHAKE_EVENTS};
	(TLS_EXTENSIONS) => {::logging::DEBUG_TLS_EXTENSIONS};
}

macro_rules! debug {
	($class:ident $obj:expr, $msg:expr) => {
		if let &Some((mask, ref x)) = &$obj {
			if mask & ::logging::DEBUG_MASK & debug_class!($class) != 0 {
				use ::stdlib::Cow;
				x.message(cow!($msg));
			}
		}
	};
	($class:ident $obj:expr, $msgf:expr, $($args:tt)*) => {
		if let &Some((mask, ref x)) = &$obj {
			if mask & ::logging::DEBUG_MASK & debug_class!($class) != 0 {
				use ::stdlib::Cow;
				x.message(cow!($msgf, $($args)*));
			}
		}
	};
}

///Failure in TLS library.
///
///This lists all the failures that can happen in TLS handshake and operation.
#[derive(Clone,Debug)]
pub enum TlsFailure
{
	///Assertion failed!
	AssertionFailed(AssertFailed),

	///ACME validation challenge done.
	AcmeValidationDone,
	///ApplicationLayerProtocolNegotiation extension contained bad protocol.
	AlpnBadProtocol,
	///ALPN protocol empty.
	AlpnEmptyProtocol,
	///ALPN non UTF-8 protocol name.
	AlpnNonUtf8Protocol,
	///No overlap between client and server ALPN values.
	AlpnNoOverlap,
	///Tried to send application data after EOF.
	AppdataAfterEof,
	///Tried to send application data before finished.
	AppdataBeforeFinished,
	///ALPN bogus protocol.
	BogusAlpnProtocol,
	///Peer does not support uncompressed.
	BogusChCompressionMethodsTls13,
	///Bogus restart request.
	BogusHrrRestart,
	///Can't parse ApplicationLayerProtocolNegotiation extension.
	CantParseAlpn,
	///Can't parse TLS 1.2 certificate.
	CantParseCertTls12,
	///Can't parse TLS 1.3 certificate.
	CantParseCertTls13,
	///Error parsing ClientHello
	CantParseCh,
	///Can't parse ClientKeyExchange.
	CantParseCke,
	///Can't parse CertificateRequest.
	CantParseCr,
	///Can't parse CertificateStatus.
	CantParseCs,
	///Can't parse CertificateVerify.
	CantParseCv,
	///Can't parse EncryptedExtensions.
	CantParseEe,
	///Can't parse HelloRetryRequest.
	CantParseHrr,
	///Can't parse KeyShare extension.
	CantParseKeyShare,
	///Can't parse OCSP response.
	CantParseOcsp,
	///Can't parse RecordSizeLimit extension.
	CantParseRecSizeLim,
	///Can't parse ServerCertificateType extension.
	CantParseSCertType,
	///Can't parse SignedCertificateTimestamp extension.
	CantParseSct,
	///Can't parse ServerName extension.
	CantParseServerName,
	///Can't parse ServerHello.
	CantParseSh,
	///Can't parse SigatureAlgorithms extension.
	CantParseSigAlgo,
	///Can't parse ServerKeyExchange.
	CantParseSke,
	///Can't parse SupportedGroups extension.
	CantParseSupGrp,
	///Can't parse SupportedVersions extension.
	CantParseSupVer,
	///Certificate requires OCSP stapling.
	CertRequiresOcsp,
	///Certificate requires SCT stapling.
	CertRequiresSct,
	///ChangeCipherSpec in middle of handshake message.
	CcsInMiddleOfHandshakeMessage,
	///Attempted to restart handshake twice.
	ChDoubleRestart,
	///Incomplete client hello.
	ChIncomplete,
	///Client tried to offer ciphersuite 0.
	Ciphersuite0,
	///Client does not support uncompressed coding.
	ClientDoesNotSupportUncompressed,
	///CerficateStatus response empty.
	CsEmptyResponse,
	///CertificateVerify signature failed verification.
	CvSignatureFailed,
	///Record deprotection failed.
	DeprotectionFailed,
	///Downgrade attack detected.
	DowngradeAttackDetected,
	///End-Entity certificate empty.
	EeCertificateEmpty,
	///Empty handshake messages are not allowed.
	EmptyHandshakeMessage,
	///Finished MAC check failed.
	FinishedMacFailed,
	///Finished does not end last fragment it is in.
	FinishedNotAligned,
	///Handshake timed out.
	HandshakeTimedOut,
	///Expected Cookie extension content in HelloRetryRequest.
	HrrExpectedCookie,
	///Expected KeyShare extension content in HelloRetryRequest.
	HrrExpectedKeyShare,
	///Interlock signature algorithm.
	InterlockSignatureAlgorithm,
	///Invalid ChangeCipherSpec message.
	InvalidCcs,
	///Key Share extension is not allowed in TLS 1.2.
	KeyShareNotAllowedInTls12,
	///Key shares are not subset of supported groups.
	KeySharesNotSubsetSupGrp,
	///KeyUpdate does not end last fragment it is in.
	KeyUpdateNotAligned,
	///TLS 1.3 ClientHello lacks KeyShare extension.
	NoKeyshareInTls13,
	///No mutually supported certificate type.
	NoMutualCertificateType,
	///No mutually supported ciphersuites.
	NoMutualCiphersuites,
	///No mutually supported groups.
	NoMutualGroups,
	///No mutually supported TLS version.
	NoMutualTlsVersion,
	///Normal connection close.
	NormalClose,
	///ClientHello missing signature_algorithms.
	NoSignatureAlgorithms,
	///ClientHello missing supported_groups.
	NoSupportedGroups,
	///No trust anchors configured.
	NoTrustAnchors,
	///Certificate chain too incomplete to evaluate OCSP.
	OcspIncompleteCertificateChain,
	///Renegotiation attack detected!
	RenegotiationAttackDetected,
	///Server certificate chain empty.
	ServerChainEmpty,
	///ServerHello does not end last fragment it is in.
	ServerHelloNotAlignedTls13,
	///Server key not whitelisted (RPK).
	ServerKeyNotWhitelisted,
	///ServerName extension contained bad hostame.
	ServerNameBadHost,
	///ServerKeyExchange signature failed to verify.
	SkeSignatureFailed,
	///SSL3 clients not supported.
	Ssl3NotSupported,
	///TLS 1.0 clients not supported.
	Tls10NotSupported,
	///TLS 1.1 clients not supported.
	Tls11NotSupported,
	///TLS 1.3 random hack detected Downgrade attack!
	Tls13DowngradeAttack,
	///No key share in TLS 1.3 ServerHello.
	Tls13NoKeyShare,
	///TLS Client authentication not OK.
	TlsClientAuthNotOk,
	///TLS session has been torn down due to an error
	TlsSessionError,
	///Peer vulernable to renegotiation attack.
	VulernableRenego,
	///Peer vulernable to THS.
	VulernableTHS,
	///Client offers bad ciphers.
	ClientSupprotsBadCiphers,

	///Name reserved for ACME.
	AcmeReservedName(String),
	///Bad RecordSizeLimit extension value.
	BadRecSizeLim(usize),
	///Unsupported extension in certificate message.
	BogusCertExtension(u16),
	///Bogus TLS version in ClientHello.
	BogusChTlsVersion(u16),
	///Bogus OCSP response type.
	BogusCsOcspType(u8),
	///Bogus signature algorithm in CertificateVerify.
	BogusCvSignatureAlgo(u16),
	///Unsupported extension in EncryptedExtensions.
	BogusEeExtension(u16),
	///HelloRetryRequest requested share for bogus curve.
	BogusHrrCurve(u16),
	///Bogus group in KeyShare.
	BogusKeyShareGroup(u16),
	///HelloRetryRequest contains bogus TLS version.
	BogusHrrTlsVersion(u16),
	///ServerCertificateType has bogus type.
	BogusSCertType(u8),
	///Bogus ciphersuite in ServerHello.
	BogusShCiphersuite(u16),
	///Bogus compression in ServerHello.
	BogusShCompression(u8),
	///Bogus version in ServerHello.
	BogusShVersion(u16),
	///ServerKeyExchange has bogus curve.
	BogusSkeCurve(u16),
	///ServerKeyExchange has bogus curve type.
	BogusSkeCurveType(u8),
	///ServerKeyExchange has bogus signature algorithm.
	BogusSkeSignatureAlgo(u16),
	///Can't extract SCTs from TLS 1.2 OCSP.
	CantExtractSct12(OcspValidationError),
	///Can't extract SCTs from TLS 1.3 OCSP.
	CantExtractSct13(OcspValidationError),
	///Can't extract SPKI from certificate.
	CantExtractSpki(CertificateError),
	///Can't parse immediate issuer certificate.
	CantParseIssuerCertificate(CertificateError),
	///Certificate Validation failed.
	CertificateValidationFailed(CertificateValidationError),
	///Certificate intermediate empty.
	CertIntermediateEmpty,
	///Unsupported client version.
	ChUnsupportedClientVersion(u16),
	///CertificateRequest context must be empty.
	CrContextMustBeEmpty,
	///Duplicate extension in Certificate message.
	DuplicateCertExtension(u16),
	///Duplicate extension in Certificate Request.
	DuplicateCertReqExtension(u16),
	///Duplicate extension in ClientHello.
	DuplicateChExtension(u16),
	///Duplicate extension in EncryptedExtensions.
	DuplicateEeExtension(u16),
	///HelloRetryRequest contains duplicate extension.
	DuplicateHrrExtension(u16),
	///Key Share duplicate groups.
	DuplicateKeyShareGroup(u16),
	///Duplicate extension in ServerHello.
	DuplicateShExtension(u16),
	///Can't parse EE certificate.
	EECertParseError(CertificateError),
	///Certificate selection produced empty chain.
	EmptyCertChain(String),
	///We already sent share for this group.
	HrrAlreadyHadShare(u16),
	///HelloRetryRequest contains unsupported TLS version.
	HrrTlsVersionUnsupported(u16),
	///Error in handshake message buffer.
	HsMsgBuffer(HsBufferError),
	///Intelock certificate chain.
	InterlockCertificate(CertificateValidationError),
	///Intelocking on OCSP on server side.
	InterlockOcsp(OcspValidationError),
	///Invalid OCSP response (does not validate).
	InvalidOcsp(OcspValidationError),
	///Junk after end of extension in certificate message.
	JunkAfterCertExtension(u16),
	///Junk after extension in Certificate Request.
	JunkAfterCertReqExtension(u16),
	///Junk after extension in ClientHello.
	JunkAfterChExtension(u16),
	///Junk after extension in EncryptedExtensions.
	JunkAfterEeExtension(u16),
	///Junk after extension in HelloRetryRequest.
	JunkAfterHrrExtension(u16),
	///Junk after end of extension in ServerHello.
	JunkAfterShExtension(u16),
	///KeyUpdate has bad length.
	KeyUpdateBadLength(usize),
	///No certificate available for SNI name.
	NoCertificateAvailable(String),
	///Received record too big.
	RxRecordTooBig(usize),
	///ServerCertificateType not in first certificate.
	SCertTypeInNotFirstCert,
	///SCT validation failed.
	SctValidationFailed(SctError),
	///ServerHelloDone message is not empty.
	ServerHelloDoneNotEmpty(usize),
	///Selected cipher not valid for TLS 1.2.
	ShBadTls12Cipher(u16),
	///Selected cipher not valid for TLS 1.3.
	ShBadTls13Cipher(u16),
	///Unsupported TLS version in ServerHello.
	ShUnsupportedTlsVersion(u16),
	///Unexpected ChangeCipherSpec message.
	UnexpectedCcs(&'static str),
	///Unexpected record type.
	UnexpectedRtype(u8),
	///Unsupported TLS version.
	UnsupportedVersion(u16),

	///ALPN length1 not what it was expected.
	AlpnExpectedLen1Val(usize, usize),
	///ALPN length2 not what it was expected.
	AlpnExpectedLen2Val(usize, usize),
	///Unsupported extension in HelloRetryRequest.
	BogusHrrExtension(u16, u16),
	///Unsupported extension in ServerHello.
	BogusShExtension(u16, u16),
	///Ciphersuites disagree between HRR and SH.
	CiphersuiteDisagreement(Ciphersuite, Ciphersuite),
	///Error performing Diffie-Hellman key agreemment
	DiffieHellmanAgreeError(u16, KeyAgreementError),
	///KeyShare groups between HRR and KeyShare disagree.
	KeyShareGroupDisagreement(u16, u16),
	///OCSP response lives too long.
	OcspValidityTooLong(u64, u64),
	///Unexpected handshake message type.
	UnexpectedHandshakeMessage(u8, &'static str),
	#[allow(dead_code)]
	#[doc(hidden)]
	Hidden__
}

impl TlsFailure
{
	///Get TLS alert number that corresponds to the failure.
	///
	///Note: Some failures return alert #0, which is CLOSE_NOTIFY.
	pub fn get_alert_num(&self) -> u8
	{
		use self::TlsFailure::*;
		match self {
			&AssertionFailed(_) => tlserr!(INTERNAL_ERROR),

			//Yes, this has error code of CLOSE_NOTIFY, which causes EOF to be sent, instead of
			//abort.
			&AcmeValidationDone => tlserr!(CLOSE_NOTIFY),
			&AlpnBadProtocol => tlserr!(ILLEGAL_PARAMETER),
			&AlpnEmptyProtocol => tlserr!(DECODE_ERROR),
			&AlpnNonUtf8Protocol => tlserr!(ILLEGAL_PARAMETER),
			&AlpnNoOverlap => tlserr!(HANDSHAKE_FAILURE),
			&AppdataAfterEof => tlserr!(UNEXPECTED_MESSAGE),
			&AppdataBeforeFinished => tlserr!(UNEXPECTED_MESSAGE),
			&BogusAlpnProtocol => tlserr!(ILLEGAL_PARAMETER),
			&BogusChCompressionMethodsTls13 => tlserr!(DECODE_ERROR),
			&BogusHrrRestart => tlserr!(DECODE_ERROR),
			&CantParseAlpn => tlserr!(DECODE_ERROR),
			&CantParseCertTls12 => tlserr!(DECODE_ERROR),
			&CantParseCertTls13 => tlserr!(DECODE_ERROR),
			&CantParseCh => tlserr!(DECODE_ERROR),
			&CantParseCke => tlserr!(DECODE_ERROR),
			&CantParseCr => tlserr!(DECODE_ERROR),
			&CantParseCs => tlserr!(DECODE_ERROR),
			&CantParseCv => tlserr!(DECODE_ERROR),
			&CantParseEe => tlserr!(DECODE_ERROR),
			&CantParseHrr => tlserr!(DECODE_ERROR),
			&CantParseKeyShare => tlserr!(DECODE_ERROR),
			&CantParseOcsp => tlserr!(DECODE_ERROR),
			&CantParseRecSizeLim => tlserr!(DECODE_ERROR),
			&CantParseSCertType => tlserr!(DECODE_ERROR),
			&CantParseSct => tlserr!(DECODE_ERROR),
			&CantParseServerName => tlserr!(DECODE_ERROR),
			&CantParseSh => tlserr!(DECODE_ERROR),
			&CantParseSigAlgo => tlserr!(DECODE_ERROR),
			&CantParseSke => tlserr!(DECODE_ERROR),
			&CantParseSupGrp => tlserr!(DECODE_ERROR),
			&CantParseSupVer => tlserr!(DECODE_ERROR),
			&CcsInMiddleOfHandshakeMessage => tlserr!(UNEXPECTED_MESSAGE),
			&CertIntermediateEmpty => tlserr!(DECODE_ERROR),
			&CertRequiresOcsp => tlserr!(BAD_CERTIFICATE_STATUS_RESPONSE),
			&CertRequiresSct => tlserr!(BAD_CERTIFICATE_STATUS_RESPONSE),
			&ChDoubleRestart => tlserr!(HANDSHAKE_FAILURE),
			&ChIncomplete => tlserr!(UNEXPECTED_MESSAGE),
			&Ciphersuite0 => tlserr!(ILLEGAL_PARAMETER),
			&ClientDoesNotSupportUncompressed => tlserr!(ILLEGAL_PARAMETER),
			&CrContextMustBeEmpty => tlserr!(DECODE_ERROR),
			&CsEmptyResponse => tlserr!(DECODE_ERROR),
			&CvSignatureFailed => tlserr!(BAD_CERTIFICATE),
			&DeprotectionFailed => tlserr!(BAD_RECORD_MAC),
			&DowngradeAttackDetected => tlserr!(INAPPORIATE_FALLBACK),
			&EeCertificateEmpty => tlserr!(DECODE_ERROR),
			&EmptyHandshakeMessage => tlserr!(HANDSHAKE_FAILURE),
			&FinishedMacFailed => tlserr!(DECRYPT_ERROR),
			&FinishedNotAligned => tlserr!(UNEXPECTED_MESSAGE),
			&HandshakeTimedOut => tlserr!(USER_CANCELED),
			&HrrExpectedKeyShare => tlserr!(DECODE_ERROR),
			&HrrExpectedCookie => tlserr!(DECODE_ERROR),
			&InterlockSignatureAlgorithm => tlserr!(INTERNAL_ERROR),
			&InvalidCcs => tlserr!(UNEXPECTED_MESSAGE),
			&KeyShareNotAllowedInTls12 => tlserr!(PROTOCOL_VERSION),
			&KeySharesNotSubsetSupGrp => tlserr!(ILLEGAL_PARAMETER),
			&KeyUpdateNotAligned => tlserr!(UNEXPECTED_MESSAGE),
			&NoKeyshareInTls13 => tlserr!(MISSING_EXTENSION),
			&NoMutualCertificateType => tlserr!(INSUFFICIENT_SECURITY),
			&NoMutualCiphersuites => tlserr!(INSUFFICIENT_SECURITY),
			&NoMutualGroups => tlserr!(INSUFFICIENT_SECURITY),
			&NoMutualTlsVersion => tlserr!(PROTOCOL_VERSION),
			&NormalClose => tlserr!(CLOSE_NOTIFY),
			&NoSignatureAlgorithms => tlserr!(MISSING_EXTENSION),
			&NoSupportedGroups => tlserr!(MISSING_EXTENSION),
			&NoTrustAnchors => tlserr!(INTERNAL_ERROR),
			&OcspIncompleteCertificateChain => tlserr!(BAD_CERTIFICATE_STATUS_RESPONSE),
			&RenegotiationAttackDetected => tlserr!(HANDSHAKE_FAILURE),
			&ServerChainEmpty => tlserr!(NO_CERTIFICATE),
			&ServerHelloNotAlignedTls13 => tlserr!(UNEXPECTED_MESSAGE),
			&ServerKeyNotWhitelisted => tlserr!(UNKNOWN_CA),
			&ServerNameBadHost => tlserr!(ILLEGAL_PARAMETER),
			&SkeSignatureFailed => tlserr!(BAD_CERTIFICATE),
			&Ssl3NotSupported => tlserr!(PROTOCOL_VERSION),
			&Tls10NotSupported => tlserr!(PROTOCOL_VERSION),
			&Tls11NotSupported => tlserr!(PROTOCOL_VERSION),
			&Tls13DowngradeAttack => tlserr!(PROTOCOL_VERSION),
			&Tls13NoKeyShare => tlserr!(MISSING_EXTENSION),
			&TlsClientAuthNotOk => tlserr!(BAD_CERTIFICATE),
			&TlsSessionError => tlserr!(INTERNAL_ERROR),
			&VulernableRenego => tlserr!(INSUFFICIENT_SECURITY),
			&VulernableTHS => tlserr!(INSUFFICIENT_SECURITY),
			&ClientSupprotsBadCiphers => tlserr!(INSUFFICIENT_SECURITY),
			&AcmeReservedName(_) => tlserr!(UNRECOGNIZED_NAME),
			&BadRecSizeLim(_) => tlserr!(ILLEGAL_PARAMETER),
			&BogusCertExtension(_) => tlserr!(UNSUPPORTED_EXTENSION),
			&BogusChTlsVersion(_) => tlserr!(ILLEGAL_PARAMETER),
			&BogusCsOcspType(_) => tlserr!(ILLEGAL_PARAMETER),
			&BogusCvSignatureAlgo(_) => tlserr!(ILLEGAL_PARAMETER),
			&BogusEeExtension(_) => tlserr!(UNSUPPORTED_EXTENSION),
			&BogusHrrCurve(_) => tlserr!(ILLEGAL_PARAMETER),
			&BogusHrrTlsVersion(_) => tlserr!(ILLEGAL_PARAMETER),
			&BogusKeyShareGroup(_) =>  tlserr!(ILLEGAL_PARAMETER),
			&BogusSCertType(_) => tlserr!(ILLEGAL_PARAMETER),
			&BogusShCiphersuite(_) => tlserr!(ILLEGAL_PARAMETER),
			&BogusShCompression(_) => tlserr!(ILLEGAL_PARAMETER),
			&BogusShVersion(_) => tlserr!(ILLEGAL_PARAMETER),
			&BogusSkeCurve(_) => tlserr!(ILLEGAL_PARAMETER),
			&BogusSkeCurveType(_) => tlserr!(ILLEGAL_PARAMETER),
			&BogusSkeSignatureAlgo(_) => tlserr!(ILLEGAL_PARAMETER),
			&BogusHrrExtension(_, _) => tlserr!(UNSUPPORTED_EXTENSION),
			&BogusShExtension(_, _) => tlserr!(UNSUPPORTED_EXTENSION),
			&CantExtractSpki(_) => tlserr!(UNSUPPORTED_CERTIFICATE),
			&CantExtractSct12(_) => tlserr!(BAD_CERTIFICATE),
			&CantExtractSct13(_) => tlserr!(BAD_CERTIFICATE),
			&CantParseIssuerCertificate(_) => tlserr!(BAD_CERTIFICATE_STATUS_RESPONSE),
			&CertificateValidationFailed(ref x) => match x.problem_class() {
				MajorCertProblem::Bad => tlserr!(BAD_CERTIFICATE),
				MajorCertProblem::Unsupported => tlserr!(UNSUPPORTED_CERTIFICATE),
				MajorCertProblem::Expired => tlserr!(CERTIFICATE_EXPIRED),
				MajorCertProblem::Ocsp => tlserr!(BAD_CERTIFICATE_STATUS_RESPONSE),
				MajorCertProblem::Unspecified => tlserr!(CERTIFICATE_UNKNOWN),
				MajorCertProblem::UnknownCa => tlserr!(UNKNOWN_CA),
				MajorCertProblem::InternalError => tlserr!(INTERNAL_ERROR),
			},
			&ChUnsupportedClientVersion(_) => tlserr!(PROTOCOL_VERSION),
			&DuplicateCertExtension(_) => tlserr!(ILLEGAL_PARAMETER),
			&DuplicateCertReqExtension(_) => tlserr!(ILLEGAL_PARAMETER),
			&DuplicateChExtension(_) => tlserr!(ILLEGAL_PARAMETER),
			&DuplicateEeExtension(_) => tlserr!(ILLEGAL_PARAMETER),
			&DuplicateHrrExtension(_) => tlserr!(ILLEGAL_PARAMETER),
			&DuplicateKeyShareGroup(_) => tlserr!(ILLEGAL_PARAMETER),
			&DuplicateShExtension(_) => tlserr!(ILLEGAL_PARAMETER),
			&EECertParseError(_) => tlserr!(UNSUPPORTED_CERTIFICATE),
			&EmptyCertChain(_) => tlserr!(NO_CERTIFICATE),
			&HrrAlreadyHadShare(_) => tlserr!(ILLEGAL_PARAMETER),
			&HrrTlsVersionUnsupported(_) => tlserr!(PROTOCOL_VERSION),
			&HsMsgBuffer(ref x) => x.tls_code(),
			&InterlockCertificate(ref x) => match x.problem_class() {
				MajorCertProblem::Bad => tlserr!(BAD_CERTIFICATE),
				MajorCertProblem::Unsupported => tlserr!(UNSUPPORTED_CERTIFICATE),
				MajorCertProblem::Expired => tlserr!(CERTIFICATE_EXPIRED),
				MajorCertProblem::Ocsp => tlserr!(BAD_CERTIFICATE_STATUS_RESPONSE),
				MajorCertProblem::Unspecified => tlserr!(CERTIFICATE_UNKNOWN),
				MajorCertProblem::UnknownCa => tlserr!(UNKNOWN_CA),
				MajorCertProblem::InternalError => tlserr!(INTERNAL_ERROR),
			},
			&InterlockOcsp(_) => tlserr!(BAD_CERTIFICATE_STATUS_RESPONSE),
			&InvalidOcsp(OcspValidationError::CertificateRevoked) => tlserr!(CERTIFICATE_REVOKED),
			&InvalidOcsp(_) => tlserr!(BAD_CERTIFICATE_STATUS_RESPONSE),
			&JunkAfterCertExtension(_) => tlserr!(DECODE_ERROR),
			&JunkAfterCertReqExtension(_) => tlserr!(DECODE_ERROR),
			&JunkAfterChExtension(_) => tlserr!(DECODE_ERROR),
			&JunkAfterEeExtension(_) => tlserr!(DECODE_ERROR),
			&JunkAfterHrrExtension(_) => tlserr!(DECODE_ERROR),
			&JunkAfterShExtension(_) => tlserr!(DECODE_ERROR),
			&KeyUpdateBadLength(_) => tlserr!(UNEXPECTED_MESSAGE),
			&NoCertificateAvailable(_) => tlserr!(UNRECOGNIZED_NAME),
			&RxRecordTooBig(_) => tlserr!(RECORD_OVERFLOW),
			&SCertTypeInNotFirstCert => tlserr!(UNSUPPORTED_EXTENSION),
			&SctValidationFailed(_) => tlserr!(BAD_CERTIFICATE_STATUS_RESPONSE),
			&ServerHelloDoneNotEmpty(_) => tlserr!(UNEXPECTED_MESSAGE),
			&ShBadTls12Cipher(_) => tlserr!(ILLEGAL_PARAMETER),
			&ShBadTls13Cipher(_) => tlserr!(ILLEGAL_PARAMETER),
			&ShUnsupportedTlsVersion(_) => tlserr!(PROTOCOL_VERSION),
			&UnexpectedCcs(_) => tlserr!(UNEXPECTED_MESSAGE),
			&UnexpectedRtype(_) => tlserr!(UNEXPECTED_MESSAGE),
			&UnsupportedVersion(_) => tlserr!(PROTOCOL_VERSION),

			&AlpnExpectedLen1Val(_, _) => tlserr!(DECODE_ERROR),
			&AlpnExpectedLen2Val(_, _) => tlserr!(DECODE_ERROR),
			&CiphersuiteDisagreement(_,_) => tlserr!(ILLEGAL_PARAMETER),
			&DiffieHellmanAgreeError(_, _) => tlserr!(ILLEGAL_PARAMETER),
			&KeyShareGroupDisagreement(_, _) => tlserr!(ILLEGAL_PARAMETER),
			&OcspValidityTooLong(_, _) => tlserr!(BAD_CERTIFICATE_STATUS_RESPONSE),
			&UnexpectedHandshakeMessage(_, _) => tlserr!(UNEXPECTED_MESSAGE),

			&Hidden__ => tlserr!(INTERNAL_ERROR)
		}
	}
	///Get explanation text for an error.
	pub fn get_explanation(&self) -> Cow<'static, str>
	{
		use self::TlsFailure::*;
		match self {
			&AssertionFailed(ref x) => cow!("Assertion failed: {}", x),

			&AcmeValidationDone => Cow::Borrowed(self.get_description()),
			&AlpnBadProtocol => Cow::Borrowed(self.get_description()),
			&AlpnEmptyProtocol => Cow::Borrowed(self.get_description()),
			&AlpnNonUtf8Protocol => Cow::Borrowed(self.get_description()),
			&AlpnNoOverlap => Cow::Borrowed(self.get_description()),
			&AppdataAfterEof => Cow::Borrowed(self.get_description()),
			&AppdataBeforeFinished => Cow::Borrowed(self.get_description()),
			&BogusAlpnProtocol => Cow::Borrowed(self.get_description()),
			&BogusChCompressionMethodsTls13 => Cow::Borrowed(self.get_description()),
			&BogusHrrRestart => Cow::Borrowed(self.get_description()),
			&CantParseAlpn => Cow::Borrowed(self.get_description()),
			&CantParseCertTls12 => Cow::Borrowed(self.get_description()),
			&CantParseCertTls13 => Cow::Borrowed(self.get_description()),
			&CantParseCh => Cow::Borrowed(self.get_description()),
			&CantParseCke => Cow::Borrowed(self.get_description()),
			&CantParseCr => Cow::Borrowed(self.get_description()),
			&CantParseCs => Cow::Borrowed(self.get_description()),
			&CantParseCv => Cow::Borrowed(self.get_description()),
			&CantParseEe => Cow::Borrowed(self.get_description()),
			&CantParseHrr => Cow::Borrowed(self.get_description()),
			&CantParseKeyShare => Cow::Borrowed(self.get_description()),
			&CantParseOcsp => Cow::Borrowed(self.get_description()),
			&CantParseRecSizeLim => Cow::Borrowed(self.get_description()),
			&CantParseSCertType => Cow::Borrowed(self.get_description()),
			&CantParseSct => Cow::Borrowed(self.get_description()),
			&CantParseServerName => Cow::Borrowed(self.get_description()),
			&CantParseSh => Cow::Borrowed(self.get_description()),
			&CantParseSigAlgo => Cow::Borrowed(self.get_description()),
			&CantParseSke => Cow::Borrowed(self.get_description()),
			&CantParseSupGrp => Cow::Borrowed(self.get_description()),
			&CantParseSupVer => Cow::Borrowed(self.get_description()),
			&CcsInMiddleOfHandshakeMessage => Cow::Borrowed(self.get_description()),
			&CertIntermediateEmpty => Cow::Borrowed(self.get_description()),
			&CertRequiresOcsp => Cow::Borrowed(self.get_description()),
			&CertRequiresSct => Cow::Borrowed(self.get_description()),
			&ChDoubleRestart => Cow::Borrowed(self.get_description()),
			&ChIncomplete => Cow::Borrowed(self.get_description()),
			&Ciphersuite0 => Cow::Borrowed(self.get_description()),
			&ClientDoesNotSupportUncompressed => Cow::Borrowed(self.get_description()),
			&CrContextMustBeEmpty => Cow::Borrowed(self.get_description()),
			&CsEmptyResponse => Cow::Borrowed(self.get_description()),
			&CvSignatureFailed => Cow::Borrowed(self.get_description()),
			&DeprotectionFailed => Cow::Borrowed(self.get_description()),
			&DowngradeAttackDetected => Cow::Borrowed(self.get_description()),
			&EeCertificateEmpty => Cow::Borrowed(self.get_description()),
			&EmptyHandshakeMessage => Cow::Borrowed(self.get_description()),
			&FinishedMacFailed => Cow::Borrowed(self.get_description()),
			&FinishedNotAligned => Cow::Borrowed(self.get_description()),
			&HandshakeTimedOut => Cow::Borrowed(self.get_description()),
			&HrrExpectedCookie => Cow::Borrowed(self.get_description()),
			&HrrExpectedKeyShare => Cow::Borrowed(self.get_description()),
			&InterlockSignatureAlgorithm => Cow::Borrowed(self.get_description()),
			&InvalidCcs => Cow::Borrowed(self.get_description()),
			&KeyShareNotAllowedInTls12 => Cow::Borrowed(self.get_description()),
			&KeySharesNotSubsetSupGrp => Cow::Borrowed(self.get_description()),
			&KeyUpdateNotAligned => Cow::Borrowed(self.get_description()),
			&NoKeyshareInTls13 => Cow::Borrowed(self.get_description()),
			&NoMutualCertificateType => Cow::Borrowed(self.get_description()),
			&NoMutualCiphersuites => Cow::Borrowed(self.get_description()),
			&NoMutualGroups => Cow::Borrowed(self.get_description()),
			&NoMutualTlsVersion => Cow::Borrowed(self.get_description()),
			&NormalClose => Cow::Borrowed(self.get_description()),
			&NoSignatureAlgorithms => Cow::Borrowed(self.get_description()),
			&NoSupportedGroups => Cow::Borrowed(self.get_description()),
			&NoTrustAnchors => Cow::Borrowed(self.get_description()),
			&OcspIncompleteCertificateChain => Cow::Borrowed(self.get_description()),
			&RenegotiationAttackDetected => Cow::Borrowed(self.get_description()),
			&SCertTypeInNotFirstCert => Cow::Borrowed(self.get_description()),
			&ServerChainEmpty => Cow::Borrowed(self.get_description()),
			&ServerHelloNotAlignedTls13 => Cow::Borrowed(self.get_description()),
			&ServerKeyNotWhitelisted => Cow::Borrowed(self.get_description()),
			&ServerNameBadHost => Cow::Borrowed(self.get_description()),
			&SkeSignatureFailed => Cow::Borrowed(self.get_description()),
			&Ssl3NotSupported => Cow::Borrowed(self.get_description()),
			&Tls10NotSupported => Cow::Borrowed(self.get_description()),
			&Tls11NotSupported => Cow::Borrowed(self.get_description()),
			&Tls13DowngradeAttack => Cow::Borrowed(self.get_description()),
			&Tls13NoKeyShare => Cow::Borrowed(self.get_description()),
			&TlsClientAuthNotOk => Cow::Borrowed(self.get_description()),
			&TlsSessionError => Cow::Borrowed(self.get_description()),
			&VulernableRenego => Cow::Borrowed(self.get_description()),
			&VulernableTHS => Cow::Borrowed(self.get_description()),
			&ClientSupprotsBadCiphers => Cow::Borrowed(self.get_description()),

			&AcmeReservedName(ref x) => cow!("SNI value is reserved for ACME: {}", x),
			&BadRecSizeLim(x) => cow!("record_size_limit contains bad limit {}", x),
			&BogusCertExtension(x) => cow!("Certificate contains bogus extension {}", x),
			&BogusChTlsVersion(x) => cow!("ClientHello contains bogus TLS version {:04x}", x),
			&BogusCsOcspType(x) => cow!("CertificateStatus contains bogus response type {}", x),
			&BogusCvSignatureAlgo(x) => cow!("CertificateVerify contains bogus signature algorithm \
				{:04x}", x),
			&BogusEeExtension(x) => cow!("EncryptedExtensions contains bogus extension {}", x),
			&BogusHrrCurve(x) => cow!("HelloRetryRequest contains bogus share {}", x),
			&BogusHrrTlsVersion(x) => cow!("HelloRetryRequest contains bogus TLS version {:04x}", x),
			&BogusKeyShareGroup(x) =>  cow!("KeyShare contains bogus group #{}", x),
			&BogusSCertType(x) => cow!("server_certificate_type contains bogus certificate type {}", x),
			&BogusShCiphersuite(x) => cow!("ServerHello contains bogus ciphersuite {:04x}", x),
			&BogusShCompression(x) => cow!("ServerHello contains bogus compression algorithm {}", x),
			&BogusShVersion(x) => cow!("ServerHello contains bogus TLS version {:04x}", x),
			&BogusSkeCurve(x) => cow!("ServerKeyExchange contains bogus curve {}", x),
			&BogusSkeCurveType(x) => cow!("ServerKeyExchange contains bogus curve type {}", x),
			&BogusSkeSignatureAlgo(x) => cow!("ServerKeyExchange contains bogus signature algorithm \
				{:04x}", x),
			&CantExtractSct12(ref x) => cow!("Error extracting SCTs from certificate_status: {}", x),
			&CantExtractSct13(ref x) => cow!("Error extracting SCTs from status_request: {}", x),
			&CantExtractSpki(ref x) => cow!("Error extracting SPKI: {}", x),
			&CantParseIssuerCertificate(ref x) => cow!("Error parsing immediate issuer certificate: {}",
				x),
			&CertificateValidationFailed(ref x) => cow!("Certificate failed to validate: {}", x),
			&ChUnsupportedClientVersion(x) => cow!("ClientHello contains unsupported version TLS 1.{}",
				x),
			&DuplicateCertExtension(x) => cow!("Certificate contains extension {} twice", x),
			&DuplicateCertReqExtension(x) => cow!("CertificateRequest contains extension {} twice", x),
			&DuplicateChExtension(x) => cow!("ClientHello contains extension {} twice", x),
			&DuplicateEeExtension(x) => cow!("EncryptedExtensions contains extension {} twice", x),
			&DuplicateHrrExtension(x) => cow!("HelloRetryRequest contains extension {} twice", x),
			&DuplicateKeyShareGroup(x) => cow!("Keyshare contains group {} twice", x),
			&DuplicateShExtension(x) => cow!("ServerHello contains extension {} twice", x),
			&EECertParseError(ref x) => cow!("Error parsing EE certificate: {}", x),
			&EmptyCertChain(ref x) => cow!("Certificate selection resulted empty chain for {}", x),
			&HrrAlreadyHadShare(x) => cow!("HelloRetryRequest contains share {} already present", x),
			&HrrTlsVersionUnsupported(x) => cow!("HelloRetryRequest contains unsupported version \
				TLS 1.{}", x),
			&HsMsgBuffer(x) => cow!("Error extracting handshake message: {}", x),
			&InterlockCertificate(ref x) => cow!("Interlock certificate chain: {}", x),
			&InterlockOcsp(ref x) => cow!("Interlock OCSP response: {}", x),
			&InvalidOcsp(ref x) => cow!("OCSP response failed to validate: {}", x),
			&JunkAfterCertExtension(x) => cow!("Certificate contains junk after extension {}", x),
			&JunkAfterCertReqExtension(x) => cow!("CertificateRequest contains junk after extension {}",
				x),
			&JunkAfterChExtension(x) => cow!("ClientHello contains junk after extension {}", x),
			&JunkAfterEeExtension(x) => cow!("EncryptedExtensions contains junk after extension {}", x),
			&JunkAfterHrrExtension(x) => cow!("HelloRetryRequest contains junk after extension {}", x),
			&JunkAfterShExtension(x) => cow!("ServerHello contains junk after extension {}", x),
			&KeyUpdateBadLength(x) => cow!("KeyUpdate is not length=1 (len={})", x),
			&NoCertificateAvailable(ref x) => cow!("No certificate available for {}", x),
			&RxRecordTooBig(x) => cow!("Received record too big ({})", x),
			&SctValidationFailed(ref x) => cow!("SCTs failed to validate: {}", x),
			&ServerHelloDoneNotEmpty(x) => cow!("ServerHelloDone is not empty (len={})", x),
			&ShBadTls12Cipher(x) => cow!("ServerHello contains ciphersuite {:04x} not compatible with \
				TLS 1.2", x),
			&ShBadTls13Cipher(x) => cow!("ServerHello contains ciphersuite {:04x} not compatible with \
				TLS 1.3", x),
			&ShUnsupportedTlsVersion(x) => cow!("ServerHello contains unsupported version TLS 1.{}", x),
			&UnexpectedCcs(x) => cow!("ChangeCipherSpec received in unexpected state {}", x),
			&UnexpectedRtype(x) => cow!("Received unexpected record type {}", x),
			&UnsupportedVersion(x) => cow!("ServerHello contains unsupported TLS version code {:x}", x),

			&AlpnExpectedLen1Val(x, y) => cow!("ALPN contains length1={} (expected {})", y, x),
			&AlpnExpectedLen2Val(x, y) => cow!("ALPN contains length2={} (expected {})", y, x),
			&CiphersuiteDisagreement(x,y) => cow!("ServerHello contains ciphersuite {:?} different \
				from HelloRetryRequest({:?})", y, x),
			&DiffieHellmanAgreeError(x, ref y) => cow!("Error doing Diffie-Hellman key agreement \
				(curve #{0}): {1}", x, y),
			&BogusHrrExtension(x, y) => cow!("HelloRetryRequest contains bogus extension {} (TLS 1.{})",
				x, y),
			&KeyShareGroupDisagreement(x, y) => cow!("ServerHello contains group {} different from\
				HelloRetryRequest({})", y, x),
			&OcspValidityTooLong(x, y) => cow!("OCSP staple lifetime too long ({}s, limit is {}s)", x,
				y),
			&BogusShExtension(x, y) => cow!("ServerHello contains bogus extension {} (TLS 1.{})", x, y),
			&UnexpectedHandshakeMessage(x, y) => cow!("Received unexpected handshake type #{} for \
				state {}", x, y),

			&Hidden__ => Cow::Borrowed(self.get_description()),
		}
	}
	fn get_description(&self) -> &'static str
	{
		use self::TlsFailure::*;
		match self {
			&AssertionFailed(_) => "Internal error, assertion failed",

			&AcmeValidationDone => "ACME validation handshake done",
			&AlpnBadProtocol => "ALPN contains bad protocol",
			&AlpnEmptyProtocol => "ALPN contains empty protocol",
			&AlpnNonUtf8Protocol => "ALPN contains non-UTF8 protocol",
			&AlpnNoOverlap => "No mutual application protocols",
			&AppdataAfterEof => "Received application data after EOF",
			&AppdataBeforeFinished => "Received application data before Finished",
			&BogusAlpnProtocol => "ALPN contains bogus protocol",
			&BogusChCompressionMethodsTls13 => "ClientHello(TLS 1.3) has bogus compression methods",
			&BogusHrrRestart => "HelloRetryRequest contains no reason for restart",
			&CantParseAlpn => "ALPN can't be parsed",
			&CantParseCertTls12 => "Certificate(TLS 1.2) can't be parsed",
			&CantParseCertTls13 => "Certificate(TLS 1.3) can't be pared",
			&CantParseCh => "ClientHello Can't be parsed",
			&CantParseCke => "ClientKeyExchange can't be parsed",
			&CantParseCr => "CertificateRequest can't be parsed",
			&CantParseCs => "CertificateStatus(TLS 1.2) can't be parsed",
			&CantParseCv => "CertificateVerify can't be parsed",
			&CantParseEe => "EncryptedExtensions can't be parsed",
			&CantParseHrr => "HelloRetryRequest can't be parsed",
			&CantParseKeyShare => "KeyShare can't be parsed",
			&CantParseOcsp => "CertificateStatus(TLS 1.3) can't be parsed",
			&CantParseRecSizeLim => "RecordSizeLimit can't be parsed",
			&CantParseSCertType => "ServerCertificateType can't be parsed",
			&CantParseSct => "SignedCertificateTimestamp can't be parsed",
			&CantParseServerName => "ServerName can't be parsed",
			&CantParseSh => "ServerHello can't be parsed",
			&CantParseSigAlgo => "SignatureAlgorithms can't be parsed",
			&CantParseSke => "ServerKeyExchange can't be parsed",
			&CantParseSupGrp => "SupportedGroups can't be parsed",
			&CantParseSupVer => "SupportedVersions can't be parsed",
			&CcsInMiddleOfHandshakeMessage => "ChangeCipherSpec interrupted a handshake message",
			&CertIntermediateEmpty => "Certificate contains empty intermediate certificate",
			&CertRequiresOcsp => "Certificate requires OCSP stapling",
			&CertRequiresSct => "Certificate requires SCT stapling",
			&ChDoubleRestart => "Double restart in TLS 1.3",
			&ChIncomplete => "ClientHello is incomplete",
			&Ciphersuite0 => "ClientHello contains illegal ciphersuite 0",
			&ClientDoesNotSupportUncompressed => "Client does not support uncompressed coding",
			&CrContextMustBeEmpty => "CertificateRequest context must be empty",
			&CsEmptyResponse => "Empty OCSP response",
			&CvSignatureFailed => "CertificateVerify contains bad signature",
			&DeprotectionFailed => "Error deprotecting record",
			&DowngradeAttackDetected => "DOWNGRADE ATTACK DETECTED!!!",
			&EeCertificateEmpty => "EE certificate is empty",
			&EmptyHandshakeMessage => "Empty Handshake message",
			&FinishedMacFailed => "Finished contains bad MAC",
			&FinishedNotAligned => "Finished does not end at record boundary",
			&HandshakeTimedOut => "Handshake timed out",
			&HrrExpectedCookie => "HelloRetryRequest expected Cookie, got end of extension",
			&HrrExpectedKeyShare => "HelloRetryRequest expected KeyShare, got end of extension",
			&InterlockSignatureAlgorithm => "Interlock signature algorithm",
			&InvalidCcs => "ChangeCipherSpec invalid ",
			&KeyShareNotAllowedInTls12 => "KeyShare is not allowed in TLS 1.2",
			&KeySharesNotSubsetSupGrp => "KeyShare is not subset of SupportedGroups",
			&KeyUpdateNotAligned => "KeyUpdate does not end at record boundary",
			&NoKeyshareInTls13 => "KeyShare is required for TLS 1.3",
			&NoMutualCertificateType => "No certificate type overlap between client and server",
			&NoMutualCiphersuites => "No ciphersuite overlap between client and server",
			&NoMutualGroups => "No group overlap between client and server",
			&NoMutualTlsVersion => "No TLS version overlap between client and server",
			&NormalClose => "Normal close",
			&NoSignatureAlgorithms => "SignatureAlgorithms is required",
			&NoSupportedGroups => "SupportedGroups is required",
			&NoTrustAnchors => "No trust anchors configured",
			&OcspIncompleteCertificateChain => "Incomplete certificate chain",
			&RenegotiationAttackDetected => "RENEGOTIATION ATTACK DETECTED!!!",
			&SCertTypeInNotFirstCert => "ServerCertificateType only allowed in first certificate",
			&ServerChainEmpty => "Server did not send any certificates",
			&ServerHelloNotAlignedTls13 => "ServerHello(TLS 1.3) does not end at record boundary",
			&ServerKeyNotWhitelisted => "Server key not on whitelist",
			&ServerNameBadHost => "ServerName bad hostname sent by client",
			&SkeSignatureFailed => "ServerKeyExchange contains bad signature",
			&Ssl3NotSupported => "SSL v3 is not supported",
			&Tls10NotSupported => "TLS 1.0 is not supported",
			&Tls11NotSupported => "TLS 1.1 is not supported",
			&Tls13DowngradeAttack => "DOWNGRADE ATTACK DETECTED!!!",
			&Tls13NoKeyShare => "TLS 1.3 chosen but no key share sent",
			&TlsClientAuthNotOk => "TLS client authentication not OK",
			&TlsSessionError => "TLS session has been torn down due to an error",
			&VulernableRenego => "Remote end is vulernable to renegotiation attack",
			&VulernableTHS => "Remote end is vulernable to triple handshake attack",
			&ClientSupprotsBadCiphers => "Client offered bad ciphers",

			&AcmeReservedName(_) => "SNI value is reserved for ACME",
			&BadRecSizeLim(_) => "record_size_limit contains bad limit",
			&BogusCertExtension(_) => "Certificate contains bogus extension",
			&BogusChTlsVersion(_) => "ClientHello contains bogus TLS version",
			&BogusCsOcspType(_) => "CertificateStatus contains bogus response type",
			&BogusCvSignatureAlgo(_) => "CertificateVerify contains bogus signature algorithm",
			&BogusEeExtension(_) => "EncryptedExtensions contains bogus extension",
			&BogusHrrCurve(_) => "HelloRetryRequest contains bogus share",
			&BogusHrrTlsVersion(_) => "HelloRetryRequest contains bogus TLS version",
			&BogusKeyShareGroup(_) =>  "KeyShare contains bogus group",
			&BogusSCertType(_) => "server_certificate_type contains bogus certificate type",
			&BogusShCiphersuite(_) => "ServerHello contains bogus ciphersuite",
			&BogusShCompression(_) => "ServerHello contains bogus compression algorithm",
			&BogusShVersion(_) => "ServerHello contains bogus TLS version",
			&BogusSkeCurveType(_) => "ServerKeyExchange contains bogus curve type",
			&BogusSkeCurve(_) => "ServerKeyExchange contains bogus curve",
			&BogusSkeSignatureAlgo(_) => "ServerKeyExchange contains bogus signature algorithm",
			&CantExtractSct12(_) => "Error extracting SCTs from certificate_status",
			&CantExtractSct13(_) => "Erfror extracting SCTs from status_request",
			&CantExtractSpki(_) => "Error extracting SPKI",
			&CantParseIssuerCertificate(_) => "Error parsing immediate issuer certificate",
			&CertificateValidationFailed(_) => "Certificate failed to validate",
			&ChUnsupportedClientVersion(_) => "ClientHello contains unsupported version",
			&DuplicateCertExtension(_) => "Certificate contains extension twice",
			&DuplicateCertReqExtension(_) => "CertificateRequest contains extension twice",
			&DuplicateChExtension(_) => "ClientHello contains extension twice",
			&DuplicateEeExtension(_) => "EncryptedExtensions contains extension twice",
			&DuplicateHrrExtension(_) => "HelloRetryRequest contains extension twice",
			&DuplicateKeyShareGroup(_) => "Keyshare contains group twice",
			&DuplicateShExtension(_) => "ServerHello contains extension twice",
			&EECertParseError(_) => "Error parsing EE certificate",
			&EmptyCertChain(_) => "Certificate selection resulted empty chain",
			&HrrAlreadyHadShare(_) => "HelloRetryRequest contains share already present",
			&HrrTlsVersionUnsupported(_) => "HelloRetryRequest contains unsupported version",
			&HsMsgBuffer(_) => "Error extracting handshake message",
			&InterlockCertificate(_) => "Interlock certificate chain",
			&InterlockOcsp(_) => "Interlock OCSP response",
			&InvalidOcsp(_) => "OCSP response failed to validate",
			&JunkAfterCertExtension(_) => "Certificate contains junk after extension",
			&JunkAfterCertReqExtension(_) => "CertificateRequest contains junk after extension",
			&JunkAfterChExtension(_) => "ClientHello contains junk after extension",
			&JunkAfterEeExtension(_) => "EncryptedExtensions contains junk after extension",
			&JunkAfterHrrExtension(_) => "HelloRetryRequest contains junk after extension",
			&JunkAfterShExtension(_) => "ServerHello contains junk after extension",
			&KeyUpdateBadLength(_) => "KeyUpdate is not length=1",
			&NoCertificateAvailable(_) => "No certificate available",
			&RxRecordTooBig(_) => "Received record too big",
			&SctValidationFailed(_) => "SCTs failed to validate",
			&ServerHelloDoneNotEmpty(_) => "ServerHelloDone is not empty",
			&ShBadTls12Cipher(_) => "ServerHello contains ciphersuite not compatible with TLS 1.2",
			&ShBadTls13Cipher(_) => "ServerHello contains ciphersuite not compatible with TLS 1.3",
			&ShUnsupportedTlsVersion(_) => "ServerHello contains unsupported version",
			&UnexpectedCcs(_) => "ChangeCipherSpec received in unexpected state",
			&UnexpectedRtype(_) => "Received unexpected record type",
			&UnsupportedVersion(_) => "ServerHello contains unsupported TLS version code",

			&AlpnExpectedLen1Val(_, _) => "ALPN length1 wrong",
			&AlpnExpectedLen2Val(_, _) => "ALPN length2 wrong",
			&BogusHrrExtension(_, _) => "Unsupported extension in HelloRetryRequest",
			&BogusShExtension(_, _) => "Unsupported extension in ServerHello",
			&CiphersuiteDisagreement(_, _) => "Ciphersuites in HRR and SH disagree",
			&DiffieHellmanAgreeError(_, KeyAgreementError::PeerPublicKeyInvalid) => "Invalid peer \
				public key in Diffie-Hellman",
			&DiffieHellmanAgreeError(_, KeyAgreementError::PeerPublicKeyLengthInvalid) => "Invalid \
				peer public key in Diffie-Hellman",
			&DiffieHellmanAgreeError(_, _) => "Internal error doing Diffie-Hellman key exchange",
			&KeyShareGroupDisagreement(_, _) => "Groups in HRR and SH disagree",
			&OcspValidityTooLong(_, _) => "OCSP staple lifetime too long",
			&UnexpectedHandshakeMessage(_, _) => "Unexpected handshake message",

			&Hidden__ => "Hidden__"
		}
	}
}

impl Display for TlsFailure
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		fmt.write_str(self.get_explanation().as_ref())
	}
}

impl From<AssertFailed> for TlsFailure
{
	fn from(x: AssertFailed) -> TlsFailure
	{
		TlsFailure::AssertionFailed(x)
	}
}

impl Error for TlsFailure
{
	fn description(&self) -> &str
	{
		self.get_description()
	}
	fn cause(&self) -> Option<&Error>
	{
		use self::TlsFailure::*;
		match self {
			&AssertionFailed(ref x) => Some(x),
			_ => None
		}
	}
}
