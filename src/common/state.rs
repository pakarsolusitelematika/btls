///The direction next handshake message will go.
#[derive(Debug,Copy,Clone,PartialEq,Eq)]
pub enum HandshakeDirection
{
	///The next handshake message will be from server to client.
	ToClient,
	///The next handshake message will be from client to server.
	ToServer,
	///The next handshake message has unknown direction, or there won't be more handshake messages.
	Neither,
}

///The state of handshake.
#[derive(Debug,Copy,Clone,PartialEq,Eq)]
pub enum HandshakeState
{
	///Next, client sends ClientHello to server.
	ClientHello,
	///Next, the server will send HelloRestartRequest to client (this state is only valid on server and
	///for TLS 1.3).
	HelloRestartRequest,
	///Next, client retries sending ClientHello to server (TLS 1.3 only).
	RetryClientHello,
	///Next, server sends ServerHello to client.
	ServerHello,
	///The client is waiting for ServerHello in response to retry from the server (TLS 1.3 only, only valid
	///on client).
	ServerHelloWaitAgain,		//ServerHello (client only)
	///Next, the server will send TLS 1.2 Certificate message (TLS 1.2 only).
	Tls12Certificate,
	///Next, the server will send TLS 1.2 ServerKeyExchange message (TLS 1.2 only). Also, preceeding that message
	///may be CertificateStatus message from the server.
	Tls12ServerKeyExchange,
	///Next, the server will send TLS 1.2 ServerHelloDone message (TLS 1.2 only). Also, preceeding that message
	///may be CertificateRequest message from the server (on client only).
	Tls12ServerHelloDone,
	///Next, the client will send TLS 1.2 Certificate message NAKing authentication to the server (TLS 1.2 only,
	///only valid on client).
	Tls12ClientCertificate,		//Client only
	///Next, the client will send TLS 1.2 ClientKeyExchange message to the server (TLS 1.2 only).
	Tls12ClientKeyExchange,
	///Next, the client will send TLS 1.2 ChangeCipherSpec message to the server (TLS 1.2 only).
	Tls12ClientChangeCipherSpec,
	///Next, the client will send TLS 1.2 Finished message to the server (TLS 1.2 only).
	Tls12ClientFinished,
	///Next, the server will send TLS 1.2 ChangeCipherSpec message to the client (TLS 1.2 only).
	Tls12ServerChangeCipherSpec,
	///Next, the server will send TLS 1.2 Finished message to the client (TLS 1.2 only).
	Tls12ServerFinished,
	///TLS 1.2 connection is up and running.
	Tls12Showtime,
	///Next, the server will send TLS 1.3 EncryptedExtensions message to the client (TLS 1.3 only).
	Tls13EncryptedExtensions,
	///Next, the server will send TLS 1.3 Certificate message to the client (TLS 1.3 only).
	Tls13Certificate,		//CertificateRequest (client only)
	///Next, the server will send TLS 1.3 CertificateVerify message to the client (TLS 1.3 only).
	Tls13CertificateVerify,
	///Next, the server will send TLS 1.3 Finished message to the client (TLS 1.3 only).
	Tls13ServerFinished,
	///Next, the client will send TLS 1.3 Certificate message, NAKing authentication to the server (TLS 1.3 only,
	///only valid on client).
	Tls13ClientCertificate,
	///Next, the client will send TLS 1.3 Finished to the server (TLS 1.3 only)
	Tls13ClientFinished,
	///TLS 1.3 connection is up and running.
	Tls13Showtime,
	///Handshake has failed.
	Error,
	#[doc(hidden)]
	Hidden__
}

impl HandshakeState
{
	///Get the direction of next handshake message.
	///
	///# Parameters:
	///
	/// * `self`: The state to get information about.
	///
	///# Returns:
	///
	/// * The direction of next handshake message.
	pub fn get_direction(&self) -> HandshakeDirection
	{
		match self {
			&HandshakeState::ClientHello => HandshakeDirection::ToServer,
			&HandshakeState::HelloRestartRequest => HandshakeDirection::ToClient,
			&HandshakeState::RetryClientHello => HandshakeDirection::ToServer,
			&HandshakeState::ServerHello => HandshakeDirection::ToClient,
			&HandshakeState::ServerHelloWaitAgain => HandshakeDirection::ToClient,
			&HandshakeState::Tls12Certificate => HandshakeDirection::ToClient,
			&HandshakeState::Tls12ServerKeyExchange => HandshakeDirection::ToClient,
			&HandshakeState::Tls12ServerHelloDone => HandshakeDirection::ToClient,
			&HandshakeState::Tls12ClientCertificate => HandshakeDirection::ToServer,
			&HandshakeState::Tls12ClientKeyExchange => HandshakeDirection::ToServer,
			&HandshakeState::Tls12ClientChangeCipherSpec => HandshakeDirection::ToServer,
			&HandshakeState::Tls12ClientFinished => HandshakeDirection::ToServer,
			&HandshakeState::Tls12ServerChangeCipherSpec => HandshakeDirection::ToClient,
			&HandshakeState::Tls12ServerFinished => HandshakeDirection::ToClient,
			&HandshakeState::Tls12Showtime => HandshakeDirection::Neither,
			&HandshakeState::Tls13EncryptedExtensions => HandshakeDirection::ToClient,
			&HandshakeState::Tls13Certificate => HandshakeDirection::ToClient,
			&HandshakeState::Tls13CertificateVerify => HandshakeDirection::ToClient,
			&HandshakeState::Tls13ServerFinished => HandshakeDirection::ToClient,
			&HandshakeState::Tls13ClientCertificate => HandshakeDirection::ToServer,
			&HandshakeState::Tls13ClientFinished => HandshakeDirection::ToServer,
			&HandshakeState::Tls13Showtime => HandshakeDirection::Neither,
			&HandshakeState::Error => HandshakeDirection::Neither,
			&HandshakeState::Hidden__ => HandshakeDirection::Neither,
		}
	}
	///Get name.
	pub fn name(&self) -> &'static str
	{
		match self {
			&HandshakeState::ClientHello => "ClientHello",
			&HandshakeState::HelloRestartRequest => "HelloRestartRequest",
			&HandshakeState::RetryClientHello => "RetryClientHello",
			&HandshakeState::ServerHello => "ServerHello",
			&HandshakeState::ServerHelloWaitAgain => "ServerHelloWaitAgain",
			&HandshakeState::Tls12Certificate => "Tls12Certificate",
			&HandshakeState::Tls12ServerKeyExchange => "Tls12ServerKeyExchange",
			&HandshakeState::Tls12ServerHelloDone => "Tls12ServerHelloDone",
			&HandshakeState::Tls12ClientCertificate => "Tls12ClientCertificate",
			&HandshakeState::Tls12ClientKeyExchange => "Tls12ClientKeyExchange",
			&HandshakeState::Tls12ClientChangeCipherSpec => "Tls12ClientChangeCipherSpec",
			&HandshakeState::Tls12ClientFinished => "Tls12ClientFinished",
			&HandshakeState::Tls12ServerChangeCipherSpec => "Tls12ServerChangeCipherSpec",
			&HandshakeState::Tls12ServerFinished => "Tls12ServerFinished",
			&HandshakeState::Tls12Showtime => "Tls12Showtime",
			&HandshakeState::Tls13EncryptedExtensions => "Tls13EncryptedExtensions",
			&HandshakeState::Tls13Certificate => "Tls13Certificate",
			&HandshakeState::Tls13CertificateVerify => "Tls13CertificateVerify",
			&HandshakeState::Tls13ServerFinished => "Tls13ServerFinished",
			&HandshakeState::Tls13ClientCertificate => "Tls13ClientCertificate",
			&HandshakeState::Tls13ClientFinished => "Tls13ClientFinished",
			&HandshakeState::Tls13Showtime => "Tls13Showtime",
			&HandshakeState::Error => "Error",
			&HandshakeState::Hidden__ => "Hidden__",
		}
	}
}
