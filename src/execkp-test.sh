#!/bin/bash

if [ "$1" = "--algorithm=0401" ]; then
	openssl dgst -sha256 -sign src/execkp-test.pem || exit 1
	exit 0
fi
if [ "$1" = "--algorithm=0501" ]; then
	openssl dgst -sha384 -sign src/execkp-test.pem || exit 1
	exit 0
fi
if [ "$1" = "--algorithm=0601" ]; then
	openssl dgst -sha512 -sign src/execkp-test.pem || exit 1
	exit 0
fi
if [ "x$1" = "x" ]; then
	echo -n -e "\x00\x03\x04\x01\x05\x01\x06\x01"
	openssl rsa -in src/execkp-test.pem -outform der -pubout 2>/dev/null || exit 1
	exit 0
fi
echo "Bad argument"
exit 1
