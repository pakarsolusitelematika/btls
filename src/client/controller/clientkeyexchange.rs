use super::HandshakeState2;
use ::client::controller::finished::Tls12ClientFinishedFields;
use ::common::{Debugger, emit_handshake, HandshakeHash, HSTYPE_CERTIFICATE, HSTYPE_CLIENT_KEY_EXCHANGE,
	TLS_LEN_TLS12_PUBKEY, TlsFailure, write_sub_fn};
use ::record::{SessionBase, Tls12MasterSecret, Tls12PremasterSecret};
use ::stdlib::Vec;

use btls_aux_dhf::DhfPublicKey;
use btls_aux_serialization::Sink;

pub struct Tls12ClientKeyExchangeFields
{
	pub pubkey: DhfPublicKey,			//Public key.
	pub sig_verified: bool,				//Exchange Signature verified.
	pub bypass_certificate_validation: bool,	//Certificate validation was bypassed.
	pub premaster_secret: Tls12PremasterSecret,	//Premaster secret.
	pub client_random: [u8; 32],			//The client random.
	pub server_random: [u8; 32],			//The server random.
	pub hs_hash: HandshakeHash,
}

impl Tls12ClientKeyExchangeFields
{
	//Transmit a certificate message.
	pub fn tx_certificate(mut self, base: &mut SessionBase, debug: Debugger, state: &str) ->
		Result<HandshakeState2, TlsFailure>
	{
		let mut content = Vec::new();
		//Just put an empty certificate list (we always refuse authentication).
		try_buffer!(content.write_u24(0));

		emit_handshake(debug, HSTYPE_CERTIFICATE, &content, base, &mut self.hs_hash, state)?;
		Ok(HandshakeState2::Tls12ClientKeyExchange(self))
	}
	//Transmit a ClientKeyExchange message.
	pub fn tx_client_key_exchange(self, base: &mut SessionBase, debug: Debugger, state: &str) ->
		Result<HandshakeState2, TlsFailure>
	{
		let pubkey = self.pubkey;
		let mut hs_hash = self.hs_hash;

		let mut content = Vec::new();
		write_sub_fn(&mut content, "ECDH public key", TLS_LEN_TLS12_PUBKEY, |x|{
			try_buffer!(x.write_slice(pubkey.as_ref()));
			Ok(())
		})?;

		emit_handshake(debug.clone(), HSTYPE_CLIENT_KEY_EXCHANGE, &content, base, &mut hs_hash,
			state)?;
		let master_secret = Tls12MasterSecret::new(self.premaster_secret, &self.client_random,
			&self.server_random, hs_hash.checkpoint()?, debug.clone())?;
		Ok(HandshakeState2::Tls12ClientFinished(Tls12ClientFinishedFields {
			sig_verified: self.sig_verified,
			bypass_certificate_validation: self.bypass_certificate_validation,
			master_secret: master_secret,
			hs_hash: hs_hash,
		}))
	}
}
