use super::ClientConfiguration;
use ::TlsConnectionInfo;
use ::certificate::CFLAG_UNKNOWN;
use ::client_certificates::HostSpecificPin;
use ::common::{CryptoTracker, EarlyHandshakeRep, HandshakeMessage, HSTYPE_CERTIFICATE, HSTYPE_CERTIFICATE_REQUEST,
	HSTYPE_CERTIFICATE_STATUS, HSTYPE_CERTIFICATE_VERIFY, HSTYPE_ENCRYPTED_EXTENSIONS, HSTYPE_FINISHED,
	HSTYPE_HELLO_RETRY_REQUEST, HSTYPE_KEY_UPDATE, HSTYPE_SERVER_HELLO, HSTYPE_SERVER_HELLO_DONE,
	HSTYPE_SERVER_KEY_EXCHANGE, HSTYPE_SESSION_TICKET, NamesContainer, PROTO_CCS, PROTO_HANDSHAKE,
	TlsFailure};
use ::features::*;
use ::logging::{bytes_as_hex_block, explain_hs_type, Logging};
use ::messages::{Tls12Certificate, Tls13Certificate, Tls13CertificateRequest,
	TlsCertificateStatus, TlsCertificateVerify, TlsEncryptedExtensions, TlsHelloRetryRequest, TlsKeyUpdate,
	TlsServerHello, TlsServerKeyExchange};
use ::record::{SessionBase, SessionController, Tls13MasterSecret, Tls13TrafficSecretIn, Tls13TrafficSecretOut};
use ::server_certificates::CertificateSigner;
use ::stdlib::{Arc, Box, String, swap, ToOwned, Vec};
use ::utils::HsBuffer;

use btls_aux_dhf::{Dhf, DiffieHellmanKey};
use btls_aux_futures::FutureReceiver;
use btls_aux_random::secure_random;


//Macro because of lifetime rules.
macro_rules! grab_issuer_key
{
	($icerts:expr, $name:expr, $talist:expr) => {
		match ($icerts.get(0), $name) {
			(Some(x), _) => Some(extract_spki(x).map_err(|x|TlsFailure::CantParseIssuerCertificate(x))?),
			//Grab this from the trustpile.
			(None, Some(name2)) => $talist.get(name2.0).and_then(|x|x.get_spki()),
			//RPKs can't have OCSP responses.
			(None, None) => None,
		};
	}
}

mod certificate;
use self::certificate::{Tls12CertificateFields, Tls13CertificateFields, Tls13CertificateVerifyFields,
	Tls13ClientCertificateFields, Tls13ClientCertificateVerifyFields};
mod clienthello;
use self::clienthello::ClientHelloFields;
mod clientkeyexchange;
use self::clientkeyexchange::Tls12ClientKeyExchangeFields;
mod encryptedextensions;
use self::encryptedextensions::Tls13EncryptedExtensionsFields;
mod finished;
use self::finished::{Tls12ClientFinishedFields, Tls12ServerChangeCipherSpecFields, Tls12ServerFinishedFields,
	Tls13ClientFinishedFields, Tls13ServerFinishedFields};
mod serverhello;
use self::serverhello::ServerHelloFields;
mod serverhellodone;
use self::serverhellodone::Tls12ServerHelloDoneFields;
mod serverkeyexchange;
use self::serverkeyexchange::Tls12ServerKeyExchangeFields;

pub struct Tls13HsSecrets
{
	server_hs_write: Tls13TrafficSecretIn,
	client_hs_write: Tls13TrafficSecretOut,
	master_secret: Tls13MasterSecret,
}

pub struct Tls13C2ndFSecrets
{
	server_app_write: Tls13TrafficSecretIn,
	client_app_write: Tls13TrafficSecretOut,
	client_hs_write: Tls13TrafficSecretOut,
}

pub struct TlsCVInfo
{
	requested_sni: String,				//The SNI that has been requested.
	pins: Vec<HostSpecificPin>,			//Host-specific pins
	bypass_certificate_validation: bool,		//Bypass certificate validation.
	killist_empty: bool,				//Certificate killist forced empty.
}

pub struct TlsClientCert
{
	client_cert: Option<Box<CertificateSigner+Send>>,
	peer_sigalgo_mask: u64,
	request_ocsp: bool,
	request_sct: bool,
}

pub struct Tls12ShowtimeFields
{
}

pub struct Tls13ShowtimeFields
{
	server_app_write: Tls13TrafficSecretIn,
	client_app_write: Tls13TrafficSecretOut,
	rekey_in_progress: bool,
}

pub enum HandshakeState2
{
	ClientHello(ClientHelloFields),
	ServerHello(ServerHelloFields),
	RetryClientHello(ClientHelloFields),
	ServerHelloWaitAgain(ServerHelloFields),
	Tls12Certificate(Tls12CertificateFields),
	Tls12ServerKeyExchange(bool, Tls12ServerKeyExchangeFields),	//Flag is for certificate status.
	Tls12ServerHelloDone(bool, Tls12ServerHelloDoneFields),		//Flag is for certificate requested.
	Tls12ClientCertificate(Tls12ClientKeyExchangeFields),	//This shares fields with CKE.
	Tls12ClientKeyExchange(Tls12ClientKeyExchangeFields),
	Tls12ClientFinished(Tls12ClientFinishedFields),
	Tls12ServerChangeCipherSpec(Tls12ServerChangeCipherSpecFields),
	Tls12ServerFinished(Tls12ServerFinishedFields),
	Tls12Showtime(Tls12ShowtimeFields),
	Tls13EncryptedExtensions(Tls13EncryptedExtensionsFields),
	Tls13Certificate(bool, Tls13CertificateFields),			//Flag is for certificate requested.
	Tls13CertificateVerify(Tls13CertificateVerifyFields),
	Tls13ServerFinished(Tls13ServerFinishedFields),
	Tls13ClientCertificate(Tls13ClientCertificateFields),
	Tls13ClientCertificateVerify(Tls13ClientCertificateVerifyFields),
	Tls13ClientFinished(Tls13ClientFinishedFields),
	Tls13Showtime(Tls13ShowtimeFields),
	Error,
}

impl HandshakeState2
{
	fn name(&self) -> &'static str
	{
		match self {
			&HandshakeState2::ClientHello(_) => "ClientHello",
			&HandshakeState2::ServerHello(_) => "ServerHello",
			&HandshakeState2::RetryClientHello(_) => "RetryClientHello",
			&HandshakeState2::ServerHelloWaitAgain(_) => "ServerHelloWaitAgain",
			&HandshakeState2::Tls12Certificate(_) => "Tls12Certificate",
			&HandshakeState2::Tls12ServerKeyExchange(false, _) => "Tls12ServerKeyExchange",
			&HandshakeState2::Tls12ServerKeyExchange(true, _) => "Tls12ServerKeyExchange(or CS)",
			&HandshakeState2::Tls12ServerHelloDone(false, _) => "Tls12ServerHelloDone",
			&HandshakeState2::Tls12ServerHelloDone(true, _) => "Tls12ServerHelloDone(with CR)",
			&HandshakeState2::Tls12ClientCertificate(_) => "Tls12ClientCertificate",
			&HandshakeState2::Tls12ClientKeyExchange(_) => "Tls12ClientKeyExchange",
			&HandshakeState2::Tls12ClientFinished(_) => "Tls12ClientFinished",
			&HandshakeState2::Tls12ServerChangeCipherSpec(_) => "Tls12ServerChangeCipherSpec",
			&HandshakeState2::Tls12ServerFinished(_) => "Tls12ServerFinished",
			&HandshakeState2::Tls12Showtime(_) => "Tls12Showtime",
			&HandshakeState2::Tls13EncryptedExtensions(_) => "Tls13EncryptedExtensions",
			&HandshakeState2::Tls13Certificate(false, _) => "Tls13Certificate",
			&HandshakeState2::Tls13Certificate(true, _) => "Tls13Certificate(with CR)",
			&HandshakeState2::Tls13CertificateVerify(_) => "Tls13CertificateVerify",
			&HandshakeState2::Tls13ServerFinished(_) => "Tls13ServerFinished",
			&HandshakeState2::Tls13ClientCertificate(_) => "Tls13ClientCertificate",
			&HandshakeState2::Tls13ClientCertificateVerify(_) => "Tls13ClientCertificateVerify",
			&HandshakeState2::Tls13ClientFinished(_) => "Tls13ClientFinished",
			&HandshakeState2::Tls13Showtime(_) => "Tls13Showtime",
			&HandshakeState2::Error => "Error"
		}
	}
}

pub struct HandshakeController
{
	config:ClientConfiguration,			//The client configuration.
	hs_state: HandshakeState2,			//The handshake state.
	crypto_algs: CryptoTracker,			//Used crypto algorithms.
	names: NamesContainer,
	hs_buffer: HsBuffer,
	debug: Option<(u64, Arc<Box<Logging+Send+Sync>>)>,
}

fn _illegal_state() -> Result<TlsFailure, TlsFailure>
{
	sanity_failed!("Attempt to TX in illegal state!");
}

fn illegal_state() -> TlsFailure
{
	match _illegal_state() { Ok(x) => x, Err(x) => x }
}

impl HandshakeController
{
	fn init_shares(config: &ClientConfiguration) -> Vec<Box<DiffieHellmanKey+Send>> {
		let mut res = Vec::new();
		if (config.flags[0] & FLAGS0_ENABLE_TLS13) != 0 &&
			(config.flags[0] & FLAGS0_DEBUG_NO_SHARES) == 0 {
			match (Dhf::X25519).generate_key() { Ok(x) => res.push(x), Err(_) => () };
		}
		res
	}
	pub fn new(config: &ClientConfiguration, host: &str, debug: Option<(u64, Arc<Box<Logging+Send+Sync>>)>) ->
		HandshakeController
	{
		const DUMMY: [HostSpecificPin;0] = [];
		Self::new_pinned(config, host, &DUMMY[..], debug)
	}
	pub fn new_pinned(config: &ClientConfiguration, host: &str, pins: &[HostSpecificPin],
		debug: Option<(u64, Arc<Box<Logging+Send+Sync>>)>) -> HandshakeController
	{
		//Check if we have pins suitable for RPK mode.
		let might_use_spki_only = {
			let any_pin = {
				let mut any_pin = false;
				for i in pins.iter() { any_pin |= i.pin; }
				any_pin
			};
			let mut might_use_spki_only = false;
			//Check host pins against EE SPKI.
			for i in pins.iter() {
				//If pin is a CA pin, it does not match.
				if i.ca_flag == Some(true) { continue; }
				//If pin is not a TA, it does not match.
				if !i.ta { continue; }
				//If pin is not a pin and there are pins, it does not match.
				if !i.pin && any_pin { continue; }
				//If pin is not for SPKI, it does not match.
				if !i.spki { continue; }
				//Might match.
				might_use_spki_only = true;
			}
			might_use_spki_only && !config.no_server_rpk
		};
		let mut client_random = [0; 32];
		secure_random(&mut client_random);
		HandshakeController {
			config: config.clone(),
			hs_state: HandshakeState2::ClientHello(ClientHelloFields{
				might_use_spki_only: might_use_spki_only,
				cookie: None,
				hs_hash: EarlyHandshakeRep::new(),
				no_request_sni: false,
				tls13_shares: Self::init_shares(config),
				ciphersuite_assigned: None,
				features: CFLAG_UNKNOWN,	//Ignore unknown features.
				group_id: None,
				client_random: client_random,
				cvinfo: TlsCVInfo {
					bypass_certificate_validation: false,
					requested_sni: host.to_owned(),
					killist_empty: false,
					pins: pins.to_owned(),
				},
			}),
			crypto_algs: CryptoTracker::new(),
			names: NamesContainer::new(),
			hs_buffer: HsBuffer::new(),
			debug: debug.clone(),
		}
	}
	pub fn new_insecure_nocheck(config: &ClientConfiguration,
		debug: Option<(u64, Arc<Box<Logging+Send+Sync>>)>) -> HandshakeController
	{
		let mut client_random = [0; 32];
		secure_random(&mut client_random);
		let mut ctrl = HandshakeController {
			config: config.clone(),
			hs_state: HandshakeState2::ClientHello(ClientHelloFields{
				might_use_spki_only: true,
				cookie: None,
				hs_hash: EarlyHandshakeRep::new(),
				no_request_sni: true,
				tls13_shares: Self::init_shares(config),
				features: CFLAG_UNKNOWN,	//Ignore unknown features.
				ciphersuite_assigned: None,
				group_id: None,
				cvinfo: TlsCVInfo {
					killist_empty: true,
					pins: Vec::new(),
					requested_sni: "".to_owned(),
					bypass_certificate_validation: true,
				},
				client_random: client_random,
			}),
			crypto_algs: CryptoTracker::new(),
			names: NamesContainer::new(),
			hs_buffer: HsBuffer::new(),
			debug: debug.clone(),
		};
		ctrl.config.flags[0] |= FLAGS0_REQUIRE_EMS;
		ctrl.config.flags[0] &= !FLAGS0_REQUIRE_CT;
		ctrl
	}
	pub fn wants_tx(&self) -> bool
	{
		match &self.hs_state {
			&HandshakeState2::ClientHello(_) => true,
			&HandshakeState2::ServerHello(_) => false,
			&HandshakeState2::RetryClientHello(_) => true,
			&HandshakeState2::ServerHelloWaitAgain(_) => false,
			&HandshakeState2::Tls12Certificate(_) => false,
			&HandshakeState2::Tls12ServerKeyExchange(_, _) => false,
			&HandshakeState2::Tls12ServerHelloDone(_, _) => false,
			&HandshakeState2::Tls12ClientCertificate(_) => true,
			&HandshakeState2::Tls12ClientKeyExchange(_) => true,
			&HandshakeState2::Tls12ClientFinished(_) => true,
			&HandshakeState2::Tls12ServerChangeCipherSpec(_) => false,
			&HandshakeState2::Tls12ServerFinished(_) => false,
			&HandshakeState2::Tls12Showtime(_) => false,
			&HandshakeState2::Tls13EncryptedExtensions(_) => false,
			&HandshakeState2::Tls13Certificate(_, _) => false,
			&HandshakeState2::Tls13CertificateVerify(_) => false,
			&HandshakeState2::Tls13ServerFinished(_) => false,
			&HandshakeState2::Tls13ClientCertificate(_) => true,
			&HandshakeState2::Tls13ClientCertificateVerify(_) => true,
			&HandshakeState2::Tls13ClientFinished(_) => true,
			&HandshakeState2::Tls13Showtime(_) => false,
			&HandshakeState2::Error => false
		}
	}
	pub fn wants_rx(&self) -> bool
	{
		match &self.hs_state {
			&HandshakeState2::ClientHello(_) => false,
			&HandshakeState2::ServerHello(_) => true,
			&HandshakeState2::RetryClientHello(_) => false,
			&HandshakeState2::ServerHelloWaitAgain(_) => true,
			&HandshakeState2::Tls12Certificate(_) => true,
			&HandshakeState2::Tls12ServerKeyExchange(_, _) => true,
			&HandshakeState2::Tls12ServerHelloDone(_, _) => true,
			&HandshakeState2::Tls12ClientCertificate(_) => false,
			&HandshakeState2::Tls12ClientKeyExchange(_) => false,
			&HandshakeState2::Tls12ClientFinished(_) => false,
			&HandshakeState2::Tls12ServerChangeCipherSpec(_) => true,
			&HandshakeState2::Tls12ServerFinished(_) => true,
			&HandshakeState2::Tls12Showtime(_) => false,
			&HandshakeState2::Tls13EncryptedExtensions(_) => true,
			&HandshakeState2::Tls13Certificate(_, _) => true,
			&HandshakeState2::Tls13CertificateVerify(_) => true,
			&HandshakeState2::Tls13ServerFinished(_) => true,
			&HandshakeState2::Tls13ClientCertificate(_) => false,
			&HandshakeState2::Tls13ClientCertificateVerify(_) => false,
			&HandshakeState2::Tls13ClientFinished(_) => false,
			&HandshakeState2::Tls13Showtime(_) => false,
			&HandshakeState2::Error => false
		}
	}
	pub fn in_showtime(&self) -> bool
	{
		match &self.hs_state {
			&HandshakeState2::Tls12Showtime(_) => true,
			&HandshakeState2::Tls13Showtime(_) => true,
			_ => false
		}
	}
	pub fn queue_hs_tx(&mut self, base: &mut SessionBase) -> bool
	{
		let ret = match &self.hs_state {
			&HandshakeState2::Tls12ClientFinished(_) => true,
			&HandshakeState2::Tls13ClientFinished(_) => true,
			_ => false
		};

		let debug = self.debug.clone();
		let sname = self.hs_state.name();
		let mut tmpstate = HandshakeState2::Error;
		swap(&mut tmpstate, &mut self.hs_state);
		self.hs_state = match match tmpstate {
			HandshakeState2::ClientHello(x) => x.tx_clienthello(base, false, &mut self.names,
				debug.clone(), &self.config, sname),
			HandshakeState2::RetryClientHello(x) => x.tx_clienthello(base, true, &mut self.names,
				debug.clone(), &self.config, sname),
			HandshakeState2::Tls12ClientCertificate(x) => x.tx_certificate(base, debug, sname),
			HandshakeState2::Tls12ClientKeyExchange(x) => x.tx_client_key_exchange(base, debug, sname),
			HandshakeState2::Tls12ClientFinished(x) => x.tx_finished(base, debug, sname),
			HandshakeState2::Tls13ClientCertificate(x) => x.tx_certificate(base, debug, sname),
			HandshakeState2::Tls13ClientCertificateVerify(x) => x.tx_certificate_verify(base, debug,
				sname),
			HandshakeState2::Tls13ClientFinished(x) => x.tx_finished(base, debug, sname),
			_ => Err(illegal_state())
		} {
			Ok(y) => y,
			Err(x) => {base.abort_handshake(x); HandshakeState2::Error }
		};
		ret
	}
	pub fn get_alpn(&self) -> Option<Option<Arc<String>>>
	{
		self.names.get_alpn()
	}
	pub fn get_sni(&self) -> Option<Option<Arc<String>>>
	{
		self.names.get_sni()
	}
	pub fn get_server_names(&self) -> Option<Arc<Vec<String>>>
	{
		self.names.get_server_names()
	}
	fn handle_hs_message(&mut self, controller: &mut SessionController, htype: u8, data: &[u8], last: bool) ->
		Result<(), TlsFailure>
	{
		let debug = self.debug.clone();
		if debug.clone().map(|x|x.0).unwrap_or(0) & debug_class!(HANDSHAKE_DATA) == 0 {
			//DEBUG_HANDSHAKE_DATA has more detailed message, so don't print one here.
			debug!(HANDSHAKE_MSGS debug, "Received handshake message {} in state {}",
				explain_hs_type(htype), self.hs_state.name());
		}
		let raw_msg = HandshakeMessage::new(htype, data);
		let mut tmpstate = HandshakeState2::Error;
		swap(&mut tmpstate, &mut self.hs_state);
		self.hs_state = match (tmpstate, htype) {
			(HandshakeState2::ServerHello(x), HSTYPE_SERVER_HELLO) =>
				x.rx_server_hello_w(controller, TlsServerHello::parse(data, debug.clone())?, last,
					&mut self.names, debug, &self.config, raw_msg, &mut self.crypto_algs)?,
			(HandshakeState2::ServerHello(x), HSTYPE_HELLO_RETRY_REQUEST) => {
				x.rx_hello_retry_request(controller, TlsHelloRetryRequest::parse(data, debug.
					clone())?, data, debug, &self.config, raw_msg, &mut self.crypto_algs)?
			},
			(HandshakeState2::ServerHelloWaitAgain(x), HSTYPE_SERVER_HELLO) =>
				x.rx_server_hello_w(controller, TlsServerHello::parse(data, debug.clone())?, last,
					&mut self.names, debug, &self.config, raw_msg, &mut self.crypto_algs)?,
			(HandshakeState2::Tls12Certificate(x), HSTYPE_CERTIFICATE) => {
				let use_spki_only = x.use_spki_only;
				x.rx_certificate(Tls12Certificate::parse(data, debug.clone(), use_spki_only)?,
					&mut self.names, debug, &self.config, raw_msg)?
			},
			(HandshakeState2::Tls12ServerKeyExchange(_, x), HSTYPE_SERVER_KEY_EXCHANGE) => {
				x.rx_server_key_exchange(TlsServerKeyExchange::parse(data)?, debug, &self.config,
					raw_msg, &mut self.crypto_algs)?
			},
			//This requires the maybe_send_status flag to be true.
			(HandshakeState2::Tls12ServerKeyExchange(true, x), HSTYPE_CERTIFICATE_STATUS)  => {
				x.rx_certificate_status(TlsCertificateStatus::parse(data, debug.clone())?,
					debug, &self.config, raw_msg, &mut self.crypto_algs)?
			},
			(HandshakeState2::Tls12ServerHelloDone(false, x), HSTYPE_CERTIFICATE_REQUEST) => {
				x.rx_certificate_request(data, debug, raw_msg)?
			},
			(HandshakeState2::Tls12ServerHelloDone(y, x), HSTYPE_SERVER_HELLO_DONE) => {
				x.rx_server_hello_done(data, y, debug, raw_msg)?
			},
			(HandshakeState2::Tls12ServerFinished(x), HSTYPE_FINISHED) => {
				//Finished.
				fail_if!(!last, TlsFailure::FinishedNotAligned);
				let tmp = x.rx_finished(controller, data, &mut self.names, debug, raw_msg)?;
				self.names.release_server_names();
				tmp
			},
			(HandshakeState2::Tls13EncryptedExtensions(x), HSTYPE_ENCRYPTED_EXTENSIONS) => {
				//EncryptedExtensions.
				x.rx_encrypted_extensions(controller, TlsEncryptedExtensions::parse(data, debug.
					clone())?, &mut self.names, debug, &self.config, raw_msg)?
			}
			(HandshakeState2::Tls13Certificate(false, x), HSTYPE_CERTIFICATE_REQUEST) => {
				//CertificateRequest.
				//Hack: If this is old TLS version, just mark the request.
				if x.is_old_tls {
					x.rx_certificate_request_unparse(debug, raw_msg)?
				} else {
					x.rx_certificate_request(Tls13CertificateRequest::parse(data, debug.
						clone())?, debug, raw_msg, &self.config)?
				}
			}
			(HandshakeState2::Tls13Certificate(y, x), HSTYPE_CERTIFICATE) => {
				//Certificate
				x.rx_certificate(Tls13Certificate::parse(data, debug.clone(), false)?,
					&mut self.names, debug.clone(), &self.config, y, raw_msg,
					&mut self.crypto_algs)?
			}
			(HandshakeState2::Tls13CertificateVerify(x), HSTYPE_CERTIFICATE_VERIFY) => {
				//CertificateVerify
				x.rx_certificate_verify(controller, TlsCertificateVerify::parse(data)?, debug.
					clone(), raw_msg, &mut self.crypto_algs)?
			}
			(HandshakeState2::Tls13ServerFinished(x), HSTYPE_FINISHED) => {
				//Finished
				fail_if!(!last, TlsFailure::FinishedNotAligned);
				x.rx_finished(controller, data, &mut self.names, debug.clone(), raw_msg)?
			}
			(HandshakeState2::Tls13Showtime(x), HSTYPE_CERTIFICATE_REQUEST) => {
				//CertificateRequest. Just ignore this.
				debug!(HANDSHAKE_EVENTS debug, "Unimplemented: \
					Post-handshake auth. Will not respond.");
				HandshakeState2::Tls13Showtime(x)
			},
			(HandshakeState2::Tls13Showtime(mut x), HSTYPE_KEY_UPDATE) => {
				//KeyUpdate
				fail_if!(!last, TlsFailure::KeyUpdateNotAligned);
				let kmsg = TlsKeyUpdate::parse(data)?;
				x.server_app_write.rachet(debug.clone())?;
				controller.set_deprotector(x.server_app_write.to_deprotector(debug.clone())?);
				debug!(HANDSHAKE_EVENTS debug, "Downstream encryption \
					keys changed by rekey message");
				if !controller.last_tx_key_update() && !x.rekey_in_progress && kmsg.requested {
					//These have to be in this order for the handshake to go out with old keys.
					controller.queue_handshake(HSTYPE_KEY_UPDATE, &[0], "Tls13Showtime");
					x.client_app_write.rachet(debug.clone())?;
					controller.set_protector(x.client_app_write.to_protector(debug.clone())?);
					debug!(HANDSHAKE_EVENTS debug, "Upstream encryption keys changed by \
						rekey-in-kind");
				}
				x.rekey_in_progress = false;
				HandshakeState2::Tls13Showtime(x)
			}
			(HandshakeState2::Tls13Showtime(x), HSTYPE_SESSION_TICKET) => {
				//Just ignore this, we don't support tickets.
				HandshakeState2::Tls13Showtime(x)
			},
			(state, htype2) => {
				fail!(TlsFailure::UnexpectedHandshakeMessage(htype2, state.name()));
			}
		};
		if htype == HSTYPE_FINISHED {
			//Receiving data is OK.
			controller.assert_hs_rx_finished();
		}
		Ok(())
	}
	fn _handle_tx_update(&mut self, controller: &mut SessionController) -> Result<(), TlsFailure>
	{
		let debug = self.debug.clone();
		//These have to be in this order for the handshake to go out with old keys.
		if let &mut HandshakeState2::Tls13Showtime(ref mut x) = &mut self.hs_state {
			controller.queue_handshake(HSTYPE_KEY_UPDATE, &[1], "Tls13Showtime");
			x.client_app_write.rachet(debug.clone())?;
			controller.set_protector(x.client_app_write.to_protector(debug.clone())?);
			x.rekey_in_progress = true;
			debug!(HANDSHAKE_EVENTS debug, "Upstream encryption keys changed by spontaneous rekey");
		}
		Ok(())
	}
	pub fn handle_tx_update(&mut self, controller: &mut SessionController)
	{
		match self._handle_tx_update(controller) {
			Ok(_) => (),
			Err(x) => controller.abort_handshake(x)
		}
	}
	pub fn handle_control(&mut self, rtype: u8, msg: &[u8], controller: &mut SessionController)
	{
		match self._handle_control(rtype, msg, controller) {
			Ok(_) => (),
			Err(x) => controller.abort_handshake(x)
		}
	}
	pub fn callback_with_signature_future<F>(&mut self, sbase: &mut SessionBase, mut cb: F)
		where F: FnMut(&mut SessionBase, &mut FutureReceiver<Result<(u16, Vec<u8>),()>>)
	{
		if let &mut HandshakeState2::Tls13ClientCertificateVerify(ref mut x) = &mut self.hs_state {
			cb(sbase, &mut x.signature_future);
		}
	}
	//Note, the handshake buffer MUST be idle.
	fn _handle_control_hs_borowed(&mut self, msg: &[u8], controller: &mut SessionController) ->
		Result<(), TlsFailure>
	{
		let debug = self.debug.clone();
		let mut _msg = msg;
		loop {
			if _msg.len() < 4 { break; }	//No full header.
			//4 bytes at least.
			let hlen = (_msg[1] as usize) * 65536 + (_msg[2] as usize) * 256 +
				(_msg[3] as usize);
			//Check if enough space (hlen is at most 16MB, well in bounds).
			//Also, hlen + 4 >= 4.
			if _msg.len() < hlen + 4 { break; } //No full record.
			let (hmsg, trailer) = _msg.split_at(hlen + 4);
			_msg = trailer;
			let last = _msg.len() == 0;
			//hmsg is at least 4 bytes, and has 4 byte header.
			debug!(HANDSHAKE_DATA debug, "[State {}] Received (borrowed) {} (length {}):\n{}",
				self.hs_state.name(), explain_hs_type(hmsg[0]), hmsg.len().saturating_sub(4),
				bytes_as_hex_block(&hmsg[4..], ">"));
			self.handle_hs_message(controller, hmsg[0], &hmsg[4..], last)?;
		}
		if _msg.len() > 0 {
			match self.hs_buffer.extract(&mut _msg) {
				Ok(None) => (),
				Ok(Some(_)) => sanity_failed!("No handshake messages but handshake \
					buffer extracted one"),
				Err(err) => fail!(TlsFailure::HsMsgBuffer(err))
			}
		}
		return Ok(());
	}
	fn _handle_control(&mut self, rtype: u8, msg: &[u8], controller: &mut SessionController) ->
		Result<(), TlsFailure>
	{
		let debug = self.debug.clone();
		if rtype == PROTO_HANDSHAKE {
			fail_if!(msg.len() == 0, TlsFailure::EmptyHandshakeMessage);
			if self.hs_buffer.is_idle() {
				return self._handle_control_hs_borowed(msg, controller);
			}
			let mut _msg = msg;
			loop {
				match self.hs_buffer.extract(&mut _msg) {
					Ok(None) => break,
					Ok(Some((htype, payload, last))) => {
						debug!(HANDSHAKE_DATA debug, "[State {}] Received {} (length {}):\
							\n{}", self.hs_state.name(), explain_hs_type(htype),
							payload.len(), bytes_as_hex_block(&payload, ">"));
						self.handle_hs_message(controller, htype, &payload, last)?;
					},
					Err(err) => fail!(TlsFailure::HsMsgBuffer(err))
				};
			}
		} else if rtype == PROTO_CCS {
			debug!(HANDSHAKE_DATA debug, "[State {}] Received change cipher spec:\n{}",
				self.hs_state.name(), bytes_as_hex_block(msg, ">"));
			//ChangeCipherSpec.
			fail_if!(!self.hs_buffer.empty(), TlsFailure::CcsInMiddleOfHandshakeMessage);
			//The || short-circuits, so the array access has to have msg.len()=1 => in bounds.
			fail_if!(msg.len() != 1 || msg[0] != 1, TlsFailure::InvalidCcs);
			let mut tmpstate = HandshakeState2::Error;
			swap(&mut tmpstate, &mut self.hs_state);
			self.hs_state = match tmpstate {
				HandshakeState2::Tls12ServerChangeCipherSpec(x) =>
					x.rx_change_cipher_spec(controller, debug)?,
				x => fail!(TlsFailure::UnexpectedCcs(x.name()))
			}
		} else {
			fail!(TlsFailure::UnexpectedRtype(rtype));
		}
		Ok(())
	}
	pub fn connection_info(&self) -> TlsConnectionInfo
	{
		self.crypto_algs.connection_info()
	}
}

const VERSION_TLS12: u16 = 0x0303;
const VERSION_TLS13: u16 = 0x0304;

impl HandshakeController
{
}
