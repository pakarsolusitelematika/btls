use super::{HandshakeState2, Tls13C2ndFSecrets, Tls13HsSecrets, TlsClientCert, TlsCVInfo};
use super::super::ClientCertificateCriteria;
use super::finished::{Tls13ClientFinishedFields, Tls13ServerFinishedFields};
use super::serverkeyexchange::Tls12ServerKeyExchangeFields;
use ::certificate::{CFLAG_MUST_CT, CFLAG_MUST_STAPLE, CFLAG_MUST_TLS13, extract_spki, OcspDumpedScts,
	OcspValidationResult, ParsedCertificate, validate_chain, validate_ee_spki, validate_scts};
use ::client::ClientConfiguration;
use ::client_certificates::HostSpecificPin;
use ::common::{CryptoTracker, Debugger, emit_handshake, EXT_CT, EXT_STATUS_REQUEST, format_tbs_tls13, HandshakeHash,
	HandshakeMessage, HashMessages, HSTYPE_CERTIFICATE, HSTYPE_CERTIFICATE_VERIFY, NamesContainer, OCSP_SINGLE,
	TLS_LEN_CERT_ENTRY, TLS_LEN_CERT_LIST, TLS_LEN_EXTENSIONS, TLS_LEN_OCSP_RESPONSE, TLS_LEN_SCT_ENTRY,
	TLS_LEN_SCT_LIST, TLS_LEN_SIGNATURE, TlsFailure, write_ext_fn, write_sub_fn};
use ::features::{FLAGS0_ENABLE_TLS13, FLAGS0_REQUIRE_CT, FLAGS0_REQUIRE_OCSP};
use ::logging::bytes_as_hex_block;
use ::messages::{Tls12Certificate, Tls13Certificate, Tls13CertificateRequest, TlsCertificateVerify};
use ::record::{SessionBase, SessionController};
use ::server_certificates::CertificateSigner;
use ::stdlib::{Arc, Box, BTreeMap, from_utf8, ToOwned, Vec};
use ::utils::subslice_to_range;

use btls_aux_aead::ProtectorType;
use btls_aux_dhf::DiffieHellmanKey;
use btls_aux_futures::FutureReceiver;
use btls_aux_hash::HashFunction;
use btls_aux_serialization::Sink;
use btls_aux_signature_algo::{SIGALGO_TLS13_MASK, SignatureType, VERIFY_FLAG_NO_RSA_PKCS1};
use btls_aux_time::TimeNow;

struct DoValidateServerCertChainParams<'a>
{
	dont_validate_ocsp: &'a mut bool,
	bypass_certificate_validation: bool,
	requested_sni: &'a str,
	features: u32,
	use_spki_only: bool,
	pins: &'a mut Vec<HostSpecificPin>,
	server_spki: &'a mut Vec<u8>,
	killist_empty: bool,
	require_ocsp_unsat: &'a mut bool,
	require_sct_unsat: &'a mut bool,
}

//Handle main chain validation.
fn do_validate_server_cert_chain<'b,'a>(eecert: &'b [u8], icerts: &[&[u8]], names: &mut NamesContainer,
	debug: Debugger, params: DoValidateServerCertChainParams<'a>, config: &ClientConfiguration) ->
	Result<Option<ParsedCertificate<'b>>, TlsFailure>
{
	let mut features = params.features;

	if params.bypass_certificate_validation {
		//There is no certificate chain, so skip OCSP.
		*params.dont_validate_ocsp = true;
		if params.use_spki_only {
			*params.server_spki = eecert.to_owned();
		} else {
			//There is no names list available in this mode.
			let eecert_p = ParsedCertificate::from(eecert).map_err(|x|TlsFailure::EECertParseError(x))?;
			*params.server_spki = eecert_p.pubkey.to_owned();
		}
		return Ok(None);
	}
	//If TLS 1.3 is disabled, consider requirement for TLS 1.3 met.
	if (config.flags[0] & FLAGS0_ENABLE_TLS13) != 0 { features |= CFLAG_MUST_TLS13; }
	//If use_spki_only is active, just validate the server key.
	let eecert_p = if params.use_spki_only {
		//OCSP can't be done with RPK.
		*params.dont_validate_ocsp = true;
		match validate_ee_spki(eecert, params.pins) {
			Ok(_) => (),
			Err(_) => fail!(TlsFailure::ServerKeyNotWhitelisted)
		};
		*params.server_spki = eecert.to_owned();
		None
	} else {
		let empty_map = BTreeMap::new();
		let killist = if params.killist_empty { &empty_map } else { &config.killist };
		match validate_chain(eecert, &icerts, params.requested_sni, &TimeNow, features,
			&config.trust_anchors, params.pins, killist, params.require_ocsp_unsat,
			params.require_sct_unsat) {
			//true means pinned to SPKI, in this case, no OCSP can be done.
			Ok(x) => *params.dont_validate_ocsp = x,
			Err(ref x) => fail!(TlsFailure::CertificateValidationFailed(x.clone()))
		};
		//Certificate is OK, extract SPKI and names out of it. Note that we will not provode the server
		//name list before server Finished is recieved and checked.
		let eecert_p = ParsedCertificate::from(eecert).map_err(|x|TlsFailure::EECertParseError(x))?;
		*params.server_spki = eecert_p.pubkey.to_owned();
		names.set_server_names(Arc::new(eecert_p.dnsnames.iter().filter_map(|x|from_utf8(x).ok()).map(|x|
			x.to_owned()).collect()));
		Some(eecert_p)
	};
	debug!(HANDSHAKE_EVENTS debug, "Validated server certificate/key");
	if *params.dont_validate_ocsp {
		debug!(HANDSHAKE_EVENTS debug, "Chain validation was bypassed: Server key is explicitly trusted");
	}
	//Pins and killist are not needed anymore.
	params.pins.clear();
	Ok(eecert_p)
}

pub struct Tls12CertificateFields
{
	pub cvinfo: TlsCVInfo,					//Server certificate validation info.
	pub tls13_shares: Vec<Box<DiffieHellmanKey+Send>>,	//The key shares (also abused for TLS 1.2).
	pub staple_scts: Vec<(bool, Vec<u8>)>,			//Stapled SCTs. The flag is set for precerts.
	pub use_spki_only: bool,				//The certificate is raw SPKI.
	pub maybe_send_status: bool,				//Maybe send certificate status.
	pub features: u32,					//Features supported.
	pub client_random: [u8; 32],				//The client random.
	pub server_random: [u8; 32],				//The server random.
	pub hs_hash: HandshakeHash,
	pub ems_enabled: bool,
	pub protection: ProtectorType,
	pub prf: HashFunction,
}

pub struct Tls13CertificateFields
{
	pub cvinfo: TlsCVInfo,					//Server certificate validation info.
	pub features: u32,				//Features supported.
	pub hs_secrets: Tls13HsSecrets,
	pub hs_hash: HandshakeHash,
	pub cc: TlsClientCert,
	pub is_old_tls: bool,
}

pub struct Tls13CertificateVerifyFields
{
	server_spki: Vec<u8>,			//The SPKI of the server.
	bypass_certificate_validation: bool,	//Certificate validation was bypassed.
	requested_certificate: bool,		//Certificate has been requested.
	hs_secrets: Tls13HsSecrets,
	pub hs_hash: HandshakeHash,
	cc: TlsClientCert,
}

impl Tls13CertificateFields
{
	//Handle unparseable TLS 1.3 CertificateRequest message
	pub fn rx_certificate_request_unparse<'a>(mut self, debug: Debugger, raw_msg: HandshakeMessage<'a>) ->
		Result<HandshakeState2, TlsFailure>
	{
		//This will always be rejected.
		debug!(HANDSHAKE_EVENTS debug, "Received old format certificate request. Will NAK.");
		self.hs_hash.add(raw_msg, debug)?;
		Ok(HandshakeState2::Tls13Certificate(true, Tls13CertificateFields{
			cvinfo: self.cvinfo,
			features: self.features,
			hs_secrets: self.hs_secrets,
			hs_hash: self.hs_hash,
			//Dummy client certificate entry, cc will be rejected.
			cc: self.cc,
			is_old_tls: self.is_old_tls,
		}))
	}
	//Handle TLS 1.3 CertificateRequest message
	pub fn rx_certificate_request<'a>(mut self, pmsg: Tls13CertificateRequest, debug: Debugger,
		raw_msg: HandshakeMessage<'a>, config: &ClientConfiguration) -> Result<HandshakeState2, TlsFailure>
	{
		//We are only interested about the context.
		fail_if!(pmsg.context.len() > 0, TlsFailure::CrContextMustBeEmpty);
		let client_cert = if let &Some(ref cb) = &config.cc_callback {
			let mut cb = cb.clone_self();
			let criteria = ClientCertificateCriteria {
				ee_algorithms: pmsg.signature_algorithms & SIGALGO_TLS13_MASK,
				algorithms: pmsg.signature_algorithms,
			};
			let tmp_cb = cb.on_certificate_request(criteria);
			if tmp_cb.is_some() {
				debug!(HANDSHAKE_EVENTS debug, "Received certificate request. Sending certificate \
					back.");
			} else {
				debug!(HANDSHAKE_EVENTS debug, "Received certificate request. NAKed by callback.");
			};
			tmp_cb
		} else {
			debug!(HANDSHAKE_EVENTS debug, "Received certificate request. Will NAK.");
			None
		};

		self.hs_hash.add(raw_msg, debug)?;
		Ok(HandshakeState2::Tls13Certificate(true, Tls13CertificateFields{
			cvinfo: self.cvinfo,
			features: self.features,
			hs_secrets: self.hs_secrets,
			hs_hash: self.hs_hash,
			cc: TlsClientCert {
				request_ocsp: pmsg.status_request,
				request_sct: pmsg.sct_request,
				client_cert: client_cert,
				peer_sigalgo_mask: pmsg.signature_algorithms & SIGALGO_TLS13_MASK,
			},
			is_old_tls: self.is_old_tls,
		}))
	}
	//Handle TLS 1.3 Certificate message.
	pub fn rx_certificate<'a, 'b>(self, certmsg: Tls13Certificate<'a>, names: &mut NamesContainer,
		debug: Debugger, config: &ClientConfiguration, certificate_reqd: bool, raw_msg: HandshakeMessage<'b>,
		crypto_algs: &mut CryptoTracker) -> Result<HandshakeState2, TlsFailure>
	{
		let mut hs_hash = self.hs_hash;
		hs_hash.add(raw_msg, debug.clone())?;

		let mut server_spki = Vec::new();
		let mut pins = self.cvinfo.pins;
		let mut dont_validate_ocsp = false;
		let mut features = self.features;
		let mut staple_scts = Vec::new();
		let require_ct = (config.flags[0] & FLAGS0_REQUIRE_CT) != 0;
		let require_ocsp = (config.flags[0] & FLAGS0_REQUIRE_OCSP) != 0;
		let mut require_ocsp_unsat = require_ocsp;
		let mut require_sct_unsat = require_ct;
		//Extract the EE certificate. This has to be present.
		fail_if!(certmsg.chain.len() == 0, TlsFailure::ServerChainEmpty);
		let eecert = certmsg.chain[0];
		fail_if!(eecert.len() == 0, TlsFailure::EeCertificateEmpty);
		//Read intermediate certs. There can be any amount of these (including 0.) There is at least one
		//certificate, so slice is valid.
		let icerts = &certmsg.chain[1..];

		//Collect the stapled SCTs from the TLS extension (in TLS 1.3, these are inside Certificate message).
		for i in certmsg.stapled_scts {
			debug!(HANDSHAKE_EVENTS debug, "Received SCT#{} using TLS extension.", staple_scts.len());
			staple_scts.push((false, i.to_owned()));
		}
		//Collect the stapled SCTs from OCSP response if any.
		if let Some(ocsp) = certmsg.stapled_ocsp {
			let sctlist = OcspDumpedScts::from(ocsp).map_err(|x|TlsFailure::CantExtractSct13(x))?;
			for i in sctlist.list {
				debug!(HANDSHAKE_EVENTS debug, "Received SCT#{} using OCSP.", staple_scts.len());
				staple_scts.push((false, i.to_owned()));
			}
		}
		//If OCSP staples were sent, satisfy CFLAG_MUST_STAPLE.
		//Always satisfy CFLAG_MUST_CT, because we need to check that later.

		if certmsg.stapled_ocsp.is_some() { features |= CFLAG_MUST_STAPLE; }
		features |= CFLAG_MUST_CT;

		let use_spki_only = certmsg.is_rpk;
		let parsed_eecert = do_validate_server_cert_chain(eecert, icerts, names, debug.clone(),
			DoValidateServerCertChainParams{dont_validate_ocsp: &mut dont_validate_ocsp,
			bypass_certificate_validation: self.cvinfo.bypass_certificate_validation, requested_sni:
			&self.cvinfo.requested_sni, features: features, use_spki_only: use_spki_only, pins:
			&mut pins, server_spki: &mut server_spki, killist_empty: self.cvinfo.killist_empty,
			require_ocsp_unsat: &mut require_ocsp_unsat, require_sct_unsat: &mut require_sct_unsat},
			config)?;
		if self.cvinfo.bypass_certificate_validation {
			return Ok(HandshakeState2::Tls13CertificateVerify(Tls13CertificateVerifyFields {
				server_spki: server_spki,
				bypass_certificate_validation: true,			//Always set.
				requested_certificate: certificate_reqd,		//Preserved.
				hs_secrets: self.hs_secrets,
				hs_hash: hs_hash,
				cc: self.cc,
			}));
		}

		//Collect SCTs from certificate.
		if let &Some(ref pcert) = &parsed_eecert {
			for i in pcert.scts.iter() {
				debug!(HANDSHAKE_EVENTS debug, "Received SCT#{} using certificate.", staple_scts.
					len());
				staple_scts.push((true, (*i).to_owned()));
			}
		}
		let eecert_issuer = match &parsed_eecert {
			&Some(ref x) => Some(x.issuer),
			&None => None
		};

		//Check SCTs.
		if !dont_validate_ocsp {	//dont_validate_ocsp covers use_spki_only.
			if staple_scts.len() > 0 {
				let mut got_one = false;
				//Grab the issuer key, if any is available.
				let key = grab_issuer_key!(icerts, eecert_issuer, config.trust_anchors);
				if let Some(k) = key {
					validate_scts(eecert, k, &mut staple_scts, &mut require_sct_unsat,
						&mut got_one, &config.trusted_logs, debug.clone()).
						map_err(|x|TlsFailure::SctValidationFailed(x))?;
				}
				if got_one { crypto_algs.ct_validated(); }
			}
		} else {
			require_sct_unsat = false;	//Don't require SCTs.
			if require_ct || staple_scts.len() > 0 {
				debug!(HANDSHAKE_EVENTS debug, "Not checking SCT, since chain validation was \
					bypassed");
			}
		}

		//Determine if certificate is short-lived.
		let short_lived_cert = if let &Some(ref ep) = &parsed_eecert {
			ep.not_before.delta(ep.not_after) <= config.ocsp_maxvalid
		} else {
			false
		};

		//On TLS 1.3, validate staple if any.
		if short_lived_cert && !dont_validate_ocsp {
			require_ocsp_unsat = false;	//OCSP is deemed OK.
			debug!(HANDSHAKE_EVENTS debug, "Not checking OCSP, since certificate is short-lived");
			crypto_algs.ocsp_shortlived();
		} else if let (Some(ocsp), false) = (certmsg.stapled_ocsp, dont_validate_ocsp) {
			fail_if!(certmsg.stapled_ocsp_type != 1, TlsFailure::BogusCsOcspType(
				certmsg.stapled_ocsp_type));
			let issuer_key = grab_issuer_key!(icerts, eecert_issuer, config.trust_anchors);
			let issuer_key = issuer_key.ok_or(TlsFailure::OcspIncompleteCertificateChain)?;
			//Grab issuer and serial. We can assume valid EE certificate if we reach here
			//(OCSP validation to be performed).
			let eecertp = parsed_eecert.ok_or(TlsFailure::OcspIncompleteCertificateChain)?;
			let issuer = eecertp.issuer;
			let serial = eecertp.serial_number;
			let results = OcspValidationResult::from(ocsp, issuer, serial, issuer_key, &TimeNow).
				map_err(|x|TlsFailure::InvalidOcsp(x))?;
			let too_long = results.response_lifetime > config.ocsp_maxvalid;
			if too_long {
				debug!(HANDSHAKE_EVENTS debug, "Not considering OCSP valid due to excessive \
					lifetime");
				if require_ocsp_unsat {
					//This certificate needs OCSP, but the response isn't valid because of the
					//too long duration.
					fail!(TlsFailure::OcspValidityTooLong(results.response_lifetime,
						config.ocsp_maxvalid));
				}
			} else {
				require_ocsp_unsat = false;	//OCSP checked.
				debug!(HANDSHAKE_EVENTS debug, "Validated stapled OCSP response");
				crypto_algs.ocsp_validated();
			}
		} else if dont_validate_ocsp {
			require_ocsp_unsat = false;	//Don't require OCSP.
			debug!(HANDSHAKE_EVENTS debug, "Not checking OCSP, since chain \
				validation was bypassed");
		} else {
			debug!(HANDSHAKE_EVENTS debug, "No stapled OCSP response");
		}
		//We have now validated CT and OCSP if we are going to do so.
		fail_if!(require_sct_unsat, TlsFailure::CertRequiresSct);
		fail_if!(require_ocsp_unsat, TlsFailure::CertRequiresOcsp);

		Ok(HandshakeState2::Tls13CertificateVerify(Tls13CertificateVerifyFields {
			server_spki: server_spki,
			bypass_certificate_validation: self.cvinfo.bypass_certificate_validation,
			requested_certificate: certificate_reqd,
			hs_secrets: self.hs_secrets,
			hs_hash: hs_hash,
			cc: self.cc,
		}))
	}
}

impl Tls13CertificateVerifyFields
{
	//Handle TLS 1.3 CertificateVerify message.
	pub fn rx_certificate_verify<'a,'b>(self, _controller: &mut SessionController,
		cvmsg: TlsCertificateVerify<'a>, debug: Debugger, raw_msg: HandshakeMessage,
		crypto_algs: &mut CryptoTracker) -> Result<HandshakeState2, TlsFailure>
	{
		//Format TBS and verify signature.
		let cv_hash = self.hs_hash.checkpoint()?;
		let tbs = format_tbs_tls13(cv_hash, true)?;
		debug!(CRYPTO_CALCS debug, "Server TBS:\n{}", bytes_as_hex_block(&tbs, ">"));
		cvmsg.signature.verify(&self.server_spki, &tbs, VERIFY_FLAG_NO_RSA_PKCS1).map_err(|_|
			TlsFailure::CvSignatureFailed)?;
		crypto_algs.set_signature(cvmsg.signature.algorithm);
		debug!(HANDSHAKE_EVENTS debug, "Server key exchange signature ({:?}) OK",
			cvmsg.signature.algorithm);

		let mut hs_hash = self.hs_hash;
		hs_hash.add(raw_msg, debug)?;
		Ok(HandshakeState2::Tls13ServerFinished(Tls13ServerFinishedFields{
			bypass_certificate_validation: self.bypass_certificate_validation,
			sig_verified: true,
			requested_certificate: self.requested_certificate,
			hs_secrets: self.hs_secrets,
			hs_hash: hs_hash,
			cc: self.cc,
		}))
	}
}

impl Tls12CertificateFields
{
	pub fn rx_certificate<'a,'b>(self, certmsg: Tls12Certificate<'a>, names: &mut NamesContainer,
		debug: Debugger, config: &ClientConfiguration, raw_msg: HandshakeMessage<'b>) ->
		Result<HandshakeState2, TlsFailure>
	{
		let mut hs_hash = self.hs_hash;
		hs_hash.add(raw_msg, debug.clone())?;

		let mut staple_ee_cert = Vec::new();
		let mut staple_issuer_key = Vec::new();
		let mut dont_validate_ocsp = false;
		let mut features = self.features;
		let mut pins = self.cvinfo.pins;
		let mut server_spki = Vec::new();
		let mut require_ocsp_unsat = (config.flags[0] & FLAGS0_REQUIRE_OCSP) != 0;
		let mut require_sct_unsat = (config.flags[0] & FLAGS0_REQUIRE_CT) != 0;
		let mut staple_scts = self.staple_scts;
		let mut staple_issuer = 0..0;
		let mut staple_serial = 0..0;
		//Extract the EE certificate. This has to be present.
		fail_if!(certmsg.chain.len() == 0, TlsFailure::ServerChainEmpty);
		let eecert = certmsg.chain[0];
		fail_if!(eecert.len() == 0, TlsFailure::EeCertificateEmpty);
		//Read intermediate certs. There can be any amount of these (including 0). There is at least one
		//certificate, so slice is valid.
		let icerts = &certmsg.chain[1..];

		//Hack: Since we don't know the OCSP status yet, set the feature flag just if we have maybe send
		//OCSP. We check later if OCSP was actually sent.
		//Do that also unconditionally for SCT.
		if self.maybe_send_status { features |= CFLAG_MUST_STAPLE; }
		features |= CFLAG_MUST_CT;
		let eecert_parsed = do_validate_server_cert_chain(eecert, icerts, names, debug.clone(),
			DoValidateServerCertChainParams{dont_validate_ocsp: &mut dont_validate_ocsp,
			bypass_certificate_validation: self.cvinfo.bypass_certificate_validation, requested_sni:
			&self.cvinfo.requested_sni, features: features, use_spki_only: self.use_spki_only,
			pins: &mut pins, server_spki: &mut server_spki, killist_empty: self.cvinfo.killist_empty,
			require_ocsp_unsat: &mut require_ocsp_unsat, require_sct_unsat: &mut require_sct_unsat},
			config)?;

		//Determine if shortlived.
		let short_lived_cert = if let &Some(ref ep) = &eecert_parsed {
			ep.not_before.delta(ep.not_after) <= config.ocsp_maxvalid
		} else {
			false
		};

		if self.cvinfo.bypass_certificate_validation {
			return Ok(HandshakeState2::Tls12ServerKeyExchange(self.maybe_send_status,
				Tls12ServerKeyExchangeFields {
				tls13_shares: self.tls13_shares,
				server_spki: server_spki,
				staple_scts: Vec::new(),			//No SCTs to validate.
				staple_ee_cert: Vec::new(),			//Nothing to validate.
				staple_issuer_key: Vec::new(),			//Nothing to validate.
				staple_issuer: 0..0,				//Nothing to validate.
				staple_serial: 0..0,				//Nothing to validate.
				dont_validate_ocsp: true,			//Never validate.
				use_spki_only: true,				//Can Assume this.
				require_ocsp_unsat: false,			//Don't require OCSP.
				bypass_certificate_validation: true,		//Bypassed.
				require_sct_unsat: false,			//Don't require SCT.
				client_random: self.client_random,
				server_random: self.server_random,
				hs_hash: hs_hash,
				ems_enabled: self.ems_enabled,
				protection: self.protection,
				prf: self.prf,
				short_lived_cert: false,			//Normal processing here.
			}));
		}

		//Save the receceived SCTs, and OCSP params.
		if let &Some(ref pcert) = &eecert_parsed {
			staple_issuer = subslice_to_range(eecert, pcert.issuer.0).map_err(|_|
				assert_failure!("Certificate issuer not from certificate???"))?;
			staple_serial = subslice_to_range(eecert, pcert.serial_number).map_err(|_|
				assert_failure!("Certificate serial not from certificate???"))?;
			for i in pcert.scts.iter() {
				debug!(HANDSHAKE_EVENTS debug, "Received SCT#{} using certificate.",
					staple_scts.len());
				staple_scts.push((true, (*i).to_owned()));
			}
		}

		//Save info for OCSP validation if sent. Also do this if we might need to validate SCTs.
		if self.maybe_send_status || staple_scts.len() > 0 {
			//Save EE cert and its issuer.
			staple_ee_cert = eecert.to_owned();
			let issuer_key = grab_issuer_key!(icerts, eecert_parsed.map(|x|x.issuer),
				config.trust_anchors);
			if let Some(isskey) = issuer_key { staple_issuer_key = isskey.to_owned(); }
		}

		//If dont_validate_ocsp is set, we never require SCT or OCSP.
		require_ocsp_unsat &= !dont_validate_ocsp;
		require_sct_unsat &= !dont_validate_ocsp;

		Ok(HandshakeState2::Tls12ServerKeyExchange(self.maybe_send_status, Tls12ServerKeyExchangeFields {
			tls13_shares: self.tls13_shares,
			server_spki: server_spki,
			staple_scts: staple_scts,
			staple_ee_cert: staple_ee_cert,
			staple_issuer_key: staple_issuer_key,
			staple_issuer: staple_issuer,
			staple_serial: staple_serial,
			dont_validate_ocsp: dont_validate_ocsp,
			use_spki_only: self.use_spki_only,
			bypass_certificate_validation: self.cvinfo.bypass_certificate_validation,
			require_ocsp_unsat: require_ocsp_unsat,
			require_sct_unsat: require_sct_unsat,
			client_random: self.client_random,
			server_random: self.server_random,
			hs_hash: hs_hash,
			ems_enabled: self.ems_enabled,
			protection: self.protection,
			prf: self.prf,
			short_lived_cert: short_lived_cert,
		}))
	}
}

pub struct Tls13ClientCertificateFields
{
	pub hs_secrets2: Tls13C2ndFSecrets,
	pub hs_hash: HandshakeHash,
	pub cc: TlsClientCert,
}

fn send_certificate(content: &mut Vec<u8>, cert: &CertificateSigner, req_ocsp: bool, req_sct: bool,
	debugger: Debugger) -> Result<(), TlsFailure>
{
	let cchain = cert.get_certificate();
	let ocsp = cert.get_ocsp();
	let scts = cert.get_scts();
	sanity_check!(cchain.0.len() > 0, "Empty client certificate chain given");
	write_sub_fn(content, "Certificate list", TLS_LEN_CERT_LIST, |x|{
		for (index, cert) in cchain.0.iter().enumerate() {
			write_sub_fn(x, "Certificate", TLS_LEN_CERT_ENTRY, |y|{
				try_buffer!(y.write_slice(&cert.0));
				Ok(())
			})?;
			write_sub_fn(x, "Extensions", TLS_LEN_EXTENSIONS, |y|{
				if let (&Some(ref ocsp), 0, true) = (&ocsp.0, index, req_ocsp) {
					sanity_check!(ocsp.len() > 0, "Empty client OCSP response given");
					debug!(HANDSHAKE_EVENTS debugger, "Sent OCSP staple");
					write_ext_fn(y, EXT_STATUS_REQUEST, "status_request", |z|{
						try_buffer!(z.write_u8(OCSP_SINGLE));
						write_sub_fn(z, "OCSP response", TLS_LEN_OCSP_RESPONSE, |w|{
							try_buffer!(w.write_slice(&ocsp));
							Ok(())
						})
					})?;
				}
				if let (&Some(ref scts), 0, true) = (&scts.0, index, req_sct) {
					sanity_check!(scts.len() > 0, "Empty client SCT list given");
					debug!(HANDSHAKE_EVENTS debugger, "Sent SCT staple");
					write_ext_fn(y, EXT_CT, "signed_certificate_timestamp", |z|{
						write_sub_fn(z, "SCT list", TLS_LEN_SCT_LIST, |w|{
							for i in scts.iter() {
								write_sub_fn(w, "SCT entry", TLS_LEN_SCT_ENTRY, |v|{
									try_buffer!(v.write_slice(&i.0));
									Ok(())
								})?;
							}
							Ok(())
						})
					})?;
				}
				Ok(())
			})?;
		}
		Ok(())
	})
}

impl Tls13ClientCertificateFields
{
	//Transmit a certificate message.
	pub fn tx_certificate(mut self, base: &mut SessionBase, debug: Debugger, state: &str) ->
		Result<HandshakeState2, TlsFailure>
	{
		let mut content = Vec::new();
		//In TLS 1.3, certificates have context field.
		//This is only used for in-handshake auth, and there context needs to be empty.
		try_buffer!(content.write_u8(0));
		let cert2 = if let Some(certbox) = self.cc.client_cert {
			send_certificate(&mut content, &*certbox, self.cc.request_ocsp, self.cc.request_sct,
				debug.clone())?;
			Some(certbox)
		} else {
			//Just put an empty certificate list (we always refuse authentication).
			try_buffer!(content.write_u24(0));
			None
		};
		emit_handshake(debug.clone(), HSTYPE_CERTIFICATE, &content, base, &mut self.hs_hash, state)?;
		if let Some(certbox) = cert2 {
			//Request signature over key exchange.
			let tbs = format_tbs_tls13(self.hs_hash.checkpoint()?, false)?;
			debug!(CRYPTO_CALCS debug, "Client TBS:\n{}", bytes_as_hex_block(&tbs, ">"));
			let signature_future = certbox.sign(&tbs);
			debug!(HANDSHAKE_EVENTS debug, "Requesting signature for key exchange");
			Ok(HandshakeState2::Tls13ClientCertificateVerify(Tls13ClientCertificateVerifyFields {
				signature_future: signature_future,
				hs_secrets2: self.hs_secrets2,
				hs_hash: self.hs_hash,
				peer_sigalgo_mask: self.cc.peer_sigalgo_mask,
			}))
		} else {
			Ok(HandshakeState2::Tls13ClientFinished(Tls13ClientFinishedFields{
				hs_secrets2: self.hs_secrets2,
				hs_hash: self.hs_hash,
			}))
		}
	}
}

pub struct Tls13ClientCertificateVerifyFields
{
	hs_secrets2: Tls13C2ndFSecrets,
	hs_hash: HandshakeHash,
	pub signature_future: FutureReceiver<Result<(u16, Vec<u8>),()>>,
	peer_sigalgo_mask: u64,
}

impl Tls13ClientCertificateVerifyFields
{
	pub fn tx_certificate_verify(self, base: &mut SessionBase, debug: Debugger, state: &str) ->
		Result<HandshakeState2, TlsFailure>
	{
		let is_ready = self.signature_future.settled();
		if !is_ready { return Ok(HandshakeState2::Tls13ClientCertificateVerify(self)); }

		let (sigalgo, signature) = self.signature_future.read().map_err(|_|assert_failure!(
			"Signature request gave no answer"))?.map_err(|_|assert_failure!(
			"Can't sign handshake"))?;
		if let Some(x) = SignatureType::by_tls_id(sigalgo) {
			fail_if!(x.sigalgo_const() & self.peer_sigalgo_mask == 0,
				TlsFailure::InterlockSignatureAlgorithm);
		} else {
			sanity_failed!("Trying to sign handshake with unknown signature scheme {:x}", sigalgo);
		};
		let mut content = Vec::new();
		try_buffer!(content.write_u16(sigalgo));
		write_sub_fn(&mut content, "Signature", TLS_LEN_SIGNATURE, |x|{
			try_buffer!(x.write_slice(signature.as_ref()));
			Ok(())
		})?;
		debug!(HANDSHAKE_EVENTS debug, "Signed the key exchange");

		let mut hs_hash = self.hs_hash;
		emit_handshake(debug.clone(), HSTYPE_CERTIFICATE_VERIFY, &content, base, &mut hs_hash, state)?;

		Ok(HandshakeState2::Tls13ClientFinished(Tls13ClientFinishedFields {
			hs_secrets2: self.hs_secrets2,
			hs_hash: hs_hash,
		}))
	}
}
