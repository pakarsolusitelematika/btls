use super::{HandshakeState2, TlsCVInfo};
use ::client::ClientConfiguration;
use ::client::controller::serverhello::ServerHelloFields;
use ::common::{CERTTYPE_RPK, CERTTYPE_X509, Ciphersuite, CS_ECDHE_ECDSA_AES128, CS_ECDHE_ECDSA_AES256,
	CS_ECDHE_ECDSA_CHACHA20, CS_ECDHE_RSA_AES128, CS_ECDHE_RSA_AES256, CS_ECDHE_RSA_CHACHA20, CS_TLS13_AES128,
	CS_TLS13_AES256, CS_TLS13_CHACHA20, Debugger, EarlyHandshakeRep, emit_handshake, EXT_ALPN, EXT_COOKIE,
	EXT_CT, EXT_EMS, EXT_KEY_SHARE, EXT_RECORD_SIZE_LIMIT, EXT_RENEGO_INFO, EXT_SERVER_CERTIFICATE_TYPE,
	EXT_SERVER_NAME, EXT_STATUS_REQUEST, EXT_SUPPORTED_GROUPS, EXT_SUPPORTED_VERSIONS, HSTYPE_CLIENT_HELLO,
	NamesContainer, OCSP_SINGLE, TLS_LEN_ALPN_ENTRY, TLS_LEN_ALPN_LIST, TLS_LEN_CIPHERSUITE_LIST, TLS_LEN_COOKIE,
	TLS_LEN_EXTENSIONS, TLS_LEN_OCSP_EXTENSIONS, TLS_LEN_OCSP_RESPONDER, TLS_LEN_SCERTT_LIST, TLS_LEN_SNI_ENTRY,
	TLS_LEN_SNI_LIST, TLS_LEN_SUPGRP_LIST, TLS_LEN_SUPVER_LIST, TLS_LEN_TLS13_PUBKEY_LIST, TlsFailure,
	TlsVersion, write_ext_fn, write_signature_algorithms_ext, write_sub_fn};
use ::features::{FLAGS0_ASSUME_HW_AES, FLAGS0_ENABLE_TLS12, FLAGS0_ENABLE_TLS13};
use ::logging::bytes_as_hex_block;
use ::record::SessionBase;
use ::stdlib::{Box, Vec};

use btls_aux_dhf::{DiffieHellmanKey, GRP_P256, GRP_P384, GRP_P521, GRP_X25519, GRP_X448};
use btls_aux_serialization::Sink;


//Write server host name as server name list.
fn write_server_name_list<S:Sink>(to: &mut S, name: &str) -> Result<(), TlsFailure>
{
	write_sub_fn(to, "server name list", TLS_LEN_SNI_LIST, |x|{
		try_buffer!(x.write_u8(0));
		write_sub_fn(x, "host_name", TLS_LEN_SNI_ENTRY, |x|{
			try_buffer!(x.write_slice(name.as_bytes())); Ok(())
		})
	})
}

//Write an ALPN entry.
fn write_alpn_entry<S:Sink>(to: &mut S, name: &str) -> Result<(), TlsFailure>
{
	write_sub_fn(to, "ALPN entry", TLS_LEN_ALPN_ENTRY, |x|{
		try_buffer!(x.write_slice(name.as_bytes()));
		Ok(())
	})
}

//Write the ALPN list.
fn write_alpn_list<S:Sink>(to: &mut S, config: &ClientConfiguration) -> Result<(), TlsFailure>
{
	write_sub_fn(to, "ALPN list", TLS_LEN_ALPN_LIST, |x|{
		for i in config.supported_alpn.iter() { write_alpn_entry(x, i)? }
		Ok(())
	})
}

pub struct ClientHelloFields
{
	pub might_use_spki_only: bool,				//Maybe try raw SPKI.
	pub cvinfo: TlsCVInfo,					//Server certificate validation info.
	pub no_request_sni: bool,				//Don't request SNI.
	pub cookie: Option<Vec<u8>>,				//Cookie the server passed back.
	pub hs_hash: EarlyHandshakeRep,				//The client hello, plus possible restart (with
								//headers).
	pub tls13_shares: Vec<Box<DiffieHellmanKey+Send>>,	//The key shares (also abused for TLS 1.2).
	pub ciphersuite_assigned: Option<Ciphersuite>,		//Assigned ciphersuite (used for HRR in 1.3-draft19+)
	pub features: u32,					//Features supported.
	pub group_id: Option<u16>,				//The group ID from HRR.
	pub client_random: [u8; 32],
}

impl ClientHelloFields
{
	//Write the key share list.
	fn write_key_share_list<S:Sink>(&self, x: &mut S) -> Result<(), TlsFailure>
	{
		write_sub_fn(x, "key_share list", TLS_LEN_TLS13_PUBKEY_LIST, |x|{
			for i in self.tls13_shares.iter() {
				write_ext_fn(x, i.tls_id(), "key_share entry", |x|{
					let pubkey = i.pubkey().map_err(|x|assert_failure!("Can't get own DH public \
						key: {}", x))?;
					try_buffer!(x.write_slice(pubkey.as_ref()));
					Ok(())
				})?;
			}
			Ok(())
		})
	}
	//Send a ClientHello message.
	pub fn tx_clienthello(self, base: &mut SessionBase, retry: bool, names: &mut NamesContainer,
		debug: Debugger, config: &ClientConfiguration, state: &str) -> Result<HandshakeState2, TlsFailure>
	{
		let offer_tls12 = (config.flags[0] & FLAGS0_ENABLE_TLS12) != 0;
		let offer_tls13 = (config.flags[0] & FLAGS0_ENABLE_TLS13) != 0;
		let _aes_gcm_is_hw = config.flags[0] & FLAGS0_ASSUME_HW_AES != 0;
		let mut content = Vec::new();
		//The maximum version (this is frozen at TLS 1.2)
		try_buffer!(content.write_u16(0x0303));
		//Random. This has already been generated.
		try_buffer!(content.write_slice(&self.client_random));
		if !retry {
			debug!(CRYPTO_CALCS debug, "Using client random:\n{}", bytes_as_hex_block(
				&self.client_random, ">"));
		}

		//Session id. This is always blank.
		try_buffer!(content.write_u8(0));
		//Ciphersuites.
		write_sub_fn(&mut content, "ciphersuite list", TLS_LEN_CIPHERSUITE_LIST, |x|{
			if offer_tls12 {
				let cslist = if _aes_gcm_is_hw {
					[CS_ECDHE_ECDSA_AES128, CS_ECDHE_ECDSA_CHACHA20, CS_ECDHE_ECDSA_AES256,
						CS_ECDHE_RSA_AES128, CS_ECDHE_RSA_CHACHA20, CS_ECDHE_RSA_AES256]
				} else {
					[CS_ECDHE_ECDSA_CHACHA20, CS_ECDHE_ECDSA_AES128, CS_ECDHE_ECDSA_AES256,
						CS_ECDHE_RSA_CHACHA20, CS_ECDHE_RSA_AES128, CS_ECDHE_RSA_AES256]
				};
				for i in cslist.iter() { try_buffer!(x.write_u16(*i)); }
			}
			if offer_tls13 {
				let cslist = if _aes_gcm_is_hw {
					[CS_TLS13_AES128, CS_TLS13_CHACHA20, CS_TLS13_AES256]
				} else {
					[CS_TLS13_CHACHA20, CS_TLS13_AES128, CS_TLS13_AES256]
				};
				for i in cslist.iter() { try_buffer!(x.write_u16(*i)); }
			}
			Ok(())
		})?;
		//Compressions. This is always [0].
		try_buffer!(content.write_u16(0x0100));
		//Extensions
		write_sub_fn(&mut content, "extension list", TLS_LEN_EXTENSIONS, |x|{
			//Supported versions.
			write_ext_fn(x, EXT_SUPPORTED_VERSIONS, "supported_versions", |x|{
				write_sub_fn(x, "Supported versions", TLS_LEN_SUPVER_LIST, |x|{
					if (config.flags[0] & FLAGS0_ENABLE_TLS13) != 0 {
						for i in TlsVersion::supported_tls13().iter() {
							try_buffer!(x.write_u16(i.to_code()));
						}
					}
					if (config.flags[0] & FLAGS0_ENABLE_TLS12) != 0 {
						try_buffer!(x.write_u16(0x0303));
					}
					Ok(())
				})
			})?;
			if !self.no_request_sni {
				//If requested_sni is set, include server_name extension.
				names.set_sni(self.cvinfo.requested_sni.clone());
				write_ext_fn(x, EXT_SERVER_NAME, "server_name", |x|
					write_server_name_list(x, &self.cvinfo.requested_sni)
				)?;
			}
			names.sni_known();
			if !self.cvinfo.bypass_certificate_validation {
				//Include status_request (single, implicit responder, no extensions).
				write_ext_fn(x, EXT_STATUS_REQUEST, "status_request", |x|{
					try_buffer!(x.write_u8(OCSP_SINGLE));
					write_sub_fn(x, "OCSP responders", TLS_LEN_OCSP_RESPONDER, |_|{Ok(())})?;
					write_sub_fn(x, "OCSP extensions", TLS_LEN_OCSP_EXTENSIONS, |_|{Ok(())})?;
					Ok(())
				})?;
				//Certificate transparency (empty).
				write_ext_fn(x, EXT_CT, "certifcate_transparency", |_|Ok(()))?;
			}
			//Include supported_groups extension.
			write_ext_fn(x, EXT_SUPPORTED_GROUPS, "supported_groups", |x|{
				let grplist = [GRP_X25519, GRP_P256, GRP_X448, GRP_P384, GRP_P521];
				write_sub_fn(x, "groups", TLS_LEN_SUPGRP_LIST, |x|{
					for i in grplist.iter() { try_buffer!(x.write_u16(*i)); }
					Ok(())
				})
			})?;
			//Include signature_algorithms extension.
			write_signature_algorithms_ext(x)?;
			//Include ALPN extension if requested.
			if config.supported_alpn.len() > 0 {
				write_ext_fn(x, EXT_ALPN, "ALPN", |x|write_alpn_list(x, config))?;
			}
			//Certificate types.
			if self.might_use_spki_only {
				write_ext_fn(x, EXT_SERVER_CERTIFICATE_TYPE, "server_certificate_type", |x|{
					write_sub_fn(x, "server_certificate_type payload", TLS_LEN_SCERTT_LIST, |x|{
						try_buffer!(x.write_u8(CERTTYPE_RPK));
						try_buffer!(x.write_u8(CERTTYPE_X509));
						Ok(())
					})
				})?;
			}
			//If TLS 1.2 is on the table, offer extenended_master_secret.
			if offer_tls12 {
				write_ext_fn(x, EXT_EMS, "extended_master_secret", |_|Ok(()))?;
			}
			//If TLS 1.3 is on the table, offer key_share.
			if offer_tls13 {
				write_ext_fn(x, EXT_KEY_SHARE, "key_share", |x|
					self.write_key_share_list(x)
				)?;
			}
			//If we got cookie from last time, offer cookie.
			if let Some(ref cookie) = self.cookie {
				write_ext_fn(x, EXT_COOKIE, "cookie", |x|{
					write_sub_fn(x, "cookie payload", TLS_LEN_COOKIE, |x|{
						try_buffer!(x.write_slice(&cookie));
						Ok(())
					})
				})?;
			}
			//If TLS 1.2 is on the table, offer renegotiation_info.
			if offer_tls12 {
				write_ext_fn(x, EXT_RENEGO_INFO, "renegotiation_info", |x|{
					try_buffer!(x.write_u8(0));
					Ok(())
				})?;
			}
			//We always advertise 16384 byte limit.
			write_ext_fn(x, EXT_RECORD_SIZE_LIMIT, "record_size_limit", |x|{
				try_buffer!(x.write_u16(16384));
				Ok(())
			})?;
			Ok(())
		})?;
		let mut hs_hash = self.hs_hash;
		emit_handshake(debug.clone(), HSTYPE_CLIENT_HELLO, &content, base, &mut hs_hash, state)?;
		let inner = ServerHelloFields{
			ciphersuite_assigned: self.ciphersuite_assigned,
			hs_hash: hs_hash,
			features: self.features,
			group_id: self.group_id,
			might_use_spki_only: self.might_use_spki_only,
			no_request_sni: self.no_request_sni,
			cvinfo: self.cvinfo,
			tls13_shares: self.tls13_shares,
			client_random: self.client_random,
		};
		Ok(if retry {
			HandshakeState2::ServerHelloWaitAgain(inner)
		} else {
			HandshakeState2::ServerHello(inner)
		})
	}
}
