use super::HandshakeState2;
use ::certificate::{CertificateIssuer, OcspDumpedScts, OcspValidationResult, validate_scts};
use ::client::ClientConfiguration;
use ::client::controller::serverhellodone::Tls12ServerHelloDoneFields;
use ::common::{CryptoTracker, Debugger, format_tbs_tls12, HandshakeHash, HandshakeMessage, HashMessages, TlsFailure};
use ::logging::bytes_as_hex_block;
use ::messages::{TlsCertificateStatus, TlsServerKeyExchange};
use ::record::{DiffieHellmanSharedSecret, Tls12PremasterSecret};
use ::stdlib::{Box, Range, ToOwned, Vec};

use btls_aux_aead::ProtectorType;
use btls_aux_dhf::DiffieHellmanKey;
use btls_aux_hash::HashFunction;
use btls_aux_time::TimeNow;

pub struct Tls12ServerKeyExchangeFields
{
	pub tls13_shares: Vec<Box<DiffieHellmanKey+Send>>,	//The key shares (also abused for TLS 1.2).
	pub server_spki: Vec<u8>,				//The SPKI of the server.
	pub staple_scts: Vec<(bool, Vec<u8>)>,			//Stapled SCTs. The flag is set for precerts.
	pub staple_ee_cert: Vec<u8>,				//EE certificate for OCSP/SCT stapling.
	pub staple_issuer_key: Vec<u8>,				//Issuer key for OCSP stapling.
	pub staple_issuer: Range<usize>,			//EE certificate issuer range.
	pub staple_serial: Range<usize>,			//EE certificate serial range.
	pub dont_validate_ocsp: bool,				//Don't validate OCSP.
	pub use_spki_only: bool,				//The certificate is raw SPKI.
	pub require_ocsp_unsat: bool,				//Unsatisfied require OCSP staple.
	pub bypass_certificate_validation: bool,		//Certificate validation was bypassed.
	pub require_sct_unsat: bool,				//Unsatisfied require SCT staple.
	pub client_random: [u8; 32],				//The client random.
	pub server_random: [u8; 32],				//The server random.
	pub hs_hash: HandshakeHash,
	pub ems_enabled: bool,
	pub protection: ProtectorType,
	pub prf: HashFunction,
	pub short_lived_cert: bool,				//Short lived certificate, don't check OCSP.
}

impl Tls12ServerKeyExchangeFields
{
	//Recieve a certificate status message.
	pub fn rx_certificate_status<'a>(self, csmsg: TlsCertificateStatus<'a>, debug: Debugger,
		config: &ClientConfiguration, raw_msg: HandshakeMessage, crypto_algs: &mut CryptoTracker) ->
		Result<HandshakeState2, TlsFailure>
	{
		let mut hs_hash = self.hs_hash;
		let mut require_sct_unsat = self.require_sct_unsat;
		let mut require_ocsp_unsat = self.require_ocsp_unsat;
		let mut staple_scts = self.staple_scts;
		fail_if!(csmsg.resptype != 1, TlsFailure::BogusCsOcspType(csmsg.resptype));
		//We do the SCT processing even if certifcate is short-lived.
		if !self.dont_validate_ocsp {
			//Collect the SCTs.
			let sctlist = OcspDumpedScts::from(csmsg.respdata).map_err(|x|
				TlsFailure::CantExtractSct12(x))?;
			for i in sctlist.list {
				debug!(HANDSHAKE_EVENTS debug, "Received SCT#{} using OCSP.",
					staple_scts.len());
				staple_scts.push((false, i.to_owned()));
			}
			//We now have complete collection of SCTs. Check those.
			let mut got_one = false;
			if staple_scts.len() > 0 {
				validate_scts(&self.staple_ee_cert, &self.staple_issuer_key, &mut staple_scts,
					&mut require_sct_unsat, &mut got_one, &config.trusted_logs,
					debug.clone()).map_err(|x|TlsFailure::SctValidationFailed(x))?;
			}
			if got_one { crypto_algs.ct_validated(); }
		}
		if self.short_lived_cert {
			//Skip OCSP. We print message when processing ServerKeyExchange.
			require_ocsp_unsat = false;
		} else if !self.dont_validate_ocsp {
			fail_if!(self.staple_issuer_key.len() == 0, TlsFailure::OcspIncompleteCertificateChain);
			let issuer_name = CertificateIssuer(&self.staple_ee_cert[self.staple_issuer.clone()]);
			let results = OcspValidationResult::from(csmsg.respdata, issuer_name,
				&self.staple_ee_cert[self.staple_serial.clone()], &self.staple_issuer_key, &TimeNow).
				map_err(|x|TlsFailure::InvalidOcsp(x))?;
			let too_long = results.response_lifetime > config.ocsp_maxvalid;
			if too_long {
				debug!(HANDSHAKE_EVENTS debug, "Not considering OCSP valid due to excessive \
					lifetime");
				if require_ocsp_unsat {
					//This certificate needs OCSP, but the response isn't valid because of the
					//too long duration.
					fail!(TlsFailure::OcspValidityTooLong(results.response_lifetime,
						config.ocsp_maxvalid));
				}
			} else {
				require_ocsp_unsat = false;
				debug!(HANDSHAKE_EVENTS debug, "Validated stapled OCSP response");
				crypto_algs.ocsp_validated();
			}
		} else {
			debug!(HANDSHAKE_EVENTS debug, "Not checking OCSP, since chain validation \
				was bypassed");
			require_ocsp_unsat = false;
			if require_sct_unsat || staple_scts.len() > 0 {
				require_sct_unsat = false;
				debug!(HANDSHAKE_EVENTS debug, "Not checking SCT, since chain \
					validation was bypassed");
			}
		}

		hs_hash.add(raw_msg, debug.clone())?;
		Ok(HandshakeState2::Tls12ServerKeyExchange(false, Tls12ServerKeyExchangeFields {
			tls13_shares: self.tls13_shares,
			staple_scts: Vec::new(),	//Not needed anymore.
			staple_ee_cert: Vec::new(),	//Not needed anymore.
			staple_issuer_key: Vec::new(),	//Not needed anymore.
			server_spki: self.server_spki,
			require_sct_unsat: require_sct_unsat,
			staple_issuer: self.staple_issuer,
			staple_serial: self.staple_serial,
			dont_validate_ocsp: self.dont_validate_ocsp,
			use_spki_only: self.use_spki_only,
			require_ocsp_unsat: require_ocsp_unsat,		//Not necressarily cleared.
			bypass_certificate_validation: self.bypass_certificate_validation,
			client_random: self.client_random,
			server_random: self.server_random,
			hs_hash: hs_hash,
			ems_enabled: self.ems_enabled,
			protection: self.protection,
			prf: self.prf,
			short_lived_cert: self.short_lived_cert,
		}))
	}
	//Recieve a ServerKeyExchange message.
	pub fn rx_server_key_exchange<'a,'b>(self, pmsg: TlsServerKeyExchange<'a>, debug: Debugger,
		config: &ClientConfiguration, raw_msg: HandshakeMessage<'b>, crypto_algs: &mut CryptoTracker) ->
		Result<HandshakeState2, TlsFailure>
	{
		let mut hs_hash = self.hs_hash;
		let mut require_sct_unsat = self.require_sct_unsat;
		let mut require_ocsp_unsat = self.require_ocsp_unsat;
		let mut staple_scts = self.staple_scts;
		//If certificate is short-lived, force the OCSP required but unsatisfied to be false, and print
		//a message.
		if self.short_lived_cert {
			require_ocsp_unsat = false;
			debug!(HANDSHAKE_EVENTS debug, "Not checking OCSP, since certificate is short-lived");
			crypto_algs.ocsp_shortlived();
		}
		//We may be in case where there are stapled SCTs via TLS extension or certificate, but no OCSP
		//staple.
		if !self.dont_validate_ocsp {
			let mut got_one = false;
			if staple_scts.len() > 0 {
				validate_scts(&self.staple_ee_cert, &self.staple_issuer_key, &mut staple_scts,
					&mut require_sct_unsat, &mut got_one, &config.trusted_logs, debug.clone()).
					map_err(|x|TlsFailure::SctValidationFailed(x))?;
			}
			if got_one { crypto_algs.ct_validated(); }
		} else {
			if require_sct_unsat || staple_scts.len() > 0 {
				require_sct_unsat = false;
				debug!(HANDSHAKE_EVENTS debug, "Not checking SCT, since chain \
					validation was bypassed");
			}
		}
		//We should have received OCSP staple by now. Check that there isn't unsatisfied OCSP requirement.
		//This can only happen in TLS 1.2. In TLS 1.3, the check is when receiving the certificate message.
		fail_if!(require_ocsp_unsat, TlsFailure::CertRequiresOcsp);
		//We should have received OCSP staple by now. Check that there isn't unsatisfied SCT requirement.
		//This can only happen in TLS 1.2. In TLS 1.3, the check is when receiving the certificate message.
		fail_if!(require_sct_unsat, TlsFailure::CertRequiresSct);

		//Try to pull a key out of TLS 1.3 keyshare buffer. Note that this clears the TLS 1.3 keyshare
		//buffer (we want just 1 key left).
		let mut _dhkey = None;
		let mut tls13_shares = self.tls13_shares;
		for i in tls13_shares.drain(..) {
			if i.tls_id() == pmsg.group.to_tls_id() {
				//Hit!
				_dhkey = Some(i);
				break;
			}
		}
		let mut dhkey = if let Some(x) = _dhkey { x } else {
			//Missed the guess. Generate a new key.
			pmsg.group.generate_key().map_err(|x|assert_failure!("Can't generate Diffie-Hellman \
				keypair for group #{}: {}", pmsg.group.to_tls_id(), x))?
		};
		debug!(HANDSHAKE_EVENTS debug, "Selected key exchange group {:?}", pmsg.group);

		//Set the premaster secret to DH result. This is TLS 1.2, so no zero block.
		let dhshared = DiffieHellmanSharedSecret::new(&mut dhkey, pmsg.pubkey)?;
		crypto_algs.set_kex_group(dhshared.get_group());
		let premaster_secret = Tls12PremasterSecret::new(dhshared, self.protection, self.prf, false,
			self.ems_enabled, debug.clone())?;
		debug!(HANDSHAKE_EVENTS debug, "Diffie-Hellman key agreement performed");

		//Construct TBS and validate the server signature.
		let tbs = format_tbs_tls12(pmsg.raw_share, &self.client_random, &self.server_random)?;
		debug!(CRYPTO_CALCS debug, "Server TBS:\n{}", bytes_as_hex_block(&tbs, ">"));
		pmsg.signature.verify(&self.server_spki, &tbs, 0).map_err(|_|TlsFailure::SkeSignatureFailed)?;
		crypto_algs.set_signature(pmsg.signature.algorithm);
		debug!(HANDSHAKE_EVENTS debug, "Server key exchange signature ({:?}) OK",
			pmsg.signature.algorithm);

		let pubkey = dhkey.pubkey().map_err(|x|assert_failure!("Can't get own DH public key: {}", x))?;

		hs_hash.add(raw_msg, debug)?;
		Ok(HandshakeState2::Tls12ServerHelloDone(false, Tls12ServerHelloDoneFields {
			pubkey: pubkey,
			bypass_certificate_validation: self.bypass_certificate_validation,
			sig_verified: true,
			premaster_secret: premaster_secret,
			client_random: self.client_random,
			server_random: self.server_random,
			hs_hash: hs_hash,
		}))
	}
}
