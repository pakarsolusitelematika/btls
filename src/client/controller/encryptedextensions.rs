use super::{HandshakeState2, Tls13HsSecrets, TlsClientCert, TlsCVInfo};
use ::client::ClientConfiguration;
use ::client::controller::certificate::Tls13CertificateFields;
use ::common::{Debugger, HandshakeHash, HandshakeMessage, HashMessages, NamesContainer, TlsFailure};
use ::messages::TlsEncryptedExtensions;
use ::record::SessionController;
use ::stdlib::ToOwned;

pub struct Tls13EncryptedExtensionsFields
{
	pub cvinfo: TlsCVInfo,				//Server certificate validation info.
	pub features: u32,				//Features supported.
	pub hs_secrets: Tls13HsSecrets,
	pub hs_hash: HandshakeHash,
	pub is_old_tls: bool,				//TLS older than draft19.
}

impl Tls13EncryptedExtensionsFields
{
	//Handle received EncryptedExtensions message.
	pub fn rx_encrypted_extensions<'a,'b>(self, controller: &mut SessionController,
		eemsg: TlsEncryptedExtensions<'a>, names: &mut NamesContainer, debug: Debugger,
		config: &ClientConfiguration, raw_msg: HandshakeMessage<'b>) -> Result<HandshakeState2, TlsFailure>
	{
		//ALPN: Record it. And present or not, it is known.
		if let Some(alpn) = eemsg.alpn {
			let alpn_candidates = &config.supported_alpn;
			fail_if!(alpn_candidates.iter().filter(|x|*x==&alpn).next().is_none(),
				TlsFailure::BogusAlpnProtocol);
			debug!(HANDSHAKE_EVENTS debug, "Application Layer Protocol is '{}'", alpn);
			names.set_alpn(alpn.to_owned());
		}
		if let Some(limit) = eemsg.max_record_size {
			debug!(HANDSHAKE_EVENTS debug, "Maximum fragment size supported by peer is {}", limit);
			controller.set_max_fragment_length(limit);
		}
		//ALPN is available, if any.
		names.alpn_known();

		let mut hs_hash = self.hs_hash;
		hs_hash.add(raw_msg, debug)?;
		Ok(HandshakeState2::Tls13Certificate(false, Tls13CertificateFields{
			features: self.features,
			hs_secrets: self.hs_secrets,
			hs_hash: hs_hash,
			cvinfo: self.cvinfo,
			cc: TlsClientCert {
				client_cert: None,
				peer_sigalgo_mask: 0,
				request_ocsp: false,
				request_sct: false,
			},
			is_old_tls: self.is_old_tls,
		}))
	}
}
