use super::{HandshakeState2, Tls13HsSecrets, TlsCVInfo, VERSION_TLS12, VERSION_TLS13};
use ::certificate::{CFLAG_MUST_EMS, CFLAG_MUST_RENEGO, CFLAG_MUST_TLS13};
use ::client::ClientConfiguration;
use ::client::controller::certificate::Tls12CertificateFields;
use ::client::controller::clienthello::ClientHelloFields;
use ::client::controller::encryptedextensions::Tls13EncryptedExtensionsFields;
use ::common::{CERTTYPE_RPK, CERTTYPE_X509, Ciphersuite, CryptoTracker, Debugger, EarlyHandshakeRep,
	EXT_SERVER_CERTIFICATE_TYPE, HandshakeMessage, HashMessages, HSTYPE_HELLO_RETRY_REQUEST, KeyExchangeType,
	NamesContainer, TlsFailure, TlsVersion};
use ::features::{FLAGS0_ENABLE_TLS12, FLAGS0_ENABLE_TLS13, FLAGS0_REQUIRE_EMS};
use ::logging::bytes_as_hex_block;
use ::messages::{TlsHelloRetryRequest, TlsServerHello};
use ::record::{DiffieHellmanSharedSecret, Tls13HandshakeSecret, Tls13MasterSecret, SessionController};
use ::stdlib::{Box, ToOwned, Vec};

use btls_aux_dhf::{Dhf, DiffieHellmanKey};

pub struct ServerHelloFields
{
	pub cvinfo: TlsCVInfo,					//Server certificate validation info.
	pub might_use_spki_only: bool,				//Maybe try raw SPKI.
	pub features: u32,					//Features supported.
	pub tls13_shares: Vec<Box<DiffieHellmanKey+Send>>,	//The key shares (also abused for TLS 1.2).
	pub ciphersuite_assigned: Option<Ciphersuite>,		//Assigned ciphersuite (used for HRR in 1.3-draft19+)
	pub hs_hash: EarlyHandshakeRep,				//The client hello, plus possible restart (with
								//headers).
	pub group_id: Option<u16>,				//The group ID from HRR.
	pub no_request_sni: bool,				//Don't request SNI.
	pub client_random: [u8; 32],
}

struct RxServerHelloParams<'a>
{
	features: &'a mut u32,
	staple_scts: &'a mut Vec<(bool, Vec<u8>)>,
	tls13_shares: &'a mut Vec<Box<DiffieHellmanKey+Send>>,
	hs_hash: &'a mut EarlyHandshakeRep,
	maybe_send_status: &'a mut bool,
	use_spki_only: &'a mut bool,
	ciphersuite_assigned: Option<Ciphersuite>,
	might_use_spki_only: bool,
	group_id: Option<u16>,
	tls13_hs_secret: &'a mut Option<Tls13HandshakeSecret>,
}

impl ServerHelloFields
{
	//Handle KeyShare extension.
	fn handle_ext_keyshare<'a>(keyshare: (Dhf, &[u8]), features: &mut u32,
		tls13_shares: &mut Vec<Box<DiffieHellmanKey+Send>>, version: TlsVersion, xgroup_id: Option<u16>,
		crypto_algs: &mut CryptoTracker) -> Result<DiffieHellmanSharedSecret, TlsFailure>
	{
		//This can only be present in TLS 1.3, so enable TLS 1.3 mode.
		crypto_algs.set_version(version);
		*features |= CFLAG_MUST_TLS13 | CFLAG_MUST_EMS | CFLAG_MUST_RENEGO;
		let group_id = keyshare.0.to_tls_id();
		let pubkey = keyshare.1;
		fail_if!(xgroup_id.is_some() && xgroup_id != Some(group_id),
			TlsFailure::KeyShareGroupDisagreement(xgroup_id.unwrap_or(0), group_id));
		{
			//Find matching key. If not found, that is an error.
			for i in tls13_shares.iter_mut() {
				if i.tls_id() != group_id { continue; }
				//Found it, perform ECDH operation and add to secret chain. Note that we need to add
				//zero block for PSK.
				let result = DiffieHellmanSharedSecret::new(i, pubkey)?;
				return Ok(result);
			}
			fail!(TlsFailure::BogusKeyShareGroup(group_id));
		}
	}
	//Handle certificate type setting.
	fn handle_certificate_type_setting(ctype: u8, debug: Debugger, use_spki_only: &mut bool) ->
		Result<(), TlsFailure>
	{
		debug!(HANDSHAKE_EVENTS debug, "Server supports Certificate types (type={})", ctype);
		//Check certificate type.
		if ctype == CERTTYPE_X509 {
			//Ignore.
		} else if ctype == CERTTYPE_RPK {
			*use_spki_only = true;
		} else {
			fail!(TlsFailure::BogusSCertType(ctype));
		}
		Ok(())
	}
	//Process a received hello retry request.
	pub fn rx_hello_retry_request<'a,'b>(self, _controller: &mut SessionController,
		emsg: TlsHelloRetryRequest<'a>, msg: &[u8], debug: Debugger, config: &ClientConfiguration,
		_raw_msg: HandshakeMessage<'b>, crypto_algs: &mut CryptoTracker) ->
		Result<HandshakeState2, TlsFailure>
	{
		let mut ciphersuite_assigned = None;	//There can only be one HRR.
		let mut cookie = None;
		let mut hs_hash = self.hs_hash;
		let mut tls13_shares = self.tls13_shares;
		let version = emsg.version;
		match emsg.version {
			TlsVersion::Tls12 => fail!(TlsFailure::BogusHrrTlsVersion(0x0303)),
			_=> (),
		};
		let is_tls12 = version.is_tls12();

		let mut known = false;
		if let Some(csobj) = emsg.ciphersuite {
			//Ciphersuite info is available.
			let hash = csobj.get_prf();
			debug!(HANDSHAKE_EVENTS debug, "Chosen ciphersuite is {:?}", csobj);
			crypto_algs.set_ciphersuite(csobj);
			//Initialize the hash.
			//This only happens in TLS 1.3-Draft19+. There the client hello is also compressed.
			hs_hash.convert_to_hash(hash, debug.clone(), true)?;
			ciphersuite_assigned = Some(csobj);
		}
		//Only supported in 1.3
		known |= !is_tls12 && (config.flags[0] & FLAGS0_ENABLE_TLS13) != 0;
		fail_if!(!known, TlsFailure::HrrTlsVersionUnsupported(if is_tls12 { 2 } else { 3 }));
		debug!(HANDSHAKE_EVENTS debug, "Restarting handshake (TLS version code \
			{:04x})", version.to_code());

		let mut had_cookie_or_something = false;
		if let Some(cookiex) = emsg.cookie {
			//Include cookie in next ClientHello.
			cookie = Some(cookiex.to_owned());
			had_cookie_or_something = true;
			debug!(HANDSHAKE_EVENTS debug, "Restart valid, cookie was missing");
		}
		if let Some(grp_id) = emsg.kex_group {
			//Extract the right group, if any.
			let mut _dhkey = None;
			for i in tls13_shares.drain(..) {
				if i.tls_id() == grp_id {
					//Hit!
					_dhkey = Some(i);
					break;
				}
			}
			debug!(HANDSHAKE_EVENTS debug, "Server requested share for group #{}", grp_id);
			let _dhkey = if let Some(x) = _dhkey {
				if !had_cookie_or_something {
					fail!(TlsFailure::HrrAlreadyHadShare(grp_id));
				}
				x
			} else {
				//Not found, generate one.
				let dhf = match Dhf::by_tls_id(grp_id) {
					Some(y) => y,
					None => fail!(TlsFailure::BogusHrrCurve(grp_id))
				};
				dhf.generate_key().map_err(|x|assert_failure!("Can't generate Diffie-Hellman \
					keypair for group #{}: {}", grp_id, x))?
			};
			tls13_shares.clear();
			tls13_shares.push(_dhkey);
		}
		//This will handle the case where hash has already been initialized right.
		hs_hash.add(HandshakeMessage::new(HSTYPE_HELLO_RETRY_REQUEST, msg), debug.clone())?;

		Ok(HandshakeState2::RetryClientHello(ClientHelloFields{
			hs_hash: hs_hash,
			cookie: cookie,
			might_use_spki_only: self.might_use_spki_only,
			no_request_sni: self.no_request_sni,
			tls13_shares: tls13_shares,
			ciphersuite_assigned: ciphersuite_assigned,
			features: self.features,
			group_id: self.group_id,
			client_random: self.client_random,
			cvinfo: self.cvinfo,
		}))
	}
	//Process recived ServerHello, Returns version code.
	fn rx_server_hello<'a,'b>(controller: &mut SessionController, pmsg: TlsServerHello<'a>,
		names: &mut NamesContainer, debug: Debugger, config: &ClientConfiguration,
		params: RxServerHelloParams<'b>, crypto_algs: &mut CryptoTracker) -> Result<u16, TlsFailure>
	{
		//Version number. Has to be TLS 1.2 or 1.3, and supported.
		let version = pmsg.server_version;
		let realversion = version.to_code();
		let draft_mode = version.draft_code();
		let is_tls12 = version.is_tls12();
		let is_tls13 = !version.is_tls12();
		let mut known = false;
		known |= is_tls12 && (config.flags[0] & FLAGS0_ENABLE_TLS12) != 0;
		known |= is_tls13 && (config.flags[0] & FLAGS0_ENABLE_TLS13) != 0;
		fail_if!(!known, TlsFailure::ShUnsupportedTlsVersion(realversion.saturating_sub(0x301)));

		let random = pmsg.random;
		debug!(CRYPTO_CALCS debug, "Received server random:\n{}", bytes_as_hex_block(&random, ">"));
		if (config.flags[0] & FLAGS0_ENABLE_TLS13) != 0 && is_tls12 {
			const DOWNGRADE_SENTINEL: [u8; 8] = [0x44, 0x4F, 0x57, 0x4E, 0x47, 0x52, 0x44, 0x01];
			let sentinel = &random[24..32];
			fail_if!(sentinel == &DOWNGRADE_SENTINEL, TlsFailure::Tls13DowngradeAttack);
		}
		//Read ciphersuite, and initialize PRF.
		let (key_exchange, protection, hash, csobj) = match pmsg.ciphersuite {
			x => (x.get_key_exchange(), x.get_protector(), x.get_prf(), x),
		};

		if params.ciphersuite_assigned.is_none() {
			//No ciphersuite assigned yet.
			debug!(HANDSHAKE_EVENTS debug, "Chosen ciphersuite is {:?}", csobj);
			crypto_algs.set_ciphersuite(csobj);
			//This is only reached by in the following cases:
			//1) TLS 1.2
			//2) TLS 1.3 pre-draft19.
			//3) No HelloRetryRequest.
			//In any case, don't compress the ClientHello...
			params.hs_hash.convert_to_hash(hash, debug.clone(), false)?;
		} else if params.ciphersuite_assigned != Some(csobj) {
			//Ciphersuite was assigned, but didn't match. this is fatal error.
			fail!(TlsFailure::CiphersuiteDisagreement(params.ciphersuite_assigned.unwrap_or(
				Ciphersuite::random_suite()), csobj));
		}
		let need_tls13 = key_exchange == KeyExchangeType::Tls13;
		fail_if!(need_tls13 && is_tls12, TlsFailure::ShBadTls12Cipher(csobj.to_tls_id()));
		fail_if!(!need_tls13 && is_tls13, TlsFailure::ShBadTls13Cipher(csobj.to_tls_id()));


		if let Some(alpn) = pmsg.alpn {
			let alpn_candidates = &config.supported_alpn;
			fail_if!(alpn_candidates.iter().filter(|x|*x==&alpn).next().is_none(),
				TlsFailure::BogusAlpnProtocol);
			debug!(HANDSHAKE_EVENTS debug, "Application Layer Protocol is '{}'", alpn);
			names.set_alpn(alpn.to_owned());
		}

		if pmsg.maybe_send_status {
			*params.maybe_send_status = true;
		}

		if let Some(scts) = pmsg.certificate_transparency {
			//Read the SCTs.
			for i in scts.iter() {
				debug!(HANDSHAKE_EVENTS debug, "Received SCT#{} using TLS extension.", params.
					staple_scts.len());
				params.staple_scts.push((false, (*i).to_owned()));
			}
		}

		//Server Certificate type extension.
		if let Some(ctype) = pmsg.cert_type {
			//This was not advertized and thus illegal if might_use_spki_only was false.
			fail_if!(!params.might_use_spki_only, TlsFailure::BogusShExtension(
				EXT_SERVER_CERTIFICATE_TYPE, if is_tls12 { 2 } else { 3 }));
			Self::handle_certificate_type_setting(ctype, debug.clone(), params.use_spki_only)?;
		}
		//Extended Master Secret extension.
		if pmsg.ems_supported {
			debug!(HANDSHAKE_EVENTS debug, "Extended Master Secret supported by the server, using.");
			crypto_algs.enable_ems();
			*params.features |= CFLAG_MUST_EMS;
		}

		let group_used = if let Some(keyshare) = pmsg.keyshare {
			let dh_shared = Self::handle_ext_keyshare(keyshare, params.features,
				params.tls13_shares, version, params.group_id, crypto_algs)?;
			let group_used = dh_shared.get_group();
			crypto_algs.set_kex_group(dh_shared.get_group());
			*params.tls13_hs_secret = Some(Tls13HandshakeSecret::new_dhe(dh_shared, protection, hash,
				false, draft_mode, debug.clone())?);
			Some(group_used)
		} else {
			None
		};
		//Renegotiation info extension.
		if pmsg.renego_ok || is_tls13 { *params.features |= CFLAG_MUST_RENEGO; }

		//Record size limit extension.
		if let Some(maxsize) = pmsg.record_size_limit {
			let maxsize = maxsize as usize;
			debug!(HANDSHAKE_EVENTS debug, "Maximum fragment size supported by peer is {}", maxsize);
			controller.set_max_fragment_length(maxsize);
		}

		fail_if!((config.flags[0] & FLAGS0_REQUIRE_EMS) != 0 && !is_tls13 && !pmsg.ems_supported,
			TlsFailure::VulernableTHS);
		fail_if!(!is_tls13 && !pmsg.renego_ok, TlsFailure::VulernableRenego);

		//If no ALPN, there isn't one (in TLS 1.2!). Also set to use TLS 1.2 records, as some stacks are
		//picky.
		if is_tls12 {
			controller.set_use_tls12_records();
			names.alpn_known();
		}
		debug!(HANDSHAKE_EVENTS debug, "Chosen configuration: Version: {}, Protection:{:?}, Hash: {:?}, \
			Exchange group: {}, Extended Master Secret: {}", version, protection, hash,
			match group_used { Some(x) => format!("{:?}", x), None => "N/A".to_owned() }, if is_tls13 {
			"N/A" } else if pmsg.ems_supported { "Yes" } else { "No" });
		//We do key change in HS demux because we need hash of this message.
		Ok(if is_tls13 { VERSION_TLS13 } else { VERSION_TLS12 })
	}
	pub fn rx_server_hello_w<'a,'b>(self, controller: &mut SessionController, data: TlsServerHello<'a>,
		last: bool, names: &mut NamesContainer, debug: Debugger, config: &ClientConfiguration,
		raw_msg: HandshakeMessage<'b>, crypto_algs: &mut CryptoTracker) ->
		Result<HandshakeState2, TlsFailure>
	{
		//This does not have to make any sense for TLS 1.2.
		let is_old_tls = !data.server_version.draft_code().at_least_19();
		let mut features = self.features;
		let mut staple_scts = Vec::new();
		let mut tls13_shares = self.tls13_shares;
		let mut hs_hash = self.hs_hash;
		let mut maybe_send_status = false;
		let mut use_spki_only = false;
		let mut handshake_secret = None;
		let server_random = data.random;
		let ciphersuite = data.ciphersuite;
		let is_ems = data.ems_supported;
		let version = Self::rx_server_hello(controller, data, names, debug.clone(), config,
			RxServerHelloParams{features: &mut features, staple_scts: &mut staple_scts, tls13_shares:
			&mut tls13_shares, hs_hash: &mut hs_hash, maybe_send_status: &mut maybe_send_status,
			use_spki_only: &mut use_spki_only, ciphersuite_assigned: self.ciphersuite_assigned,
			might_use_spki_only: self.might_use_spki_only, group_id: self.group_id, tls13_hs_secret:
			&mut handshake_secret}, crypto_algs)?;
		fail_if!(version >= 0x0304 && !last, TlsFailure::ServerHelloNotAlignedTls13);

		//Unwrap the handshake hash and add server hello.
		let mut hs_hash = hs_hash.unwrap_hash()?;
		hs_hash.add(raw_msg, debug.clone())?;

		Ok(match version {
			VERSION_TLS12 => {
				HandshakeState2::Tls12Certificate(Tls12CertificateFields {
					use_spki_only: use_spki_only,
					tls13_shares: tls13_shares,
					staple_scts: staple_scts,
					maybe_send_status: maybe_send_status,
					features: features,
					client_random: self.client_random,
					server_random: server_random,
					hs_hash: hs_hash,
					ems_enabled: is_ems,
					protection: ciphersuite.get_protector(),
					prf: ciphersuite.get_prf(),
					cvinfo: self.cvinfo,
				})
			},
			VERSION_TLS13 => {
				let handshake_secret = match handshake_secret {
					Some(x) => x,
					None => fail!(TlsFailure::Tls13NoKeyShare)
				};
				let sh_hash = hs_hash.checkpoint()?;
				let server_hs_write = handshake_secret.traffic_secret_in(&sh_hash, debug.clone())?;
				let client_hs_write = handshake_secret.traffic_secret_out(&sh_hash, debug.clone())?;
				controller.set_protector(client_hs_write.to_protector(debug.clone())?);
				controller.set_deprotector(server_hs_write.to_deprotector(debug.clone())?);
				debug!(HANDSHAKE_EVENTS debug, "Received TLS 1.3 serverhello, \
					downstream&upstream encryption enabled");
				let master_secret = Tls13MasterSecret::new(handshake_secret, debug.clone())?;

				HandshakeState2::Tls13EncryptedExtensions(Tls13EncryptedExtensionsFields {
					cvinfo: self.cvinfo,
					features: self.features,
					hs_secrets: Tls13HsSecrets {
						master_secret: master_secret,
						client_hs_write: client_hs_write,
						server_hs_write: server_hs_write,
					},
					hs_hash: hs_hash,
					is_old_tls: is_old_tls,
				})
			},
			x =>  fail!(TlsFailure::UnsupportedVersion(x))
		})
	}
}
