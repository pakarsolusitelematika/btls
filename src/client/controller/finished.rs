use super::{HandshakeState2, Tls12ShowtimeFields, Tls13C2ndFSecrets, Tls13HsSecrets, Tls13ShowtimeFields,
	TlsClientCert};
use super::certificate::Tls13ClientCertificateFields;
use ::common::{Debugger, emit_handshake, HandshakeHash, HandshakeMessage, HashMessages, HSTYPE_FINISHED,
	NamesContainer, TlsFailure};
use ::record::{SessionBase, SessionController, Tls12MasterSecret};

pub struct Tls12ClientFinishedFields
{
	pub bypass_certificate_validation: bool,	//Certificate validation was bypassed.
	pub master_secret: Tls12MasterSecret,		//The master secret.
	pub sig_verified: bool,				//Exchange Signature verified.
	pub hs_hash: HandshakeHash,
}

pub struct Tls12ServerFinishedFields
{
	pub bypass_certificate_validation: bool,	//Certificate validation was bypassed.
	pub master_secret: Tls12MasterSecret,		//The master secret.
	pub sig_verified: bool,				//Exchange Signature verified.
	pub hs_hash: HandshakeHash,
}

pub struct Tls13ServerFinishedFields
{
	pub bypass_certificate_validation: bool,	//Certificate validation was bypassed.
	pub hs_secrets: Tls13HsSecrets,
	pub requested_certificate: bool,		//Certificate has been requested.
	pub sig_verified: bool,			//Exchange Signature verified.
	pub hs_hash: HandshakeHash,
	pub cc: TlsClientCert,
}

pub struct Tls13ClientFinishedFields
{
	pub hs_secrets2: Tls13C2ndFSecrets,
	pub hs_hash: HandshakeHash,
}

impl Tls13ServerFinishedFields
{
	//Handle received Finished message.
	pub fn rx_finished<'a>(self, controller: &mut SessionController, msg: &[u8], names: &mut NamesContainer,
		debug: Debugger, raw_msg: HandshakeMessage<'a>) -> Result<HandshakeState2, TlsFailure>
	{
		//The finished message just consists of the MAC, so no need to parse it.
		self.hs_secrets.server_hs_write.check_finished(&self.hs_hash.checkpoint()?, msg, debug.clone())?;
		if !self.bypass_certificate_validation { names.release_server_names(); }
		sanity_check!(self.sig_verified, "Interlock missing signature verification");

		let mut hs_hash = self.hs_hash;
		hs_hash.add(raw_msg, debug.clone())?;

		let hs_hash = hs_hash;
		let app_hash = hs_hash.checkpoint()?;
		let server_app_write = self.hs_secrets.master_secret.traffic_secret_in(&app_hash, debug.clone())?;
		let client_app_write = self.hs_secrets.master_secret.traffic_secret_out(&app_hash, debug.clone())?;
		let extractor = self.hs_secrets.master_secret.extractor(&app_hash, debug.clone())?;
		controller.set_deprotector(server_app_write.to_deprotector(debug.clone())?);
		controller.set_extractor(extractor);
		debug!(HANDSHAKE_EVENTS debug, "Received Finished, downstream encryption changing keys");
		let keys = Tls13C2ndFSecrets {
			client_app_write: client_app_write,
			client_hs_write: self.hs_secrets.client_hs_write,
			server_app_write: server_app_write,
		};
		Ok(if self.requested_certificate {
			HandshakeState2::Tls13ClientCertificate(Tls13ClientCertificateFields {
				hs_secrets2: keys,
				hs_hash: hs_hash,
				cc: self.cc,
			})
		} else {
			HandshakeState2::Tls13ClientFinished(Tls13ClientFinishedFields {
				hs_secrets2: keys,
				hs_hash: hs_hash,
			})
		})
	}
}

impl Tls13ClientFinishedFields
{
	//Transmit a Finished message.
	pub fn tx_finished(self, base: &mut SessionBase, debug: Debugger, state: &str) ->
		Result<HandshakeState2, TlsFailure>
	{
		let mut hs_hash = self.hs_hash;
		let htype = HSTYPE_FINISHED;
		let mac = self.hs_secrets2.client_hs_write.generate_finished(&hs_hash.checkpoint()?, debug.clone())?;
		emit_handshake(debug.clone(), htype, mac.as_ref(), base, &mut hs_hash, state)?;
		base.change_protector(self.hs_secrets2.client_app_write.to_protector(debug.clone())?);
		debug!(HANDSHAKE_EVENTS debug, "Sent Finished, upstream encryption \
			changing keys");
		//hs_hash at this point is the final handshake hash, e.g. for RMS.
		Ok(HandshakeState2::Tls13Showtime(Tls13ShowtimeFields {
			rekey_in_progress: false,
			client_app_write: self.hs_secrets2.client_app_write,
			server_app_write: self.hs_secrets2.server_app_write,
		}))
	}
}

impl Tls12ClientFinishedFields
{
	//Transmit a Finished message.
	pub fn tx_finished(self, base: &mut SessionBase, debug: Debugger, state: &str) ->
		Result<HandshakeState2, TlsFailure>
	{
		base.send_message_fragment(20, &[1]);
		base.change_protector(self.master_secret.get_protector(debug.clone())?);
		debug!(HANDSHAKE_EVENTS debug, "Sent ChangeCipherSpec, upstream encryption enabled");
		//The CCS message is not hashed.

		let mut hs_hash = self.hs_hash;
		let htype = HSTYPE_FINISHED;
		let mac = self.master_secret.generate_finished(&hs_hash.checkpoint()?, debug.clone())?;
		emit_handshake(debug.clone(), htype, mac.as_ref(), base, &mut hs_hash, state)?;
		base.change_extractor(self.master_secret.get_exporter(debug)?);
		Ok(HandshakeState2::Tls12ServerChangeCipherSpec(Tls12ServerChangeCipherSpecFields{
			bypass_certificate_validation:self.bypass_certificate_validation,
			master_secret: self.master_secret,
			sig_verified:self.sig_verified,
			hs_hash: hs_hash,
		}))
	}
}

impl Tls12ServerFinishedFields
{
	//Handle received Finished message.
	pub fn rx_finished<'a>(self, _controller: &mut SessionController, msg: &[u8], names: &mut NamesContainer,
		debug: Debugger, raw_msg: HandshakeMessage<'a>) -> Result<HandshakeState2, TlsFailure>
	{
		let mut hs_hash = self.hs_hash;
		//The finished message just consists of the MAC, so no need to parse it.
		self.master_secret.check_finished(&hs_hash.checkpoint()?, debug.clone(), msg)?;
		if !self.bypass_certificate_validation { names.release_server_names(); }
		sanity_check!(self.sig_verified, "Interlock missing signature verification");

		//The final handshake hash.
		hs_hash.add(raw_msg, debug)?;
		Ok(HandshakeState2::Tls12Showtime(Tls12ShowtimeFields{
		}))
	}
}

pub struct Tls12ServerChangeCipherSpecFields
{
	pub sig_verified: bool,				//Exchange Signature verified.
	pub bypass_certificate_validation: bool,	//Certificate validation was bypassed.
	pub master_secret: Tls12MasterSecret,		//The master secret.
	pub hs_hash: HandshakeHash,
}

impl Tls12ServerChangeCipherSpecFields
{
	//Handle received ChangeCipherSpec message.
	pub fn rx_change_cipher_spec(self, controller: &mut SessionController, debug: Debugger)
		-> Result<HandshakeState2, TlsFailure>
	{
		//Not real handshake message, thus not added to the hash.
		controller.set_deprotector(self.master_secret.get_deprotector(debug.clone())?);
		debug!(HANDSHAKE_EVENTS debug, "Received ChangeCipherSpec, downstream encryption enabled");
		Ok(HandshakeState2::Tls12ServerFinished(Tls12ServerFinishedFields{
			bypass_certificate_validation:self.bypass_certificate_validation,
			sig_verified:self.sig_verified,
			master_secret: self.master_secret,
			hs_hash: self.hs_hash,
		}))
	}
}
