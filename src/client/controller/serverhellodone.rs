use super::HandshakeState2;
use ::client::controller::clientkeyexchange::Tls12ClientKeyExchangeFields;
use ::common::{Debugger, HandshakeHash, HandshakeMessage, HashMessages, TlsFailure};
use ::record::Tls12PremasterSecret;

use btls_aux_dhf::DhfPublicKey;

pub struct Tls12ServerHelloDoneFields
{
	pub pubkey: DhfPublicKey,			//Public key.
	pub sig_verified: bool,				//Exchange Signature verified.
	pub bypass_certificate_validation: bool,	//Certificate validation was bypassed.
	pub premaster_secret: Tls12PremasterSecret,	//Premaster secret.
	pub client_random: [u8; 32],			//The client random.
	pub server_random: [u8; 32],			//The server random.
	pub hs_hash: HandshakeHash,
}

impl Tls12ServerHelloDoneFields
{
	//Recieve a certificate message.
	pub fn rx_certificate_request<'a>(self, _data: &[u8], debug: Debugger, raw_msg: HandshakeMessage<'a>) ->
		Result<HandshakeState2, TlsFailure>
	{
		//CertificateRequest.
		let mut hs_hash = self.hs_hash;
		hs_hash.add(raw_msg, debug)?;
		Ok(HandshakeState2::Tls12ServerHelloDone(true, Tls12ServerHelloDoneFields {
			pubkey: self.pubkey,
			bypass_certificate_validation: self.bypass_certificate_validation,
			sig_verified: self.sig_verified,
			premaster_secret: self.premaster_secret,
			client_random: self.client_random,
			server_random: self.server_random,
			hs_hash: hs_hash,
		}))
	}
	//Recieve a server_hello_done message.
	pub fn rx_server_hello_done(self, data: &[u8], certificate_reqd: bool, debug: Debugger,
		raw_msg: HandshakeMessage) -> Result<HandshakeState2, TlsFailure>
	{
		//ServerHelloDone. This message does nothing.
		let mut hs_hash = self.hs_hash;
		hs_hash.add(raw_msg, debug)?;

		fail_if!(data.len() != 0, TlsFailure::ServerHelloDoneNotEmpty(data.len()));
		let inner = Tls12ClientKeyExchangeFields {
			pubkey: self.pubkey,
			bypass_certificate_validation: self.bypass_certificate_validation,
			sig_verified: self.sig_verified,
			premaster_secret: self.premaster_secret,
			client_random: self.client_random,
			server_random: self.server_random,
			hs_hash: hs_hash,
		};
		Ok(if certificate_reqd {
			HandshakeState2::Tls12ClientCertificate(inner)
		} else {
			HandshakeState2::Tls12ClientKeyExchange(inner)
		})
	}
}
