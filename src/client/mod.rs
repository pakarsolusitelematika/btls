use ::{AbortReason, RxAppdataHandler, Session, TlsConnectionInfo, TxIrqHandler};
use ::certificate::TrustAnchorValue;
use ::client_certificates::{HostSpecificPin, TrustAnchor, TrustedLog};
use ::common::{builtin_killist, handle_blacklist, handle_ctlog, handle_ocsp_maxvalid, handle_trustanchor,
	TlsFailure};
use ::features::{ALLOW_BAD_CRYPTO_NAME, CONFIG_PAGES, ConfigFlagsError, ConfigFlagsErrorKind, ConfigFlagsValue,
	DEFAULT_FLAGS, lookup_flag, set_combine_flags_set, set_combine_flags_clear,
	split_config_str};
use ::logging::Logging;
use ::record::{ControlHandler, SessionBase, SessionController};
use ::server_certificates::CertificateSigner;
use ::stdlib::{Arc, Box, BTreeMap, Deref, DerefMut, IoError, IoErrorKind, IoRead, IoResult, IoWrite,
	Mutex, String, ToOwned, Vec};

use btls_aux_futures::FutureCallback;

mod controller;
use self::controller::HandshakeController;

///Client certificate criteria.
pub struct ClientCertificateCriteria
{
	///The allowed exchange signature algorithms.
	pub ee_algorithms: u64,
	///The allowed chain algorithms.
	pub algorithms: u64,
}

///Callback to call when server requests a client certificate.
///
///This trait is base interface for callback called when TLS 1.3 certificate request is received.
///
///This callback sets the client certificate that is sent back, if any.
pub trait ClientCertificateCallback
{
	///Received a TLS 1.3 CertificateRequest message.
	///
	///This method is called when TLS 1.3 CertificateRequest message, containing criteria `criteria` is received.
	///It needs to decide what certificate (if any) to send back.
	///
	///Note that this is not called for TLS 1.2 CertificateRequest messages, if one is received, authentication
	///will be always refused.
	///
	///Also note that the signature algorithm used by the signer must be among the algorithms allowed in
	///`criteria.ee_algorithms`.
	///
	///To send back client certificate, return `Some(certificate)`. To refuse client authentication, return
	///`None`.
	fn on_certificate_request(&mut self, criterial: ClientCertificateCriteria) ->
		Option<Box<CertificateSigner+Send>>;
	///Return a clone of this object.
	fn clone_self(&self) -> Box<ClientCertificateCallback+Send>;
}

///Client configuration.
///
///This structure contains various parts of the client configuration.
///
///# Setting trust anchors and creating configuration object (required).
///
///You can set up trust anchors and create the configuration like:
///
///```no-run
///let mut talist = Vec::new();
///talist.push(TrustAnchor::from_certificate_file("ca1.crt")?);
///talist.push(TrustAnchor::from_certificate_file("ca2.crt")?);
///let mut config = ClientConfiguration::from(&talist[..]);
///```
///
///If you want to "burn" the key into executable image:
///
///```no-run
///let mut talist = Vec::new();
///let cert = include_bytes!("../certificate/test3.der");
///talist.push(TrustAnchor::from_certificate(&mut &cert[..])?);
///let mut config = ClientConfiguration::from(&talist[..]);
///```
///
///
///Alternatively, if you are using host-specific pinning with trust anchors, you can also use it as follows:
///
///```no-run
///let mut config = ClientConfiguration::new();
///```
///
///This is handy if you are e.g. implementing DANE (RFC6698) validation (but don't use it for HPKP (RFC7469), see
///below).
///
///# Setting supported ALPNs (optional):
///
///You can set up supported ALPNs like follows:
///
///```no-run
///config.add_alpn("h2");
///config.add_alpn("http/1.1");
///```
///
///# Setting require EMS (optional):
///
///You can set EMS to be required as follows:
///
///```no-run
///config.set_flags(0, FLAGS0_REQUIRE_EMS);
///```
///
///This is useful if you are going to use exporters, since those won't work without EMS.
///
///Note: Negotiating TLS 1.3 is taken as sufficient to meet the EMS requirement.
///
///# Setting TLS version range (optional):
///
///Enabling TLS 1.3 support:
///
///```no-run
///config.set_flags(0, FLAGS0_ENABLE_TLS13);
///```
///
///Requiring TLS 1.3
///
///```no-run
///config.set_flags(0, FLAGS0_ENABLE_TLS13);
///config.clear_flags(0, FLAGS0_ENABLE_TLS12);
///```
///
///# Certificate Transparency (optional):
///
///Adding a log:
///
///```no-run
///let ct_log = TrustedLog{...};
///config.add_ct_log(&ct_log);
///```
///
///Requiring Certificate Transparency.
///
///```no-run
///config.set_flags(0, FLAGS0_REQUIRE_CT);
///```
///
///# Making session
///
///When you have it all set up, you can get client session using `.make_session()`.
///
///```no-run
///let mut session = config.make_session(hostname);
///```
///
///Or if you want to specify host-specific pins (mandatory if you used `.new()`):
///
///```no-run
///const SHAHASH1: [u8; 32] = [66;32/*...*/];
///const SHAHASH2: [u8; 32] = [77;32/*...*/];
///let pin1 = HostSpecificPin::trust_server_key_by_sha256(&SHAHASH1, false, true);
///let pin2 = HostSpecificPin::trust_server_key_by_sha256(&SHAHASH2, false, true);
///let pins = [pin1, pin2];
///let mut session = config.make_session_pinned(hostname, &pins[..]);
///```
///
///This is useful for implementing DANE (RFC6698) or HPKP (RFC7469). Or if you want to "just test" without "valid"
///CA certificate.
pub struct ClientConfiguration
{
	//Trust anchor set.
	trust_anchors: BTreeMap<Vec<u8>, TrustAnchorValue>,
	//Supported ALPN's, in order of decreasing perference.
	supported_alpn: Vec<String>,
	//Flags pages.
	flags: [u64; CONFIG_PAGES],
	//Logging config.
	log: Option<(u64, Arc<Box<Logging+Send+Sync>>)>,
	//Certificate Killist.
	killist: BTreeMap<[u8; 32], ()>,
	//Allow SPKI mode.
	no_server_rpk: bool,
	//Trusted logs.
	trusted_logs: Vec<TrustedLog>,
	//OCSP maximum validitiy.
	ocsp_maxvalid: u64,
	//Client certificate callback.
	cc_callback: Option<Box<ClientCertificateCallback+Send>>,
}

impl Clone for ClientConfiguration
{
	fn clone(&self) -> ClientConfiguration
	{
		ClientConfiguration {
			trust_anchors:self.trust_anchors.clone(),
			supported_alpn:self.supported_alpn.clone(),
			flags: self.flags.clone(),
			log:self.log.clone(),
			killist: self.killist.clone(),
			no_server_rpk: self.no_server_rpk.clone(),
			trusted_logs: self.trusted_logs.clone(),
			ocsp_maxvalid: self.ocsp_maxvalid.clone(),
			cc_callback: match &self.cc_callback {
				&Some(ref x) => Some(x.clone_self()),
				&None => None
			},
		}
	}
}


fn convert_trust_anchors(list: &[TrustAnchor]) -> BTreeMap<Vec<u8>, TrustAnchorValue>
{
	let mut ret = BTreeMap::new();
	for i in list {
		let (name, content) = i.to_internal_form();
		ret.insert(name, content);
	}
	ret
}


impl<'a> From<&'a [TrustAnchor]> for ClientConfiguration
{
	fn from(anchors: &'a [TrustAnchor]) -> ClientConfiguration
	{
		ClientConfiguration {
			trust_anchors: convert_trust_anchors(anchors),
			supported_alpn: Vec::new(),
			flags: DEFAULT_FLAGS,
			log: None,
			killist: builtin_killist(),
			no_server_rpk: false,
			trusted_logs: Vec::new(),
			ocsp_maxvalid: 7 * 60 * 60 * 24,	//7 days.
			cc_callback: None,
		}
	}
}


impl ClientConfiguration
{
	///Return a default client configuration with empty trust anchor list.
	///
	///Note that in order to successfully create a session out of this, you need to use `.make_session_pinned()`
	///specifying at least one pin with TA flag set (`trust_server_key_by_sha256` and `from_tlsa_raw` (when
	///first byte is 2 or 3) constructors both set the TA flag).
	pub fn new() -> ClientConfiguration
	{
		ClientConfiguration {
			trust_anchors: BTreeMap::new(),
			supported_alpn: Vec::new(),
			flags: DEFAULT_FLAGS,
			log: None,
			killist: builtin_killist(),
			no_server_rpk: false,
			trusted_logs: Vec::new(),
			ocsp_maxvalid: 7 * 60 * 60 * 24,	//7 days.
			cc_callback: None,
		}
	}
	///Add a trust anchor to trust pile.
	///
	///This method adds trust anchor `anchor` to the trust anchor pile. This pile is used for verifying server
	///certificates.
	///
	///Trust anchors can also be added via the `trustanchor=` option in configuration string (see the method
	///`.config_flags`).
	///
	///The default trust pile contains any trust anchors set when creating the configuration object.
	pub fn add_trust_anchor(&mut self, anchor: &TrustAnchor)
	{
		let (name, content) = anchor.to_internal_form();
		self.trust_anchors.insert(name, content);
	}
	////Set the OCSP maximum validity limit.
	///
	///This method sets the OCSP maximum validity limit `limit` (expressed in seconds).
	///
	///This limit is used in two ways:
	///
	/// - If OCSP response lives longer than this limit, the OCSP response is not considered valid.
	/// - If certificate lives at most this limit, no OCSP response is ever required. This is even if
	///OCSP required flag is set, or if certificate is marked must-staple.
	///
	///This limit can also be modified by `ocsp-maxvalid=` option in configuration string (see the method
	///`.config_flags`).
	///
	///The default value is 604,800 seconds (i.e., 7 days).
	pub fn set_ocsp_maxvalidity(&mut self, limit: u64)
	{
		self.ocsp_maxvalid = limit;
	}
	///Set the client certificate callback and enable client certificates.
	///
	///This method sets the client certificate callback to `func`, enabling client certificate support.
	///
	///When this callback is set, TLS 1.3 CertificateRequest message causes the callback to be called.
	///The selected certificate is then sent back as client certificate (including the OCSP and SCT staples,
	///if any). Or alternatively, the callback may refuse authentication, then a refusal to authenticate is
	///signaled to the server.
	///
	///TLS 1.2 client authentication is not supported. If TLS 1.2 CertificateRequest message is sent, this
	///callback is not called and authentication is always refused.
	///
	///The default is to always refuse authentication if asked via CertificateRequest.
	pub fn set_client_certificate(&mut self, func: Box<ClientCertificateCallback+Send>)
	{
		self.cc_callback = Some(func);
	}
	///Add supported ALPN.
	///
	///Add a new least-preferred ALPN value `alpn` as supported.
	///
	///When ALPN value is marked as supported, it is sent to the server for possible choice. Then ALPN values
	///are sent in the order added.
	///
	///The default is no ALPN values recognized (do not send ALPN extension).
	pub fn add_alpn(&mut self, alpn: &str)
	{
		self.supported_alpn.push(alpn.to_owned());
	}
	///Set flags.
	///
	///This method sets flags specified by bitmask `mask` on configuration page `page`.
	///
	///Flags can affect various options in server behavior. See module [`features`] for the flag constants.
	///
	///If nonexistent page is specified, this method does nothing.
	///
	///[`features`]: features/index.html
	pub fn set_flags(&mut self, page: usize, mask: u64)
	{
		if page < CONFIG_PAGES { self.flags[page] = set_combine_flags_set(page, mask, self.flags[page]); }
	}
	///Clear flags.
	///
	///This method clears flags specified by bitmask `mask` on configuration page `page`.
	///
	///Flags can affect various options in server behavior. See module [`features`] for the flag constants.
	///
	///If nonexistent page is specified, this method does nothing.
	///
	///[`features`]: features/index.html
	pub fn clear_flags(&mut self, page: usize, mask: u64)
	{
		if page < CONFIG_PAGES { self.flags[page] = set_combine_flags_clear(page, mask, self.flags[page]); }
	}
	///Set/Clear flags according to config string.
	///
	///This method sets or clears configuration options according to configuration string `config` (presumably
	///read from configuration file). If any errors are encountered during processing, the error callback
	///`error_cb` is called to report those errors (possibly multiple times).
	///
	///The configuration string consists of comma-separated elements, with each element being one of:
	///
	/// * flagname -> Enable flag named `flagname`.
	/// * +flagname -> Enable flag named `flagname`.
	/// * -flagname -> Disable flag named `flagname`.
	/// * !flagname -> Disable flag named `flagname`.
	/// * setting=value -> Set setting `setting` to value `value`.
	///
	///Use backslashes to escape backslash or comma in the value.
	///
	///# The following flags are supported:
	///
	/// * `tls12` -> Enable TLS 1.2 support.
	/// * `tls13` -> Enable TLS 1.3 support.
	/// * `require-ems` -> Require servers to support Extended Master Secret or TLS 1.3.
	/// * `require-ct` -> Require server certificates to have Certificate Transparency staple.
	/// * `require-ocsp` -> Require server certificates to have OCSP staple (if not shortlived).
	/// * `assume-hw-aes` -> Assume hardware AES-GCM support is present.
	/// * `debug-no-shares` -> Don't send initial shares for TLS 1.3 (only useful for debugging).
	///
	///# The following settings are supported:
	///
	/// * `blacklist` -> Blacklist specified SPKI hash (specified as 64 hexadecimal digit representation of
	///SHA-256 hash of the SubjectPublicKeyInfo).
	/// * `trustanchor` -> Read the specified file in DER (possibly concatenated from multiple certificates) or
	///PEM format as series of trust anchors to add.
	/// * `ctlog` -> Read the specified file as series of CT logs (one per line) to add.
	/// * `ocsp-maxvalid` -> Set the OCSP maximum validity limit in seconds.
	///
	///# Format of ctlog files:
	///
	///ctlog files consists of one or more lines, with each line containing a trusted CT log. Each line has one
	///of two forms:
	///
	/// * `v1:<key>:<expiry>:<name>`
	/// * `v2:<id>/<hash>:<key>:<expiry>:<name>`
	///
	///Where:
	///
	/// * `<id>` is base64 encoding of DER encoding the log OID, without the DER tag or length.
	/// * `<hash>` is the log hash function. Currently only `sha256` is supported.
	/// * `<key>`is base64 encoding of log key formatted as X.509 SubjectPublicKeyInfo.
	/// * `<expiry>` is the time log expired in seconds since Unix epoch, or blank if log has not expired yet.
	///Certificate-stapled SCTs are accepted up to expiry date from expired logs, other kinds of SCTs are not
	///accepted.
	/// * `<name>` is the name of the log.
	pub fn config_flags<F>(&mut self, config: &str, mut error_cb: F) where F: FnMut(ConfigFlagsError)
	{
		split_config_str(self, config, |objself, name, value, entry| {
			if if name == "blacklist" {
				config_strval!(handle_blacklist, value, &mut error_cb, entry, &mut objself.killist)
			} else if name == "trustanchor" {
				config_strval!(handle_trustanchor, value, &mut error_cb, entry, &mut objself.
					trust_anchors)
			} else if name == "ctlog" {
				config_strval!(handle_ctlog, value, &mut error_cb, entry, &mut objself.
					trusted_logs)
			} else if name == "ocsp-maxvalid" {
				config_strval!(handle_ocsp_maxvalid, value, &mut error_cb, entry, &mut objself.
					ocsp_maxvalid)
			} else {
				false
			} {
				error_cb(ConfigFlagsError{entry:entry,
					kind:ConfigFlagsErrorKind::ArgumentRequired(name)});
				return;
			}
			if let Some((page, mask)) = lookup_flag(name) {
				match value {
					ConfigFlagsValue::Disabled => objself.clear_flags(page, mask),
					ConfigFlagsValue::Enabled if name == ALLOW_BAD_CRYPTO_NAME =>
						//Allow-bad-crypto is not supported clientside.
						error_cb(ConfigFlagsError{entry:entry,
							kind:ConfigFlagsErrorKind::NoEffect(name)}),
					ConfigFlagsValue::Enabled => objself.set_flags(page, mask),
					ConfigFlagsValue::Explicit(_) => error_cb(ConfigFlagsError{entry:entry,
						kind:ConfigFlagsErrorKind::NoArgument(name)})
				}
			} else {
				error_cb(ConfigFlagsError{entry:entry,
					kind:ConfigFlagsErrorKind::UnrecognizedSetting(name)
				});
			}
		});
	}
	///Set debugging mode.
	///
	///This method enables debugging mode, with debug event mask of `mask` (constructed from bitwise OR of 
	///various `DEBUG_*` constatnts from module [`logging`]) and debug sink `log`.
	///
	///Note that enabling `DEBUG_CRYPTO_CALCS` class requires a build with `enable-key-debug` cargo feature,
	///otherwise that debugging event is just ignored.
	///
	///[`logging`]: logging/index.html
	pub fn set_debug(&mut self, mask: u64, log: Box<Logging+Send+Sync>)
	{
		self.log = Some((mask, Arc::new(log)));
	}
	///Add a new trusted certificate transparency log.
	///
	///This method adds certificate transprancy log `log` to the list of trusted certificate transparency logs.
	///This list is used for verifying certificate transparency proofs in server certificates.
	///
	///CT logs can also be added via the `ctlog=` option in configuration string (see the method
	///`.config_flags`).
	///
	///The default is not to have any trusted CT logs.
	pub fn add_ct_log(&mut self, log: &TrustedLog)
	{
		self.trusted_logs.push(log.clone());
	}
	///Add a new certificate key blacklist entry.
	///
	///This method adds a blacklist entry for certificate key hash `spkihash` (a 32-byte SHA-256 hash of
	///certificate SubjectPublicKeyInfo).
	///
	///If the key hash of any sent client certificate chain entry matches any hash on the blacklist, the
	///certificate validation is rejected and connection fails.
	///
	///Additionally, if trust anchor matches blacklist entry, that trust anchor is not used.
	///
	///Default is not to have any blacklist entries.
	pub fn blacklist(&mut self, spkihash: &[u8])
	{
		let mut x = [0; 32];
		if spkihash.len() != x.len() { return; }
		x.copy_from_slice(spkihash);
		self.killist.insert(x, ());
	}
	///Disallow using raw SPKI for server authentication.
	///
	///This method disallows using raw SPKI for server authentication.
	///
	///When using raw SPKI is disallowed, the client won't advertise support for raw public keys, even if it has
	///trust anchor pins that match server SPKIs (which would normally cause advertiment for RPKs).
	///
	///This is unlikely to be needed, unless the application is a browser, since most protocols can handle
	///server receiving misdirected requests without an issue.
	///
	///Note that all pins set continue to be valid.
	pub fn disallow_server_rpk(&mut self)
	{
		self.no_server_rpk = true;
	}
	///Make a new session.
	///
	///This methods creates a new client connection to host with name `host` (this name is sent as SNI).
	///
	///On success, returns a newly created connection.
	///
	///On failure, fails with an appropriate error. In practicular this fails if there are no items in trusted
	///root certificate list.
	pub fn make_session(&self, host: &str) -> Result<ClientSession, TlsFailure>
	{
		fail_if!(self.trust_anchors.len() == 0, TlsFailure::NoTrustAnchors);
		let controller = HandshakeController::new(&self, host, self.log.clone());
		Ok(self.make_session_tail(controller))
	}
	///Make a new session with host-specific pins.
	///
	///This method creates a new client connection to host with name `host` (this name is sent as SNI) and
	///host-specific pins `pins`.
	///
	///This is useful for implementing DANE (RFC6698) and HPKP (RFC7469)
	///
	///On success, returns a newly created connection.
	///
	///On failure, fails with an appropriate error. In practicular this fails if there are no items in trusted
	///root certificate list nor there is any trust anchor pin.
	pub fn make_session_pinned(&self, host: &str, pins: &[HostSpecificPin]) ->
		Result<ClientSession, TlsFailure>
	{
		let extra_ta_count = pins.iter().filter(|x|x.ta).count();
		//We want to check if the sum is bigger than 0.
		fail_if!(self.trust_anchors.len().saturating_add(extra_ta_count) == 0, TlsFailure::NoTrustAnchors);
		let controller = HandshakeController::new_pinned(&self, host, pins, self.log.clone());
		Ok(self.make_session_tail(controller))
	}
	///Make a client session WITH NO CHECKING WHATSOEVER.
	///
	///This method makes a client session that does not check for certificates. It is meant for connections that
	///are authenticated via TLS-EXPORTER values.
	///
	///The following restrictions apply to these connections:
	///
	/// * SNI is disabled, and will not be sent.
	/// * RPK support indication is forcibly enabled.
	/// * The "EMS support required" is forcibly enabled (i.e. the server MUST support either EMS or TLS 1.3,
	///or the connection will fail).
	/// * get_server_names() never succeeds.
	///
	///# DANGER:
	///
	///Without additional verification via signing TLS-EXPORTER values, this is TRIVIALLY INSECURE. Hijacking
	///connections without verification is VERY EASY. If such connection is not hijacked, it is only because
	///nobody tried.
	pub fn make_session_insecure_nocheck(&self) -> Result<ClientSession, TlsFailure>
	{
		let controller = HandshakeController::new_insecure_nocheck(&self, self.log.clone());
		Ok(self.make_session_tail(controller))
	}
	fn make_session_tail(&self, controller: HandshakeController) -> ClientSession
	{
		let mut sess = _ClientSession {
			base: SessionBase::new(self.log.clone()),
			hs:ClientHandshake(controller)
		};
		//Since client goes first, immediately do the first TX cycle.
		sess.base.do_tx_request();
		//Ok.
		ClientSession(Arc::new(Mutex::new(sess)))
	}
}


struct ClientHandshake(HandshakeController);

impl ControlHandler for ClientHandshake
{
	fn handle_control(&mut self, rtype: u8, msg: &[u8], controller: &mut SessionController)
	{
		self.0.handle_control(rtype, msg, controller)
	}
	fn handle_tx_update(&mut self, controller: &mut SessionController)
	{
		self.0.handle_tx_update(controller)
	}
}

impl ClientHandshake
{
	fn borrow_inner(&self) -> &HandshakeController
	{
		&self.0
	}
	fn borrow_inner_mut(&mut self) -> &mut HandshakeController
	{
		&mut self.0
	}
}

//The internal state, send but pesumably not sync. Not cloneable.
struct _ClientSession
{
	base: SessionBase,
	hs: ClientHandshake,
}

///A client TLS session.
///
///This is the client end of TLS session.
///
///Use `.make_session()` or `.make_session_pinned()` of [`ClientConfiguration`] to create it.
///
///For operations supported, see trait [`Session`].
///
///If the session is cloned, both sessions refer to the same underlying TLS connection. This can be used to refer
///to the same session from multiple threads at once.
///
///[`ClientConfiguration`]: struct.ClientConfiguration.html
///[`Session`]: trait.Session.html
#[derive(Clone)]
pub struct ClientSession(Arc<Mutex<_ClientSession>>);

impl Session for ClientSession
{
	fn set_high_water_mark(&mut self, amount: usize)
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.set_high_water_mark(amount),
			Err(_) => ()
		}
	}
	fn set_send_threshold(&mut self, threshold: usize)
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.set_send_threshold(threshold),
			Err(_) => ()
		}
	}
	fn bytes_available(&self) -> usize
	{
		match self.0.lock() {
			Ok(ref x) => x.base.bytes_available(),
			Err(_) => 0
		}
	}
	fn bytes_queued(&self) -> usize
	{
		match self.0.lock() {
			Ok(ref x) => x.base.bytes_queued(),
			Err(_) => 0
		}
	}
	fn is_eof(&self) -> bool
	{
		match self.0.lock() {
			Ok(ref x) => x.base.is_eof(),
			Err(_) => true
		}
	}
	fn wants_read(&self) -> bool
	{
		match self.0.lock() {
			Ok(ref x) => x.base.wants_read(),
			Err(_) => false
		}
	}
	fn set_tx_irq(&mut self, handler: Box<TxIrqHandler+Send>)
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.set_tx_irq(handler),
			Err(_) => ()
		}
	}
	fn set_appdata_rx_fn(&mut self, handler: Option<Box<RxAppdataHandler+Send>>)
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.set_appdata_rx_fn(handler),
			Err(_) => ()
		}
	}
	fn read_tls<R:IoRead>(&mut self, stream: &mut R) -> IoResult<()>
	{
		//We can't read with lock held.
		const READ_BLOCKSIZE: usize = 16500;
		let mut buf = [0; READ_BLOCKSIZE];
		let rlen = stream.read(&mut buf)?;
		if rlen > buf.len() {
			return Err(IoError::new(IoErrorKind::Other, TlsFailure::from(assert_failure!("Stream \
				read of {} bytes into buffer of {}", rlen, buf.len()))));
		}
		let mut buf = &buf[..rlen];
		let res = match self.0.lock() {
			Ok(ref mut x) => {
				let x = x.deref_mut();
				x.base.read_tls(&mut buf, &mut x.hs)
			},
			Err(_) => fail!(IoError::new(IoErrorKind::Other, TlsFailure::from(assert_failure!(
				"Can't lock TLS session"))))
		};
		self.do_tls_tx_rx_cycle().map_err(|x|IoError::new(IoErrorKind::Other, x))?;
		res
	}
	fn submit_tls_record(&mut self, record: &mut [u8]) -> Result<(), TlsFailure>
	{
		let res = match self.0.lock() {
			Ok(ref mut x) => {
				let x = x.deref_mut();
				x.base.submit_tls_record(record, &mut x.hs)
			},
			Err(_) => sanity_failed!("Can't lock TLS session")
		};
		self.do_tls_tx_rx_cycle()?;
		res
	}
	fn write_tls<W:IoWrite>(&mut self, stream: &mut W) -> IoResult<()>
	{
		//This can happen with waiting for signatures.
		self.do_tls_tx_rx_cycle().map_err(|x|IoError::new(IoErrorKind::Other, x))?;
		let ret = match self.0.lock() {
			Ok(ref mut x) => {
				let x = x.deref_mut();
				x.base.write_tls(stream, &mut x.hs, false)
			}
			Err(_) => fail!(IoError::new(IoErrorKind::Other, TlsFailure::from(assert_failure!(
				"Can't lock TLS session"))))
		};
		ret
	}
	fn zerolatency_write(&mut self, output: &mut [u8], input: &[u8]) -> Result<(usize, usize), TlsFailure>
	{
		//This can happen with waiting for signatures.
		self.do_tls_tx_rx_cycle()?;
		match self.0.lock() {
			Ok(ref mut x) => {
				let x = x.deref_mut();
				x.base.zerolatency_write(&mut x.hs, output, input)
			},
			Err(_) => sanity_failed!("Can't lock TLS session")
		}
	}
	fn estimate_zerolatency_size(&self, outsize: usize) -> usize
	{
		match self.0.lock() {
			Ok(ref mut x) => {
				let x = x.deref();
				x.base.estimate_zerolatency_size(outsize)
			},
			Err(_) => 0
		}
	}
	fn can_tx_data(&self) -> bool
	{
		match self.0.lock() {
			Ok(ref x) => x.base.can_tx_data(),
			Err(_) => false
		}
	}
	fn handshake_completed(&self) -> bool
	{
		match self.0.lock() {
			Ok(ref x) => x.hs.borrow_inner().in_showtime(),
			Err(_) => false,	//Should not happen.
		}
	}
	fn send_eof(&mut self)
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.send_eof(),
			Err(_) => ()
		}
	}
	fn aborted_by(&self) -> Option<AbortReason>
	{
		match self.0.lock() {
			Ok(ref x) => x.base.aborted_by(),
			Err(_) => Some(AbortReason::HandshakeError(TlsFailure::from(assert_failure!(
				"Can't lock TLS session"))))
		}
	}
	fn extractor(&self, label: &str, context: Option<&[u8]>, buffer: &mut [u8]) -> Result<(), TlsFailure>
	{
		match self.0.lock() {
			Ok(ref x) => x.base.extractor(label, context, buffer),
			Err(_) => sanity_failed!("Can't lock TLS session")
		}
	}
	fn get_alpn(&self) -> Option<Option<Arc<String>>>
	{
		match self.0.lock() {
			Ok(ref x) => x.hs.borrow_inner().get_alpn(),
			Err(_) => None		//Shouldn't be here.
		}
	}
	fn get_server_names(&self) -> Option<Arc<Vec<String>>>
	{
		match self.0.lock() {
			Ok(ref x) => x.hs.borrow_inner().get_server_names(),
			Err(_) => None		//Shouldn't be here.
		}
	}
	fn get_sni(&self) -> Option<Option<Arc<String>>>
	{
		match self.0.lock() {
			Ok(x) => x.hs.borrow_inner().get_sni(),
			Err(_) => None		//Shouldn't be here.
		}
	}
	fn connection_info(&self) -> TlsConnectionInfo
	{
		match self.0.lock() {
			Ok(x) => x.hs.borrow_inner().connection_info(),
			Err(_) => TlsConnectionInfo {
				version: 0,
				ems_available: false,
				ciphersuite: 0,
				kex: None,
				signature: None,
				version_str: "",
				protection_str: "",
				hash_str: "",
				exchange_group_str: "",
				signature_str: "",
				validated_ct: false,
				validated_ocsp: false,
				validated_ocsp_shortlived: false,
			}
		}
	}
	fn get_record_size(&self, buffer: &[u8]) -> Option<usize>
	{
		match self.0.lock() {
			Ok(x) => x.base.get_record_size(buffer),
			Err(_) => Some(5)	//Shouldn't be here.
		}
	}
	fn begin_transacted_read<'a>(&self, buffer: &'a mut [u8]) -> Result<&'a [u8], TlsFailure>
	{
		match self.0.lock() {
			Ok(x) => x.base.begin_transacted_read(buffer),
			Err(_) => sanity_failed!("Can't lock TLS session")
		}
	}
	fn end_transacted_read(&mut self, len: usize) -> Result<(), TlsFailure>
	{
		match self.0.lock() {
			Ok(mut x) => x.base.end_transacted_read(len),
			Err(_) => sanity_failed!("Can't lock TLS session")
		}
	}
}

impl IoRead for ClientSession
{
	fn read(&mut self, buf: &mut [u8]) -> IoResult<usize>
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.read(buf),
			Err(_) => fail!(IoError::new(IoErrorKind::Other, TlsFailure::from(assert_failure!(
				"Can't lock TLS session"))))
		}
	}
}

impl IoWrite for ClientSession
{
	fn write(&mut self, buf: &[u8]) -> IoResult<usize>
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.write(buf),
			Err(_) => fail!(IoError::new(IoErrorKind::Other, TlsFailure::from(assert_failure!(
				"Can't lock TLS session"))))
		}
	}
	fn flush(&mut self) -> IoResult<()>
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.flush(),
			Err(_) => fail!(IoError::new(IoErrorKind::Other, TlsFailure::from(assert_failure!(
				"Can't lock TLS session"))))
		}
	}
}

impl ClientSession
{
	fn do_tls_tx_rx_cycle(&mut self) -> Result<(),TlsFailure>
	{
		let mut inner = match self.0.lock() {
			Ok(x) => x,
			Err(_) => sanity_failed!("Can't lock TLS session")
		};
		let mut inner = inner.deref_mut();
		//Do TX cycles if needed.
		while inner.hs.borrow_inner().wants_tx() {
			if inner.hs.borrow_inner_mut().queue_hs_tx(&mut inner.base) {
				inner.base.finish_tx_handshake();
			}
			//Arrange do_tx_request to be called when signature_future settles, if needed.
			inner.hs.borrow_inner_mut().callback_with_signature_future(&mut inner.base, |sb, y|{
				if !y.settled_cb(Box::new(DoTxRequestOnSettle(self.clone()))).is_none() {
					//Already settled.
					sb.do_tx_request();
				}
			});
		}
		//Re-arm the RX if needed. Note that previous TX cycle can have altered the condition.
		if inner.hs.borrow_inner().wants_rx() {
			inner.base.request_rx_handshake();
		}
		Ok(())
	}
}

struct DoTxRequestOnSettle(ClientSession);

type SignerFT = Result<(u16, Vec<u8>),()>;

impl FutureCallback<SignerFT> for DoTxRequestOnSettle
{
	fn on_settled(&mut self, val: SignerFT) -> SignerFT
	{
		match (self.0).0.lock() {
			Ok(ref mut y) => y.base.do_tx_request(),
			Err(_) => ()
		};
		val
	}
}
