use ::{AbortReason, RxAppdataHandler, Session, TlsConnectionInfo, TxIrqHandler};
use ::certificate::{ParsedCertificate, TrustAnchorValue};
use ::client_certificates::{TrustAnchor, TrustedLog};
use ::common::{builtin_killist, handle_blacklist, handle_ctlog, handle_ocsp_maxvalid, handle_trustanchor,
	TlsFailure};
use ::features::{CONFIG_PAGES, ConfigFlagsEntry, ConfigFlagsError, ConfigFlagsErrorKind, ConfigFlagsValue,
	DEFAULT_FLAGS, lookup_flag, set_combine_flags_clear, set_combine_flags_set, split_config_str,
	DEBUG_NO_SHARES_NAME};
use ::logging::Logging;
pub use ::messages::AlpnList;
use ::record::{ControlHandler,SessionBase, SessionController};
use ::server_certificates::{Certificate, CertificateLookup, CertificateSigner};
use ::server_keys::LocalKeyPair;
use ::stdlib::{Arc, Box, BTreeMap, Deref, DerefMut, IoError, IoErrorKind, IoRead, IoResult, IoWrite, Mutex, Range,
	RwLock, String, ToOwned, UNIX_EPOCH, Weak, Vec};
use ::system::{Path, PathBuf, SystemTime};
use ::utils::subslice_to_range;

use btls_aux_assert::AssertFailed;
use btls_aux_futures::FutureCallback;
pub use btls_aux_hash::{ChecksumFunction, HashOutput};
use btls_aux_time::Timestamp;

mod controller;
use self::controller::HandshakeController;

#[derive(Clone)]
struct AcmeChallengeTable
{
	entries: BTreeMap<String, Option<String>>,
}

impl AcmeChallengeTable
{
	fn new() -> AcmeChallengeTable
	{
		AcmeChallengeTable{entries: BTreeMap::new()}
	}
}

fn millisec_clock() -> i64
{
	let now = SystemTime::now();
	let (now, dir) = match now.duration_since(UNIX_EPOCH) {
		Ok(x) => (x, true),
		Err(x) => (x.duration(), false),
	};
	let now = (now.as_secs() * 1000 + (now.subsec_nanos() / 1000000) as u64) as i64;
	if dir { now } else { -now }
}

///Callback to call when ALPN extension is received.
///
///This trait is base interface for callback called when TLS ALPN extension is received.
///
///This callback sets the ALPN used on the connection, if any.
pub trait AlpnCallback
{
	///The ALPN needs to be selected.
	///
	///This method is called when ClientHello with ALPN extension is received. It needs to decide what ALPN
	///is used on the connection. The ALPNs in client hello are passed as `alpn_list`, and the SNI signaled
	///in the client hello is passed as `sni` (`Some(x)` if SNI was sent, or `None` if no SNI was sent).
	///
	///If this method fails, the entiere handshake fails.
	///
	///To select x:th (counting from zero) entry in `alpn_list` as the connection ALPN, return `Ok(Some(x))`.
	///To not send ALPN back at all, return `Ok(None)`.
	///
	///Note that one should not construct `TlsFailure` directly. Use `alpn_error_internal_error()` and
	///`alpn_error_no_overlap()` for constructing TlsFailure objects.
	fn select_alpn<'a>(&mut self, sni: Option<&str>, alpn_list: AlpnList<'a>) ->
		Result<Option<usize>, TlsFailure>;
	///Return a clone of this object.
	fn clone_self(&self) -> Box<AlpnCallback+Send>;
}

///Return assertion failure error.
pub fn alpn_error_internal_error(msg: String) -> Result<(), TlsFailure>
{
	Err(TlsFailure::AssertionFailed(AssertFailed(msg)))
}

///Return no ALPN overlap error.
pub fn alpn_error_no_overlap() -> Result<(), TlsFailure>
{
	Err(TlsFailure::AlpnNoOverlap)
}

fn check_range(backing: &[u8], range: &Range<usize>) -> Result<Range<usize>, ()>
{
	fail_if!(range.end < range.start, ());
	fail_if!(range.end > backing.len(), ());
	fail_if!(range.start > backing.len(), ());
	Ok(range.clone())
}

///Client certificate information.
pub struct ClientCertificateInfo
{
	eecert: Option<Vec<u8>>,
	spki_range: Range<usize>,
	name_range: Range<usize>,
	ocsp: bool,
	sct: bool,
}

impl ClientCertificateInfo
{
	fn new_noauth() -> ClientCertificateInfo
	{
		ClientCertificateInfo
		{
			eecert: None,
			spki_range: 1..0,
			name_range: 1..0,
			ocsp: false,
			sct: false,
		}
	}
	fn new_authd<'a>(eecert: &ParsedCertificate<'a>, reliable_name: bool, ocsp: bool, sct:bool) ->
		ClientCertificateInfo
	{
		let spki_range = subslice_to_range(eecert.entiere, eecert.pubkey).ok().unwrap_or(1..0);
		let name_range = subslice_to_range(eecert.entiere, eecert.subject.as_raw_subject()).ok().unwrap_or(
			1..0);
		ClientCertificateInfo
		{
			eecert: Some(eecert.entiere.to_owned()),
			spki_range: spki_range,
			name_range: if reliable_name { name_range } else { 1..0 },
			ocsp: ocsp,
			sct: sct,
		}
	}
	///Get the subject name.
	///
	///This method obtains the Subject name in the sent client certificate.
	///
	///If no client certificate was sent, or if validating the client certificate chain failed, returns
	///`None`. Otherwise it returns `Some(name)`, where `name` is the PKIX Name structure (including the
	///leading SEQUENCE header) for the Subject of the client certificate.
	pub fn get_subject(&self) -> Option<Vec<u8>>
	{
		match &self.eecert {
			&Some(ref x) => check_range(x, &self.name_range).ok().and_then(|y|Some(x[y].to_owned())),
			&None => None
		}
	}
	///Get the Subject public key.
	///
	///This method obtains the public key in the sent client certificate.
	///
	///If no client certificate was sent, returns `None`. Otherwise it returns `Some(pubkey)`, where `pubkey`
	///is the PKIX SubjectPublicKeyInfo structure (including the leading SEQUENCE header) for the subject of
	///the client certificate.
	///
	///Note that this is returned regardless if the chain validation was successful or not.
	pub fn get_public_key(&self) -> Option<Vec<u8>>
	{
		match &self.eecert {
			&Some(ref x) => check_range(x, &self.spki_range).ok().and_then(|y|Some((&x[y]).to_owned())),
			&None => None
		}
	}
	///Get the Subject public key hash.
	///
	///This method obtains the hash of public key in the sent client certificate, calculated using hash
	///algorithm `hash`.
	///
	///The hash is calculated over the same data as `.get_public_key()` returns. If `.get_public_key()` returns
	///`None`, this returns `None` too.
	pub fn get_public_key_hash(&self, hash: ChecksumFunction) -> Option<HashOutput>
	{
		match &self.eecert {
			&Some(ref x) => check_range(x, &self.spki_range).ok().and_then(|y|hash.calculate(&x[y]).
				ok()),
			&None => None
		}
	}
	///Get the end entity certificate hash.
	///
	///This method obtains the hash of end-entity client certificate, calculated using hash algorithm `hash`.
	///
	///If no client certificate was sent, returns `None`. Otherwise it returns `Some(hash)`, where `hash`
	///is the hash for the end-entity client certificate.
	///
	///Note that this is returned regardless if the chain validation was successful or not.
	pub fn get_ee_certificate_hash(&self, hash: ChecksumFunction) -> Option<HashOutput>
	{
		match &self.eecert {
			&Some(ref x) => hash.calculate(x).ok(),
			&None => None
		}
	}
	///Determine if OCSP staple was present for client certificate (or certificate is shortlived).
	pub fn is_ocsp_stapled(&self) -> bool { self.ocsp }
	///Determine if SCT staple was present for client certificate.
	pub fn is_sct_stapled(&self) -> bool { self.sct }
}

///Callback to call when server requests and receives a client certificate, or refusal.
///
///This trait is base interface for callback called when client authentication is completed or refused.
///
///This callback needs record the client identity received.
pub trait ClientCertificateCallback
{
	///Received client authentication or refusal.
	///
	///This method is called when client provodes (or refuses) a certificate for authentication. The information
	///about the certificate is passed in `info`.
	///
	///This is called after client Finished has been checked.
	///
	///If client certificate callback is set, and TLS 1.2 is negotiated, after client Finished is received,
	///this callback is called, indicating that client authentication was refused (even through no certificate
	///request was sent).
	///
	///Returning `Err(())` aborts the handshake.
	fn on_certificate_auth(&mut self, info: ClientCertificateInfo) -> Result<(), ()>;
	///Return a clone of this object.
	fn clone_self(&self) -> Box<ClientCertificateCallback+Send>;
}

///Server configuration.
///
///This structure contains various parts of the server configuration.
///
///# Creating a configuration:
///
///You can create the configuration object as:
///
///```no-run
///```
///
///# Setting certificate store and creating configuration (required).
///
///You can use [`CertificateDirectory`] as the certificate lookup (unless you need something like "keyless
///SSL" or "LURK SKI"), like this:
///
///```no-run
///let mut cert_store = CertificateDirectory::new(cert_dir_path, logger);
///let mut config = ServerConfiguration::from(&cert_store);
///```
///
///# Setting supported ALPNs (optional):
///
///You can set up supported ALPNs like follows:
///
///```no-run
///config.add_alpn("h2");
///config.add_alpn("http/1.1");
///```
///
///Note that if there are supported ALPNs, and client hello contains the ALPN extension, the client hello will be
///rejected if there is no overlap in ALPN values.
///
///# Setting require EMS (optional):
///
///You can set EMS to be required as follows:
///
///```no-run
///config.set_flags(0, FLAGS0_REQUIRE_EMS);
///```
///
///This is useful if you are going to use exporters, since those won't work without EMS.
///
///Note: If the negotiatied TLS version is 1.3, that is interpretted as sufficient to pass the EMS requirement,
///even if EMS is deprecated in TLS 1.3.
///
///# Setting TLS version range (optional):
///
///Enabling TLS 1.3 support:
///
///```no-run
///config.set_flags(0, FLAGS0_ENABLE_TLS13);
///```
///
///Requiring TLS 1.3.
///
///```no-run
///config.set_flags(0, FLAGS0_ENABLE_TLS13);
///config.clear_flags(0, FLAGS0_ENABLE_TLS12);
///```
///
///# Setting ACME challenge directory (strongly recommended to let admin configure this if binding to port 443
///for an extended time).
///
///```no-run
///config.set_acme_dir(path_from_config);
///```
///
///
///# Making session
///
///When you have it all set up, you can get server session using `.make_session()`.
///
///[`CertificateDirectory`]: server_certificates/struct.CertificateDirectory.html
pub struct ServerConfiguration
{
	//Trust anchor set.
	trust_anchors: BTreeMap<Vec<u8>, TrustAnchorValue>,
	//Certificate lookup routine.
	lookup_cert: Box<CertificateLookup+Send>,
	//Supported ALPN's, in order of decreasing perference.
	supported_alpn: Vec<String>,
	//ALPN selection routine, if any. Overrides supported_alpn.
	alpn_function: Option<Box<AlpnCallback+Send>>,
	//Flags page0.
	flags: [u64; CONFIG_PAGES],
	//The ACME challenge table.
	acme_challenges: Arc<RwLock<AcmeChallengeTable>>,
	//Logging config.
	log: Option<(u64, Arc<Box<Logging+Send+Sync>>)>,
	//Default SNI name.
	default_sni_name: Option<String>,
	//Certificate Killist.
	killist: BTreeMap<[u8; 32], ()>,
	//ACME path.
	acme_path: Option<PathBuf>,
	//Certificate sanity cache.
	sane_certificates: Arc<RwLock<BTreeMap<usize, SaneCertVal>>>,
	//OCSP sanity cache.
	sane_ocsp: Arc<RwLock<BTreeMap<usize, SaneOcspVal>>>,
	//Handshake timeout (milliseconds).
	handshake_timeout: u32,
	//ACME validation key.
	acme_key: Option<Arc<LocalKeyPair>>,
	//Ask client certificate.
	ask_client_cert: Option<Box<ClientCertificateCallback+Send>>,
	//OCSP maximum validitiy.
	ocsp_maxvalid: u64,
	//Trusted logs.
	trusted_logs: Vec<TrustedLog>,
}

struct SaneCertVal
{
	ccref: Weak<Vec<Certificate>>,
	expires: Timestamp,
	flags: u32,
	names: Arc<Vec<String>>,
	issuer: Arc<Vec<u8>>,
	serial: Arc<Vec<u8>>,
}

struct SaneOcspVal
{
	certaddr: usize,
	ocspref: Weak<Vec<u8>>,
	expires: Timestamp,
}

impl Clone for ServerConfiguration
{
	fn clone(&self) -> ServerConfiguration
	{
		ServerConfiguration {
			trust_anchors:self.trust_anchors.clone(),
			lookup_cert:self.lookup_cert.clone_self(),
			supported_alpn:self.supported_alpn.clone(),
			flags: self.flags.clone(),
			acme_challenges:self.acme_challenges.clone(),
			log:self.log.clone(),
			default_sni_name: self.default_sni_name.clone(),
			killist: self.killist.clone(),
			acme_path: self.acme_path.clone(),
			sane_certificates: self.sane_certificates.clone(),
			sane_ocsp: self.sane_ocsp.clone(),
			handshake_timeout: self.handshake_timeout.clone(),
			alpn_function: match &self.alpn_function {
				&Some(ref x) => Some(x.clone_self()),
				&None => None
			},
			acme_key: self.acme_key.clone(),
			ask_client_cert: match &self.ask_client_cert {
				&Some(ref x) => Some(x.clone_self()),
				&None => None
			},
			ocsp_maxvalid: self.ocsp_maxvalid.clone(),
			trusted_logs: self.trusted_logs.clone(),
		}
	}
}

impl<'a> From<&'a CertificateLookup> for ServerConfiguration
{
	fn from(lookup: &'a CertificateLookup) -> ServerConfiguration
	{
		ServerConfiguration {
			trust_anchors: BTreeMap::new(),
			lookup_cert: lookup.clone_self(),
			supported_alpn: Vec::new(),
			flags: DEFAULT_FLAGS,
			acme_challenges: Arc::new(RwLock::new(AcmeChallengeTable::new())),
			log: None,
			default_sni_name: None,
			killist: builtin_killist(),
			acme_path: None,
			sane_certificates: Arc::new(RwLock::new(BTreeMap::new())),
			sane_ocsp: Arc::new(RwLock::new(BTreeMap::new())),
			handshake_timeout: 0,
			alpn_function: None,
			acme_key: LocalKeyPair::new_transient_ecdsa_p256().ok().map(|x|Arc::new(x)),
			ask_client_cert: None,
			ocsp_maxvalid: 7 * 24 * 60 * 60, 	//7 days.
			trusted_logs: Vec::new(),
		}
	}
}

impl ServerConfiguration
{
	///Add a trust anchor to trust pile.
	///
	///This method adds trust anchor `anchor` to the trust anchor pile. This pile is used for verifying client
	///certificates (see method `.set_ask_client_cert` for enabling client certificate support).
	///
	///Trust anchors can also be added via the `trustanchor=` option in configuration string (see the method
	///`.config_flags`).
	///
	///If client certificates are not enabled, the trust pile is not used for anything.
	///
	///The default trust pile is empty.
	pub fn add_trust_anchor(&mut self, anchor: &TrustAnchor)
	{
		let (name, content) = anchor.to_internal_form();
		self.trust_anchors.insert(name, content);
	}
	////Set the OCSP maximum validity limit.
	///
	///This method sets the OCSP maximum validity limit `limit` (expressed in seconds).
	///
	///This limit is used in two ways:
	///
	/// - If OCSP response lives longer than this limit, the OCSP response is not considered valid.
	/// - If certificate lives at most this limit, no OCSP response is ever required. This is even if
	///OCSP required flag is set, or if certificate is marked must-staple.
	///
	///This limit can also be modified by `ocsp-maxvalid=` option in configuration string (see the method
	///`.config_flags`).
	///
	///If client certificates are not enabled, the OCSP maximum validity limit is not used for anything.
	///
	///The default value is 604,800 seconds (i.e., 7 days).
	pub fn set_ocsp_maxvalidity(&mut self, limit: u64)
	{
		self.ocsp_maxvalid = limit;
	}
	///Set the client certificate callback and enable client certificates.
	///
	///This method sets the client certificate callback to `func`, enabling client certificate request.
	///
	///When this callback is set, TLS 1.3 handshakes request a client certificate via TLS CertificateRequest
	///message (TLS 1.2 handshakes do not, since client certificates in TLS 1.2 are not supported). Then after
	///the TLS client Finished message arrives and is verified, this callback is called to report the
	///authentication results (in TLS 1.2, it is unconditionally called reporting that client refused to send
	///a certificate).
	///
	///The default is not ask for client certificate.
	pub fn set_ask_client_cert(&mut self, func: Box<ClientCertificateCallback+Send>)
	{
		self.ask_client_cert = Some(func);
	}
	///Set handshake timeout.
	///
	///This method sets the handshake timeout to `timeout` (in milliseconds). Specifying timeout of 0 disables
	///the timeout.
	///
	///If the handshake has not completed (for TLS 1.2, the handshake completes when server Finished is sent,
	///and for TLS 1.3, the handshake completes when client Finished is received) within the handshake timeout,
	///counting from when session is created (by `.make_session()`), the handshake is considered to have been
	///failed (by timing out).
	///
	///The default is no timeout.
	pub fn set_handshake_timeout(&mut self, timeout: u32)
	{
		self.handshake_timeout = timeout;
	}
	///Set ACME challenge database path.
	///
	///This method sets the directory ACME TLS-SNI-01/TLS-SNI-02 challenge database is to `path`.
	///
	///This is STRONGLY RECOMMENDED to be admin-configurable if you bind to port 443.
	///
	///The ACME challenge database contains the pending ACME TLS-SNI-01/TLS-SNI-02 challenges that will be
	///answered if query comes. Note that loading normal certificates in order to pass those WILL NOT WORK.
	///The handshakes will fail via interlock if you try.
	///
	///The database directory contains symbolic links, named with the hexadecimal part (without the middle
	///dot) of the challenge, and pointing either anywhere (TLS-SNI-01) or to similar hexadecimal part of
	///the response (TLS-SNI-02). Lowercase hexadecimal digits are always used.
	///
	///Setting this database does not override challenges crated by `.create_acme_challenge()` method.
	///
	///This directory can also be modified by `acmedir=` option in configuration string (see the method
	///`.config_flags`).
	///
	///The default is no ACME challenge database.
	pub fn set_acme_dir<P:AsRef<Path>>(&mut self, path: P)
	{
		self.acme_path = Some(path.as_ref().to_path_buf());
	}
	///Set ALPN callback function.
	///
	///This method sets the ALPN callback function to `func`, overriding the entries added by `.add_alpn()`.
	///
	///When client hello containing an ALPN is received, the ALPN callback is called, passing the received
	///SNI and ALPN values to the callback. The callback can then decide the ALPN to use.
	///
	///The default is no ALPN callback function.
	pub fn set_alpn_function(&mut self, func: Box<AlpnCallback+Send>)
	{
		self.alpn_function = Some(func);
	}
	///Add supported ALPN.
	///
	///Add a new least-preferred ALPN value `alpn` as supported.
	///
	///When ALPN value is marked as supported, it may be chosen if client supports it. More preferred values
	///are chosen in preference to less preferred values if client supports both.
	///
	///Note, the values added are overridden if `.set_alpn_function()` has been used to set ALPN callback.
	///
	///Also note: If any ALPN values are set, and client sends a totally disjoint ALPN value set, the handshake
	///will fail.
	///
	///The default is no ALPN values recognized (no response to ALPN extension request). This is appropriate
	///behavior if server does not support ALPN.
	pub fn add_alpn(&mut self, alpn: &str)
	{
		self.supported_alpn.push(alpn.to_owned());
	}
	///Set flags.
	///
	///This method sets flags specified by bitmask `mask` on configuration page `page`.
	///
	///Flags can affect various options in server behavior. See module [`features`] for the flag constants.
	///
	///If nonexistent page is specified, this method does nothing.
	///
	///[`features`]: features/index.html
	pub fn set_flags(&mut self, page: usize, mask: u64)
	{
		if page < CONFIG_PAGES { self.flags[page] = set_combine_flags_set(page, mask, self.flags[page]); }
	}
	///Clear flags.
	///
	///This method clears flags specified by bitmask `mask` on configuration page `page`.
	///
	///Flags can affect various options in server behavior. See module [`features`] for the flag constants.
	///
	///If nonexistent page is specified, this method does nothing.
	///
	///[`features`]: features/index.html
	pub fn clear_flags(&mut self, page: usize, mask: u64)
	{
		if page < CONFIG_PAGES { self.flags[page] = set_combine_flags_clear(page, mask, self.flags[page]); }
	}
	///Add a new trusted certificate transparency log.
	///
	///This method adds certificate transprancy log `log` to the list of trusted certificate transparency logs.
	///This list is used for verifying certificate transparency proofs in client certificates (see method
	///`.set_ask_client_cert` for enabling client certificate support).
	///
	///CT logs can also be added via the `ctlog=` option in configuration string (see the method
	///`.config_flags`).
	///
	///If client certificates are not enabled, the CT log list is not used for anything. The list of trusted 
	///logs for CT stapling in server cert is separate, and set via the certificate selector.
	///
	///The default is not to have any trusted CT logs.
	pub fn add_ct_log(&mut self, log: &TrustedLog)
	{
		self.trusted_logs.push(log.clone());
	}
	///Add a new certificate key blacklist entry.
	///
	///This method adds a blacklist entry for certificate key hash `spkihash` (a 32-byte SHA-256 hash of
	///certificate SubjectPublicKeyInfo).
	///
	///If the key hash of any sent client certificate chain entry matches any hash on the blacklist, the
	///certificate validation is rejected (this will not cause connection failure).
	///
	///Additionally, if trust anchor matches blacklist entry, that trust anchor is not used.
	///
	///If client certificates are not enabled, the blacklist is not used for anything.
	///
	///Default is not to have any blacklist entries.
	pub fn blacklist(&mut self, spkihash: &[u8])
	{
		let mut x = [0; 32];
		if spkihash.len() != x.len() { return; }
		x.copy_from_slice(spkihash);
		self.killist.insert(x, ());
	}
	///Set/Clear flags according to config string.
	///
	///This method sets or clears configuration options according to configuration string `config` (presumably
	///read from configuration file). If any errors are encountered during processing, the error callback
	///`error_cb` is called to report those errors (possibly multiple times).
	///
	///The configuration string consists of comma-separated elements, with each element being one of:
	///
	/// * flagname -> Enable flag named `flagname`.
	/// * +flagname -> Enable flag named `flagname`.
	/// * -flagname -> Disable flag named `flagname`.
	/// * !flagname -> Disable flag named `flagname`.
	/// * setting=value -> Set setting `setting` to value `value`.
	///
	///Use backslashes to escape backslash or comma in the value.
	///
	///# The following flags are supported:
	///
	/// * `tls12` -> Enable TLS 1.2 support.
	/// * `tls13` -> Enable TLS 1.3 support.
	/// * `require-ems` -> Require clients to support Extended Master Secret or TLS 1.3.
	/// * `require-ct` -> Require client certificates to have Certificate Transparency staple.
	/// * `require-ocsp` -> Require client certificates to have OCSP staple (if not shortlived).
	/// * `assume-hw-aes` -> Assume hardware AES-GCM support is present.
	/// * `allow-bad-crypto` -> Accept handshakes advertising bad cryptographic algorithms.
	///
	///# The following settings are supported:
	///
	/// * `acmedir` -> Set the ACME challenge database directory to the specified value.
	/// * `blacklist` -> Blacklist specified SPKI hash (specified as 64 hexadecimal digit representation of
	///SHA-256 hash of the SubjectPublicKeyInfo).
	/// * `trustanchor` -> Read the specified file in DER (possibly concatenated from multiple certificates) or
	///PEM format as series of trust anchors to add.
	/// * `ctlog` -> Read the specified file as series of CT logs (one per line) to add.
	/// * `ocsp-maxvalid` -> Set the OCSP maximum validity limit in seconds.
	///
	///# Format of ctlog files:
	///
	///ctlog files consists of one or more lines, with each line containing a trusted CT log. Each line has one
	///of two forms:
	///
	/// * `v1:<key>:<expiry>:<name>`
	/// * `v2:<id>/<hash>:<key>:<expiry>:<name>`
	///
	///Where:
	///
	/// * `<id>` is base64 encoding of DER encoding the log OID, without the DER tag or length.
	/// * `<hash>` is the log hash function. Currently only `sha256` is supported.
	/// * `<key>`is base64 encoding of log key formatted as X.509 SubjectPublicKeyInfo.
	/// * `<expiry>` is the time log expired in seconds since Unix epoch, or blank if log has not expired yet.
	///Certificate-stapled SCTs are accepted up to expiry date from expired logs, other kinds of SCTs are not
	///accepted.
	/// * `<name>` is the name of the log.
	pub fn config_flags<F>(&mut self, config: &str, mut error_cb: F) where F: FnMut(ConfigFlagsError)
	{
		split_config_str(self, config, |objself, name, value, entry| {
			if if name == "acmedir" {
				config_strval!(Self::set_acme_dir_helper, value, &mut error_cb, entry, &mut objself.
					acme_path)
			} else if name == "blacklist" {
				config_strval!(handle_blacklist, value, &mut error_cb, entry, &mut objself.killist)
			} else if name == "trustanchor" {
				config_strval!(handle_trustanchor, value, &mut error_cb, entry, &mut objself.
					trust_anchors)
			} else if name == "ctlog" {
				config_strval!(handle_ctlog, value, &mut error_cb, entry, &mut objself.
					trusted_logs)
			} else if name == "ocsp-maxvalid" {
				config_strval!(handle_ocsp_maxvalid, value, &mut error_cb, entry, &mut objself.
					ocsp_maxvalid)
			} else {
				false
			} {
				error_cb(ConfigFlagsError{entry:entry,
					kind:ConfigFlagsErrorKind::ArgumentRequired(name)});
				return;
			}

			if let Some((page, mask)) = lookup_flag(name) {
				match value {
					ConfigFlagsValue::Disabled => objself.clear_flags(page, mask),
					ConfigFlagsValue::Enabled if 
						name == DEBUG_NO_SHARES_NAME =>
						//debug-no-shares is not supported serverside.
						error_cb(ConfigFlagsError{entry:entry,
							kind:ConfigFlagsErrorKind::NoEffect(name)}),
					ConfigFlagsValue::Enabled => objself.set_flags(page, mask),
					ConfigFlagsValue::Explicit(_) => error_cb(ConfigFlagsError{entry:entry,
						kind:ConfigFlagsErrorKind::NoArgument(name)})
				}
			} else {
				error_cb(ConfigFlagsError{entry:entry,
					kind:ConfigFlagsErrorKind::UnrecognizedSetting(name)});
			}
		});
	}
	///Create an ACME challenge response endpoint.
	///
	///This method creates an ACME challenge-response endpoint for challenge `challenge`, which is specified
	///as challenge hostname (ending in `.acme.invalid`). The challenge response is `response`, which is
	///the response hostname (ending in `.acme.invalid`) for TLS-SNI-02. For TLS-SNI-01, `response` must be
	///`None`.
	///
	///If handshake is sent with SNI matching a known challenge, the TLS library will immediately disconnect
	///the session when the session reaches showtime state. No data transport on challenge connection is
	///possible.
	///
	///On failure, appropriate error is returned.
	///
	///The default is to have no challenges (but the challenges in challenge directory set by `.set_acme_dir()`
	///are also considered).
	pub fn create_acme_challenge(&mut self, challenge: &str, response: Option<&str>) ->
		Result<(), TlsFailure>
	{
		match self.acme_challenges.write() {
			Ok(mut x) => x.entries.insert(challenge.to_owned(), response.map(|x|x.to_owned())),
			Err(_) => sanity_failed!("Can't lock ACME challenge table")
		};
		Ok(())
	}
	///Delete an ACME challenge response endpoint.
	///
	///This method deletes an ACME challenge-response endpoint `challenge`.
	///
	///Note that only endpoints created by `.create_acme_challenge()` can be deleted. The endpoints coming from
	///`.set_acme_dir()` can not be deleted by this method.
	///
	///On failure, appropriate error is returned.
	pub fn delete_acme_challenge(&mut self, challenge: &str) -> Result<(), TlsFailure>
	{
		match self.acme_challenges.write() {
			Ok(mut x) => x.entries.remove(challenge),
			Err(_) => sanity_failed!("Can't lock ACME challenge table")
		};
		Ok(())
	}
	///Set debugging mode.
	///
	///This method enables debugging mode, with debug event mask of `mask` (constructed from bitwise OR of 
	///various `DEBUG_*` constatnts from module [`logging`]) and debug sink `log`.
	///
	///Note that enabling `DEBUG_CRYPTO_CALCS` class requires a build with `enable-key-debug` cargo feature,
	///otherwise that debugging event is just ignored.
	///
	///[`logging`]: logging/index.html
	pub fn set_debug(&mut self, mask: u64, log: Box<Logging+Send+Sync>)
	{
		self.log = Some((mask, Arc::new(log)));
	}
	///Set default server name.
	///
	///This method sets the default server name to `name`. 
	///
	///If a request is received without any SNI, then this name is used instead in certificate lookup.
	///
	///Note, this setting does not affect the SNI reported by `ServerSession::get_sni()`.
	///
	///The default is to have no default (any certificate can be sent back).
	pub fn set_default_server_name(&mut self, name: &str)
	{
		self.default_sni_name = Some(name.to_owned());
	}
	///Create a server session with these settings.
	///
	///This method creates a new server-side TLS connection, using setttings set by this object.
	///
	///On success, returns the connection created.
	///
	///On failure, appropriate error is returned.
	pub fn make_session(&self) -> Result<ServerSession, TlsFailure>
	{
		let now = millisec_clock();
		let controller = HandshakeController::new(&self, self.log.clone());
		let mut sess = _ServerSession {
			base: SessionBase::new(self.log.clone()),
			hs:ServerHandshake(controller),
			handshake_deadline: if self.handshake_timeout != 0 {
				now + self.handshake_timeout as i64
			} else {
				i64::max_value()
			}
		};
		//Immediately request RX handshake, for receiving the ClientHello.
		sess.base.request_rx_handshake();
		//Ok.
		Ok(ServerSession(Arc::new(Mutex::new(sess))))
	}

	fn set_acme_dir_helper<F>(value: &str, mut _error_cb: &mut F, _entry: ConfigFlagsEntry,
		acme_path: &mut Option<PathBuf>) where F: FnMut(ConfigFlagsError)
	{
		let p: &Path = value.as_ref();
		*acme_path = Some(p.to_path_buf());
	}
}

struct ServerHandshake(HandshakeController);

impl ControlHandler for ServerHandshake
{
	fn handle_control(&mut self, rtype: u8, msg: &[u8], controller: &mut SessionController)
	{
		self.0.handle_control(rtype, msg, controller)
	}
	fn handle_tx_update(&mut self, controller: &mut SessionController)
	{
		self.0.handle_tx_update(controller)
	}
}

impl ServerHandshake
{
	fn borrow_inner(&self) -> &HandshakeController
	{
		&self.0
	}
	fn borrow_inner_mut(&mut self) -> &mut HandshakeController
	{
		&mut self.0
	}
}

//The internal state, send but pesumably not sync. Not cloneable.
struct _ServerSession
{
	base: SessionBase,
	hs: ServerHandshake,
	handshake_deadline: i64,	//Milliseconds since UNIX epoch.
}


///A server TLS session.
///
///This is the server end of TLS session.
///
///Use `.make_session()` of [`ServerConfiguration`] to create it.
///
///For operations supported, see trait [`Session`].
///
///If the session is cloned, both sessions refer to the same underlying TLS connection. This can be used to refer
///to the same session from multiple threads at once.
///
///[`ServerConfiguration`]: struct.ServerConfiguration.html
///[`Session`]: trait.Session.html
#[derive(Clone)]
pub struct ServerSession(Arc<Mutex<_ServerSession>>);

impl Session for ServerSession
{
	fn set_high_water_mark(&mut self, amount: usize)
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.set_high_water_mark(amount),
			Err(_) => ()
		}
	}
	fn set_send_threshold(&mut self, threshold: usize)
	{
		match self.0.lock() {
			Ok(mut x) => x.base.set_send_threshold(threshold),
			Err(_) => ()
		}
	}
	fn bytes_available(&self) -> usize
	{
		match self.0.lock() {
			Ok(ref x) => x.base.bytes_available(),
			Err(_) => 0
		}
	}
	fn bytes_queued(&self) -> usize
	{
		match self.0.lock() {
			Ok(ref x) => x.base.bytes_queued(),
			Err(_) => 0
		}
	}
	fn is_eof(&self) -> bool
	{
		match self.0.lock() {
			Ok(ref x) => x.base.is_eof(),
			Err(_) => true
		}
	}
	fn wants_read(&self) -> bool
	{
		match self.0.lock() {
			Ok(ref x) => x.base.wants_read(),
			Err(_) => false
		}
	}
	fn set_tx_irq(&mut self, handler: Box<TxIrqHandler+Send>)
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.set_tx_irq(handler),
			Err(_) => ()
		}
	}
	fn set_appdata_rx_fn(&mut self, handler: Option<Box<RxAppdataHandler+Send>>)
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.set_appdata_rx_fn(handler),
			Err(_) => ()
		}
	}
	fn read_tls<R:IoRead>(&mut self, stream: &mut R) -> IoResult<()>
	{
		//We can't read with lock held.
		const READ_BLOCKSIZE: usize = 16500;
		let mut buf = [0; READ_BLOCKSIZE];
		let rlen = stream.read(&mut buf)?;
		if rlen > buf.len() {
			return Err(IoError::new(IoErrorKind::Other, TlsFailure::from(assert_failure!("Stream \
				read of {} bytes into buffer of {}", rlen, buf.len()))));
		}
		let mut buf = &buf[..rlen];
		let selfc = self.clone();
		let res = match self.0.lock() {
			Ok(ref mut x) => {
				let x = x.deref_mut();
				let res = x.base.read_tls(&mut buf, &mut x.hs)?;
				//Arrange do_tx_request to be called when certlookup_future settles, if needed.
				x.hs.borrow_inner_mut().callback_with_certlookup_future(&mut x.base, move |sb, y|{
					if y.settled_cb(Box::new(DoTxRequestOnSettle(selfc.clone()))).is_none() {
						//Already settled.
						sb.do_tx_request();
					}
				});
				res
			},
			Err(_) => fail!(IoError::new(IoErrorKind::Other, TlsFailure::from(assert_failure!(
				"Can't lock TLS session"))))
		};
		self.do_tls_tx_rx_cycle().map_err(|x|IoError::new(IoErrorKind::Other, x))?;
		Ok(res)
	}
	fn submit_tls_record(&mut self, record: &mut [u8]) -> Result<(), TlsFailure>
	{
		let selfc = self.clone();
		let res = match self.0.lock() {
			Ok(ref mut x) => {
				let x = x.deref_mut();
				let res = x.base.submit_tls_record(record, &mut x.hs)?;
				//Arrange do_tx_request to be called when certlookup_future settles, if needed.
				x.hs.borrow_inner_mut().callback_with_certlookup_future(&mut x.base, move |sb, y|{
					if !y.settled_cb(Box::new(DoTxRequestOnSettle(selfc.clone()))).is_none() {
						//Already settled.
						sb.do_tx_request();
					}
				});
				res
			},
			Err(_) => sanity_failed!("Can't lock TLS session")
		};
		self.do_tls_tx_rx_cycle()?;
		Ok(res)
	}
	fn write_tls<W:IoWrite>(&mut self, stream: &mut W) -> IoResult<()>
	{
		//This can happen with waiting for signatures.
		self.do_tls_tx_rx_cycle().map_err(|x|IoError::new(IoErrorKind::Other, x))?;
		match self.0.lock() {
			Ok(ref mut x) => {
				let x = x.deref_mut();
				x.base.write_tls(stream, &mut x.hs, false)
			},
			Err(_) => fail!(IoError::new(IoErrorKind::Other, TlsFailure::from(assert_failure!(
				"Can't lock TLS session"))))
		}
	}
	fn zerolatency_write(&mut self, output: &mut [u8], input: &[u8]) -> Result<(usize, usize), TlsFailure>
	{
		//This can happen with waiting for signatures.
		self.do_tls_tx_rx_cycle()?;
		match self.0.lock() {
			Ok(ref mut x) => {
				let x = x.deref_mut();
				x.base.zerolatency_write(&mut x.hs, output, input)
			},
			Err(_) => sanity_failed!("Can't lock TLS session")
		}
	}
	fn estimate_zerolatency_size(&self, outsize: usize) -> usize
	{
		match self.0.lock() {
			Ok(ref mut x) => {
				let x = x.deref();
				x.base.estimate_zerolatency_size(outsize)
			},
			Err(_) => 0
		}
	}
	fn can_tx_data(&self) -> bool
	{
		match self.0.lock() {
			Ok(ref x) => x.base.can_tx_data(),
			Err(_) => false
		}
	}
	fn handshake_completed(&self) -> bool
	{
		match self.0.lock() {
			Ok(x) => x.hs.borrow_inner().in_showtime(),
			Err(_) => false,	//Should not happen.
		}
	}
	fn send_eof(&mut self)
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.send_eof(),
			Err(_) => ()
		}
	}
	fn aborted_by(&self) -> Option<AbortReason>
	{
		match self.0.lock() {
			Ok(ref x) => {
				if !x.hs.0.ignore_handshake_timeout() && millisec_clock() > x.handshake_deadline {
					Some(AbortReason::HandshakeError(TlsFailure::HandshakeTimedOut))
				} else {
					x.base.aborted_by()
				}
			},
			Err(_) => Some(AbortReason::HandshakeError(TlsFailure::from(assert_failure!(
				"Can't lock TLS session"))))
		}
	}
	fn extractor(&self, label: &str, context: Option<&[u8]>, buffer: &mut [u8]) -> Result<(), TlsFailure>
	{
		match self.0.lock() {
			Ok(ref x) => x.base.extractor(label, context, buffer),
			Err(_) => sanity_failed!("Can't lock TLS session")
		}
	}
	fn get_alpn(&self) -> Option<Option<Arc<String>>>
	{
		match self.0.lock() {
			Ok(x) => x.hs.borrow_inner().get_alpn(),
			Err(_) => None		//Shouldn't be here.
		}
	}
	fn get_server_names(&self) -> Option<Arc<Vec<String>>>
	{
		match self.0.lock() {
			Ok(x) => x.hs.borrow_inner().get_server_names(),
			Err(_) => None		//Shouldn't be here.
		}
	}
	fn get_sni(&self) -> Option<Option<Arc<String>>>
	{
		match self.0.lock() {
			Ok(x) => x.hs.borrow_inner().get_sni(),
			Err(_) => None		//Shouldn't be here.
		}
	}
	fn connection_info(&self) -> TlsConnectionInfo
	{
		match self.0.lock() {
			Ok(x) => x.hs.borrow_inner().connection_info(),
			Err(_) => TlsConnectionInfo {
				version: 0,
				ems_available: false,
				ciphersuite: 0,
				kex: None,
				signature: None,
				version_str: "",
				protection_str: "",
				hash_str: "",
				exchange_group_str: "",
				signature_str: "",
				validated_ct: false,
				validated_ocsp: false,
				validated_ocsp_shortlived: false,
			}
		}
	}
	fn get_record_size(&self, buffer: &[u8]) -> Option<usize>
	{
		match self.0.lock() {
			Ok(x) => x.base.get_record_size(buffer),
			Err(_) => Some(5)	//Shouldn't be here.
		}
	}
	fn begin_transacted_read<'a>(&self, buffer: &'a mut [u8]) -> Result<&'a [u8], TlsFailure>
	{
		match self.0.lock() {
			Ok(x) => x.base.begin_transacted_read(buffer),
			Err(_) => sanity_failed!("Can't lock TLS session")
		}
	}
	fn end_transacted_read(&mut self, len: usize) -> Result<(), TlsFailure>
	{
		match self.0.lock() {
			Ok(mut x) => x.base.end_transacted_read(len),
			Err(_) => sanity_failed!("Can't lock TLS session")
		}
	}
}

impl IoRead for ServerSession
{
	fn read(&mut self, buf: &mut [u8]) -> IoResult<usize>
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.read(buf),
			Err(_) => fail!(IoError::new(IoErrorKind::Other, TlsFailure::from(assert_failure!(
				"Can't lock TLS session"))))
		}
	}
}

impl IoWrite for ServerSession
{
	fn write(&mut self, buf: &[u8]) -> IoResult<usize>
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.write(buf),
			Err(_) => fail!(IoError::new(IoErrorKind::Other, TlsFailure::from(assert_failure!(
				"Can't lock TLS session"))))
		}
	}
	fn flush(&mut self) -> IoResult<()>
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.flush(),
			Err(_) => fail!(IoError::new(IoErrorKind::Other, TlsFailure::from(assert_failure!(
				"Can't lock TLS session"))))
		}
	}
}

impl ServerSession
{
	fn do_tls_tx_rx_cycle(&mut self) -> Result<(), TlsFailure>
	{
		let selfc = self.clone();
		let mut inner = match self.0.lock() {
			Ok(x) => x,
			Err(_) => sanity_failed!("Can't lock TLS session")
		};
		//Do TX cycles if needed.
		while inner.hs.borrow_inner().wants_tx() {
			let inner = inner.deref_mut();
			if inner.hs.borrow_inner_mut().queue_hs_tx(&mut inner.base) {
				inner.base.finish_tx_handshake();
			}
			//Arrange do_tx_request to be called when signature_future settles, if needed.
			inner.hs.borrow_inner_mut().callback_with_signature_future(&mut inner.base, |sb, y|{
				if !y.settled_cb(Box::new(DoTxRequestOnSettle(selfc.clone()))).is_none() {
					//Already settled.
					sb.do_tx_request();
				}
			});
		}
		//Re-arm the RX if needed. Note that previous TX cycle can have altered the condition.
		if inner.hs.borrow_inner().wants_rx() {
			inner.base.request_rx_handshake();
		}
		Ok(())
	}
}

struct DoTxRequestOnSettle(ServerSession);

type ChooserFT = Result<Box<CertificateSigner+Send>, ()>;
type SignerFT = Result<(u16, Vec<u8>),()>;

impl FutureCallback<ChooserFT> for DoTxRequestOnSettle
{
	fn on_settled(&mut self, val: ChooserFT) -> ChooserFT
	{
		match (self.0).0.lock() {
			Ok(ref mut y) => y.base.do_tx_request(),
			Err(_) => ()
		};
		val
	}
}

impl FutureCallback<SignerFT> for DoTxRequestOnSettle
{
	fn on_settled(&mut self, val: SignerFT) -> SignerFT
	{
		match (self.0).0.lock() {
			Ok(ref mut y) => y.base.do_tx_request(),
			Err(_) => ()
		};
		val
	}
}
