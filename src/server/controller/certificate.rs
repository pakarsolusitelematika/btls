use super::{HandshakeState2, Tls13C2ndFSecrets, Tls13HsSecrets, tx_sct_ext_common};
use super::super::ClientCertificateInfo;
use super::finished::{Tls13ServerFinishedFields, Tls13ClientFinishedFields};
use super::serverhellodone::Tls12ServerHelloDoneFields;

use ::certificate::{CertificateIssuer, CFLAG_MUST_CT, CFLAG_MUST_EMS, CFLAG_MUST_RENEGO, CFLAG_MUST_STAPLE,
	CFLAG_MUST_TLS13, extract_spki, name_in_set2, OcspDumpedScts, OcspSanityCheckResults, OcspValidationResult,
	ParsedCertificate, sanity_check_certificate_chain, validate_chain_client, validate_scts};
use ::common::{CERTTYPE_RPK, CryptoTracker, Debugger, emit_handshake, EXT_CT, EXT_SERVER_CERTIFICATE_TYPE,
	EXT_STATUS_REQUEST, format_tbs_tls12, format_tbs_tls13, HandshakeHash, HandshakeMessage, HashMessages,
	HSTYPE_CERTIFICATE, HSTYPE_CERTIFICATE_REQUEST, HSTYPE_CERTIFICATE_STATUS, HSTYPE_CERTIFICATE_VERIFY,
	HSTYPE_SERVER_KEY_EXCHANGE, NamesContainer, OCSP_SINGLE, TLS_LEN_CERT_CONTEXT, TLS_LEN_CERT_ENTRY,
	TLS_LEN_CERT_LIST, TLS_LEN_EXTENSIONS, TLS_LEN_OCSP_RESPONSE, TLS_LEN_SIGNATURE, TLS_LEN_TLS12_PUBKEY,
	TlsFailure, write_ext_fn, write_signature_algorithms_ext, write_sub_fn};
use ::record::{SessionBase, SessionController};
use ::features::{FLAGS0_ENABLE_TLS13, FLAGS0_REQUIRE_CT, FLAGS0_REQUIRE_OCSP};
use ::logging::bytes_as_hex_block;
use ::messages::{Tls13Certificate, TlsCertificateVerify};
use ::server::{SaneCertVal, SaneOcspVal, ServerConfiguration};
use ::server_certificates::{CertificateSigner};
use ::stdlib::{Arc, Box, Deref, from_utf8, String, ToOwned, Vec};

use btls_aux_aead::ProtectorType;
use btls_aux_dhf::DiffieHellmanKey;
use btls_aux_hash::HashFunction;
use btls_aux_futures::FutureReceiver;
use btls_aux_serialization::Sink;
use btls_aux_signature_algo::{SignatureType, VERIFY_FLAG_NO_RSA_PKCS1};
use btls_aux_time::{TimeInterface, TimeNow};

pub struct Tls12CertificateFields
{
	pub certificate: Box<CertificateSigner+Send>,
	pub key_share: Box<DiffieHellmanKey+Send>,
	pub send_spki_only: bool,
	pub sni_name: Option<String>,
	pub no_check_certificate: bool,
	pub ocsp_v1_offered: bool,
	pub ct_offered: bool,
	pub maybe_send_ocsp: bool,
	pub kill_connection_after_handshake: bool,
	pub client_random: [u8; 32],
	pub server_random: [u8; 32],
	pub hs_hash: HandshakeHash,
	pub ems_enabled: bool,
	pub protection: ProtectorType,
	pub prf: HashFunction,
}

pub struct Tls13CertificateFields
{
	pub hs_secrets: Tls13HsSecrets,
	pub certificate: Box<CertificateSigner+Send>,
	pub send_spki_only: bool,
	pub sni_name: Option<String>,
	pub no_check_certificate: bool,
	pub ocsp_v1_offered: bool,
	pub ct_offered: bool,
	pub kill_connection_after_handshake: bool,
	pub hs_hash: HandshakeHash,
	pub requested_cc: bool,
	pub peer_sigalgo_mask: u64,
}

pub struct Tls13CertificateVerifyFields
{
	hs_secrets: Tls13HsSecrets,
	pub signature_future: FutureReceiver<Result<(u16, Vec<u8>),()>>,
	kill_connection_after_handshake: bool,
	hs_hash: HandshakeHash,
	requested_cc: bool,
	peer_sigalgo_mask: u64,
}

fn tx_ocsp_ext_tls13_common<S:Sink>(x: &mut S, certificate: &Box<CertificateSigner+Send>,
	debugger: Debugger, crypto_algs: &mut CryptoTracker) -> Result<(), TlsFailure>
{
	let ocsp = certificate.get_ocsp();
	let ocsp = if let Some(ref x) = ocsp.0 { x } else { return Ok(()); };

	sanity_check!(ocsp.len() > 0, "Certificate has empty OCSP staple data");
	debug!(HANDSHAKE_EVENTS debugger, "Sent OCSP staple");
	crypto_algs.ocsp_validated();
	write_ext_fn(x, EXT_STATUS_REQUEST, "status_reqeust", |x|{
		try_buffer!(x.write_u8(OCSP_SINGLE));
		write_sub_fn(x, "OCSP response", TLS_LEN_OCSP_RESPONSE, |x|{
			try_buffer!(x.write_slice(&ocsp));
			Ok(())
		})
	})
}

struct TxCertificateCommonParameters<'a>
{
	tls13: bool,
	certificate: &'a Box<CertificateSigner+Send>,
	send_spki_only: bool,
	sni_name: Option<String>,
	no_check_certificate: bool,
	ocsp_v1_offered: bool,
	ct_offered: bool,
	maybe_send_ocsp: bool,
}

fn tx_certificate_common<'a>(base: &mut SessionBase, params: TxCertificateCommonParameters<'a>,
	names: &mut NamesContainer, debug: Debugger, config: &ServerConfiguration, hs_hash: &mut HandshakeHash,
	state: &str, crypto_algs: &mut CryptoTracker) -> Result<(), TlsFailure>
{
	let mut have_sct = false;
	let (chain, ocsp) = (params.certificate.get_certificate(), params.certificate.get_ocsp());
	let have_ocsp = ocsp.0.is_some();
	//Check if there are SCTs in OCSP response.
	if let &Some(ref x) = &ocsp.0 {
		have_sct |= OcspDumpedScts::from(x).map(|x|x.list.len() > 0).unwrap_or(false);
	}

	let certarr = &chain.0;
	let eecert = match certarr.get(0) {
		Some(x) => &(x.0)[..],
		None => fail!(TlsFailure::EmptyCertChain(match params.sni_name {
			Some(ref x) => x.deref(),
			None => "<default>"
		}.to_owned()))
	};
	let mut intermediates = Vec::<&[u8]>::new();
	for i in certarr.iter().skip(1) {
		intermediates.push(&i.0);
	}
	//Check if there are dedicated SCTs.
	have_sct |= params.certificate.get_scts().0.is_some();

	let mut features = CFLAG_MUST_EMS | CFLAG_MUST_RENEGO;	//Always supported.
	if (config.flags[0] | FLAGS0_ENABLE_TLS13) != 0 { features |= CFLAG_MUST_TLS13; }
	if have_ocsp { features |= CFLAG_MUST_STAPLE; }

	//Don't sanity-check the certificate if sending RPK or ACME response.
	if !params.send_spki_only && !params.no_check_certificate {
		let reference_name = match params.sni_name {
			Some(ref x) => Some(x.deref()),
			None => match config.default_sni_name {
				Some(ref x) => Some(x.deref()),
				None => None
			}
		};
		if have_sct { features |= CFLAG_MUST_CT; }
		let mut namelist: Option<Arc<Vec<String>>> = None;
		let mut c_issuer: Option<Arc<Vec<u8>>> = None;
		let mut c_serial: Option<Arc<Vec<u8>>> = None;
		let mut dummy = false;		//Require OCSP flag, we aren't interested in it.
		let mut dummy2 = false;		//Require SCT flag, we aren't interested in it.

		//Check cache.
		let caddr = chain.0.deref() as &_ as *const _ as usize;
		let time_now = TimeNow.get_time();
		if let Ok(x) = config.sane_certificates.read() {
			if let Some(ref cache) = x.get(&caddr) {
				//Try upgrading reference to check its validity. If it can be up-
				//graded, check that it is not expired and that all needed flags
				//are there (flags are internally set, so an attacker can't force
				//repeated validations).
				if cache.ccref.upgrade().is_some() && time_now < cache.expires &&
					cache.flags & !features == 0 {
					namelist = Some(cache.names.clone());
					c_issuer = Some(cache.issuer.clone());
					c_serial = Some(cache.serial.clone());
				}
			}
		};

		//We need to check that the certificate is valid for host.
		let invalid = match (&reference_name, &namelist) {
			(&Some(ref refname), &Some(ref names)) => !name_in_set2(names.iter().map(|x|
				x.as_bytes()), refname.as_bytes()),
			(_, _) => true
		};
		if invalid {
			//The cached validation is not valid.
			namelist = None;
		}

		let cert_known_valid = namelist.is_some();
		if let Some(ref x) = namelist {
			//Cached validation. The certificate is known good, so data can be released
			//immediately.
			names.set_server_names(x.clone());
			names.release_server_names();
		} else {
			//Only run sanity checks if certificate is not known to pass those.
			let (good_until, need_features, namelist, t_issuer, t_serial) =
				match sanity_check_certificate_chain(eecert, &intermediates, reference_name,
					&time_now, true, features,
					|eecert_p,_,_,_,_,_,v_until, need_features|{
					//Get the server names and release those immediately.
					let namelist: Arc<Vec<String>> = Arc::new(eecert_p.dnsnames.iter().
						filter_map(|x|from_utf8(x).ok()).map(|x|x.to_owned()).
						collect());
					let t_issuer = Arc::new(eecert_p.issuer.as_raw_issuer().to_owned());
					let t_serial = Arc::new(eecert_p.serial_number.to_owned());
					Ok((v_until, need_features, namelist, t_issuer, t_serial))
				}, (), &config.killist, &mut dummy, &mut dummy2, false) {
				Ok(x) => x,
				Err(ref x) => fail!(TlsFailure::InterlockCertificate(x.clone()))
			};
			c_issuer = Some(t_issuer.clone());
			c_serial = Some(t_serial.clone());
			names.set_server_names(namelist.clone());
			names.release_server_names();
			//Save good-until, associating with chain. Note that the key may already exist
			//In this case we know it is stale and we want to update it, which insert does.
			if let Ok(mut x) = config.sane_certificates.write() {
				x.insert(caddr, SaneCertVal{
					ccref: Arc::downgrade(&chain.0),
					expires: good_until,
					flags: need_features,
					names: namelist.clone(),
					issuer: t_issuer,
					serial: t_serial,
				});
			};
		}
		debug!(HANDSHAKE_EVENTS debug, "Certificate sanity check passed");
		if let Some(ref ocspcontent) = ocsp.0 {
			let (eecert_issuer, eecert_serial) = match (c_issuer, c_serial) {
				(Some(x), Some(y)) => (x, y),
				(_, _) => sanity_failed!("No certificate issuer&serial available")
			};
			let ocspaddr = ocspcontent.deref() as &_ as *const _ as usize;
			let known_valid_ocsp = if let Ok(x) = config.sane_ocsp.read() {
				if let Some(ref cache) = x.get(&ocspaddr) {
					//Check the entry is not expired and belongs to the certificate.
					cache.certaddr == caddr && cache.ocspref.upgrade().is_some() &&
						time_now < cache.expires && cert_known_valid
				} else {
					false
				}
			} else {
				false
			};

			if !known_valid_ocsp {
				let res = OcspSanityCheckResults::from(ocspcontent, CertificateIssuer(
					eecert_issuer.deref()), eecert_serial.deref(), &TimeNow).map_err(|x|
					TlsFailure::InterlockOcsp(x))?;
				if let Ok(mut x) = config.sane_ocsp.write() {
					x.insert(ocspaddr, SaneOcspVal{
						certaddr: caddr,
						ocspref: Arc::downgrade(ocspcontent),
						expires: res.not_after
					});
				};
			}
			debug!(HANDSHAKE_EVENTS debug, "OCSP sanity check passed");
		}
	}

	let mut content = Vec::new();
	if params.tls13 {
		//The context field must be empty.
		write_sub_fn(&mut content, "certificate context", TLS_LEN_CERT_CONTEXT, |_|Ok(()))?;
	}
	write_sub_fn(&mut content, "certificate chain", TLS_LEN_CERT_LIST, |x|{
		if params.send_spki_only {
			//Just send the SPKI of the first certificate.
			if params.tls13 {
			//Just send the SPKI of the first certificate.
				write_sub_fn(x, "certificate", TLS_LEN_CERT_ENTRY, |x|{
					try_buffer!(x.write_slice(extract_spki(eecert).map_err(|x|
						TlsFailure::CantExtractSpki(x))?));
					Ok(())
				})?;
				write_sub_fn(x, "Certificate extensions", TLS_LEN_EXTENSIONS, |x|{
					//Server key type. If using RPK, send this.
					write_ext_fn(x, EXT_SERVER_CERTIFICATE_TYPE,
						"server_certificate_type", |y|{
							try_buffer!(y.write_u8(CERTTYPE_RPK));
							Ok(())
						})?;
					Ok(())
				})?;
			} else {
				try_buffer!(x.write_slice(extract_spki(eecert).map_err(|x|
					TlsFailure::CantExtractSpki(x))?));
			}
		} else {
			let mut first = true;
			for i in certarr.iter() {
				write_sub_fn(x, "certificate", TLS_LEN_CERT_LIST, |x|{
					try_buffer!(x.write_slice(&i.0));
					Ok(())
				})?;
				if params.tls13 {
					write_sub_fn(x, "Certificate extensions", TLS_LEN_EXTENSIONS, |x|{
						//OCSP.
						if params.ocsp_v1_offered && !params.send_spki_only && first {
							tx_ocsp_ext_tls13_common(x, params.certificate, debug.
							clone(), crypto_algs)?;
						}
						//Extension SCT.
						if params.ct_offered && !params.send_spki_only && first {
							tx_sct_ext_common(x, params.certificate, debug.clone(),
							crypto_algs)?;
						}
						Ok(())
					})?;
				}
				first = false;
			}
		}
		Ok(())
	})?;
	emit_handshake(debug.clone(), HSTYPE_CERTIFICATE, &content, base, hs_hash, state)?;
	//Send OCSP message if we have one. Note, maybe_send_ocsp should be never be set for TLS 1.3.
	if let (true, Some(ref ocspcontent)) = (params.maybe_send_ocsp, ocsp.0) {
		sanity_check!(ocspcontent.len() > 0, "Certificate has empty OCSP staple data");
		let mut content = Vec::new();
		try_buffer!(content.write_u8(OCSP_SINGLE));
		write_sub_fn(&mut content, "OCSP response", TLS_LEN_OCSP_RESPONSE, |x|{
			try_buffer!(x.write_slice(ocspcontent));
			Ok(())
		})?;
		emit_handshake(debug.clone(), HSTYPE_CERTIFICATE_STATUS, &content, base, hs_hash, state)?;
		debug!(HANDSHAKE_EVENTS debug, "Sent OCSP staple");
	}
	Ok(())
}

impl Tls12CertificateFields
{
	pub fn tx_certificate(self, base: &mut SessionBase, names: &mut NamesContainer, debug: Debugger,
		config: &ServerConfiguration, state: &str, crypto_algs: &mut CryptoTracker) ->
		Result<HandshakeState2, TlsFailure>
	{
		let certificate = self.certificate;
		let mut hs_hash = self.hs_hash;
		tx_certificate_common(base, TxCertificateCommonParameters{tls13: false, certificate:
			&certificate, send_spki_only: self.send_spki_only, sni_name: self.sni_name,
			no_check_certificate: self.no_check_certificate, ocsp_v1_offered: self.ocsp_v1_offered,
			ct_offered: self.ct_offered, maybe_send_ocsp: self.maybe_send_ocsp}, names, debug.clone(),
			config, &mut hs_hash, state, crypto_algs)?;

		let mut content = Vec::new();
		let marker = content.marker_posonly();

		//Emit the parameters.
		try_buffer!(content.write_u8(3));	//Named group.
		try_buffer!(content.write_u16(self.key_share.tls_id()));		//The group.
		let pubkey = self.key_share.pubkey().map_err(|x|assert_failure!("Can't get own DH public key: {}",
			x))?;
		write_sub_fn(&mut content, "Public key", TLS_LEN_TLS12_PUBKEY, |x|{
		try_buffer!(x.write_slice(pubkey.as_ref()));
			Ok(())
		})?;

		//Sign the parameters and emit the signature.
		let mut tbs = None;
		let mut err = Ok(());
		let client_random = &self.client_random;
		let server_random = &self.server_random;
		content.callback_after(marker, &mut |dhparams| {
			let x = format_tbs_tls12(dhparams, client_random, server_random);
			match x {
				Ok(y) => { tbs = Some(y); },
				Err(y) => { err = Err(y); }
			}
		});
		let tbs = tbs.ok_or(assert_failure!("Can't obtain handshake data to sign"))?;
		debug!(CRYPTO_CALCS debug, "Server TBS:\n{}", bytes_as_hex_block(&tbs, ">"));
		let signature_future = certificate.sign(&tbs);
		debug!(HANDSHAKE_EVENTS debug, "Requesting signature for key exchange");
		Ok(HandshakeState2::Tls12ServerKeyExchange(Tls12ServerKeyExchangeFields {
			key_share: self.key_share,
			incomplete_message: content,
			signature_future: signature_future,
			kill_connection_after_handshake: self.kill_connection_after_handshake,
			client_random: self.client_random,
			server_random: self.server_random,
			hs_hash: hs_hash,
			ems_enabled: self.ems_enabled,
			protection: self.protection,
			prf: self.prf,
		}))
	}
}

impl Tls13CertificateFields
{
	pub fn tx_certificate(self, base: &mut SessionBase, names: &mut NamesContainer, debug: Debugger,
		config: &ServerConfiguration, state: &str, crypto_algs: &mut CryptoTracker) ->
		Result<HandshakeState2, TlsFailure>
	{
		let mut certificate = self.certificate;
		let mut hs_hash = self.hs_hash;
		//maybe_send_ocsp is always false for TLS 1.3.
		tx_certificate_common(base, TxCertificateCommonParameters{tls13: true, certificate: &mut certificate,
			send_spki_only: self.send_spki_only, sni_name: self.sni_name, no_check_certificate:
			self.no_check_certificate, ocsp_v1_offered: self.ocsp_v1_offered, ct_offered:
			self.ct_offered, maybe_send_ocsp: false}, names, debug.clone(), config, &mut hs_hash, state,
			crypto_algs)?;
		//Sign the thing and emit the signature.
		let tbs = format_tbs_tls13(hs_hash.checkpoint()?, true)?;
		debug!(CRYPTO_CALCS debug, "Server TBS:\n{}", bytes_as_hex_block(&tbs, ">"));
		let signature_future = certificate.sign(&tbs);
		debug!(HANDSHAKE_EVENTS debug, "Requesting signature for key exchange");
		Ok(HandshakeState2::Tls13CertificateVerify(Tls13CertificateVerifyFields {
			signature_future: signature_future,
			kill_connection_after_handshake: self.kill_connection_after_handshake,
			hs_secrets: self.hs_secrets,
			hs_hash: hs_hash,
			requested_cc: self.requested_cc,
			peer_sigalgo_mask: self.peer_sigalgo_mask,
		}))
	}
}

impl Tls13CertificateVerifyFields
{
	pub fn tx_certificate_verify(self, base: &mut SessionBase, debug: Debugger, state: &str,
		crypto_algs: &mut CryptoTracker) -> Result<HandshakeState2, TlsFailure>
	{
		let is_ready = self.signature_future.settled();
		if !is_ready { return Ok(HandshakeState2::Tls13CertificateVerify(self)); }

		let (sigalgo, signature) = self.signature_future.read().map_err(|_|assert_failure!(
			"Signature request gave no answer"))?.map_err(|_|assert_failure!(
			"Can't sign handshake"))?;
		if let Some(x) = SignatureType::by_tls_id(sigalgo) {
			fail_if!(x.sigalgo_const() & self.peer_sigalgo_mask == 0,
				TlsFailure::InterlockSignatureAlgorithm);
			crypto_algs.set_signature(x);
		} else {
			sanity_failed!("Trying to sign handshake with unknown signature scheme {:x}", sigalgo);
		};
		let mut content = Vec::new();
		try_buffer!(content.write_u16(sigalgo));
		write_sub_fn(&mut content, "Signature", TLS_LEN_SIGNATURE, |x|{
			try_buffer!(x.write_slice(signature.as_ref()));
			Ok(())
		})?;
		debug!(HANDSHAKE_EVENTS debug, "Signed the key exchange");

		let mut hs_hash = self.hs_hash;
		emit_handshake(debug.clone(), HSTYPE_CERTIFICATE_VERIFY, &content, base, &mut hs_hash, state)?;

		Ok(HandshakeState2::Tls13ServerFinished(Tls13ServerFinishedFields {
			kill_connection_after_handshake: self.kill_connection_after_handshake,
			hs_secrets: self.hs_secrets,
			hs_hash: hs_hash,
			requested_cc: self.requested_cc,
		}))
	}
}

//Macro because of lifetime rules.
macro_rules! grab_issuer_key
{
	($icerts:expr, $name:expr, $talist:expr, $debug:expr) => {
		match $icerts.get(0) {
			Some(x) => Some(match extract_spki(x) {
				Ok(x) => x,
				Err(x) => {
					debug!(HANDSHAKE_EVENTS $debug, "Can't extract EE issuer key: {}", x);
					return None;
				}
			}),
			//Grab this from the trustpile.
			None => {
				let name = $name;
				$talist.get(name.0).and_then(|x|x.get_spki())
			}
		};
	}
}

pub struct Tls13ClientCertificateFields
{
	pub hs_secrets2: Tls13C2ndFSecrets,
	pub kill_connection_after_handshake: bool,
	pub hs_hash: HandshakeHash,
}

#[derive(Copy,Clone)]
struct ValidationResult
{
	ocsp: bool,
	sct: bool,
}

fn validate_certificate_chain<'a>(eecert: &ParsedCertificate<'a>, certmsg: &Tls13Certificate<'a>,
	config: &ServerConfiguration, debug: Debugger) -> Option<ValidationResult>
{
	let mut result = ValidationResult {
		ocsp: false,
		sct: false,
	};
	if certmsg.chain.len() == 0 { return None; }	//Should not even be here.
	//This is TLS 1.3, so always assert EMS, TLS13 and RENEGO.
	let mut feature_flags = CFLAG_MUST_RENEGO | CFLAG_MUST_TLS13 | CFLAG_MUST_EMS;
	let mut staple_scts: Vec<(bool, Vec<u8>)> = Vec::new();

	//Collect the stapled SCTs from the TLS extension (in TLS 1.3, these are inside Certificate message).
	for i in certmsg.stapled_scts.iter() {
		debug!(HANDSHAKE_EVENTS debug, "Client certificate: Received SCT#{} using TLS extension.",
			staple_scts.len());
		staple_scts.push((false, (*i).to_owned()));
	}
	//Collect the stapled SCTs from OCSP response if any.
	if let (&Some(ref ocsp), OCSP_SINGLE) = (&certmsg.stapled_ocsp, certmsg.stapled_ocsp_type) {
		let sctlist = match OcspDumpedScts::from(ocsp) {
			Ok(x) => x,
			Err(x) => {
				debug!(HANDSHAKE_EVENTS debug, "Error validating client certificate: Can't dump \
					SCTs from OCSP staple: {}", x);
				return None;
			}
		};
		for i in sctlist.list.iter() {
			debug!(HANDSHAKE_EVENTS debug, "Client certificate: Received SCT#{} using OCSP.",
				staple_scts.len());
			staple_scts.push((false, (*i).to_owned()));
		}
	}
	for i in eecert.scts.iter() {
		debug!(HANDSHAKE_EVENTS debug, "Client certificate: Received SCT#{} using certificate.",
			staple_scts.len());
		staple_scts.push((true, (*i).to_owned()));
	}
	
	//If any CT staples, assert CT.
	if  staple_scts.len() > 0 { feature_flags |= CFLAG_MUST_CT; }
	//If OCSP is present, assert STAPLE.
	if certmsg.stapled_ocsp_type == OCSP_SINGLE && certmsg.stapled_ocsp.is_some() {
		feature_flags |= CFLAG_MUST_STAPLE;
	}
	//Flags.
	let mut need_ocsp = (config.flags[0] & FLAGS0_REQUIRE_OCSP) != 0;
	let mut need_sct = (config.flags[0] & FLAGS0_REQUIRE_CT) != 0;
	match validate_chain_client(&certmsg.chain[0], &certmsg.chain[1..], &TimeNow, feature_flags,
		&config.trust_anchors, &config.killist, &mut need_ocsp, &mut need_sct) {
		Ok(_) => (),
		Err(x) => {
			debug!(HANDSHAKE_EVENTS debug, "Error validating client certificate: Chain validation: {}",
				x);
			return None;
		}
	}

	//Determine if certificate is short-lived.
	let short_lived_cert = eecert.not_before.delta(eecert.not_after) <= config.ocsp_maxvalid;
	//Try validating OCSP.
	if short_lived_cert {
		need_ocsp = false;	//OCSP is deemed OK.
		debug!(HANDSHAKE_EVENTS debug, "Client certificate: Not checking OCSP, since certificate is \
			short-lived");
		result.ocsp = true;
	} else if let (&Some(ref ocsp), OCSP_SINGLE) = (&certmsg.stapled_ocsp, certmsg.stapled_ocsp_type) {
		let issuer_key = grab_issuer_key!(&certmsg.chain[1..], eecert.issuer, &config.trust_anchors, debug);
		let issuer_key = match issuer_key {
			Some(x) => x,
			None => {
				debug!(HANDSHAKE_EVENTS debug, "Error validating client certificate: OCSP issuer \
					key not found (incomplete chain?)");
				return None;
			}
		};
		//Grab issuer and serial. We can assume valid EE certificate if we reach here
		//(OCSP validation to be performed).
		let issuer = eecert.issuer;
		let serial = eecert.serial_number;
		let results = match OcspValidationResult::from(ocsp, issuer, serial, issuer_key, &TimeNow) {
			Ok(x) => x,
			Err(x) => {
				debug!(HANDSHAKE_EVENTS debug, "Error validating client certificate: OCSP \
					validation failed: {}", x);
				return None;
			}
		};
		let too_long = results.response_lifetime > config.ocsp_maxvalid;
		if too_long {
			debug!(HANDSHAKE_EVENTS debug, "Client certificate: Not considering OCSP valid due to \
				excessive lifetime");
		} else {
			result.ocsp = true;
			need_ocsp = false;	//OCSP checked.
			debug!(HANDSHAKE_EVENTS debug, "Client certificate: Validated stapled OCSP response");
		}
	}
	if need_ocsp {
		debug!(HANDSHAKE_EVENTS debug, "Error validating client certificate: Valid OCSP staple required \
			but not found");
		return None;
	}
	
	//SCT checking.
	if staple_scts.len() > 0 {
		let mut _got_one = false;
		//Grab the issuer key, if any is available.
		let key = grab_issuer_key!(&certmsg.chain[1..], eecert.issuer, &config.trust_anchors, debug);
		if let Some(k) = key {
			match validate_scts(eecert.entiere, k, &mut staple_scts, &mut need_sct,
				&mut _got_one, &config.trusted_logs, debug.clone()) {
				Ok(_) => (),
				Err(x) => {
					debug!(HANDSHAKE_EVENTS debug, "Error validating client certificate: \
						Failed to verify SCT for client certificate: {}", x);
					return None;
				}
			}
		}
		result.sct = _got_one;
	}
	if need_sct {
		debug!(HANDSHAKE_EVENTS debug, "Error validating client certificate: Valid CT staple required \
			but not found");
		return None;
	}
	//Ok, it is valid.
	debug!(HANDSHAKE_EVENTS debug, "Client certificate: Verified");
	Some(result)
}

impl Tls13ClientCertificateFields
{
	pub fn rx_certificate<'a,'b>(self, _controller: &mut SessionController, certmsg: Tls13Certificate<'a>,
		debug: Debugger, raw_msg: HandshakeMessage<'b>, config: &ServerConfiguration) ->
		Result<HandshakeState2, TlsFailure>
	{
		let mut hs_hash = self.hs_hash;
		hs_hash.add(raw_msg, debug.clone())?;
		fail_if!(certmsg.context.len() > 0 , TlsFailure::CrContextMustBeEmpty);
		if certmsg.chain.len() > 0 {
			//The first certificate exists by above branch.
			let eecert = certmsg.chain[0];
			let eecert = ParsedCertificate::from(eecert).map_err(|x|TlsFailure::EECertParseError(x))?;
			let val_res = validate_certificate_chain(&eecert, &certmsg, config, debug.clone());
			let pubkey = eecert.pubkey.to_owned();
			let auth_as = ClientCertificateInfo::new_authd(&eecert, val_res.is_some(),
				val_res.map(|x|x.ocsp).unwrap_or(false), val_res.map(|x|x.sct).unwrap_or(false));
			Ok(HandshakeState2::Tls13ClientCertificateVerify(Tls13ClientCertificateVerifyFields {
				kill_connection_after_handshake: self.kill_connection_after_handshake,
				client_spki: pubkey,
				hs_secrets2: self.hs_secrets2,
				hs_hash: hs_hash,
				auth_as: auth_as,
			}))
		} else {
			//Client NAKed authentication. Proceed directly to Finished message.
			debug!(HANDSHAKE_EVENTS debug, "Client refused to send a certificate");
			Ok(HandshakeState2::Tls13ClientFinished(Tls13ClientFinishedFields {
				kill_connection_after_handshake: self.kill_connection_after_handshake,
				hs_secrets2: self.hs_secrets2,
				hs_hash: hs_hash,
				auth_as: ClientCertificateInfo::new_noauth(),
			}))
		}
	}
}

pub struct Tls13ClientCertificateVerifyFields
{
	pub hs_secrets2: Tls13C2ndFSecrets,
	pub kill_connection_after_handshake: bool,
	pub hs_hash: HandshakeHash,
	pub client_spki: Vec<u8>,
	auth_as: ClientCertificateInfo,
}

impl Tls13ClientCertificateVerifyFields
{
	//Handle TLS 1.3 CertificateVerify message.
	pub fn rx_certificate_verify<'a,'b>(self, _controller: &mut SessionController,
		cvmsg: TlsCertificateVerify<'a>, debug: Debugger, raw_msg: HandshakeMessage) ->
		Result<HandshakeState2, TlsFailure>
	{
		//Format TBS and verify signature.
		let cv_hash = self.hs_hash.checkpoint()?;
		let tbs = format_tbs_tls13(cv_hash, false)?;
		debug!(CRYPTO_CALCS debug, "Client TBS:\n{}", bytes_as_hex_block(&tbs, ">"));
		cvmsg.signature.verify(&self.client_spki, &tbs, VERIFY_FLAG_NO_RSA_PKCS1).map_err(|_|
			TlsFailure::CvSignatureFailed)?;
		debug!(HANDSHAKE_EVENTS debug, "Client key exchange signature ({:?}) OK",
			cvmsg.signature.algorithm);

		let mut hs_hash = self.hs_hash;
		hs_hash.add(raw_msg, debug)?;
		Ok(HandshakeState2::Tls13ClientFinished(Tls13ClientFinishedFields{
			kill_connection_after_handshake: self.kill_connection_after_handshake,
			hs_secrets2: self.hs_secrets2,
			hs_hash: hs_hash,
			auth_as: self.auth_as,
		}))
	}
}


pub struct Tls13CertificateRequestFields
{
	pub hs_secrets: Tls13HsSecrets,
	pub certificate: Box<CertificateSigner+Send>,
	pub send_spki_only: bool,
	pub sni_name: Option<String>,
	pub no_check_certificate: bool,
	pub ocsp_v1_offered: bool,
	pub ct_offered: bool,
	pub kill_connection_after_handshake: bool,
	pub hs_hash: HandshakeHash,
	pub peer_sigalgo_mask: u64,
}

impl Tls13CertificateRequestFields
{
	pub fn tx_certificate_request(self, base: &mut SessionBase, debug: Debugger, state: &str) ->
		Result<HandshakeState2, TlsFailure>
	{
		let mut content = Vec::new();
		try_buffer!(content.write_u8(0));	//Empty context.
		write_sub_fn(&mut content, "extension list", TLS_LEN_EXTENSIONS, |x|{
			//Request status and CT.
			write_ext_fn(x, EXT_STATUS_REQUEST, "status_reqeust", |_|Ok(()))?;
			write_ext_fn(x, EXT_CT, "certificate_transparency", |_|Ok(()))?;
			//Include signature_algorithms extension.
			write_signature_algorithms_ext(x)?;
			Ok(())
		})?;
		let mut hs_hash = self.hs_hash;
		emit_handshake(debug.clone(), HSTYPE_CERTIFICATE_REQUEST, &content, base, &mut hs_hash, state)?;
		debug!(HANDSHAKE_EVENTS debug, "Sent certificate request");
		Ok(HandshakeState2::Tls13Certificate(Tls13CertificateFields {
			certificate: self.certificate,
			ct_offered: self.ct_offered,
			no_check_certificate: self.no_check_certificate,
			ocsp_v1_offered: self.ocsp_v1_offered,
			send_spki_only: self.send_spki_only,
			sni_name: self.sni_name,
			kill_connection_after_handshake: self.kill_connection_after_handshake,
			hs_secrets: self.hs_secrets,
			hs_hash: hs_hash,
			requested_cc: true,
			peer_sigalgo_mask: self.peer_sigalgo_mask,
		}))
	}
}

pub struct Tls12ServerKeyExchangeFields
{
	pub incomplete_message: Vec<u8>,
	pub signature_future: FutureReceiver<Result<(u16, Vec<u8>),()>>,
	pub key_share: Box<DiffieHellmanKey+Send>,
	pub kill_connection_after_handshake: bool,
	pub client_random: [u8; 32],
	pub server_random: [u8; 32],
	pub hs_hash: HandshakeHash,
	pub ems_enabled: bool,
	pub protection: ProtectorType,
	pub prf: HashFunction,
}

impl Tls12ServerKeyExchangeFields
{
	pub fn tx_server_key_exchange(self, base: &mut SessionBase, debug: Debugger, state: &str,
		crypto_algs: &mut CryptoTracker) -> Result<HandshakeState2, TlsFailure>
	{
		let is_ready = self.signature_future.settled();
		if !is_ready { return Ok(HandshakeState2::Tls12ServerKeyExchange(self)); }

		let (sigalgo, signature) = self.signature_future.read().map_err(|_|assert_failure!(
			"Signature request gave no answer"))?.map_err(|_|assert_failure!(
			"Can't sign handshake"))?;
		if let Some(x) = SignatureType::by_tls_id(sigalgo) {
			crypto_algs.set_signature(x);
		} else {
			sanity_failed!("Trying to sign handshake with unknown signature scheme {:x}", sigalgo);
		};
		let mut content = self.incomplete_message;
		try_buffer!(content.write_u16(sigalgo));
		write_sub_fn(&mut content, "Signature", TLS_LEN_SIGNATURE, |x|{
			try_buffer!(x.write_slice(signature.as_ref()));
			Ok(())
		})?;
		debug!(HANDSHAKE_EVENTS debug, "Signed the key exchange");

		let mut hs_hash = self.hs_hash;
		emit_handshake(debug, HSTYPE_SERVER_KEY_EXCHANGE, &content, base, &mut hs_hash, state)?;
		Ok(HandshakeState2::Tls12ServerHelloDone(Tls12ServerHelloDoneFields {
			key_share: self.key_share,
			kill_connection_after_handshake: self.kill_connection_after_handshake,
			client_random: self.client_random,
			server_random: self.server_random,
			hs_hash: hs_hash,
			ems_enabled: self.ems_enabled,
			protection: self.protection,
			prf: self.prf,
		}))
	}
}
