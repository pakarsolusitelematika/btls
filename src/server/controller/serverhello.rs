use super::{do_tls13_key_agreement, emit_alpn_extension, find_matching_ciphersuite, HandshakeState2,
	Tls13HsSecrets, tx_sct_ext_common};
use super::certificate::Tls12CertificateFields;
use super::encryptedextensions::Tls13EncryptedExtensionsFields;
use ::common::{CERTTYPE_RPK, Ciphersuite, CryptoTracker, Debugger, emit_handshake, EXT_EMS, EXT_KEY_SHARE,
	EXT_RECORD_SIZE_LIMIT, EXT_RENEGO_INFO, EXT_SERVER_CERTIFICATE_TYPE, EXT_STATUS_REQUEST, HandshakeHash,
	HandshakeMessage, HashMessages, HSTYPE_HELLO_RETRY_REQUEST, HSTYPE_SERVER_HELLO, KeyExchangeType,
	NamesContainer, TLS_LEN_EXTENSIONS, TLS_LEN_TLS13_PUBKEY, TlsFailure, TlsVersion, write_ext_fn,
	write_sub_fn};
use ::features::FLAGS0_ENABLE_TLS13;
use ::logging::bytes_as_hex_block;
use ::messages::TlsClientHello;
use ::record::{DiffieHellmanSharedSecret, SessionBase, SessionController, Tls13HandshakeSecret, Tls13MasterSecret};
use ::server::ServerConfiguration;
use ::server_certificates::CertificateSigner;
use ::stdlib::{Box, Deref, String, ToOwned, Vec};

use btls_aux_aead::ProtectorType;
use btls_aux_dhf::DiffieHellmanKey;
use btls_aux_futures::FutureReceiver;
use btls_aux_hash::HashFunction;
use btls_aux_random::secure_random;
use btls_aux_serialization::Sink;

pub struct ServerHelloFields
{
	pub certlookup_future: FutureReceiver<Result<Box<CertificateSigner+Send>, ()>>,
	pub key_share: Box<DiffieHellmanKey+Send>,
	pub send_spki_only: bool,
	pub sni_name: Option<String>,
	pub no_check_certificate: bool,
	pub ocsp_v1_offered: bool,
	pub ct_offered: bool,
	pub reduced_record_size: bool,
	pub ems_offered: bool,
	pub chosen_ciphersuite: Ciphersuite,
	pub real_version: TlsVersion,
	pub ciphersuite_mask: u64,
	pub tls12_partial_cipher: (ProtectorType, HashFunction),
	pub is_acme_challenge: bool,
	pub using_tls13: bool,
	pub kill_connection_after_handshake: bool,
	pub client_random: [u8; 32],
	pub dh_secret: Option<DiffieHellmanSharedSecret>,
	pub hs_hash: HandshakeHash,
	pub peer_sigalgo_mask: u64,
}

impl ServerHelloFields
{
	pub fn rx_client_hello_again<'a>(self, _controller: &mut SessionController, pmsg: TlsClientHello<'a>,
		debug: Debugger, raw_msg: HandshakeMessage) -> Result<HandshakeState2, TlsFailure>
	{
		let mut key_share = self.key_share;
		let mut hs_hash = self.hs_hash;
		//This is always TLS 1.3. We are only interested on the key shares.
		debug!(HANDSHAKE_EVENTS debug, "Received remedial ClientHello");
		//Key share extension.
		let keyshare = pmsg.key_shares;

		let group = key_share.group();
		let dh_secret = do_tls13_key_agreement(keyshare, &mut key_share, group, debug.clone())?;
		fail_if!(dh_secret.is_none(), TlsFailure::ChDoubleRestart);

		hs_hash.add(raw_msg, debug)?;
		Ok(HandshakeState2::ServerHello(ServerHelloFields{
			dh_secret: dh_secret,
			key_share:  key_share,
			hs_hash: hs_hash,
			..self
		}))
	}
	pub fn tx_hello_restart_request(mut self, base: &mut SessionBase, debug: Debugger, state: &str) ->
		Result<HandshakeState2, TlsFailure>
	{
		let mut content = Vec::new();
		//HelloRestartRequest constists of
		//- TLS version (2 bytes)
		//- Ciphersuite.
		//- Extensions (single extension of type 40, 2 bytes, containg the group.
		try_buffer!(content.write_u16(self.real_version.to_code()));
		//At least draft-19, or TLS 1.3 final. These have extra ciphersuite field in HRR.
		if self.real_version.draft_code().at_least_19() {
			try_buffer!(content.write_u16(self.chosen_ciphersuite.to_tls_id()));
		}
		try_buffer!(content.write_u16(6));	//Length of extensions
		try_buffer!(content.write_u16(40));	//Key_share
		try_buffer!(content.write_u16(2));	//2 Bytes in key share.
		try_buffer!(content.write_u16(self.key_share.tls_id()));	//The group

		emit_handshake(debug.clone(), HSTYPE_HELLO_RETRY_REQUEST, &content, base, &mut self.hs_hash, state)?;
		debug!(HANDSHAKE_EVENTS debug, "Restarting handshake to get share for group {:?}", self.key_share.
			group());
		Ok(HandshakeState2::RetryClientHello(self))
	}
	pub fn tx_server_hello(mut self, base: &mut SessionBase, names: &mut NamesContainer, debug: Debugger,
		config: &ServerConfiguration, state: &str, crypto_algs: &mut CryptoTracker) ->
		Result<HandshakeState2, TlsFailure>
	{
		let mut maybe_send_ocsp = false;
		let mut chosen_ciphersuite = self.chosen_ciphersuite;
		let key_share = self.key_share;
		let ocsp_v1_offered = self.ocsp_v1_offered;
		let send_spki_only = self.send_spki_only;
		let ct_offered = self.ct_offered;
		let ems_offered = self.ems_offered;
		let reduced_record_size = self.reduced_record_size;
		let dh_secret = self.dh_secret;
		if self.is_acme_challenge {
			debug!(HANDSHAKE_EVENTS debug, "Connection is for ACME validation, closing after handshake");
			//ACME connections can't be used for data transport.
			base.set_internal_only();
		}
		let tls13 = self.using_tls13;
		//Fixup the record version... Some stacks seem to be picky.
		if !tls13 { base.set_use_tls12_records(); }
		//The delayed certificate.
		let mut certificate = {
			let certlookup_future = self.certlookup_future;
			let sni_name = &self.sni_name;
			certlookup_future.read().map_err(|_|assert_failure!("Certificate \
				selection gave no answer"))?.map_err(|_|TlsFailure::NoCertificateAvailable(match
				sni_name { &Some(ref x) => x.deref(), &None => "<default>" }.to_owned()))?
		};
		debug!(HANDSHAKE_EVENTS debug, "Certificate selected for the connection");
		if !tls13 {
			let kex_to_use = if certificate.is_ecdsa() {
				KeyExchangeType::Tls12EcdheEcdsa
			} else {
				KeyExchangeType::Tls12EcdheRsa
			};
			//Find ciphersuite statisfying the constraints.
			chosen_ciphersuite = find_matching_ciphersuite(Some(kex_to_use),
				Some(self.tls12_partial_cipher.0), Some(self.tls12_partial_cipher.1),
				self.ciphersuite_mask)?;
			debug!(HANDSHAKE_EVENTS debug, "Chosen ciphersuite is {:?}", chosen_ciphersuite);
			crypto_algs.set_ciphersuite(chosen_ciphersuite);
		}

		let mut content = Vec::new();
		//Version: Either TLS 1.2 or TLS 1.3 according to client offer.
		try_buffer!(content.write_u16(self.real_version.to_code()));
		//Random: Also include sentinel if supporting 1.3, but offered <1.3.
		let mut server_random = [0; 32];
		secure_random(&mut server_random);
		if !tls13 && (config.flags[0] & FLAGS0_ENABLE_TLS13) != 0 {
			//Downgrade hack.
			const DOWNGRADE_SENTINEL: [u8; 8] = [0x44, 0x4F, 0x57, 0x4E, 0x47, 0x52, 0x44, 0x01];
			(&mut server_random[24..32]).copy_from_slice(&DOWNGRADE_SENTINEL);
		}
		debug!(CRYPTO_CALCS debug, "Using server random:\n{}", bytes_as_hex_block(&server_random, ">"));
		try_buffer!(content.write_slice(&server_random));
		//Session ID: Blank.
		if !tls13 { try_buffer!(content.write_u8(0)); }
		//Cipher suite.
		try_buffer!(content.write_u16(chosen_ciphersuite.to_tls_id()));
		debug!(HANDSHAKE_EVENTS debug, "Using Ciphersuite {:?}", chosen_ciphersuite);
		//Compression method: Always NULL.
		if !tls13 { try_buffer!(content.write_u8(0)); }
		//Extensions:
		write_sub_fn(&mut content, "ServerHello extensions", TLS_LEN_EXTENSIONS, |x|{
			let have_ocsp =  certificate.get_ocsp().0.is_some();

			//If offered, ACK OCSP on TLS 1.2. Don't do this in SPKI mode.
			if ocsp_v1_offered && have_ocsp && !send_spki_only && !tls13 {
				maybe_send_ocsp = true;
				write_ext_fn(x, EXT_STATUS_REQUEST, "status_request", |_|Ok(()))?;
			}
			//Extension ALPN... We give the ALPN if present in TLS 1.2.
			if !tls13 { emit_alpn_extension(x, names)?; }
			//Extension SCT. We send this if we have data in TLS 1.2 if offered.
			if ct_offered && !tls13 {
				tx_sct_ext_common(x, &mut certificate, debug.clone(), crypto_algs)?;
			}
			//Server key type. If in TLS 1.2 and using RPK, send this.
			if send_spki_only && !tls13 {
				write_ext_fn(x, EXT_SERVER_CERTIFICATE_TYPE, "server_certificate_type", |x|{
					try_buffer!(x.write_u8(CERTTYPE_RPK));
					Ok(())
				})?;
			}
			//Extension EMS. We always send this in TLS 1.2 if offered.
			if ems_offered && !tls13 {
				crypto_algs.enable_ems();
				write_ext_fn(x, EXT_EMS, "extended_master_secret", |_|Ok(()))?;
				debug!(HANDSHAKE_EVENTS debug.clone(), "Using Extended master secret");
			}
			//Extension key_share. We always send this in TLS 1.3
			if tls13 {
				write_ext_fn(x, EXT_KEY_SHARE, "key_share", |x|{
					try_buffer!(x.write_u16(key_share.tls_id()));
					let pubkey = key_share.pubkey().map_err(|x|assert_failure!(
						"Can't get own DH public key: {}", x))?;
					write_sub_fn(x, "Public key", TLS_LEN_TLS13_PUBKEY, |x|{
						try_buffer!(x.write_slice(pubkey.as_ref()));
						Ok(())
					})
				})?;
			}
			//Extension renegotiation_info. We always send this in TLS 1.2.
			if !tls13 {
				write_ext_fn(x, EXT_RENEGO_INFO, "renegotiation_info", |x|{
					try_buffer!(x.write_u8(0));
					Ok(())
				})?;
			}
			//Extension record_size_limit, send this if reduced in TLS 1.2.
			if !tls13 && reduced_record_size {
				write_ext_fn(x, EXT_RECORD_SIZE_LIMIT, "record_size_limit", |x|{
					//Always 16384.
					try_buffer!(x.write_u16(16384));
					Ok(())
				})?;
			}
			Ok(())
		})?;

		emit_handshake(debug.clone(), HSTYPE_SERVER_HELLO, &content, base, &mut self.hs_hash, state)?;
		Ok(if tls13 {
			let dh_secret = match dh_secret {
				Some(x) => x,
				None => sanity_failed!("DH shared secret doesn't exist even after remedial CH")
			};
			crypto_algs.set_kex_group(dh_secret.get_group());
			let hs_secret = Tls13HandshakeSecret::new_dhe(dh_secret, self.tls12_partial_cipher.0,
				self.tls12_partial_cipher.1, true, self.real_version.draft_code(), debug.clone())?;
			let sh_hash = self.hs_hash.checkpoint()?;
			let client_hs_write = hs_secret.traffic_secret_in(&sh_hash, debug.clone())?;
			let server_hs_write = hs_secret.traffic_secret_out(&sh_hash, debug.clone())?;
			//Note: There is a nasty special case here: If client chokes on the ServerHello message,
			//it will never switch on encryption, and will send its alert unencrypted, wheras normally
			//it will send an encrypted packet.
			base.change_protector(server_hs_write.to_protector(debug.clone())?);
			base.change_deprotector(client_hs_write.to_deprotector(debug.clone())?);
			base.next_record_may_be_unencrypted_alert();
			let master_secret = Tls13MasterSecret::new(hs_secret, debug.clone())?;
			debug!(HANDSHAKE_EVENTS debug, "Sent ServerHello, enabling downstream&upstream encryption");
			HandshakeState2::Tls13EncryptedExtensions(Tls13EncryptedExtensionsFields {
				certificate: certificate,
				ct_offered: ct_offered,
				no_check_certificate: self.no_check_certificate,
				ocsp_v1_offered: ocsp_v1_offered,
				send_spki_only: send_spki_only,
				sni_name: self.sni_name,
				reduced_record_size: reduced_record_size,
				kill_connection_after_handshake: self.kill_connection_after_handshake,
				hs_secrets: Tls13HsSecrets{
					client_hs_write: client_hs_write,
					server_hs_write: server_hs_write,
					master_secret: master_secret,
				},
				hs_hash: self.hs_hash,
				peer_sigalgo_mask: self.peer_sigalgo_mask,
				real_version: self.real_version,
			})
		} else {
			HandshakeState2::Tls12Certificate(Tls12CertificateFields {
				certificate: certificate,
				ct_offered: ct_offered,
				no_check_certificate: self.no_check_certificate,
				ocsp_v1_offered: ocsp_v1_offered,
				send_spki_only: send_spki_only,
				sni_name: self.sni_name,
				key_share: key_share,
				maybe_send_ocsp: maybe_send_ocsp,
				kill_connection_after_handshake: self.kill_connection_after_handshake,
				client_random: self.client_random,
				server_random: server_random,
				hs_hash: self.hs_hash,
				ems_enabled: ems_offered,
				protection: self.tls12_partial_cipher.0,
				prf: self.tls12_partial_cipher.1,
			})
		})
	}
}
