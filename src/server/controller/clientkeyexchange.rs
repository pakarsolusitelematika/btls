use super::HandshakeState2;
use super::finished::Tls12ClientChangeCipherSpecFields;
use ::common::{Debugger, HandshakeHash, HandshakeMessage, HashMessages, TlsFailure};
use ::messages::TlsClientKeyExchange;
use ::record::{DiffieHellmanSharedSecret, SessionController, Tls12PremasterSecret};
use ::stdlib::Box;

use btls_aux_aead::ProtectorType;
use btls_aux_dhf::DiffieHellmanKey;
use btls_aux_hash::HashFunction;

pub struct Tls12ClientKeyExchangeFields
{
	pub key_share: Box<DiffieHellmanKey+Send>,
	pub kill_connection_after_handshake: bool,
	pub client_random: [u8; 32],
	pub server_random: [u8; 32],
	pub hs_hash: HandshakeHash,
	pub ems_enabled: bool,
	pub protection: ProtectorType,
	pub prf: HashFunction,
}

impl Tls12ClientKeyExchangeFields
{
	pub fn rx_client_key_exchange<'a>(self, _controller: &mut SessionController, pubkey: TlsClientKeyExchange,
		debug: Debugger, raw_msg: HandshakeMessage<'a>) -> Result<HandshakeState2, TlsFailure>
	{
		let mut hs_hash = self.hs_hash;
		let mut key_share = self.key_share;
		let sharedkey = DiffieHellmanSharedSecret::new(&mut key_share, pubkey.pubkey)?;
		let premaster_secret = Tls12PremasterSecret::new(sharedkey, self.protection, self.prf, true,
			self.ems_enabled, debug.clone())?;
		debug!(HANDSHAKE_EVENTS debug, "Diffie-Hellman key agreement performed");

		hs_hash.add(raw_msg, debug)?;
		Ok(HandshakeState2::Tls12ClientChangeCipherSpec(Tls12ClientChangeCipherSpecFields {
			kill_connection_after_handshake: self.kill_connection_after_handshake,
			premaster_secret: premaster_secret,
			client_random: self.client_random,
			server_random: self.server_random,
			hs_hash: hs_hash,
		}))
	}
}
