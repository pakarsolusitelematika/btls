use super::{do_tls13_key_agreement, find_matching_ciphersuite, HandshakeState2, SelectProtectionParameters,
	select_protection, VERSION_TLS12, VERSION_TLS13};
use super::serverhello::ServerHelloFields;
use ::acmecert::AcmeCertificate;
use ::common::{CERTTYPE_STRINGS, Ciphersuite, CIPHERSUITE_STRINGS, CryptoTracker, Debugger, DHGROUP_STRINGS,
	HandshakeHash, HandshakeMessage, HashMessages, KeyExchangeType, NamesContainer, PrintBitfieldFlags,
	TlsFailure, TlsVersion};
use ::features::{FLAGS0_ALLOW_BAD_CRYPTO, FLAGS0_ASSUME_HW_AES, FLAGS0_ENABLE_TLS12, FLAGS0_ENABLE_TLS13,
	FLAGS0_REQUIRE_EMS};
use ::logging::bytes_as_hex_block;
use ::messages::{AlpnListIterator, KeyShareList, TlsClientHello};
use ::record::{DiffieHellmanSharedSecret, SessionController};
use ::server::ServerConfiguration;
use ::server_certificates::{CertificateLookupCriteria, CertificateSigner};
use ::stdlib::{Box, BTreeMap, Deref, from_utf8, String, ToOwned};
use ::system::Path;

use btls_aux_aead::ProtectorType;
use btls_aux_dhf::{Dhf, DiffieHellmanKey};
use btls_aux_futures::FutureReceiver;
use btls_aux_hash::HashFunction;
use btls_aux_serialization::SlicingExt;
use btls_aux_signature_algo::{SIGALGO_ECDSA_MASK, SIGALGO_RSA_MASK, SIGALGO_STRINGS, SIGALGO_TLS13_MASK};


pub struct ClientHelloFields
{
}

struct NegotiateCiphersuiteReturn
{
	protection: ProtectorType,
	prf: HashFunction,
	chosen_ciphersuite: Ciphersuite,
}

//Select ciphersuite with TLS 1.3 key exchange.
fn negotiate_ciphersuite(ciphersuite_mask: u64, prefer_chacha: bool, config: &ServerConfiguration) ->
	Result<NegotiateCiphersuiteReturn, TlsFailure>
{
	let hw_aes = config.flags[0] & FLAGS0_ASSUME_HW_AES != 0;
	let (protection, prfhash) = select_protection(SelectProtectionParameters{prefer_chacha: prefer_chacha,
		ciphersuite_mask: ciphersuite_mask, tls13: true, hw_aes: hw_aes})?;
	let chosen_ciphersuite = find_matching_ciphersuite(Some(KeyExchangeType::Tls13),
		Some(protection), Some(prfhash), ciphersuite_mask)?;
	let hash = chosen_ciphersuite.get_prf();
	Ok(NegotiateCiphersuiteReturn {
		protection: protection,
		prf: hash,
		chosen_ciphersuite: chosen_ciphersuite
	})
}

//borrow_memory needs to be at least 64 bytes.
fn fn_for_challenge<'a>(chal: &str, borrow_memory: &'a mut [u8]) -> Option<&'a str>
{
	if borrow_memory.len() < 64 { return None; }
	if chal.len() != 65 { return None; }
	for i in chal.chars().enumerate() {
		let x = i.1 as u32;
		if i.0 == 32 {
			if x != 46 { return None; }	//. should be here.
			continue;
		}
		if x < 48 || (x > 57 && x < 97) || x > 102 { return None; }
		let idx = i.0.wrapping_sub(if i.0 > 32 {1} else {0});
		*match borrow_memory.get_mut(idx) { Some(x) => x, None => return None} = x as u8;
	}
	match from_utf8(match borrow_memory.slice_np(..64) { Some(x) => x, None => return None}) {
		Ok(x) => Some(x),//borrow_memory needs to be at least 64 bytes.
		Err(_) => None,
	}
}

//borrow_memory needs to be at least 65 bytes.
fn challenge_for_fn<'a>(fname: &Path, borrow_memory: &'a mut [u8]) -> Option<&'a str>
{
	if borrow_memory.len() < 65 { return None; }
	let fname = match fname.to_str() { Some(x) => x, None => return None };
	if fname.len() != 64 { return None; }
	*match borrow_memory.get_mut(32) { Some(x) => x, None => return None} = 46;	//'.'
	for i in fname.chars().enumerate() {
		let x = i.1 as u32;
		if x < 48 || (x > 57 && x < 97) || x > 102 { return None; }
		let idx = i.0.wrapping_add(i.0 >> 5);
		*match borrow_memory.get_mut(idx) { Some(x) => x, None => return None} = x as u8;
	}
	match from_utf8(match borrow_memory.slice_np(..65) { Some(x) => x, None => return None}) {
		Ok(x) => Some(x),
		Err(_) => None,
	}
}

fn handle_alpn<'a>(list: AlpnListIterator<'a>, names: &mut NamesContainer, debug: Debugger,
	config: &ServerConfiguration) -> Result<(), TlsFailure>
{
	let mut known_alpns = BTreeMap::new();
	for i in config.supported_alpn.iter() { known_alpns.insert(i.clone(), ()); }
	let mut set_alpn = false;
	for alpn in list {
		if known_alpns.contains_key(alpn) {
			debug!(HANDSHAKE_EVENTS debug, "Using Application Layer Protocol '{}'", alpn);
			names.set_alpn(alpn.to_owned());
			set_alpn = true;
			break;
		}
	}
	fail_if!(known_alpns.len() > 0 && !set_alpn, TlsFailure::AlpnNoOverlap);
	Ok(())
}

struct NegotiateCryptoParameters<'a>
{
	ciphersuite_mask: u64,
	prefer_chacha: bool,
	sigalgo_mask: u64,
	group_mask: u64,
	keyshare: Option<KeyShareList<'a>>,
	version: TlsVersion,
	use_acme_challenge: Option<(String, Option<String>)>,
	sni_name: &'a Option<String>,
	client_accepts_spki: bool,
}

struct NegotiateCryptoRetrurn
{
	dh_secret: Option<DiffieHellmanSharedSecret>,
	certificate: FutureReceiver<Result<Box<CertificateSigner+Send>, ()>>,
	tls12_partial_cipher: (ProtectorType, HashFunction),
	chosen_ciphersuite: Ciphersuite,	//Note: This is garbage value for TLS 1.2.
	key_share: Box<DiffieHellmanKey+Send>,
	is_acme_challenge: bool,
	no_check_certificate: bool,
	kill_connection_after_handshake: bool,
	group: Dhf,
}

//Returns true on success, false if restart is required.
fn negotiate_crypto<'a>(params: NegotiateCryptoParameters<'a>, debug: Debugger, config: &ServerConfiguration,
	crypto_algs: &mut CryptoTracker) -> Result<NegotiateCryptoRetrurn, TlsFailure>
{
	let mut kill_connection_after_handshake = false;
	let mut is_acme_challenge = false;
	let mut no_check_certificate = false;
	let mut group = Dhf::X25519;
	let mut found_group = false;
	//Because we only support ECDHE, we can choose group freely.
	static GROUP_PRIO: [Dhf; 5] = [Dhf::X25519, Dhf::NsaP256, Dhf::X448, Dhf::NsaP384, Dhf::NsaP521];
	for i in GROUP_PRIO.iter() {
		if params.group_mask & i.dhfalgo_const() != 0 {
			group = *i;
			found_group = true;
			break;
		}
	}
	fail_if!(!found_group, TlsFailure::NoMutualGroups);
	//Check if client supports RSA&ECDSA.
	let mut supports_rsa = false;
	let mut supports_ecdsa = false;
	for i in Ciphersuite::all_suites().iter() {
		if params.ciphersuite_mask & i.algo_const() != 0 {
			supports_rsa |= i.get_key_exchange() == KeyExchangeType::Tls12EcdheRsa;
			supports_ecdsa |= i.get_key_exchange() == KeyExchangeType::Tls12EcdheEcdsa;
		}
	}
	let sigalgos_enabled = if !params.version.is_tls12() {
		SIGALGO_TLS13_MASK
	} else {
		let mut sigalgos_enabled = 0;
		if supports_rsa { sigalgos_enabled |= SIGALGO_RSA_MASK; }
		if supports_ecdsa { sigalgos_enabled |= SIGALGO_ECDSA_MASK; }
		sigalgos_enabled
	};
	let ee_sigalgo_mask = params.sigalgo_mask & sigalgos_enabled;

	//Choose the certificate.
	let certlookup_future = if let &Some((ref x, ref y)) = &params.use_acme_challenge {
		let challenge = x.deref();
		let response = match y { &Some(ref z) => Some(z.deref()), &None => None };
		let cert = AcmeCertificate::new(challenge, response, config.acme_key.clone()).
			map_err(|x|assert_failure!("Failed to generate ACME challenge certificate: \
			{}", x))?;
		let cert: Box<CertificateSigner+Send> = Box::new(cert);
		let certlookup_future = FutureReceiver::from(Ok(cert));
		//Set the kill connection after handshake flag, since we clear the
		//use_acme_challenge, and want to still have a flag to kill the connection.
		//Set the no_check_certificate, since certificate sanity check does not like ACME
		//certificates.
		//Set is_acme_challenge, since use_acme_challenge gets cleared, and we want to do
		//some specials in ServerHello based on it.
		kill_connection_after_handshake = true;
		no_check_certificate = true;
		is_acme_challenge = true;
		//We could do the rest of negotiation here, since certificate has alread been
		//settled, but for easy-of-implementation, we follow the normal path.
		certlookup_future
	} else {
		let name_str = match params.sni_name {
			&Some(ref x) => Some(x.deref()),
			&None => match config.default_sni_name {
				Some(ref x) => Some(x.deref()),
				None => None
			}
		};
		let criteria = CertificateLookupCriteria {
			sni: name_str,
			sig_flags: params.sigalgo_mask,
			ee_flags: ee_sigalgo_mask,
			tls13: !params.version.is_tls12(),
			selfsigned_ok: params.client_accepts_spki,
			__dummy: ()
		};
		config.lookup_cert.lookup(&criteria)
	};

	if !params.version.is_tls12() {
		crypto_algs.set_version(params.version);
		//In TLS 1.3, we can run rest of the negotiation, because legacy certificate types are
		//gone! Yay.
		let ret = negotiate_ciphersuite(params.ciphersuite_mask, params.prefer_chacha, config)?;
		let mut key_share = group.generate_key().map_err(|x|assert_failure!("Can't generate \
			Diffie-Hellman keypair for group #{}: {}", group.to_tls_id(), x))?;
		let dh_secret = do_tls13_key_agreement(params.keyshare, &mut key_share, group, debug.clone())?;
		return Ok(NegotiateCryptoRetrurn {
			dh_secret: dh_secret,
			certificate: certlookup_future,
			tls12_partial_cipher: (ret.protection, ret.prf),
			chosen_ciphersuite: ret.chosen_ciphersuite,
			key_share: key_share,
			is_acme_challenge: is_acme_challenge,
			no_check_certificate: no_check_certificate,
			kill_connection_after_handshake: kill_connection_after_handshake,
			group: group,
		});
	} else {
		//We do preliminary ciphersuite negotiation now. This causes problems with some insane
		//clients, but we don't support those.
		let hw_aes = config.flags[0] & FLAGS0_ASSUME_HW_AES != 0;
		let tls12_partial_cipher = select_protection(SelectProtectionParameters{prefer_chacha:
			params.prefer_chacha, ciphersuite_mask: params.ciphersuite_mask, tls13: false,
			hw_aes: hw_aes})?;
		//Generate key share for group.
		let key_share = group.generate_key().map_err(|x|assert_failure!("Can't generate \
			Diffie-Hellman keypair for group #{}: {}", group.to_tls_id(), x))?;
		return Ok(NegotiateCryptoRetrurn {
			dh_secret: None,	//DH secret not available for TLS 1.2.
			certificate: certlookup_future,
			tls12_partial_cipher: tls12_partial_cipher,
			chosen_ciphersuite: Ciphersuite::random_suite(),	//Just return something.
			key_share: key_share,
			is_acme_challenge: is_acme_challenge,
			no_check_certificate: no_check_certificate,
			kill_connection_after_handshake: kill_connection_after_handshake,
			group: group,
		});
	}
}



fn get_acme_challenge1(sni: &str, config: &ServerConfiguration) -> Option<(String, Option<String>)>
{
	match config.acme_challenges.read() {
		Ok(x) => x.entries.get(sni).map(|y|(sni.to_owned(), y.clone())),
		Err(_) => None,
	}
}

fn get_acme_challenge2(sni: &str, config: &ServerConfiguration) -> Option<(String, Option<String>)>
{
	let mut tmp1 = [0u8;70];
	let mut tmp2 = [0u8;70];
	if let &Some(ref acmedir) = &config.acme_path {
		let mut lpath = acmedir.clone();
		if sni.ends_with(".token.acme.invalid") {
			//TLS-SNI-02.
			let fname = fn_for_challenge(&sni[..sni.len()-19], &mut tmp1[..]);
			lpath.push(match fname { Some(x) => x, None => return None });
			let target = match lpath.read_link() {
				Ok(x) => match challenge_for_fn(&x, &mut tmp2[..]) {
					Some(y) => format!("{}.ka.acme.invalid", y),
					None => return None
				},
				Err(_) => return None
			};
			return Some((sni.to_owned(), Some(target)));
		};
		if sni.ends_with(".acme.invalid") {
			//TLS-SNI-01.
			let fname = fn_for_challenge(&sni[..sni.len()-13], &mut tmp1[..]);
			lpath.push(match fname { Some(x) => x, None => return None });
			match lpath.read_link() {
				Ok(x) => match x.to_str() {
					Some(_) => (),	//Ok, points somewhere.
					None => return None
				},
				Err(_) => return None
			};
			return Some((sni.to_owned(), None));
		}
	}
	None
}

impl ClientHelloFields
{
	pub fn rx_client_hello<'a>(self, _controller: &mut SessionController, pmsg: TlsClientHello<'a>,
		names: &mut NamesContainer, debug: Debugger, config: &mut ServerConfiguration,
		raw_msg: HandshakeMessage, crypto_algs: &mut CryptoTracker) -> Result<HandshakeState2, TlsFailure>
	{
		let mut ciphersuite_mask = 0;
		let mut prefer_chacha = None;
		let mut sigalgo_mask = 0;
		let mut group_mask = 0;
		let mut recvd_signature_algorithms = false;
		let mut recvd_supported_groups = false;
		let mut send_spki_only = false;
		let mut use_acme_challenge = None;
		let mut sni_name = None;
		let mut ct_offered = false;
		let mut ems_offered = false;
		let mut reduced_record_size = false;

		let tls12_enabled = (config.flags[0] & FLAGS0_ENABLE_TLS12) != 0;
		let tls13_enabled = (config.flags[0] & FLAGS0_ENABLE_TLS13) != 0;
		let allow_bad_ciphers = (config.flags[0] & FLAGS0_ALLOW_BAD_CRYPTO) != 0;

		//Check for bad ciphers.
		fail_if!(!allow_bad_ciphers && pmsg.bad_ciphers, TlsFailure::ClientSupprotsBadCiphers);

		//Version negotiation.
		let mut version = None;
		if tls12_enabled && pmsg.tls12_supported {
			version = Some(VERSION_TLS12);			//TLS 1.2
		}
		if let (true, Some(tls13ver)) = (tls13_enabled, pmsg.tls13_variant) {
			if tls13ver == 255 {
				version = Some(VERSION_TLS13); 	//TLS 1.3 final.
			} else if tls13ver > 0 && tls13ver < 254 {
				version = Some(0x7f00 + (tls13ver as u16));
			}
		}
		let version = version.ok_or(TlsFailure::ChUnsupportedClientVersion(pmsg.client_version.to_code().
			saturating_sub(0x301)))?;
		let real_version = version;
		let real_version_c = TlsVersion::by_tls_id(version, false).map_err(|_|assert_failure!(
			"WTF with client version {:04x}", version))?;
		let version = if version >> 8 == 0x7F { VERSION_TLS13 } else { version };

		if (real_version >> 8) == 0x7F {
			debug!(HANDSHAKE_EVENTS debug, "Client supports TLS 1.3-draft{}", real_version & 0xFF);
		} else {
			debug!(HANDSHAKE_EVENTS debug, "Client supports TLS 1.{}",
				version.wrapping_sub(0x301));
		}

		let is_tls13 = !real_version_c.is_tls12();
		//Random.
		debug!(CRYPTO_CALCS debug, "Received client random:\n{}", bytes_as_hex_block(&pmsg.random, ">"));
		//Ciphersuites.
		for cs in pmsg.ciphersuites.iter() {
			if prefer_chacha.is_none() { prefer_chacha = Some(cs.is_chacha()); }
			ciphersuite_mask |= cs.algo_const();
		}
		debug!(HANDSHAKE_EVENTS debug, "Client supports ciphersuites: {} (Cacha preferred \
			{})", PrintBitfieldFlags(ciphersuite_mask, &CIPHERSUITE_STRINGS[..]), prefer_chacha.
			unwrap_or(false));

		//Fallback hack.
		if pmsg.fallback_hack {
			let maxversion = if (config.flags[0] & FLAGS0_ENABLE_TLS13) != 0 {
				4
			} else if (config.flags[0] & FLAGS0_ENABLE_TLS12) != 0 {
				3
			} else {
				0
			};
			fail_if!((version as u8) < maxversion, TlsFailure::DowngradeAttackDetected);
		}

		//Key share extension.
		let keyshare = if is_tls13 { pmsg.key_shares } else { None };
		//ALPN extension.
		if let Some(alpn_list) = pmsg.alpn_list {
			if let &mut Some(ref mut func) = &mut config.alpn_function {
				match func.select_alpn(pmsg.host_name, alpn_list)? {
					Some(index) => {
						let entry = match alpn_list.iter().nth(index) {
							Some(x) => x,
							None => fail!(TlsFailure::AlpnNoOverlap),
						};
						debug!(HANDSHAKE_EVENTS debug,  "Using Application Layer \
							Protocol '{}'", entry);
						names.set_alpn(entry.to_owned());
					},
					None => (),	//Don't select.
				}
			}
			//This has to not be else block because of borrow checking.
			if config.alpn_function.is_none() {
				handle_alpn(alpn_list.iter(), names, debug.clone(), config)?;
			}
		}

		let certificate_type_flags = pmsg.certtypes;
		fail_if!(certificate_type_flags == 0,  TlsFailure::NoMutualCertificateType);
		if certificate_type_flags != 1 {
			debug!(HANDSHAKE_EVENTS debug, "Client supports certificate types: {}",
				PrintBitfieldFlags(certificate_type_flags as u64, &CERTTYPE_STRINGS[..]));
			send_spki_only = certificate_type_flags & 2 != 0;
		}

		//Supported groups extension
		if let Some(sgrp_mask) = pmsg.supported_groups_mask {
			recvd_supported_groups = true;
			group_mask = sgrp_mask;
			debug!(HANDSHAKE_EVENTS debug, "Client supports groups: {}",
				PrintBitfieldFlags(sgrp_mask, &DHGROUP_STRINGS[..]));
		}

		//Signature algorithms extension.
		if let Some(sigalg_mask) = pmsg.signature_algorithms_mask {
			recvd_signature_algorithms = true;
			sigalgo_mask = sigalg_mask;
			debug!(HANDSHAKE_EVENTS debug, "Client supports signature algorithms: {}",
				PrintBitfieldFlags(sigalg_mask, &SIGALGO_STRINGS[..]));
		}

		//Server name extension.
		if let Some(sni) = pmsg.host_name {
			//Fetch the ACME data if any.
			use_acme_challenge = get_acme_challenge1(sni, config).or_else(||get_acme_challenge2(sni,
				config));
			//Don't allow using ACME names in regular handshakes.
			fail_if!(use_acme_challenge.is_none() && sni.ends_with(".acme.invalid"),
				TlsFailure::AcmeReservedName(sni.to_owned()));
			debug!(HANDSHAKE_EVENTS debug, "Client requests vserver '{}'", sni);
			let sni = sni.to_owned();
			names.set_sni(sni.clone());
			sni_name = Some(sni);
		}

		//Status request extension.
		let ocsp_v1_offered = pmsg.ocsp_offered;
		if ocsp_v1_offered {
			debug!(HANDSHAKE_EVENTS debug, "Client supports OCSP stapling");
		}

		//Signed Certificate Timestamp extension.
		if pmsg.sct_offered {
			debug!(HANDSHAKE_EVENTS debug, "Client supports CT stapling");
			ct_offered = true;
		}

		//Renegotiation info extension.
		let renego_fixed = is_tls13 || pmsg.renego_ok;

		//Extended master secret extension.
		let ths_fixed = is_tls13 || pmsg.ems_offered;
		if pmsg.ems_offered {
			debug!(HANDSHAKE_EVENTS debug, "Client supports Extended Master Secret");
			ems_offered = true;
		}
		//Maximum fragment size extension.
		if let Some(maxfragsize) = pmsg.record_size_limit {
			let maxsize = maxfragsize as usize;
			reduced_record_size = maxsize < 16384;
			debug!(HANDSHAKE_EVENTS debug, "Client supports fragment size {}", maxsize);
			_controller.set_max_fragment_length(maxsize);
		}

		names.sni_known();
		fail_if!(!recvd_supported_groups, TlsFailure::NoSupportedGroups);
		fail_if!(!recvd_signature_algorithms, TlsFailure::NoSignatureAlgorithms);
		fail_if!(keyshare.is_none() && version >= 0x0304, TlsFailure::NoKeyshareInTls13);

		let ret = negotiate_crypto(NegotiateCryptoParameters{ciphersuite_mask: ciphersuite_mask,
			prefer_chacha: prefer_chacha.unwrap_or(true), sigalgo_mask: sigalgo_mask, group_mask:
			group_mask, keyshare: keyshare, version: real_version_c, use_acme_challenge:
			use_acme_challenge, sni_name: &sni_name, client_accepts_spki: send_spki_only}, debug.clone(),
			config, crypto_algs)?;
		fail_if!(!ths_fixed && (config.flags[0] & FLAGS0_REQUIRE_EMS) != 0, TlsFailure::VulernableTHS);
		fail_if!(!renego_fixed, TlsFailure::VulernableRenego);
		debug!(HANDSHAKE_EVENTS debug, "Chosen configuration: Version: {}, Protection:{:?}, \
			Hash: {:?}, Exchange group: {:?}, Extended Master Secret: {}, Send SPKI only: {}",
			real_version_c, ret.tls12_partial_cipher.0, ret.tls12_partial_cipher.1,
			ret.group, if is_tls13 { "N/A" } else if ems_offered { "Yes" } else { "No" },
			if send_spki_only { "Yes" } else { "No" });

		crypto_algs.set_kex_group(ret.group);
		//The ciphersuite is only available here for TLS 1.3.
		if is_tls13 {
			debug!(HANDSHAKE_EVENTS debug, "Chosen ciphersuite is {:?}", ret.
				chosen_ciphersuite);
			crypto_algs.set_ciphersuite(ret.chosen_ciphersuite);
		} else {
			crypto_algs.set_protection_prf(ret.tls12_partial_cipher.0, ret.tls12_partial_cipher.1);
		}
		let mut hs_hash = HandshakeHash::new(ret.tls12_partial_cipher.1, debug.clone());
		hs_hash.add(raw_msg, debug.clone())?;

		let need_restart = is_tls13 && ret.dh_secret.is_none();
		if need_restart && pmsg.tls13_variant.unwrap_or(0) >= 19 {
			//TLS 1.3-draft19 and forwards compress the first CH on restart.
			hs_hash.compress(debug)?;
		}

		let inner = ServerHelloFields{
			dh_secret: ret.dh_secret,
			no_check_certificate: ret.no_check_certificate,
			is_acme_challenge: ret.is_acme_challenge,
			key_share: ret.key_share,
			chosen_ciphersuite: ret.chosen_ciphersuite,
			tls12_partial_cipher: ret.tls12_partial_cipher,
			ciphersuite_mask: ciphersuite_mask,
			certlookup_future: ret.certificate,
			real_version: real_version_c,
			ct_offered: ct_offered,
			ems_offered: ems_offered,
			reduced_record_size: reduced_record_size,
			ocsp_v1_offered: ocsp_v1_offered,
			sni_name: sni_name,
			send_spki_only: send_spki_only,
			using_tls13: is_tls13,
			kill_connection_after_handshake: ret.kill_connection_after_handshake,
			client_random: pmsg.random,
			hs_hash: hs_hash,
			peer_sigalgo_mask: sigalgo_mask,
		};
		Ok(if !need_restart {
			HandshakeState2::ServerHello(inner)
		} else {
			HandshakeState2::HelloRestartRequest(inner)
		})
	}
}
