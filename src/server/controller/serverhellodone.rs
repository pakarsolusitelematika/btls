use super::HandshakeState2;
use ::common::{Debugger, emit_handshake, HandshakeHash, HSTYPE_SERVER_HELLO_DONE, TlsFailure};
use ::record::SessionBase;
use ::server::controller::clientkeyexchange::Tls12ClientKeyExchangeFields;
use ::stdlib::{Box, Vec};

use btls_aux_aead::ProtectorType;
use btls_aux_dhf::DiffieHellmanKey;
use btls_aux_hash::HashFunction;

pub struct Tls12ServerHelloDoneFields
{
	pub key_share: Box<DiffieHellmanKey+Send>,
	pub kill_connection_after_handshake: bool,
	pub client_random: [u8; 32],
	pub server_random: [u8; 32],
	pub hs_hash: HandshakeHash,
	pub ems_enabled: bool,
	pub protection: ProtectorType,
	pub prf: HashFunction,
}

impl Tls12ServerHelloDoneFields
{
	pub fn tx_server_hello_done(self, base: &mut SessionBase, debug: Debugger, state: &str) ->
		Result<HandshakeState2, TlsFailure>
	{
		let mut hs_hash = self.hs_hash;
		//Yup, this message is empty.
		let content = Vec::new();
		emit_handshake(debug, HSTYPE_SERVER_HELLO_DONE, &content, base, &mut hs_hash, state)?;
		Ok(HandshakeState2::Tls12ClientKeyExchange(Tls12ClientKeyExchangeFields{
			key_share: self.key_share,
			kill_connection_after_handshake: self.kill_connection_after_handshake,
			client_random: self.client_random,
			server_random: self.server_random,
			hs_hash: hs_hash,
			ems_enabled: self.ems_enabled,
			protection: self.protection,
			prf: self.prf,
		}))
	}
}
