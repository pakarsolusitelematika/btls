use super::{ClientCertificateInfo, ServerConfiguration};
use ::TlsConnectionInfo;
use ::common::{Ciphersuite, CryptoTracker, Debugger, EXT_ALPN, EXT_CT, HandshakeMessage, HSTYPE_CERTIFICATE,
	HSTYPE_CERTIFICATE_VERIFY, HSTYPE_CLIENT_HELLO, HSTYPE_CLIENT_KEY_EXCHANGE, HSTYPE_FINISHED,
	HSTYPE_KEY_UPDATE, PROTO_CCS, PROTO_HANDSHAKE, KeyExchangeType, NamesContainer, TLS_LEN_ALPN_ENTRY,
	TLS_LEN_ALPN_LIST, TLS_LEN_SCT_ENTRY, TLS_LEN_SCT_LIST, TlsFailure, write_ext_fn, write_sub_fn};
use ::logging::{bytes_as_hex_block, explain_hs_type, Logging};
use ::messages::{KeyShareList, Tls13Certificate, TlsCertificateVerify, TlsClientHello, TlsClientKeyExchange,
	TlsKeyUpdate};
use ::record::{DiffieHellmanSharedSecret, Tls13MasterSecret, SessionBase, SessionController, Tls13TrafficSecretIn,
	Tls13TrafficSecretOut};
use ::server_certificates::CertificateSigner;
use ::stdlib::{Arc, Box, String, swap, Vec};
use ::utils::HsBuffer;

use btls_aux_aead::ProtectorType;
use btls_aux_dhf::{Dhf, DiffieHellmanKey};
use btls_aux_futures::FutureReceiver;
use btls_aux_hash::HashFunction;
use btls_aux_serialization::Sink;

macro_rules! try_buffer
{
	($exp:expr) => { $exp.map_err(|_|assert_failure!("TX serialization buffer overflow"))? }
}

mod certificate;
use self::certificate::{Tls12CertificateFields, Tls12ServerKeyExchangeFields, Tls13CertificateFields,
	Tls13CertificateRequestFields, Tls13CertificateVerifyFields, Tls13ClientCertificateFields,
	Tls13ClientCertificateVerifyFields};
mod clienthello;
use self::clienthello::ClientHelloFields;
mod clientkeyexchange;
use self::clientkeyexchange::Tls12ClientKeyExchangeFields;
mod encryptedextensions;
use self::encryptedextensions::Tls13EncryptedExtensionsFields;
mod finished;
use self::finished::{Tls12ClientChangeCipherSpecFields, Tls12ClientFinishedFields, Tls12ServerFinishedFields,
	Tls13ClientFinishedFields, Tls13ServerFinishedFields};
mod serverhello;
use self::serverhello::ServerHelloFields;
mod serverhellodone;
use self::serverhellodone::Tls12ServerHelloDoneFields;

pub struct Tls13HsSecrets
{
	server_hs_write: Tls13TrafficSecretOut,
	client_hs_write: Tls13TrafficSecretIn,
	master_secret: Tls13MasterSecret,
}

pub struct Tls13C2ndFSecrets
{
	server_app_write: Tls13TrafficSecretOut,
	client_app_write: Tls13TrafficSecretIn,
	client_hs_write: Tls13TrafficSecretIn,
}

pub struct Tls12ShowtimeFields
{
	kill_connection_after_handshake: bool,
}

pub struct Tls13ShowtimeFields
{
	server_app_write: Tls13TrafficSecretOut,
	client_app_write: Tls13TrafficSecretIn,
	rekey_in_progress: bool,
	kill_connection_after_handshake: bool,
}

pub enum HandshakeState2
{
	ClientHello(ClientHelloFields),
	HelloRestartRequest(ServerHelloFields),		//HelloRestartRequest uses ServerHello fields.
	RetryClientHello(ServerHelloFields),		//RetryClientHello uses ServerHello fields.
	ServerHello(ServerHelloFields),
	Tls12Certificate(Tls12CertificateFields),
	Tls12ServerKeyExchange(Tls12ServerKeyExchangeFields),
	Tls12ServerHelloDone(Tls12ServerHelloDoneFields),
	Tls12ClientKeyExchange(Tls12ClientKeyExchangeFields),
	Tls12ClientChangeCipherSpec(Tls12ClientChangeCipherSpecFields),
	Tls12ClientFinished(Tls12ClientFinishedFields),
	Tls12ServerFinished(Tls12ServerFinishedFields),
	Tls12Showtime(Tls12ShowtimeFields),
	Tls13EncryptedExtensions(Tls13EncryptedExtensionsFields),
	Tls13CertificateRequest(Tls13CertificateRequestFields),
	Tls13Certificate(Tls13CertificateFields),
	Tls13CertificateVerify(Tls13CertificateVerifyFields),
	Tls13ServerFinished(Tls13ServerFinishedFields),
	Tls13ClientCertificate(Tls13ClientCertificateFields),
	Tls13ClientCertificateVerify(Tls13ClientCertificateVerifyFields),
	Tls13ClientFinished(Tls13ClientFinishedFields),
	Tls13Showtime(Tls13ShowtimeFields),
	Error,
}

impl HandshakeState2
{
	fn name(&self) -> &'static str
	{
		match self {
			&HandshakeState2::ClientHello(_) => "ClientHello",
			&HandshakeState2::HelloRestartRequest(_) => "HelloRestartRequest",
			&HandshakeState2::RetryClientHello(_) => "RetryClientHello",
			&HandshakeState2::ServerHello(_) => "ServerHello",
			&HandshakeState2::Tls12Certificate(_) => "Tls12Certificate",
			&HandshakeState2::Tls12ServerKeyExchange(_) => "Tls12ServerKeyExchange",
			&HandshakeState2::Tls12ServerHelloDone(_) => "Tls12ServerHelloDone",
			&HandshakeState2::Tls12ClientKeyExchange(_) => "Tls12ClientKeyExchange",
			&HandshakeState2::Tls12ClientChangeCipherSpec(_) => "Tls12ClientChangeCipherSpec",
			&HandshakeState2::Tls12ClientFinished(_) => "Tls12ClientFinished",
			&HandshakeState2::Tls12ServerFinished(_) => "Tls12ServerFinished",
			&HandshakeState2::Tls12Showtime(_) => "Tls12Showtime",
			&HandshakeState2::Tls13EncryptedExtensions(_) => "Tls13EncryptedExtensions",
			&HandshakeState2::Tls13CertificateRequest(_) => "Tls13CertificateRequest",
			&HandshakeState2::Tls13Certificate(_) => "Tls13Certificate",
			&HandshakeState2::Tls13CertificateVerify(_) => "Tls13CertificateVerify",
			&HandshakeState2::Tls13ServerFinished(_) => "Tls13ServerFinished",
			&HandshakeState2::Tls13ClientCertificate(_) => "Tls13ClientCertificate",
			&HandshakeState2::Tls13ClientCertificateVerify(_) => "Tls13ClientCertificateVerify",
			&HandshakeState2::Tls13ClientFinished(_) => "Tls13ClientFinished",
			&HandshakeState2::Tls13Showtime(_) => "Tls13Showtime",
			&HandshakeState2::Error => "Error",
		}
	}
}

pub struct HandshakeController
{
	config: ServerConfiguration,
	hs_state: HandshakeState2,
	crypto_algs: CryptoTracker,
	names: NamesContainer,
	hs_buffer: HsBuffer,
	debug: Option<(u64, Arc<Box<Logging+Send+Sync>>)>,
}

fn _illegal_state() -> Result<TlsFailure, TlsFailure>
{
	sanity_failed!("Attempt to TX in illegal state!");
}

fn illegal_state() -> TlsFailure
{
	match _illegal_state() { Ok(x) => x, Err(x) => x }
}

fn do_refused_client_auth(config: &ServerConfiguration) -> Result<(), TlsFailure>
{
	if let &Some(ref cc_cb) = &config.ask_client_cert {
		let mut cc_cb = cc_cb.clone_self();
		cc_cb.on_certificate_auth(ClientCertificateInfo::new_noauth()).map_err(|_|
			TlsFailure::TlsClientAuthNotOk)?;
	}
	Ok(())
}

impl HandshakeController
{
	pub fn new(config: &ServerConfiguration, debug: Option<(u64, Arc<Box<Logging+Send+Sync>>)>) ->
		HandshakeController
	{
		HandshakeController{
			config: config.clone(),
			hs_state: HandshakeState2::ClientHello(ClientHelloFields{
			}),
			crypto_algs: CryptoTracker::new(),
			names: NamesContainer::new(),
			hs_buffer: HsBuffer::new(),
			debug: debug.clone(),
		}
	}
	pub fn wants_tx(&self) -> bool
	{
		//If certlookup_future is set, but not settled yet, we wait. But only in ServerHello state.
		if let &HandshakeState2::ServerHello(ref x) = &self.hs_state {
			if !x.certlookup_future.settled() { return false; }
		}
		//Also, if signature_future is set, but not settled yet, we wait.
		if let &HandshakeState2::Tls12ServerKeyExchange(ref x) = &self.hs_state {
			if !x.signature_future.settled() { return false; }
		}
		if let &HandshakeState2::Tls13CertificateVerify(ref x) = &self.hs_state {
			if !x.signature_future.settled() { return false; }
		}
		match &self.hs_state {
			&HandshakeState2::ClientHello(_) => false,
			&HandshakeState2::HelloRestartRequest(_) => true,
			&HandshakeState2::RetryClientHello(_) => false,
			&HandshakeState2::ServerHello(_) => true,		//Assume ready.
			&HandshakeState2::Tls12Certificate(_) => true,
			&HandshakeState2::Tls12ServerKeyExchange(_) => true,	//Assume ready.
			&HandshakeState2::Tls12ServerHelloDone(_) => true,
			&HandshakeState2::Tls12ClientKeyExchange(_) => false,
			&HandshakeState2::Tls12ClientChangeCipherSpec(_) => false,
			&HandshakeState2::Tls12ClientFinished(_) => false,
			&HandshakeState2::Tls12ServerFinished(_) => true,
			&HandshakeState2::Tls12Showtime(_) => false,
			&HandshakeState2::Tls13EncryptedExtensions(_) => true,
			&HandshakeState2::Tls13CertificateRequest(_) => true,
			&HandshakeState2::Tls13Certificate(_) => true,
			&HandshakeState2::Tls13CertificateVerify(_) => true,	//Assume ready.
			&HandshakeState2::Tls13ServerFinished(_) => true,
			&HandshakeState2::Tls13ClientCertificate(_) => false,
			&HandshakeState2::Tls13ClientCertificateVerify(_) => false,
			&HandshakeState2::Tls13ClientFinished(_) => false,
			&HandshakeState2::Tls13Showtime(_) => false,
			&HandshakeState2::Error => false,
		}
	}
	pub fn wants_rx(&self) -> bool
	{
		match &self.hs_state {
			&HandshakeState2::ClientHello(_) => true,
			&HandshakeState2::HelloRestartRequest(_) => false,
			&HandshakeState2::RetryClientHello(_) => true,
			&HandshakeState2::ServerHello(_) => false,
			&HandshakeState2::Tls12Certificate(_) => false,
			&HandshakeState2::Tls12ServerKeyExchange(_) => false,
			&HandshakeState2::Tls12ServerHelloDone(_) => false,
			&HandshakeState2::Tls12ClientKeyExchange(_) => true,
			&HandshakeState2::Tls12ClientChangeCipherSpec(_) => true,
			&HandshakeState2::Tls12ClientFinished(_) => true,
			&HandshakeState2::Tls12ServerFinished(_) => false,
			&HandshakeState2::Tls12Showtime(_) => false,
			&HandshakeState2::Tls13EncryptedExtensions(_) => false,
			&HandshakeState2::Tls13CertificateRequest(_) => false,
			&HandshakeState2::Tls13Certificate(_) => false,
			&HandshakeState2::Tls13CertificateVerify(_) => false,
			&HandshakeState2::Tls13ServerFinished(_) => false,
			&HandshakeState2::Tls13ClientCertificate(_) => true,
			&HandshakeState2::Tls13ClientCertificateVerify(_) => true,
			&HandshakeState2::Tls13ClientFinished(_) => true,
			&HandshakeState2::Tls13Showtime(_) => false,
			&HandshakeState2::Error => false,
		}
	}
	pub fn in_showtime(&self) -> bool
	{
		if self.kill_connection_after_handshake() { return false; }
		self._in_showtime()
	}
	pub fn ignore_handshake_timeout(&self) -> bool
	{
		//Ignore handshake timeouts on connections in showtime. This also applies to internal connections.
		self._in_showtime()
	}
	fn _in_showtime(&self) -> bool
	{
		match &self.hs_state {
			&HandshakeState2::Tls12Showtime(_) => true,
			&HandshakeState2::Tls13Showtime(_) => true,
			_ => false
		}
	}
	pub fn queue_hs_tx(&mut self, base: &mut SessionBase) -> bool
	{
		let ret = match &self.hs_state {
			&HandshakeState2::Tls12ServerFinished(_) => true,
			&HandshakeState2::Tls13ServerFinished(_) => true,
			_ => false
		};
		let mut success = false;
		let debug = self.debug.clone();
		let sname = self.hs_state.name();
		let mut tmp_state = HandshakeState2::Error;
		swap(&mut tmp_state, &mut self.hs_state);
		match match tmp_state {
			HandshakeState2::HelloRestartRequest(x) => x.tx_hello_restart_request(base, debug.clone(),
				sname),
			HandshakeState2::ServerHello(x) => x.tx_server_hello(base, &mut self.names, debug.clone(),
				&self.config, sname, &mut self.crypto_algs),
			HandshakeState2::Tls12Certificate(x) => x.tx_certificate(base, &mut self.names, debug.
				clone(), &self.config, sname, &mut self.crypto_algs),
			HandshakeState2::Tls12ServerKeyExchange(x) => x.tx_server_key_exchange(base, debug.clone(),
				sname, &mut self.crypto_algs),
			HandshakeState2::Tls12ServerHelloDone(x) => x.tx_server_hello_done(base, debug.clone(),
				sname),
			HandshakeState2::Tls12ServerFinished(x) => x.tx_finished(base, debug.clone(), sname),
			HandshakeState2::Tls13EncryptedExtensions(x) => x.tx_encrypted_extensions(base,
				&mut self.names, debug.clone(), sname, &self.config),
			HandshakeState2::Tls13CertificateRequest(x) => x.tx_certificate_request(base,
				debug.clone(), sname),
			HandshakeState2::Tls13Certificate(x) => x.tx_certificate(base, &mut self.names, debug.
				clone(), &self.config, sname, &mut self.crypto_algs),
			HandshakeState2::Tls13CertificateVerify(x) => x.tx_certificate_verify(base, debug.clone(),
				sname, &mut self.crypto_algs),
			HandshakeState2::Tls13ServerFinished(x) => x.tx_finished(base, debug.clone(), sname),
			_ => Err(illegal_state())
		} {
			Ok(y) => { self.hs_state = y; success = true; }
			Err(x) => base.abort_handshake(x)
		};
		if !success {
			//Ensure no more messages get generated.
			self.hs_state = HandshakeState2::Error;
			return false;
		}

		//If in ACME challenge mode, shut down the connection, it has served its purpose.
		if self.kill_connection_after_handshake() && self._in_showtime() {
			debug!(HANDSHAKE_EVENTS debug, "Validation handshake done, closing connection");
			base.abort_handshake(TlsFailure::AcmeValidationDone);
			return false;
		}
		ret
	}
	pub fn get_alpn(&self) -> Option<Option<Arc<String>>>
	{
		self.names.get_alpn()
	}
	pub fn get_sni(&self) -> Option<Option<Arc<String>>>
	{
		self.names.get_sni()
	}
	pub fn get_server_names(&self) -> Option<Arc<Vec<String>>>
	{
		self.names.get_server_names()
	}
	fn kill_connection_after_handshake(&self) -> bool
	{
		match &self.hs_state {
			&HandshakeState2::Tls12Showtime(ref x) => x.kill_connection_after_handshake,
			&HandshakeState2::Tls13Showtime(ref x) => x.kill_connection_after_handshake,
			_ => false
		}
	}
	fn handle_hs_message(&mut self, controller: &mut SessionController, htype: u8, data: &[u8], last: bool) ->
		Result<(), TlsFailure>
	{
		let debug = self.debug.clone();
		if debug.clone().map(|x|x.0).unwrap_or(0) & debug_class!(HANDSHAKE_DATA) == 0 {
			//DEBUG_HANDSHAKE_DATA has more detailed message, so don't print this here.
			debug!(HANDSHAKE_MSGS debug, "RX Handshake msg {} in state {}", explain_hs_type(htype),
				self.hs_state.name());
		}
		let raw_msg = HandshakeMessage::new(htype, data);
		let mut tmp_state = HandshakeState2::Error;
		swap(&mut tmp_state, &mut self.hs_state);
		self.hs_state = match (tmp_state, htype) {
			(HandshakeState2::ClientHello(x), HSTYPE_CLIENT_HELLO) => {
				//ClientHello.
				x.rx_client_hello(controller, TlsClientHello::parse(data, debug.clone())?,
					&mut self.names, debug.clone(), &mut self.config, raw_msg,
					&mut self.crypto_algs)?
			},
			(HandshakeState2::RetryClientHello(x), HSTYPE_CLIENT_HELLO) => {
				//ClientHello again.
				x.rx_client_hello_again(controller, TlsClientHello::parse(data, debug.clone())?,
					debug.clone(), raw_msg)?
			},
			(HandshakeState2::Tls12ClientKeyExchange(x), HSTYPE_CLIENT_KEY_EXCHANGE) => {
				//ClientKeyExchange
				x.rx_client_key_exchange(controller, TlsClientKeyExchange::parse(data)?,
					debug.clone(), raw_msg)?
			},
			(HandshakeState2::Tls12ClientFinished(x), HSTYPE_FINISHED) => {
				//Finished
				fail_if!(!last, TlsFailure::FinishedNotAligned);
				x.rx_finished(data, controller, debug.clone(), raw_msg, &self.config)?
			},
			(HandshakeState2::Tls13ClientCertificate(x), HSTYPE_CERTIFICATE) => {
				x.rx_certificate(controller, Tls13Certificate::parse(data, debug.clone(), true)?, 
					debug.clone(), raw_msg, &self.config)?
			},
			(HandshakeState2::Tls13ClientCertificateVerify(x), HSTYPE_CERTIFICATE_VERIFY) => {
				x.rx_certificate_verify(controller,TlsCertificateVerify::parse(data)?,
					debug.clone(), raw_msg)?
			},
			(HandshakeState2::Tls13ClientFinished(x), HSTYPE_FINISHED) => {
				//Finished
				fail_if!(!last, TlsFailure::FinishedNotAligned);
				x.rx_finished(data, controller, debug.clone(), raw_msg, &self.config)?
			},
			(HandshakeState2::Tls13Showtime(mut x), HSTYPE_KEY_UPDATE) => {
				//KeyUpdate
				fail_if!(!last, TlsFailure::KeyUpdateNotAligned);
				let kmsg = TlsKeyUpdate::parse(data)?;
				x.client_app_write.rachet(debug.clone())?;
				controller.set_deprotector(x.client_app_write.to_deprotector(debug.clone())?);
				debug!(HANDSHAKE_EVENTS debug, "Downstream encryption \
					keys changed by rekey message");
				if !controller.last_tx_key_update() && !x.rekey_in_progress && kmsg.requested {
					//These have to be in this order for the handshake to go out with old keys.
					controller.queue_handshake(HSTYPE_KEY_UPDATE, &[0], "Tls13Showtime");
					x.server_app_write.rachet(debug.clone())?;
					controller.set_protector(x.server_app_write.to_protector(debug.clone())?);
					debug!(HANDSHAKE_EVENTS debug, "Upstream encryption keys changed by \
						rekey-in-kind");
				}
				x.rekey_in_progress = false;
				HandshakeState2::Tls13Showtime(x)
			}
			(state, htype2) => fail!(TlsFailure::UnexpectedHandshakeMessage(htype2, state.name()))
		};
		if htype == HSTYPE_FINISHED {
			//Receiving data is OK.
			controller.assert_hs_rx_finished();
		}
		//If in ACME challenge mode, shut down the connection, it has served its purpose.
		if self.kill_connection_after_handshake() && self._in_showtime() {
			debug!(HANDSHAKE_EVENTS debug, "Validation handshake done, closing \
				connection");
			fail!(TlsFailure::AcmeValidationDone);
		}
		Ok(())
	}
	pub fn handle_control(&mut self, rtype: u8, msg: &[u8], controller: &mut SessionController)
	{
		match self._handle_control(rtype, msg, controller) {
			Ok(_) => (),
			Err(x) => {
				//Ensure no more messages get generated.
				self.hs_state = HandshakeState2::Error;
				controller.abort_handshake(x);
			}
		}
	}
	pub fn _handle_tx_update(&mut self, controller: &mut SessionController) -> Result<(), TlsFailure>
	{
		let debug = self.debug.clone();
		//These have to be in this order for the handshake to go out with old keys.
		if let &mut HandshakeState2::Tls13Showtime(ref mut x) = &mut self.hs_state {
			controller.queue_handshake(HSTYPE_KEY_UPDATE, &[1], "Tls13Showtime");
			x.server_app_write.rachet(debug.clone())?;
			controller.set_protector(x.server_app_write.to_protector(debug.clone())?);
			x.rekey_in_progress = true;
			debug!(HANDSHAKE_EVENTS debug, "Upstream encryption keys changed by \
				spontaneous rekey");
		}
		Ok(())
	}
	pub fn handle_tx_update(&mut self, controller: &mut SessionController)
	{
		match self._handle_tx_update(controller) {
			Ok(_) => (),
			Err(x) => controller.abort_handshake(x)
		}
	}
	pub fn callback_with_certlookup_future<F>(&mut self, sbase: &mut SessionBase, mut cb: F)
		where F: FnMut(&mut SessionBase, &mut FutureReceiver<Result<Box<CertificateSigner+Send>, ()>>)
	{
		if let &mut HandshakeState2::ServerHello(ref mut x) = &mut self.hs_state {
			cb(sbase, &mut x.certlookup_future);
		}
	}
	pub fn callback_with_signature_future<F>(&mut self, sbase: &mut SessionBase, mut cb: F)
		where F: FnMut(&mut SessionBase, &mut FutureReceiver<Result<(u16, Vec<u8>),()>>)
	{
		if let &mut HandshakeState2::Tls12ServerKeyExchange(ref mut x) = &mut self.hs_state {
			cb(sbase, &mut x.signature_future);
		}
		if let &mut HandshakeState2::Tls13CertificateVerify(ref mut x) = &mut self.hs_state {
			cb(sbase, &mut x.signature_future);
		}
	}
	//Note, the handshake buffer MUST be idle.
	fn _handle_control_hs_borowed(&mut self, msg: &[u8], controller: &mut SessionController) ->
		Result<(), TlsFailure>
	{
		let debug = self.debug.clone();
		let mut _msg = msg;
		loop {
			if _msg.len() < 4 { break; }	//No full header.
			//4 bytes at least.
			let hlen = (_msg[1] as usize) * 65536 + (_msg[2] as usize) * 256 +
				(_msg[3] as usize);
			//Check if enough space (hlen is at most 16MB, well in bounds).
			//Also, hlen + 4 >= 4.
			if _msg.len() < hlen + 4 { break; } //No full record.
			let (hmsg, trailer) = _msg.split_at(hlen + 4);
			_msg = trailer;
			let last = _msg.len() == 0;
			//hmsg is at least 4 bytes, and has 4 byte header.
			debug!(HANDSHAKE_DATA debug, "[State {}] Received (borrowed) {} (length {}):\n{}",
				self.hs_state.name(), explain_hs_type(hmsg[0]), hmsg.len().saturating_sub(4),
				bytes_as_hex_block(&hmsg[4..], ">"));
			self.handle_hs_message(controller, hmsg[0], &hmsg[4..], last)?;
		}
		if _msg.len() > 0 {
			//Client Hello fragmentation is great way to do DoS attacks.
			fail_if!(_msg[0] == HSTYPE_CLIENT_HELLO, TlsFailure::ChIncomplete);
			match self.hs_buffer.extract(&mut _msg) {
				Ok(None) => (),
				Ok(Some(_)) => sanity_failed!("No handshake messages but handshake \
					buffer extracted one"),
				Err(err) => fail!(TlsFailure::HsMsgBuffer(err))
			}
		}
		return Ok(());
	}
	fn _handle_control(&mut self, rtype: u8, msg: &[u8], controller: &mut SessionController) ->
		Result<(), TlsFailure>
	{
		let debug = self.debug.clone();
		if rtype == PROTO_HANDSHAKE {
			fail_if!(msg.len() == 0, TlsFailure::EmptyHandshakeMessage);
			if self.hs_buffer.is_idle() {
				return self._handle_control_hs_borowed(msg, controller);
			}
			let mut _msg = msg;
			loop {
				match self.hs_buffer.extract(&mut _msg) {
					Ok(None) => break,
					Ok(Some((htype, payload, last))) => {
						debug!(HANDSHAKE_DATA debug, "[State {}] Received {} (length {}):\
							\n{}", self.hs_state.name(), explain_hs_type(htype),
							payload.len(), bytes_as_hex_block(&payload, ">"));
						self.handle_hs_message(controller, htype, &payload, last)?;
					},
					Err(err) => fail!(TlsFailure::HsMsgBuffer(err))
				};
			}
		} else if rtype == PROTO_CCS {
			//ChangeCipherSpec.
			debug!(HANDSHAKE_DATA debug, "[State {}] Received change cipher spec:\n{}",
				self.hs_state.name(), bytes_as_hex_block(msg, ">"));
			fail_if!(!self.hs_buffer.empty(), TlsFailure::CcsInMiddleOfHandshakeMessage);
			//The || short-circuits, so the array access has to have msg.len()=1 => in bounds.
			fail_if!(msg.len() != 1 || msg[0] != 1, TlsFailure::InvalidCcs);
			let mut tmpstate = HandshakeState2::Error;
			swap(&mut tmpstate, &mut self.hs_state);
			self.hs_state = match tmpstate {
				HandshakeState2::Tls12ClientChangeCipherSpec(x) => x.rx_change_cipher_spec(
					controller, debug.clone())?,
				x => fail!(TlsFailure::UnexpectedCcs(x.name()))
			};
		} else {
			fail!(TlsFailure::UnexpectedRtype(rtype));
		}
		Ok(())
	}
	pub fn connection_info(&self) -> TlsConnectionInfo
	{
		self.crypto_algs.connection_info()
	}
}

const VERSION_TLS12: u16 = 0x0303;
const VERSION_TLS13: u16 = 0x0304;

//Find a ciphersuite matching criteria.
fn find_matching_ciphersuite(kex: Option<KeyExchangeType>, protection: Option<ProtectorType>,
	prf: Option<HashFunction>, ciphersuite_mask: u64) -> Result<Ciphersuite, TlsFailure>
{
	let mut chosen_ciphersuite = None;
	for i in Ciphersuite::all_suites().iter() {
		if ciphersuite_mask & i.algo_const() != 0 &&
			(kex.is_none() || kex == Some(i.get_key_exchange())) &&
			(protection.is_none() || protection == Some(i.get_protector())) &&
			(prf.is_none() || prf == Some(i.get_prf())) {
			chosen_ciphersuite = Some(*i);
		}
	}
	chosen_ciphersuite.ok_or(TlsFailure::NoMutualCiphersuites)
}

struct SelectProtectionParameters
{
	ciphersuite_mask: u64,
	prefer_chacha: bool,
	tls13: bool,
	hw_aes: bool,
}

//Select a protection.
fn select_protection(params: SelectProtectionParameters) -> Result<(ProtectorType, HashFunction), TlsFailure>
{
	let mut ciphers = 0;
	for i in Ciphersuite::all_suites().iter() {
		let is_tls13 = i.get_key_exchange() == KeyExchangeType::Tls13;
		if is_tls13 != params.tls13 { continue; }
		if params.ciphersuite_mask & i.algo_const() != 0 {
			ciphers |= i.get_protector().algo_const();
		}
	}
	//We pretend client perfers Chacha if it supports it and we don't have AES HW.
	let prefer_chacha = params.prefer_chacha || !params.hw_aes;
	let avail_chacha20poly1305 = ciphers & ProtectorType::Chacha20Poly1305.algo_const() != 0;
	let avail_aes128gcm = ciphers & ProtectorType::Aes128Gcm.algo_const() != 0;
	let avail_aes256gcm = ciphers & ProtectorType::Aes256Gcm.algo_const() != 0;
	Ok(if prefer_chacha && avail_chacha20poly1305 {
		(ProtectorType::Chacha20Poly1305, HashFunction::Sha256)
	} else if avail_aes128gcm {
		(ProtectorType::Aes128Gcm, HashFunction::Sha256)
	} else if avail_chacha20poly1305 {
		(ProtectorType::Chacha20Poly1305, HashFunction::Sha256)
	} else if avail_aes256gcm {
		(ProtectorType::Aes256Gcm, HashFunction::Sha384)
	} else {
		fail!(TlsFailure::NoMutualCiphersuites);
	})
}


fn do_tls13_key_agreement<'a>(keyshare: Option<KeyShareList<'a>>, key_share: &mut Box<DiffieHellmanKey+Send>,
	group: Dhf, debug: Debugger) -> Result<Option<DiffieHellmanSharedSecret>, TlsFailure>
{
	let k = keyshare.ok_or(TlsFailure::NoKeyshareInTls13)?;
	for share in k.iter() {
		//The group (as object) is share.0, the public key is share.1.
		if group.to_tls_id() == share.0.to_tls_id() {
			//Note that we need zero block for PSK.
			let dh_secret = DiffieHellmanSharedSecret::new(key_share, share.1)?;
			debug!(HANDSHAKE_EVENTS debug, "Diffie-Hellman key agreement performed");
			return Ok(Some(dh_secret));
		}
	}
	Ok(None)
}

fn emit_alpn_extension<S:Sink>(x: &mut S, names: &mut NamesContainer) -> Result<(), TlsFailure>
{
	if let Some(Some(alpn)) = names.get_alpn() {
		//Yes, really: 3 nested lengths.
		write_ext_fn(x, EXT_ALPN, "alpn", |x|{
			write_sub_fn(x, "alpn outer", TLS_LEN_ALPN_LIST, |x|{
				write_sub_fn(x, "alpn inner", TLS_LEN_ALPN_ENTRY, |x|{
					try_buffer!(x.write_slice(alpn.as_bytes()));
					Ok(())
				})
			})
		})
	} else {
		Ok(())
	}
}

fn tx_sct_ext_common<S:Sink>(x: &mut S, certificate: &Box<CertificateSigner+Send>,
	debug: Debugger, crypto_algs: &mut CryptoTracker) -> Result<(), TlsFailure>
{
	let sctlist = certificate.get_scts();
	let sctlist = if let Some(ref x) = sctlist.0 { x } else { return Ok(()); };

	sanity_check!(sctlist.len() > 0, "Certificate selection gave empty SCT list");
	debug!(HANDSHAKE_EVENTS debug, "Sending {} CT proofs via extension", sctlist.len());
	crypto_algs.ct_validated();
	write_ext_fn(x, EXT_CT, "certificate_transparency", |x|{
		write_sub_fn(x, "SCT list", TLS_LEN_SCT_LIST, |x|{
			for i in sctlist.iter() {
				write_sub_fn(x, "SCT entry", TLS_LEN_SCT_ENTRY, |x|{
					try_buffer!(x.write_slice(&i.0));
					Ok(())
				})?;
			}
			Ok(())
		})
	})
}
