use super::{emit_alpn_extension, HandshakeState2};
use super::super::ServerConfiguration;
use super::certificate::Tls13CertificateRequestFields;
use super::Tls13HsSecrets;
use ::common::{Debugger, emit_handshake, EXT_RECORD_SIZE_LIMIT, HandshakeHash, HSTYPE_ENCRYPTED_EXTENSIONS,
	NamesContainer, TLS_LEN_EXTENSIONS, TlsFailure, TlsVersion, write_ext_fn, write_sub_fn};
use ::record::SessionBase;
use server::controller::certificate::Tls13CertificateFields;
use ::server_certificates::CertificateSigner;
use ::stdlib::{Box, String, Vec};

use btls_aux_serialization::Sink;

pub struct Tls13EncryptedExtensionsFields
{
	pub hs_secrets: Tls13HsSecrets,
	pub certificate: Box<CertificateSigner+Send>,
	pub send_spki_only: bool,
	pub sni_name: Option<String>,
	pub no_check_certificate: bool,
	pub ocsp_v1_offered: bool,
	pub ct_offered: bool,
	pub reduced_record_size: bool,
	pub kill_connection_after_handshake: bool,
	pub hs_hash: HandshakeHash,
	pub peer_sigalgo_mask: u64,
	pub real_version: TlsVersion,
}

impl Tls13EncryptedExtensionsFields
{
	pub fn tx_encrypted_extensions(self, base: &mut SessionBase, names: &mut NamesContainer, debug: Debugger,
		state: &str, config: &ServerConfiguration) -> Result<HandshakeState2, TlsFailure>
	{
		let mut content = Vec::new();
		write_sub_fn(&mut content, "EncryptedExtensions extensions", TLS_LEN_EXTENSIONS, |x|{
			//Extension ALPN... We give the ALPN if present.
			emit_alpn_extension(x, names)?;
			//Extension record_size_limit, send this if reduced in TLS 1.2.
			if self.reduced_record_size {
				write_ext_fn(x, EXT_RECORD_SIZE_LIMIT, "record_size_limit", |x|{
					//Always 16384.
					try_buffer!(x.write_u16(16384));
					Ok(())
				})?;
			}
			//OCSP, CT and RPK flag are sent in the certificate message.
			Ok(())
		})?;
		let mut hs_hash = self.hs_hash;
		emit_handshake(debug, HSTYPE_ENCRYPTED_EXTENSIONS, &content, base, &mut hs_hash, state)?;
		//Don't send certificate requests for versions older than 19, the format is different.
		if config.ask_client_cert.is_some() && self.real_version.draft_code().at_least_19() {
			Ok(HandshakeState2::Tls13CertificateRequest(Tls13CertificateRequestFields {
				certificate: self.certificate,
				ct_offered: self.ct_offered,
				no_check_certificate: self.no_check_certificate,
				ocsp_v1_offered: self.ocsp_v1_offered,
				send_spki_only: self.send_spki_only,
				sni_name: self.sni_name,
				kill_connection_after_handshake: self.kill_connection_after_handshake,
				hs_secrets: self.hs_secrets,
				hs_hash: hs_hash,
				peer_sigalgo_mask: self.peer_sigalgo_mask,
			}))
		} else {
			Ok(HandshakeState2::Tls13Certificate(Tls13CertificateFields {
				certificate: self.certificate,
				ct_offered: self.ct_offered,
				no_check_certificate: self.no_check_certificate,
				ocsp_v1_offered: self.ocsp_v1_offered,
				send_spki_only: self.send_spki_only,
				sni_name: self.sni_name,
				kill_connection_after_handshake: self.kill_connection_after_handshake,
				hs_secrets: self.hs_secrets,
				hs_hash: hs_hash,
				requested_cc: false,
				peer_sigalgo_mask: self.peer_sigalgo_mask,
			}))
		}
	}
}
