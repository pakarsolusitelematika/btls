use super::{do_refused_client_auth, HandshakeState2, Tls12ShowtimeFields, Tls13C2ndFSecrets, Tls13HsSecrets,
	Tls13ShowtimeFields};
use super::super::{ClientCertificateInfo, ServerConfiguration};
use super::certificate::Tls13ClientCertificateFields;
use ::common::{Debugger, emit_handshake, HandshakeHash, HandshakeMessage, HashMessages, HSTYPE_FINISHED, TlsFailure};
use ::record::{SessionBase, SessionController, Tls12MasterSecret, Tls12PremasterSecret};

pub struct Tls12ClientFinishedFields
{
	pub kill_connection_after_handshake: bool,
	pub master_secret: Tls12MasterSecret,	//The master secret.
	pub hs_hash: HandshakeHash,
}

pub struct Tls12ServerFinishedFields
{
	pub kill_connection_after_handshake: bool,
	pub master_secret: Tls12MasterSecret,	//The master secret.
	pub hs_hash: HandshakeHash,
}

pub struct Tls13ServerFinishedFields
{
	pub hs_secrets: Tls13HsSecrets,
	pub kill_connection_after_handshake: bool,
	pub hs_hash: HandshakeHash,
	pub requested_cc: bool,
}

pub struct Tls13ClientFinishedFields
{
	pub hs_secrets2: Tls13C2ndFSecrets,
	pub kill_connection_after_handshake: bool,
	pub hs_hash: HandshakeHash,
	pub auth_as: ClientCertificateInfo,
}

impl Tls13ServerFinishedFields
{
	pub fn tx_finished(self, base: &mut SessionBase, debug: Debugger, state: &str) ->
		Result<HandshakeState2, TlsFailure>
	{
		let mut hs_hash = self.hs_hash;
		let htype = HSTYPE_FINISHED;
		let mac = self.hs_secrets.server_hs_write.generate_finished(&hs_hash.checkpoint()?, debug.clone())?;
		emit_handshake(debug.clone(), htype, mac.as_ref(), base, &mut hs_hash, state)?;
		let appkeys_hash = hs_hash.checkpoint()?;
		let client_app_write = self.hs_secrets.master_secret.traffic_secret_in(&appkeys_hash,
			debug.clone())?;
		let server_app_write = self.hs_secrets.master_secret.traffic_secret_out(&appkeys_hash,
			debug.clone())?;
		let extractor = self.hs_secrets.master_secret.extractor(&appkeys_hash, debug.clone())?;
		base.change_protector(server_app_write.to_protector(debug.clone())?);
		base.change_extractor(extractor);
		debug!(HANDSHAKE_EVENTS debug, "Sent Finished, upstream encryption keys changed");
		let keys = Tls13C2ndFSecrets {
			client_app_write: client_app_write,
			server_app_write: server_app_write,
			client_hs_write: self.hs_secrets.client_hs_write,
		};
		if self.requested_cc {
			Ok(HandshakeState2::Tls13ClientCertificate(Tls13ClientCertificateFields {
				kill_connection_after_handshake: self.kill_connection_after_handshake,
				hs_secrets2: keys,
				hs_hash: hs_hash,
			}))
		} else {
			Ok(HandshakeState2::Tls13ClientFinished(Tls13ClientFinishedFields{
				kill_connection_after_handshake: self.kill_connection_after_handshake,
				hs_secrets2: keys,
				hs_hash: hs_hash,
				auth_as: ClientCertificateInfo::new_noauth(),
			}))
		}
	}
}

impl Tls13ClientFinishedFields
{
	pub fn rx_finished<'a>(self, msg: &[u8], controller: &mut SessionController, debug: Debugger,
		raw_msg: HandshakeMessage<'a>, config: &ServerConfiguration) -> Result<HandshakeState2, TlsFailure>
	{
		let mut hs_hash = self.hs_hash;
		self.hs_secrets2.client_hs_write.check_finished(&hs_hash.checkpoint()?, msg, debug.clone())?;
		controller.set_deprotector(self.hs_secrets2.client_app_write.to_deprotector(debug.clone())?);
		debug!(HANDSHAKE_EVENTS debug, "Received finished, downstream \
			encryption keys changed");
		//Signal auth if any.
		if let &Some(ref cc_cb) = &config.ask_client_cert {
			let mut cc_cb = cc_cb.clone_self();
			cc_cb.on_certificate_auth(self.auth_as).map_err(|_|TlsFailure::TlsClientAuthNotOk)?;
		}
		
		hs_hash.add(raw_msg, debug)?;	//The final handshake hash. Needed for e.g. RMS.
		Ok(HandshakeState2::Tls13Showtime(Tls13ShowtimeFields{
			kill_connection_after_handshake: self.kill_connection_after_handshake,
			client_app_write: self.hs_secrets2.client_app_write,
			server_app_write: self.hs_secrets2.server_app_write,
			rekey_in_progress: false,
		}))
	}
}

impl Tls12ClientFinishedFields
{
	pub fn rx_finished<'a>(self, msg: &[u8], controller: &mut SessionController, debug: Debugger,
		raw_msg: HandshakeMessage<'a>, config: &ServerConfiguration) -> Result<HandshakeState2, TlsFailure>
	{
		//If client authentication was requested, we always signal it was rejected in TLS 1.2
		do_refused_client_auth(config)?;

		let mut hs_hash = self.hs_hash;
		self.master_secret.check_finished(&hs_hash.checkpoint()?, debug.clone(), msg)?;
		controller.set_extractor(self.master_secret.get_exporter(debug.clone())?);
		//We compute EMS key in HS demux since we need hash of this message.
		hs_hash.add(raw_msg, debug.clone())?;
		Ok(HandshakeState2::Tls12ServerFinished(Tls12ServerFinishedFields {
			kill_connection_after_handshake: self.kill_connection_after_handshake,
			master_secret: self.master_secret,
			hs_hash: hs_hash,
		}))
	}
}


impl Tls12ServerFinishedFields
{
	pub fn tx_finished(self, base: &mut SessionBase, debug: Debugger, state: &str) ->
		Result<HandshakeState2, TlsFailure>
	{
		base.send_message_fragment(20, &[1]);
		base.change_protector(self.master_secret.get_protector(debug.clone())?);
		debug!(HANDSHAKE_EVENTS debug, "Sent ChangeCipherSpec, enabling upstream \
			encryption");
		//The CCS is not hashed according to TLS 1.2.

		let mut hs_hash = self.hs_hash;
		let htype = HSTYPE_FINISHED;
		let mac = self.master_secret.generate_finished(&hs_hash.checkpoint()?, debug.clone())?;
		emit_handshake(debug.clone(), htype, mac.as_ref(), base, &mut hs_hash, state)?;
		Ok(HandshakeState2::Tls12Showtime(Tls12ShowtimeFields{
			kill_connection_after_handshake: self.kill_connection_after_handshake,
		}))
	}
}

pub struct Tls12ClientChangeCipherSpecFields
{
	pub kill_connection_after_handshake: bool,
	pub premaster_secret: Tls12PremasterSecret,	//Premaster secret.
	pub client_random: [u8; 32],
	pub server_random: [u8; 32],
	pub hs_hash: HandshakeHash,
}

impl Tls12ClientChangeCipherSpecFields
{
	pub fn rx_change_cipher_spec(self, controller: &mut SessionController, debug: Debugger) ->
		Result<HandshakeState2, TlsFailure>
	{
		//This is not true handshake message, so not added to handshake hash.
		let master_secret = Tls12MasterSecret::new(self.premaster_secret, &self.client_random,
			&self.server_random, self.hs_hash.checkpoint()?, debug.clone())?;
		controller.set_deprotector(master_secret.get_deprotector(debug.clone())?);
		debug!(HANDSHAKE_EVENTS debug, "Received ChangeCipherSpec, downstream \
			encryption enabled");
		Ok(HandshakeState2::Tls12ClientFinished(Tls12ClientFinishedFields{
			kill_connection_after_handshake: self.kill_connection_after_handshake,
			master_secret: master_secret,
			hs_hash: self.hs_hash,
		}))
	}
}
