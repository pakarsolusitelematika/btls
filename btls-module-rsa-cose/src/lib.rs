#[macro_use]
extern crate btls_module_defs;
extern crate btls_aux_rsa_sign;
use btls_module_defs::{HandleType, PubkeyReply, SignatureReply};
use btls_module_defs::{BTLS_ALG_RSA_PKCS1_SHA256, BTLS_ALG_RSA_PKCS1_SHA384, BTLS_ALG_RSA_PKCS1_SHA512,
	BTLS_ALG_RSA_PSS_SHA256, BTLS_ALG_RSA_PSS_SHA384, BTLS_ALG_RSA_PSS_SHA512};
use btls_aux_rsa_sign::RsaKeyPair;
use std::io::Read as IoRead;
use std::iter::repeat;
use std::sync::Arc;
use std::str::from_utf8;
use std::cmp::min;

macro_rules! decrement_count {
	($left:expr) => {
		match $left {
			Some(0) => break,		//End.
			Some(x) => Some(x - 1),		//Take one.
			None => None,			//Indefinite.
		}
	}
}

macro_rules! handle_break {
	($left:expr, $error:expr) => {
		if $left.is_some() {
			return Err($error);
		} else {
			break;		//End of map.
		}
	}
}

//Max 8192 bits.
const CBOR_MAX: usize = 1024;


#[derive(Clone)]
struct KeyHandle
{
	rsa_pss_variant: u8,
	pubkey: Vec<u8>,
	keypair: Arc<RsaKeyPair>,
}

fn extend_to(data: &[u8], size: usize) -> Vec<u8>
{
	let mut v = Vec::with_capacity(size);
	v.extend(repeat(0u8).take(size.saturating_sub(data.len())));
	v.extend_from_slice(data);
	v
}

//This is really screwed up, but enough for our purposes.
fn wrap_asn1(tag: u8, data: &[u8]) -> Vec<u8>
{
	let mut out = Vec::with_capacity(data.len() + 4);
	out.push(tag);
	if data.len() < 128 {
		out.push(data.len() as u8);
	} else if data.len() < 256 {
		out.push(0x81);
		out.push(data.len() as u8);
	} else {
		out.push(0x82);
		out.push((data.len() >> 8) as u8);
		out.push(data.len() as u8);
	}
	out.extend_from_slice(data);
	out
}

//Add leading zero to integer if needed.
fn add_leading_zero(data: &[u8]) -> Vec<u8>
{
	let mut out = Vec::with_capacity(data.len() + 1);
	if data.len() == 0 || data[0] >= 128 { out.push(0); }
	out.extend_from_slice(data);
	out
}

#[derive(Copy,Clone,PartialEq,Eq,Debug)]
enum CborHeader
{
	PositiveInt(u64),
	NegativeInt(u64),
	Octets(Option<u64>),
	String(Option<u64>),
	Array(Option<u64>),
	Map(Option<u64>),
	Tag(u64),
	Simple(u8),
	Half(u16),
	Single(u32),
	Double(u64),
	Break,
}

fn read_cbor_header<R:IoRead>(data: &mut R) -> Result<CborHeader, ()>
{
	let mut tagbyte = [0];
	data.read_exact(&mut tagbyte).map_err(|_|())?;
	let tagbyte = tagbyte[0];	//tagbyte is 1-length.
	let minor = tagbyte & 31;
	//If low 5 bits are 24-27, there are additional bytes.
	let extrabytes = match minor {
		24 => 1,
		25 => 2,
		26 => 4,
		27 => 8,
		_ => 0,
	};
	let mut extra = [0;8];
	let extra = &mut extra[..extrabytes];
	data.read_exact(extra).map_err(|_|())?;
	let argument = {
		let mut a = if minor < 24 { minor as u64 } else { 0 as u64 };
		for i in extra.iter() { a = a * 256 + (*i as u64); }
		a
	};
	match tagbyte {
		0x00...0x1B => Ok(CborHeader::PositiveInt(argument)),
		0x20...0x3B => Ok(CborHeader::NegativeInt(argument)),
		0x40...0x5B => Ok(CborHeader::Octets(Some(argument))),
		0x5F => Ok(CborHeader::Octets(None)),
		0x60...0x7B => Ok(CborHeader::String(Some(argument))),
		0x7F => Ok(CborHeader::String(None)),
		0x80...0x9B => Ok(CborHeader::Array(Some(argument))),
		0x9F => Ok(CborHeader::Array(None)),
		0xA0...0xBB => Ok(CborHeader::Map(Some(argument))),
		0xBF => Ok(CborHeader::Map(None)),
		0xC0...0xDB => Ok(CborHeader::Tag(argument)),
		0xE0...0xF7 => Ok(CborHeader::Simple(minor)),
		0xF8 if argument >= 32 => Ok(CborHeader::Simple(argument as u8)),
		0xF9 => Ok(CborHeader::Half(argument as u16)),
		0xFA => Ok(CborHeader::Single(argument as u32)),
		0xFB => Ok(CborHeader::Double(argument)),
		0xFF => Ok(CborHeader::Break),
		_ => Err(())
	}
}

trait SprimValue
{
	type Container: Sized;
	fn new_container() -> <Self as SprimValue>::Container;
	fn get_length(hdr: CborHeader) -> Result<Option<u64>, ()>;
	fn append(container: &mut <Self as SprimValue>::Container, frag: &[u8]) -> Result<(), ()>;
	fn len_check(container: &<Self as SprimValue>::Container, flen: u64) -> Result<(), ()>;
}

struct CborString;

impl SprimValue for CborString
{
	type Container = String;
	fn new_container() -> String { String::new() }
	fn get_length(hdr: CborHeader) -> Result<Option<u64>, ()>
	{
		if let CborHeader::String(x) = hdr { Ok(x) } else { Err(()) }
	}
	fn append(container: &mut String, frag: &[u8]) -> Result<(), ()>
	{
		container.push_str(from_utf8(frag).map_err(|_|())?);
		Ok(())
	}
	fn len_check(container: &String, flen: u64) -> Result<(), ()>
	{
		if (CBOR_MAX.saturating_sub(container.len()) as u64) >= flen { Ok(()) } else { Err(()) }
	}
}

struct CborOctets;

impl SprimValue for CborOctets
{
	type Container = Vec<u8>;
	fn new_container() -> Vec<u8> { Vec::new() }
	fn get_length(hdr: CborHeader) -> Result<Option<u64>, ()>
	{
		if let CborHeader::Octets(x) = hdr { Ok(x) } else { Err(()) }
	}
	fn append(container: &mut Vec<u8>, frag: &[u8]) -> Result<(), ()>
	{
		container.extend_from_slice(frag);
		Ok(())
	}
	fn len_check(container: &Vec<u8>, flen: u64) -> Result<(), ()>
	{
		if (CBOR_MAX.saturating_sub(container.len()) as u64) >= flen { Ok(()) } else { Err(()) }
	}
}

fn skip_cbor_sprim2<R:IoRead,Sp:SprimValue>(data: &mut R, flen: Option<u64>, _spk: Sp) -> Result<(), ()>
{
	match flen {
		Some(s) => skip_input(data, s),
		None => {
			loop {
				match read_cbor_header(data)? {
					CborHeader::Break => break,
					x => skip_input(data, Sp::get_length(x)?.ok_or(())?)?
				};
			}
			Ok(())
		}
	}
}

fn read_cbor_sprim2_frag<R:IoRead,Sp:SprimValue>(data: &mut R, _spk: &Sp, s: u64,
	output: &mut <Sp as SprimValue>::Container) -> Result<(), ()>
{
	Sp::len_check(output, s)?;
	let mut out = Vec::with_capacity(s as usize);
	for _ in 0..s { out.push(0); }
	data.read_exact(&mut out).map_err(|_|())?;
	Sp::append(output, &out)?;
	Ok(())
}

fn read_cbor_sprim2<R:IoRead,Sp:SprimValue>(data: &mut R, flen: Option<u64>, spk: Sp) ->
	Result<<Sp as SprimValue>::Container, ()>
{
	let mut container = Sp::new_container();
	match flen {
		Some(s) => read_cbor_sprim2_frag(data, &spk, s, &mut container)?,
		None => {
			loop {
				match read_cbor_header(data)? {
					CborHeader::Break => break,
					x => {
						let s = Sp::get_length(x)?.ok_or(())?;
						read_cbor_sprim2_frag(data, &spk, s, &mut container)?;
					}
				};
			}
		}
	};
	Ok(container)
}


fn skip_input<R:IoRead>(data: &mut R, mut size: u64) -> Result<(), ()>
{
	let mut buf = [0;256];
	while size > 0 {
		let frag = min(size, buf.len() as u64) as usize;
		data.read_exact(&mut buf[..frag]).map_err(|_|())?;
		size -= frag as u64;
	}
	Ok(())
}

const ERR_CBOR_ERROR: u32 = 1;				//Key is invalid CBOR.
const ERR_NOT_CWK: u32 = 2;				//Invalid key format.
const ERR_NOT_RSA_KEY: u32 = 3;				//Not RSA key, or required parameters missing.
const ERR_UNKNOWN_ALGORITHM: u32 = 4;			//Algorithm is unknown.
const ERR_UNKNOWN_RSA_PARAMETER: u32 = 5;		//Unknown RSA parameter encountered (key not supported)
const ERR_OPERATION_NOT_ALLOWED: u32 = 6;		//Key does not allow signatures.
const ERR_JUNK_AFTER_END: u32 = 7;			//Junk after end of key.
const ERR_CANT_IMPORT_KEYPAIR: u32 = 8;			//Keypair import failed.
//const ERR_CANT_MAKE_SIGNING_STATE: u32 = 9;		//Can't make signing state.
const ERR_COMPONENT_TOO_LARGE: u32 = 10;		//Component too big.

const KIND_KTY: u32 = 1;
const KIND_KEY_OPS: u32 = 2;
const KIND_ALG: u32 = 3;
const KIND_RSA_N: u32 = 4;
const KIND_RSA_E: u32 = 5;
const KIND_RSA_D: u32 = 6;
const KIND_RSA_P: u32 = 7;
const KIND_RSA_Q: u32 = 8;
const KIND_RSA_DP: u32 = 9;
const KIND_RSA_DQ: u32 = 10;
const KIND_RSA_QI: u32 = 11;
const KIND_KID: u32 = 12;

impl KeyHandle
{
	fn _get_key_handle2(mut data: &[u8]) -> Result<Self, u32>
	{
		let (mut rsa_n, mut rsa_e, mut rsa_d, mut rsa_p, mut rsa_q, mut rsa_dp, mut rsa_dq, mut rsa_qi) =
			(Vec::new(), Vec::new(), Vec::new(), Vec::new(), Vec::new(), Vec::new(), Vec::new(),
			Vec::new());
		let mut type_ok = false;
		let mut variant = 0;
		//Read the key top-level map header. This has to be map.
		let mut reader = &mut data;
		let mut left = match read_cbor_header(&mut reader) {
			Ok(CborHeader::Map(x)) => x,
			Ok(_) => return Err(ERR_NOT_CWK),
			Err(_) => return Err(ERR_CBOR_ERROR)
		};
		loop {
			//Decrement entries left.
			left = decrement_count!(left);
			//Read and process the key.
			let kind = match read_cbor_header(&mut reader) {
				Ok(CborHeader::Break) => handle_break!(left, ERR_CBOR_ERROR),
				Ok(CborHeader::PositiveInt(1)) => KIND_KTY,
				Ok(CborHeader::PositiveInt(2)) => KIND_KID,
				Ok(CborHeader::PositiveInt(3)) => KIND_ALG,
				Ok(CborHeader::PositiveInt(4)) => KIND_KEY_OPS,
				Ok(CborHeader::NegativeInt(0)) => KIND_RSA_N,
				Ok(CborHeader::NegativeInt(1)) => KIND_RSA_E,
				Ok(CborHeader::NegativeInt(2)) => KIND_RSA_D,
				Ok(CborHeader::NegativeInt(3)) => KIND_RSA_P,
				Ok(CborHeader::NegativeInt(4)) => KIND_RSA_Q,
				Ok(CborHeader::NegativeInt(5)) => KIND_RSA_DP,
				Ok(CborHeader::NegativeInt(6)) => KIND_RSA_DQ,
				Ok(CborHeader::NegativeInt(7)) => KIND_RSA_QI,
				//We don't know about any string labels on keys.
				Ok(CborHeader::PositiveInt(_)) => return Err(ERR_UNKNOWN_RSA_PARAMETER),
				Ok(CborHeader::NegativeInt(_)) => return Err(ERR_UNKNOWN_RSA_PARAMETER),
				Ok(CborHeader::String(_)) => return Err(ERR_UNKNOWN_RSA_PARAMETER),
				Ok(_) => return Err(ERR_NOT_CWK),
				Err(_) => return Err(ERR_CBOR_ERROR)
			};
			let value = read_cbor_header(&mut reader).map_err(|_|ERR_CBOR_ERROR)?;
			match (kind, value) {
				//KTY: This must be integer 3. Otherwise this is not RSA key.
				(KIND_KTY, CborHeader::PositiveInt(3)) => type_ok = true,
				(KIND_KTY, _) => return Err(ERR_NOT_RSA_KEY),
				//RSA parameters. These must be cotet strings.
				(KIND_RSA_N, CborHeader::Octets(x)) => rsa_n = read_cbor_sprim2(&mut reader, x,
					CborOctets).map_err(|_|ERR_CBOR_ERROR)?,
				(KIND_RSA_E, CborHeader::Octets(x)) => rsa_e = read_cbor_sprim2(&mut reader, x,
					CborOctets).map_err(|_|ERR_CBOR_ERROR)?,
				(KIND_RSA_D, CborHeader::Octets(x)) => rsa_d = read_cbor_sprim2(&mut reader, x,
					CborOctets).map_err(|_|ERR_CBOR_ERROR)?,
				(KIND_RSA_P, CborHeader::Octets(x)) => rsa_p = read_cbor_sprim2(&mut reader, x,
					CborOctets).map_err(|_|ERR_CBOR_ERROR)?,
				(KIND_RSA_Q, CborHeader::Octets(x)) => rsa_q = read_cbor_sprim2(&mut reader, x,
					CborOctets).map_err(|_|ERR_CBOR_ERROR)?,
				(KIND_RSA_DP, CborHeader::Octets(x)) => rsa_dp = read_cbor_sprim2(&mut reader, x,
					CborOctets).map_err(|_|ERR_CBOR_ERROR)?,
				(KIND_RSA_DQ, CborHeader::Octets(x)) => rsa_dq = read_cbor_sprim2(&mut reader, x,
					CborOctets).map_err(|_|ERR_CBOR_ERROR)?,
				(KIND_RSA_QI, CborHeader::Octets(x)) => rsa_qi = read_cbor_sprim2(&mut reader, x,
					CborOctets).map_err(|_|ERR_CBOR_ERROR)?,
				//ALGORITHM. This must be -37, -38 or -39.
				(KIND_ALG, CborHeader::NegativeInt(36)) => variant = 1,
				(KIND_ALG, CborHeader::NegativeInt(37)) => variant = 2,
				(KIND_ALG, CborHeader::NegativeInt(38)) => variant = 3,
				(KIND_ALG, _) => return Err(ERR_UNKNOWN_ALGORITHM),
				//KEY_OPS. This must be an array containg 1.
				(KIND_KEY_OPS, CborHeader::Array(x)) => {
					//This array has to contain 1, to signal signing is allowed.
					let mut is_ok = false;
					let mut left2 = x;
					loop {
						//Decrement entries left.
						left2 = decrement_count!(left2);
						match read_cbor_header(&mut reader) {
							Ok(CborHeader::Break) => handle_break!(left2,
								ERR_CBOR_ERROR),
							Ok(CborHeader::PositiveInt(1)) => is_ok = true,
							Ok(CborHeader::PositiveInt(_)) => (),
							Ok(CborHeader::NegativeInt(_)) => (),
							Ok(CborHeader::String(x)) => skip_cbor_sprim2(&mut reader,
								x, CborString).map_err(|_|ERR_CBOR_ERROR)?,
							Ok(_) => return Err(ERR_NOT_CWK),
							Err(_) => return Err(ERR_CBOR_ERROR),
						};
					}
					if !is_ok { return Err(ERR_OPERATION_NOT_ALLOWED) }
				},
				//KID has to be octet string (skipped).
				(KIND_KID, CborHeader::Octets(x)) => skip_cbor_sprim2(&mut reader, x,
					CborOctets).map_err(|_|ERR_CBOR_ERROR)?,
				//Others are not allowed in CWK.
				(_, _) => return Err(ERR_NOT_CWK),
			}
		}
		if reader.len() != 0 { return Err(ERR_JUNK_AFTER_END); }
		if !type_ok || rsa_n.len() == 0 || rsa_e.len() == 0 || rsa_d.len() == 0 || rsa_p.len() == 0 ||
			rsa_q.len() == 0 || rsa_dp.len() == 0 || rsa_dq.len() == 0 || rsa_qi.len() == 0 {
			return Err(ERR_NOT_RSA_KEY);
		}
		//Serialize the private key.
		let lbytes = rsa_n.len();
		let sbytes = (lbytes + 1) / 2;
		if rsa_n.len() > lbytes || rsa_d.len() > lbytes || rsa_p.len() > sbytes || rsa_q.len() > sbytes ||
			rsa_dp.len() > sbytes || rsa_dq.len() > sbytes ||  rsa_qi.len() > sbytes {
			return Err(ERR_COMPONENT_TOO_LARGE);
		}
		
		let mut private_key = Vec::new();
		private_key.extend_from_slice(&[(lbytes >> 8) as u8, lbytes as u8]);
		private_key.extend_from_slice(&extend_to(&rsa_n, lbytes));
		private_key.extend_from_slice(&extend_to(&rsa_d, lbytes));
		private_key.extend_from_slice(&extend_to(&rsa_p, sbytes));
		private_key.extend_from_slice(&extend_to(&rsa_q, sbytes));
		private_key.extend_from_slice(&extend_to(&rsa_dp, sbytes));
		private_key.extend_from_slice(&extend_to(&rsa_dq, sbytes));
		private_key.extend_from_slice(&extend_to(&rsa_qi, sbytes));
		private_key.extend_from_slice(&rsa_e);

		//Public key.
		let mut pub_main = Vec::new();
		pub_main.extend_from_slice(&wrap_asn1(2, &add_leading_zero(&rsa_n)));
		pub_main.extend_from_slice(&wrap_asn1(2, &add_leading_zero(&rsa_e)));
		let mut public_key = wrap_asn1(0x30, &pub_main);
		public_key.insert(0, 0);
		let public_key_b = wrap_asn1(0x03, &public_key);	//Wrap in BIT STRING.
		let mut public_key = Vec::new();
		public_key.extend_from_slice(&[0x30, 0x0d, 0x06, 0x09, 42, 134, 72, 134, 247, 13, 1, 1, 1, 5, 0]);
		public_key.extend_from_slice(&public_key_b);
		let public_key = wrap_asn1(0x30, &public_key);
		//Make a key pair and signing state.
		let (key_pair, _) = RsaKeyPair::from_bytes(&private_key).map_err(|_|ERR_CANT_IMPORT_KEYPAIR)?;
		Ok(KeyHandle{
			rsa_pss_variant: variant,
			pubkey: public_key,
			keypair: Arc::new(key_pair),
		})
	}
}

impl HandleType for KeyHandle
{
	fn get_key_handle2(data: &[u8], errcode: &mut u32) -> Option<Self>
	{
		match Self::_get_key_handle2(data) {
			Ok(x) => Some(x),
			Err(x) => {
				*errcode = x;
				None
			}
		}
	}
	fn request_sign(&self, reply: SignatureReply, algorithm: u16, data: &[u8])
	{
		let variant = self.rsa_pss_variant;
		match match algorithm {
			BTLS_ALG_RSA_PKCS1_SHA256 if variant == 0 =>
				self.keypair.sign(data, algorithm),
			BTLS_ALG_RSA_PKCS1_SHA384 if variant == 0 =>
				self.keypair.sign(data, algorithm),
			BTLS_ALG_RSA_PKCS1_SHA512 if variant == 0 =>
				self.keypair.sign(data, algorithm),
			BTLS_ALG_RSA_PSS_SHA256 if variant == 0 || variant == 1 =>
				self.keypair.sign(data, algorithm),
			BTLS_ALG_RSA_PSS_SHA384 if variant == 0 || variant == 2 =>
				self.keypair.sign(data, algorithm),
			BTLS_ALG_RSA_PSS_SHA512 if variant == 0 || variant == 3 =>
				self.keypair.sign(data, algorithm),
			_ => Err(())
		} {
			Ok(x) => reply.finish(Some(&x)),
			Err(_) => reply.finish(None),
		};
	}
	fn request_pubkey<'a>(&self, reply: PubkeyReply<'a>)
	{
		static SCHEMES: [u16; 6] = [BTLS_ALG_RSA_PKCS1_SHA256, BTLS_ALG_RSA_PKCS1_SHA384,
			BTLS_ALG_RSA_PKCS1_SHA512, BTLS_ALG_RSA_PSS_SHA256, BTLS_ALG_RSA_PSS_SHA384,
			BTLS_ALG_RSA_PSS_SHA512];
		static VARIANT1: [u16; 1] = [BTLS_ALG_RSA_PSS_SHA256];
		static VARIANT2: [u16; 1] = [BTLS_ALG_RSA_PSS_SHA384];
		static VARIANT3: [u16; 1] = [BTLS_ALG_RSA_PSS_SHA512];
		match self.rsa_pss_variant {
			0 => reply.finish(&self.pubkey[..], &SCHEMES[..]),
			1 => reply.finish(&self.pubkey[..], &VARIANT1[..]),
			2 => reply.finish(&self.pubkey[..], &VARIANT2[..]),
			3 => reply.finish(&self.pubkey[..], &VARIANT3[..]),
			_ => ()
		};
	}
}

btls_module!(KeyHandle);

#[cfg(test)]
fn test_cbor_header_decode_inner(mut data: &[u8], expected: CborHeader)
{
	let mut data = &mut data;
	let header = read_cbor_header(&mut data).unwrap();
	assert_eq!(data.len(), 0);
	assert_eq!(header, expected);
}

#[cfg(test)]
fn test_cbor_header_decode_fail(mut data: &[u8])
{
	let mut data = &mut data;
	read_cbor_header(&mut data).unwrap_err();
}

#[cfg(test)]
fn test_normal_major<F>(base: u8, expect: F, expect_ind: Option<CborHeader>) where F: Fn(u64) -> CborHeader
{
	test_cbor_header_decode_inner(&[base+0x00], expect(0));
	test_cbor_header_decode_inner(&[base+0x01], expect(1));
	test_cbor_header_decode_inner(&[base+0x17], expect(23));
	test_cbor_header_decode_inner(&[base+0x18,0x00], expect(0));
	test_cbor_header_decode_inner(&[base+0x18,0x03], expect(3));
	test_cbor_header_decode_inner(&[base+0x18,0x18], expect(24));
	test_cbor_header_decode_inner(&[base+0x18,0xFF], expect(255));
	test_cbor_header_decode_inner(&[base+0x19,0x00,0x00], expect(0));
	test_cbor_header_decode_inner(&[base+0x19,0x00,0x03], expect(3));
	test_cbor_header_decode_inner(&[base+0x19,0x01,0x00], expect(256));
	test_cbor_header_decode_inner(&[base+0x19,0xFF,0xFF], expect(65535));
	test_cbor_header_decode_inner(&[base+0x1A,0x00,0x00,0x00,0x00], expect(0));
	test_cbor_header_decode_inner(&[base+0x1A,0x00,0x00,0x00,0x03], expect(3));
	test_cbor_header_decode_inner(&[base+0x1A,0x00,0x01,0x00,0x00], expect(65536));
	test_cbor_header_decode_inner(&[base+0x1A,0xFF,0xFF,0xFF,0xFF], expect(0xFFFFFFFF));
	test_cbor_header_decode_inner(&[base+0x1B,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00], expect(0));
	test_cbor_header_decode_inner(&[base+0x1B,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x03], expect(3));
	test_cbor_header_decode_inner(&[base+0x1B,0x00,0x00,0x00,0x01,0x00,0x00,0x00,0x00],
		expect(0x100000000));
	test_cbor_header_decode_inner(&[base+0x1B,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF],
		expect(0xFFFFFFFFFFFFFFFF));
	test_cbor_header_decode_fail(&[base+0x1C,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00]);
	test_cbor_header_decode_fail(&[base+0x1D,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00]);
	test_cbor_header_decode_fail(&[base+0x1E,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00]);
	if let Some(_expect) = expect_ind {
		test_cbor_header_decode_inner(&[base+0x1F], _expect);
	} else {
		test_cbor_header_decode_fail(&[base+0x1F,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00]);
	}
}

#[test]
fn test_cbor_header_decode()
{
	test_normal_major(0x00, |x|CborHeader::PositiveInt(x), None);
	test_normal_major(0x20, |x|CborHeader::NegativeInt(x), None);
	test_normal_major(0x40, |x|CborHeader::Octets(Some(x)), Some(CborHeader::Octets(None)));
	test_normal_major(0x60, |x|CborHeader::String(Some(x)), Some(CborHeader::String(None)));
	test_normal_major(0x80, |x|CborHeader::Array(Some(x)), Some(CborHeader::Array(None)));
	test_normal_major(0xA0, |x|CborHeader::Map(Some(x)), Some(CborHeader::Map(None)));
	test_normal_major(0xC0, |x|CborHeader::Tag(x), None);
	test_cbor_header_decode_inner(&[0xE0], CborHeader::Simple(0));
	test_cbor_header_decode_inner(&[0xE1], CborHeader::Simple(1));
	test_cbor_header_decode_inner(&[0xF7], CborHeader::Simple(23));
	test_cbor_header_decode_fail(&[0xF8,0x1F]);
	test_cbor_header_decode_inner(&[0xF8,0x20], CborHeader::Simple(32));
	test_cbor_header_decode_inner(&[0xF8,0xFF], CborHeader::Simple(255));
	test_cbor_header_decode_inner(&[0xF9,0x12,0x34], CborHeader::Half(0x1234));
	test_cbor_header_decode_inner(&[0xFA,0x12,0x34,0x56,0x78], CborHeader::Single(0x12345678));
	test_cbor_header_decode_inner(&[0xFB,0x12,0x34,0x56,0x78,0x9A,0xBC,0xDE,0xF0],
		CborHeader::Double(0x123456789ABCDEF0));
	test_cbor_header_decode_fail(&[0xFC,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00]);
	test_cbor_header_decode_fail(&[0xFD,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00]);
	test_cbor_header_decode_fail(&[0xFE,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00]);
	test_cbor_header_decode_inner(&[0xFF], CborHeader::Break);
}
