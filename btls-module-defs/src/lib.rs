//!Bindings to btls modules
//!
//!Use like follows:
//!
//!```no-run
//!#[macro_use]
//!extern crate btls_module_defs;
//!use btls_module_defs::{HandleType, PubkeyReply, SignatureReply};
//!
//!#[derive(Clone)]
//!struct FooKeyHandle
//!{
//!	//...
//!}
//!
//!impl HandleType for FooKeyHandle
//!{
//!	//Implement get_key_handle, request_sign and request_pubkey.
//!}
//!
//!btls_module!(FooKeyHandle);
//!```

extern crate libc;
pub use libc::c_char;
use std::ffi::{CStr,CString};
use std::mem::transmute;
use std::slice::from_raw_parts;
use std::convert::AsRef;
use std::collections::BTreeMap;
use std::ptr::null;
use std::panic::{catch_unwind, AssertUnwindSafe};
use std::marker::PhantomData;
use std::sync::{Arc, Mutex, Once, ONCE_INIT, RwLock};
use std::any::TypeId;

///RSA PKCS#1 v1.5 signature with SHA-256.
pub const BTLS_ALG_RSA_PKCS1_SHA256: u16 = 0x0401;
///RSA PKCS#1 v1.5 signature with SHA-384.
pub const BTLS_ALG_RSA_PKCS1_SHA384: u16 = 0x0501;
///RSA PKCS#1 v1.5 signature with SHA-512.
pub const BTLS_ALG_RSA_PKCS1_SHA512: u16 = 0x0601;
///RSA PSS signature with SHA-256 hash and MGF1, 32 byte salt.
pub const BTLS_ALG_RSA_PSS_SHA256: u16 = 0x0804;
///RSA PSS signature with SHA-384 hash and MGF1, 48 byte salt.
pub const BTLS_ALG_RSA_PSS_SHA384: u16 = 0x0805;
///RSA PSS signature with SHA-512 hash and MGF1, 64 byte salt.
pub const BTLS_ALG_RSA_PSS_SHA512: u16 = 0x0806;
///ECDSA with P-256 curve and SHA-256 hash.
pub const BTLS_ALG_ECDSA_P256_SHA256: u16 = 0x0403;
///ECDSA with P-384 curve and SHA-384 hash.
pub const BTLS_ALG_ECDSA_P384_SHA384: u16 = 0x0503;
///Ed25519
pub const BTLS_ALG_ED25519: u16 = 0x0807;
///Ed448
pub const BTLS_ALG_ED448: u16 = 0x0808;

#[repr(C)]
struct SignatureReplyInner;
#[repr(C)]
struct PubkeyReplyInner;

///The signature reply structure
///
///Ths structure encapsulates callback to respond to signature requests with.
#[repr(C)]
pub struct SignatureReply
{
	ctx: *mut SignatureReplyInner,
	_finish: extern "C" fn(*mut SignatureReplyInner, *const u8, usize),
	_log: extern "C" fn(*mut SignatureReplyInner, *const c_char)
}

unsafe impl Send for SignatureReply {}
unsafe impl Sync for SignatureReply {}

impl Drop for SignatureReply
{
	fn drop(&mut self)
	{
		//If not answered yet, answer with error.
		if !self.ctx.is_null() {
			(self._finish)(self.ctx, 0 as *const u8, 0)
		}
	}
}

impl SignatureReply
{
	///Finish signature computation and return signature
	///
	///# Parameters:
	///
	/// * `self`: The signature response structure.
	/// * `signature`: The signature to reply with. Use `None` if signature generation failed.
	pub fn finish(mut self, signature: Option<&[u8]>)
	{
		match signature {
			Some(x) => (self._finish)(self.ctx, x.as_ptr(), x.len()),
			None => (self._finish)(self.ctx, 0 as *const u8, 0),
		};
		//Nasty hack: Set context to NULL, so we know on dropping SignatureReply that it has already been
		//answered.
		self.ctx = null::<SignatureReplyInner>() as *mut _;
	}
	///Log a message.
	///
	///# Parameters:
	///
	/// * `self`: The signature response structure.
	/// * `message`: The message to log.
	pub fn log(&self, message: &str)
	{
		let tmp = match CString::new(message) { Ok(x) => x, Err(_) => return };
		(self._log)(self.ctx, tmp.as_ptr());
	}
}

///The public reply structure
///
///Ths structure encapsulates callback to respond to public key requests with.
#[repr(C)]
pub struct PubkeyReply<'a>
{
	ctx: *mut PubkeyReplyInner,
	_finish: extern "C" fn(*mut PubkeyReplyInner, pubkey: *const u8, pubkeylen: usize, schemes: *const u16,
		schemescount: usize),
	_phantom: PhantomData<&'a u8>	//So we can have lifetime argument.
}

impl<'a> PubkeyReply<'a>
{
	///Return a public key
	///
	///# Parameters:
	///
	/// * `self`: The public key response structure.
	/// * `pubkey`: The public key in X.509 SPKI form, including the outer SEQUENCE header.
	/// * `schemes`: The list of schemes supported, from `BTLS_ALG_*` constants.
	pub fn finish(self, pubkey: &[u8], schemes: &[u16])
	{
		(self._finish)(self.ctx, pubkey.as_ptr(), pubkey.len(), schemes.as_ptr(), schemes.len());
	}
}

type NtiMap = Arc<RwLock<BTreeMap<String, u64>>>;
type ItoMap = Arc<RwLock<BTreeMap<u64, (*mut u8, TypeId)>>>;
type NidxS = Arc<Mutex<u64>>;

//A dummy function that causes compilation error in rust <1.13.
fn dummy() -> Result<(), ()>
{
	Err(())?;
	Ok(())
}

fn with_trap<T:Sized, F: FnOnce() -> T>(cb: F, on_ex: T) -> T
{
	if false { let _ = dummy(); }		//Reference dummy somewhere.
	match catch_unwind(AssertUnwindSafe(||cb())) {
		Ok(x) => x,
		Err(_) => on_ex
	}
}

fn get_namemap() -> NtiMap
{
	static START: Once = ONCE_INIT;
	static mut X: *mut NtiMap = 0 as *mut NtiMap;
	START.call_once(|| {
		unsafe {
			X = Box::into_raw(Box::new(Arc::new(RwLock::new(BTreeMap::new()))));
		}
	});
	unsafe{(*X).clone()}
}

fn get_itemmap() -> ItoMap
{
	static START: Once = ONCE_INIT;
	static mut X: *mut ItoMap = 0 as *mut ItoMap;
	START.call_once(|| {
		unsafe {
			X = Box::into_raw(Box::new(Arc::new(RwLock::new(BTreeMap::new()))));
		}
	});
	unsafe{(*X).clone()}
}

fn get_next_idx() -> NidxS
{
	static START: Once = ONCE_INIT;
	static mut X: *mut NidxS = 0 as *mut NidxS;
	START.call_once(|| {
		unsafe {
			X = Box::into_raw(Box::new(Arc::new(Mutex::new(0))));
		}
	});
	unsafe{(*X).clone()}
}

///Trait that key handle types implement.
pub trait HandleType: Send+Clone+Sized+'static
{
	///Get the key handle.
	///
	///One can return a separate structure for each lookup: This method should not be called more than once
	///for each key.
	///
	///# Parameters:
	///
	/// * `_name`: The name of key to look up.
	/// * `_errcode`: If this fails, assign some error code to help debugging.
	///
	///# Return value:
	///
	/// * On success, return `Some(x)`, where `x` is the key handle.
	/// * On failure, return `None`.
	fn get_key_handle(_name: &str, _errcode: &mut u32) -> Option<Self>
	{
		None
	}
	///Get the key handle to inline key.
	///
	///Note that unlike get_key_handle, the returns are not cached.
	///
	///# Parameters:
	///
	/// * `_data`: The key data.
	/// * `_errcode`: If this fails, assign some error code to help debugging.
	///
	///# Return value:
	///
	/// * On success, return `Some(x)`, where `x` is the key handle.
	/// * On failure, return `None`.
	fn get_key_handle2(_name: &[u8], _errcode: &mut u32) -> Option<Self>
	{
		None
	}
	///Request a signature for data
	///
	///The call to `reply.finish()` MAY be delayed and occur in another thread after this call has returned.
	///If the reply context is dropped without calling `reply.finish()`, the signature operation is assumed to
	///have failed.
	///
	///# Parameters:
	///
	/// * `self`: The key handle.
	/// * `reply`: Reply structure. Use this to return the answer.
	fn request_sign(&self, reply: SignatureReply, algorithm: u16, data: &[u8]);
	///Request a public key corresponding to handle.
	///
	///If `reply.finish()` is not called during execution of this routine, the request is deemed failed.
	///
	///# Parameters:
	///
	/// * `self`: The key handle.
	/// * `reply`: Reply structure. Use this to return the answer.
	fn request_pubkey<'a>(&self, reply: PubkeyReply<'a>);
	#[doc(hidden)]
	fn name_to_handle(name: &str) -> Option<u64>
	{
		let namemap = get_namemap();
		let x = match namemap.read() {
			Ok(x) => x,
			Err(_) => return None
		};
		x.get(name).map(|x|*x)
	}
	#[doc(hidden)]
	fn handle_to_raw(&self, name: Option<&str>) -> u64
	{
		let namemap = get_namemap();
		let itemmap = get_itemmap();
		let nextidx = get_next_idx();
		let (mut x, mut y, mut z) = match (namemap.write(), itemmap.write(), nextidx.lock()) {
			(Ok(x), Ok(y), Ok(z)) => (x, y, z),
			(_, _, _) => return 0xFFFFFFFFFFFFFFFFu64
		};
		let newidx = *z;
		let selftype = TypeId::of::<Self>();
		*z = *z + 1;
		if let Some(_name) = name { x.insert(_name.to_owned(), newidx); }
		y.insert(newidx, (unsafe{transmute::<_,&mut u8>(Box::new(self.clone()))}, selftype));
		newidx
	}
	#[doc(hidden)]
	fn handle_from_raw(h: u64) -> Option<Self>
	{
		let selftype = TypeId::of::<Self>();
		let itemmap = get_itemmap();
		let x = match itemmap.read() {
			Ok(x) => x,
			Err(_) => return None
		};
		x.get(&h).and_then(|y|{
			if y.1 != selftype { return None; }	//Wrong type!
			let obj = unsafe{transmute::<_,&Self>(y.0).clone()};
			Some(obj)
		})
	}
	#[doc(hidden)]
	unsafe fn with_get_key_handle(name: *const c_char, errcode: *mut u32) -> u64
	{
		let name = CStr::from_ptr(name).to_string_lossy();
		//Check name cache.
		match Self::name_to_handle(name.as_ref()) {
			Some(x) => return x,
			None => ()
		};
		let ret = with_trap(||{Self::get_key_handle(name.as_ref(), &mut *errcode)}, None);
		match ret {
			Some(x) => x.handle_to_raw(Some(name.as_ref())),
			None => 0xFFFFFFFFFFFFFFFFu64,	//Invalid.
		}
	}
	#[doc(hidden)]
	unsafe fn with_get_key_handle2(data: *const u8, datalen: usize, errcode: *mut u32) -> u64
	{
		let data = from_raw_parts(data, datalen);
		let ret = with_trap(||{Self::get_key_handle2(data, &mut *errcode)}, None);
		match ret {
			Some(x) => x.handle_to_raw(None),	//No name.
			None => 0xFFFFFFFFFFFFFFFFu64,	//Invalid.
		}
	}
	#[doc(hidden)]
	unsafe fn with_request_sign(handle: u64, reply: SignatureReply, algorithm: u16, data: *const u8,
		datalen: usize)
	{
		let data = from_raw_parts(data, datalen);
		match Self::handle_from_raw(handle) {
			Some(x) => with_trap(||{x.request_sign(reply, algorithm, data)}, ()),
			None => reply.finish(None)	//NAK the signature.
		}
	}
	#[doc(hidden)]
	unsafe fn with_request_pubkey<'a>(handle: u64, reply: PubkeyReply<'a>)
	{
		match Self::handle_from_raw(handle) {
			Some(x) => with_trap(||{x.request_pubkey(reply)}, ()),
			None => ()
		}
	}
}

#[macro_export]
macro_rules! btls_module {
	($name:ident) => {
#[no_mangle]
pub unsafe extern "C" fn btls_mod_get_key_handle(a: *const btls_module_defs::c_char, b: *mut u32) -> u64
{
	$name::with_get_key_handle(a, b)
}

#[no_mangle]
pub unsafe extern "C" fn btls_mod_get_key_handle2(a: *const u8, b: usize, c: *mut u32) -> u64
{
	$name::with_get_key_handle2(a, b, c)
}

#[no_mangle]
pub unsafe extern "C" fn btls_mod_request_sign(a: u64, b: SignatureReply, c: u16, d: *const u8, e: usize)
{
	$name::with_request_sign(a, b, c, d, e)
}

#[no_mangle]
pub unsafe extern "C" fn btls_mod_request_pubkey(a: u64, b: PubkeyReply)
{
	$name::with_request_pubkey(a, b)
}
	}
}
