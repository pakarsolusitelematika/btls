use mio::{Poll, PollOpt, Ready, Token};
use mio::unix::UnixReady;
use std::borrow::Cow;
use std::cmp::min;
use std::collections::BTreeMap;
use std::fs::File;
use std::io::{ErrorKind as IoErrorKind, Read as IoRead, stderr, Write as IoWrite};
use std::mem::{swap};
use std::net::SocketAddr;
use std::ops::Deref;
use std::os::unix::io::IntoRawFd;
use std::os::unix::net::UnixStream;
use std::path::{Path,PathBuf};
use std::process::{Command, Stdio};
use std::str::FromStr;
use std::sync::{Arc, Mutex};
use std::sync::atomic::AtomicUsize;
use std::time::SystemTime;
use libc::{AF_INET, AF_INET6, SOCK_STREAM};
use ::file_descriptor::FileDescriptor;
use ::token::{AllocatedToken, TokenAllocatorPool};

macro_rules! cow {
	($x:expr) => { Cow::Borrowed($x) };
	($x:expr,$($p:expr),*) => { Cow::Owned(format!($x, $($p),*)) };
}

macro_rules! scow {
	($($p:expr),*) => { Some(cow!($($p),*)) };
}

macro_rules! store_err {
	($v:expr, $($p:expr),*) => { $v = scow!($($p),*) };
	(RET $v:expr, $($p:expr),*) => {{ store_err!($v, $($p),*); return; }};
}

#[derive(Clone)]
pub struct ConnectionInfo
{
	pub src_addr: [u16;8],
	pub src_port: u16,
	pub dst_addr: [u16;8],
	pub dst_port: u16,
}

impl ConnectionInfo
{
	pub fn from_socket_addrs(src: SocketAddr, dest: Option<SocketAddr>) -> ConnectionInfo
	{
		let (src_addr, src_port) = match src {
			SocketAddr::V4(x) => {
				let addr = x.ip().octets();
				([0u16,0,0,0,0,65535,(addr[0] as u16)*256+(addr[1] as u16),(addr[2] as u16) * 256 +
					(addr[3] as u16)], x.port())
			},
			SocketAddr::V6(x) => (x.ip().segments(), x.port())
		};
		let (dst_addr, dst_port) = match dest {
			Some(SocketAddr::V4(x)) => {
				let addr = x.ip().octets();
				([0u16,0,0,0,0,65535,(addr[0] as u16)*256+(addr[1] as u16),(addr[2] as u16) * 256 +
					(addr[3] as u16)], x.port())
			},
			Some(SocketAddr::V6(x)) => (x.ip().segments(), x.port()),
			None => ([0u16; 8], 0) //Just use unspecified address with port 0.
		};
		ConnectionInfo {
			src_addr: src_addr,
			dst_addr: dst_addr,
			src_port: src_port,
			dst_port: dst_port,
		}
	}
}

fn format_ipv6_address(addr: [u16;8]) -> [u8;39]
{
	let mut ret = [0u8; 39];
	for i in 0..7 { ret[5*i+4] = 58; }
	for i in 0..32 {
		let bits = 12 - 4 * (i & 3);
		let val = ((addr[i >> 2] >> bits) & 15) as u8;
		let idx = i + (i >> 2);
		ret[idx] = if val < 10 { val + 48 } else { val + 87 };
	}
	ret
}

fn with_proxyv2_len<F>(out: &mut Vec<u8>, mut cb: F) where F: FnMut(&mut Vec<u8>) -> ()
{
	let pos = out.len();
	out.push(0);
	out.push(0);
	let lbase = out.len();
	cb(out);
	let len = out.len() - lbase;
	out[pos+0] = (len >> 8) as u8;
	out[pos+1] = len as u8;
}

fn format_proxyv1_header(info: &ConnectionInfo) -> String {
	let srcaddr = format_ipv6_address(info.src_addr);
	let dstaddr = format_ipv6_address(info.src_addr);
	format!("PROXY TCP6 {} {} {} {}\r\n", String::from_utf8_lossy(&srcaddr),
		String::from_utf8_lossy(&dstaddr), info.src_port, info.dst_port)
}


fn format_proxyv2_header(info: &ConnectionInfo, alpn: Option<Arc<String>>, sni: Arc<String>, tlsver: u8) -> Vec<u8>
{
	let mut ret = Vec::new();
	const MAGIC:[u8;12] = [13,10,13,10,0,13,10,0x51,0x55,0x49,0x54,10];
	ret.extend_from_slice(&MAGIC);
	ret.push(0x21);		//PROXY v2, on behalf of another node.
	ret.push(0x21);		//TCP/IPv6.
	with_proxyv2_len(&mut ret, |ret|{
		for i in 0..16 { ret.push((info.src_addr[i >> 1] >> 8 - 8 * (i & 1)) as u8); }
		for i in 0..16 { ret.push((info.dst_addr[i >> 1] >> 8 - 8 * (i & 1)) as u8); }
		ret.push((info.src_port >> 8) as u8);
		ret.push(info.src_port as u8);
		ret.push((info.dst_port >> 8) as u8);
		ret.push(info.dst_port as u8);
		ret.push(0x20);						//PP2_TYPE_SSL.
		with_proxyv2_len(ret, |ret|{
			ret.push(1);					//This is TLS connection.
			for _ in 0..4 { ret.push(0); }			//verify=0.
			ret.push(0x21);					//SSL version.
			with_proxyv2_len(ret, |ret|{
				let mut version = [84,76,83,32,49,46,50];
				//Assume that TLS 1.10 is way off:
				version[6] = tlsver.wrapping_add(47);
				ret.extend_from_slice(&version);
			})
		});
		//ALPN, if any.
		if let &Some(ref alpn) = &alpn {
			ret.push(0x01);		//ALPN.
			with_proxyv2_len(ret, |ret|{
				ret.extend_from_slice(alpn.as_bytes());
			});
		}
		if sni.len() > 0 {
			//Add SNI info if any.
			ret.push(0x02);		//SNI.
			with_proxyv2_len(ret, |ret|{
				ret.extend_from_slice(sni.as_bytes());
				//The authority info also has a port!
				ret.extend_from_slice(format!(":{}", info.dst_port).as_bytes());
			});
		}
	});
	ret
}

#[derive(Clone)]
enum Service
{
	Bad,
	Tcp(SocketAddr, u32),
	Socket(String, u32),
	Command(Vec<String>),
}


#[derive(Clone)]
struct ServiceCacheInfo
{
	timestamp: SystemTime,
	data: Service,
	alpns: Arc<Vec<String>>,
}

fn refresh_cached_entry(path: &Path) -> Result<ServiceCacheInfo, ()>
{
	let mut fp = File::open(path).map_err(|_|())?;
	let metadata = match fp.metadata() {
		Ok(x) => x,
		Err(_) => return Err(())
	};
	let ts = match metadata.modified() {
		Ok(x) => x,
		Err(_) => return Err(())
	};
	let bad_service = Ok(ServiceCacheInfo{timestamp:ts, data:Service::Bad, alpns: Arc::new(Vec::new())});
	let mut content = String::new();
	fp.read_to_string(&mut content).map_err(|_|())?;
	let spacepos = match content.find(' ') {
		Some(x) => x,
		None => return bad_service
	};
	let protocol = &content[..spacepos];
	let rest = &content[spacepos+1..];	//spacepos is an index, and thus not near overflow.
	let (rest, otherconfig) = match rest.find('\n') {
		Some(x) => (&rest[..x], &rest[x+1..]),
		None => (rest, "")
	};
	//Parse otherconfig.
	let mut alpns = Vec::new();
	for cfgline in otherconfig.lines() {
		if cfgline.starts_with("alpn ") {
			alpns.push((&cfgline[5..]).to_owned());
		}
	}
	Ok(ServiceCacheInfo{timestamp:ts, data:if protocol == "socket" {
		Service::Socket(rest.to_owned(), 0)
	} else if protocol == "socket-proxy" {
		Service::Socket(rest.to_owned(), 1)
	} else if protocol == "socket-proxy-v2" {
		Service::Socket(rest.to_owned(), 2)
	} else if protocol == "tcp" {
		Service::Tcp(match SocketAddr::from_str(rest) { Ok(x) => x, Err(_) => return bad_service}, 0)
	} else if protocol == "tcp-proxy" {
		Service::Tcp(match SocketAddr::from_str(rest) { Ok(x) => x, Err(_) => return bad_service}, 1)
	} else if protocol == "tcp-proxy-v2" {
		Service::Tcp(match SocketAddr::from_str(rest) { Ok(x) => x, Err(_) => return bad_service}, 2)
	} else if protocol == "execute" {
		let mut components = Vec::new();
		let mut component = String::new();
		let mut escaped = false;
		for i in rest.chars() {
			if !escaped {
				if i == ' ' {
					if component.len() > 0 { components.push(component.clone()); }
					component = String::new();
				} else if i == '\\' {
					escaped = true;
				} else {
					component.push(i);
				}
			} else {
				escaped = false;
				component.push(i);
			}
		}
		if component.len() > 0 { components.push(component.clone()); }
		if components.len() == 0 { return bad_service; }
		Service::Command(components)
	} else {
		Service::Bad
	}, alpns: Arc::new(alpns)})
}

#[derive(Clone)]
pub struct ServiceCache {
	cache: Arc<Mutex<BTreeMap<String, ServiceCacheInfo>>>,
	services_dir: PathBuf
}

impl ServiceCache
{
	pub fn new(services_dir: &Path) -> ServiceCache
	{
		ServiceCache{
			cache: Arc::new(Mutex::new(BTreeMap::new())),
			services_dir: services_dir.to_path_buf(),
		}
	}
	//Query list of supported ALPNs.
	pub fn query_alpn_list(&self, sni: &str) -> Arc<Vec<String>>
	{
		match self.lookup_by_sni(sni) {
			Ok(x) => x.alpns.clone(),
			Err(_) => Arc::new(Vec::new())
		}
	}
	//Lookup handler for SNI.
	fn lookup_by_sni(&self, sni: &str) -> Result<ServiceCacheInfo, Cow<'static, str>>
	{
		//See that there is no characters in SNI that would cause problems.
		//The following characters are illegal: 0-31, slash, backslash, 127-159.
		for i in sni.chars() {
			let chval = i as u32;
			if chval < 32 || i == '/' || i == '\\' || (chval >= 127 && chval < 160) {
				return Err(cow!("Client sent illegal SNI"));
			}
		}
		let spath = self.services_dir.join(sni.deref());
		let metadata = match spath.metadata() {
			Ok(x) => x,
			Err(_) => return Err(cow!("No handler for SNI '{}'", sni))
		};
		let lastmod = match (metadata.is_file(), metadata.modified()) {
			(true, Ok(x)) => x,
			_ => return Err(cow!("Bad handler for SNI '{}'", sni))
		};
		let cached_entry = match self.cache.lock() {
			Ok(x) => x.get(sni.deref()).map(|x|x.clone()),
			Err(_) => None
		};
		let cache_up_to_date = if let &Some(ref centry) = &cached_entry {
			centry.timestamp == lastmod
		} else {
			false
		};
		Ok(if !cache_up_to_date {
			let cached_entry = match refresh_cached_entry(&spath) {
				Ok(x) => x,
				Err(_) => return Err(cow!("Bad handler for SNI '{}'", sni))
			};
			match self.cache.lock() {
				Ok(mut x) => {x.insert(sni.to_owned(), cached_entry.clone());},
				Err(_) => ()
			};
			cached_entry
		} else {
			cached_entry.unwrap()	//This must be some(), because otherwise cache_up_to_date=false.
		})
	}
}

pub struct ConnectionService
{
	//Service Common
	s_split_fd: bool,				//The incoming&outgoing fds are different?
	s_launched: bool,				//service launched.
	soft_error: Option<Cow<'static, str>>,		//Need shutdown on error.
	delayed_socket: Option<String>,			//Socket initialization delayed.
	should_launch: bool,				//Flag that service should be launched.
	//Service stdin.
	token_s_stdin: AllocatedToken,			//Token for the service write (if not combined with read).
	token_s_stdin2: AllocatedToken,			//The original service write token.
	s_stdin: Option<FileDescriptor>,		//Service write side (outgoing).
	s_stdin_ready: bool,				//Service output (outgoing) ready?
	s_prefix: Option<(Vec<u8>, usize)>,		//The service prefix to send.
	s_in_eof_sent: bool,				//EOF has been sent to service.
	//Service stdout.
	token_s_stdout: AllocatedToken,			//Token for the service read.
	s_stdout: Option<FileDescriptor>,		//Service read side (incoming).
	s_stdout_ready: bool,				//Service input (incoming) ready?
	s_stdout_closed: bool,				//Service read has been EOF'd.
	//Service stderr.
	token_s_stderr: AllocatedToken,			//Token for the service error.
	s_stderr: Option<FileDescriptor>,		//Service error side (incoming).
	s_err_buf: Vec<u8>,				//Incomplete line from service error.
	s_err_overflowed: bool,				//s_err_buf has overflowed.
	//Global shared state.
	service_cache: ServiceCache,			//Cached service info.
	fd_count: Arc<AtomicUsize>,			//Fd count.
}

impl ConnectionService
{
	pub fn new(allocator: &mut TokenAllocatorPool, scache: ServiceCache,
		fd_count: Arc<AtomicUsize>) -> Result<ConnectionService, Cow<'static, str>>
	{
		let token_serv_in = allocator.allocate().map_err(|_|cow!("Can't allocate Tokens"))?;
		let token_serv_out = allocator.allocate().map_err(|_|cow!("Can't allocate Tokens"))?;
		let token_s_stderr = allocator.allocate().map_err(|_|cow!("Can't allocate Tokens"))?;
		Ok(ConnectionService {
			token_s_stdout: token_serv_in,
			token_s_stdin: token_serv_out.clone(),
			token_s_stdin2: token_serv_out,
			token_s_stderr: token_s_stderr,
			s_stdout: None,
			s_stdin: None,
			s_stderr: None,
			s_split_fd: false,
			s_stdin_ready: false,
			s_stdout_ready: false,
			soft_error: None,
			s_prefix: None,
			s_in_eof_sent: false,
			s_err_buf: Vec::new(),
			s_err_overflowed: false,
			s_launched: false,
			s_stdout_closed: false,
			delayed_socket: None,
			should_launch: false,
			service_cache: scache,
			fd_count: fd_count,
		})
	}
	//Read from service.
	pub fn read_from_service(&mut self, buf: &mut [u8]) -> Result<Option<usize>, Cow<'static, str>>
	{
		let mut srfd = if let &Some(ref fd) = &self.s_stdout { fd.clone() } else { return Ok(Some(0)); };
		//We clear s_stdin_ready so this gets re-registered.
		self.s_stdin_ready = false;
		let r = match srfd.read(buf) {
			Ok(x) => x,
			Err(x) => {
				if x.kind() == IoErrorKind::Interrupted { return Ok(Some(0)); }
				if x.kind() == IoErrorKind::WouldBlock { return Ok(Some(0)); }
				//Other errors are fatal.
				return Err(cow!("Service read error: {}", x));
			}
		};
		//If read produced 0, the service EOF'd, so we forward EOF over TLS.
		if r == 0 {
			self.s_stdout_closed = true;
			return Ok(None);
		}
		Ok(Some(r))
	}
	//No prefix buffer?
	pub fn prefix_sent(&self) -> bool
	{
		self.s_prefix.is_none()
	}
	//Wants write to service?
	pub fn wants_write_to_service(&self) -> bool
	{
		//Need stdin.
		if self.s_stdin.is_none() { return false; }
		//Not interested if pending shutdown.
		if self.pending_shutdown() { return false; }
		//If EOF has already been sent, don't process anymore.
		if self.s_in_eof_sent { return false; }
		//Otherwise, we can take write.
		true
	}
	//Write to service.
	pub fn write_to_service(&mut self, buf: &[u8]) -> Result<usize, Cow<'static, str>>
	{
		//The file descriptor to write to.
		let mut swfd = if let &Some(ref fd) = &self.s_stdin { fd.clone() } else { return Ok(0); };
		let ret = {
			//If there is still prefix, drain it.
			let buf = if let &Some((ref x, c)) = &self.s_prefix { &x[c..] } else { buf };
			swfd.write(&buf)
		};
		//Handle errors and see how much we got.
		let amount = match ret {
			Ok(x) => x,
			Err(x) => {
				if x.kind() == IoErrorKind::Interrupted { return Ok(0); }
				if x.kind() == IoErrorKind::WouldBlock { return Ok(0); }
				if x.kind() == IoErrorKind::BrokenPipe {
					//This is special.
					self.close_link_to_service()?;
					return Ok(0);
				}
				//Other errors are fatal.
				return Err(cow!("Service write error: {}", x));
			}
		};
		//Record advance in prefix buffer and dump it if needed.
		let dump_prefix = if let &mut Some((ref x, ref mut c)) = &mut self.s_prefix {
			//This is at most buffer length, since amount is at most buffer length - c.
			*c += amount;
			*c >= x.len()
		} else {
			//Accept as much as was written.
			return Ok(amount);
		};
		//Delete prefix buffer if we reached end of it. And in any case, don't accept any input.
		if dump_prefix { self.s_prefix = None; }
		Ok(0)
	}
	//Set soft error.
	pub fn set_error(&mut self, err: Cow<'static, str>)
	{
		self.soft_error = Some(err)
	}
	//Get soft error (assumes pending_shutdown()=true).
	pub fn get_error(&self) -> Cow<'static, str>
	{
		match &self.soft_error {
			&Some(Cow::Owned(ref x)) => Cow::Owned(x.clone()),
			&Some(Cow::Borrowed(x)) => Cow::Borrowed(x),
			&None => Cow::Borrowed("Unknown error???"),
		}
	}
	//Get rendezvous requests: Write, force-Write and Read.
	pub fn rendezvous_request(&self) -> (bool, bool, bool)
	{
		(self.s_stdout_ready, self.s_prefix.is_some() && self.s_stdout_ready, self.s_stdin_ready)
	}
	//Is pending shutdown?
	pub fn pending_shutdown(&self) -> bool
	{
		self.soft_error.is_some()
	}
	//Should launch?
	pub fn should_launch(&self) -> bool
	{
		self.s_stdout.is_none() && self.should_launch && !self.s_launched
	}
	//Signal launch.
	pub fn signal_launch(&mut self)
	{
		self.should_launch = true;
	}
	//Handle event.
	pub fn handle_event(&mut self, poll: &mut Poll, tok: usize, kind: &Ready, cnum: u64)
	{
		let mut maybe_delayed_init = false;
		if self.token_s_stdout.value() == tok && self.delayed_socket.is_some() {
			//We are handling delayed socket initialization, and just got notification that
			//the socket should be ready. Fetch the SO_ERROR to determine the status.
			self.handle_delayed_socket(cnum);
			maybe_delayed_init = true;
		}
		if self.token_s_stdout.value() == tok && (kind.is_readable() || UnixReady::from(*kind).is_hup()) &&
			!self.s_stdout_closed && !maybe_delayed_init {
			//Service read event.
			self.s_stdin_ready = true;
		}
		if self.token_s_stdin.value() == tok && kind.is_writable() {
			//Service write event.
			self.s_stdout_ready = true;
		}
		if self.token_s_stderr.value() == tok && (kind.is_readable() || UnixReady::from(*kind).is_hup()) {
			self.handle_service_error_event(poll, cnum);
		}
	}
	//This connection is going to be destroyed NOW. No more data can be sent or received.
	pub fn destroy_notify(&mut self)
	{
		//Close the file descriptors for task. These all are garbage-collected.
		self.s_stdout = None;
		self.s_stdin = None;
		self.s_stderr = None;
	}
	//Re-arm the service polls, as appropriate.
	pub fn retrigger_service(&mut self, poll: &mut Poll)
	{
		//We don't process the service at all on pending shutdown.
		if self.soft_error.is_some() { return; }
		//Retrigger service polls. Be careful that the service descriptors can be the same or not.
		let r1 = if let &Some(ref rd) = &self.s_stdout {
			let mut ready = Ready::empty();
			if !self.s_stdin_ready && !self.s_stdout_closed { ready = ready | Ready::readable(); }
			if !self.s_split_fd && !self.s_stdout_ready { ready = ready | Ready::writable(); }
			if ready.is_empty() { None } else { Some((rd.clone(), self.token_s_stdout.value(), ready)) }
		} else {
			None
		};
		let r2 = if let (true, &Some(ref wd)) = (self.s_split_fd, &self.s_stdin) {
			let mut ready = Ready::empty();
			if !self.s_stdout_ready { ready = ready | Ready::writable(); }
			if ready.is_empty() { None } else { Some((wd.clone(), self.token_s_stdin.value(), ready)) }
		} else {
			None
		};
		if let Some((fd, token, ready)) = r1 {
			match poll.reregister(&fd, Token(token), ready, PollOpt::oneshot()) {
				Ok(_) => (),
				Err(x) => store_err!(self.soft_error, "poll.reregister({:?}) error: {}", fd,
					x)
			};
		}
		if let Some((fd, token, ready)) = r2 {
			match poll.reregister(&fd, Token(token), ready, PollOpt::oneshot()) {
				Ok(_) => (),
				Err(x) => store_err!(self.soft_error, "poll.reregister({:?}) error: {}", fd,
					x)
			};
		}
	}
	//Handle a delayed socket initialization.
	fn handle_delayed_socket(&mut self, cnum: u64)
	{
		//In case this fails in non-fatal way, service retriggers re-arms the poll.
		let serv_stdout = if let &Some(ref x) = &self.s_stdout { x.clone() }  else { return; };
		match serv_stdout.so_error() {
			Ok(Ok(_)) => {
				writeln!(stderr(), "[#{}] Connection to `{}` established.", cnum,
					self.delayed_socket.clone().unwrap()).unwrap();
				self.delayed_socket = None;		//Connected!
				return;
			}
			Ok(Err(x)) => {
				if x.kind() == IoErrorKind::Interrupted || x.kind() == IoErrorKind::WouldBlock {
					return;
				}
				store_err!(RET self.soft_error, "Error connecting to TCP socket `{}`: {}",
					self.delayed_socket.clone().unwrap(), x)
			},
			Err(x) => {
				if x.kind() == IoErrorKind::Interrupted || x.kind() == IoErrorKind::WouldBlock {
					return;
				}
				store_err!(RET self.soft_error, "Error reading SO_ERROR: {}", x);
			}
		}
	}
	//Handle line fragment from stderr.
	fn handle_stderr_frag(&mut self, frag: &[u8])
	{
		const MAX_ERROR_LINE: usize = 512;
		if self.s_err_overflowed { return; }
		let maxfrag = MAX_ERROR_LINE - self.s_err_buf.len();
		self.s_err_overflowed = maxfrag < frag.len();
		let frag = &frag[..min(frag.len(), maxfrag)];
		self.s_err_buf.extend_from_slice(frag);
	}
	//Handle event on service error.
	fn handle_service_error_event(&mut self, poll: &mut Poll, cnum: u64)
	{
		//This is handled even if stream is in error.
		let mut sefd = if let &Some(ref fd) = &self.s_stderr { fd.clone() } else { return; };
		let mut buf = [0u8; 8192];
		let r = match sefd.read(&mut buf) {
			Ok(0) => {
				//EOF from error.
				self.s_stderr = None;		//Garbage-collect.
				return;
			}
			Ok(x) => x,
			Err(x) => {
				let mut not_fatal = false;
				not_fatal |= x.kind() == IoErrorKind::Interrupted;
				not_fatal |= x.kind() == IoErrorKind::WouldBlock;
				if not_fatal {
					//Treat this as EOF.
					self.s_stderr = None;	//Garbage-collect.
					return;
				}
				0
			}
		};
		let mut buf = &buf[..r];
		while buf.len() > 0 {
			//If there is a 10 in buffer, split at that position, concatenating with s_err_buf.
			//Otherwise, throw the rest into s_err_buf.
			let mut lfpos = None;
			for i in buf.iter().enumerate() {
				if *i.1 == 10 {
					lfpos = Some(i.0);
					break;
				}
			}
			if let Some(x) = lfpos {
				let line_remainder = &buf[..x];
				self.handle_stderr_frag(line_remainder);
				{
					let s = String::from_utf8_lossy(&self.s_err_buf);
					writeln!(stderr(), "[#{}]: {}", cnum, s).unwrap();
				}
				self.s_err_overflowed = false;
				self.s_err_buf.clear();
				buf = &buf[(r + 1)..];
			} else {
				self.handle_stderr_frag(buf);
				buf = &[];
			}
		}
		//Retrigger the poll.
		match poll.reregister(&sefd, Token(self.token_s_stderr.value()), Ready::readable(),
			PollOpt::oneshot()) {
			Ok(_) => (),
			Err(x) => store_err!(self.soft_error, "poll.reregister error: {}", x)
		};
	}
	pub fn close_link_to_service(&mut self) -> Result<(), Cow<'static, str>>
	{
		let mut ret = Ok(());
		if self.s_in_eof_sent { return Ok(()); }	//Don't do this twice.
		if self.s_stdout_closed {
			//Connection to service lost: We have lost service stdout before, and now lost stdin.
			ret = Err(cow!("Connection to service lost"));
		}
		//If service hasn't closed its stdout yet and we have non-split fd (i.e. a socket), then shutdown
		//writes to cause EOF.
		if !self.s_split_fd && !self.s_stdout_closed {
			if let &Some(ref x) = &self.s_stdin { x.shutdown_write(); }
		}
		self.s_stdin = None;		//Garbage-collects if needed.
		self.s_stdout_ready = true;	//Set this to quench polling.
		self.s_in_eof_sent = true;
		ret
	}
	//Launch the service.
	pub fn launch_service(&mut self, poll: &mut Poll, info: ConnectionInfo, cnum: u64, tlsver: u8,
		sni: Arc<String>, alpn: Option<Arc<String>>, conninfo: &str)
	{
		self.s_launched = true;
		let cached_entry = match self.service_cache.lookup_by_sni(&sni) {
			Ok(x) => x,
			Err(x) => {
				self.soft_error = Some(x);
				return;
			}
		};
		if let &Service::Bad = &cached_entry.data {
			//Do not print message on these bad entries.
		} else {
			writeln!(stderr(), "[#{}] {}: Using {}", cnum, &sni, conninfo).unwrap();
		}
		match &cached_entry.data {
			&Service::Bad => store_err!(RET self.soft_error, "Bad handler for SNI '{}'", sni),
			&Service::Socket(ref x, version) => self.launch_service_socket(poll, x, version, info,
				tlsver, sni, alpn),
			&Service::Tcp(ref x, version) => self.launch_service_tcp(poll, x, version, info,
				tlsver, cnum, sni, alpn),
			&Service::Command(ref x) => self.launch_service_command(poll, x, info, tlsver, sni, alpn),
		};
	}
	//Launch a socket (PROXY) service.
	fn launch_service_socket(&mut self, poll: &mut Poll, name: &str, version: u32, info: ConnectionInfo,
		tlsver: u8, sni: Arc<String>, alpn: Option<Arc<String>>)
	{
		let stream = match UnixStream::connect(name) {
			Ok(x) => x,
			Err(x) => store_err!(RET self.soft_error, "Failed to connect to socket `{}`: {}",
				name, x)
		};
		let fd_count2 = self.fd_count.clone();
		self.launch_service_socket_fd(poll, FileDescriptor::new(stream.into_raw_fd(), fd_count2),
			version, info, tlsver, name, false, sni, alpn);
	}
	//Launch a TCP socket (PROXY) service.
	fn launch_service_tcp(&mut self, poll: &mut Poll, addr: &SocketAddr, version: u32, info: ConnectionInfo,
		tlsver: u8, cnum: u64, sni: Arc<String>, alpn: Option<Arc<String>>)
	{
		let sname = format!("{}", addr);
		//Create socket fd.
		let af = match addr {
			&SocketAddr::V4(_) => AF_INET,
			&SocketAddr::V6(_) => AF_INET6,
		};
		let fd = match FileDescriptor::socket(af, SOCK_STREAM, 0, self.fd_count.clone()) {
			Ok(x) => x,
			Err(x) => store_err!(RET self.soft_error, "Failed to create socket `{}`: {}",
				sname, x)
		};
		//Turn on nonblock.
		match fd.set_nonblock() {
			Ok(_) => (),
			Err(x) => store_err!(RET self.soft_error, "Failed to set nonblocking mode for `{}`: {}",
				sname, x)
		}
		let delayed = match fd.connect(addr) {
			Ok(true) => false,
			Ok(false) => true,
			Err(x) => store_err!(RET self.soft_error, "Failed to connect to `{}`: {}", sname, x)
		};
		if delayed {
			writeln!(stderr(), "[#{}] Connection to `{}` in progress...", cnum, sname).unwrap();
		}
		self.launch_service_socket_fd(poll, fd, version, info, tlsver, &sname, delayed, sni, alpn);
	}
	fn launch_service_socket_fd(&mut self, poll: &mut Poll, fd: FileDescriptor, version: u32,
		info: ConnectionInfo, tlsver: u8, sname: &str, delayed: bool, sni: Arc<String>,
			alpn: Option<Arc<String>>)
	{
		let tmp;
		let tmp2;
		let proxy_header = match version {
			0 => &[],
			1 => {
				tmp = format_proxyv1_header(&info);
				tmp.as_bytes()
			},
			2 => {
				tmp2 = format_proxyv2_header(&info, alpn, sni, tlsver);
				&tmp2[..]
			},
			x => store_err!(RET self.soft_error, "Illegal proxy version {} on socket `{}`", x,
				sname)
		};
		let fd1 = fd.clone();
		let fd2 = fd;
		self.launch_service_finish(poll, fd1, fd2, proxy_header, if delayed { Some(sname.to_owned())
			} else { None });
	}
	//Launch a command service.
	fn launch_service_command(&mut self, poll: &mut Poll, name: &[String], info: ConnectionInfo, tlsver: u8,
		sni: Arc<String>, alpn: Option<Arc<String>>)
	{
		let srcaddr = format_ipv6_address(info.src_addr);
		let dstaddr = format_ipv6_address(info.src_addr);
		let srcport = format!("{}", info.src_port);
		let dstport = format!("{}", info.dst_port);
		let tlsver = format!("{}", tlsver);
		if name.len() == 0 {
			store_err!(RET self.soft_error, "Empty command specified");
		}
		let mut cmd = Command::new(&name[0]);
		for i in name.iter().skip(1) { cmd.arg(i); }
		cmd.stdin(Stdio::piped());
		cmd.stdout(Stdio::piped());
		cmd.stderr(Stdio::piped());
		cmd.env("BTLS_INETD_SRC_ADDR", String::from_utf8_lossy(&srcaddr).deref());
		cmd.env("BTLS_INETD_DST_ADDR", String::from_utf8_lossy(&dstaddr).deref());
		cmd.env("BTLS_INETD_SRC_PORT", &srcport);
		cmd.env("BTLS_INETD_DST_PORT", &dstport);
		cmd.env("BTLS_INETD_TLS_VERSION", &tlsver);
		cmd.env("BTLS_INETD_SNI", sni.deref());
		if let Some(alpn) = alpn {
			cmd.env("BTLS_INETD_ALPN", alpn.deref());
		}
		let mut handle = match cmd.spawn() {
			Ok(x) => x,
			Err(x) => store_err!(RET self.soft_error, "Failed to execute command: {}", x)
		};
		let mut stdin = None;
		let mut stdout = None;
		let mut stderr = None;
		swap(&mut stdin, &mut handle.stdin);
		swap(&mut stdout, &mut handle.stdout);
		swap(&mut stderr, &mut handle.stderr);
		let (stdin, stdout, stderr) = match (stdin, stdout, stderr) {
			(Some(x), Some(y), Some(z)) => (x, y, z),
			_ => store_err!(RET self.soft_error, "Failed to get child file descriptors")
		};
		let w_stdin = FileDescriptor::new(stdin.into_raw_fd(), self.fd_count.clone());
		let w_stdout = FileDescriptor::new(stdout.into_raw_fd(), self.fd_count.clone());
		let w_stderr = FileDescriptor::new(stderr.into_raw_fd(), self.fd_count.clone());
		self.launch_service_finish(poll, w_stdout, w_stdin, &[], None);
		if self.soft_error.is_some() { return; }
		self.s_stderr = Some(w_stderr);
		match poll.register(&self.s_stderr.clone().unwrap(), Token(self.token_s_stderr.value()),
			Ready::readable(), PollOpt::oneshot()) {
			Ok(_) => (),
			Err(x) => store_err!(self.soft_error, "poll.register error: {}", x)
		};
	}
	//This assumes that if delayed=true, rfd=wfd.
	fn launch_service_finish(&mut self, poll: &mut Poll, rfd: FileDescriptor, wfd: FileDescriptor,
		initial_data: &[u8], delayed: Option<String>)
	{
		//We don't process the service at all on pending shutdown.
		if self.soft_error.is_some() { return; }
		//We need to register the file descriptors. Take note if the fds are the same or not.
		//Also, write possible initial data into unsent data buffer, so we prepend it to material sent.
		self.s_stdout = Some(rfd.clone());
		self.s_stdin = Some(wfd.clone());
		if initial_data.len() > 0 { self.s_prefix = Some((initial_data.to_owned(), 0)); }
		self.s_split_fd = rfd != wfd;
		self.delayed_socket = delayed;
		//If not split, the read/write share tokens.
		if !self.s_split_fd { self.token_s_stdin = self.token_s_stdout.clone(); }

		//The initial state is always waiting for write, and waiting for read if t_out_ready is asserted.
		//Note that any errors here trigger pending error instead of immediate error, as they are
		//interpretted as service errors.
		let r1 = {
			let mut ready = Ready::empty();
			//Do this unconditionally, we must register the fd.
			ready = ready | Ready::readable();
			if !self.s_split_fd && self.delayed_socket.is_none() {
				ready = ready | Ready::writable();
			}
			if ready.is_empty() { None } else { Some((rfd, self.token_s_stdout.value(), ready)) }
		};
		let r2 = if self.s_split_fd {
			Some((wfd, self.token_s_stdin.value(), Ready::writable()))
		} else {
			None
		};
		if let Some((fd, token, ready)) = r1 {
			match poll.register(&fd, Token(token), ready, PollOpt::oneshot()) {
				Ok(_) => (),
				Err(x) => store_err!(RET self.soft_error, "poll.register error: {}", x)
			};
		}
		if let Some((fd, token, ready)) = r2 {
			match poll.register(&fd, Token(token), ready, PollOpt::oneshot()) {
				Ok(_) => (),
				Err(x) => store_err!(RET self.soft_error, "poll.register error: {}", x)
			};
		}
	}
	pub fn add_tokens(&self, tokens: &mut BTreeMap<usize, u64>, next_seq: u64)
	{
		tokens.insert(self.token_s_stdout.value(), next_seq);
		tokens.insert(self.token_s_stdin2.value(), next_seq);
		tokens.insert(self.token_s_stderr.value(), next_seq);
	}
	pub fn remove_tokens_and_pollers(&self, tokens: &mut BTreeMap<usize, u64>, poll: &mut Poll)
	{
		tokens.remove(&self.token_s_stdout.value());
		tokens.remove(&self.token_s_stdin2.value());	//Always remove original.
		tokens.remove(&self.token_s_stderr.value());
		if let &Some(ref x) = &self.s_stdout {
			let _ = poll.deregister(x);
		}
		if self.s_stdin != self.s_stdout {
			if let &Some(ref x) = &self.s_stdin {
				let _ = poll.deregister(x);
			}
		}
	}
}
