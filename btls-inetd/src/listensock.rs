use std::str::FromStr;
use net2::TcpBuilder;
use mio::tcp::{TcpListener};
use std::net::{Ipv4Addr, Ipv6Addr, SocketAddr, ToSocketAddrs};

fn make_socket_common(sock: TcpBuilder, addr: SocketAddr) -> Result<TcpListener, String>
{
	//Enable V6only if supported.
	let _ = sock.only_v6(true);
	let sock = match sock.reuse_address(true) {
		Ok(x) => x,
		Err(x) => return Err(format!("Error setting reuse for port: {}", x))
	};
	match sock.bind(addr) {
		Ok(x) => x,
		Err(x) => return Err(format!("Error binding to `{}`: {}", addr, x))
	};
	let sock = match sock.listen(10) {
		Ok(x) => x,
		Err(x) => return Err(format!("Error listening: {}", x))
	};
	let addr = match sock.local_addr() {
		Ok(x) => x,
		Err(x) => return Err(format!("Error getting address: {}", x))
	};
	let sock = match TcpListener::from_listener(sock, &addr) {
		Ok(x) => x,
		Err(x) => return Err(format!("Unable to wrap listener: {}", x))
	};
	Ok(sock)
}

fn make_socket_wild(port: u16, ipv6: bool) -> Result<TcpListener, String>
{
	let (sock, sockaddr) = if ipv6 {
		let unspec = Ipv6Addr::new(0, 0, 0, 0, 0, 0, 0, 0);
		let sock = match TcpBuilder::new_v6() {
			Ok(x) => x,
			Err(x) => return Err(format!("Error creating socket: {}", x))
		};
		let sockaddr = match (unspec, port).to_socket_addrs().map(|mut x|x.next()) {
			Ok(Some(x)) => x,
			_ => return Err(format!("Internal error assembling addr&port pair"))
		};
		(sock, sockaddr)
	} else {
		let unspec = Ipv4Addr::new(0, 0, 0, 0);
		let sock = match TcpBuilder::new_v4() {
			Ok(x) => x,
			Err(x) => return Err(format!("Error creating socket: {}", x))
		};
		let sockaddr = match (unspec, port).to_socket_addrs().map(|mut x|x.next()) {
			Ok(Some(x)) => x,
			_ => return Err(format!("Internal error assembling addr&port pair"))
		};
		(sock, sockaddr)
	};
	match make_socket_common(sock, sockaddr) {
		Ok(x) => Ok(x),
		Err(x) => Err(format!("Listen '*:{}': {}", port, x))
	}
}

fn make_socket_spec(addr: SocketAddr, spec: &str) -> Result<TcpListener, String>
{
	if let Some(p) = match addr {
		SocketAddr::V4(x) => if x.ip().is_unspecified() { Some(x.port()) } else { None },
		SocketAddr::V6(x) => if x.ip().is_unspecified() { Some(x.port()) } else { None },
	} {
		return Err(format!("Can't use unspecified IP address, use '*:{}' instead", p))
	}
	let sock = match match addr {
		SocketAddr::V4(_) => TcpBuilder::new_v4(),
		SocketAddr::V6(_) => TcpBuilder::new_v6()
	} {
		Ok(x) => x,
		Err(x) => return Err(format!("Error creating socket: {}", x))
	};
	match make_socket_common(sock, addr) {
		Ok(x) => Ok(x),
		Err(x) => Err(format!("Listen '{}': {}", spec, x))
	}
}


pub fn make_socket(spec: &str) -> Result<Vec<TcpListener>, String>
{
	let last_colon = match spec.rfind(':') {
		Some(x) => x,
		None => return Err(format!("Expected at least one ':' in listener address specification '{}' \
			(did you mean '*:{}'?)", spec, spec)),
	};
	let addr = &spec[..last_colon];
	let port = &spec[last_colon+1..];
	let portnum = match u16::from_str(port) {
		Ok(x) => x,
		Err(_) => return Err(format!("Invalid port number '{}' in listener address specification", port)),
	};

	if addr == "*" {
		let mut ret = Vec::new();
		//Some OSes (especially *BSD) doesn't support V6ONLY, so we need to complicate this code.
		ret.push(try!(make_socket_wild(portnum, true)));
		ret.push(try!(make_socket_wild(portnum, false)));
		Ok(ret)
	} else {
		let mut ret = Vec::new();
		let saddr = match spec.to_socket_addrs() {
			Ok(x) => x,
			Err(x) => return Err(format!("Error parsing listener specification '{}': {}", spec, x))
		};
		for i in saddr {
			ret.push(try!(make_socket_spec(i, spec)));
		}
		if ret.len() == 0 {
			return Err(format!("Listener address specification '{}' does not match any addresses",
				spec))
		}
		Ok(ret)
	}
}
