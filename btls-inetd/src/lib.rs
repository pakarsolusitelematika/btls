extern crate libc;
extern crate mio;
extern crate net2;
use std::sync::Arc;
use std::sync::atomic::{AtomicUsize, Ordering as MemOrder};

pub mod token;
pub mod listener;
pub mod file_descriptor;
pub mod listensock;
pub mod service;

pub fn decrement_count(c: &Arc<AtomicUsize>)
{
	c.store(c.load(MemOrder::Relaxed)-1, MemOrder::Relaxed);
}

pub fn increment_count(c: &Arc<AtomicUsize>)
{
	c.store(c.load(MemOrder::Relaxed)+1, MemOrder::Relaxed);
}
