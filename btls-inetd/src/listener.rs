use mio::{Evented, Poll, PollOpt, Ready, Token};
use mio::tcp::{TcpListener, TcpStream};
use std::net::{Ipv4Addr, SocketAddr, SocketAddrV4};
use std::io::{Result as IoResult};
use std::os::unix::io::{IntoRawFd};
use std::sync::Arc;
use std::sync::atomic::AtomicUsize;
use super::file_descriptor::FileDescriptor;

pub enum ConnectionKind
{
	Tcp(TcpStream),
	Fd(FileDescriptor),
}

impl ConnectionKind
{
	pub fn local_addr(&self) -> IoResult<SocketAddr>
	{
		match self {
			&ConnectionKind::Tcp(ref x) => x.local_addr(),
			&ConnectionKind::Fd(ref x) => match x.local_addr() {
				Ok(Some(x)) => Ok(x),
				Ok(None) => Ok(SocketAddr::V4(SocketAddrV4::new(Ipv4Addr::new(0, 0, 0, 0), 0))),
				Err(x) => Err(x)
			},
		}
	}
	pub fn into_raw_fd(self, count: Arc<AtomicUsize>) -> FileDescriptor
	{
		match self {
			ConnectionKind::Tcp(x) => FileDescriptor::new(x.into_raw_fd(), count),
			ConnectionKind::Fd(x) => x,
		}
	}
}

pub enum ListenerKind
{
	Tcp(TcpListener),
	Fd(FileDescriptor),
}

impl ListenerKind
{
	pub fn new_fd(fd: FileDescriptor) -> ListenerKind
	{
		ListenerKind::Fd(fd)
	}
	pub fn new_tcp(l: TcpListener) -> ListenerKind
	{
		ListenerKind::Tcp(l)
	}
	pub fn accept(&self) -> IoResult<(ConnectionKind, SocketAddr)>
	{
		match self {
			&ListenerKind::Tcp(ref x) => {
				let (a, b) = x.accept()?;
				Ok((ConnectionKind::Tcp(a), b))
			},
			&ListenerKind::Fd(ref x) => {
				let (a, b) = x.accept()?;
				let b = match b {
					Some(x) => x,
					None => SocketAddr::V4(SocketAddrV4::new(Ipv4Addr::new(0, 0, 0, 0), 0))
				};
				Ok((ConnectionKind::Fd(a), b))
			}
		}
	}
}


impl Evented for ListenerKind
{
	fn register(&self, poll: &Poll, token: Token, interest: Ready, opts: PollOpt) -> IoResult<()>
	{
		match self {
			&ListenerKind::Tcp(ref x) => x.register(poll, token, interest, opts),
			&ListenerKind::Fd(ref x) => x.register(poll, token, interest, opts),
		}
	}
	fn reregister(&self, poll: &Poll, token: Token, interest: Ready, opts: PollOpt) -> IoResult<()>
	{
		match self {
			&ListenerKind::Tcp(ref x) => x.reregister(poll, token, interest, opts),
			&ListenerKind::Fd(ref x) => x.reregister(poll, token, interest, opts),
		}
	}
	fn deregister(&self, poll: &Poll) -> IoResult<()>
	{
		match self {
			&ListenerKind::Tcp(ref x) => x.deregister(poll),
			&ListenerKind::Fd(ref x) => x.deregister(poll),
		}
	}
}
