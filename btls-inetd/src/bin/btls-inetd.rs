//WARNING: We are probably running as ROOT and the subprocesses are not. Anyhing received from subprocesses
//should be treated with EXTREME paranoia.
extern crate libc;
extern crate mio;
extern crate btls_inetd;
use libc::{pid_t, fork, getuid, setuid, setgid, setgroups, chdir, getpwnam, getpid,
	gid_t, setsid, O_RDWR, open, poll, pollfd, POLLIN, POLLHUP, LOG_DAEMON, LOG_NDELAY, LOG_PID, openlog,
	syslog, LOG_ALERT, LOG_ERR, LOG_NOTICE, WTERMSIG, WEXITSTATUS, WIFEXITED, WIFSIGNALED, WNOHANG,
	c_int, waitpid, c_char, execv, SIGCHLD, SIGHUP, SIGUSR1, sigaction, SA_SIGINFO, c_void, kill, ioctl,
	FIONREAD};
use mio::{Events, Poll, PollOpt, Ready, Token};
use std::fs::File;
use std::process::exit;
use std::mem::forget;
use std::mem::drop;
use std::mem::zeroed;
use std::io::{Result as IoResult, Write as IoWrite, Error as IoError, Read as IoRead, ErrorKind as IoErrorKind};
use std::io::stderr;
use std::env::var;
use std::ffi::CString;
use std::env::args_os;
use std::path::Path;
use std::sync::Arc;
use std::thread::sleep;
use std::time::Duration;
use std::ops::Deref;
use std::sync::atomic::{AtomicUsize, AtomicBool, Ordering as MemOrder};
use std::str::FromStr;
use std::fmt::{Arguments, Write as FmtWrite};
use std::os::unix::io::{IntoRawFd, RawFd};
use btls_inetd::file_descriptor::FileDescriptor;
use btls_inetd::listensock::{make_socket};

static mut GLOBAL_SIGHUP_FLAG: *const AtomicBool = 0 as *const AtomicBool;
static mut GLOBAL_SIGUSR1_FLAG: *const AtomicBool = 0 as *const AtomicBool;

macro_rules! abort {
	($x:expr) => {{
		writeln!(stderr(), $x).unwrap();
		exit(1);
	}};
	($x:expr, $($y:expr),*) => {{
		writeln!(stderr(), $x, $($y),*).unwrap();
		exit(1);
	}};
}

macro_rules! abort_syserr {
	($x:expr) => {{
		let err = IoError::last_os_error();
		writeln!(stderr(), $x, err).unwrap();
		exit(1);
	}};
	($x:expr, $($y:expr),*) => {{
		let err = IoError::last_os_error();
		writeln!(stderr(), $x, $($y),*, err).unwrap();
		exit(1);
	}};
}


fn assume_user(username: &str)
{
	let dummy_arr: [gid_t;1] = [0];
	let _username = match CString::new(username) {
		Ok(x) => x,
		Err(_) => abort!("Specified --user is invalid"),
	};
	let pwent = unsafe{getpwnam(_username.as_ptr())};
	if pwent.is_null() { abort!("Specified --user does not exist in the user database"); }
	let uid = unsafe{(*pwent).pw_uid};
	let gid = unsafe{(*pwent).pw_gid};
	match  unsafe{chdir((*pwent).pw_dir)} {
		x if x >= 0 => (),
		_ => abort_syserr!("Can't chdir to home directory of user `{}`: {}", username)
	};
	match unsafe{setgroups(0, dummy_arr.as_ptr())} {
		x if x >= 0 => (),
		_ => abort_syserr!("Can't drop supplementary groups: {}")
	};
	match unsafe{setgid(gid)} {
		x if x >= 0 => (),
		_ => abort_syserr!("Can't change group: {}")
	};
	match unsafe{setuid(uid)} {
		x if x >= 0 => (),
		_ => abort_syserr!("Can't change user: {}")
	};
	writeln!(stderr(), "Assumed user {} (uid={}, gid={})", username, uid, gid).unwrap();
}


fn absolute_path(p: &str) -> IoResult<String>
{
	match Path::new(p).canonicalize()?.to_str() {
		Some(x) => Ok(x.to_owned()),
		None => return Err(IoError::new(IoErrorKind::Other, "Path is not valid unicode")),
	}
}

struct Config
{
	executable: String,
	username: Option<String>,
	config: Option<String>,
	services: String,
	certificates: String,
	processes: usize,
	listen_spec: Vec<String>,
	pidfile: Option<String>,
	no_detach: bool,
}

impl Config
{
	fn parse() -> Config
	{
		let mut executable: Option<String> = None;
		let mut username: Option<String> = None;
		let mut config: Option<String> = None;
		let mut services: Option<String> = None;
		let mut certificates: Option<String> = None;
		let mut pidfile: Option<String> = None;
		let mut processes = 8usize;
		let mut listen_spec: Vec<String> = Vec::new();
		let mut no_detach = false;
		let mut dummy = true;
		for i in args_os() {
			if dummy { dummy = false; continue; }
			let xstr = match i.clone().into_string() {
				Ok(x) => x,
				Err(x) => abort!("Invalid argument: `{}`", x.to_string_lossy())
			};
			if executable.is_none() {
				executable = Some(xstr.to_owned());
				continue;
			}
			if xstr == "--no-detach" { no_detach = true; }
			if xstr.starts_with("--user=") {username = Some((&xstr[7..]).to_owned()); }
			if xstr.starts_with("--config=") {config = Some((&xstr[9..]).to_owned()); }
			if xstr.starts_with("--services=") {services = Some((&xstr[11..]).to_owned()); }
			if xstr.starts_with("--listen=") {listen_spec.push((&xstr[9..]).to_owned()); }
			if xstr.starts_with("--certificates=") {certificates = Some((&xstr[15..]).to_owned()); }
			if xstr.starts_with("--pidfile=") {pidfile = Some((&xstr[10..]).to_owned()); }
			if xstr.starts_with("--processes=") {
				processes = {
					match usize::from_str(&xstr[12..]) {
						Ok(x) if x < 1 => abort!("--processes less than 1"),
						Ok(x) => x,
						Err(_) => abort!("Can't parse --processes=<count>"),
					}
				};
			}
		}
		let executable = if let Some(executable) = executable { executable } else {
			abort!("Need <executable-path>");
		};
		let services = if let Some(services) = services { services } else {
			abort!("Need --services=<path>");
		};
		let certificates = if let Some(certificates) = certificates { certificates } else {
			abort!("Need --certificates=<path>");
		};
		//Make executable, services and certificates absolute, since the daemonization will chdir
		//to /.
		let executable = match absolute_path(&executable) {
			Ok(x) => x,
			Err(x) => abort!("Can't make executable path absolute: {}", x)
		};
		let services = match absolute_path(&services) {
			Ok(x) => x,
			Err(x) => abort!("Can't make services path absolute: {}", x)
		};
		let certificates = match absolute_path(&certificates) {
			Ok(x) => x,
			Err(x) => abort!("Can't make certificates path absolute: {}", x)
		};
		Config {
			executable: executable,
			username: username,
			config: config,
			services: services,
			certificates: certificates,
			processes: processes,
			listen_spec: listen_spec,
			pidfile: pidfile,
			no_detach: no_detach,
		}
	}
}

fn drop_privileges(username: &Option<String>)
{
	//Then, we drop privileges. We WILL NOT run as root.
	let is_root = unsafe{getuid()} <= 0;
	if let &Some(ref uname) = username {
		if is_root {
			assume_user(uname);
		} else {
			abort!("Can't use --user=<name> without root privileges");
		}
	} else {
		if is_root { abort!("--user=<name> must be specified if running with root privileges"); }
	}
}

fn add_raw_fd_listener(fd_set: &mut Vec<FileDescriptor>, fd: RawFd, fdcount: &Arc<AtomicUsize>)
{
	let fd = FileDescriptor::new(fd, fdcount.clone());
	match fd.set_close_exec(false) {	//These need to be inheritable.
		Ok(_) => (),
		Err(x) => abort!("Error: Can't clear O_CLOEXEC: {}", x)
	};
	fd_set.push(fd);
}

fn make_listeners(specs: &[String]) -> Vec<FileDescriptor>
{
	let dummy = Arc::new(AtomicUsize::new(0));
	let mut fd_set = Vec::new();
	//Systemd support.
	let systemd_listen_pid = var("LISTEN_PID").ok();
	let systemd_listen_fds = var("LISTEN_FDS").ok();
	if systemd_listen_pid.and_then(|x|i32::from_str(&x).ok()) == Some(unsafe{getpid()}) {
		//These are for us.
		if let Some(fdcount) = systemd_listen_fds.and_then(|x|u32::from_str(&x).ok()) {
			for i in 0..fdcount {
				//Systemd maps listener sockets starting from 3.
				add_raw_fd_listener(&mut fd_set, (i + 3) as RawFd, &dummy);
			}
		}
	}
	for i in specs.iter()
	{
		match make_socket(i) {
			Ok(x) => for i in x { add_raw_fd_listener(&mut fd_set, i.into_raw_fd(), &dummy) },
			Err(x) => {
				writeln!(stderr(), "Warning: Bad listener `{}`: {}", i, x).unwrap();
				continue;
			}
		};
	}
	if fd_set.len() == 0 { abort!("Error: No ports to listen"); }
	fd_set
}

fn parent_process(mut cp_read: FileDescriptor, mut start_read: FileDescriptor, mut stderr_fd: FileDescriptor)
{
	//We are the parent.
	//Copy stuff from cp_read to stderr, until receiving ACK or failure from start_read.
	let mut successful = false;
	let mut bad = false;
	let mut start_read_eof = false;
	let mut cp_read_eof = false;
	loop {
		let mut fdset: [pollfd;2] = [pollfd{
			fd: 0,
			events: POLLIN,
			revents: 0
		}, pollfd{
			fd: 0,
			events: POLLIN,
			revents: 0
		}];
		let mut idx = 0;
		if !cp_read_eof {
			fdset[idx as usize].fd = cp_read.as_raw_fd();
			idx += 1;
		}
		if !start_read_eof {
			fdset[idx as usize].fd = start_read.as_raw_fd();
			idx += 1;
		}
		unsafe{poll(fdset.as_mut_ptr(), idx, -1)};
		if !cp_read_eof && fdset[0].revents & (POLLIN|POLLHUP) != 0 {
			//cp_read provodes data, copy it to stderr.
			let mut buf = [0u8; 512];
			match cp_read.read(&mut buf) {
				Ok(0) => {
					//Ok, cp_write got closed. Exit if start_write has also been closed.
					if start_read_eof {
						if !successful { abort!("Failed to start the daemon"); }
						exit(0);
					}
					cp_read_eof = true;
				},
				Ok(x) => {
					//Ok, got data, copy it to stderr.
					let mut idx = 0;
					while idx < x {
						match stderr_fd.write(&buf[idx..x]) {
							Ok(y) => idx += y,
							Err(_) => (),	//Ignore errors.
						}
					}
				},
				Err(_) => (),	//Ignore errors.
			};
		}
		if !start_read_eof && fdset[(idx-1) as usize].revents & (POLLIN|POLLHUP) != 0 {
			//start_read provodes data, use for ACK.
			let mut buf = [0u8; 1];
			match start_read.read(&mut buf) {
				Ok(0) => {
					//Ok, start_write got closed. Exit if cp_write has also been closed.
					if cp_read_eof {
						if !successful { abort!("Failed to start the daemon"); }
						exit(0);
					}
					start_read_eof = true;
				},
				Ok(_) => {
					bad = bad || successful || buf[0] != 49;
					successful = !bad && buf[0] == 49;
				},
				Err(_) => (),	//Ignore errors.
			};
		}
	}
}

fn child_process(start_read: FileDescriptor)
{
	//Monitor start_read losing connection, and exit when it does.
	loop {
		let mut fdset: [pollfd;1] = [pollfd{
			fd: start_read.as_raw_fd(),
			events: 0,
			revents: 0
		}];
		unsafe{poll(fdset.as_mut_ptr(), 1, -1)};
		if fdset[0].revents & POLLHUP != 0 {
			//Grandchild ready, exit.
			exit(0);
		}
	}
}

macro_rules! syslog {
	($stoo:expr, $prio:expr, $fmt:expr) => { do_syslog($stoo, $prio, format_args!($fmt)); };
	($stoo:expr, $prio:expr, $fmt:expr, $($args:expr),*) => {
		do_syslog($stoo, $prio, format_args!($fmt, $($args),*));
	};
}

struct Subprocess
{
	pid: pid_t,
	messages_pipe: FileDescriptor,
	signal_pipe: FileDescriptor,
	messages_pipe_tok: usize,
	signal_pipe_tok: usize,
	msg_buf: Vec<u8>,
	ack_state: u8,
	signal_pipe_closed: bool,
}

impl Subprocess
{
	fn handle_event(&mut self, poller: &mut Poll, token: usize, early: bool)
	{
		//In early, we only copy up to signal pipe closing.
		if self.signal_pipe.as_raw_fd() < 0 && early { return; }
		if token == self.signal_pipe_tok {
			self.handle_event_sigpipe(poller, early);
		} else if token == self.messages_pipe_tok && self.messages_pipe.as_raw_fd() >= 0 {
			self.handle_event_msgpipe(poller, early);
		}
	}
	fn handle_event_sigpipe(&mut self, poller: &mut Poll, early: bool)
	{
		if self.signal_pipe_closed { return; }
		let mut frag = [0u8; 1];
		match self.signal_pipe.read(&mut frag) {
			Ok(0) => {
				let _ = poller.deregister(&self.signal_pipe);
				self.signal_pipe_closed = true;
				let mut remaining = 0 as c_int;
				let msgfd = self.messages_pipe.as_raw_fd();
				if msgfd >= 0 { unsafe { ioctl(msgfd, FIONREAD, &mut remaining as *mut c_int); } }
				if remaining == 0 {
					self.signal_pipe = FileDescriptor::blank();
				}
				return;
			},
			Ok(_) => {
				if self.ack_state == 0 && frag[0] == 49 {
					self.ack_state = 1;
				} else {
					self.ack_state = 2;
				}
			},
			Err(x) => {
				let mut not_fatal = false;
				not_fatal |= x.kind() == IoErrorKind::Interrupted;
				not_fatal |= x.kind() == IoErrorKind::WouldBlock;
				if !not_fatal {
					syslog!(early, LOG_ERR, "[pid {}]: Read error: {}", self.pid, x);
					let _ = poller.deregister(&self.signal_pipe);
					self.signal_pipe = FileDescriptor::blank();
					self.ack_state = 2;
					return;
				}
				return;
			}
		}
	}
	fn handle_event_msgpipe(&mut self, poller: &mut Poll, early: bool)
	{
		let mut frag = [0u8; 600];
		let (r, not_fatal) = match self.messages_pipe.read(&mut frag) {
			Ok(0) => (0, false),
			Ok(x) => (x, true),
			Err(x) => {
				let mut not_fatal = false;
				not_fatal |= x.kind() == IoErrorKind::Interrupted;
				not_fatal |= x.kind() == IoErrorKind::WouldBlock;
				if !not_fatal {
					syslog!(early, LOG_ERR, "[pid {}]: Read error: {}", self.pid, x);
				}
				(0, not_fatal)
			}
		};
		if !not_fatal {
			//Channel lost.
			let _ = poller.deregister(&self.messages_pipe);
			self.messages_pipe = FileDescriptor::blank();
		}
		for j in (&frag[..r]).iter() {
			if *j == 10 {
				//Flush message.
				if self.msg_buf.len() > 0 {
					syslog!(early, LOG_NOTICE, "[pid {}]{}", self.pid, String::from_utf8_lossy(
						&self.msg_buf));
				}
				self.msg_buf.clear();
			} else {
				if self.msg_buf.len() < 640 {
					self.msg_buf.push(*j);
				}
			}
		}
		if self.messages_pipe.as_raw_fd() < 0 && self.msg_buf.len() > 0 {
			syslog!(early, LOG_NOTICE, "[pid {}]{}", self.pid, String::from_utf8_lossy(&self.msg_buf));
			self.msg_buf.clear();
		}
		if self.signal_pipe_closed && self.signal_pipe.as_raw_fd() >= 0 {
			//Check if signal pipe should be considered closed.
			let mut remaining = 0 as c_int;
			let msgfd = self.messages_pipe.as_raw_fd();
			if msgfd >= 0 { unsafe { ioctl(msgfd, FIONREAD, &mut remaining as *mut c_int); } }
			if remaining == 0 {
				self.signal_pipe = FileDescriptor::blank();
			}
		}
	}
}

fn do_syslog(stoo: bool, prio: i32, arguments: Arguments)
{
	let mut s = String::new();
	s.write_fmt(arguments).unwrap();
	if stoo { writeln!(stderr(), "{}", s).unwrap(); }
	let fmt = CString::new("%s").unwrap();
	let arg = CString::new(s.as_bytes()).unwrap();
	if !stoo { unsafe{syslog(prio, fmt.as_ptr(), arg.as_ptr())}; }
}

//Does not return on success.
fn try_launch_executable(config: &Config, listen_fds: &Vec<FileDescriptor>, msg_write: FileDescriptor,
	sig_write: FileDescriptor) -> IoError
{
	if msg_write.as_raw_fd() >= 0 {
		match msg_write.dup2(2) {
			Ok(x) => forget(x),
			Err(x) => abort!("Can't dup2: {}", x)
		};
	}
	drop(msg_write);
	let mut _args_array: Vec<CString> = Vec::new();
	let executable_name: CString = match CString::new(config.executable.as_bytes()) {
		Ok(x) => x,
		Err(_) => abort!("NUL not allowed in executable name")
	};
	_args_array.push(executable_name.clone());
	_args_array.push(match CString::new(format!("--certificates={}", &config.certificates).as_bytes()) {
		Ok(x) => x,
		Err(_) => abort!("NUL not allowed in certificates path")
	});
	_args_array.push(match CString::new(format!("--services={}", &config.services).as_bytes()) {
		Ok(x) => x,
		Err(_) => abort!("NUL not allowed in services path")
	});
	if let &Some(ref xconfig) = &config.config {
		_args_array.push(match CString::new(format!("--config={}", xconfig).as_bytes()) {
			Ok(x) => x,
			Err(_) => abort!("NUL not allowed in config string")
		});
	}
	if sig_write.as_raw_fd() >= 0 {
		_args_array.push(CString::new(format!("--ack={}", &sig_write.as_raw_fd()).as_bytes()).unwrap());
	}
	for i in listen_fds.iter() {
		_args_array.push(CString::new(format!("--listen-fd={}", i.as_raw_fd()).as_bytes()).unwrap());
	}
	let mut args_array: Vec<*const c_char> = Vec::new();
	for i in _args_array.iter() {
		args_array.push(i.as_ptr());
	}
	args_array.push(0 as *const c_char);	//Terminator.
	unsafe{execv(executable_name.as_ptr(), args_array.as_ptr())};
	//Execve only returns on failure.
	IoError::last_os_error()
}

fn launch_process(config: &Config, poller: &mut Poll, listen_fds: &Vec<FileDescriptor>, index: usize, early: bool) ->
	Subprocess
{
	//Dummy subprocess that is returned at failure.
	let dummy_subprocess = Subprocess {
		pid: 0,
		messages_pipe: FileDescriptor::blank(),
		signal_pipe: FileDescriptor::blank(),
		messages_pipe_tok: 0,
		signal_pipe_tok: 0,
		msg_buf: Vec::new(),
		ack_state: 2,
		signal_pipe_closed: false,
	};
	let messages_pipe_tok = 2 * index + 128;
	let signal_pipe_tok = 2 * index + 129;
	let dummy = Arc::new(AtomicUsize::new(0));
	let (msg_read, msg_write) = match FileDescriptor::pipe(dummy.clone()) {
		Ok((x, y)) => (x, y),
		Err(x) => {
			syslog!(early, LOG_ALERT, "Can't pipe: {}", x);
			return dummy_subprocess;
		}
	};
	let (sig_read, sig_write) = match FileDescriptor::pipe(dummy.clone()) {
		Ok((x, y)) => (x, y),
		Err(x) => {
			syslog!(early, LOG_ALERT, "Can't pipe: {}", x);
			return dummy_subprocess;
		}
	};
	let _ = msg_read.set_close_exec(true);
	let _ = sig_read.set_close_exec(true);
	let _ = sig_write.set_close_exec(true);		//Prevent double-inherits.
	//msg_write/sig_write definitely should no be close-on-exec.
	match poller.register(&msg_read, Token(messages_pipe_tok), Ready::readable(), PollOpt::empty()) {
		Ok(_) => (),
		Err(x) => {
			syslog!(early, LOG_ALERT, "Can't register fd for polling: {}", x);
			return dummy_subprocess;
		}
	}
	match poller.register(&sig_read, Token(signal_pipe_tok), Ready::readable(), PollOpt::empty()) {
		Ok(_) => (),
		Err(x) => {
			syslog!(early, LOG_ALERT, "Can't register fd for polling: {}", x);
			let _ = poller.deregister(&msg_read);
			return dummy_subprocess;
		}
	}
	//Fork the subprocess.
	let pid = match unsafe{fork()} {
		x if x < 0 => {
			//Failed!
			let err = IoError::last_os_error();
			syslog!(early, LOG_ALERT, "Can't fork: {}", err);
			let _ = poller.deregister(&msg_read);
			let _ = poller.deregister(&sig_read);
			return dummy_subprocess;
		},
		0 => {
			//This is the child.
			let _ = sig_write.set_close_exec(false);
			let err = try_launch_executable(config, listen_fds, msg_write, sig_write);
			syslog!(early, LOG_ALERT, "Can't execve handler task: {}", err);
			exit(1);
		},
		x => {
			//This is the parent. Close the write ends of pipes.
			drop(msg_write);
			drop(sig_write);
			x
		}
	};
	//Ok, return the subprocess handle.
	Subprocess {
		pid: pid,
		messages_pipe: msg_read,
		signal_pipe: sig_read,
		messages_pipe_tok: messages_pipe_tok,
		signal_pipe_tok: signal_pipe_tok,
		msg_buf: Vec::new(),
		ack_state: 0,
		signal_pipe_closed: false,
	}
}

unsafe extern "C" fn sighandler(signum: c_int, _a: *const c_void, _b: *const c_void)
{
	if signum == SIGHUP {
		if !GLOBAL_SIGHUP_FLAG.is_null() {
			(*GLOBAL_SIGHUP_FLAG).store(true, MemOrder::Relaxed);
		}
	} else if signum == SIGUSR1 {
		if !GLOBAL_SIGUSR1_FLAG.is_null() {
			(*GLOBAL_SIGUSR1_FLAG).store(true, MemOrder::Relaxed);
		}
	}
}


fn grandchild_process(mut start_write: FileDescriptor, null_fd: FileDescriptor, listen_fds: Vec<FileDescriptor>,
	config: Config)
{
	let exit_sig = Arc::new(AtomicBool::new(false));
	let usr1_sig = Arc::new(AtomicBool::new(false));

	//Write our PID to pidfile.
	if let &Some(ref pidfile) = &config.pidfile {
		let mut file = match File::create(pidfile) {
			Ok(x) => x,
			Err(x) => abort!("Error: Can't create pidfile: {}", x)
		};
		match file.write(format!("{}\n", unsafe{getpid()}).as_bytes()) {
			Ok(_) => (),
			Err(x) => abort!("Error: Can't write pidfile: {}", x)
		}
	}

	//Register dummy SIGCHLD signal handler, so our polls get interrupted by child exiting.
	let mut action: sigaction = unsafe{zeroed()};
	action.sa_sigaction = sighandler as usize;
	action.sa_flags = SA_SIGINFO;
	unsafe {sigaction(SIGCHLD, &action as *const sigaction, 0 as *mut sigaction)};

	//Register SIGHUP signal handler, triggering SIGHUP to all tasks.
	unsafe { GLOBAL_SIGHUP_FLAG = exit_sig.deref() as *const AtomicBool; }
	let mut action: sigaction = unsafe{zeroed()};
	action.sa_sigaction = sighandler as usize;
	action.sa_flags = SA_SIGINFO;
	unsafe {sigaction(SIGHUP, &action as *const sigaction, 0 as *mut sigaction)};

	//Register SIGUSR1 signal handler, triggering SIGHUP to all tasks and spawning new ones.
	unsafe { GLOBAL_SIGUSR1_FLAG = usr1_sig.deref() as *const AtomicBool; }
	let mut action: sigaction = unsafe{zeroed()};
	action.sa_sigaction = sighandler as usize;
	action.sa_flags = SA_SIGINFO;
	unsafe {sigaction(SIGUSR1, &action as *const sigaction, 0 as *mut sigaction)};

	//Chdir to root.
	let rootpath = CString::new("/").unwrap();
	match unsafe{chdir(rootpath.as_ptr())} {
		x if x >= 0 => (),
		_ => abort_syserr!("Can't chdir to root: {}")
	};
	//Open the system log.
	let taskname = CString::new("btls-inetd").unwrap();
	unsafe{openlog(taskname.as_ptr(), LOG_NDELAY | LOG_PID, LOG_DAEMON)};
	//Create the poller.
	let mut poller = match Poll::new() {
		Ok(x) => x,
		Err(x) => abort!("Error: Can't create poller: {}", x)
	};
	let mut events = Events::with_capacity(1024);
	let mut sproc = Vec::<Subprocess>::new();
	for i in 0..config.processes {
		//Launch the process.
		sproc.push(launch_process(&config, &mut poller, &listen_fds, i, true));
	}
	grandchild_wait_launch(&mut sproc, &mut events, &mut poller);
	//ACK successful launch. First close stderr (replacing with NULL) and then write to start_write and close
	//it.
	match null_fd.dup2(2) {
		Ok(_) => (),
		Err(x) => abort!("Can't dup2: {}", x)
	};
	let success = [49u8];
	match start_write.write(&success) {
		Ok(_) => (),
		//We need to report error to syslog, as we are past point where we can report errors.
		Err(x) => {
			syslog!(false, LOG_ALERT, "Can't send ACK: {}", x);
			exit(1);
		}
	};
	drop(start_write);		//Close start_write to complete startup.
	grandchild_log_loop(sproc, events, poller, exit_sig, usr1_sig, &listen_fds, &config);
}

fn grandchild_wait_launch(sproc: &mut Vec<Subprocess>, events: &mut Events, poller: &mut Poll)
{
	let mut completed = 0usize;
	let mut at_least_one = false;
	while completed < sproc.len() {
		match poller.poll(events, None) {
			Ok(_) => (),
			Err(x) => {
				let mut not_fatal = false;
				not_fatal |= x.kind() == IoErrorKind::Interrupted;
				not_fatal |= x.kind() == IoErrorKind::WouldBlock;
				if not_fatal { continue; }
				syslog!(true, LOG_ERR, "Error in poll: {}", x);
				continue;
			}
		};
		for event in events.iter() {
			let tok = event.token();
			for i in sproc.iter_mut() {
				//When launching, task can't be marked dead yet.
				i.handle_event(poller, tok.0, true);
				if i.signal_pipe.as_raw_fd() < 0 {
					//One task completed or failed.
					completed += 1;
					at_least_one |= i.ack_state == 1;
				}
			}
		}
	}
	if !at_least_one {
		syslog!(true, LOG_ALERT, "All subprocesses failed to launch");
		exit(1);
	}
}

fn grandchild_log_loop(mut sproc: Vec<Subprocess>, mut events: Events, mut poller: Poll, exit_flag: Arc<AtomicBool>,
	usr1_flag: Arc<AtomicBool>, listen_fds: &Vec<FileDescriptor>, config: &Config)
{
	loop {
		let usr1_flagged = usr1_flag.swap(false, MemOrder::Relaxed);
		let exit_flagged = exit_flag.swap(false, MemOrder::Relaxed);
		if exit_flagged {
			//OK, send SIGHUP to all our tasks.
			syslog!(false, LOG_NOTICE, "Received SIGHUP, forwarding to all tasks...");
			for i in sproc.iter() {
				if i.pid > 0 { unsafe{kill(i.pid, SIGHUP)}; }
			}
		}
		if usr1_flagged {
			//First, send SIGHUP to all our tasks.
			syslog!(false, LOG_NOTICE, "Received SIGUSR1, sending SIGHUP to all tasks...");
			for i in sproc.iter() {
				if i.pid > 0 { unsafe{kill(i.pid, SIGHUP)}; }
			}
			//Then, spawn new tasks to replace ones lost. We can just spawn raw tasks, as the log loop
			//will process them.
			let offset = sproc.len();
			for i in 0..config.processes {
				//Launch the process.
				sproc.push(launch_process(config, &mut poller, listen_fds, offset+i, false));
			}
		}
		match poller.poll(&mut events, None) {
			Ok(_) => (),
			Err(x) => {
				let mut not_fatal = false;
				not_fatal |= x.kind() == IoErrorKind::Interrupted;
				not_fatal |= x.kind() == IoErrorKind::WouldBlock;
				if not_fatal { continue; }
				syslog!(false, LOG_ERR, "Error in poll: {}", x);
				continue;
			}
		};
		for event in events.iter() {
			let tok = event.token();
			let mut not_dead = 0u32;
			for i in sproc.iter_mut() {
				if i.pid == 0 { continue; }	//Ignore dead taks.
				not_dead += 1;
				i.handle_event(&mut poller, tok.0, false);
				if i.messages_pipe.as_raw_fd() < 0 && i.signal_pipe.as_raw_fd() < 0 && i.pid > 0 {
					//Task is likely dead, try to clean it up.
					let mut status = 0 as c_int;
					let z = unsafe{waitpid(i.pid, &mut status as *mut c_int, WNOHANG)};
					if z > 0 {
						//The child exited. Mark it as dead. These events are always
						//syslog-only.
						if unsafe{WIFEXITED(status)} {
							let estatus = unsafe{WEXITSTATUS(status)};
							if estatus != 0 {
								syslog!(false, LOG_ERR,
									"PID {} exited abnormally with code {}",
									i.pid, estatus);
							} else {
								syslog!(false, LOG_NOTICE,
									"PID {} exited normally", i.pid);
							}
						} else if unsafe{WIFSIGNALED(status)} {
							syslog!(false, LOG_ALERT,
								"PID {} CRASHED with signal {}!", i.pid,
								unsafe{WTERMSIG(status)});
						} else {
							syslog!(false, LOG_ALERT,
								"PID {} aborted by unknown cause!", i.pid);
						}
						i.pid = 0;
						not_dead -= 1;	//This just died!
					}
				}
			}
			if not_dead == 0 {
				//All our children have exited. We exit too.
				syslog!(false, LOG_NOTICE, "All children exited, exiting daemon");
				exit(0);
			}
		}
	}
}

fn no_detach(config: &Config, listen_fds: &Vec<FileDescriptor>)
{
	let mut pids = Vec::new();
	for _ in 0..config.processes {
		//Fork the subprocess.
		let pid = match unsafe{fork()} {
			x if x < 0 => {
				//Failed!
				let err = IoError::last_os_error();
				abort!("Can't fork: {}", err);
			},
			0 => {
				//This is the child.
				let err = try_launch_executable(config, listen_fds, FileDescriptor::blank(),
					FileDescriptor::blank());
				abort!("Can't execve handler task: {}", err);
			},
			x => {
				//This is the parent.
				x
			}
		};
		pids.push(pid);
	};
	//Wait for all handlers to exit.
	while pids.len() > 0 {
		for i in pids.iter_mut() {
			let mut status = 0 as c_int;
			let z = unsafe{waitpid(*i, &mut status as *mut c_int, WNOHANG)};
			if z > 0 {
				//The child exited. Mark it as dead. These events are always
				//syslog-only.
				if unsafe{WIFEXITED(status)} {
					let estatus = unsafe{WEXITSTATUS(status)};
					if estatus != 0 {
						writeln!(stderr(), "PID {} exited abnormally with code {}",
							*i, estatus).unwrap();
					} else {
						writeln!(stderr(), "PID {} exited normally", *i).unwrap();
					}
				} else if unsafe{WIFSIGNALED(status)} {
					writeln!(stderr(), "PID {} CRASHED with signal {}!", *i,
						unsafe{WTERMSIG(status)}).unwrap();
				} else {
					writeln!(stderr(), "PID {} aborted by unknown cause!", *i).unwrap();
				}
				*i = 0;
			}
		}
		let mut i = 0;
		while i < pids.len() {
			if pids[i] == 0 { pids.swap_remove(i); } else { i = i + 1; }
		}
		sleep(Duration::from_millis(1000));	//Wait a bit to save CPU.
	}
}

fn main()
{
	//Open /dev/null to check that file descriptors are properly set up.
	let devnullpath = CString::new("/dev/null").unwrap();
	let null_fd = unsafe{open(devnullpath.as_ptr(), O_RDWR)};
	if null_fd < 3 {
		//stdin/stdout/stderr aren't properly open. We can't even write an error message in this case.
		exit(57);
	}
	let dummy = Arc::new(AtomicUsize::new(0));
	let null_fd = FileDescriptor::new(null_fd, dummy.clone());
	let config = Config::parse();
	//We first aquire any listening file descriptors, because these might have privileged ports.
	let mut fd_set = make_listeners(&config.listen_spec);
	drop_privileges(&config.username);
	if config.no_detach {
		return no_detach(&config, &fd_set);
	}
	//Create a pipe to serve as temporary child stderr.
	let (cp_read, cp_write) = match FileDescriptor::pipe(dummy.clone()) {
		Ok((x, y)) => (x, y),
		Err(err) => abort!("Can't create pipe: {}", err)
	};
	let (start_read, start_write) = match FileDescriptor::pipe(dummy.clone()) {
		Ok((x, y)) => (x, y),
		Err(err) => abort!("Can't create pipe: {}", err)
	};
	//start_write is inherited by tasks, so it needs close_on_exec.
	match start_write.set_close_exec(true) {
		Ok(_) => (),
		Err(err) => abort!("Can't set close-on-execp: {}", err)
	};
	//Fork twice so the grandchild can be daemon.
	let child_pid = match unsafe{fork()} {
		x if x >= 0 => x,
		_ => abort_syserr!("Can't fork: {}")
	};
	//On, parent..
	if child_pid > 0 {
		//This is already open, don't double-count it.
		let dummy = Arc::new(AtomicUsize::new(0));
		let stderr_fd = FileDescriptor::new(2, dummy);
		//Close the listen fds. Only grandchild will get those.
		fd_set.clear();
		//cp_write and start_write are only for children. We retain cp_read and start_read.
		drop(cp_write);
		drop(start_write);
		parent_process(cp_read, start_read, stderr_fd.clone());
		forget(stderr_fd);	//Prevent closing this.
		exit(0);
	}
	//On child... Only parent is supposed to have cp_read.
	drop(cp_read);
	//Dup NULL to fds 0 and 1, cp_write to 2. Forget the FDs, as these must not be closed.
	match null_fd.dup2(0) {
		Ok(x) => forget(x),
		Err(x) => abort!("Can't dup2: {}", x)
	};
	match null_fd.dup2(1) {
		Ok(x) => forget(x),
		Err(x) => abort!("Can't dup2: {}", x)
	};
	match cp_write.dup2(2) {
		Ok(x) => forget(x),
		Err(x) => abort!("Can't dup2: {}", x)
	};
	//Do sesid.
	match unsafe{setsid()} {
		x if x >= 0 => (),
		_ => abort_syserr!("Can't setsid: {}")
	};
	//Close the original cp_write. Only the fd2 remains.
	drop(cp_write);

	let child_pid = match unsafe{fork()} {
		x if x >= 0 => x,
		_ => abort_syserr!("Can't fork: {}")
	};
	if child_pid > 0 {
		//We are the child. Close start_write, as only grandchild is supposed to have it.
		//cp_read is already closed. cp_write is reassigned to fd 2. start_read is retained.
		drop(start_write);
		//Also, close the listen fds.
		fd_set.clear();
		child_process(start_read);
		exit(0);
	}
	//On grandchild...
	//Close start_read. Only parent and child is supposed to have that. cp_read is already closed.
	drop(start_read);
	grandchild_process(start_write, null_fd, fd_set, config);
	exit(0);
}
