extern crate mio;
extern crate btls;
extern crate libc;
extern crate btls_inetd;

use mio::{Event, Events, Poll, PollOpt, Ready, Registration, SetReadiness, Token};
use std::collections::BTreeMap;
use std::io::{ErrorKind as IoErrorKind, stderr, Write as IoWrite};
use std::process::exit;
use std::sync::Arc;
use std::sync::atomic::{AtomicBool, AtomicUsize, Ordering as MemOrder};
use std::borrow::Cow;
use std::mem::zeroed;
use std::env::args_os;
use std::ops::Deref;
use std::panic::{catch_unwind, AssertUnwindSafe};
use std::thread::sleep;
use std::time::Duration;
use std::iter::repeat;
use std::time::SystemTime;
use std::path::PathBuf;
use std::os::unix::io::RawFd;
use std::str::FromStr;
use btls::error::TlsFailure;
use btls::{AlpnCallback, AlpnList, alpn_error_no_overlap};
use btls::server_certificates::{CertificateDirectory, CertificateLookup};
use btls::logging::StderrLog;
use btls::{AbortReason, ServerConfiguration, ServerSession, Session, TxIrqHandler};
use libc::{c_int, c_void, SA_SIGINFO, sigaction, SIGHUP};
use libc::{getuid, geteuid, getrlimit, rlimit, RLIMIT_NOFILE};

use btls_inetd::{increment_count, decrement_count};
use btls_inetd::file_descriptor::FileDescriptor;
use btls_inetd::service::{ConnectionInfo, ConnectionService, ServiceCache};
use btls_inetd::token::{AllocatedToken, TokenAllocatorPool};
use btls_inetd::listener::{ListenerKind};
use btls_inetd::listensock::{make_socket};

const MAX_FDS_REDUCE: usize = 100;
static mut GLOBAL_SIGHUP_FLAG: *const AtomicBool = 0 as *const AtomicBool;

macro_rules! cow {
	($x:expr) => { Cow::Borrowed($x) };
	($x:expr,$($p:expr),*) => { Cow::Owned(format!($x, $($p),*)) };
}

macro_rules! scow {
	($($p:expr),*) => { Some(cow!($($p),*)) };
}

macro_rules! store_err {
	($v:expr, $($p:expr),*) => { $v = scow!($($p),*) };
	(RET $v:expr, $($p:expr),*) => {{ store_err!($v, $($p),*); return; }};
}

unsafe extern "C" fn sighup_handler(_signum: c_int, _a: *const c_void, _b: *const c_void)
{
	if !GLOBAL_SIGHUP_FLAG.is_null() {
		(*GLOBAL_SIGHUP_FLAG).store(true, MemOrder::Relaxed);
	}
}

#[derive(Clone)]
struct AlpnFunction(ServiceCache);

impl AlpnCallback for AlpnFunction
{
	fn select_alpn<'a>(&mut self, sni: Option<&str>, alpn_list: AlpnList<'a>) ->
		Result<Option<usize>, TlsFailure>
	{
		if let Some(sni) = sni {
			let mut client_supports = BTreeMap::new();
			for i in alpn_list.iter().enumerate() { client_supports.insert(i.1, i.0); }
			let alpn_candidates = self.0.query_alpn_list(sni);
			if alpn_candidates.len() == 0 { return Ok(None); }
			for i in alpn_candidates.iter() {
				match client_supports.get(i.deref()) {
					Some(idx) => return Ok(Some(*idx)),
					None => (),
				};
			}
			alpn_error_no_overlap()?;
		}
		Ok(None)	//Can't choose.
	}
	fn clone_self(&self) -> Box<AlpnCallback+Send> { Box::new(self.clone()) as Box<AlpnCallback+Send> }
}

struct Config
{
	cdpath: String,				//Certificate directory path.
	config: String,				//Configuration string.
	listener_specs: Vec<String>,		//Listeners
	services_dir: String,			//Services dir.
	launch_ack: Option<i32>,		//fd to direct launch ack.
	listener_fds: Vec<RawFd>,		//Fds to listen.
}

impl Config
{
	fn from_cmdline() -> Config
	{
		let mut cdpath = None;
		let mut config = String::new();
		let mut listen = Vec::new();
		let mut listen_fd = Vec::new();
		let mut services = None;
		let mut launch_ack = None;
		let mut dummy = true;
		for i in args_os() {
			if dummy { dummy = false; continue; }
			let xstr = match i.clone().into_string() {
				Ok(x) => x,
				Err(x) => {
					writeln!(stderr(), "Invalid argument: `{}`", x.to_string_lossy()).unwrap();
					exit(1);
				}
			};
			if xstr.starts_with("--certificates=") { cdpath = Some((&xstr[15..]).to_owned()); }
			if xstr.starts_with("--config=") { config = (&xstr[9..]).to_owned(); }
			if xstr.starts_with("--listen=") { listen.push((&xstr[9..]).to_owned()); }
			if xstr.starts_with("--listen-fd=") {
				listen_fd.push({
					match RawFd::from_str(&xstr[12..]) {
						Ok(x) if x < 3 => {
							writeln!(stderr(), "--socket fd can't be less than 3").
								unwrap();
							exit(1);
						},
						Ok(x) => x,
						Err(_) => {
							writeln!(stderr(), "Can't parse --listen-fd=<fd>").unwrap();
							exit(1);
						}
					}
				});
			}
			if xstr.starts_with("--services=") { services = Some((&xstr[11..]).to_owned()); }
			if xstr.starts_with("--ack=") {
				launch_ack = Some({
					match i32::from_str(&xstr[6..]) {
						Ok(x) if x < 3 => {
							writeln!(stderr(), "--ack fd can't be less than 3").unwrap();
							exit(1);
						},
						Ok(x) => x,
						Err(_) => {
							writeln!(stderr(), "Can't parse --ack=<fd>").unwrap();
							exit(1);
						}
					}
				});
			}
		}
		let cdpath = if let Some(cdpath) = cdpath { cdpath } else {
			writeln!(stderr(), "Need --certificates=<path>").unwrap();
			exit(1);
		};
		if listen.len() == 0 && listen_fd.len() == 0 {
			writeln!(stderr(), "Need --listen=<spec> or --listen-fd=<fd>").unwrap();
			exit(1);
		};
		let services = if let Some(services) = services { services } else {
			writeln!(stderr(), "Need --services=<path>").unwrap();
			exit(1);
		};
		Config {
			cdpath: cdpath,
			config: config,
			listener_specs: listen,
			services_dir: services,
			launch_ack: launch_ack,
			listener_fds: listen_fd,
		}
	}
}


fn main()
{
	let mut filelim: rlimit = unsafe{zeroed()};
	let maxfiles = match unsafe{getrlimit(RLIMIT_NOFILE, &mut filelim as *mut _)} {
		x if x < 0 => 1024,		//Assumed.
		_ => filelim.rlim_cur,		//Measured.
	};
	let maxfiles = maxfiles as usize - MAX_FDS_REDUCE;
	writeln!(stderr(), "Notice: Using maximum of {} file descriptors", maxfiles).unwrap();

	let config = Config::from_cmdline();
	if unsafe{getuid()} <= 0 || unsafe{geteuid()} <= 0 {
		writeln!(stderr(), "Error: Can't run program as root").unwrap();
		exit(1);
	}

	//Create substates.
	let store = match CertificateDirectory::new(&config.cdpath, StderrLog) {
		Ok(x) => x,
		Err(x) => {
			writeln!(stderr(), "Error: Can't load certificate store: {}", x).unwrap();
			exit(1);
		}
	};
	let mut server_config = ServerConfiguration::from(&store as &CertificateLookup);
	server_config.config_flags(&config.config, |error|{writeln!(stderr(), "{}", error).unwrap();});
	server_config.set_handshake_timeout(10000);	//10 seconds.
	let poller = match Poll::new() {
		Ok(x) => x,
		Err(x) => {
			writeln!(stderr(), "Error: Can't create poller: {}", x).unwrap();
			exit(1);
		}
	};
	let services_dir = match PathBuf::from(&config.services_dir).canonicalize() {
		Ok(x) => x,
		Err(x) => {
			writeln!(stderr(), "Error: Services directory does not exist: {}", x).unwrap();
			exit(1);
		}
	};
	//Set ALPN callback.
	let scache = ServiceCache::new(&services_dir);
	let alpn_function = Box::new(AlpnFunction(scache.clone()));
	server_config.set_alpn_function(alpn_function);
	//Construct the state.
	let mut gstate = GlobalState {
		tokens: BTreeMap::new(),
		connections: BTreeMap::new(),
		listeners: BTreeMap::new(),
		next_conn_seq: 0,
		poll: poller,
		tpool: TokenAllocatorPool::new(0, 16384),
		config: server_config,
		scache: scache,
		fd_count: Arc::new(AtomicUsize::new(0)),
		max_fd_count: maxfiles,
		start_drain_signal: Arc::new(AtomicBool::new(false)),
		last_timed_broadcast: SystemTime::now()
	};
	for i in config.listener_specs.iter() {
		let obtained = match make_socket(i) {
			Ok(x) => x,
			Err(x) => {
				writeln!(stderr(), "Warning: Bad listener `{}`: {}", i, x).unwrap();
				continue;
			}
		};
		for k in obtained {
			let j = Listener{
				socket: ListenerKind::new_tcp(k)
			};
			gstate.add_listener(j);
		}
	}
	for i in config.listener_fds.iter() {
		let j = Listener{
			socket: ListenerKind::new_fd(FileDescriptor::new(*i, gstate.fd_count.clone()))
		};
		gstate.add_listener(j);
	}
	if gstate.listeners.len() == 0 {
		writeln!(stderr(), "Error: No ports to listen").unwrap();
		exit(1);
	}
	//Register SIGHUP handler that triggers drain.
	unsafe { GLOBAL_SIGHUP_FLAG = gstate.start_drain_signal.deref() as *const AtomicBool; }
	let mut action: sigaction = unsafe{zeroed()};
	action.sa_sigaction = sighup_handler as usize;
	action.sa_flags = SA_SIGINFO;
	unsafe {sigaction(SIGHUP, &action as *const sigaction, 0 as *mut sigaction)};
	sleep(Duration::from_millis(1000));	//Wait a bit for certificate load to complete.
	if let Some(x) = config.launch_ack {
		let mut fd_h = FileDescriptor::new(x, gstate.fd_count.clone());
		let _ = fd_h.write(&[0x31]);	//"1".
		//The ack fd is dropped to floor here, causing it to be closed.
	}
	gstate.loop_events();
	//Don't generate any more events.
	unsafe { GLOBAL_SIGHUP_FLAG = 0 as *const AtomicBool; }
	writeln!(stderr(), "All connections have ended, exiting.").unwrap();
}

//TX IRQ notifier. This is a combination of MIO sender and an atomic flag.
#[derive(Clone)]
struct TxIrqNotifier(SetReadiness, Arc<AtomicBool>, Arc<AtomicUsize>);

impl TxIrqHandler for TxIrqNotifier
{
	fn wants_transmit(&mut self)
	{
		//Increment the count.
		loop {
			let val = self.2.load(MemOrder::SeqCst);
			if self.2.compare_and_swap(val, val + 1, MemOrder::SeqCst) == val { break; }
		}
		//Don't send TX IRQ if there is unacked TX IRQ.
		let val = self.1.swap(true, MemOrder::SeqCst);
		if !val { let _ = self.0.set_readiness(Ready::readable()); }
	}
}


struct Connection
{
	//The underlying socket.
	token_socket: AllocatedToken,			//Token for the socket itself.
	external_socket: FileDescriptor,		//The socket that transports the TLS.
	seen_tls_incoming_eof: bool,			//TLS has signaled EOF once.
	seen_tls_txstart: bool,				//TLS has signaled TX start once.
	hard_error: Option<Cow<'static, str>>,		//Error code.
	connection_info: ConnectionInfo,		//Connection information.
	//TX interrupts.
	token_txirq: AllocatedToken,			//Token for the TX IRQ.
	txirq_receiver: Registration,			//TX IRQ event receiver.
	txirq_sender: SetReadiness,			//TX IRQ event sender.
	txirq_flag: Arc<AtomicBool>,			//TX IRQ flag.
	txirq_count: Arc<AtomicUsize>,			//Number of interrupts queued.
	//Service Common
	tls: ServerSession,				//The TLS state.
	//Service stdin.
	s_scratch: Vec<u8>,				//Scratch buffer.
	t_out_ready: bool,				//TLS output (outgoing) ready?
	//Service stdout.
	t_in_ready: bool,				//TLS input (incoming) ready?
	//The connected service.
	service: ConnectionService,			//Service.
}


impl Connection
{
	fn new(poll: &mut Poll, allocator: &mut TokenAllocatorPool, config: &ServerConfiguration,
		socket: FileDescriptor, connection_info: ConnectionInfo, scache: ServiceCache,
		fd_count: Arc<AtomicUsize>) -> Result<Connection, Cow<'static, str>>
	{
		let token_socket = allocator.allocate().map_err(|_|cow!("Can't allocate Tokens"))?;
		let token_txirq = allocator.allocate().map_err(|_|cow!("Can't allocate Tokens"))?;
		let (receiver, sender) = Registration::new2();
		let txirq_flag = Arc::new(AtomicBool::new(false));
		let txirq_count = Arc::new(AtomicUsize::new(0));
		let txirq_handler = TxIrqNotifier(sender.clone(), txirq_flag.clone(), txirq_count.clone());
		//Need to register readabilty for the TLS socket and the IRQ notifier.
		poll.register(&socket, Token(token_socket.value()), Ready::readable(), PollOpt::oneshot()).
			map_err(|_|cow!("Can't register main socket for polling"))?;
		poll.register(&receiver, Token(token_txirq.value()), Ready::readable(), PollOpt::oneshot()).
			map_err(|_|cow!("Can't register TX irq for polling"))?;
		//Create the TLS session and set TX IRQ. Setting TX IRQ causes IRQ to immediately happen if the
		//session is ready to transmit.
		let mut tls = config.make_session().map_err(|x|Cow::Owned(format!("Can't create TLS session: {}",
			x)))?;
		tls.set_tx_irq(Box::new(txirq_handler));
		//Fill the structure.
		Ok(Connection {
			token_socket: token_socket,
			token_txirq: token_txirq,
			txirq_receiver: receiver,
			txirq_sender: sender,
			txirq_flag: txirq_flag,
			txirq_count: txirq_count,
			tls: tls,
			external_socket: socket,
			hard_error: None,
			t_out_ready: false,
			t_in_ready: false,
			seen_tls_incoming_eof: false,
			seen_tls_txstart: false,
			s_scratch: repeat(0u8).take(16384).collect(),
			connection_info: connection_info,
			service: ConnectionService::new(allocator, scache, fd_count)?,
		})
	}
	//Handle timed event on socket. Returns Err(()) if socket should be destroyed.
	fn handle_timed_event(&mut self) -> Result<(), ()>
	{
		//Hard TLS errors.
		if let (Some(cause), true) = (self.tls.aborted_by(), self.hard_error.is_none()) {
			store_err!(self.hard_error, "{}", cause);
		}
		if self.hard_error.is_some() { Err(()) } else { Ok(()) }
	}
	//Handle an event on the socket.
	fn handle_socket_event(&mut self, poll: &mut Poll, tok: usize, kind: &Ready)
	{
		//If soft_error is set, we need to process writes, but reads need not be processed.
		let pending_shutdown = self.service.pending_shutdown();
		//Handle an read event. In this case, we tell TLS library to read the socket.
		//Any errors from this need to be immediately appiled.
		if kind.is_readable() && self.hard_error.is_none() && !pending_shutdown {
			//We ignore the return value, because aborted_by() covers any error aborts.
			let _ = self.tls.read_tls(&mut self.external_socket);
			self.hard_error = self.tls.aborted_by().map(|x|{
				if let AbortReason::MutualEof = x {
					return cow!("Connection closed normally");
				}
				cow!("TLS error: {}", x)
			});
		}
		//Handle an read event. In this case, we tell TLS library to write the socket.
		//Any errors from this need to be immediately appiled.
		let mut connection_lost = false;
		if kind.is_writable() && self.hard_error.is_none() {
			//We need to clear txirq_flag before call,
			//since on success the call triggers TX IRQ from within, which resets txirq
			//flag to true.
			self.txirq_flag.store(false, MemOrder::SeqCst);
			match self.tls.write_tls(&mut self.external_socket) {
				Ok(_) => (),
				Err(x) => match x.kind() {
					IoErrorKind::Interrupted => {
						//We want to re-enable TX IRQ, to try again.
						self.txirq_flag.store(true, MemOrder::SeqCst);
					},
					IoErrorKind::WouldBlock => {
						//We want to re-enable TX IRQ, to try again.
						self.txirq_flag.store(true, MemOrder::SeqCst);
					},
					_ => {
						//Assume connection has been lost.
						connection_lost = true;
					},
				}
			};
			self.hard_error = self.tls.aborted_by().map(|x|cow!("TLS error: {}", x));
			if connection_lost && self.hard_error.is_none() {
				store_err!(self.hard_error, "Connection with client has been lost");
			}
		}
		//Reregister the event with appropriate R/W state.
		if self.hard_error.is_none() {
			//Reregister the event.
			let mut ready = Ready::empty();
			if !pending_shutdown && self.tls.wants_read() { ready = ready | Ready::readable(); }
			if self.txirq_flag.load(MemOrder::SeqCst) { ready = ready | Ready::writable(); }
			//Poll.reregister does not like having no ready bits.
			if !ready.is_empty() {
				match poll.reregister(&self.external_socket, Token(tok), ready, PollOpt::oneshot()) {
					Ok(_) => (),
					Err(x) => store_err!(self.hard_error, "poll.reregister error: {}", x)
				}
			}
			//Check if service should be launched: When at least one side of handshake completes.
			if self.tls.can_tx_data() || self.tls.handshake_completed() {
				self.service.signal_launch();
			}
			//We only signal t_out_ready with EOF set once.
			let is_eof = self.tls.is_eof();
			self.t_out_ready = (!self.seen_tls_incoming_eof && is_eof) || self.tls.bytes_available() > 0;
			self.seen_tls_incoming_eof |= is_eof;
			//We only signal TX start once.
			let is_tx = self.tls.can_tx_data();
			self.t_in_ready = is_tx && (!self.seen_tls_txstart || self.tls.bytes_queued() < 32768);
			self.seen_tls_txstart |= is_tx;
		}
	}
	//Handle event on TX IRQ.
	fn handle_txirq_event(&mut self, poll: &mut Poll)
	{
		//Clear the interrupt.
		let _ = self.txirq_sender.set_readiness(Ready::empty());
		//If soft_error is set, we need to process writes, but reads need not be processed.
		let pending_shutdown = self.service.pending_shutdown();
		if self.hard_error.is_some() { return; }
		//We don't check for pending shutdown, because this needs to still work in case of pending shutdown,
		//in order to send the EOF notification.
		//We need to reregister the socket, with R/W bits as appropriate.
		let mut socket_ready = Ready::empty();
		if !pending_shutdown && self.tls.wants_read() { socket_ready = socket_ready | Ready::readable(); }
		//Decrement the IRQ count.
		let mut val = self.txirq_count.load(MemOrder::SeqCst);
		loop {
			if val == 0 { break; }
			let val2 = self.txirq_count.compare_and_swap(val, val - 1, MemOrder::SeqCst);
			if val2 == val { break; }
			val = val2;
		}
		if val > 0 {
			//TX is pending.
			socket_ready = socket_ready | Ready::writable();
			//More IRQs sent? Then process those too in subsequent iterations.
			if val > 1 { let _ = self.txirq_sender.set_readiness(Ready::readable()); }
		}
		//Poll.reregister does not like having no ready bits.
		if !socket_ready.is_empty() {
			match poll.reregister(&self.external_socket, Token(self.token_socket.value()), socket_ready,
				PollOpt::oneshot()) {
				Ok(_) => (),
				Err(x) => store_err!(self.hard_error, "poll.reregister error: {}", x)
			};
		}
		if self.hard_error.is_none() {
			//Always reregister IRQ.
			match poll.reregister(&self.txirq_receiver, Token(self.token_txirq.value()),
				Ready::readable(), PollOpt::oneshot()) {
				Ok(_) => (),
				Err(x) => store_err!(self.hard_error, "poll.reregister error: {}", x)
			};
		}
	}
	//Handle event. Returns Err(()) if connection should be destroyed.
	fn handle_event(&mut self, poll: &mut Poll, tok: usize, kind: &Ready, cnum: u64) -> Result<(), ()>
	{
		if self.token_socket.value() == tok { self.handle_socket_event(poll, tok, kind); }
		if self.token_txirq.value() == tok { self.handle_txirq_event(poll); }
		self.service.handle_event(poll, tok, kind, cnum);
		if self.service.should_launch() { self.launch_service(poll, cnum); }
		let (write, forcewrite, read) = self.service.rendezvous_request();
		if (self.t_out_ready || forcewrite) && write { self.rendezvous_tls_to_service(); }
		if self.t_in_ready && read { self.rendezvous_service_to_tls(); }
		if self.hard_error.is_none() { self.service.retrigger_service(poll); }
		if self.service.pending_shutdown() && !self.txirq_flag.load(MemOrder::SeqCst) &&
			self.tls.bytes_queued() == 0 {
			//OK, error shutdown is complete when TX queues are flushed.
			self.hard_error = Some(self.service.get_error());
		}
		//Hard TLS errors.
		if let (Some(cause), true) = (self.tls.aborted_by(), self.hard_error.is_none()) {
			store_err!(self.hard_error, "{}", cause);
		}
		if self.hard_error.is_some() { Err(()) } else { Ok(()) }
	}
	//This connection is going to be destroyed NOW. No more data can be sent or received.
	fn destroy_notify(&mut self)
	{
		self.service.destroy_notify();
	}
	//Transfer data from TLS to service.
	fn rendezvous_tls_to_service(&mut self)
	{
		//We don't process the service at all on pending shutdown.
		if self.hard_error.is_some() { return; }
		//Service is OK with write?
		if !self.service.wants_write_to_service() { return; }
		let amount = {
			let buf = match self.service.prefix_sent() {
				false => &[],
				true => match self.tls.begin_transacted_read(&mut self.s_scratch) {
					Ok(x) => x,
					Err(x) => store_err!(RET self.hard_error, "TLS read error: {}", x)
				}
			};
			match self.service.write_to_service(&buf) {
				Ok(x) => x,
				Err(x) => {
					self.hard_error = Some(x);
					return;
				}
			}
		};
		if amount > 0 {
			match self.tls.end_transacted_read(amount) {
				Ok(_) => (),
				Err(x) => store_err!(RET self.hard_error, "TLS read error: {}", x)
			};
		};
		//Take EOF flag, and end stream if EOF is asserted and prefix has been sent.
		if self.service.prefix_sent() && self.tls.is_eof() {
			if let Err(x) = self.service.close_link_to_service() { self.hard_error = Some(x); }
		}
	}
	//Transfer data from service to TLS.
	fn rendezvous_service_to_tls(&mut self)
	{
		let mut buf = [0u8; 16384];
		let r = match self.service.read_from_service(&mut buf) {
			Ok(Some(0)) => return,		//Nothing to do.
			Ok(Some(x)) => x,
			Ok(None) => {
				self.tls.send_eof();
				return;
			},
			Err(x) => {
				self.hard_error = Some(x);
				return;
			}
		};
		//We use the property of TLS buffering that it accepts all or nothing.
		match self.tls.write(&buf[..r]) {
			Ok(_) => (),		//OK.
			Err(x) => {
				if x.kind() == IoErrorKind::Interrupted { return; }
				if x.kind() == IoErrorKind::WouldBlock { return; }
				//Other errors are fatal.
				store_err!(RET self.hard_error, "TLS write error: {}", x);
			}
		};
	}
	//Launch the service.
	fn launch_service(&mut self, poll: &mut Poll, cnum: u64)
	{
		let info = self.connection_info.clone();
		let tlsinfo = self.tls.connection_info();
		let sni = match self.tls.get_sni() {
			Some(Some(x)) => x,
			//This is interpretted as service error.
			_ => return self.service.set_error(cow!("No SNI sent"))
		};
		let alpn = self.tls.get_alpn().unwrap_or(None);
		if self.hard_error.is_some() { return; }
		let conninfo = format!("TLS {}{}/{}/{}/{} ({}{}{})", tlsinfo.version_str, if tlsinfo.version >= 4 {
			""} else if tlsinfo.ems_available { "(EMS)" } else { "(LEGACY)" },
			tlsinfo.exchange_group_str, tlsinfo.protection_str, tlsinfo.hash_str, tlsinfo.signature_str,
			if tlsinfo.validated_ocsp { ", OCSP stapled" } else {""}, if tlsinfo.validated_ct {
			", CT stapled" } else {""});
		self.service.launch_service(poll, info, cnum, tlsinfo.version, sni, alpn, &conninfo);
	}
}


struct Listener
{
	socket: ListenerKind,
}

impl Listener
{
	fn accept(&mut self,  poll: &mut Poll, allocator: &mut TokenAllocatorPool, config: &ServerConfiguration,
		scache: ServiceCache, fd_count: Arc<AtomicUsize>, fd_limit: usize) ->
		Result<(Connection, Cow<'static, str>), Cow<'static, str>>
	{
		if fd_count.load(MemOrder::Relaxed) > fd_limit { return Err(cow!("")); }
		let (conn, addr) = self.socket.accept().map_err(|x|{
			if x.kind() == IoErrorKind::Interrupted { return cow!(""); }
			if x.kind() == IoErrorKind::WouldBlock { return cow!(""); }
			cow!("Can't accept connection: {}", x)
		})?;
		let info = ConnectionInfo::from_socket_addrs(addr, conn.local_addr().ok());
		let fd = conn.into_raw_fd(fd_count.clone());
		let tls = Connection::new(poll, allocator, config, fd, info, scache, fd_count).
			map_err(|x|cow!("Can't construct TLS connection: {}", x))?;
		let taddr = format!("{}", addr);
		Ok((tls, Cow::Owned(taddr)))
	}
}

struct GlobalState
{
	tokens: BTreeMap<usize, u64>,
	connections: BTreeMap<u64, Connection>,
	listeners: BTreeMap<usize, (Listener, AllocatedToken)>,
	next_conn_seq: u64,
	poll: Poll,
	tpool: TokenAllocatorPool,
	config: ServerConfiguration,
	scache: ServiceCache,
	fd_count: Arc<AtomicUsize>,
	start_drain_signal: Arc<AtomicBool>,
	max_fd_count: usize,				//Maximum fd count.
	last_timed_broadcast: SystemTime,
}

impl GlobalState
{
	fn broadcast_timed_event(&mut self)
	{
		//Limit broadcast frequency to 1/5s.
		let now = SystemTime::now();
		if let Ok(diff) = now.duration_since(self.last_timed_broadcast) {
			if diff.as_secs() < 5 { return; }
			self.last_timed_broadcast = now;
		}
		let mut to_kill = Vec::new();
		//Loop over all connections.
		for i in self.connections.iter_mut() {
			match catch_unwind(AssertUnwindSafe(||{
				i.1.handle_timed_event().is_err()
			})) {
				Ok(true) => (),
				Ok(false) => continue,	//Ok, leave alone.
				Err(_) => {
					//Ouch, we took a fatal exception in connection context. Try to kill the
					//connection and continue.
					i.1.hard_error = Some(Cow::Borrowed("Fatal exeception in connection \
						context"));
				}
			};
			//Need to tear down this connection.
			to_kill.push(*i.0);
		}
		//Kill those that need to be.
		for i in to_kill.iter() { self.kill_connection(*i); }
	}
	fn loop_events(&mut self)
	{
		let mut events = Events::with_capacity(1024);
		//If all our file descriptors are gone, exit.
		while self.fd_count.load(MemOrder::Relaxed) != 0 {
			if self.start_drain_signal.load(MemOrder::Relaxed) {
				//Ok, we got the signal that we should begin draining.
				writeln!(stderr(), "Starting preparing for exit by dumping listeners...").unwrap();
				for i in self.listeners.iter() {
					//For each listener, before dumping them, decrement the fd count and
					//unregister the poll. The token is dumped automatically.
					decrement_count(&self.fd_count);
					let _ = self.poll.deregister(&((i.1).0).socket);
				}
				//Dump all the listeners.
				self.listeners.clear();
				writeln!(stderr(), "Waiting for all connections to end...").unwrap();
				//Okay, processed it already.
				self.start_drain_signal.store(false, MemOrder::Relaxed);
				continue;
			}
			match self.poll.poll(&mut events, Some(Duration::from_secs(15))) {
				Ok(_) => (),
				Err(x) => {
					let mut not_fatal = false;
					not_fatal |= x.kind() == IoErrorKind::Interrupted;
					not_fatal |= x.kind() == IoErrorKind::WouldBlock;
					if not_fatal { continue; }
					writeln!(stderr(), "Error in poll: {}", x).unwrap();
					continue;
				}
			};
			for event in events.iter() {
				self.handle_event(event);
			}
			self.broadcast_timed_event();
		}
	}
	fn add_listener(&mut self, listener: Listener)
	{
		let token = match self.tpool.allocate() {
			Ok(x) => x,
			Err(_) => {
				writeln!(stderr(), "Can't allocate token for listener").unwrap();
				return;
			}
		};
		let listener_seq = token.value();
		match self.poll.register(&listener.socket, Token(listener_seq), Ready::readable(),
			PollOpt::empty()) {
			Ok(_) => (),
			Err(x) => {
				writeln!(stderr(), "poll.register (listener) error: {}", x).unwrap();
				return;
			}
		};
		increment_count(&self.fd_count);
		self.listeners.insert(listener_seq, (listener, token));
	}
	fn handle_event(&mut self, ev: Event)
	{
		let mut kill_connection_flag = false;
		let tok = ev.token().0;
		let kind = ev.readiness();
		if let Some(connid) = self.tokens.get(&tok).map(|x|*x) {
			//Ok, this is event related to a connection.
			let conn = match self.connections.get_mut(&connid) {
				Some(x) => x,
				None => {
					//Dangling token, delete.
					self.tokens.remove(&tok);
					return;
				}
			};
			//Fuck... Whatever the fault, this should be at least in good enough shape to tear down.
			//Seriously, Fuck UnwindSafe.
			let tmp_poll = &mut self.poll;
			kill_connection_flag = match catch_unwind(AssertUnwindSafe(||{
				conn.handle_event(tmp_poll, tok, &kind, connid).is_err()
			})) {
				Ok(x) => x,
				Err(_) => {
					//Ouch, we took a fatal exception in connection context. Try to kill the
					//connection and continue.
					conn.hard_error = Some(Cow::Borrowed("Fatal exeception in connection \
						context"));
					true	//Always kill the connection.
				}
			};
		} else if let Some(ref mut listener) = self.listeners.get_mut(&tok) {
			//Listener got connection.
			let (conn, addr) = match listener.0.accept(&mut self.poll, &mut self.tpool, &self.config,
				self.scache.clone(), self.fd_count.clone(), self.max_fd_count) {
				Ok((x, y)) => (x, y),
				Err(x) => {
					if x.deref().len() > 0 {
						writeln!(stderr(), "Failed to accept connection: {}", x).unwrap();
					}
					return;
				}
			};
			let next_seq = self.next_conn_seq;
			self.next_conn_seq += 1;
			//Add the tokens to lookup.
			self.tokens.insert(conn.token_socket.value(), next_seq);
			self.tokens.insert(conn.token_txirq.value(), next_seq);
			conn.service.add_tokens(&mut self.tokens, next_seq);
			self.connections.insert(next_seq, conn);
			writeln!(stderr(), "[#{}] New connection from {}", next_seq, addr).unwrap();
		} else {
			//We have no idea what this is for. Ignore it.
		}
		if kill_connection_flag {
			//Kill whatever connection this flag is set on.
			let connid = match self.tokens.get(&tok).map(|x|*x) {
				Some(x) => x,
				None => return		//WTF?
			};
			self.kill_connection(connid);
		}
	}
	fn kill_connection(&mut self, connid: u64)
	{
		if let Some(ref mut conn) = self.connections.get_mut(&connid) {
			let cause = match &conn.hard_error {
				&Some(Cow::Owned(ref x)) => Cow::Owned(x.clone()),
				&Some(Cow::Borrowed(x)) => Cow::Borrowed(x),
				&None => cow!("Unknown cause"),
			};
			writeln!(stderr(), "[#{}] Connection ending: {}", connid, cause).unwrap();
			//Kill the tokens.
			self.tokens.remove(&conn.token_socket.value());
			self.tokens.remove(&conn.token_txirq.value());
			conn.service.remove_tokens_and_pollers(&mut self.tokens, &mut self.poll);
			//Unregister the pollers.
			let _ = self.poll.deregister(&conn.external_socket);
			let _ = self.poll.deregister(&conn.txirq_receiver);
			//Notify that the connection is about to get destroyed.
			conn.destroy_notify();
		}
		//Finally, destroy the connection structure.
		self.connections.remove(&connid);
	}
}
