Some example scripts for btls-inetd under Systemd. This assumes that btls-inetd is installed as user account
btlsinetd, with home directory of /home/btlsinetd.

btlsinetd.service:
------------------
[Service]
Type=forking
PIDFile=/home/btlsinetd/run/daemon.pid
ExecStart=/home/btlsinetd/start.sh
ExecReload=/home/btlsinetd/reload.sh
ExecStop=/home/btlsinetd/stop.sh
WorkingDirectory=~
User=btlsinetd
LimitNOFILE=8192
NoNewPrivileges=yes

[Install]
WantedBy=multi-user.target

btlsinetd.socket:
-----------------
[Socket]
ListenStream=443
BindIPv6Only=both
Accept=false

[Install]
WantedBy=btlsinetd.service

start.sh:
---------
#!/bin/bash
cd ~
exec bin/btls-inetd bin/btls-inetd-proc --services=services --certificates=certificates --config=tls13,acmedir=/home/btlsinetd/acme --subprocesses=8 --pidfile=/home/btlsinetd/run/daemon.pid

reload.sh:
----------
#!/bin/bash
killall -USR1 btls-inetd

stop.sh:
--------
#!/bin/bash
killall -HUP btls-inetd
sleep 5
