//!Some error handling macros.
//!
//!This crate provodes error handling macros `fail!` and `fail_if!`.

#![forbid(unsafe_code)]
#![forbid(missing_docs)]

///Fail the current function with error.
///
///Takes in:
///
/// * A value `$f`.
///
///This macro wraps the value in `Err()` and then the result is immediately returned out of the current function
///using the `?` operator. Due to this, the return value underoes `From` trait conversions.
///
///This macro never returns, all code after it is dead.
#[macro_export]
macro_rules! fail
{
	($f:expr) => {{ Err($f)?; unreachable!() }};
}

///Fail the current function with error if condition is true.
///
///Takes in:
///
/// * A condition `$c`.
/// * A value `$f`.
///
///If the condition is true, This macro wraps the value in `Err()` and then the result is immediately returned out of
///the current function using the `?` operator. Due to this, the return value underoes `From` trait conversions.
#[macro_export]
macro_rules! fail_if
{
	($c: expr, $f:expr) => { if $c { Err($f)?; } };
}
