//!ECDSA signing
//!
//!This crate provodes routines for ECDSA signing.
#![forbid(unsafe_code)]
#![forbid(missing_docs)]

extern crate btls_aux_autogenerated_definitions;
#[macro_use]
extern crate btls_aux_fail;
extern crate btls_aux_hash;
extern crate btls_aux_keyconvert;
extern crate btls_aux_nettle;
extern crate btls_aux_securebuf;
extern crate btls_aux_serialization;
extern crate btls_aux_signature_algo;
extern crate btls_aux_signature_ecdsa;

use btls_aux_autogenerated_definitions::{CURVE_NSA_P256, CURVE_NSA_P384, CURVE_NSA_P521, ECDSA_KEY_OID};
use btls_aux_keyconvert::{EcdsaCurve, EcdsaKeyDecode};
use btls_aux_nettle::{EccCurve, EccPoint, EccScalar};
use btls_aux_securebuf::SecStackBuffer;
use btls_aux_signature_algo::{MajorSignatureAlgorithm, SignatureKeyPair, SignatureType};
use btls_aux_serialization::{ASN1_BIT_STRING, ASN1_INTEGER, ASN1_OID, ASN1_SEQUENCE, Sink, SliceSink};
use std::fmt::{Display, Error as FmtError, Formatter};
use std::ops::{Deref, DerefMut};

///Error from loading ECDSA key pair
///
///This enumeration describes the error that occured in loading of ECDSA key pair.
#[derive(Clone,Debug,PartialEq,Eq)]
pub enum EcdsaKeyLoadingError
{
	#[doc(hidden)]
	BadScalar,
	#[doc(hidden)]
	BadPoint,
	#[doc(hidden)]
	UnsupportedCurve,
	#[doc(hidden)]
	CantSerializePubkey,
	#[doc(hidden)]
	Hidden__
}

impl Display for EcdsaKeyLoadingError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use EcdsaKeyLoadingError::*;
		match self {
			&BadScalar => fmt.write_str("Bad scalar"),
			&BadPoint => fmt.write_str("Bad point"),
			&UnsupportedCurve => fmt.write_str("Unsupported curve"),
			&CantSerializePubkey => fmt.write_str("Can't serialize public key"),
			&Hidden__ => fmt.write_str("Hidden__")
		}
	}
}

///A ECDSA key pair.
///
///This structure represents a ECDSA key pair.
///
///The signatures are output as PKIX ECDSA signature format, as ASN.1 structure.
///
///Sufficient signature buffer sizes (including the needed overhead) are:
///
/// * Curve `NSA P-256`: 80 bytes.
/// * Curve `NSA P-384`: 112 bytes.
/// * Curve `NSA P-521`: 144 bytes.
pub struct EcdsaKeyPair(EccScalar, EccPoint);

impl EcdsaKeyPair
{
	///Create a new transient P-256 keypair.
	///
	///This method generates a new transient ECDSA with NSA P-256 keypair. The created key is volatile: There
	///is no way to export the private part of it.
	///
	///On success, returns `Ok((keypair, pubkey))`, where `keypair` is the ECDSA key pair, and `pubkey` is the
	///corresponding public key in the SubjectPublicKeyInfo format.
	///
	///On failure, returns `Err(())`.
	pub fn new_transient_ecdsa_p256() -> Result<(EcdsaKeyPair, Vec<u8>), ()>
	{
		const OUTPUT_SIZE: usize = 97;
		let crvid = 0;
		let crv = EccCurve::new(crvid)?;
		//Should be exactly 97 bytes.
		let mut output = [0;OUTPUT_SIZE];
		let mut output = SecStackBuffer::new(&mut output, OUTPUT_SIZE).unwrap();	//Exact size.
		let psize = crv.generate(output.deref_mut())?;
		fail_if!(psize != OUTPUT_SIZE, ());
		fail_if!(output[0] != 0, ());		//97 byte buffer.
		let (_, privkey, pubkey) = EcdsaKeyPair::keypair_load(output.deref()).map_err(|_|())?;
		Ok((privkey, pubkey))
	}
	///Deprecated, use `.SignatureKeyPair::keypair_sign()` instead.
	pub fn sign(&self, message: &[u8], signature: &mut [u8], alg: u16) -> Result<usize, ()>
	{
		self.keypair_sign(message, alg, signature)
	}
	///Deprecated, use `.SignatureKeyPair::keypair_load()` instead.
	pub fn from_bytes(data: &[u8]) -> Result<(u32, EcdsaKeyPair, Vec<u8>), EcdsaKeyLoadingError>
	{
		match EcdsaKeyPair::keypair_load(data) {
			Ok((x, y, z)) => Ok((x.get_id(), y, z)),
			Err(x) => Err(x)
		}
	}
	///Deprecated, use `.SignatureKeyPair::keypair_generate()` instead.
	pub fn generate(id: u32) -> Result<Vec<u8>, ()>
	{
		EcdsaKeyPair::keypair_generate(EcdsaCurve::by_id(id).ok_or(())?)
	}
}

impl SignatureKeyPair for EcdsaKeyPair
{
	type KeygenParameters = EcdsaCurve;
	type BadKeyError = EcdsaKeyLoadingError;
	type Variant = EcdsaCurve;
	fn keypair_generate(params: EcdsaCurve) -> Result<Vec<u8>, ()>
	{
		let crv = EccCurve::new(params.get_id())?;
		//The biggest is 3*66+1 bytes, so fits in 256 bytes.
		let mut output = [0; 256];
		let mut output = SecStackBuffer::new(&mut output, 256).unwrap();	//Always fits.
		let rep_bytes = crv.generate(output.deref_mut())?;
		Ok((&(output.deref())[..rep_bytes]).to_owned())
	}
	fn keypair_load(data: &[u8]) -> Result<(EcdsaCurve, EcdsaKeyPair, Vec<u8>), EcdsaKeyLoadingError>
	{
		let p = EcdsaKeyDecode::new(data).map_err(|_|EcdsaKeyLoadingError::UnsupportedCurve)?;
		let id = p.crv.get_id();
		let crv = EccCurve::new(id).map_err(|_|EcdsaKeyLoadingError::UnsupportedCurve)?;
		let mut scalar = EccScalar::new(crv);
		let mut point = EccPoint::new(crv);
		scalar.load(p.d).map_err(|_|EcdsaKeyLoadingError::BadScalar)?;
		point.load(p.x, p.y).map_err(|_|EcdsaKeyLoadingError::BadPoint)?;
		let (_, pubkey) = make_pubkey(p.crv, p.x, p.y).map_err(|_|
			EcdsaKeyLoadingError::CantSerializePubkey)?;
		Ok((p.crv, EcdsaKeyPair(scalar, point), pubkey))
	}
	fn keypair_sign(&self, message: &[u8], scheme: u16, output: &mut [u8]) -> Result<usize, ()>
	{
		let hashfunc = match SignatureType::by_tls_id(scheme).map(|x|x.get_major_type()) {
			Some(MajorSignatureAlgorithm::Ecdsa(hash)) => hash,
			_ => fail!(())
		};
		let hash = hashfunc.oneshot(|x|x.input(message)).map_err(|_|())?;
		let hash = hash.as_ref();
		let mut toutput = [0u8; 192];
		let outputlen = self.0.ecdsa_sign_hash(hash, &mut toutput)?;
		fail_if!(outputlen > toutput.len(), ());
		let toutput = &mut toutput[..outputlen];
		let (r, s) = toutput.split_at(toutput.len() >> 1);
		let signature_size = {
			let mut sink = SliceSink::new(output);
			//Pad the integers if needed, and strip leading zeros.
			sink.asn1_fn(ASN1_SEQUENCE, |sink| {
				sink.asn1_fn(ASN1_INTEGER, |sink| {
					let mut start = 0;
					while start < r.len() && r[start] == 0 { start += 1; }
					if r[start] >= 128 { sink.write_u8(0)?; }
					sink.write_slice(&r[start..])
				})?;
				sink.asn1_fn(ASN1_INTEGER, |sink| {
					let mut start = 0;
					while start < s.len() && s[start] == 0 { start += 1; }
					if s[start] >= 128 { sink.write_u8(0)?; }
					sink.write_slice(&s[start..])
				})?;
				Ok(())
			})?;
			sink.written()
		};
		Ok(signature_size)
	}
}

fn make_pubkey(crv: EcdsaCurve, x: &[u8], y: &[u8]) -> Result<(u32, Vec<u8>), ()>
{
	//Format the SPKI.
	let mut sink = Vec::new();
	sink.asn1_fn(ASN1_SEQUENCE, |sink| {
		sink.asn1_fn(ASN1_SEQUENCE, |sink| {
			sink.asn1_fn(ASN1_OID, |sink| {
				sink.write_slice(&ECDSA_KEY_OID)
			})?;
			sink.asn1_fn(ASN1_OID, |sink| {
				match crv {
					EcdsaCurve::NsaP256 => sink.write_slice(&CURVE_NSA_P256),
					EcdsaCurve::NsaP384 => sink.write_slice(&CURVE_NSA_P384),
					EcdsaCurve::NsaP521 => sink.write_slice(&CURVE_NSA_P521),
				}
			})
		})?;
		sink.asn1_fn(ASN1_BIT_STRING, |sink| {
			let rlen = crv.get_component_bytes();
			sink.write_u16(4)?;

			for _ in x.len()..rlen { sink.write_u8(0)?; }
			if x.len() <= rlen {
				sink.write_slice(&x)
			} else {
				sink.write_slice(&x[x.len()-rlen..])
			}?;

			for _ in y.len()..rlen { sink.write_u8(0)?; }
			if y.len() <= rlen {
				sink.write_slice(&y)
			} else {
				sink.write_slice(&y[y.len()-rlen..])
			}?;
			Ok(())
		})
	})?;
	Ok((crv.get_id(), sink))
}

#[cfg(test)]
fn do_test(tlsid: u16, key_bytes: &[u8])
{
	use self::btls_aux_keyconvert::convert_key_from_sexpr;
	use self::btls_aux_signature_algo::{SignatureAlgorithm, SignatureType, WrappedRecognizedSignatureAlgorithm};
	use self::btls_aux_signature_ecdsa::ecdsa_verify_pkix;
	let (_, key_bytes) = convert_key_from_sexpr(&key_bytes[..]).unwrap();
	let (_, keypair, pubkey) = EcdsaKeyPair::from_bytes(&key_bytes).unwrap();
	let msg = "Hello, World!";
	let mut signature = [0;160];
	let signaturelen = keypair.sign(msg.as_bytes(), &mut signature, tlsid).unwrap();
	let signature = &signature[..signaturelen];
	let algo = SignatureType::by_tls_id(tlsid).unwrap();
	let algo = WrappedRecognizedSignatureAlgorithm::from(SignatureAlgorithm::Tls(algo)).unwrap();
	ecdsa_verify_pkix(&pubkey, msg.as_bytes(), &algo, &signature, 0).unwrap();
}


#[test]
fn sign_ecdsa_sha256_p521()
{
	do_test(0x403, include_bytes!("testecdsa.sexp"));
}

#[test]
fn sign_ecdsa_sha384_p521()
{
	do_test(0x503, include_bytes!("testecdsa.sexp"));
}

#[test]
fn sign_ecdsa_sha512_p521()
{
	do_test(0x603, include_bytes!("testecdsa.sexp"));
}

#[test]
fn sign_ecdsa_sha256_p384()
{
	do_test(0x403, include_bytes!("testecdsa2.sexp"));
}

#[test]
fn sign_ecdsa_sha384_p384()
{
	do_test(0x503, include_bytes!("testecdsa2.sexp"));
}

#[test]
fn sign_ecdsa_sha512_p384()
{
	do_test(0x603, include_bytes!("testecdsa2.sexp"));
}

#[test]
fn sign_ecdsa_sha256_p256()
{
	do_test(0x403, include_bytes!("testecdsa3.sexp"));
}

#[test]
fn sign_ecdsa_sha384_p256()
{
	do_test(0x503, include_bytes!("testecdsa3.sexp"));
}

#[test]
fn sign_ecdsa_sha512_p256()
{
	do_test(0x603, include_bytes!("testecdsa3.sexp"));
}


