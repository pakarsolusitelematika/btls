//!Simple implementation of futures.
//!
//!This crate provodes a simple implementation of futures.
//!
//!Futures represent result of computation that might not have been finished yet. This is handy for representing
//!asynchronous processes.
//!
//!This module has the following things:
//!
//! * `FutureReceiver`: Object that acts as the result of computation that has possibly not finished yet.
//! * `FutureSender`: Object that acts as capability to produce the value of `FutureReceiver`.
//! * `FutureCallback`: A trait for callback to call on producing the value of future. Useful for chaining
//!futures.
//! * `create_future`: A function that creates a future value and the capability to produce the value.

#![forbid(unsafe_code)]
#![forbid(missing_docs)]

use std::mem::swap;
use std::sync::{Arc, Mutex};

///A function to call when future has produced its value.
///
///This trait is interface to call when the value of future is produced. It is object-safe, so objects of
///structures implementing this trait can be boxed as objects of this trait.
pub trait FutureCallback<T: Sized+Send>
{
	///The future has its final value assigned.
	///
	///Takes in:
	///
	/// * A callback receiver `self`.
	/// * A value `value`.
	///
	///This method is called when the value of future is produced. The method then returns the value
	///that is actually produced as the value.
	///
	///The reason for allowing the callback to alter the value produced is to allow chaining to work with
	///affine types. If the original future is never read, then the returned value can be any valid value of
	///the type, even nonsensical one. E.g. `Err(x)` for `Result` type.
	fn on_settled(&mut self, value: T) -> T;
}

///A future sender.
///
///This structure act as a capability to produce the value of future.
#[derive(Clone)]
pub struct FutureSender<T: Sized+Send>
{
	value: Arc<Mutex<FutureReceiverInner<T>>>
}

impl<T:Sized+Send> FutureSender<T>
{
	///Produce the final value of future.
	///
	///Takes in:
	///
	/// * A future sender `self`.
	/// * A value `value`.
	///
	///This method consumes the future sender and produces the value on the associated future. If the future
	///has a callback set, that callback is called, and the result is used as the produced value.
	pub fn settle(self, val: T)
	{
		match self.value.lock() {
			Ok(mut x) =>  {
				let mut func = None;
				swap(&mut x.func, &mut func);
				if let Some(mut func2) = func {
					x.value = Some(func2.on_settled(val));
				} else {
					x.value = Some(val);
				}
			}
			Err(_) => (),
		}
	}
}

struct FutureReceiverInner<T: Sized+Send>
{
	func: Option<Box<FutureCallback<T>+Send>>,
	value: Option<T>
}

#[derive(Clone)]
enum _FutureReceiver<T: Sized+Send>
{
	Delayed(Arc<Mutex<FutureReceiverInner<T>>>),
	Immediate(T),
}

///A future value.
///
///This structure represents a value that might not have been computed yet.
///
///If created via the `From` trait, the final value of the future is the value converted.
///
///For more information about futures, see [`create_future`].
///
///[`create_future`]: fn.create_future.html
#[derive(Clone)]
pub struct FutureReceiver<T: Sized+Send>(_FutureReceiver<T>);

impl<T:Sized+Send> From<T> for FutureReceiver<T>
{
	fn from(val: T) -> FutureReceiver<T>
	{
		FutureReceiver(_FutureReceiver::Immediate(val))
	}
}

impl<T:Sized+Send> FutureReceiver<T>
{
	///Set function to call when the value of future is produced.
	///
	///Takes in:
	///
	/// * A future `self`.
	/// * A callback `cb`.
	///
	///This method sets that the future calls the callback when the value is produced. However, if the value
	///of the future has already been produced, this does nothing.
	///
	///If the future does not have produced a value, returns `None`.
	///
	///If the future does have produced a value, returns `Some(cb)`, where `cb` is the callback.
	///
	///This is meant to be used like:
	///
	///```no-run
	/// //ctx is some object that implements FutureReceiver<T> trait, and has further FutureSender<U> field.
	///if let Some(mut ctx) = source.settled_cb(Box::new(ctx)) {
	///	//Ok, the signing is already complete, Forward the value.
	///	let _ = match source.read() {
	///		Ok(x) => ctx.on_settled(x),
	///		_ => return FutureReceiver::from(Err(())),
	///	};
	///}
	///```
	pub fn settled_cb(&mut self, cb: Box<FutureCallback<T>+Send>) -> Option<Box<FutureCallback<T>+Send>>
	{
		match &self.0 {
			&_FutureReceiver::Delayed(ref x) =>  match x.lock() {
				Ok(mut y) => {
					if y.value.is_none() {
						y.func = Some(cb);
						return  None;
					}
				}
				Err(_) => return None,
			},
			&_FutureReceiver::Immediate(_) => ()
		};
		return Some(cb);
	}
	///Is the final value produced yet?
	///
	///Takes in:
	///
	/// * A future reciver `self`.
	///
	///This method returns `true` if the value of future has been produced, `false` otherwise.
	pub fn settled(&self) -> bool
	{
		match &self.0 {
			&_FutureReceiver::Delayed(ref x) =>  match x.lock() {
				Ok(y) => y.value.is_some(),
				Err(_) => false,
			},
			&_FutureReceiver::Immediate(_) => true,
		}
	}
	///Read the produced value of the future.
	///
	///Takes in:
	///
	/// * A future reciver `self`.
	///
	///This method consumes the future and returns the produced value.
	///
	///If the value has been produced, returns `Ok(value)`, where `value` is the produced value.
	///
	///If the value has not been produced, returns `Err(self)`, where `self` is the future.
	pub fn read(self) -> Result<T, FutureReceiver<T>>
	{
		match self.0 {
			_FutureReceiver::Immediate(x) => return Ok(x),
			_FutureReceiver::Delayed(y) => {
				if let Ok(mut z) = y.lock() {
					let mut x = None;
					swap(&mut z.value, &mut x);
					match x {
						Some(w) => return Ok(w),
						None => (),
					};
				};
				return Err(FutureReceiver(_FutureReceiver::Delayed(y)));
			}
		}
	}
}

///Create a future value.
///
///This function creates a `(sender, receiver)` pair for a future value of type `T`.
///
///It returns `(sender, receiver)`, where `sender` is the object that can produce the value, and `receiver` is the
///value of computation, initially not known.
///
///# Futures:
///
///Futures model computation that happens asynchronously, and whose final value may not be known yet.
///When the value is finally produced at end of computation, the value of computation can then be extracted from
///the future.
///
///The computation invoked can also be synchronous: In that case, the value is produced during invocation of the
///computation.
///
///Because the values are stored, they need to be `Sized` and because the values can cross threads,
///the values need to be `Send`. Because no synchronous mutation is possible, `Sync` is not needed.
///
///In this implementation of futures, one can:
///
/// * Create an future representing unfinished computation, and object that can produce the value of the computation
///(using the `.settle()` method, even across threads), by using the `create_future()` function.
/// * Create an future with immediately produced value by using the [`FutureReceiver::from`] method (from `From`
///trait).
/// * Check if value has been produced by using the `.settled()` method on the receiver.
/// * When the value has been produced, read the value using `.read()` value on receiver, consuming the receiver in
///process.
///
///[`FutureReceiver::from`]: struct.FutureReceiver.html
pub fn create_future<T:Sized+Send>() -> (FutureSender<T>, FutureReceiver<T>)
{
	let inner = Arc::new(Mutex::new(FutureReceiverInner{value:None, func:None}));
	let sender = FutureSender{value:inner.clone()};
	let receiver = FutureReceiver(_FutureReceiver::Delayed(inner));
	(sender, receiver)
}
