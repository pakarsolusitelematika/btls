use ::core::ops::{Range, RangeFrom, RangeFull, RangeTo};

///Trait for index range.
///
///This trait models a index range. It is implemented by all the standard four range types over usize.
///
///The only purpose for existence of this trait is that the standard range notations, e.g. 1..5 can be used with
///extension trait [`SlicingExt`] methods.
///
///[`SlicingExt`]: trait.SlicingExt.html
pub trait RangeTrait
{
	///Get the starting index of the range.
	///
	///Takes in:
	///
	/// * The index range `self`.
	///
	///This method returns the starting index of the range.
	fn start(&self) -> usize;
	///Get the one past ending index of the range.
	///
	///Takes in:
	///
	/// * The index range `self`.
	///
	///This method returns one-past-end of the range.
	///
	///If the range has an end, returns `Some(one_past_end)`, where `one_past_end` is first element index not
	///in range. Otherwise returns `None`.
	fn end(&self) -> Option<usize>;
}

///Extension trait implementing non-panicing subslicing.
///
///This trait provodes methods to extract subslice from slice without panicing. Great for extracting slices with
///indexes of unknown provenance.
///
///This trait is blanket-implemented for all slice types (`[T]`).
pub trait SlicingExt
{
	///Extract subslice range.
	///
	///Takes in:
	///
	/// * A buffer `self`.
	/// * A range `r`.
	///
	///This method extracts the range from the buffer as subslice.
	///
	///If the range is in-bounds, returns `Some(subslice)`, where `subslice` is the extracted subslice.
	///Otherwise, returns `None`.
	fn slice_np<X:RangeTrait>(&self, r: X) -> Option<&Self>;
	///Extract mutable subslice range.
	///
	///Takes in:
	///
	/// * A mutable buffer `self`.
	/// * A range `r`.
	///
	///The same as `.slice_np()`, but instead extracts a mutable subslice.
	fn slice_np_mut<X:RangeTrait>(&mut self, r: X) -> Option<&mut Self>;
	///Split a slice into two at index.
	///
	///Takes in:
	///
	/// * A buffer `self`.
	/// * An index `pt`.
	///
	///This method splits the buffer into two parts, the part before the index and the part from that
	///onwards.
	///
	///If the index is in-range (including being self.len()), returns `Some((x, y))`, where `x` is the
	///subslice before `pt` and `y` the remainder. Otherwise, returns `None`.
	fn split_at_np<'a>(&'a self, pt: usize) -> Option<(&'a Self, &'a Self)>;
	///Extract subslice of specified length starting at specified index.
	///
	///Takes in:
	///
	/// * A buffer `self`.
	/// * A starting index `start`.
	/// * A length `len`.
	///
	///This method slices an subslice with the specified starting index and length out of the buffer.
	///
	///If the range is in-bounds, returns `Some(subslice)`, where `subslice` is the extracted subslice.
	///Otherwise, returns `None`.
	fn slice_np_len(&self, start: usize, len: usize) -> Option<&Self>
	{
		let end = match start.checked_add(len) { Some(x) => x, None => return None };
		self.slice_np(start..end)
	}
	///Extract mutable subslice of specified length starting at specified index.
	///
	///Takes in:
	///
	/// * A mutable buffer `self`.
	/// * A starting index `start`.
	/// * A length `len`.
	///
	///The same as `.slice_np_len()`, but instead extracts a mutable subslice.
	fn slice_np_len_mut(&mut self, start: usize, len: usize) -> Option<&mut Self>
	{
		let end = match start.checked_add(len) { Some(x) => x, None => return None };
		self.slice_np_mut(start..end)
	}
}



fn extract_range<X:RangeTrait>(slen: usize, r: X) -> Option<Range<usize>>
{
	let start = r.start();
	let end = r.end().unwrap_or(slen);
	if start > end || end > slen {
		//println!("slice_np failed: range={}..{}, length={}", start, end, self.len());
		//unreachable!();
		return None;
	}
	Some(start..end)
}

impl<T:Sized> SlicingExt for [T]
{
	fn slice_np<X:RangeTrait>(&self, r: X) -> Option<&Self>
	{
		let range = match extract_range(self.len(), r) { Some(x) => x, None => return None };
		Some(&self[range])
	}
	fn slice_np_mut<X:RangeTrait>(&mut self, r: X) -> Option<&mut Self>
	{
		let range = match extract_range(self.len(), r) { Some(x) => x, None => return None };
		Some(&mut self[range])
	}
	fn split_at_np<'a>(&'a self, pt: usize) -> Option<(&'a Self, &'a Self)>
	{
		if let (Some(a), Some(b)) = (self.slice_np(..pt), self.slice_np(pt..)) {
			Some((a, b))
		} else {
			None
		}
	}
}

impl RangeTrait for Range<usize>
{
	fn start(&self) -> usize { self.start }
	fn end(&self) -> Option<usize> { Some(self.end) }
}

impl RangeTrait for RangeTo<usize>
{
	fn start(&self) -> usize { 0 }
	fn end(&self) -> Option<usize> { Some(self.end) }
}

impl RangeTrait for RangeFrom<usize>
{
	fn start(&self) -> usize { self.start }
	fn end(&self) -> Option<usize> { None }
}

impl RangeTrait for RangeFull
{
	fn start(&self) -> usize { 0}
	fn end(&self) -> Option<usize> { None }
}
