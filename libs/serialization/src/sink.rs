//Silence warnings about std_imports if !stdlib.
#![allow(unused_imports)]

use super::{Asn1Tag, LengthTrait};
#[cfg(test)]
extern crate std;
#[cfg(feature="stdlib")]
mod std_imports
{
	extern crate std;
	pub use self::std::vec::Vec;
}
#[cfg(not(feature="stdlib"))]
mod std_imports {}
use self::std_imports::*;

///Sink that outputs raw byte data.
///
///This trait models a sink for TLS structures or ASN.1 structures (encoded as DER).
///
///This trait is implemented by struct [`SliceSink`], and additionally if this library was compiled with the `stdlib`
///feature, `Vec<u8>` implements this trait as well.
///
///The sinks implemented by [`SliceSink`] have fixed capacity, the sinks implemented by `Vec<u8>` have no fixed
///capacity limit, but are resized when needed.
///
///[`SliceSink`]: struct.SliceSink.html
pub trait Sink
{
	///Write a slice into sink.
	///
	///Takes in:
	///
	/// * A sink `self`.
	/// * Slice of data to append `data`.
	///
	///This method appends the slice into the sink.
	///
	///On success, returns `Ok(())`.
	///
	///If the write exceeds the capacity of the sink, returns `Err(())`.
	fn write_slice(&mut self, data: &[u8]) -> Result<(), ()>;
	///Alter past-written byte in the sink.
	///
	///Takes in:
	///
	/// * A sink `self`.
	/// * An index `ptr`.
	/// * A byte `data`.
	///
	///This method alters the sink at the index into the byte.
	///
	///Attempts to alter invalid indices (pointing outside data written so far) are silently ignored.
	fn _alter(&mut self, ptr: usize, data: u8);
	///Read back past-written byte in the sink.
	///
	///Takes in:
	///
	/// * A sink `self`.
	/// * An index `ptr`.
	///
	///This method reads the byte at the index in the sink.
	///
	///If the specified index is invalid (pointing outside data written so far), returns 0.
	fn _readback(&self, ptr: usize) -> u8;
	///Get number of bytes written so far.
	///
	///Takes in:
	///
	/// * A sink `self`.
	///
	///This method returns the number of bytes that have been written into the sink so far.
	fn written(&self) -> usize;
	///Remove data from end of sink.
	///
	///Takes in:
	///
	/// * A sink `self`.
	/// * The amount of data to remove `amount`.
	///
	///This method removes the specified amount of bytes from the end of the sink.
	///
	///If the amount is greater than all data written so far, the sink is emptied.
	fn _pop(&mut self, amount: usize);
	///Write a 8-bit value into sink.
	///
	///Takes in:
	///
	/// * A sink `self`.
	/// * A 8-bit value `data`.
	///
	///This method writes the value into the sink.
	///
	///On success, returns `Ok(())`.
	///
	///If the write exceeds the capacity of the sink, returns `Err(())`.
	fn write_u8(&mut self, data: u8) -> Result<(),()>
	{
		self.write_slice(&[data])
	}
	///Write a big-endian 16-bit value into sink.
	///
	///Takes in:
	///
	/// * A sink `self`.
	/// * A 16-bit value `data`.
	///
	///This method appends the value into the sink, using big-endian order.
	///
	///On success, returns `Ok(())`.
	///
	///If the write exceeds the capacity of the sink, returns `Err(())`.
	fn write_u16(&mut self, data: u16) -> Result<(),()>
	{
		self.write_slice(&[(data>>8) as u8, data as u8])
	}
	///Write a big-endian 24-bit value into sink.
	///
	///Takes in:
	///
	/// * A sink `self`.
	/// * A 24-bit value `data`.
	///
	///This method appends the value into the sink, using big-endian order.
	///
	///Note: Upper 8 bits of the value are ignored.
	///
	///On success, returns `Ok(())`.
	///
	///If the write exceeds the capacity of the sink, returns `Err(())`.
	fn write_u24(&mut self, data: u32) -> Result<(),()>
	{
		self.write_slice(&[(data>>16) as u8, (data>>8) as u8, data as u8])
	}
	///Write a big-endian 32-bit value into sink.
	///
	///Takes in:
	///
	/// * A sink `self`.
	/// * A 32-bit value `data`.
	///
	///This method appends the value into the sink, using big-endian order.
	///
	///On success, returns `Ok(())`.
	///
	///If the write exceeds the capacity of the sink, returns `Err(())`.
	fn write_u32(&mut self, data: u32) -> Result<(),()>
	{
		self.write_slice(&[(data>>24) as u8, (data>>16) as u8, (data>>8) as u8, data as u8])
	}
	///Write a big-endian 64-bit value into sink.
	///
	///Takes in:
	///
	/// * A sink `self`.
	/// * A 64-bit value `data`.
	///
	///This method appends the value into the sink, using big-endian order.
	///
	///On success, returns `Ok(())`.
	///
	///If the write exceeds the capacity of the sink, returns `Err(())`.
	fn write_u64(&mut self, data: u64) -> Result<(),()>
	{
		self.write_slice(&[(data>>56) as u8, (data>>48) as u8, (data>>40) as u8, (data>>32) as u8,
			(data>>24) as u8, (data>>16) as u8, (data>>8) as u8, data as u8])
	}
	///Alter past-written 8-bit value in the sink.
	///
	///Takes in:
	///
	/// * A sink `self`.
	/// * An index `ptr`.
	/// * A 8-bit value `data`.
	///
	///This method modifies the index in the sink to contain the value.
	///
	///On success, returns `Ok(())`.
	///
	///If the index points to outside data written so far, returns `Err(())`.
	fn alter_u8(&mut self, ptr: usize, data: u8) -> Result<(),()>
	{
		fail_if!(ptr >= self.written(), ());
		self._alter(ptr, data);
		Ok(())
	}
	///Alter past-written big-endian 16-bit value in the sink.
	///
	///Takes in:
	///
	/// * A sink `self`.
	/// * An index `ptr`.
	/// * A 16-bit value `data`.
	///
	///This method modifies the index in the sink to contain the value, in big-endian order.
	///
	///On success, returns `Ok(())`.
	///
	///If the index points to outside data written so far, returns `Err(())`.
	fn alter_u16(&mut self, ptr: usize, data: u16) -> Result<(),()>
	{
		let ptr1 = ptr.checked_add(1).ok_or(())?;
		fail_if!(ptr1 >= self.written(), ());
		self._alter(ptr, (data>>8) as u8);
		self._alter(ptr1, data as u8);
		Ok(())
	}
	///Alter past-written big-endian 24-bit value in the sink.
	///
	///Takes in:
	///
	/// * A sink `self`.
	/// * An index `ptr`.
	/// * A 24-bit value `data`.
	///
	///This method modifies the index in the sink to contain the value, in big-endian order.
	///
	///Note that upper 8 bits of the value are ignored.
	///
	///On success, returns `Ok(())`.
	///
	///If the index points to outside data written so far, returns `Err(())`.
	fn alter_u24(&mut self, ptr: usize, data: u32) -> Result<(),()>
	{
		let ptr2 = ptr.checked_add(2).ok_or(())?;
		let ptr1 = ptr.checked_add(1).ok_or(())?;
		fail_if!(ptr2 >= self.written(), ());
		self._alter(ptr, (data>>16) as u8);
		self._alter(ptr1, (data>>8) as u8);
		self._alter(ptr2, data as u8);
		Ok(())
	}
	///Read back past-written big-endian 24-bit value in the sink.
	///
	///Takes in:
	///
	/// * A sink `self`.
	/// * An index `ptr`.
	///
	///This method reads the big-endian 24-bit value at the index in the sink.
	///
	///On success, returns `Ok(value)`, where `value` is the value read.
	///
	///If the index points to outside data written so far, returns `Err(())`.
	fn readback_u24(&self, ptr: usize) -> Result<u32,()>
	{
		let ptr2 = ptr.checked_add(2).ok_or(())?;
		let ptr1 = ptr.checked_add(1).ok_or(())?;
		fail_if!(ptr2 >= self.written(), ());
		let mut x = 0;
		x |= (self._readback(ptr) as u32) << 16;
		x |= (self._readback(ptr1) as u32) << 8;
		x |= self._readback(ptr2) as u32;
		Ok(x)
	}
	///Get a marker for current position.
	///
	///Takes in:
	///
	/// * A sink `self`.
	///
	///This method returns a marker that represents the current end of write position in the sink.
	fn marker_posonly(&self) -> SinkMarker
	{
		SinkMarker(self.written(), 0)
	}
	///Write a big-endian length marker followed by data from function.
	///
	///Takes in:
	///
	/// * A sink `self`.
	/// * A length range `length`.
	/// * A closure `cb`. The closure takes a `&mut Sink` as an argument.
	///
	///This method first writes a big-endian length marker into the sink with size appropriate for the range.
	///It then calls the closure and appends the output of the closure into the same sink.
	///
	///The rules for chosing the length marker size are the follows:
	///
	/// * If the range is a `usize`, the length marker is omitted. 
	/// * If the upper limit of the range is at most 255, the length marker is 1 byte.
	/// * If the upper limit of the range is at most 65535, the length marker is 2 bytes.
	/// * If the upper limit of the range is at most 16777215, the length marker is 3 bytes.
	///
	///This operation is very common when serializing TLS structures. Specifically, it deals with serialization
	///of the TLS vector syntax.
	///
	///On success, returns `Ok(())`.
	///
	///On failure, returns `Err(())`. At least the following factors cause the method to fail:
	///
	/// * Write causes capacity of the sink to be exceeded.
	/// * The closure tries to write too much data (the length can't be encoded with length marker of deduced
	///size).
	/// * The closure returns `Err(())`.
	fn vector_fn<'a,F,L:LengthTrait>(&'a mut self, length: L, mut cb: F) -> Result<(),()> where Self: Sized,
		F: FnMut(&mut Self) -> Result<(),()>
	{
		let marker = match length.get_bytes() {
			0 => {
				SinkMarker(self.written(), 0)
			},
			1 => {
				self.write_u8(0)?;
				SinkMarker(self.written(), 1)
			},
			2 => {
				self.write_u16(0)?;
				SinkMarker(self.written(), 2)
			},
			3 => {
				self.write_u24(0)?;
				SinkMarker(self.written(), 3)
			},
			_ => fail!(())
		};
		cb(self)?;
		marker.commit(self).map_err(|_|())?;
		Ok(())
	}
	///Write a ASN.1 TLV of specified tag with data from specified closure.
	///
	///Takes in:
	///
	/// * A sink `self`.
	/// * A ASN.1 tag `tag`.
	/// * A closure `cb`. The closure takes a `&mut Sink` as an argument.
	///
	///This method writes ASN.1 TLV into the sink, with specified tag, and the value is as emitted by the
	///closure.
	///
	///Note: This method may need up to 3 bytes of temporary scratch space in the sink, so writing a `SliceSink`
	///to capacity may not work.
	///
	///On success, returns `Ok(())`.
	///
	///On failure, returns `Err(())`. At least the following factors cause the method to fail:
	///
	/// * Write causes capacity of the sink to be exceeded.
	/// * The closure tries to write over 16,777,215 bytes of data.
	/// * The closure returns `Err(())`.
	fn asn1_fn<'a, F>(&'a mut self, tag: Asn1Tag, mut cb: F) -> Result<(),()> where Self: Sized,
		F: FnMut(&mut Self) -> Result<(),()>
	{
		let (base,tag) = match tag {
			Asn1Tag::Universal(x, y) => (0x00 + if y { 0x20 } else { 0x00 }, x),
			Asn1Tag::Application(x, y) => (0x40 + if y { 0x20 } else { 0x00 }, x),
			Asn1Tag::ContextSpecific(x, y) => (0x80 + if y { 0x20 } else { 0x00 }, x),
			Asn1Tag::Private(x, y) => (0xC0 + if y { 0x20 } else { 0x00 }, x),
		};
		if tag < 31 {
			self.write_u8(base | (tag as u8))?;
		} else {
			self.write_u8(base | 31)?;
			//63, 56, 49, ..., 7
			for i in 0..9 {
				let j = 63 - 7 * i;
				if tag >= 1u64<<j { self.write_u8(128 | ((tag >> j) as u8))?; }
			}
			self.write_u8((tag & 127) as u8)?;
		}
		self.write_u32(0x83000000)?;	//83 00 00 00
		let w = self.written();
		let marker = SinkMarker(w, 255);
		cb(self)?;
		marker.commit(self).map_err(|_|())?;
		Ok(())
	}
	///Request callback with data after marker.
	///
	///Takes in:
	///
	/// * A sink `self`.
	/// * A marker `marker`.
	/// * A closure `cb`. The closure takes a `&[u8]` as an argument.
	///
	///This method causes the callback to be called with all data written to the sink since the marker.
	///
	///This is intended to perform readback of range written into sink, for purposes such as emitting a signature
	///or a MAC for the data written.
	///
	///Example of use:
	///
	///```no-run
	///let marker = sink.marker_posonly();
	///let mut signature = Vec::new();
	///let mut err = Ok(());
	/// //Write more data into sink.
	///sink.callback_after(marker, |x|{
	///     signature = match key.sign(x) { Ok(x) => x, Err(x) => err = Err(x); };
	///});
	///err?;
	///
	///```
	fn callback_after(&self, marker: SinkMarker, cb: &mut FnMut(&[u8])->());
}

///A position marker in sink
///
///This structure is a position marker in a sink. It is used in `Sink::callback_after`, and constructed by
///`Sink::marker_posonly`.
pub struct SinkMarker(usize, usize);

impl SinkMarker
{
	//This is now only internally used. The only markers externally exposed are ones from marker_posonly(),
	//and those don't do anything if passed hre.
	#[doc(hidden)]
	pub fn commit<S:Sink>(mut self, sink: &mut S) -> Result<(), usize>
	{
		fail_if!(sink.written() < self.0, 0usize);
		let size = sink.written() - self.0;
		match self.1 {
			0 => {
				//Do nothing, nothing to fix up.
				Ok(())
			},
			1 => {
				fail_if!(size > 255, size);
				fail_if!(self.0 < 1, size);
				sink.alter_u8(self.0 - 1, size as u8).map_err(|_|size)
			},
			2 => {
				fail_if!(size > 65535, size);
				fail_if!(self.0 < 2, size);
				sink.alter_u16(self.0 - 2, size as u16).map_err(|_|size)
			},
			3 => {
				fail_if!(size > 16777215, size);
				fail_if!(self.0 < 3, size);
				sink.alter_u24(self.0 - 3, size as u32).map_err(|_|size)
			},
			255 => {	//ASN.1.
				fail_if!(self.0 < 4, size);	//So substracts will be in-range. The last only
								//needs 3, but there is fourth byte as well.
				if size < 128 {
					//We need to write <len> and shift data by 3.
					sink.alter_u8(self.0 - 4, size as u8).map_err(|_|size)?;
					self.shift(sink, 3, size)
				} else if size < 256 {
					//We need to write 0x81 <len> and shift data by 2.
					sink.alter_u8(self.0 - 4, 0x81).map_err(|_|size)?;
					sink.alter_u8(self.0- 3, size as u8).map_err(|_|size)?;
					self.shift(sink, 2, size)
				} else if size < 65536 {
					//We need to write 0x82 <len> and shift data by 1.
					sink.alter_u8(self.0 - 4, 0x82).map_err(|_|size)?;
					sink.alter_u16(self.0 - 3, size as u16).map_err(|_|size)?;
					self.shift(sink, 1, size)
				} else if size < 16777216 {
					//The length of length is already 0x83, just alter the actual length.
					sink.alter_u24(self.0 - 3, size as u32).map_err(|_|size)
				} else {
					fail!(size);
				}
			},
			_ => fail!(size)
		}
	}
	fn shift<S:Sink>(&mut self, sink: &mut S, amount: usize, size: usize) -> Result<(), usize>
	{
		let mut idx = self.0;
		let maxlen = sink.written();
		fail_if!(idx < amount, size);	//The first iteration is the most extreme.
		while idx < maxlen {
			let x = sink._readback(idx);
			//idx >= amount, so substraction is valid.
			sink.alter_u8(idx - amount, x).map_err(|_|size)?;
			idx += 1;	//This is an index into valid array and never overflows usize.
		}
		sink._pop(amount);
		Ok(())
	}
}

///Sink that backs to writeable slice.
///
///This structure is a [`Sink`] that is backed by a mutable slice.
///
///The maximum amount of writable data is bounded by the size of the slice.
///
///Note that this mutably borrows the buffer. Thus, the way of using this structure is:
///
///```no-run
///let mut some_buffer = [0u8;192];
///let some_buffer_len = {
///	let sink = SliceSink::new(&mut some_buffer);
///	//Write data into sink here...
///	sink.written()
///}
///let some_buffer = &some_buffer[..some_buffer_len];
/// //Now, some_buffer contains the written data.
///
///```
///
///[`Sink`]: trait.Sink.html
pub struct SliceSink<'a>(&'a mut [u8], usize);

impl<'a> SliceSink<'a>
{
	///Create a new slice sink
	///
	///Takes in:
	///
	/// * A backing slice `slice`.
	///
	///This method creates a new slice sink, using the slice as backing storage.
	pub fn new(slice: &'a mut [u8]) -> SliceSink<'a>
	{
		SliceSink(slice, 0)
	}
}

impl<'a> Sink for SliceSink<'a>
{
	fn write_slice(&mut self, data: &[u8]) -> Result<(),()>
	{
		use super::SlicingExt;
		self.0.slice_np_len_mut(self.1, data.len()).ok_or(())?.copy_from_slice(data);
		self.1 += data.len();
		Ok(())
	}
	fn _alter(&mut self, ptr: usize, data: u8)
	{
		match self.0.get_mut(ptr) {
			Some(x) => *x = data,
			None => ()
		};
	}
	fn _readback(&self, ptr: usize) -> u8
	{
		self.0.get(ptr).map(|x|*x).unwrap_or(0)
	}
	fn written(&self) -> usize { self.1 }
	fn _pop(&mut self, amount: usize)
	{
		self.1 = self.1.saturating_sub(amount);
	}
	fn callback_after(&self, marker: SinkMarker, cb: &mut FnMut(&[u8])->())
	{
		use super::SlicingExt;
		match self.0.slice_np(marker.0..) {
			Some(x) => cb(x),
			None => cb(&[])
		};
	}
}

#[cfg(feature="stdlib")]
impl Sink for Vec<u8>
{
	fn write_slice(&mut self, data: &[u8]) -> Result<(),()>
	{
		self.extend_from_slice(data);
		Ok(())
	}
	fn _alter(&mut self, ptr: usize, data: u8)
	{
		match self.get_mut(ptr) {
			Some(x) => *x = data,
			None => ()
		};
	}
	fn _readback(&self, ptr: usize) -> u8
	{
		self.get(ptr).map(|x|*x).unwrap_or(0)
	}
	fn _pop(&mut self, amount: usize)
	{
		let nlen = self.len().saturating_sub(amount);
		self.truncate(nlen);
	}
	fn written(&self) -> usize { self.len() }
	fn callback_after(&self, marker: SinkMarker, cb: &mut FnMut(&[u8])->())
	{
		use super::SlicingExt;
		match self.slice_np(marker.0..) {
			Some(x) => cb(x),
			None => cb(&[])
		};
	}
}

#[test]
fn test_slicesink_basic()
{
	let mut buf = [0; 32];
	let len = {
		let mut ssink = SliceSink::new(&mut buf);
		ssink.write_slice("Hello,".as_bytes()).unwrap();
		ssink.write_slice(" World!".as_bytes()).unwrap();
		ssink.written()
	};
	let buf2 = &buf[..len];
	assert_eq!(buf2, "Hello, World!".as_bytes());
}

#[test]
fn write_overflow_0()
{
	let mut buf = [0; 6];
	let len = {
		let mut ssink = SliceSink::new(&mut buf);
		ssink.write_slice("XXXXXX".as_bytes()).unwrap();
		ssink.write_slice("YYY".as_bytes()).unwrap_err();
		ssink.written()
	};
	assert_eq!(len, 6);
}

#[test]
fn write_overflow_1()
{
	let mut buf = [0; 7];
	let len = {
		let mut ssink = SliceSink::new(&mut buf);
		ssink.write_slice("XXXXXX".as_bytes()).unwrap();
		ssink.write_slice("YYY".as_bytes()).unwrap_err();
		ssink.written()
	};
	assert_eq!(len, 6);
}

#[test]
fn write_asn1_empty()
{
	let tag = Asn1Tag::Universal(2, false);
	let mut buf = [0; 32];
	let len = {
		let mut ssink = SliceSink::new(&mut buf);
		ssink.asn1_fn(tag, |_x|Ok(())).unwrap();
		ssink.written()
	};
	assert_eq!(&buf[..len], b"\x02\x00");
}

#[test]
fn write_asn1_1()
{
	let tag = Asn1Tag::Universal(2, false);
	let mut buf = [0; 32];
	let len = {
		let mut ssink = SliceSink::new(&mut buf);
		ssink.asn1_fn(tag, |sub|sub.write_u8(5)).unwrap();
		ssink.written()
	};
	assert_eq!(&buf[..len], b"\x02\x01\x05");
}

#[test]
fn write_asn1_127()
{
	let tag = Asn1Tag::Universal(2, false);
	let mut buf = [0; 150];
	let len = {
		let mut ssink = SliceSink::new(&mut buf);
		ssink.asn1_fn(tag,|sub|{
			sub.write_u8(5).unwrap();
			for i in 0..126 { sub.write_u8(i).unwrap(); }
			Ok(())
		}).unwrap();
		ssink.written()
	};
	assert_eq!(len, 129);
	assert_eq!(&buf[..6], b"\x02\x7f\x05\x00\x01\x02");
}

#[test]
fn write_asn1_128()
{
	let tag = Asn1Tag::Universal(2, false);
	let mut buf = [0; 150];
	let len = {
		let mut ssink = SliceSink::new(&mut buf);
		{
			ssink.asn1_fn(tag, |sub|{
				sub.write_u8(5).unwrap();
				for i in 0..127 { sub.write_u8(i).unwrap(); }
				Ok(())
			}).unwrap();
		}
		ssink.written()
	};
	assert_eq!(len, 131);
	assert_eq!(&buf[..7], b"\x02\x81\x80\x05\x00\x01\x02");
}

#[test]
fn write_asn1_255()
{
	let tag = Asn1Tag::Universal(2, false);
	let mut buf = [0; 300];
	let len = {
		let mut ssink = SliceSink::new(&mut buf);
		ssink.asn1_fn(tag, |sub|{
			sub.write_u8(5).unwrap();
			for i in 0..254 { sub.write_u8(i).unwrap(); }
			Ok(())
		}).unwrap();
		ssink.written()
	};
	assert_eq!(len, 258);
	assert_eq!(&buf[..7], b"\x02\x81\xff\x05\x00\x01\x02");
}

#[test]
fn write_asn1_256()
{
	let tag = Asn1Tag::Universal(2, false);
	let mut buf = [0; 300];
	let len = {
		let mut ssink = SliceSink::new(&mut buf);
		ssink.asn1_fn(tag, |sub|{
			sub.write_u8(5).unwrap();
			for i in 0..255 { sub.write_u8(i).unwrap(); }
			Ok(())
		}).unwrap();
		ssink.written()
	};
	assert_eq!(len, 260);
	assert_eq!(&buf[..8], b"\x02\x82\x01\x00\x05\x00\x01\x02");
}

#[test]
fn write_asn1_259()
{
	let tag = Asn1Tag::Universal(2, false);
	let mut buf = [0; 300];
	let len = {
		let mut ssink = SliceSink::new(&mut buf);
		ssink.asn1_fn(tag, |sub|{
			sub.write_u8(5).unwrap();
			for i in 0..255 { sub.write_u8(i).unwrap(); }
			for i in 0..3 { sub.write_u8(i).unwrap(); }
			Ok(())
		}).unwrap();
		ssink.written()
	};
	assert_eq!(len, 263);
	assert_eq!(&buf[..8], b"\x02\x82\x01\x03\x05\x00\x01\x02");
}

#[test]
fn write_asn1_65535()
{
	let tag = Asn1Tag::Universal(2, false);
	let mut buf = [0; 70000];
	let len = {
		let mut ssink = SliceSink::new(&mut buf);
		ssink.asn1_fn(tag, |sub|{
			sub.write_u8(5).unwrap();
			for _ in 0..65534 { sub.write_u8(0).unwrap(); }
			Ok(())
		}).unwrap();
		ssink.written()
	};
	assert_eq!(len, 65539);
	assert_eq!(&buf[..6], b"\x02\x82\xFF\xFF\x05\x00");
}

#[test]
fn write_asn1_65536()
{
	let tag = Asn1Tag::Universal(2, false);
	let mut buf = [0; 70000];
	let len = {
		let mut ssink = SliceSink::new(&mut buf);
		ssink.asn1_fn(tag, |sub|{
			sub.write_u8(5).unwrap();
			for _ in 0..65535 { sub.write_u8(0).unwrap(); }
			Ok(())
		}).unwrap();
		ssink.written()
	};
	assert_eq!(len, 65541);
	assert_eq!(&buf[..7], b"\x02\x83\x01\x00\x00\x05\x00");
}

#[test]
fn write_asn1_65053()
{
	let tag = Asn1Tag::Universal(2, false);
	let mut buf = [0; 70000];
	let len = {
		let mut ssink = SliceSink::new(&mut buf);
		ssink.asn1_fn(tag, |sub|{
			sub.write_u8(9).unwrap();
			for _ in 0..66052 { sub.write_u8(0).unwrap(); }
			Ok(())
		}).unwrap();
		ssink.written()
	};
	assert_eq!(len, 66058);
	assert_eq!(&buf[..7], b"\x02\x83\x01\x02\x05\x09\x00");
}

#[test]
fn write_asn1_16777215()
{
	use self::std::vec::Vec;
	let tag = Asn1Tag::Universal(2, false);
	let buf = {
		let mut ssink = Vec::new();
		ssink.resize(16777215+128,0);
		let bytes = {
			let mut tmp_ssink = SliceSink::new(&mut ssink);
			tmp_ssink.asn1_fn(tag, |sub|{
				sub.write_u8(9).unwrap();
				for _ in 0..16777214 { sub.write_u8(0).unwrap(); }
				Ok(())
			}).unwrap();
			tmp_ssink.written()
		};
		ssink.resize(bytes, 0);
		ssink
	};
	assert_eq!(buf.len(), 16777220);
	assert_eq!(&buf[..7], b"\x02\x83\xFF\xFF\xFF\x09\x00");
}

#[test]
fn write_asn1_16777216()
{
	use self::std::vec::Vec;
	let tag = Asn1Tag::Universal(2, false);
	let mut ssink = Vec::new();
	ssink.resize(16777215+128,0);
	{
		let mut tmp_ssink = SliceSink::new(&mut ssink);
		tmp_ssink.asn1_fn(tag, |sub| {
			sub.write_u8(9).unwrap();
			for _ in 0..16777215 { sub.write_u8(0).unwrap(); }
			Ok(())
		}).unwrap_err();
	};
}

#[cfg(test)]
fn write_asn1_test(tag: Asn1Tag, answer: &[u8], skip: usize)
{
	use self::std::vec::Vec;
	let buf = {
		let mut ssink = Vec::new();
		ssink.resize(answer.len() + 3, 0);	//The +3 is for extra tag space not winding used.
		let bytes = {
			let mut tmp_ssink = SliceSink::new(&mut ssink);
			tmp_ssink.asn1_fn(tag, |sub|sub.write_slice(&answer[skip..])).unwrap();
			tmp_ssink.written()
		};
		ssink.resize(bytes, 0);
		ssink
	};
	assert_eq!(&buf[..], answer);
}

#[test]
fn write_asn1_basic_tags()
{
	let tag = Asn1Tag::Universal(16, true);
	write_asn1_test(tag, b"\x30\x04ABCD", 2);
	let tag = Asn1Tag::Application(30, false);
	write_asn1_test(tag, b"\x5E\x04ABCD", 2);
	let tag = Asn1Tag::Application(29, true);
	write_asn1_test(tag, b"\x7D\x04ABCD", 2);
	let tag = Asn1Tag::ContextSpecific(28, false);
	write_asn1_test(tag, b"\x9C\x04ABCD", 2);
	let tag = Asn1Tag::ContextSpecific(27, true);
	write_asn1_test(tag, b"\xBB\x04ABCD", 2);
	let tag = Asn1Tag::Private(26, false);
	write_asn1_test(tag, b"\xDA\x04ABCD", 2);
	let tag = Asn1Tag::Private(25, true);
	write_asn1_test(tag, b"\xF9\x04ABCD", 2);
}

#[test]
fn write_asn1_extended_tags()
{
	let tag = Asn1Tag::Application(31, false);
	write_asn1_test(tag, b"\x5F\x1f\x04ABCD", 3);
	let tag = Asn1Tag::Application(127, true);
	write_asn1_test(tag, b"\x7F\x7f\x04ABCD", 3);
	let tag = Asn1Tag::Application(128, true);
	write_asn1_test(tag, b"\x7F\x81\x00\x04ABCD", 4);
	let tag = Asn1Tag::Application(131, true);
	write_asn1_test(tag, b"\x7F\x81\x03\x04ABCD", 4);
	let tag = Asn1Tag::Universal(16383, true);
	write_asn1_test(tag, b"\x3F\xff\x7f\x04ABCD", 4);
	let tag = Asn1Tag::Universal(16384, true);
	write_asn1_test(tag, b"\x3F\x81\x80\x00\x04ABCD", 5);
	let tag = Asn1Tag::Universal(16645, true);
	write_asn1_test(tag, b"\x3F\x81\x82\x05\x04ABCD", 5);
	let tag = Asn1Tag::Private(2097151, true);
	write_asn1_test(tag, b"\xFF\xff\xff\x7f\x04ABCD", 5);
	let tag = Asn1Tag::ContextSpecific(2097152, false);
	write_asn1_test(tag, b"\x9f\x81\x80\x80\x00\x04ABCD", 6);
}

#[cfg(feature="stdlib")]
#[test]
fn test_vector_sink()
{
	let mut ssink = Vec::new();
	ssink.write_slice("ABCD".as_bytes()).unwrap();
	ssink._alter(3, b'E');
	assert_eq!(ssink._readback(2), b'C');
	assert_eq!(ssink._readback(4), 0);
	assert_eq!(&ssink[..], b"ABCE");
	let mark = ssink.marker_posonly();
	ssink.write_slice("ABCD".as_bytes()).unwrap();
	ssink._pop(2);
	assert_eq!(&ssink[..], b"ABCEAB");
	assert_eq!(ssink.written(), 6);
	ssink.callback_after(mark, &mut |x|{
		assert_eq!(x, b"AB");
	})
}
