use super::npslice::SlicingExt;
use ::core::fmt::{Display, Formatter, Error as FmtError};
use ::core::cmp::min;
use ::core::ops::{Range, RangeTo};
#[cfg(test)]
extern crate std;

///The largest 8-bit unsigned value.
pub const MAX8: usize = 255;
///The largest 16-bit unsigned value.
pub const MAX16: usize = 65535;
///The largest 24-bit unsigned value.
pub const MAX24: usize = 16777215;

///Trait for reading length out of source.
///
///This trait represents a way to encode length of some structure.
///
///This trait is implemented by `usize`, which represents a fixed length equal to its value, and which is implicit,
///and takes no space to encode.
///
///This trait is implemented by `Range<usize>`, which represents legnth in the specified range. The range takes
///the smallest amount possible to encode (e.g. 1 byte if upper limit is at most 255).
///
///Also, `RangeTo<usize>` implements the trait, acting like `Range<usize>`, but with implicit lower limit of 0
///bytes.
pub trait LengthTrait
{
	///Get the length from source.
	///
	///Takes in:
	///
	/// * A length range `self`.
	/// * A source `src`.
	///
	///This method returns the length of next structure in the source. The structure is assumed to be described
	///the the length range.
	///
	///On success, returns `Ok(size)`, where `size` is the size of structure in bytes.
	///
	///On failure, returns `Err(())`. At least the following factors cause the method to fail:
	///
	/// * The encoded length is out of range,
	/// * The encoded length extends past the end of the source.
	fn get_length<'a>(&self, src: &mut Source<'a>) -> Result<usize, ()>;
	///Get number of bytes to encode the length.
	///
	///Takes in:
	///
	/// * A length range `self`.
	///
	///This method returns the number of bytes it takes to encode this length range in source.
	fn get_bytes(&self) -> usize;
}

impl LengthTrait for usize
{
	fn get_length<'a>(&self, _: &mut Source<'a>) -> Result<usize, ()> { Ok(*self) }
	fn get_bytes(&self) -> usize { 0 }
}

fn range_get_length_common<'a>(start: usize, end: usize, src: &mut Source<'a>) -> Result<usize, ()>
{
	let len = match range_get_bytes_common(end) {
		1 => src.read_u8(())? as usize,
		2 => src.read_u16(())? as usize,
		3 => src.read_u24(())? as usize,
		_ => fail!(())
	};
	fail_if!(len < start || len > end, ());
	Ok(len)
}

fn range_get_bytes_common(end: usize) -> usize
{
	match end {
		0...255 => 1,
		256...65535 => 2,
		65536...16777215 => 3,
		_ => 0
	}
}


impl LengthTrait for Range<usize>
{
	fn get_length<'a>(&self, src: &mut Source<'a>) -> Result<usize, ()>
	{
		range_get_length_common(self.start, self.end, src)
	}
	fn get_bytes(&self) -> usize { range_get_bytes_common(self.end) }
}

impl LengthTrait for RangeTo<usize>
{
	fn get_length<'a>(&self, src: &mut Source<'a>) -> Result<usize, ()>
	{
		range_get_length_common(0, self.end, src)
	}
	fn get_bytes(&self) -> usize { range_get_bytes_common(self.end) }
}

///Source that provodes data out of byte slice.
///
///This structure reads byte slice as various data types, in order.
///
///This is especially useful for pasing TLS syntax or ASN.1 DER.
///
///Note that methods enforce ASN.1 DER strictly. Also, the maximum allowed length of any ASN.1 element is restricted
///to 16,777,215 bytes for security reasons (it is hazardous to parse elements any bigger than this).
#[derive(Copy,Clone,Debug)]
pub struct Source<'a>(&'a [u8],usize);

///The tag value for ASN.1 BOOLEAN type
pub const ASN1_BOOLEAN: Asn1Tag = Asn1Tag::Universal(1, false);
///The tag value for ASN.1 INTEGER type
pub const ASN1_INTEGER: Asn1Tag = Asn1Tag::Universal(2, false);
///The tag value for ASN.1 BIT STRING type
pub const ASN1_BIT_STRING: Asn1Tag = Asn1Tag::Universal(3, false);
///The tag value for ASN.1 OCTET STRING type
pub const ASN1_OCTET_STRING: Asn1Tag = Asn1Tag::Universal(4, false);
///The tag value for ASN.1 NULL type
pub const ASN1_NULL: Asn1Tag = Asn1Tag::Universal(5, false);
///The tag value for ASN.1 OBJECT IDENTIFIER type
pub const ASN1_OID: Asn1Tag = Asn1Tag::Universal(6, false);
///The tag value for ASN.1 OBJECT DESCRIPTOR type
pub const ASN1_OBJECT_DESCRIPTOR: Asn1Tag = Asn1Tag::Universal(7, false);
///The tag value for ASN.1 INSTANCE_OF type
pub const ASN1_INSTANCE_OF: Asn1Tag = Asn1Tag::Universal(8, true);
///The tag value for ASN.1 REAL type
pub const ASN1_REAL: Asn1Tag = Asn1Tag::Universal(9, false);
///The tag value for ASN.1 ENUMERATED type
pub const ASN1_ENUMERATED: Asn1Tag = Asn1Tag::Universal(10, false);
///The tag value for ASN.1 EMBEDDED PDV type
pub const ASN1_EMBEDDED_PDV: Asn1Tag = Asn1Tag::Universal(11, true);
///The tag value for ASN.1 UTF-8 STRING type
pub const ASN1_UTF8_STRING: Asn1Tag = Asn1Tag::Universal(12, false);
///The tag value for ASN.1 UTF-8 STRING type
pub const ASN1_RELATIVE_OID: Asn1Tag = Asn1Tag::Universal(13, false);
//14 and 15 are unknown.
///The tag value for ASN.1 SEQUENCE type
pub const ASN1_SEQUENCE: Asn1Tag = Asn1Tag::Universal(16, true);
///The tag value for ASN.1 SET type
pub const ASN1_SET: Asn1Tag = Asn1Tag::Universal(17, true);
///The tag value for ASN.1 NUMERIC STRING type
pub const ASN1_NUMERIC_STRING: Asn1Tag = Asn1Tag::Universal(18, true);
///The tag value for ASN.1 PRINTABLE STRING type
pub const ASN1_PRINTABLE_STRING: Asn1Tag = Asn1Tag::Universal(19, false);
///The tag value for ASN.1 TELEX STRING type
pub const ASN1_TELEX_STRING: Asn1Tag = Asn1Tag::Universal(20, false);
///The tag value for VIDEO STRING type
pub const ASN1_VIDEO_STRING: Asn1Tag = Asn1Tag::Universal(21, false);
///The tag value for ASN.1 IA5 STRING type
pub const ASN1_IA5_STRING: Asn1Tag = Asn1Tag::Universal(22, false);
///The tag value for ASN.1 UTC TIME type
pub const ASN1_UTC_TIME: Asn1Tag = Asn1Tag::Universal(23, false);
///The tag value for ASN.1 GENERALIZED TIME type
pub const ASN1_GENERALIZED_TIME: Asn1Tag = Asn1Tag::Universal(24, false);
///The tag value for ASN.1 GRAPHICS STRING type
pub const ASN1_GRAPHICS_STRING: Asn1Tag = Asn1Tag::Universal(25, false);
///The tag value for ASN.1 ISO646 STRING type
pub const ASN1_ISO646_STRING: Asn1Tag = Asn1Tag::Universal(26, false);
///The tag value for ASN.1 GENERAL STRING type
pub const ASN1_GENERAL_STRING: Asn1Tag = Asn1Tag::Universal(27, false);
///The tag value for ASN.1 UNIVERSAL STRING type
pub const ASN1_UNIVERSAL_STRING: Asn1Tag = Asn1Tag::Universal(28, false);
///The tag value for ASN.1 CHARACTER STRING type
pub const ASN1_CHARACTER_STRING: Asn1Tag = Asn1Tag::Universal(29, false);
///The tag value for ASN.1 BMP STRING type
pub const ASN1_BMP_STRING: Asn1Tag = Asn1Tag::Universal(30, false);
///The tag value for ASN.1 [0] constructed
pub const ASN1_CTX0_C: Asn1Tag = Asn1Tag::ContextSpecific(0, true);
///The tag value for ASN.1 [1] constructed
pub const ASN1_CTX1_C: Asn1Tag = Asn1Tag::ContextSpecific(1, true);
///The tag value for ASN.1 [2] constructed
pub const ASN1_CTX2_C: Asn1Tag = Asn1Tag::ContextSpecific(2, true);
///The tag value for ASN.1 [3] constructed
pub const ASN1_CTX3_C: Asn1Tag = Asn1Tag::ContextSpecific(3, true);
///The tag value for ASN.1 [0] primitive
pub const ASN1_CTX0_P: Asn1Tag = Asn1Tag::ContextSpecific(0, false);
///The tag value for ASN.1 [1] primitive
pub const ASN1_CTX1_P: Asn1Tag = Asn1Tag::ContextSpecific(1, false);
///The tag value for ASN.1 [2] primitive
pub const ASN1_CTX2_P: Asn1Tag = Asn1Tag::ContextSpecific(2, false);
///The tag value for ASN.1 [3] primitive
pub const ASN1_CTX3_P: Asn1Tag = Asn1Tag::ContextSpecific(3, false);

///Parsed ASN.1 DER TLV
///
///This structure represents a parsed ASN.1 TLV.
#[derive(Copy,Clone,Debug)]
pub struct Asn1Tlv<'a>
{
	///The tag of the TLV.
	pub tag: Asn1Tag,
	///`Source` constructed over the value of the TLV.
	pub value: Source<'a>,
	///Raw value of the TLV
	pub raw_p: &'a [u8],
	///Raw representation of the TLV, including the tag and length.
	pub outer: &'a [u8],
}

impl<'a> Asn1Tlv<'a>
{
	///Call closure on value.
	///
	///Takes in:
	///
	/// * A TLV `self`.
	/// * A closure `closure`. The closure takes a `&mut Source` as an argument.
	///
	///This method calls the closure on the value field of the TLV, and returns whatever the closure returns.
	pub fn call<T:Sized, F>(&self, mut closure: F) -> T where F: FnMut(&mut Source<'a>) -> T
	{
		closure(&mut self.value.clone())
	}
	///Fold a closure until end of source.
	///
	///Takes in:
	///
	/// * A TLV `self`.
	/// * An initial state `initial`.
	/// * A closure `closure`. The closure takes a `&mut Source` and a state as arguments.
	///
	///This method repeatedly calls the closure until end of the TLV value is reached. The return value of the
	///previous closure call is used as the state of next, starting from the initial state.
	///
	///If the closure fails, this method fails instantly.
	///
	///On success, returns `Ok(last_value)`, where `last_value` is the last value the closure returned.
	///
	///On failure, returns `Err(error)`, where `error` is the failure returned by the closure.
	pub fn fold<T:Sized, E:Sized, F>(&self, initial: T, mut closure: F) -> Result<T, E>
		where F: FnMut(&mut Source<'a>, T) -> Result<T, E>
	{
		let mut itr = self.value.clone();
		let mut state: T = initial;
		while !itr.at_end() { state = closure(&mut itr, state)?; }
		Ok(state)
	}
}

///Error from reading ASN.1 DER TLV.
///
///This enumeration describes an error from ASN.1 TLV parsing.
#[derive(Copy,Clone,Eq,PartialEq,Debug)]
pub enum Asn1Error
{
	#[doc(hidden)]
	Truncated,
	#[doc(hidden)]
	NotDer,
	#[doc(hidden)]
	LengthTooBig,
	#[doc(hidden)]
	TagTooBig,
	#[doc(hidden)]
	UnexpectedType(Asn1Tag, Asn1Tag),
	#[doc(hidden)]
	ReadZero,
	#[doc(hidden)]
	Hidden__
}

impl Display for Asn1Error
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::Asn1Error::*;
		match self {
			&Truncated => fmt.write_str("Truncated"),
			&NotDer => fmt.write_str("Not valid DER"),
			&LengthTooBig => fmt.write_str("Value too big"),
			&TagTooBig => fmt.write_str("Tag too big"),
			&UnexpectedType(x,y) => fmt.write_fmt(format_args!("Unexpected type (got {}, epxpected {})",
				x, y)),
			&ReadZero => fmt.write_str("Internal error: Array element empty"),
			&Hidden__ => fmt.write_str("Hidden__"),
		}
	}
}

///ASN.1 tag
///
///This enumeration describes an ASN.1 Tag.
#[derive(Copy,Clone,Eq,PartialEq,Debug)]
pub enum Asn1Tag
{
	///Universal tag.
	///
	/// * The 1st parameter is the tag number.
	/// * The 2nd parameter is the constructed flag.
	///
	///# Notes:
	///
	/// * This class contains various well-known tags, including:
	///   * 1: Boolean
	///   * 2: Integer
	///   * 3: Bit String
	///   * 4: Octet String
	///   * 5: NULL
	///   * 6: OID
	///   * 7: Object Descriptor
	///   * 8: Instance Of
	///   * 9: Real
	///   * 10: Enumerated
	///   * 11: Embedded PDV
	///   * 12: UTF-8 String
	///   * 13: Relative OID
	///   * 16: Sequence
	///   * 17: Set
	///   * 18: Numeric String
	///   * 19: Printable String
	///   * 20: Telex String
	///   * 21: Video String
	///   * 22: IA5 String
	///   * 23: UTC Time
	///   * 24: Generalized Time
	///   * 25: Graphics String
	///   * 26: ISO646 String
	///   * 27: General String
	///   * 28: Universal String
	///   * 29: Character String
	///   * 30: BMP String
	Universal(u64, bool),
	///Application-specific tag
	///
	/// * The 1st parameter is the tag number.
	/// * The 2nd parameter is the constructed flag.
	Application(u64, bool),
	///Context-specific tag
	///
	/// * The 1st parameter is the tag number.
	/// * The 2nd parameter is the constructed flag.
	ContextSpecific(u64, bool),
	///Private-use tag
	///
	/// * The 1st parameter is the tag number.
	/// * The 2nd parameter is the constructed flag.
	Private(u64, bool)
}

///Trait used to cast Asn1Tag matchers to actual values.
///
///This trait represents a match on ASN.1 tag.
pub trait Asn1TagMatch
{
	///Check for match.
	///
	///Takes in:
	///
	/// * A matcher `self`.
	/// * A matched tag `val`.
	///
	///This method returns true if the matcher matches the tag, false otherwise.
	fn tmatch(&self, val: Asn1Tag) -> bool;
	///The primary tag for this matcher.
	///
	///Takes in:
	///
	/// * A matcher `self`.
	///
	///This method returns the primary tag the matcher uses. This is only used for error messages.
	fn expected(&self) -> Asn1Tag;
}

impl Asn1TagMatch for Asn1Tag
{
	fn tmatch(&self, val: Asn1Tag) -> bool { *self == val }
	fn expected(&self) -> Asn1Tag { *self }
}

///Match any ASN.1 Tag.
///
///This structure is used in place of the ASN.1 tag in ASN.1 read routines to indicate that any tag is acceptable.
#[derive(Copy,Clone,Debug)]
pub struct AnyAsn1Tag;


impl Asn1TagMatch for AnyAsn1Tag
{
	fn tmatch(&self, _: Asn1Tag) -> bool { true }
	fn expected(&self) -> Asn1Tag { Asn1Tag::Universal(0, false) }		//Never called.
}

impl Display for Asn1Tag
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		match *self {
			ASN1_BOOLEAN => fmt.write_str("BOOLEAN"),
			ASN1_INTEGER => fmt.write_str("INTEGER"),
			ASN1_BIT_STRING => fmt.write_str("BIT STRING"),
			ASN1_OCTET_STRING => fmt.write_str("OCTET STRING"),
			ASN1_NULL => fmt.write_str("NULL"),
			ASN1_OID => fmt.write_str("OID"),
			ASN1_OBJECT_DESCRIPTOR => fmt.write_str("OBJECT DESCRIPTOR"),
			ASN1_INSTANCE_OF => fmt.write_str("INSTANCE OF"),
			ASN1_REAL => fmt.write_str("REAL"),
			ASN1_ENUMERATED => fmt.write_str("ENUMERATED"),
			ASN1_EMBEDDED_PDV => fmt.write_str("EMBEDDED PDV"),
			ASN1_UTF8_STRING => fmt.write_str("UTF8 STRING"),
			ASN1_RELATIVE_OID => fmt.write_str("RELATIVE OID"),
			ASN1_SEQUENCE => fmt.write_str("SEQUENCE"),
			ASN1_SET => fmt.write_str("SET"),
			ASN1_NUMERIC_STRING => fmt.write_str("NUMERIC STRING"),
			ASN1_PRINTABLE_STRING => fmt.write_str("PRINTABLE STRING"),
			ASN1_TELEX_STRING => fmt.write_str("TELEX STRING"),
			ASN1_VIDEO_STRING => fmt.write_str("VIDEO STRING"),
			ASN1_IA5_STRING => fmt.write_str("IA5 STRING"),
			ASN1_UTC_TIME => fmt.write_str("UTC TIME"),
			ASN1_GENERALIZED_TIME => fmt.write_str("GENERALIZED TIME"),
			ASN1_GRAPHICS_STRING => fmt.write_str("GRAPHICS STRING"),
			ASN1_ISO646_STRING => fmt.write_str("ISO646 STRING"),
			ASN1_GENERAL_STRING => fmt.write_str("GENERAL STRING"),
			ASN1_UNIVERSAL_STRING => fmt.write_str("UNIVERSAL STRING"),
			ASN1_CHARACTER_STRING => fmt.write_str("CHARACTER STRING"),
			ASN1_BMP_STRING => fmt.write_str("BMP STRING"),
			Asn1Tag::Universal(8, false) => fmt.write_str("[Invalid tag: INSTANCE OF but primitive???]"),
			Asn1Tag::Universal(11, false) => fmt.write_str("[Invalid tag: EMBEDDED PDV but \
				primitive???]"),
			Asn1Tag::Universal(16, false) => fmt.write_str("[Invalid tag: SEQUENCE but primitive???]"),
			Asn1Tag::Universal(17, false) => fmt.write_str("[Invalid tag: SET but primitive???]"),
			Asn1Tag::Universal(x, false) => fmt.write_fmt(format_args!("[Universal {} primitive]", x)),
			Asn1Tag::Universal(x, true) => fmt.write_fmt(format_args!("[Universal {} constructed]", x)),
			Asn1Tag::Application(x, false) => fmt.write_fmt(format_args!("[Application {} primitive]",
				x)),
			Asn1Tag::Application(x, true) => fmt.write_fmt(format_args!("[Application {} constructed]",
				x)),
			Asn1Tag::ContextSpecific(x, false) => fmt.write_fmt(format_args!("[{} primitive]", x)),
			Asn1Tag::ContextSpecific(x, true) => fmt.write_fmt(format_args!("[{} constructed]", x)),
			Asn1Tag::Private(x, false) => fmt.write_fmt(format_args!("[Private {} primitive]", x)),
			Asn1Tag::Private(x, true) => fmt.write_fmt(format_args!("[Private {} constructed]", x)),
		}
	}
}

impl<'a> Source<'a>
{
	///Create a new source.
	///
	///Takes in:
	///
	/// * A slice `slice`.
	///
	///This method creates a new source, that reads out of the slice.
	pub fn new<'b>(slice: &'b [u8]) -> Source<'b>
	{
		Source(slice,0)
	}
	///Check for end of data.
	///
	///Takes in:
	///
	/// * A source `self`.
	///
	///This method returns `true` if all the data has been read from the source, `false` otherwise.
	pub fn at_end(&self) -> bool { self.1 >= self.0.len() }
	///Return the entiere source.
	///
	///Takes in:
	///
	/// * A source `self`.
	///
	///This method returns the entiere contents of the source, including the already read parts.
	pub fn as_slice(&self) -> &'a [u8] { self.0 }
	///Read a fixed slice out of source.
	///
	///Takes in:
	///
	/// * A source `self`.
	/// * An output buffer `buf`.
	/// * A read falure value `err`.
	///
	///This method fills the output buffer with data read from the source 
	///
	///On success, returns `Ok(())`.
	///
	///If there is insufficient data in source to satisfy the request, returns `Err(err)`, where `err` is given
	///as the read failure value.
	pub fn read_array<E:Sized+Clone>(&mut self, buf: &mut [u8], err: E) -> Result<(), E>
	{
		let base = self.1;
		let eptr = base.checked_add(buf.len()).ok_or(err.clone())?;
		buf.copy_from_slice(self.0.slice_np(base..eptr).ok_or(err)?);
		self.1 = eptr;
		Ok(())
	}
	///Read a slice with a length marker out of source.
	///
	///Takes in:
	///
	/// * A source `self`.
	/// * A length range `length`.
	/// * A read falure value `err`.
	///
	///This method reads a TLS vector with the specified length range (if length range is usize, reads a fixed-
	///length slice) out of the source, as a a slice.
	///
	///This method is useful for reading opaque TLS vectors.
	///
	///On success, returns `Ok(slice)`, where `slice` is the slice read.
	///
	///If there is insufficient data in source to satisfy the request, or if the length of vector is out of
	///range, returns `Err(err)`, where `err` is the read failure value.
	pub fn read_slice<L:LengthTrait,E:Sized+Clone>(&mut self, len: L, err: E) -> Result<&'a [u8], E>
	{
		//Save position across call, but use new position in calculations. This is in case some call
		//fails: We want to keep position unchanged.
		let saved = self.1;
		let len = len.get_length(self).map_err(|_|err.clone())?;
		let base = self.1;
		self.1 = saved;
		let eptr = base.checked_add(len).ok_or(err.clone())?;
		let res = self.0.slice_np(base..eptr).ok_or(err)?;
		self.1 = eptr;
		Ok(res)
	}
	///Read everything remaining as slice.
	///
	///Takes in:
	///
	/// * A source `self`.
	///
	///This method reads everything remaining from the source, and returns the data as slice.
	///
	///After this method is used, `.at_end()` will return `true`.
	pub fn read_remaining(&mut self) -> &'a [u8]
	{
		static BLANK: [u8;0] = [];
		let res = self.0.slice_np(self.1..).unwrap_or(&BLANK[..]);
		self.1 = self.0.len();
		res
	}
	///Read a subsource with a length marker out of source.
	///
	///Takes in:
	///
	/// * A source `self`.
	/// * A length range `length`.
	/// * A read falure value `err`.
	///
	///This is the same as `.read_slice()`, but instead of returning a slice, returns a source.
	pub fn read_source<L:LengthTrait,E:Sized+Clone>(&mut self, len: L, err: E) -> Result<Source<'a>, E>
	{
		Ok(Source::new(self.read_slice(len, err)?))
	}
	///Read a source with in-band length and use it to call closure.
	///
	///Takes in:
	///
	/// * A source `self`.
	/// * A length range `length`.
	/// * A repeat flag `repeat`.
	/// * A closure `closure`.
	/// * A read falure value `readfail`.
	/// * A blank value `blankret`
	///
	///This method reads a slice with the specified length from the source much like `.read_slice()`. Then it
	///calls the closure. If the repeat flag is true, the closure is called repeatedly until end of the source is
	//reached.
	///
	///On success, returns `Ok(last_ret)`, where `last_ret` is the return value of the last call to the closure.
	///
	///If `repeat` is true and the slice read is empty, returns `Ok(blankret)`, where `blankret` is the blank
	///value.
	///
	///If reading fails, returns `Err(readfail)`, where `readfail` is the read failure value.
	///
	///If call to closure fails, returns `Err(err)`, where `err` is the failure returned by the closure.
	pub fn read_source_fn<F, T: Sized, E: Sized+Clone, L:LengthTrait>(&mut self, length: L, repeat: bool,
		mut closure: F, readfail: E, blankret: T) -> Result<T, E> where F: FnMut(&mut Source<'a>) ->
		Result<T,E>
	{
		let mut tmp = self.read_source(length, readfail.clone())?;
		if repeat {
			let mut retval = Ok(blankret);
			while !tmp.at_end() { retval = Ok(closure(&mut tmp)?); }
			retval
		} else {
			closure(&mut tmp)
		}
	}
	///Read an 8 bit value out of source.
	///
	///Takes in:
	///
	/// * A source `self`.
	/// * The value to use in case of error `err`.
	///
	///This method reads a 8-bit value out of the source.
	///
	///On success, returns `Ok(value)`, where `value` is the value read.
	///
	///If there is insufficient data, returns `Err(err)`, where `err` is the value to use on error.
	pub fn read_u8<E:Sized+Clone>(&mut self, err: E) -> Result<u8, E>
	{
		let l = self.read_slice(1, err)?;
		let mut x = 0;
		for i in l.iter() { x = *i as u8; }
		Ok(x)
	}
	///Read a big-endian 16 bit value out of source.
	///
	///Takes in:
	///
	/// * A source `self`.
	/// * The value to use in case of error `err`.
	///
	///This method reads a big-endian 16-bit value out of the source.
	///
	///On success, returns `Ok(value)`, where `value` is the value read.
	///
	///If there is insufficient data, returns `Err(err)`, where `err` is the value to use on error.
	pub fn read_u16<E:Sized+Clone>(&mut self, err: E) -> Result<u16, E>
	{
		let l = self.read_slice(2, err)?;
		let mut x = 0;
		for i in l.iter() { x = (x << 8) | (*i as u16); }
		Ok(x)
	}
	///Read a big-endian 24 bit value out of source.
	///
	///Takes in:
	///
	/// * A source `self`.
	/// * The value to use in case of error `err`.
	///
	///This method reads a big-endian 24-bit value out of the source.
	///
	///On success, returns `Ok(value)`, where `value` is the value read.
	///
	///If there is insufficient data, returns `Err(err)`, where `err` is the value to use on error.
	pub fn read_u24<E:Sized+Clone>(&mut self, err: E) -> Result<u32, E>
	{
		let l = self.read_slice(3, err)?;
		let mut x = 0;
		for i in l.iter() { x = (x << 8) | (*i as u32); }
		Ok(x)
	}
	///Read a big-endian 32 bit value out of source.
	///
	///Takes in:
	///
	/// * A source `self`.
	/// * The value to use in case of error `err`.
	///
	///This method reads a big-endian 32-bit value out of the source.
	///
	///On success, returns `Ok(value)`, where `value` is the value read.
	///
	///If there is insufficient data, returns `Err(err)`, where `err` is the value to use on error.
	pub fn read_u32<E:Sized+Clone>(&mut self, err: E) -> Result<u32, E>
	{
		let l = self.read_slice(4, err)?;
		let mut x = 0;
		for i in l.iter() { x = (x << 8) | (*i as u32); }
		Ok(x)
	}
	///Read a big-endian 64 bit value out of source.
	///
	///Takes in:
	///
	/// * A source `self`.
	/// * The value to use in case of error `err`.
	///
	///This method reads a big-endian 64-bit value out of the source.
	///
	///On success, returns `Ok(value)`, where `value` is the value read.
	///
	///If there is insufficient data, returns `Err(err)`, where `err` is the value to use on error.
	pub fn read_u64<E:Sized+Clone>(&mut self, err: E) -> Result<u64, E>
	{
		let l = self.read_slice(8, err)?;
		let mut x = 0;
		for i in l.iter() { x = (x << 8) | (*i as u64); }
		Ok(x)
	}
	fn read_asn1_tag(&mut self) -> Result<Asn1Tag, Asn1Error>
	{
		let fb = *self.0.get(self.1).ok_or(Asn1Error::Truncated)? as usize;
		let class = (fb >> 6) & 3;
		let constructed = (fb & 32) != 0;
		let num = (fb & 31) as u64;
		let value = if num < 31 {
			self.1 = self.1.checked_add(1).ok_or(Asn1Error::Truncated)?;
			num
		} else {
			//num=31 means high-tag form.
			let mut idx = 1;
			let mut value = 0u64;
			loop {
				if idx > 9 { return Err(Asn1Error::TagTooBig);}
				let fchidx = self.1.checked_add(idx).ok_or(Asn1Error::Truncated)?;
				let lbits = *self.0.get(fchidx).ok_or(Asn1Error::Truncated)?;
				value = (value << 7) | (lbits & 127) as u64;
				if idx == 1 && lbits == 128 { return Err(Asn1Error::NotDer); }
				idx = idx.checked_add(1).ok_or(Asn1Error::Truncated)?;
				if lbits < 128 { break; }
			}
			if value < 31 { return Err(Asn1Error::NotDer); }
			self.1 = self.1.checked_add(idx).ok_or(Asn1Error::Truncated)?;
			value
		};
		Ok(match class {
			0 => Asn1Tag::Universal(value, constructed),
			1 => Asn1Tag::Application(value, constructed),
			2 => Asn1Tag::ContextSpecific(value, constructed),
			3 => Asn1Tag::Private(value, constructed),
			_ => Asn1Tag::Private(value, constructed),	//Can't actually happen.
		})
	}
	fn read_asn1_len(&mut self) -> Result<usize, Asn1Error>
	{
		let fb = *self.0.get(self.1).ok_or(Asn1Error::Truncated)? as usize;
		if fb < 128 {
			//If checked_add fails, no way in hell input is long enough.
			self.1 = self.1.checked_add(1).ok_or(Asn1Error::Truncated)?;
			Ok(fb)
		} else if fb == 128 {
			Err(Asn1Error::NotDer)
		} else {
			let ptr = self.1.checked_add(1).ok_or(Asn1Error::Truncated)?;
			let eptr = ptr.checked_add(fb & 127).ok_or(Asn1Error::Truncated)?;
			let l = self.0.slice_np(ptr..eptr).ok_or(Asn1Error::Truncated)?;
			let first = l.get(0).map(|x|*x).unwrap_or(0);
			let mut x = 0usize;
			match l.len() {
				0 => return Err(Asn1Error::NotDer),
				1 if first >= 128 => (),
				1 => return Err(Asn1Error::NotDer),
				2 if first > 0 => (),
				2 => return Err(Asn1Error::NotDer),
				3 if first > 0 => (),
				3 => return Err(Asn1Error::NotDer),
				_ => return Err(Asn1Error::LengthTooBig),
			};
			for i in l.iter() { x = (x << 8) | (*i as usize) };
			self.1 = eptr;
			Ok(x)
		}
	}
	///Read an ASN.1 DER TLV out of source.
	///
	///Takes in:
	///
	/// * A source `self`.
	/// * A ASN.1 tag matcher `xtag`.
	/// * An error-mapping function `maperr`. Takes a `Asn1Error` as an arguemnt.
	///
	///This method reads an ASN.1 DER TLV with the matched tag type out of the source. The lifetimes associated
	///with the returned value are the same as lifetimes of the source passed to this method.
	///
	///On success, returns `Ok(tlv)`, where `tlv` is the TLV read.
	///
	///On failure returns `Err(mappederr)`, where `mappederr` is the return value of the error mapping function
	///on the low-level read error.  At least the following factors cause the method to fail:
	///
	/// * The ASN.1 TLV is truncated by end of the source.
	/// * The ASN.1 is not DER (not canonical).
	pub fn read_asn1_value<Tag:Asn1TagMatch, E, EM>(&mut self, xtag: Tag, maperr: EM) ->
		Result<Asn1Tlv<'a>, E> where EM: Fn(Asn1Error) -> E
	{
		self._read_asn1_value(xtag).map_err(maperr)
	}
	fn _read_asn1_value<Tag:Asn1TagMatch>(&mut self, xtag: Tag) -> Result<Asn1Tlv<'a>, Asn1Error>
	{
		let y = self.1;
		let tag = self.read_asn1_tag()?;
		let len = self.read_asn1_len()?;
		let src = self.read_source(len, Asn1Error::Truncated)?;
		let raw = src.as_slice();
		let outer = self.0.slice_np(y..self.1).ok_or_else(||Asn1Error::Truncated)?;
		let x = Asn1Tlv{tag:tag, value:src, raw_p:raw, outer: outer};
		if xtag.tmatch(x.tag) { return Ok(x); }
		self.1 = y;
		Err(Asn1Error::UnexpectedType(x.tag, xtag.expected()))
	}
	///Read an optional ASN.1 DER TLV out of source.
	///
	///Takes in:
	///
	/// * A source `self`.
	/// * A ASN.1 tag matcher `tag`.
	/// * A value for wrong type `wrong_type`.
	/// * A closure `cb`. Takes a `&mut Source` spanning the TLV value as an argument. The lifetime of the
	///source passed is the same as the lifetime the source this method is called on.
	/// * An error-mapping function `errmap`. Takes a `Asn1Error` as an arguemnt.
	///
	///This method reads an optional ASN.1 DER TLV with tag matching the matched tag type out of the source.
	///It then and calls the closure with the result.
	///
	///On success, returns whatever `cb` returns.
	///
	///If the source is at end, or does not contain tag of specified type as the next tag, returns
	///`Ok(wrong_type)`, where `wrong_type` is the wrong type value.
	///
	///If reading the ASN.1 TLV fails, returns `Err(mappederr)`, where `mappederr` is the
	///return value of the error-mapping function for the low-level read error.
	pub fn read_asn1_value_opt_fn<T, E, Fc, Fe>(&mut self, tag: Asn1Tag, wrong_type: T, mut cb: Fc,
		errmap: Fe) -> Result<T, E> where Fc: FnMut(Source<'a>) -> Result<T, E>, Fe: Fn(Asn1Error) -> E
	{
		let y = self.1;
		if self.1 >= self.0.len() { return Ok(wrong_type) };	//At end.
		let x = self.read_asn1_value(AnyAsn1Tag, errmap)?;
		if x.tag == tag { return cb(x.value); }
		self.1 = y;
		Ok(wrong_type)
	}
	///Return a starting marker for raw subslice operation.
	///
	///Takes in:
	///
	/// * A source `self`.
	///
	///This method returns a marker for current read position in the source, used as starting marker for raw
	///subslice operation.
	pub fn slice_marker(&self) -> SliceMarker
	{
		SliceMarker(self.1)
	}
	///Return the current position in source.
	///
	///Takes in:
	///
	/// * A source `self`.
	///
	///This method returns the current read position in the source.
	///
	///Note that only this source is considered, the starting position in supersource is ignored.
	pub fn position(&self) -> usize { self.1 }
}

///Is the slice a single ASN.1 structure or not?
///
///Takes in:
///
/// * A slice `slice`.
///
///This function returns `true` if the slice is spanned by one ASN.1 TLV, `false` otherwise.
///
///Note that only the top-level ASN.1 TLV is considered.
pub fn is_single_asn1_structure(slice: &[u8]) -> bool
{
	let mut src = Source::new(slice);
	let val = match src.read_asn1_value(AnyAsn1Tag, |_|()) { Ok(x) => x, Err(_) => return false};
	val.outer.len() == slice.len()
}

///A saved position.
///
///This structure represents as saved position in slice marker.
pub struct SliceMarker(usize);

impl SliceMarker
{
	///Return remainder of source as slice.
	///
	///This method returns the part of source `src` after marker `self` as a slice.
	pub fn commit<'a>(self, src: &Source<'a>) -> &'a [u8]
	{
		static BLANK: [u8;0] = [];
		let max = min(src.1, src.0.len());
		let min = min(self.0, max);
		src.0.slice_np(min..max).unwrap_or(&BLANK[..])
	}
}


#[test]
fn source_as_slice()
{
	let x = [6,7,3];
	let y = Source::new(&x);
	let z = y.as_slice();
	assert_eq!(&x, z);
}

#[test]
fn source_at_end()
{
	let x = [6,7,3];
	let mut y = Source::new(&x);
	assert!(!y.at_end());
	y.read_u8(()).unwrap();
	assert!(!y.at_end());
	y.read_u8(()).unwrap();
	assert!(!y.at_end());
	y.read_u8(()).unwrap();
	assert!(y.at_end());
}

#[test]
fn source_read_slice()
{
	let x = [6, 7, 3, 6, 8, 3, 2, 6, 8, 4, 3, 2, 1, 7, 3, 0];
	let mut y = Source::new(&x);
	assert!(!y.at_end());
	let z1 = y.read_slice(4, ()).unwrap();
	assert!(!y.at_end());
	let z2 = y.read_slice(8, ()).unwrap();
	assert!(!y.at_end());
	let z3 = y.read_slice(4, ()).unwrap();
	assert!(y.at_end());
	y.read_slice(1, ()).unwrap_err();
	assert_eq!(z1, &x[0..4]);
	assert_eq!(z2, &x[4..12]);
	assert_eq!(z3, &x[12..16]);
}

#[test]
fn source_read_slice_len()
{
	let x1 = [3, 1, 2, 3, 4];
	let x2 = [0, 3, 2, 3, 4, 5];
	let x3 = [0, 0, 3, 3, 4, 5, 6];
	let mut y1 = Source::new(&x1);
	let mut y2 = Source::new(&x2);
	let mut y3 = Source::new(&x3);
	let z1 = y1.read_slice(..MAX8, ()).unwrap();
	let z2 = y2.read_slice(..MAX16, ()).unwrap();
	let z3 = y3.read_slice(..MAX24, ()).unwrap();
	assert_eq!(&z1, &[1,2,3]);
	assert_eq!(&z2, &[2,3,4]);
	assert_eq!(&z3, &[3,4,5]);
	assert_eq!(y1.read_u8(()).unwrap(), 4);
	assert_eq!(y2.read_u8(()).unwrap(), 5);
	assert_eq!(y3.read_u8(()).unwrap(), 6);
}

#[test]
fn source_read_slice_len_long()
{
	let mut x = [0;70000];
	x[0] = 1;
	x[1] = 2;
	x[2] = 3;
	let mut y1 = Source::new(&x);
	let mut y2 = Source::new(&x);
	let mut y3 = Source::new(&x);
	assert_eq!(y1.read_slice(..MAX8, ()).unwrap().len(), 0x01);
	assert_eq!(y2.read_slice(..MAX16, ()).unwrap().len(), 0x0102);
	assert_eq!(y3.read_slice(..MAX24, ()).unwrap().len(), 0x010203);
	assert_eq!(y1.position(), 0x02);
	assert_eq!(y2.position(), 0x0104);
	assert_eq!(y3.position(), 0x010206);
}

#[test]
fn source_read_remaining()
{
	let x = [6, 7, 3, 6, 8, 3, 2, 6, 8, 4, 3, 2, 1, 7, 3, 0];
	let mut y = Source::new(&x);
	assert!(!y.at_end());
	y.read_slice(5, ()).unwrap();
	assert!(!y.at_end());
	let z1 = y.read_remaining();
	assert!(y.at_end());
	let z2 = y.read_remaining();
	assert_eq!(z1, &x[5..]);
	assert_eq!(z2, &[]);
}

#[test]
fn source_read_source()
{
	let x = [6, 7, 3, 6, 8, 3, 2, 6, 8, 4, 3, 2, 1, 7, 3, 0];
	let mut y = Source::new(&x);
	assert!(!y.at_end());
	let mut z1 = y.read_source(4, ()).unwrap();
	assert!(!y.at_end());
	let mut z2 = y.read_source(8, ()).unwrap();
	assert!(!y.at_end());
	let mut z3 = y.read_source(4, ()).unwrap();
	assert!(y.at_end());
	y.read_source(1, ()).unwrap_err();
	assert_eq!(z1.read_remaining(), &x[0..4]);
	assert_eq!(z2.read_remaining(), &x[4..12]);
	assert_eq!(z3.read_remaining(), &x[12..16]);
}

#[test]
fn source_read_source_len()
{
	let x1 = [3, 1, 2, 3, 4];
	let x2 = [0, 3, 2, 3, 4, 5];
	let x3 = [0, 0, 3, 3, 4, 5, 6];
	let mut y1 = Source::new(&x1);
	let mut y2 = Source::new(&x2);
	let mut y3 = Source::new(&x3);
	let mut z1 = y1.read_source(..MAX8, ()).unwrap();
	let mut z2 = y2.read_source(..MAX16, ()).unwrap();
	let mut z3 = y3.read_source(..MAX24, ()).unwrap();
	assert_eq!(z1.read_remaining(), &[1,2,3]);
	assert_eq!(z2.read_remaining(), &[2,3,4]);
	assert_eq!(z3.read_remaining(), &[3,4,5]);
	assert_eq!(y1.read_u8(()).unwrap(), 4);
	assert_eq!(y2.read_u8(()).unwrap(), 5);
	assert_eq!(y3.read_u8(()).unwrap(), 6);
}

#[test]
fn source_read_source_len_long()
{
	let mut x = [0;70000];
	x[0] = 1;
	x[1] = 2;
	x[2] = 3;
	let mut y1 = Source::new(&x);
	let mut y2 = Source::new(&x);
	let mut y3 = Source::new(&x);
	assert_eq!(y1.read_source(..MAX8, ()).unwrap().as_slice().len(), 0x01);
	assert_eq!(y2.read_source(..MAX16, ()).unwrap().as_slice().len(), 0x0102);
	assert_eq!(y3.read_source(..MAX24, ()).unwrap().as_slice().len(), 0x010203);
	assert_eq!(y1.position(), 0x02);
	assert_eq!(y2.position(), 0x0104);
	assert_eq!(y3.position(), 0x010206);
}

#[test]
fn source_read_source_fn()
{
	let mut x = [0;70000];
	x[0] = 1;
	x[1] = 2;
	x[2] = 3;
	let mut y1 = Source::new(&x);
	let mut y2 = Source::new(&x);
	let mut y3 = Source::new(&x);
	y1.read_source_fn(..MAX8, false, |x|{assert_eq!(x.as_slice().len(), 0x01); Ok(())}, (), ()).unwrap();
	y2.read_source_fn(..MAX16, false, |x|{assert_eq!(x.as_slice().len(), 0x0102); Ok(())}, (), ()).unwrap();
	y3.read_source_fn(..MAX24, false, |x|{assert_eq!(x.as_slice().len(), 0x010203); Ok(())}, (), ()).unwrap();
	assert_eq!(y1.position(), 0x02);
	assert_eq!(y2.position(), 0x0104);
	assert_eq!(y3.position(), 0x010206);
}

#[test]
fn source_read_ux()
{
	let x = [
		2, 6,
		7, 3, 2, 6,
		8, 4, 3, 3, 6, 7,
		2, 4, 5, 3, 2, 1, 7, 8,
		7, 3, 7, 2, 1, 3, 5, 6, 8, 4, 4, 7, 8, 6, 4, 0
	];
	let mut y = Source::new(&x);
	assert_eq!(y.read_u8(()).unwrap(), 0x02);
	assert_eq!(y.read_u8(()).unwrap(), 0x06);
	assert_eq!(y.read_u16(()).unwrap(), 0x0703);
	assert_eq!(y.read_u16(()).unwrap(), 0x0206);
	assert_eq!(y.read_u24(()).unwrap(), 0x080403);
	assert_eq!(y.read_u24(()).unwrap(), 0x030607);
	assert_eq!(y.read_u32(()).unwrap(), 0x02040503);
	assert_eq!(y.read_u32(()).unwrap(), 0x02010708);
	assert_eq!(y.read_u64(()).unwrap(), 0x0703070201030506);
	assert_eq!(y.read_u64(()).unwrap(), 0x0804040708060400);
	y.read_u8(()).unwrap_err();
	y.read_u16(()).unwrap_err();
	y.read_u24(()).unwrap_err();
	y.read_u32(()).unwrap_err();
	y.read_u64(()).unwrap_err();
}

#[test]
fn source_read_ux_one()
{
	let x = [0];
	let mut y = Source::new(&x);
	y.read_u16(()).unwrap_err();
	y.read_u24(()).unwrap_err();
	y.read_u32(()).unwrap_err();
	y.read_u64(()).unwrap_err();
}

#[test]
fn source_read_ux_one_shy()
{
	let x = [1,2,3,4,5,6,7];
	let mut y = Source::new(&x);
	let mut y1 = Source::new(&x);
	y.read_u64(()).unwrap_err();
	y.read_u32(()).unwrap();
	y.read_u32(()).unwrap_err();
	y.read_u16(()).unwrap();
	y.read_u16(()).unwrap_err();
	y1.read_slice(8, ()).unwrap_err();
	y1.read_source(8, ()).unwrap_err();
}

#[test]
fn source_slice_raw()
{
	let x = [1,2,3,8,5,6,7];
	let mut y = Source::new(&x);
	y.read_u16(()).unwrap();
	let marker = y.slice_marker();
	assert_eq!(y.position(), 2);
	y.read_u32(()).unwrap();
	let z = marker.commit(&y);
	assert_eq!(y.position(), 6);
	y.read_u8(()).unwrap();
	assert!(y.at_end());
	assert_eq!(y.position(), 7);
	assert_eq!(z, &x[2..6]);
}

#[test]
fn asn1_basic_oid()
{
	let x = [6, 4, 82, 5, 29, 17];
	let mut y = Source::new(&x);
	let z = y.read_asn1_value(AnyAsn1Tag, |x|x).unwrap();
	assert_eq!(z.tag, ASN1_OID);
	assert_eq!(z.value.as_slice(), &x[2..6]);
	assert_eq!(z.raw_p, &x[2..6]);
}

#[test]
fn asn1_basic_sequence()
{
	let x = [0x30, 4, 82, 5, 29, 17];
	let mut y = Source::new(&x);
	let z = y.read_asn1_value(AnyAsn1Tag, |x|x).unwrap();
	assert_eq!(z.tag, ASN1_SEQUENCE);
	assert_eq!(z.value.as_slice(), &x[2..6]);
	assert_eq!(z.raw_p, &x[2..6]);
}

#[test]
fn asn1_one_shy()
{
	let x = [0x30, 4, 82, 5, 29];
	let mut y = Source::new(&x);
	assert_eq!(y.read_asn1_value(AnyAsn1Tag, |x|x).unwrap_err(), Asn1Error::Truncated);;
}

#[test]
fn asn1_extended_tag()
{
	let x = [0xFF, 0x1F, 4, 0, 5, 29, 17, 0];
	let mut y = Source::new(&x);
	let z = y.read_asn1_value(AnyAsn1Tag, |x|x).unwrap();
	assert_eq!(z.tag, Asn1Tag::Private(31, true));
	assert_eq!(z.value.as_slice(), &x[3..7]);
}

#[test]
fn asn1_extended_tag_under()
{
	let x = [0xFF, 0x1E, 4, 0, 5, 29, 17, 0];
	let mut y = Source::new(&x);
	assert_eq!(y.read_asn1_value(AnyAsn1Tag, |x|x).unwrap_err(), Asn1Error::NotDer);
}

#[test]
fn asn1_extended_tag_2()
{
	let x = [0x9F, 0xFE, 0x7F, 4, 0, 5, 29, 17, 0];
	let mut y = Source::new(&x);
	let z = y.read_asn1_value(AnyAsn1Tag, |x|x).unwrap();
	assert_eq!(z.tag, Asn1Tag::ContextSpecific(16255, false));
	assert_eq!(z.value.as_slice(), &x[4..8]);
}

#[test]
fn asn1_extended_tag_2_0x81()
{
	let x = [0x9F, 0x81, 0x00, 4, 0, 5, 29, 17, 0];
	let mut y = Source::new(&x);
	let z = y.read_asn1_value(AnyAsn1Tag, |x|x).unwrap();
	assert_eq!(z.tag, Asn1Tag::ContextSpecific(128, false));
	assert_eq!(z.value.as_slice(), &x[4..8]);
}

#[test]
fn asn1_extended_tag_2_under()
{
	let x = [0xFF, 0x80, 0x7F, 4, 0, 5, 29, 17, 0];
	let mut y = Source::new(&x);
	assert_eq!(y.read_asn1_value(AnyAsn1Tag, |x|x).unwrap_err(), Asn1Error::NotDer);
}

#[test]
fn asn1_extended_tag_3()
{
	let x = [0x9F, 0xFD, 0xFE, 0x7F, 4, 0, 5, 29, 17, 0];
	let mut y = Source::new(&x);
	let z = y.read_asn1_value(AnyAsn1Tag, |x|x).unwrap();
	assert_eq!(z.tag, Asn1Tag::ContextSpecific(2064255, false));
	assert_eq!(z.value.as_slice(), &x[5..9]);
}

#[test]
fn asn1_extended_tag_3_0x8180()
{
	let x = [0x9F, 0x81, 0x80, 0x00, 4, 0, 5, 29, 17, 0];
	let mut y = Source::new(&x);
	let z = y.read_asn1_value(AnyAsn1Tag, |x|x).unwrap();
	assert_eq!(z.tag, Asn1Tag::ContextSpecific(16384, false));
	assert_eq!(z.value.as_slice(), &x[5..9]);
}

#[test]
fn asn1_extended_tag_3_under()
{
	let x = [0xFF, 0x80, 0xFF, 0x7F, 4, 0, 5, 29, 17, 0];
	let mut y = Source::new(&x);
	assert_eq!(y.read_asn1_value(AnyAsn1Tag, |x|x).unwrap_err(), Asn1Error::NotDer);
}

#[test]
fn asn1_extended_tag_max()
{
	let x = [0xDF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x7F, 4, 0, 5, 29, 17, 0];
	let mut y = Source::new(&x);
	let z = y.read_asn1_value(AnyAsn1Tag, |x|x).unwrap();
	assert_eq!(z.tag, Asn1Tag::Private(0x7FFFFFFFFFFFFFFF, false));
	assert_eq!(z.value.as_slice(), &x[11..15]);
}

#[test]
fn asn1_extended_tag_over()
{
	let x = [0xDF, 0x81, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x00, 4, 0, 5, 29, 17, 0];
	let mut y = Source::new(&x);
	assert_eq!(y.read_asn1_value(AnyAsn1Tag, |x|x).unwrap_err(), Asn1Error::TagTooBig);
}


#[test]
fn asn1_taglen_1()
{
	let mut x = [0;128+3];
	x[0] = 0x30;
	x[1] = 0x81;
	x[2] = 0x80;
	x[3] = 0x55;
	x[2+128] = 0xAA;
	let mut y = Source::new(&x);
	let z = y.read_asn1_value(AnyAsn1Tag, |x|x).unwrap();
	assert_eq!(z.tag, ASN1_SEQUENCE);
	assert_eq!(z.value.as_slice(), &x[3..]);
}

#[test]
fn asn1_taglen_1_under()
{
	let mut x = [0;127+3];
	x[0] = 0x30;
	x[1] = 0x81;
	x[2] = 0x7F;
	x[3] = 0x55;
	x[2+127] = 0xAA;
	let mut y = Source::new(&x);
	assert_eq!(y.read_asn1_value(AnyAsn1Tag, |x|x).unwrap_err(), Asn1Error::NotDer);
}

#[test]
fn asn1_taglen_2()
{
	let mut x = [0;259+4];
	x[0] = 0x30;
	x[1] = 0x82;
	x[2] = 0x01;
	x[3] = 0x03;
	x[4] = 0x55;
	x[3+259] = 0xAA;
	let mut y = Source::new(&x);
	let z = y.read_asn1_value(AnyAsn1Tag, |x|x).unwrap();
	assert_eq!(z.tag, ASN1_SEQUENCE);
	assert_eq!(z.value.as_slice(), &x[4..]);
}

#[test]
fn asn1_taglen_2_under()
{
	let mut x = [0;255+4];
	x[0] = 0x30;
	x[1] = 0x82;
	x[2] = 0x00;
	x[3] = 0xFF;
	x[4] = 0x55;
	x[3+255] = 0xAA;
	let mut y = Source::new(&x);
	assert_eq!(y.read_asn1_value(AnyAsn1Tag, |x|x).unwrap_err(), Asn1Error::NotDer);
}

#[test]
fn asn1_taglen_3()
{
	let mut x = [0;66052+5];
	x[0] = 0x30;
	x[1] = 0x83;
	x[2] = 0x01;
	x[3] = 0x02;
	x[4] = 0x04;
	x[5] = 0x55;
	x[4+66052] = 0xAA;
	let mut y = Source::new(&x);
	let z = y.read_asn1_value(AnyAsn1Tag, |x|x).unwrap();
	assert_eq!(z.tag, ASN1_SEQUENCE);
	assert_eq!(z.value.as_slice().len(), 0x010204);
	assert_eq!(z.value.as_slice(), &x[5..]);
}

#[test]
fn asn1_taglen_3_under()
{
	let mut x = [0;65535+5];
	x[0] = 0x30;
	x[1] = 0x83;
	x[2] = 0x00;
	x[3] = 0xFF;
	x[4] = 0xFF;
	x[5] = 0x55;
	x[4+65535] = 0xAA;
	let mut y = Source::new(&x);
	assert_eq!(y.read_asn1_value(AnyAsn1Tag, |x|x).unwrap_err(), Asn1Error::NotDer);
}

#[test]
fn asn1_taglen_4()
{
	use self::std::vec::Vec;
	let mut x = Vec::with_capacity(0x01020304+6);
	x.push(0x30);
	x.push(0x84);
	x.push(0x01);
	x.push(0x02);
	x.push(0x03);
	x.push(0x04);
	for i in 0..0x01020304 { x.push(i as u8); }
	let mut y = Source::new(&x);
	assert_eq!(y.read_asn1_value(AnyAsn1Tag, |x|x).unwrap_err(), Asn1Error::LengthTooBig);
}

#[test]
fn asn1_taglen_0()
{
	let x = [0x30, 0x80];
	let mut y = Source::new(&x);
	assert_eq!(y.read_asn1_value(AnyAsn1Tag, |x|x).unwrap_err(), Asn1Error::NotDer);
}
