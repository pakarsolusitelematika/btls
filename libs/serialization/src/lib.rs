//!Routines for serializing and unserializing TLS and ASN.1 DER data.
//!
//!This crate provodes methods for serializing and unserializing TLS structures and ASN.1 DER.
#![no_std]
#![forbid(unsafe_code)]
#![forbid(missing_docs)]

#[macro_use]
extern crate btls_aux_fail;

mod sink;
pub use self::sink::*;
mod source;
pub use self::source::*;
mod npslice;
pub use self::npslice::*;
