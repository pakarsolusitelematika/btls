//!CSPRNG
//!
//!This crate provodes a Cryptographically Secure Random Number Generator.
#![forbid(unsafe_code)]
#![forbid(missing_docs)]
extern crate btls_aux_securebuf;
extern crate rand;
use btls_aux_securebuf::wipe_buffer2;
use rand::OsRng;
use rand::Rng;
use std::rc::Rc;
use std::cell::RefCell;
use std::cmp::min;

//This was exposed, but is now deprecated
#[doc(hidden)]
#[derive(Clone,Debug,PartialEq,Eq)]
pub enum RngError
{
	///Random number generator failed!
	RngFailure,
	#[doc(hidden)]
	Hidden__
}

fn get_osrng() -> OsRng
{
	loop {
		match OsRng::new() { Ok(x) => return x, Err(_) => () };
	}
}

///Fill buffer with random numbers.
///
///Takes in:
///
/// * An output buffer `x`.
///
///This function fills the output buffer with cryptographically secure random numbers.
///
///This function is both thread-safe and fork-safe. However, it is not safe to call in signal handlers.
pub fn secure_random(x: &mut [u8])
{
	//This is thread local to only initialize the generator once per thread. This matters if get_random isn't
	//present, since each initialization opens a file.
	//WARNING: This can't be replaced by ThreadRng due to a security flaw in ThreadRng of all current versions
	//of the rand crate (it is at 0.3.15 at the moment).
	thread_local!(static RNG_FOR_THREAD: Rc<RefCell<OsRng>> = {Rc::new(RefCell::new(get_osrng()))});
	//fill_bytes can't fail.
	RNG_FOR_THREAD.with(|y|y.borrow_mut().fill_bytes(x));
}

///A CSPRNG stream.
///
///This structure is a fast random number stream, initialized once and then used to extract random numbers
///quickly.
///
///This stream is much faster than [`secure_random()`], but does not handle multithreading nor multiprocessing
///(see section DANGER). It is not safe for signal handlers either.
///
///# DANGER:
///
///This structure is NOT SAFE for multithreading or forking. Do not transfer it between threads or across forks.
///Failing this likely leads to predictable output, leading to catastrophic system failure.
///
///[`secure_random()`]: fn.secure_random.html
pub struct RandomStream
{
	state: [u32; 16],
	output: [u32; 16],
	used: usize,
}

impl Drop for RandomStream
{
	fn drop(&mut self)
	{
		wipe_buffer2(&mut self.state);
		wipe_buffer2(&mut self.output);
	}
}

//Return 1 if x is 0, 0 otherwise.
fn iszero(x: u32) -> u32
{
	let x = x | (x >> 16);
	let x = x | (x >> 8);
	let x = x | (x >> 4);
	let x = x | (x >> 2);
	let x = x | (x >> 1);
	let x = x & 1;
	1 - x
}

macro_rules! Qround
{
	($s:expr, $a:expr, $b:expr, $c:expr, $d:expr) => {{
		$s[$a] = $s[$a].wrapping_add($s[$b]); $s[$d] ^= $s[$a]; $s[$d] = $s[$d].rotate_left(16);
		$s[$c] = $s[$c].wrapping_add($s[$d]); $s[$b] ^= $s[$c]; $s[$b] = $s[$b].rotate_left(12);
		$s[$a] = $s[$a].wrapping_add($s[$b]); $s[$d] ^= $s[$a]; $s[$d] = $s[$d].rotate_left(8);
		$s[$c] = $s[$c].wrapping_add($s[$d]); $s[$b] ^= $s[$c]; $s[$b] = $s[$b].rotate_left(7);
	}}
}

impl RandomStream
{
	fn refill(&mut self)
	{
		for i in 0..16 { self.output[i] = self.state[i]; }
		for _ in 0..10 {
			Qround!(self.output, 0, 4, 8,12);
			Qround!(self.output, 1, 5, 9,13);
			Qround!(self.output, 2, 6,10,14);
			Qround!(self.output, 3, 7,11,15);
			Qround!(self.output, 0, 5,10,15);
			Qround!(self.output, 1, 6,11,12);
			Qround!(self.output, 2, 7, 8,13);
			Qround!(self.output, 3, 4, 9,14);
		}
		for i in 0..16 { self.output[i] = self.output[i].wrapping_add(self.state[i]); }
		//Advance the state.
		self.state[12] = self.state[12].wrapping_add(1);
		self.state[13] = self.state[13].wrapping_add(iszero(self.state[12]));
		self.state[14] = self.state[14].wrapping_add(iszero(self.state[12]) * iszero(self.state[13]));
		self.used = 0;
	}
	///Create a new random stream.
	///
	///This method creates a new random stream, initializing it off the system Random Number Generator.
	pub fn new() -> RandomStream
	{
		let mut buf = [0; 64];
		secure_random(&mut buf);
		let mut state = RandomStream {
			state: [0; 16],
			output: [0; 16],
			used: 0,
		};
		for i in 0..64 { state.state[i >> 2] |= (buf[i] as u32) << ((i & 3) << 3); }
		wipe_buffer2(&mut buf);
		state.refill();
		state
	}
	///Fill buffer with random bytes.
	///
	///Takes in:
	///
	/// * A random number stream `self`.
	/// * An output buffer `x`.
	///
	///This method fills the output buffer with with cryptographically secure random bytes from the stream.
	pub fn fill_bytes(&mut self, x: &mut [u8])
	{
		let mut itr = 0;
		while itr < x.len() {
			//used < 64, itr < x.len().
			let block = min(64 - self.used, x.len() - itr);
			let xb = &mut x[itr..][..block];			//itr+block <= x.len().
			for i in 0..block {
				let idx = i + self.used;
				xb[i] = (self.output[idx >> 2] >> ((idx & 3) << 3)) as u8;
			}
			itr += block;
			self.used += block;
			if self.used == 64 { self.refill() }
		}
	}
}
