//!Ed25519 signing
//!
//!This crate provodes routines for Ed25519 signing.
#![forbid(unsafe_code)]
#![forbid(missing_docs)]
#[macro_use]
extern crate btls_aux_fail;
extern crate btls_aux_keyconvert;
extern crate btls_aux_random;
extern crate btls_aux_securebuf;
extern crate btls_aux_signature_algo;
extern crate btls_aux_xed25519;
use btls_aux_keyconvert::Ed25519KeyDecode;
use btls_aux_random::secure_random;
use btls_aux_securebuf::wipe_buffer;
use btls_aux_signature_algo::SignatureKeyPair;
use btls_aux_xed25519::{ed25519_pubkey, ed25519_sign};

use std::fmt::{Display, Error as FmtError, Formatter};


///Error in loading Ed25519 key pair
///
///This enumeration describes the error that occured in loading of Ed25519 key pair.
#[derive(Clone,Debug,PartialEq,Eq)]
pub enum Ed25519KeyLoadingError
{
	#[doc(hidden)]
	InvalidEd25519DataLength(usize),
	#[doc(hidden)]
	InvalidEd25519Keypair,
	#[doc(hidden)]
	Hidden__
}

impl Display for Ed25519KeyLoadingError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use Ed25519KeyLoadingError::*;
		match self {
			&InvalidEd25519DataLength(x) => fmt.write_fmt(format_args!("Invalid Ed25519 key length \
				{} (expected 64)", x)),
			&InvalidEd25519Keypair => fmt.write_str("Inconsistent public/private keys in Ed25519 key"),
			&Hidden__ => fmt.write_str("Hidden__")
		}
	}
}

///Ed25519 keypair.
///
///This structure describes an Ed25519 key pair.
pub struct Ed25519KeyPair
{
	sk: [u8;32],
	pk: [u8;32],
}

impl Drop for Ed25519KeyPair
{
	fn drop(&mut self)
	{
		wipe_buffer(&mut self.sk);
	}
}

const EDDSA_TLS_ID: u16 = 0x807;

impl Ed25519KeyPair
{
	///Deprecated, use `.SignatureKeyPair::keypair_sign()` instead.
	pub fn sign(&self, message: &[u8], output: &mut [u8]) -> Result<(), ()>
	{
		fail_if!(output.len() != 64, ());
		self.keypair_sign(message, EDDSA_TLS_ID, output)?; 
		Ok(())
	}
	///Deprecated, use `.SignatureKeyPair::keypair_sign()` instead.
	pub fn sign2(&self, message: &[u8], output: &mut [u8], algo: u16) -> Result<usize, ()>
	{
		self.keypair_sign(message, algo, output)
	}
	///Deprecated, use `.SignatureKeyPair::keypair_load()` instead.
	pub fn from_bytes(data: &[u8]) -> Result<(Ed25519KeyPair, Vec<u8>), Ed25519KeyLoadingError>
	{
		match Ed25519KeyPair::keypair_load(data) {
			Ok((_, x, y)) => Ok((x, y)),
			Err(x) => Err(x)
		}
	}
	///Deprecated, use `.SignatureKeyPair::keypair_generate()` instead.
	pub fn generate() -> Result<Vec<u8>, ()>
	{
		Ed25519KeyPair::keypair_generate(())
	}
}

impl SignatureKeyPair for Ed25519KeyPair
{
	type KeygenParameters = ();
	type BadKeyError = Ed25519KeyLoadingError;
	type Variant = ();
	fn keypair_generate(_params: ()) -> Result<Vec<u8>, ()>
	{
		const SIZE: usize = 32;
		let mut privkey = [0; SIZE];
		let mut pubkey = [0; SIZE];
		let mut target = Vec::with_capacity(2*SIZE);
		target.resize(2*SIZE, 0);
		secure_random(&mut privkey);
		ed25519_pubkey(&mut pubkey, &privkey);
		(&mut target[..SIZE]).copy_from_slice(&privkey);
		(&mut target[SIZE..]).copy_from_slice(&pubkey);
		wipe_buffer(&mut privkey);
		Ok(target)
	}
	fn keypair_load(data: &[u8]) -> Result<((), Ed25519KeyPair, Vec<u8>), Ed25519KeyLoadingError>
	{
		use Ed25519KeyLoadingError::*;
		let keypair = Ed25519KeyDecode::new(data).map_err(|_|InvalidEd25519DataLength(data.len()))?;

		//Check the private and public keys correspond.
		let mut tmp = [0;32];
		let mut tmp2 = [0;32];
		fail_if!(keypair.private.len() != tmp.len(), InvalidEd25519Keypair);	//Should be the same size.
		tmp.copy_from_slice(keypair.private);
		ed25519_pubkey(&mut tmp2, &tmp);
		fail_if!(&tmp2[..] != keypair.public, InvalidEd25519Keypair);

		//Serialize to internal representation and SPKI public key.
		let output = Ed25519KeyPair{sk: tmp, pk: tmp2};
		wipe_buffer(&mut tmp);
		let ed25519_header = b"\x30\x2A\x30\x05\x06\x03\x2B\x65\x70\x03\x21\x00";
		let mut out = Vec::new();
		out.extend_from_slice(ed25519_header);
		out.extend_from_slice(&keypair.public);
		Ok(((), output, out))
	}
	fn keypair_sign(&self, message: &[u8], scheme: u16, output: &mut [u8]) -> Result<usize, ()>
	{
		fail_if!(scheme != EDDSA_TLS_ID, ());
		let mut signature = [0;64];
		fail_if!(signature.len() > output.len(), ());
		ed25519_sign(&self.sk, &self.pk, message, &mut signature);
		(&mut output[..signature.len()]).copy_from_slice(&signature);
		Ok(signature.len())
	}
}
