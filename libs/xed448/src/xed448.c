#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#define FE448_STORE_SIZE 56
#define CURVE448_SCALAR_OCTETS 56
#define CURVE448_POINT_STORE_SIZE 57
#define EDWARDS_CONST -39081
#define MONTGOMERY_CONSTANT 39082
#define CURVE448_SCALAR_WORDS 15

#define WORDSHIFT 3
#define WORDBYTES (1 << WORDSHIFT)
#define WORDBYTEMASK (WORDBYTES - 1)

#define ROT(x,p) ((x << (p)) | (x >> ((64 - (p)) & 63)))
#define ROTP(s, i ,p) do { s[i] = ROT(s[i], p); } while(0)

#define THETA_ROW(state, i) (state[0+(i)]^state[5+(i)]^state[10+(i)]^state[15+(i)]^state[20+(i)])
#define THETA_COL(state, i, d) do { state[(i)+0] ^= d; state[(i)+5] ^= d; state[(i)+10] ^= d; state[(i)+15] ^= d; \
	state[(i)+20] ^= d; } while(0)

static void do_global_init();

static void xor_load_le64(uint64_t* w, const uint8_t* b, size_t nw)
{
	unsigned n, i;
	for(n = 0; n < nw; n++)
		for(i = 0; i < 8; i++) w[n] ^= (uint64_t)b[i + 8 * n] << ((i&7) << 3);
}

static void xor_load_le64_1(uint64_t* w, const uint8_t* b)
{
	unsigned i;
	for(i = 0; i < 8; i++) *w ^= (uint64_t)b[i] << (i << 3);
}

static void theta(uint64_t* state)
{
	uint64_t c0 = THETA_ROW(state, 0);
	uint64_t c1 = THETA_ROW(state, 1);
	uint64_t c2 = THETA_ROW(state, 2);
	uint64_t c3 = THETA_ROW(state, 3);
	uint64_t c4 = THETA_ROW(state, 4);
	uint64_t d0 = c4 ^ ROT(c1, 1);
	uint64_t d1 = c0 ^ ROT(c2, 1);
	uint64_t d2 = c1 ^ ROT(c3, 1);
	uint64_t d3 = c2 ^ ROT(c4, 1);
	uint64_t d4 = c3 ^ ROT(c0, 1);
	THETA_COL(state, 0, d0);
	THETA_COL(state, 1, d1);
	THETA_COL(state, 2, d2);
	THETA_COL(state, 3, d3);
	THETA_COL(state, 4, d4);
}

static void rho(uint64_t* state)
{
	                     ROTP(state,  1,  1); ROTP(state,  2, 62); ROTP(state,  3, 28); ROTP(state,  4, 27);
	ROTP(state,  5, 36); ROTP(state,  6, 44); ROTP(state,  7,  6); ROTP(state,  8, 55); ROTP(state,  9, 20);
	ROTP(state, 10,  3); ROTP(state, 11, 10); ROTP(state, 12, 43); ROTP(state, 13, 25); ROTP(state, 14, 39);
	ROTP(state, 15, 41); ROTP(state, 16, 45); ROTP(state, 17, 15); ROTP(state, 18, 21); ROTP(state, 19,  8);
	ROTP(state, 20, 18); ROTP(state, 21,  2); ROTP(state, 22, 61); ROTP(state, 23, 56); ROTP(state, 24, 14);
}

static void pi(uint64_t* state)
{
	uint64_t tmp = state[1];
	state[ 1] = state[ 6]; state[ 6] = state[ 9]; state[ 9] = state[22]; state[22] = state[14];
	state[14] = state[20]; state[20] = state[ 2]; state[ 2] = state[12]; state[12] = state[13];
	state[13] = state[19]; state[19] = state[23]; state[23] = state[15]; state[15] = state[ 4];
	state[ 4] = state[24]; state[24] = state[21]; state[21] = state[ 8]; state[ 8] = state[16];
	state[16] = state[ 5]; state[ 5] = state[ 3]; state[ 3] = state[18]; state[18] = state[17];
	state[17] = state[11]; state[11] = state[ 7]; state[ 7] = state[10];
	state[10] = tmp;
}

#define CHI_ROW(state, i) do { \
	uint64_t x0 = state[i + 0]; \
	uint64_t x1 = state[i + 1]; \
	state[i + 0] ^= (~state[i + 1]) & state[i + 2]; \
	state[i + 1] ^= (~state[i + 2]) & state[i + 3]; \
	state[i + 2] ^= (~state[i + 3]) & state[i + 4]; \
	state[i + 3] ^= (~state[i + 4]) & x0; \
	state[i + 4] ^= (~x0) & x1; \
} while(0)

static void chi(uint64_t* state)
{
	CHI_ROW(state, 0);
	CHI_ROW(state, 5);
	CHI_ROW(state, 10);
	CHI_ROW(state, 15);
	CHI_ROW(state, 20);
}

static void sha3_compress(uint64_t* state)
{
	unsigned round;
	const static uint64_t rc[24] = {
		0x0000000000000001, 0x0000000000008082, 0x800000000000808a, 0x8000000080008000,
		0x000000000000808b, 0x0000000080000001, 0x8000000080008081, 0x8000000000008009,
		0x000000000000008a, 0x0000000000000088, 0x0000000080008009, 0x000000008000000a,
		0x000000008000808b, 0x800000000000008b, 0x8000000000008089, 0x8000000000008003,
		0x8000000000008002, 0x8000000000000080, 0x000000000000800a, 0x800000008000000a,
		0x8000000080008081, 0x8000000000008080, 0x0000000080000001, 0x8000000080008008,
	};
	for(round = 0; round < 24; round++) {
		theta(state);
		rho(state);
		pi(state);
		chi(state);
		state[0] ^= rc[round];		/*iota.*/
	}
}

struct sha3_state
{
	uint64_t state[25];
	uint64_t maxblock;
	uint64_t blockfill;
	uint64_t outbytes;
	uint64_t fpad;
};

static void sha3_init_low(struct sha3_state* state, uint8_t rbytes)
{
	unsigned i;
	for(i = 0; i < 25; i++) state->state[i] = 0;
	state->blockfill = 0;
	state->maxblock = 200 - 2 * rbytes;
	state->outbytes = rbytes;
	state->fpad = 6;	/*The usual SHA-3.*/
}

static void shake256_init(struct sha3_state* state, size_t rbytes)
{
	sha3_init_low(state, 32);
	state->outbytes = rbytes;	/*Overwrite the length.*/
	state->fpad = 31;		/*Shake.*/
}


static void sha3_input(struct sha3_state* state, const uint8_t* buf, size_t bufsize)
{
	while(bufsize > 0) {
		if(state->blockfill == 0 && bufsize >= state->maxblock) {
			/*Do entiere block at once.*/
			xor_load_le64(state->state, buf, state->maxblock >> WORDSHIFT);
			bufsize -= state->maxblock;
			buf += state->maxblock;
			state->blockfill = state->maxblock;
		} else if((state->blockfill & WORDBYTEMASK) == 0 && bufsize >= WORDBYTES) {
			/*Do word at once.*/
			uint64_t* ptr = &state->state[state->blockfill >> WORDSHIFT];
			xor_load_le64_1(ptr, buf);
			bufsize -= WORDBYTES;
			buf += WORDBYTES;
			state->blockfill += WORDBYTES;
		} else {
			/*Do byte at a time.*/
			unsigned blockfill = state->blockfill;
			state->state[blockfill >> WORDSHIFT] ^= (uint64_t)buf[0]
				<< ((blockfill & WORDBYTEMASK) << 3);
			bufsize--;
			buf++;
			state->blockfill += 1;
		}
		if(state->blockfill == state->maxblock) {
			/*Compress it.*/
			sha3_compress(state->state);
			state->blockfill = 0;
		}
	}
}

static void sha3_result(const struct sha3_state* state, uint8_t* out)
{
	uint64_t block[25];
	unsigned last;
	size_t i;
	for(i = 0; i < 25; i++) block[i] = state->state[i];
	/*Pad.*/
	block[state->blockfill>>WORDSHIFT] ^= state->fpad << ((state->blockfill & WORDBYTEMASK) << 3);
	last = state->maxblock - 1;
	block[last>>WORDSHIFT] ^= 128ULL << ((last & WORDBYTEMASK) << 3);
	/*Start Squeeze*/
	for(i = 0; i < state->outbytes; i += state->maxblock) {
		unsigned j;
		sha3_compress(block);
		for(j = 0; j < state->maxblock && i + j < state->outbytes; j++)
			out[i + j] = block[j >> WORDSHIFT] >> ((j & WORDBYTEMASK) << 3);
	}
}

static void ecclib_bigint_add(uint32_t* out, const uint32_t* in1, const uint32_t* in2, size_t words)
{
	uint64_t carry = 0;
	size_t i;
	for(i = 0; i < words; i++) {
		carry = carry + in1[i] + in2[i];
		out[i] = carry;
		carry >>= 32;
	}
}

static void ecclib_bigint_sub(uint32_t* out, const uint32_t* in1, const uint32_t* in2, size_t words)
{
	uint64_t borrow = 0;
	size_t i;
	for(i = 0; i < words; i++) {
		uint64_t r = (uint64_t)in1[i] - in2[i] - borrow;
		out[i] = r;
		borrow = r >> 63;
	}
}

static void ecclib_bigint_mul(uint32_t* out, const uint32_t* in1, size_t words1, const uint32_t* in2, size_t words2)
{
	if(words2 == 1) {
		/*Special case for 1-word multiplication.*/
		uint64_t carry = 0;
		uint64_t m;
		size_t i;

		m = in2[0];
		for(i = 0; i < words1; i++) {
			carry += m * in1[i];
			out[i] = carry;
			carry >>= 32;
		}
		out[words1] = carry;
	} else {
		uint64_t carry_l = 0, carry_h = 0;
		size_t o, words0;

		words0 = words1 + words2;
		for(o = 0; o < words0; o++) {
			size_t i, imin, imax;
			imin = (o >= words2) ? (o - words2 + 1) : 0;
			imax = (words1 > o + 1) ? (o + 1) : words1;
			for(i = imin; i < imax; i++) {
				uint64_t r = (uint64_t)in1[i] * in2[o - i];
				carry_l += r & ((1ULL << 32) - 1);
				carry_h += r >> 32;
			}
			out[o] = carry_l;
			carry_l = carry_h + (carry_l >> 32);
			carry_h = 0;
		}
	}
}

static void ecclib_bigint_load(uint32_t* out, size_t outwords, const uint8_t* in, size_t inoctets)
{
	size_t i;
	if(inoctets > 4 * outwords)
		inoctets = 4 * outwords;
	for(i = 0; i < outwords; i++)
		out[i] = 0;
	for(i = 0; i < inoctets; i++)
		out[i>>2] |= (uint32_t)in[i] << ((i&3)<<3);
}

static void ecclib_bigint_store(uint8_t* out, size_t outoctets, const uint32_t* in)
{
	size_t i;
	for(i = 0; i < outoctets; i++)
		out[i] = in[i>>2] >> ((i&3)<<3);
}

/**
 * Element of field GF(2^448-2^224-1)
 */
typedef struct fe448
{
#ifdef USE_64BIT_MATH
	uint64_t x[8];
#else
	uint32_t x[15];	/*+1 Extra word for intermediates too large.*/
#endif
} fe448_t;

#ifdef USE_64BIT_MATH

static const uint64_t emask = (1ULL << 56) - 1;
static const unsigned eshift = 56;

/*The modulus as words*/
static const uint64_t modulus64[8] = {
	0xFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFF,
	0xFFFFFFFFFFFFFE, 0xFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFF
};

static void fe448_load(struct fe448* out, const uint8_t* num)
{
	uint64_t tmp = 0;
	unsigned shift = 0;
	unsigned idx = 0;
	unsigned i;

	for(i = 0; i < 8; i++) {
		while(shift < eshift) {
			uint64_t byte;
			if(idx == 56) break;
			byte = num[idx];
			tmp = tmp + (byte << shift);
			idx++;
			shift += 8;
		}
		out->x[i] = tmp & emask;
		shift -= eshift;
		tmp >>= eshift;
	}
}

static void reduce(uint64_t out[8], const uint64_t in[8])
{
	unsigned i;
	uint64_t lt, gt, mask, borrow;
	uint64_t s[8] = {0};

	uint64_t carry = (in[7] >> eshift);
	for(i = 0; i < 8; i++) out[i] = in[i];
	out[7] = in[7] & emask;
	for(i = 0; i < 8; i++) {
		carry = out[i] + carry;
		out[i] = carry & emask;
		carry >>= eshift;
	}
	out[7] = out[7] + (carry << eshift);
	gt = 0;
	lt = 0;
	for(i = 7; i < 8; i--) {
		uint64_t neutral = (gt ^ 1) & (lt ^ 1);
		gt |= neutral & (out[i] > modulus64[i]);
		lt |= neutral & (out[i] < modulus64[i]);
	}
	mask = -(1 ^ lt);
	for(i = 0; i < 8; i++) s[i] = modulus64[i] & mask;
	borrow = 0;
	for(i = 0; i < 8; i++) {
		out[i] = out[i] - s[i] - borrow;
		borrow = out[i] >> 63;
		out[i] &= emask;
	}
}

static void fe448_store(uint8_t* out, const struct fe448* elem)
{
	uint64_t tmp = 0;
	unsigned shift = 0;
	unsigned idx = 0;
	unsigned i;
	uint64_t y[8];

	reduce(y, elem->x);
	for(i = 0; i < 8; i++) {
		tmp = tmp + (y[i] << shift);
		shift = shift + eshift;
		while(shift >= 8) {
			if(idx >= 56) return;
			out[idx] = tmp;
			idx++;
			tmp >>= 8;
			shift -= 8;
		}
	}
}

static void fe448_zero(struct fe448* out)
{
	unsigned i;
	for(i = 0; i < 8; i++) out->x[i] = 0;
}

static void fe448_load_small(struct fe448* out, uint32_t num)
{
	unsigned i;
	for(i = 0; i < 8; i++) out->x[i] = 0;
	out->x[0] = num;
}

static void fe448_neg(struct fe448* out, const struct fe448* in)
{
	unsigned i;
	uint64_t carry = 0;

	for(i = 0; i < 8; i++) {
		uint64_t base = emask - (i == 4);
		uint64_t r = 4 * base - in->x[i] + carry;
		out->x[i] = r & emask;
		carry = r >> eshift;
	}
	out->x[0] += carry;
	out->x[4] += carry;
}

static void fe448_sub(struct fe448* out, const struct fe448* in1, const struct fe448* in2)
{
	uint64_t carry = 0;
	unsigned i;

	for(i = 0; i < 8; i++) {
		uint64_t base = emask - (i == 4);
		uint64_t r = 4 * base + in1->x[i] - in2->x[i] + carry;
		out->x[i] = r & emask;
		carry = r >> eshift;
	}
	out->x[0] += carry;
	out->x[4] += carry;
}

static void fe448_add(struct fe448* out, const struct fe448* in1, const struct fe448* in2)
{
	uint64_t carry = 0;
	unsigned i;

	for(i = 0; i < 8; i++) {
		carry = in1->x[i] + in2->x[i] + carry;
		out->x[i] = carry & emask;
		carry >>= eshift;
	}
	out->x[0] += carry;
	out->x[4] += carry;
}

#define WM(X, Y) ((__uint128_t)(X) * (__uint128_t)(Y))

static void fe448_smul(struct fe448* out, const struct fe448* in1, uint32_t in2)
{
	__uint128_t carry = 0, carry2 = 0;
	unsigned i;

	for(i = 0; i < 8; i++) {
		carry = WM(in1->x[i], in2) + carry;
		out->x[i] = carry & emask;
		carry >>= eshift;
	}
	carry2 = out->x[0] + carry;
	out->x[0] = carry2 & emask;
	carry2 >>= eshift;
	out->x[1] += carry2;
	carry2 = out->x[4] + carry;
	out->x[4] = carry2 & emask;
	carry2 >>= eshift;
	out->x[5] += carry2;
}

static void fe448_mul(struct fe448* out, const struct fe448* in1, const struct fe448* in2)
{
	/*FIXME: Use Karatsuba*/
	__uint128_t w = 0;
	uint64_t carry2;
	uint64_t a0, a1, a2, a3, a4, a5, a6, a7, b0, b1, b2, b3, b4, b5, b6, b7, c0, c1, c2, c3, d1, d2, d3;

	a0 = in1->x[0], a1 = in1->x[1], a2 = in1->x[2], a3 = in1->x[3], a4 = in1->x[4], a5 = in1->x[5],
		a6 = in1->x[6], a7 = in1->x[7];
	b0 = in2->x[0], b1 = in2->x[1], b2 = in2->x[2], b3 = in2->x[3], b4 = in2->x[4], b5 = in2->x[5],
		b6 = in2->x[6], b7 = in2->x[7];
	c0 = b0 + b4, c1 = b1 + b5, c2 = b2 + b6, c3 = b3 + b7;
	d1 = b1 + (b5 << 1), d2 = b2 + (b6 << 1), d3 = b3 + (b7 << 1);

	w = w + WM(a0, b0) + WM(a1, b7) + WM(a2, b6) + WM(a3, b5) + WM(a4, b4) + WM(a5, c3) + WM(a6, c2) + WM(a7, c1);
	out->x[0]=w & emask; w = w >> eshift;
	w = w + WM(a0, b1) + WM(a1, b0) + WM(a2, b7) + WM(a3, b6) + WM(a4, b5) + WM(a5, b4) + WM(a6, c3) + WM(a7, c2);
	out->x[1]=w & emask; w = w >> eshift;
	w = w + WM(a0, b2) + WM(a1, b1) + WM(a2, b0) + WM(a3, b7) + WM(a4, b6) + WM(a5, b5) + WM(a6, b4) + WM(a7, c3);
	out->x[2]=w & emask; w = w >> eshift;
	w = w + WM(a0, b3) + WM(a1, b2) + WM(a2, b1) + WM(a3, b0) + WM(a4, b7) + WM(a5, b6) + WM(a6, b5) + WM(a7, b4);
	out->x[3]=w & emask; w = w >> eshift;
	w = w + WM(a0, b4) + WM(a1, c3) + WM(a2, c2) + WM(a3, c1) + WM(a4, c0) + WM(a5, d3) + WM(a6, d2) + WM(a7, d1);
	out->x[4]=w & emask; w = w >> eshift;
	w = w + WM(a0, b5) + WM(a1, b4) + WM(a2, c3) + WM(a3, c2) + WM(a4, c1) + WM(a5, c0) + WM(a6, d3) + WM(a7, d2);
	out->x[5]=w & emask; w = w >> eshift;
	w = w + WM(a0, b6) + WM(a1, b5) + WM(a2, b4) + WM(a3, c3) + WM(a4, c2) + WM(a5, c1) + WM(a6, c0) + WM(a7, d3);
	out->x[6]=w & emask; w = w >> eshift;
	w = w + WM(a0, b7) + WM(a1, b6) + WM(a2, b5) + WM(a3, b4) + WM(a4, c3) + WM(a5, c2) + WM(a6, c1) + WM(a7, c0);
	out->x[7]=w & emask; w = w >> eshift;

	out->x[0] = out->x[0] + w;
	carry2 = out->x[0] >> eshift;
	out->x[0] = out->x[0] & emask;
	out->x[1] = out->x[1] + carry2;
	out->x[4] = out->x[4] + w;
	carry2 = out->x[4] >> eshift;
	out->x[4] = out->x[4] & emask;
	out->x[5] = out->x[5] + carry2;
}

static void fe448_sqr(struct fe448* out, const struct fe448* in)
{
	__uint128_t w = 0;
	uint64_t carry2;
	uint64_t a0, a1, a2, a3, a4, a5, a6, a7, b1, b2, b3, c1, c2, c3, d0, d1, d2, d3, e0, e1, e4;

	a0 = in->x[0], a1 = in->x[1], a2 = in->x[2], a3 = in->x[3], a4 = in->x[4], a5 = in->x[5],
		a6 = in->x[6], a7 = in->x[7];
	b1 = a1 + a5, b2 = a2 + a6, b3 = a3 + a7;
	c1 = a1 + (a5 << 1), c2 = a2 + (a6 << 1), c3 = a3 + (a7 << 1);
	d0 = (a0 << 1) + a4, d1 = (a1 << 1) + a5, d2 = (a2 << 1) + a6, d3 = (a3 << 1) + a7;
	e0 = (a0 << 1), e1 = (a1 << 1), e4 = (a4 << 1);

	w=w + WM(a0, a0) + WM(d1, a7) + WM(d2, a6) + WM(d3, a5) + WM(a4, a4);
	out->x[0]=w & emask; w = w >> eshift;
	w=w + WM(e0, a1) + WM(d2, a7) + WM(d3, a6) + WM(e4, a5);
	out->x[1]=w & emask; w = w >> eshift;
	w=w + WM(e0, a2) + WM(a1, a1) + WM(d3, a7) + WM(e4, a6) + WM(a5, a5);
	out->x[2]=w & emask; w = w >> eshift;
	w=w + WM(e0, a3) + WM(e1, a2) + WM(e4, a7) + WM(a5, a6) + WM(a6, a5);
	out->x[3]=w & emask; w = w >> eshift;
	w=w + WM(d0, a4) + WM(a1, b3) + WM(a2, b2) + WM(a3, b1) + WM(a5, c3) + WM(a6, c2) + WM(a7, c1);
	out->x[4]=w & emask; w = w >> eshift;
	w=w + WM(d0, a5) + WM(d1, a4) + WM(a2, b3) + WM(a3, b2) + WM(a6, c3) + WM(a7, c2);
	out->x[5]=w & emask; w = w >> eshift;
	w=w + WM(d0, a6) + WM(d1, a5) +WM(d2, a4) + WM(a3, b3) + WM(a7, c3);
	out->x[6]=w & emask; w = w >> eshift;
	w=w + WM(d0, a7) + WM(d1, a6) +WM(d2, a5) + WM(d3, a4);
	out->x[7]=w & emask; w = w >> eshift;

	out->x[0] = out->x[0] + w;
	carry2 = out->x[0] >> eshift;
	out->x[0] = out->x[0] & emask;
	out->x[1] = out->x[1] + carry2;
	out->x[4] = out->x[4] + w;
	carry2 = out->x[4] >> eshift;
	out->x[4] = out->x[4] & emask;
	out->x[5] = out->x[5] + carry2;
}

static void fe448_load_cond(struct fe448* out, const struct fe448* in, uint32_t flag)
{
	unsigned i;
	uint64_t rflag, iflag;

	rflag = -(uint64_t)flag;
	iflag = ~rflag;
	for(i = 0; i < 8; i++)
		out->x[i] = (out->x[i] & iflag) | (in->x[i] & rflag);
}

static void fe448_select(struct fe448* out, const struct fe448* in1, const struct fe448* in0, uint32_t flag)
{
	unsigned i;
	uint64_t rflag, iflag;

	rflag = -(uint64_t)flag;
	iflag = ~rflag;
	for(i = 0; i < 8; i++)
		out->x[i] = (in0->x[i] & iflag) | (in1->x[i] & rflag);
}

static void fe448_cswap(struct fe448* x0, struct fe448* x1, uint32_t flag)
{
	unsigned i;
	uint64_t rflag;

	rflag = -(uint64_t)flag;
	for(i = 0; i < 8; i++) {
		uint64_t t = rflag & (x0->x[i] ^ x1->x[i]);
		x0->x[i] ^= t;
		x1->x[i] ^= t;
	}
}
#else
/*in is assumed to be <2^926*/
static void reduce_after_mul(uint32_t out[15], const uint32_t in[30])
{
	uint64_t tmp[29];
	uint64_t tmp2[22];
	uint64_t carry;
	unsigned i;

	for(i = 0; i < 29; i++) tmp[i] = in[i];
	/*Now in represents at most 2^926*/
	for(i = 0; i < 14; i++) tmp2[i] = in[i];
	for(i = 14; i < 22; i++) tmp2[i] = 0;
	for(i = 0; i < 15; i++) {
		tmp2[i] += in[i+14];
		tmp2[i+7] += in[i+14];
	}
	/*Now tmp2 represents at most 2^703*/
	for(i = 0; i < 14; i++) tmp[i] = tmp2[i];
	for(i = 14; i < 15; i++) tmp[i] = 0;
	for(i = 0; i < 8; i++) {
		tmp[i] += tmp2[i+14];
		tmp[i+7] += tmp2[i+14];
	}
	/*Now tmp represents at most 2^480.*/
	tmp[0] += tmp[14];
	tmp[7] += tmp[14];
	tmp[14] = 0;
	/*Now tmp represents at most 2^449, resolve carries.*/
	carry = 0;
	for(i = 0; i < 15; i++) {
		carry += tmp[i];
		out[i] = carry;
		carry >>= 32;
	}
}

/*in is assumed to be <2^480*/
static void reduce_after_add(uint32_t out[15], const uint32_t in[15])
{
	uint64_t tmp[15];
	uint64_t carry;
	unsigned i;

	for(i = 0; i < 15; i++) tmp[i] = in[i];
	/*Now tmp represents at most 2^480.*/
	tmp[0] += tmp[14];
	tmp[7] += tmp[14];
	tmp[14] = 0;
	/*Now tmp represents at most 2^449, resolve carries.*/
	carry = 0;
	for(i = 0; i < 15; i++) {
		carry += tmp[i];
		out[i] = carry;
		carry >>= 32;
	}
}

/*65536 * modulus*/
static const uint32_t sub_bias[15] = {
	0xFFFF0000, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
	0xFFFEFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
	0x0000FFFF
};

/*The modulus as words*/
static const uint32_t modulus32[15] = {
	0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
	0xFFFFFFFE, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
	0x00000000
};

static void fe448_store(uint8_t* out, const struct fe448* elem)
{
	/*We need to perform full reduction.*/
	uint64_t tmp[15];
	uint32_t tmp2[15];
	uint32_t tmp3[15];
	uint32_t sub[15];
	uint32_t lt, gt, mask;
	uint64_t carry;
	unsigned i;

	for(i = 0; i < 15; i++) tmp[i] = elem->x[i];
	/*Now tmp represents at most 2^480.*/
	tmp[0] += tmp[14];
	tmp[7] += tmp[14];
	tmp[14] = 0;
	/*Now tmp represents at most 2^449, resolve carries.*/
	carry = 0;
	for(i = 0; i < 15; i++) {
		carry += tmp[i];
		tmp2[i] = carry;
		carry >>= 32;
	}
	/*Compare against modulus.*/
	lt = 0;
	gt = 0;
	for(i = 14; i < 15; i--) {
		uint8_t neutral = (1^gt)&(1^lt);
		lt |= neutral & (tmp[i] < modulus32[i]);
		gt |= neutral & (tmp[i] > modulus32[i]);
	}
	/*Load substract.*/
	mask = -(1^lt);
	for(i = 0; i < 15; i++) sub[i] = modulus32[i] & mask;
	/*Substract the modulus if needed.*/
	ecclib_bigint_sub(tmp3, tmp2, sub, 15);
	/*Now, write this to bytes*/
	for(i = 0; i < 56; i++)
		out[i] = tmp3[i>>2] >> ((i&3)<<3);
}

static void fe448_load(struct fe448* out, const uint8_t* num)
{
	unsigned i;
	for(i = 0; i < 15; i++)
		out->x[i] = 0;
	for(i = 0; i < 56; i++)
		out->x[i>>2] |= num[i] << ((i&3)<<3);
}

static void fe448_zero(struct fe448* out)
{
	unsigned i;
	for(i = 0; i < 15; i++) out->x[i] = 0;
}

static void fe448_load_small(struct fe448* out, uint32_t num)
{
	unsigned i;
	for(i = 0; i < 15; i++) out->x[i] = 0;
	out->x[0] = num;
}

static void fe448_neg(struct fe448* out, const struct fe448* in)
{
	uint32_t result[15];
	ecclib_bigint_sub(result, sub_bias, in->x, 15);
	reduce_after_add(out->x, result);
}

static void fe448_sub(struct fe448* out, const struct fe448* in1, const struct fe448* in2)
{
	uint32_t tmp[15];
	uint32_t result[15];
	ecclib_bigint_add(tmp, in1->x, sub_bias, 15);
	ecclib_bigint_sub(result, tmp, in2->x, 15);
	reduce_after_add(out->x, result);
}

static void fe448_add(struct fe448* out, const struct fe448* in1, const struct fe448* in2)
{
	uint32_t result[15];
	ecclib_bigint_add(result, in1->x, in2->x, 15);
	reduce_after_add(out->x, result);
}

static void fe448_smul(struct fe448* out, const struct fe448* in1, uint32_t in2)
{
	uint32_t result[16];
	ecclib_bigint_mul(result, in1->x, 15, &in2, 1);
	/*This is so small reduce_after_add can do it.*/
	reduce_after_add(out->x, result);
}

static void fe448_mul(struct fe448* out, const struct fe448* in1, const struct fe448* in2)
{
	uint32_t result[30];
	ecclib_bigint_mul(result, in1->x, 15, in2->x, 15);
	reduce_after_mul(out->x, result);
}

static void fe448_sqr(struct fe448* out, const struct fe448* in)
{
	uint32_t result[30];
	ecclib_bigint_mul(result, in->x, 15, in->x, 15);
	reduce_after_mul(out->x, result);
}

static void fe448_load_cond(struct fe448* out, const struct fe448* in, uint32_t flag)
{
	unsigned i;
	uint32_t rflag, iflag;

	rflag = -flag;
	iflag = ~rflag;
	for(i = 0; i < 15; i++)
		out->x[i] = (out->x[i] & iflag) | (in->x[i] & rflag);
}

static void fe448_select(struct fe448* out, const struct fe448* in1, const struct fe448* in0, uint32_t flag)
{
	unsigned i;
	uint32_t rflag, iflag;

	rflag = -flag;
	iflag = ~rflag;
	for(i = 0; i < 15; i++)
		out->x[i] = (in0->x[i] & iflag) | (in1->x[i] & rflag);
}

static void fe448_cswap(struct fe448* x0, struct fe448* x1, uint32_t flag)
{
	unsigned i;
	uint32_t rflag;

	rflag = -flag;
	for(i = 0; i < 15; i++) {
		uint32_t t = rflag & (x0->x[i] ^ x1->x[i]);
		x0->x[i] ^= t;
		x1->x[i] ^= t;
	}
}
#endif

/*The modulus as bytes*/
static const uint8_t modulus[56] = {
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
};

static int fe448_check(const uint8_t* num)
{
	unsigned i;
	uint8_t lt, gt;

	lt = 0;
	gt = 0;
	for(i = 55; i < 56; i--) {
		uint8_t neutral = (1^gt)&(1^lt);
		lt |= neutral & (num[i] < modulus[i]);
		gt |= neutral & (num[i] > modulus[i]);
	}
	return (int)lt;
}

static void fe448_sqrn(struct fe448* out, const struct fe448* in, unsigned count)
{
	struct fe448 tmp1, tmp2;
	unsigned i;

	if(count % 2 == 0) {
		fe448_sqr(&tmp1, in);
		for(i = 0; i < (count-2)/2; i++) {
			fe448_sqr(&tmp2, &tmp1);
			fe448_sqr(&tmp1, &tmp2);
		}
		fe448_sqr(out, &tmp1);
	} else {
		fe448_sqr(&tmp1, in);
		fe448_sqr(&tmp2, &tmp1);
		for(i = 0; i < (count-3)/2; i++) {
			fe448_sqr(&tmp1, &tmp2);
			fe448_sqr(&tmp2, &tmp1);
		}
		fe448_sqr(out, &tmp2);
	}
}

static int fe448_sqrt(struct fe448* out, const struct fe448* in)
{
	/*Raise to power of 2^446-2^222*/
	struct fe448 x0, x1, x2;
	uint8_t check1[56];
	uint8_t check2[56];
	unsigned i;
	uint8_t syndrome;

	fe448_sqr(&x1, in);		/*2^2-2=2*/
	fe448_mul(&x0, &x1, in);	/*2^2-1*/
	fe448_sqr(&x1, &x0);		/*2^3-2*/
	fe448_mul(&x2, &x1, in);	/*2^3-1*/
	fe448_sqrn(&x1, &x2, 3);	/*2^6-2^3*/
	fe448_mul(&x0, &x1, &x2);	/*2^6-1*/
	fe448_sqr(&x1, &x0);		/*2^7-2*/
	fe448_mul(&x0, &x1, in);	/*2^7-1*/
	fe448_sqrn(&x2, &x0, 7);	/*2^14-2^7*/
	fe448_mul(&x1, &x2, &x0);	/*2^14-1*/
	fe448_sqrn(&x2, &x1, 14);	/*2^28-2^14*/
	fe448_mul(&x0, &x2, &x1);	/*2^28-1*/
	fe448_sqrn(&x2, &x0, 28);	/*2^56-2^28*/
	fe448_mul(&x1, &x2, &x0);	/*2^56-1*/
	fe448_sqrn(&x0, &x1, 56);	/*2^112-2^56*/
	fe448_mul(&x2, &x0, &x1);	/*2^112-1*/
	fe448_sqrn(&x0, &x2, 112);	/*2^224-2^112*/
	fe448_mul(&x1, &x0, &x2);	/*2^224-1*/
	fe448_sqrn(&x0, &x1, 222);	/*2^446-2^222*/
	fe448_sqr(&x1, &x0);		/*Check.*/
	fe448_store(check1, in);
	fe448_store(check2, &x1);
	syndrome = 0;
	for(i = 0; i < 56; i++)
		syndrome |= (check1[i] ^ check2[i]);
	syndrome = (syndrome | (syndrome >> 4)) & 15;
	syndrome = (syndrome | (syndrome >> 2)) & 3;
	syndrome = (syndrome | (syndrome >> 1)) & 1;
	fe448_load_cond(out, &x0, syndrome ^ 1);
	return 1-(int)syndrome;
}

static void fe448_inv(struct fe448* out, const struct fe448* in)
{
	/*Raise to power of 2^448-2^224-3.*/
	struct fe448 x0, x1, x2;
	fe448_sqr(&x1, in);		/*2^2-2=2*/
	fe448_mul(&x0, &x1, in);	/*2^2-1*/
	fe448_sqr(&x1, &x0);		/*2^3-2*/
	fe448_mul(&x0, &x1, in);	/*2^3-1*/
	fe448_sqrn(&x1, &x0, 3);	/*2^6-2^3*/
	fe448_mul(&x2, &x1, &x0);	/*2^6-1*/
	fe448_sqrn(&x0, &x2, 6);	/*2^12-2^6*/
	fe448_mul(&x1, &x0, &x2);	/*2^12-1*/
	fe448_sqr(&x0, &x1);		/*2^13-2*/
	fe448_mul(&x1, &x0, in);	/*2^13-1*/
	fe448_sqrn(&x2, &x1, 13);	/*2^26-2^13*/
	fe448_mul(&x0, &x2, &x1);	/*2^26-1*/
	fe448_sqr(&x1, &x0);		/*2^27-2*/
	fe448_mul(&x0, &x1, in);	/*2^27-1*/
	fe448_sqrn(&x2, &x0, 27);	/*2^54-2^27*/
	fe448_mul(&x1, &x2, &x0);	/*2^54-1*/
	fe448_sqr(&x0, &x1);		/*2^55-2*/
	fe448_mul(&x2, &x0, in);	/*2^55-1*/
	fe448_sqrn(&x0, &x2, 55);	/*2^110-2^55*/
	fe448_mul(&x1, &x0, &x2);	/*2^110-1*/
	fe448_sqr(&x0, &x1);		/*2^111-2*/
	fe448_mul(&x2, &x0, in);	/*2^111-1*/
	fe448_sqrn(&x1, &x2, 111);	/*2^222-2^111*/
	fe448_mul(&x0, &x1, &x2);	/*2^222-1*/
	fe448_sqr(&x2, &x0);		/*2^223-2*/
	fe448_mul(&x1, &x2, in);	/*2^223-1*/
	fe448_sqrn(&x2, &x1, 223);	/*2^446-2^223*/
	fe448_mul(&x1, &x2, &x0);	/*2^446-2^222-1*/
	fe448_sqrn(&x0, &x1, 2);	/*2^448-2^224-4*/
	fe448_mul(out, &x0, in);	/*2^448-2^224-3*/
}


static fe448_t base_x;
static fe448_t base_y;
static fe448_t const_one;

extern void call_only_once_xed448(void(*fn)());


typedef struct point448
{
	fe448_t x;
	fe448_t y;
	fe448_t z;
} point448_t;

static int _point_set(point448_t* point, const uint8_t* data);
static int _point_montgomery(fe448_t* output, const point448_t* point);
static void _point_double(point448_t* out, const point448_t* in);
static void _point_add(point448_t* out, const point448_t* in1, const point448_t* in2);
static void _point_base(point448_t* point);
static void _point_zero(point448_t* point);
static void _point_load_cond(point448_t* out, const point448_t* in, uint32_t flag);

static void _point_get(const point448_t* point, uint8_t* output)
{
	uint8_t tmp[FE448_STORE_SIZE];
	fe448_t x, y, iz;
	size_t i;

	fe448_inv(&iz, &point->z);
	fe448_mul(&x, &point->x, &iz);
	fe448_mul(&y, &point->y, &iz);
	fe448_store(tmp, &x);
	fe448_store(output, &y);
	/*Clear any extra octets.*/
	for(i = FE448_STORE_SIZE; i < CURVE448_POINT_STORE_SIZE; i++) output[i] = 0;
	output[CURVE448_POINT_STORE_SIZE - 1] |= tmp[0] << 7;	/*Sign of X.*/
}

static void _montgomery(fe448_t* dblx, fe448_t* dblz, fe448_t* sumx, fe448_t* sumz,
	const fe448_t* ax, const fe448_t* az, const fe448_t* bx, const fe448_t* bz,
	const fe448_t* diff)
{
	fe448_t A, B, C, D, E, F, G, H;
	fe448_add(&A, ax, az);
	fe448_sqr(&F, &A);
	fe448_sub(&B, ax, az);
	fe448_sqr(&G, &B);
	fe448_sub(&E, &F, &G);
	fe448_add(&C, bx, bz);
	fe448_sub(&D, bx, bz);
	fe448_mul(&H, &D, &A);
	fe448_mul(&A, &C, &B);
	fe448_add(&B, &H, &A);
	fe448_sqr(sumx, &B);
	fe448_sub(&B, &H, &A);
	fe448_sqr(&A, &B);
	fe448_mul(sumz, diff, &A);
	fe448_mul(dblx, &F, &G);
	/*Absorb possible negative montgomery constant into next addition, turning it into substration.*/
	if(MONTGOMERY_CONSTANT > 0) {
		fe448_smul(&A, &E, MONTGOMERY_CONSTANT);
		fe448_add(&B, &G, &A);
	} else {
		fe448_smul(&A, &E, -MONTGOMERY_CONSTANT);
		fe448_sub(&B, &G, &A);
	}
	fe448_mul(dblz, &E, &B);
}

static int _montgomery_mul(fe448_t* output, const uint32_t* mul, const fe448_t* base)
{
	fe448_t x1a, z1a, x2a, z2a, x1b, z1b, x2b, z2b;
	unsigned i, j;
	uint32_t lbit;

	fe448_load_small(&x1a, 1);
	fe448_zero(&z1a);
	x2a = *base;
	fe448_load_small(&z2a, 1);
	fe448_zero(&x1b);
	fe448_load_small(&z1b, 1);
	fe448_zero(&x2b);
	fe448_load_small(&z2b, 1);

	lbit = 0;
	for(i = CURVE448_SCALAR_WORDS - 1; i < CURVE448_SCALAR_WORDS; i--) {
		uint32_t x = mul[i];
		for(j = 0; j < 16; j++) {
			uint32_t bit = (x >> 31);
			fe448_cswap(&x1a, &x2a, bit ^ lbit);
			fe448_cswap(&z1a, &z2a, bit ^ lbit);
			_montgomery(&x1b, &z1b, &x2b, &z2b, &x1a, &z1a, &x2a, &z2a, base);
			lbit = bit;
			x <<= 1;
			bit = (x >> 31);
			fe448_cswap(&x1b, &x2b, bit ^ lbit);
			fe448_cswap(&z1b, &z2b, bit ^ lbit);
			_montgomery(&x1a, &z1a, &x2a, &z2a, &x1b, &z1b, &x2b, &z2b, base);
			x <<= 1;
			lbit = bit;
		}
	}
	fe448_cswap(&x1a, &x2a, lbit);
	fe448_cswap(&z1a, &z2a, lbit);
	fe448_inv(&z2a, &z1a);
	fe448_mul(output, &x1a, &z2a);
	return 1;
}

static void _init_window(point448_t win[16], const point448_t* base)
{
	unsigned i;

	_point_zero(&win[0]);
	win[1] = *base;
	/*Fill the rest.*/
	for(i = 2; i < 16; i += 2) {
		_point_double(win + i, win + (i >> 1));
		_point_add(win + i + 1, win + i, base);
	}
}

static void _load_window(point448_t* res, point448_t win[16], uint32_t index)
{
	unsigned i;
	for(i = 0; i < 16; i++) {
		uint32_t flag = i - index;
		/*flag = (flag | (flag >> 4)) & 15;	//window has 16 elements.*/
		flag = (flag | (flag >> 2)) & 3;
		flag = (flag | (flag >> 1)) & 1;
		_point_load_cond(res, win + i, 1  - flag);
	}
}

static point448_t* points;

static void _point_mul_base(point448_t* out, const uint32_t* mul)
{
	unsigned i, j;
	unsigned k = 0;
	point448_t accum, tmp2;

	do_global_init();
	_point_zero(&accum);
	_point_zero(&tmp2);	/*Avoid false warning from Valgrind.*/
	for(i = 0; i < CURVE448_SCALAR_WORDS; i++) {
		uint32_t word = mul[i];
		/*Note: This assumes window is 4 bits.*/
		for(j = 0; j < 8; j++, k++) {
			_load_window(&tmp2, points + k * 16, word & 15);
			_point_add(&accum, &accum, &tmp2);
			word >>= 4;
		}
	}
	*out = accum;
}

static void _point_mul(point448_t* out, const uint32_t* mul, const point448_t* in)
{
	unsigned i, j;
	point448_t win[16], accum, tmp2;

	do_global_init();

	_init_window(win, in);
	_point_zero(&accum);
	_point_zero(&tmp2);	/*Avoid false warning from Valgrind.*/
	for(i = CURVE448_SCALAR_WORDS - 1; i < CURVE448_SCALAR_WORDS; i--) {
		uint32_t word = mul[i];
		/*Note: This assumes window is 4 bits.*/
		for(j = 0; j < 8; j++) {
			_load_window(&tmp2, win, word >> 28);
			_point_double(&accum, &accum);
			_point_double(&accum, &accum);
			_point_double(&accum, &accum);
			_point_double(&accum, &accum);
			_point_add(&accum, &accum, &tmp2);
			word <<= 4;
		}
	}
	*out = accum;
}

static void _point_load_cond(point448_t* out, const point448_t* in, uint32_t flag)
{
	fe448_load_cond(&out->x, &in->x, flag);
	fe448_load_cond(&out->y, &in->y, flag);
	fe448_load_cond(&out->z, &in->z, flag);
}

static void _point_zero(point448_t* point)
{
	fe448_zero(&point->x);
	fe448_load_small(&point->y, 1);
	fe448_load_small(&point->z, 1);
}

static void _point_base(point448_t* point)
{
	point->x = base_x;
	point->y = base_y;
	fe448_load_small(&point->z, 1);
}

static void _point_double(point448_t* out, const point448_t* in)
{
	fe448_t A, B, C, D, E, F;
	fe448_add(&A, &in->x, &in->y);
	fe448_sqr(&B, &A);
	fe448_sqr(&A, &in->x);
	fe448_sqr(&D, &in->y);
	fe448_add(&C, &A, &D);
	fe448_sqr(&E, &in->z);
	fe448_add(&F, &E, &E);
	fe448_sub(&E, &C, &F);
	fe448_sub(&F, &B, &C);
	fe448_mul(&out->x, &F, &E);
	fe448_sub(&B, &A, &D);
	fe448_mul(&out->y, &C, &B);
	fe448_mul(&out->z, &C, &E);
}
/*
static void _point_neg(point448_t* out, const point448_t* in)
{
	fe448_neg(&out->x, &in->x);
	out->y = in->y;
	out->z = in->z;
}
*/
static void _point_add(point448_t* out, const point448_t* in1, const point448_t* in2)
{
	fe448_t A, B, C, D, E, F, G, H;
	fe448_mul(&A, &in1->z, &in2->z);
	fe448_sqr(&B, &A);
	fe448_mul(&C, &in1->x, &in2->x);
	fe448_mul(&D, &in1->y, &in2->y);
	fe448_mul(&F, &C, &D);
	fe448_smul(&E, &F, -EDWARDS_CONST);	/*Really -39081, flip sign of E.*/
	fe448_add(&F, &B, &E);
	fe448_sub(&G, &B, &E);
	fe448_add(&B, &in1->x, &in1->y);
	fe448_add(&E, &in2->x, &in2->y);
	fe448_mul(&H, &B, &E);
	fe448_sub(&B, &H, &C);
	fe448_sub(&E, &B, &D);
	fe448_mul(&B, &F, &E);
	fe448_mul(&out->x, &A, &B);
	fe448_sub(&B, &D, &C);
	fe448_mul(&C, &G, &B);
	fe448_mul(&out->y, &A, &C);
	fe448_mul(&out->z, &F, &G);
}

static int _point_set(point448_t* point, const uint8_t* data)
{
	uint8_t _y[FE448_STORE_SIZE];
	fe448_t y, A, B, C;
	size_t i;
	uint8_t syndrome;
	uint32_t flag, needs_flip;

	do_global_init();

	for(i = 0; i < FE448_STORE_SIZE; i++) _y[i] = data[i];

	/*Check y in range.*/
	syndrome = 0;
	syndrome = 1 - fe448_check(_y);
	syndrome |= data[56]&127;  /*Excess bits MUST be 0.*/

	fe448_load(&y, _y);
	fe448_sqr(&A, &y);				/*y^2*/
	fe448_sub(&B, &const_one, &A);			/*1 - y^2*/
	fe448_smul(&C, &A, -EDWARDS_CONST);		/*-d*y^2*/
	fe448_add(&A, &C, &const_one);			/*-d*y^2 + 1*/
	fe448_inv(&C, &A);				/*1/(-d*y^2 + 1)*/
	fe448_mul(&A, &B, &C);				/*(1 - y^2)/(-d*y^2 + 1)*/
	flag = fe448_sqrt(&B, &A);			/*sqrt((1 - y^2)/(-d*y^2 + 1))*/
	/*Now (B,y) is correct up to sign of x if flag is 1.*/
	fe448_store(_y, &B);
	needs_flip = (data[CURVE448_POINT_STORE_SIZE - 1] >> 7) ^ (_y[0] & 1);

	/*Check for x=0 and needs_flip, that is illegal.*/
	for(i = 0; i < FE448_STORE_SIZE; i++) syndrome |= _y[i];
	syndrome = (syndrome | (syndrome >> 4)) & 15;
	syndrome = (syndrome | (syndrome >> 2)) & 3;
	syndrome = (syndrome | (syndrome >> 1)) & 1;
	flag &= 1 - ((1 - syndrome) & needs_flip);

	fe448_neg(&A, &B);
	/*If needs_flip is 1, x coordinate is A, otherwise B.*/
	fe448_select(&C, &A, &B, needs_flip);
	fe448_load_cond(&point->x, &C, flag);
	fe448_load_cond(&point->y, &y, flag);
	fe448_load_cond(&point->z, &const_one, flag);
	return flag;
}

static int _point_montgomery(fe448_t* output, const point448_t* point)
{
	fe448_t A, B;
	fe448_inv(&A, &point->x);
	fe448_mul(&B, &point->y, &A);
	fe448_sqr(output, &B);
	return 1;
}

static const uint32_t high_word_mul[8] = {
	0x529eec34, 0x721cf5b5, 0xc8e9c2ab, 0x7a4cf635, 0x44a725bf, 0xeec492d9, 0x0cd77058, 0x00000002
};
static const uint32_t high_bit_mul[7] = {
	0x54a7bb0d, 0xdc873d6d, 0x723a70aa, 0xde933d8d, 0x5129c96f, 0x3bb124b6, 0x8335dc16
};
static const uint32_t curve_order[15] = {
	0xab5844f3, 0x2378c292, 0x8dc58f55, 0x216cc272, 0xaed63690, 0xc44edb49, 0x7cca23e9,
	0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0x3fffffff
};

static void reduce_partial(uint32_t* output, const uint32_t* input)
{
	uint32_t tmp1[23];
	uint32_t tmp2[23];
	uint32_t tmp3[23];
	unsigned i;
	uint32_t xh;

	/*First, split to lower 448-bit and upper 474-bit parts, multiply upper and accumulate. The largest possible*/
	/*result for this is 2^700 (22 words). We need to pad the lower words due to addition routine workings.*/
	ecclib_bigint_mul(tmp1, input + 14, 15, high_word_mul, sizeof(high_word_mul) / sizeof(high_word_mul[0]));
	for(i = 0; i < 14; i++) tmp2[i] = input[i];
	for(i = 14; i < 22; i++) tmp2[i] = 0;
	ecclib_bigint_add(tmp3, tmp1, tmp2, 22);
	/*Then split the lower 448-bit and upper 252-bit (8 words) parts, multiply upper and accumulate. The largest*/
	/*possible result for this is 2^478 (15 words). Again, lower words need to be padded.*/
	ecclib_bigint_mul(tmp1, tmp3 + 14, 8, high_word_mul, sizeof(high_word_mul) / sizeof(high_word_mul[0]));
	for(i = 0; i < 14; i++) tmp2[i] = tmp3[i];
	for(i = 14; i < 15; i++) tmp2[i] = 0;
	ecclib_bigint_add(tmp3, tmp1, tmp2, 15);
	/*Then split the lower 446-bit and upper 32-bit parts, multiply upper and accumulate. The result of this is*/
	/*less than 2^447. Pad multiply result, it is too small otherwise.*/
	xh = (tmp3[13] >> 30) | (tmp3[14] << 2);
	tmp3[13] &= (1UL << 30) - 1;
	ecclib_bigint_mul(tmp1, high_bit_mul, sizeof(high_bit_mul) / sizeof(high_bit_mul[0]), &xh, 1);
	for(i = 1 + sizeof(high_bit_mul) / sizeof(high_bit_mul[0]); i < 14; i++) tmp1[i] = 0;
	ecclib_bigint_add(output, tmp1, tmp3, 14);
	output[14] = 0;
}

static void reduce_full(uint8_t* output, const uint32_t* input)
{
	uint32_t tmp1[15];
	uint32_t tmp2[15];
	uint32_t tmp3[15];
	uint32_t xh, lt, gt, mask;
	size_t i;
	/*Split the lower and upper parts for one final multiplicative reduction. The multiply result is padded as it*/
	/*is too small otherwise.*/
	for(i = 0; i < 15; i++) tmp3[i] = input[i];
	xh = (tmp3[13] >> 30) | (tmp3[14] << 2);
	tmp3[13] &= (1UL << 30) - 1;
	tmp3[14] = 0;
	ecclib_bigint_mul(tmp1, high_bit_mul, sizeof(high_bit_mul) / sizeof(high_bit_mul[0]), &xh, 1);
	for(i = 1 + sizeof(high_bit_mul) / sizeof(high_bit_mul[0]); i < 14; i++) tmp1[i] = 0;
	ecclib_bigint_add(tmp2, tmp1, tmp3, 14);
	/*Now tmp2 is at most twice order. Conditionally substract order.*/
	lt = 0, gt = 0;
	for(i = 13; i < 14; i--) {
		uint32_t neutral = (1 ^ gt) & (1 ^ lt);
		lt |= neutral & (tmp2[i] < curve_order[i]);
		gt |= neutral & (tmp2[i] > curve_order[i]);
	}
	mask = -(1 ^ lt);
	for(i = 0; i < 14; i++) tmp3[i] = curve_order[i] & mask;
	ecclib_bigint_sub(tmp1, tmp2, tmp3, 14);
	/*Now tmp1 is final reduced value. Convert it to octets.*/
	ecclib_bigint_store(output, CURVE448_SCALAR_OCTETS, tmp1);
}

static void curve448_clamp_scalar(uint32_t* scalar)
{
	scalar[0] &= ~3;
	scalar[13] &= (1 << 31);
	scalar[14] = 0;
}

extern void zeroize_xed448(uint8_t* buf, size_t size);

static void _do_global_init()
{
	static uint8_t _x[FE448_STORE_SIZE] = {
		0x5E, 0xC0, 0x0C, 0xC7, 0x2B, 0xA8, 0x26, 0x26, 0x8E, 0x93, 0x00, 0x8B, 0xE1, 0x80,
		0x3B, 0x43, 0x11, 0x65, 0xB6, 0x2A, 0xF7, 0x1A, 0xAE, 0x12, 0x64, 0xA4, 0xD3, 0xA3,
		0x24, 0xE3, 0x6D, 0xEA, 0x67, 0x17, 0x0F, 0x47, 0x70, 0x65, 0x14, 0x9E, 0xDA, 0x36,
		0xBF, 0x22, 0xA6, 0x15, 0x1D, 0x22, 0xED, 0x0D, 0xED, 0x6B, 0xC6, 0x70, 0x19, 0x4F
	};
	static uint8_t _y[FE448_STORE_SIZE] = {
		0x14, 0xfa, 0x30, 0xf2, 0x5b, 0x79, 0x08, 0x98, 0xad, 0xc8, 0xd7, 0x4e, 0x2c, 0x13,
		0xbd, 0xfd, 0xc4, 0x39, 0x7c, 0xe6, 0x1c, 0xff, 0xd3, 0x3a, 0xd7, 0xc2, 0xa0, 0x05,
		0x1e, 0x9c, 0x78, 0x87, 0x40, 0x98, 0xa3, 0x6c, 0x73, 0x73, 0xea, 0x4b, 0x62, 0xc7,
		0xc9, 0x56, 0x37, 0x20, 0x76, 0x88, 0x24, 0xbc, 0xb6, 0x6e, 0x71, 0x46, 0x3f, 0x69
	};
	size_t i;
	point448_t base;

	fe448_load(&base_x, _x);
	fe448_load(&base_y, _y);
	fe448_load_small(&const_one, 1);

	while(!points) points = calloc(sizeof(point448_t), CURVE448_SCALAR_WORDS * 8 * 16);
	_point_base(&base);
	for(i = 0; i < CURVE448_SCALAR_WORDS * 8; i++) {
		_init_window(points + i * 16, &base);
		_point_double(&base, &base);
		_point_double(&base, &base);
		_point_double(&base, &base);
		_point_double(&base, &base);
	}
}

static void do_global_init()
{
	call_only_once_xed448(_do_global_init);
}

void curved448_scalarmult_basepoint_xed448(uint8_t* p, const uint8_t* s)
{
	fe448_t _pubkey;
	point448_t _tmp;
	uint32_t _secret[CURVE448_SCALAR_WORDS];
	ecclib_bigint_load(_secret, CURVE448_SCALAR_WORDS, s, FE448_STORE_SIZE);
	curve448_clamp_scalar(_secret);
	_point_mul_base(&_tmp, _secret);
	_point_montgomery(&_pubkey, &_tmp);
	zeroize_xed448((uint8_t*)_secret, sizeof(_secret));
	fe448_store(p, &_pubkey);
}

int curved448_scalarmult_xed448(uint8_t* o, const uint8_t* s, const uint8_t* b)
{
	fe448_t _ppubkey;
	fe448_t _shared;
	uint32_t _secret[CURVE448_SCALAR_WORDS];
	ecclib_bigint_load(_secret, CURVE448_SCALAR_WORDS, s, FE448_STORE_SIZE);
	curve448_clamp_scalar(_secret);
	fe448_load(&_ppubkey, b);
	_montgomery_mul(&_shared, _secret, &_ppubkey);
	fe448_store(o, &_shared);
	zeroize_xed448((uint8_t*)_secret, sizeof(_secret));
	zeroize_xed448((uint8_t*)&_shared, sizeof(_shared));
	return 0;
}

void ed448_publickey_xed448(const uint8_t* sk, uint8_t* pk)
{
	uint8_t intermediate[114];
	uint32_t scalar[CURVE448_SCALAR_WORDS];
	point448_t p;
	struct sha3_state ctx;

	shake256_init(&ctx, 114);
	sha3_input(&ctx, sk, 57);
	sha3_result(&ctx, intermediate);
	intermediate[0] &= ~3;
	intermediate[55] |= 128;
	intermediate[56] = 0;
	ecclib_bigint_load(scalar, CURVE448_SCALAR_WORDS, intermediate, 57);
	_point_mul_base(&p, scalar);
	zeroize_xed448((uint8_t*)scalar, sizeof(scalar));
	zeroize_xed448((uint8_t*)intermediate, sizeof(intermediate));
	_point_get(&p, pk);
}

const uint8_t* SIG_PREFIX = (uint8_t*)"SigEd448\x00\x00";
const size_t SIG_PREFIX_LEN = 10;

void ed448_sign_xed448(const uint8_t* msg, size_t msglen, const uint8_t* sk, const uint8_t* pk, uint8_t* sig)
{
	uint8_t intermediate[114];
	uint8_t _hram[114];
	uint32_t a[CURVE448_SCALAR_WORDS];
	uint32_t r[CURVE448_SCALAR_WORDS];
	uint32_t hram[CURVE448_SCALAR_WORDS];
	uint32_t hram_raw[2*CURVE448_SCALAR_WORDS];
	point448_t p;
	struct sha3_state ctx;
	shake256_init(&ctx, 114);
	sha3_input(&ctx, sk, 57);
	sha3_result(&ctx, intermediate);
	intermediate[0] &= ~3;
	intermediate[55] |= 128;
	intermediate[56] = 0;
	shake256_init(&ctx, 114);
	sha3_input(&ctx, SIG_PREFIX, SIG_PREFIX_LEN);
	sha3_input(&ctx, intermediate + 57, 57);
	sha3_input(&ctx, msg, msglen);
	sha3_result(&ctx, _hram);
	ecclib_bigint_load(hram_raw, 2*CURVE448_SCALAR_WORDS, _hram, 114);
	reduce_partial(r, hram_raw);
	_point_mul_base(&p, r);
	_point_get(&p, sig);
	shake256_init(&ctx, 114);
	sha3_input(&ctx, SIG_PREFIX, SIG_PREFIX_LEN);
	sha3_input(&ctx, sig, 57);
	sha3_input(&ctx, pk, 57);
	sha3_input(&ctx, msg, msglen);
	sha3_result(&ctx, _hram);
	ecclib_bigint_load(hram_raw, 2*CURVE448_SCALAR_WORDS, _hram, 114);
	reduce_partial(hram, hram_raw);
	ecclib_bigint_load(a, CURVE448_SCALAR_WORDS, intermediate, 57);
	ecclib_bigint_mul(hram_raw, hram, CURVE448_SCALAR_WORDS, a, CURVE448_SCALAR_WORDS);
	reduce_partial(hram, hram_raw);
	ecclib_bigint_add(a, r, hram, CURVE448_SCALAR_WORDS);
	reduce_full(sig + 57, a);
	sig[113] = 0;		/*Last byte unused.*/
	zeroize_xed448((uint8_t*)r, sizeof(r));
	zeroize_xed448(intermediate, sizeof(intermediate));
}

int ed448_sign_open_xed448(const uint8_t* msg, size_t msglen, const uint8_t* pk, const uint8_t* sig)
{
	point448_t r, a;
	size_t i;
	uint8_t _hram[114];
	uint32_t hram[CURVE448_SCALAR_WORDS];
	uint32_t hram_raw[2*CURVE448_SCALAR_WORDS];
	uint32_t s[CURVE448_SCALAR_WORDS];
	uint8_t scopy[57];
	uint8_t _lhs[57];
	uint8_t _rhs[57];
	point448_t lhs, rhs, tmp1;
	struct sha3_state ctx;
	uint8_t syndrome;

	shake256_init(&ctx, 114);
	sha3_input(&ctx, SIG_PREFIX, SIG_PREFIX_LEN);
	sha3_input(&ctx, sig, 57);
	sha3_input(&ctx, pk, 57);
	sha3_input(&ctx, msg, msglen);
	sha3_result(&ctx, _hram);
	ecclib_bigint_load(hram_raw, 2*CURVE448_SCALAR_WORDS, _hram, 114);
	reduce_partial(hram, hram_raw);
	/*Hack: Load and store s to check it is in base form.*/
	ecclib_bigint_load(s, CURVE448_SCALAR_WORDS, sig + 57, 57);
	reduce_full(scopy, s);
	scopy[56] = 0;
	syndrome = 0;
	for(i = 0; i < 57; i++) { syndrome |= sig[57+i] ^ scopy[i]; }
	if(syndrome) return -1;		/*s not canonical.*/
	if(!_point_set(&r, sig)) return -1;		/*Does canonical check.*/
	if(!_point_set(&a, pk)) return -1;		/*Does canonical check.*/
	/*sB = R + hA*/
	_point_mul_base(&lhs, s);
	_point_mul(&tmp1, hram, &a);
	_point_add(&rhs, &r, &tmp1);
	/*Multiply both sides by 4 and dump.*/
	_point_double(&tmp1, &lhs);
	_point_double(&lhs, &tmp1);
	_point_double(&tmp1, &rhs);
	_point_double(&rhs, &tmp1);
	_point_get(&lhs, _lhs);
	_point_get(&rhs, _rhs);
	/*Are equal?*/
	syndrome = 0;
	for(i = 0; i < 57; i++) { syndrome |= _lhs[i] ^ _rhs[i]; }
	return syndrome ? -1 : 0;
}
