extern crate gcc;
use gcc::Config;
use std::panic::catch_unwind;

fn main()
{
	//The UnwindSafe is retarded.
	let x = catch_unwind(||{
		let mut config = Config::new();
		config.file("src/xed448.c");
		config.define("USE_64BIT_MATH", None);
		config.compile("libxed448.a");
	});
	if x.is_err() {
		//Fall back to 32-bit.
		let mut config = Config::new();
		config.file("src/xed448.c");
		config.compile("libxed448.a");
	}
}
