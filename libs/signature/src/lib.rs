//!Signature verification
//!
//!This crate provodes TLS signature verification functions, across multiple signature algorithms.
//!
//!Currently supported algorithms include RSA-PKCS#1 v1.5, RSA-PSS, ECDSA, Ed25519 and Ed448.
#![forbid(unsafe_code)]
#![forbid(missing_docs)]

extern crate btls_aux_signature_algo;
use self::btls_aux_signature_algo::{SignatureAlgorithm, SignatureType, WrappedRecognizedSignatureAlgorithm};
extern crate btls_aux_signature_ecdsa;
use self::btls_aux_signature_ecdsa::{ecdsa_known_algo, ecdsa_verify_pkix};
extern crate btls_aux_signature_ed25519;
use self::btls_aux_signature_ed25519::{ed25519_known_algo, ed25519_verify_pkix};
extern crate btls_aux_signature_ed448;
use self::btls_aux_signature_ed448::{ed448_known_algo, ed448_verify_pkix};
extern crate btls_aux_signature_rsa;
use self::btls_aux_signature_rsa::{rsa_known_algo, rsa_verify_pkix};

struct SignatureVerifier
{
	recognize: fn(/*recalgo:*/&WrappedRecognizedSignatureAlgorithm, /*flags:*/u32) -> bool,
	validate: fn(/*key:*/&[u8], /*tbs:*/&[u8], /*recalgo*:*/&WrappedRecognizedSignatureAlgorithm,
		/*signature:*/&[u8], /*flags:*/u32) -> Result<(), ()>,
}

static VERIFIERS: [SignatureVerifier; 4] = [
	SignatureVerifier{recognize: ed25519_known_algo, validate: ed25519_verify_pkix},
	SignatureVerifier{recognize: ed448_known_algo, validate: ed448_verify_pkix},
	SignatureVerifier{recognize: ecdsa_known_algo, validate: ecdsa_verify_pkix},
	SignatureVerifier{recognize: rsa_known_algo, validate: rsa_verify_pkix},
];

///Validate a signature.
///
///Takes in:
///
/// * A key `key`. The key is in SubjectPublicKeyInfo format.
/// * A message `tbs`.
/// * A wrapped signature algorithm `algo`.
/// * A signature `signature`. The signature is big-endian encoding of integer.
/// * The signature condition flags `flags`.
///
///This function validates the signature over the message, using the key for the algorithm and the specified flags.
///
///This routine can verify RSA-PKCS#1 v1.5, RSA-PSS, ECDSA, Ed25519 and Ed448 signatures.
///
///If the signature verifies, returns `Ok(())`.
///
///If the signature does not verify, returns `Err(())`.
pub fn validate_signature<'a>(key: &[u8], tbs: &[u8], signature: SignatureBlock<'a>, flags: u32) ->
	Result<(), ()>
{
	let recalgo = WrappedRecognizedSignatureAlgorithm::from(signature.algorithm).ok_or(())?;
	for i in VERIFIERS.iter() {
		if (i.recognize)(&recalgo, flags) {
			return (i.validate)(key, tbs, &recalgo, signature.signature, flags);
		}
	}
	Err(())		//Not supported.
}

fn check_sig_algorithm_low(sigalgo: SignatureAlgorithm, flags: u32) -> bool
{
	let recalgo = match WrappedRecognizedSignatureAlgorithm::from(sigalgo) {
		Some(x) => x,
		None => return false
	};
	for i in VERIFIERS.iter() {
		if (i.recognize)(&recalgo, flags) { return true; }		//This verifier supports.
	}
	false		//Not supported.
}

///Check if `validate_signature` supports the specified algorithm.
///
///Takes in:
///
/// * A wrapped signature algorithm `algo`.
/// * The signature condition flags `flags`.
///
///This function determines if [`validate_signature()`] can verify the algorithm under the flags.
///
///If the specified algorithm can be verified, returns `true`, if not `false`.
///
///[`validate_signature()`]: fn.validate_signature.html
pub fn check_sig_algorithm_supported(sigalgo: SignatureAlgorithm, flags: u32) -> bool
{
	check_sig_algorithm_low(sigalgo, flags)
}

///A Signature block.
///
///This structure carries together a signature algorithm specification, and a signature value.
#[derive(Copy,Clone,Debug)]
pub struct SignatureBlock<'a>
{
	///Algorithm.
	///
	///This is the algorithm signature is for, expressed as either TLS SignatureScheme or PKIX Algorithm.
	pub algorithm: SignatureAlgorithm<'a>,
	///Signature data.
	///
	///This is the raw signature data, not including any BIT STRING wrapper.
	pub signature: &'a [u8]
}

impl<'a> SignatureBlock<'a>
{
	///Make signature block from PKIX Algorithm and signature data.
	///
	///Takes in:
	///
	/// * A PKIX algorithm `algo`, without the outer SEQUENCE header.
	/// * Raw signature data `sig`.
	///
	///This function takes the algorithm the data and makes a signature block out of them.
	pub fn from_x509(algo: &'a [u8], sig: &'a [u8]) -> SignatureBlock<'a>
	{
		SignatureBlock {
			algorithm: SignatureAlgorithm::X509(algo),
			signature: sig,
		}
	}
	///Make signature block from TLS SignatureScheme and signature data.
	///
	///Takes in:
	///
	/// * A TLS signature scheme `algo`.
	/// * Raw signature data `sig`.
	///
	///This function takes the algorithm the data and makes a signature block out of them.
	pub fn from_tls(algo: SignatureType, sig: &'a [u8]) -> SignatureBlock<'a>
	{
		SignatureBlock {
			algorithm: SignatureAlgorithm::Tls(algo),
			signature: sig,
		}
	}
	///Validate signature.
	///
	///Takes in:
	///
	/// * A signature block `self`.
	/// * A key `key`. The key is in SubjectPublicKeyInfo format.
	/// * A message `tbs`.
	/// * The signature condition flags `flags`.
	///
	///Validates the signature on the message using the key and the flags.
	///
	///If the validation is successful, returns `Ok(())`.
	///
	///If the validation fails, returns `Err(())`.
	pub fn verify(self, key: &[u8], tbs: &[u8], flags: u32) -> Result<(), ()>
	{
		validate_signature(key, tbs, self, flags)
	}
	///Convert a TLS DigitallySigned structure into signature block.
	///
	///Takes in:
	///
	/// * A TLS DigitallySigned structure `dsig`.
	///
	///This method takes the structure and converts it into a signature block.
	pub fn from3<DigitallySigned:FromDigitallySigned<'a>+'a>(dsig: DigitallySigned) -> SignatureBlock<'a>
	{
		SignatureBlock {
			algorithm: SignatureAlgorithm::Tls(dsig.get_algorithm()),
			signature: dsig.get_payload(),
		}
	}
}

///Trait: Cast to TLS DigitallySigned structure.
///
///This trait models a container for the TLS DigitallySigned structure.
pub trait FromDigitallySigned<'a>: Copy
{
	///Get the algorithm number.
	///
	///Takes in:
	///
	/// * A TLS DigitallySigned structure `self`.
	///
	///This method returns the TLS SignatureScheme the structure uses.
	fn get_algorithm(self) -> SignatureType;
	///Get the signature payload.
	///
	///Takes in:
	///
	/// * A TLS DigitallySigned structure `self`.
	///
	///This method returns the signature payload in the structure.
	fn get_payload(self) -> &'a [u8];
}
