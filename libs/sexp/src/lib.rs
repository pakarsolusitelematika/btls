//!Canonical S-Expression parser.
//!
//!This crate provodes a parser for canonical S-Expressions.
//!
//!Currently, display hints are not supported.
#![forbid(unsafe_code)]
#![forbid(missing_docs)]
#![no_std]
use core::str::from_utf8;
extern crate btls_aux_serialization;
use btls_aux_serialization::SlicingExt;
#[macro_use]
extern crate btls_aux_fail;

///A canonical S-Expression iterator.
///
///This structure acts as an iterator over a canonical S-Expression.
#[derive(Copy,Clone, Debug)]
pub struct SexprParseStream<'a>
{
	data: &'a [u8],
	ptr: usize,
	depth: usize,
	root_status: u8,
}

///A token in S-Expression.
///
///This enumeration contains one token in canonical S-Expression.
#[derive(Copy,Clone, PartialEq, Eq, Debug)]
pub enum SexprNode<'a>
{
	///Start of list token.
	StartList,
	///End of list token.
	EndList,
	///Raw octet string token.
	///
	///This is a raw octet string token, The argument is the raw token value.
	Data(&'a [u8]),
}

impl<'a> SexprParseStream<'a>
{
	fn next_token(&mut self) -> Result<SexprNode<'a>, bool>
	{
		fail_if!(self.ptr == self.data.len(), false);
		fail_if!(self.root_status == 2, true);
		let ntype = *self.data.get(self.ptr).ok_or(true)?;
		if ntype == 40 { //Start of list.
			if self.root_status == 0 { self.root_status = 1; }
			self.ptr = self.ptr.checked_add(1).ok_or(true)?;
			self.depth = self.depth.checked_add(1).ok_or(true)?;
			return Ok(SexprNode::StartList);
		} else if ntype == 41 { //End of list.
			if self.depth == 1 && self.root_status == 1 { self.root_status = 2; }
			self.ptr = self.ptr.checked_add(1).ok_or(true)?;
			self.depth = self.depth.checked_sub(1).ok_or(true)?;
			return Ok(SexprNode::EndList);
		} else if ntype >= 48 && ntype <= 57 { //Data.
			if self.depth == 0 { self.root_status = 2; }
			let mut len = 0usize;
			let mut nptr = self.ptr;
			loop {
				fail_if!(nptr == self.data.len(), true);
				let ch = *self.data.get(nptr).ok_or(true)?;
				if ch == 58 { break; }	//End on :
				fail_if!(ch < 48 || ch > 57, true);	//Only numbers and : valid.
				let dch = (ch - 48) as usize;
				len = len.checked_mul(10).ok_or(true)?.checked_add(dch).ok_or(true)?;
				nptr = nptr.checked_add(1).ok_or(true)?;
			}
			//Zero leading is only valid for '0'.
			fail_if!(ntype == 48 && nptr.saturating_sub(self.ptr) != 1, true);
			//Advance to start of data.
			nptr = nptr.checked_add(1).ok_or(true)?;
			let eptr = nptr.checked_add(len).ok_or(true)?;
			let data = self.data.slice_np(nptr..eptr).ok_or(true)?;
			self.ptr = eptr;
			return Ok(SexprNode::Data(data))
		} else {
			fail!(true);
		}
	}
}

impl<'a> Iterator for SexprParseStream<'a>
{
	type Item = Result<SexprNode<'a>, ()>;
	fn next(&mut self) -> Option<Result<SexprNode<'a>, ()>>
	{
		match self.next_token() {
			Ok(x) => Some(Ok(x)),
			Err(true) => Some(Err(())),
			Err(false) => None
		}
	}
}

impl<'a> SexprParseStream<'a>
{
	///Create a new S-Expression iterator.
	///
	///Takes in:
	///
	/// * A canonical S-Expression `data`.
	///
	///This method creates a new S-Expression iterator, iterating over the S-Expression.
	pub fn new(data: &'a [u8]) -> SexprParseStream<'a>
	{
		SexprParseStream {
			data: data,
			ptr: 0,
			depth: 0,
			root_status: 0
		}
	}
	///Read start of list, and the first item in the list as octet string.
	///
	///Takes in:
	///
	/// * An S-Expression parse stream.
	///
	///This method reads start of list token followed by octet string token from the stream.
	///
	///On success, returns `Ok(token)` where `token` is the octet string read.
	///
	///On failure, returns `Err(())`. At least the following factors cause the method to fail:
	///
	/// * The first token read is not start of list.
	/// * The second token read is not an octet string.
	pub fn read_typed(&mut self) -> Result<&'a [u8], ()>
	{
		fail_if!(self.next().unwrap_or(Err(()))? != SexprNode::StartList, ());
		let ltype = if let SexprNode::Data(x) = self.next().unwrap_or(Err(()))? { x } else { fail!(()); };
		Ok(ltype)
	}
	///Read start of list, and the first item in the list as text string.
	///
	///Takes in:
	///
	/// * An S-Expression parse stream.
	///
	///This method is the same as `.read_typed()`, except it transforms the read octet string into UTF-8 text
	///string.
	///
	///The following additional causes of failure exist compared to `.read_typed()`:
	///
	/// * The octet string is not valid UTF-8.
	pub fn read_typed_str(&mut self) -> Result<&'a str, ()>
	{
		from_utf8(self.read_typed()?).map_err(|_|())
	}
	///Read a raw octet string.
	///
	///Takes in:
	///
	/// * An S-Expression parse stream.
	///
	///This method reads the next token from the stream as octet string.
	///
	///On success, returns `Ok(token)`, where `token` is the octet string read.
	///
	///On failure, returns `Err(())`. Causes of failure include:
	///
	/// * The read token is not octet string.
	pub fn next_data(&mut self) -> Result<&'a [u8], ()>
	{
		match self.next().unwrap_or(Err(())) { Ok(SexprNode::Data(x)) => Ok(x), _ => fail!(()) }
	}
	///Read end of list.
	///
	///Takes in:
	///
	/// * An S-Expression parse stream.
	///
	///This method reads the next token from the stream and checks if it is end of list.
	///
	///If the next token is end of list, returns `Ok(())`.
	///
	///If the next token is not end of list, returns `Err(())`.
	pub fn assert_eol(&mut self) -> Result<(), ()>
	{
		match self.next().unwrap_or(Err(())) { Ok(SexprNode::EndList) => Ok(()), _ => fail!(()) }
	}
	///Read a text string.
	///
	///Takes in:
	///
	/// * An S-Expression parse stream.
	///
	///This is the same as `.next_data()`, but interprets the read string as UTF-8 text string.
	///
	///The following additional causes of failure exist compared to `.next_data()`:
	///
	/// * The octet string is not valid UTF-8.
	pub fn next_str(&mut self) -> Result<&'a str, ()>
	{
		from_utf8(self.next_data()?).map_err(|_|())
	}
	///Read a list containing a pair of raw octet strings.
	///
	///Takes in:
	///
	/// * An S-Expression parse stream.
	///
	///This method reads four tokens from the stream. The first is start of list, the two middle ones
	///are octet strings and the last one is end of list.
	///
	///On success, returns `Ok(str1, str2)`, where `str1` is the first octet string read, and `str2` is the
	///second octet string read.
	///
	///On failure, returns `Err(())`. Causes of failure include:
	///
	/// * The first token is not a start of list.
	/// * The second token is not an octet string.
	/// * The third token is not an octet string.
	/// * The fourth token is not an end of list.
	pub fn read_pair(&mut self) -> Result<(&'a [u8], &'a [u8]), ()>
	{
		if self.next().unwrap_or(Err(()))? != SexprNode::StartList { fail!(()); }
		let first = if let SexprNode::Data(x) = self.next().unwrap_or(Err(()))? { x } else { fail!(()); };
		let second = if let SexprNode::Data(x) = self.next().unwrap_or(Err(()))? { x } else { fail!(()); };
		if self.next().unwrap_or(Err(()))? != SexprNode::EndList { fail!(()); }
		Ok((first, second))
	}
	///Read a list containing a pair of text and raw octet strings.
	///
	///Takes in:
	///
	/// * An S-Expression parse stream.
	///
	///This method is the same as `.read_pair()`, but instead of returning the first string as octet string,
	///the method returns the string as UTF-8 text.
	///
	///The following additional causes of failure exist compared to `.read_pair()`:
	///
	/// * The first octet string is not valid UTF-8.
	pub fn read_pair_strn(&mut self) -> Result<(&'a str, &'a [u8]), ()>
	{
		self.read_pair().and_then(|(x,y)|Ok((from_utf8(x).map_err(|_|())?, y)))
	}
	///Read a list containing a pair of text  strings.
	///
	///Takes in:
	///
	/// * An S-Expression parse stream.
	///
	///This method is the same as `.read_pair()`, but instead of returning the strings as octet strings,
	///the method returns the strings as UTF-8 text.
	///
	///The following additional causes of failure exist compared to `.read_pair()`:
	///
	/// * Either octet string is not valid UTF-8.
	pub fn read_pair_str(&mut self) -> Result<(&'a str, &'a str), ()>
	{
		self.read_pair().and_then(|(x,y)|Ok((from_utf8(x).map_err(|_|())?, from_utf8(y).map_err(|_|())?)))
	}
}

#[test]
fn test_read_typed()
{
	let input = "(3:foo4:quux(6:barqux3:zot))";
	let mut strm = SexprParseStream::new(input.as_bytes());
	assert_eq!(strm.read_typed(), Ok(&b"foo"[..]));
	assert_eq!(strm.next_data(), Ok(&b"quux"[..]));
	assert_eq!(strm.read_typed(), Ok(&b"barqux"[..]));
	assert_eq!(strm.next_data(), Ok(&b"zot"[..]));
	strm.assert_eol().unwrap();
	strm.assert_eol().unwrap();
	assert_eq!(strm.next(), None);
}

#[test]
fn test_read_typed_str()
{
	let input = "(3:foo4:quux(6:barqux3:zot))";
	let mut strm = SexprParseStream::new(input.as_bytes());
	assert_eq!(strm.read_typed_str(), Ok("foo"));
	assert_eq!(strm.next_str(), Ok("quux"));
	assert_eq!(strm.read_typed_str(), Ok("barqux"));
	assert_eq!(strm.next_str(), Ok("zot"));
	strm.assert_eol().unwrap();
	strm.assert_eol().unwrap();
	assert_eq!(strm.next(), None);
}

#[test]
fn test_read_pair()
{
	let input = "((3:foo6:barqux)(4:quux3:zot))";
	let mut strm = SexprParseStream::new(input.as_bytes());
	assert_eq!(strm.next(), Some(Ok(SexprNode::StartList)));
	assert_eq!(strm.read_pair(), Ok((&b"foo"[..], &b"barqux"[..])));
	assert_eq!(strm.read_pair(), Ok((&b"quux"[..], &b"zot"[..])));
	assert_eq!(strm.next(), Some(Ok(SexprNode::EndList)));
	assert_eq!(strm.next(), None);
}

#[test]
fn test_read_pair_strn()
{
	let input = "((3:foo6:barqux)(4:quux3:zot))";
	let mut strm = SexprParseStream::new(input.as_bytes());
	assert_eq!(strm.next(), Some(Ok(SexprNode::StartList)));
	assert_eq!(strm.read_pair_strn(), Ok(("foo", &b"barqux"[..])));
	assert_eq!(strm.read_pair_strn(), Ok(("quux", &b"zot"[..])));
	assert_eq!(strm.next(), Some(Ok(SexprNode::EndList)));
	assert_eq!(strm.next(), None);
}

#[test]
fn test_read_pair_str()
{
	let input = "((3:foo6:barqux)(4:quux3:zot))";
	let mut strm = SexprParseStream::new(input.as_bytes());
	assert_eq!(strm.next(), Some(Ok(SexprNode::StartList)));
	assert_eq!(strm.read_pair_str(), Ok(("foo", "barqux")));
	assert_eq!(strm.read_pair_str(), Ok(("quux", "zot")));
	assert_eq!(strm.next(), Some(Ok(SexprNode::EndList)));
	assert_eq!(strm.next(), None);
}

#[test]
fn test_read_pair_1elem()
{
	let input = "((3:foo))";
	let mut strm = SexprParseStream::new(input.as_bytes());
	assert_eq!(strm.next(), Some(Ok(SexprNode::StartList)));
	assert_eq!(strm.read_pair(), Err(()));
}

#[test]
fn test_read_pair_2elem()
{
	let input = "((3:foo6:barqux))";
	let mut strm = SexprParseStream::new(input.as_bytes());
	assert_eq!(strm.next(), Some(Ok(SexprNode::StartList)));
	assert_eq!(strm.read_pair(), Ok((&b"foo"[..], &b"barqux"[..])));
}

#[test]
fn test_read_pair_3elem()
{
	let input = "((3:foo6:barqux3:zot))";
	let mut strm = SexprParseStream::new(input.as_bytes());
	assert_eq!(strm.next(), Some(Ok(SexprNode::StartList)));
	assert_eq!(strm.read_pair(), Err(()));
}

#[test]
fn test_read_pair_overlen1()
{
	let input = "((32:foo6:barqux))";
	let mut strm = SexprParseStream::new(input.as_bytes());
	assert_eq!(strm.next(), Some(Ok(SexprNode::StartList)));
	assert_eq!(strm.read_pair(), Err(()));
}


#[test]
fn test_read_pair_overlen2()
{
	let input = "((3:foo66:barqux))";
	let mut strm = SexprParseStream::new(input.as_bytes());
	assert_eq!(strm.next(), Some(Ok(SexprNode::StartList)));
	assert_eq!(strm.read_pair(), Err(()));
}
