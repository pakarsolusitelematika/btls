//!Various routines related to hashes and especially TLS PRF hashes.
//!
//!This crate contains implementations of various hashes and their applications to TLS PRF.
#![forbid(unsafe_code)]
#![forbid(missing_docs)]

use std::cmp::min;
use std::convert::AsRef;
use std::fmt::{Debug, Display, Error as FmtError, Formatter};
use std::ops::DerefMut;
#[macro_use]
extern crate btls_aux_fail;
extern crate btls_aux_nettle;
extern crate btls_aux_securebuf;
extern crate sha3;
extern crate digest;
use btls_aux_securebuf::{SecStackBuffer, wipe_buffer};

macro_rules! make_bytes_wrapper
{
	($name:ident, $maxsize:expr) => {
		impl Clone for $name {
			fn clone(&self) -> Self
			{
				$name(self.0, self.1)
			}
		}
		impl Drop for $name { fn drop(&mut self) { wipe_buffer(&mut self.0); } }
		impl $name
		{
			///Convert a octet slice into type.
			///
			///Takes in:
			///
			/// A slice `y`. Must be within size limit of the type.
			///
			///This method constructs an object of the type, initializing it with the slice.
			///
			///On success, returns `Ok(obj)`, where `obj` is the newly constructed object.
			///
			///If the slice is larger than the maximum size, returns `Err((size, limit))`, where
			///`size` is the size of the slice and `limit` is the maximum number of octets
			///allowed.
			pub fn from2(y: &[u8]) -> Result<$name, (usize, usize)>
			{
				fail_if!(y.len() > $maxsize, (y.len(), $maxsize));
				let mut x = $name([0;$maxsize], y.len());
				(&mut x.0[..y.len()]).copy_from_slice(y);
				Ok(x)
			}
		}
		impl Debug for $name
		{
			fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
			{
				if self.1 > self.0.len() {
					fmt.write_fmt(format_args!("<Invalid data: {:?}, length={}>", &self.0[..],
						self.1))
				} else {
					fmt.write_fmt(format_args!("{:?}", &self.0[..self.1]))
				}
			}
		}
		impl AsRef<[u8]> for $name
		{
			fn as_ref(&self) -> &[u8]
			{
				//We outright assume length is valid.
				&self.0[..self.1]
			}
		}
	}
}

///Error in hash function.
///
///This enumeration describes the error that occured in calculating a hash function.
#[derive(Clone,Debug,PartialEq,Eq)]
pub enum HashFunctionError
{
	///The output length is wrong.
	///
	///This error happens if the reported output length of hash function is wrong. This is a bug in hash
	///function implementation. The arguments in order are:
	///
	/// * The name of hash function.
	/// * The actual size of hash output.
	/// * The reported size of hash output.
	OutputLengthWrong(&'static str, usize, usize),
	///The output length is too big
	///
	///This error happens if the output length of hash function is too big (that is, larger than
	//`MAX_HASH_OUTPUT`). This is a bug in hash function implementation. The arguments in order are:
	///
	/// * The name of hash function.
	/// * The actual size of hash output.
	/// * The reported size of hash output.
	OutputLengthTooBig(&'static str, usize, usize),
	///The input block length is too big
	///
	///This error happens if the input block length of hash function is too big (that is, larger than
	//`MAX_HASH_INPUT`). This is a bug in hash function implementation. The arguments in order are:
	///
	/// * The name of hash function.
	/// * The actual size of hash output.
	/// * The reported size of hash output.
	InputLengthTooBig(&'static str, usize, usize),
	///HMAC is undefined for this hash function.
	///
	///This error happens if one tries to use HMAC or related algorithms (e.g., HKDF) on hash function where
	///output size is bigger than input block size. HMAC is undefined for such hash functions. The arguments in
	///order are:
	///
	/// * The name of hash function.
	/// * The input block size.
	/// * The output size.
	HmacUndefined(&'static str, usize, usize),
	///Wrong derive-secret context length specified.
	///
	///This error happens if one tries to use derive-secret with wrong context argument length. The arguments in
	///order are:
	///
	/// * The name of hash function.
	/// * The size of context passed.
	/// * The size of context expected.
	BadContextLength(&'static str, usize, usize),
	///Dummy hash function.
	///
	///This error happens if one tries to use a dummy hash function. Such hash functions are meant to be
	///placeholders, not to be calculated.
	DummyHash,
	///Unsupported hash function.
	///
	///This error happens if one tries to use a hash function that is not actually implemented.
	UnsupportedHash,
}

impl Display for HashFunctionError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::HashFunctionError::*;
		match self {
			&OutputLengthWrong(name, x, y) => fmt.write_fmt(format_args!("Hash output length wrong \
				({}, got {}) for {}", y, x, name)),
			&OutputLengthTooBig(name, x, y) => fmt.write_fmt(format_args!("Hash output length too big \
				({}, limit {}) for {}", x, y, name)),
			&InputLengthTooBig(name, x, y) => fmt.write_fmt(format_args!("Hash input length too big \
				({}, limit {}) for {}", x, y, name)),
			&HmacUndefined(name, x, y) => fmt.write_fmt(format_args!("HMAC undefined for hash \
				(input size {}, output size {}) for {}", x, y, name)),
			&BadContextLength(name, x, y) => fmt.write_fmt(format_args!("Bad derive-secret context \
				length ({}, expected {}) for {}", x, y, name)),
			&DummyHash => fmt.write_str("Invalid operation for dummy hash"),
			&UnsupportedHash => fmt.write_str("Unsupported hash function"),
		}
	}
}

///A TLS PRF hash function.
///
///This enumeration describes a hash function that can act as TLS PRF (Pseudo Random Function) hash 
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub enum HashFunction
{
	///SHA-256.
	///
	///This is the `SHA-2` hash function with 256 bit output.
	Sha256,
	///SHA-384
	///
	///This is the `SHA-2` hash function with 384 bit output.
	Sha384,
	///SHA-512
	///
	///This is the `SHA-2` hash function with 512 bit output.
	Sha512,
	///SHA3-256
	///
	///This is the `SHA-3` hash function with 256 bit output.
	Sha3256,
	///SHA3-384
	///
	///This is the `SHA-3` hash function with 384 bit output.
	Sha3384,
	///SHA3-512.
	///
	///This is the `SHA-3` hash function with 512 bit output.
	///
	///Note: This hash function is slow to compute.
	Sha3512,
}

///A calculation context for hash function.
///
///This trait is interface to low level hash function computation. It is object-safe, so objects of structures
///implementing this trait can be boxed as objects of this trait.
pub trait HashFunctionContext
{
	///Get the input block length of the hash.
	///
	///Takes in:
	///
	/// * A hash computation context `self`.
	///
	///This method returns the input block size of the hash in bytes. This length is a property of the hash, and
	///is used in calculating HMAC (specifically, HMAC pads the key to input block size).
	///
	///The value returned is expected to be at most `MAX_HASH_INPUT`.
	fn input_length(&self) -> usize { self.function().input_length() }
	///Get the output length of the hash.
	///
	///Takes in:
	///
	/// * A hash computation context `self`.
	///
	///This method returns the output length of the hash in bytes.
	///
	///The value returned is expected to be at most `MAX_HASH_OUTPUT` and to always match the length returned
	///by the `.output()` method.
	fn output_length(&self) -> usize { self.function().output_length() }
	///Reset the hash to its initial state.
	///
	///Takes in:
	///
	/// * A hash computation context `self`.
	///
	///This method resets the context to its initial state, that is, no data added to computation.
	fn reset(&mut self);
	///Add data to be hashed.
	///
	///Takes in:
	///
	/// * A hash computation context `self`.
	/// * A piece of data `data`.
	///
	///This method appends the data to the data to be hashed in the context. This can be called multiple times.
	fn input(&mut self, data: &[u8]);
	///Get the hash for currently added data.
	///
	///Takes in:
	///
	/// * A hash computation context `self`.
	///
	///This method obtains the hash of currently added data in the context, without modifying it.
	///
	///On success, returns `Ok(hashval)`, where `hashval` is the hash value.
	///
	///On failure, returns `Err(error)`, where `err` describes the error. All errors from this method can be
	///treated as internal errors, this method should never actually fail.
	fn output(&self) -> Result<HashOutput, HashFunctionError>;
	///Get the hash function this context is for.
	///
	///Takes in:
	///
	/// * A hash computation context `self`.
	///
	///This method returns the hash function described by the context.
	fn function(&self) -> HashFunction;
	///Clone the hash state (forking).
	///
	///Takes in:
	///
	/// * A hash computation context `self`.
	///
	///This method clones the context, and returns a boxed copy. After cloning, the hash contexts work
	///independently (e.g., resetting one does not reset the other).
	fn clone_state(&self) -> Box<HashFunctionContext+Send>;
	///Add scatter-gather data to be hashed.
	///
	///Takes in:
	///
	/// * A hash computation context `self`.
	/// * An array of pieces of data `data`.
	///
	///This method appends all slices the array in order to the data to be hashed in the context. This can be
	///called multiple times.
	///
	///This is equivalent to calling `.input()` on all the slices in the array, but using this method avoids
	///virtual dispatch overhead for all the slices.
	fn input_sg(&mut self, data: &[&[u8]])
	{
		//By default, just do input calls for each fragment.
		for i in data.iter() { self.input(i); }
	}
}

trait HashFunctionContextExtended: HashFunctionContext+Sized
{
	fn tls13_derive_secret(&mut self, key: &[u8], label: (&str, bool), hash: &HashOutput) ->
		Result<HashOutput, HashFunctionError>
	{
		let func = self.function();
		let sname = func.name();
		let outlen = func.output_length();
		let context = hash.as_ref();
		assert_eq!(context.len(), outlen);
		fail_if!(context.len() != outlen, HashFunctionError::BadContextLength(sname, context.len(),
			outlen));
		//Values very close to usize limit don't appear as buffer sizes.
		let lprefix = if label.1 { "tls13 " } else { "TLS 1.3, " };
		let extlabel = label.0.len() + lprefix.len();
		let xlen = &[(outlen >> 8) as u8, outlen as u8];
		let labellen = &[extlabel as u8];
		let contextlen = &[context.len() as u8];
		let trailer = &[0x01];
		self.hmac(key, |x| {
			x.input(xlen);
			x.input(labellen);
			x.input(lprefix.as_bytes());
			x.input(label.0.as_bytes());
			x.input(contextlen);
			x.input(context);
			x.input(trailer);
		})
	}
	//Limit label to 246 bytes, context to 255 and output to 65535 bytes or 255 output blocks, whichever is less.
	//The second component of label is omit-prefix flag.
	fn tls13_hkdf_expand_label(&mut self, key: &[u8], label: (&str, bool), context: &[u8], data: &mut [u8]) ->
		Result<(), HashFunctionError>
	{
		//Values very close to usize limit don't appear as buffer sizes.
		let lprefix = if label.1 { "tls13 " } else { "TLS 1.3, " };
		let extlabel = label.0.len() + lprefix.len();
		let xlen = &[(data.len() >> 8) as u8, data.len() as u8];
		let labellen = &[extlabel as u8];
		let contextlen = &[context.len() as u8];
		self.hkdf_expand(key, |x| {
			x.input(xlen);
			x.input(labellen);
			x.input(lprefix.as_bytes());
			x.input(label.0.as_bytes());
			x.input(contextlen);
			x.input(context);
		}, data)
	}
	//One shot hashing of data.
	fn oneshot<F>(&mut self, data_cb: F) -> Result<HashOutput, HashFunctionError>
		where F: Fn(&mut HashInput) -> ()
	{
		{
			let mut tmp = HashInput(self);
			data_cb(&mut tmp);
		}
		self.output()
	}
	fn hmac<F>(&mut self, key: &[u8], data_cb: F) -> Result<HashOutput, HashFunctionError>
		where F: Fn(&mut HashInput) -> ()
	{
		let func = self.function();
		let sname = func.name();
		self.reset();
		let inlen = func.input_length();
		let outlen = func.output_length();
		let mut expkey = [0; MAX_HASH_INPUT];
		fail_if!(inlen > expkey.len(), HashFunctionError::InputLengthTooBig(sname, inlen, expkey.len()));
		let mut expkey = SecStackBuffer::new(&mut expkey, inlen).unwrap();	//Can't fail.
		if key.len() <= expkey.len() {
			let _expkey: &mut [u8] = expkey.deref_mut();
			(&mut _expkey[..key.len()]).copy_from_slice(key);
		} else {
			self.input(key);
			let tmpout = self.output()?;
			let tmpout = tmpout.as_ref();
			fail_if!(expkey.len() < tmpout.len(), HashFunctionError::HmacUndefined(sname, expkey.len(),
				outlen));
			fail_if!(tmpout.len() != outlen, HashFunctionError::OutputLengthWrong(sname, tmpout.len(),
				outlen));
			(&mut expkey[..outlen]).copy_from_slice(tmpout);
			self.reset();
		}
		for i in expkey.deref_mut().iter_mut() { *i ^= 0x36; }
		self.input(&mut expkey);
		{
			let mut tmp = HashInput(self);
			data_cb(&mut tmp);
		}
		let interm = self.output()?;
		self.reset();
		for i in expkey.deref_mut().iter_mut() { *i ^= 0x6A; }
		self.input(&mut expkey);
		self.input(interm.as_ref());
		self.output()
	}
	fn tls_prf<F>(&mut self, key: &[u8], label: &str, data_cb: F, data: &mut [u8]) ->
		Result<(), HashFunctionError> where F: Fn(&mut HashInput) -> ()
	{
		let mut a = self.hmac(key, |x| {
			x.input(label.as_bytes());
			data_cb(x);
		})?;
		let mut ctr = 0;
		while ctr < data.len() {
			let b = self.hmac(key, |x| {
				x.input(a.as_ref());
				x.input(label.as_bytes());
				data_cb(x);
			})?;
			a = self.hmac(key, |x| { x.input(a.as_ref()); })?;
			let _b = b.as_ref();
			//By loop condition above, ctr < data.len().
			let tocopy = min(data.len() - ctr, _b.len());
			//The target fits because tocopy <= data.len() - ctr, and so tocopy + ctr <= data.len().
			//The soruce fits because tocopy <= _b.len().
			(&mut data[ctr..][..tocopy]).copy_from_slice(&_b[..tocopy]);
			//Now, tocopy + ctr is <= data.len() and fits into usize.
			ctr += tocopy;
		}
		Ok(())
	}
	//Don't request over 255 blocks of output! And check that key is at least one output block.
	fn hkdf_expand<F>(&mut self, key: &[u8], data_cb: F, data: &mut [u8]) -> Result<(), HashFunctionError>
		where F: Fn(&mut HashInput) -> ()
	{
		let mut ctr = 0;
		let mut b = HashOutput([0;MAX_HASH_OUTPUT],0);
		let mut c = [0u8];
		while ctr < data.len() {
			c[0] += 1;	//
			b = self.hmac(key, |x| {
				x.input(b.as_ref());
				data_cb(x);
				x.input(&c);
			})?;
			let _b = b.as_ref();
			//By loop condition above, ctr < data.len().
			let tocopy = min(data.len() - ctr, _b.len());
			//The target fits because tocopy <= data.len() - ctr, and so tocopy + ctr <= data.len().
			//The soruce fits because tocopy <= _b.len().
			(&mut data[ctr..][..tocopy]).copy_from_slice(&_b[..tocopy]);
			//Now, tocopy + ctr is <= data.len() and fits into usize.
			ctr += tocopy;
		}
		Ok(())
	}
}

///The maximum size of hash input block.
///
///This constant is the maximum size of hash function input block in bytes.
pub const MAX_HASH_INPUT: usize = 136;	//SHA3-256 has 136-byte inputs, all others are less.
///The maximum size of hash output.
///
///This constant is the maximum size of hash function output in bytes.
pub const MAX_HASH_OUTPUT: usize = 64;	//Assume hashes are below 512 bits.

///A hash output
///
///This structure represents the output from hash function computation. The structure acts as owned buffer with
///fixed maximum size (`MAX_HASH_OUTPUT`).
pub struct HashOutput([u8; MAX_HASH_OUTPUT], usize);
make_bytes_wrapper!(HashOutput, MAX_HASH_OUTPUT);
///A buffer storing one hash input block
///
///This structure represents the input block of hash function computation. The structure acts as owned buffer with
///fixed maximum size (`MAX_HASH_INPUT`).
///
///This structure is useful if compressing HMAC keys: One can show that any value acting as HMAC key can be stored
///as one hash input block, regardless of how large the value actually is.
pub struct HashInputBlock([u8; MAX_HASH_INPUT], usize);
make_bytes_wrapper!(HashInputBlock, MAX_HASH_INPUT);


impl HashFunction
{
	fn sha256() -> nettlef::Sha256 { nettlef::Sha256::new() }
	fn sha384() -> nettlef::Sha384 { nettlef::Sha384::new() }
	fn sha512() -> nettlef::Sha512 { nettlef::Sha512::new() }
	fn sha3_256() -> nettlef::Sha3256 { nettlef::Sha3256::new() }
	fn sha3_384() -> nettlef::Sha3384 { nettlef::Sha3384::new() }
	fn sha3_512() -> nettlef::Sha3512 { nettlef::Sha3512::new() }
	///Get the input block size of the hash function.
	///
	///Takes in:
	///
	/// * A hash function `self`.
	///
	///This method returns the input block size of the hash in bytes.
	///
	///This is used by HMAC, for padding the key to the input block size.
	pub fn input_length(&self) -> usize
	{
		match self {
			&HashFunction::Sha256 => 64,
			&HashFunction::Sha384 => 128,
			&HashFunction::Sha512 => 128,
			&HashFunction::Sha3256 => 136,
			&HashFunction::Sha3384 => 104,
			&HashFunction::Sha3512 => 72,
		}
	}
	///Get the output size of the hash function.
	///
	///Takes in:
	///
	/// * A hash function `self`.
	///
	///This method returns the output size of the hash in bytes.
	pub fn output_length(&self) -> usize
	{
		match self {
			&HashFunction::Sha256 => 32,
			&HashFunction::Sha384 => 48,
			&HashFunction::Sha512 => 64,
			&HashFunction::Sha3256 => 32,
			&HashFunction::Sha3384 => 48,
			&HashFunction::Sha3512 => 64,
		}
	}
	///Get the name of the hash.
	///
	///Takes in:
	///
	/// * A hash function `self`.
	///
	///This method returns the name of the hash.
	pub fn name(&self) -> &'static str
	{
		match self {
			&HashFunction::Sha256 => "SHA-256(PRF)",
			&HashFunction::Sha384 => "SHA-384(PRF)",
			&HashFunction::Sha512 => "SHA-512(PRF)",
			&HashFunction::Sha3256 => "SHA3-256(PRF)",
			&HashFunction::Sha3384 => "SHA3-384(PRF)",
			&HashFunction::Sha3512 => "SHA3-512(PRF)",
		}
	}
	///Get the name of the hash (for human-readable purposes).
	///
	///Takes in:
	///
	/// * A hash function `self`.
	///
	///This method returns human-readable name of the hash.
	pub fn as_string(&self) -> &'static str
	{
		match self {
			&HashFunction::Sha256 => "SHA-256",
			&HashFunction::Sha384 => "SHA-384",
			&HashFunction::Sha512 => "SHA-512",
			&HashFunction::Sha3256 => "SHA3-256",
			&HashFunction::Sha3384 => "SHA3-384",
			&HashFunction::Sha3512 => "SHA3-512",
		}
	}
	///Make a context for this hash.
	///
	///Takes in:
	///
	/// * A hash function `self`.
	///
	///This method returns a new hash computation context for the hash. The context is wrapped in a box.
	pub fn make_context(&self) -> Box<HashFunctionContext+Send>
	{
		match self {
			&HashFunction::Sha256 => Box::new(Self::sha256()),
			&HashFunction::Sha384 => Box::new(Self::sha384()),
			&HashFunction::Sha512 => Box::new(Self::sha512()),
			&HashFunction::Sha3256 => Box::new(Self::sha3_256()),
			&HashFunction::Sha3384 => Box::new(Self::sha3_384()),
			&HashFunction::Sha3512 => Box::new(Self::sha3_512()),
		}
	}
	///Compute HKDF-Extract.
	///
	///Takes in:
	///
	/// * A hash function `self`.
	/// * A key `key`. Must be at least one hash output in size.
	/// * A salt `salt`.
	///
	///This method computes HKDF-Extract using key, the hash and the salt.
	///
	///On success, returns `Ok(prk)`, where `prk` is the extracted key. The length of extracted key is always
	///equal to the hash output.
	///
	///On failure, returns `Err(error)`, where `error` describes the error.
	pub fn hkdf_extract(&self, key: &[u8], salt: &[u8]) -> Result<HashOutput, HashFunctionError>
	{
		match self {
			&HashFunction::Sha256 => Self::sha256().hmac(salt, |x|x.input(key)),
			&HashFunction::Sha384 => Self::sha384().hmac(salt, |x|x.input(key)),
			&HashFunction::Sha512 => Self::sha512().hmac(salt, |x|x.input(key)),
			&HashFunction::Sha3256 => Self::sha3_256().hmac(salt, |x|x.input(key)),
			&HashFunction::Sha3384 => Self::sha3_384().hmac(salt, |x|x.input(key)),
			&HashFunction::Sha3512 => Self::sha3_512().hmac(salt, |x|x.input(key)),
		}
	}
	///Compute TLS 1.3 derive-secret.
	///
	///Takes in:
	///
	/// * A hash function `self`.
	/// * A key `key`. Must be exactly one hash output in size.
	/// * A label `label`. The first component can be at most 246 bytes in size.
	/// * A hash value `hash`. Must be exactly one hash output in size.
	///
	///This method computes the TLS 1.3 derive-secret function using the key, the hash, the label and the value.
	///
	///If the second component of the label is `true`, the first component is prefixed by `tls13 ` instead of
	///`TLS 1.3, ` before being used as the actual label.
	///
	///On success, returns `Ok(newkey)`, where `newkey` is the derived key. The length of extracted key is always
	///equal to the hash output.
	///
	///On failure, returns `Err(error)`, where `error` describes the error.
	pub fn tls13_derive_secret(&self, key: &[u8], label: (&str, bool), hash: &HashOutput) ->
		Result<HashOutput, HashFunctionError>
	{
		match self {
			&HashFunction::Sha256 => Self::sha256().tls13_derive_secret(key, label, hash),
			&HashFunction::Sha384 => Self::sha384().tls13_derive_secret(key, label, hash),
			&HashFunction::Sha512 => Self::sha512().tls13_derive_secret(key, label, hash),
			&HashFunction::Sha3256 => Self::sha3_256().tls13_derive_secret(key, label, hash),
			&HashFunction::Sha3384 => Self::sha3_384().tls13_derive_secret(key, label, hash),
			&HashFunction::Sha3512 => Self::sha3_512().tls13_derive_secret(key, label, hash),
		}
	}
	///Compute TLS 1.3 hkdf-expand-label.
	///
	///Takes in:
	///
	/// * A hash function `self`.
	/// * A key `key`. Must be exactly one hash output in size.
	/// * A label `label`. The first component can be at most 246 bytes in size.
	/// * A context value `ctx`. Must be at most 255 bytes.
	/// * An output buffer `data`. Must be at most 255 times the hash output length.
	///
	///This method computes the TLS 1.3 hkdf-expand-label function using the key, the hash, the label and the
	///context. The result is stored in the result buffer, filling it completely.
	///
	///If the second component of the label is `true`, the first component is prefixed by `tls13 ` instead of
	///`TLS 1.3, ` before being used as the actual label.
	///
	///On success, returns `Ok(())`.
	///
	///On failure, returns `Err(error)`, where `error` describes the error.
	pub fn tls13_hkdf_expand_label(&self, key: &[u8], label: (&str, bool), ctx: &[u8], data: &mut [u8]) ->
		Result<(), HashFunctionError>
	{
		match self {
			&HashFunction::Sha256 => Self::sha256().tls13_hkdf_expand_label(key, label, ctx, data),
			&HashFunction::Sha384 => Self::sha384().tls13_hkdf_expand_label(key, label, ctx, data),
			&HashFunction::Sha512 => Self::sha512().tls13_hkdf_expand_label(key, label, ctx, data),
			&HashFunction::Sha3256 => Self::sha3_256().tls13_hkdf_expand_label(key, label, ctx, data),
			&HashFunction::Sha3384 => Self::sha3_384().tls13_hkdf_expand_label(key, label, ctx, data),
			&HashFunction::Sha3512 => Self::sha3_512().tls13_hkdf_expand_label(key, label, ctx, data),
		}
	}
	///Hash the data emitted by callback in one shot.
	///
	///Takes in:
	///
	/// * A hash function `self`.
	/// * A callback `data_cb`. The callback has a single `&mut` [`HashInput`] parameter.
	///
	///This method calls the callback and hashes the concatenation of all data chunks emitted (using the
	///`.input()` method of the passed [`HashInput`] object) using the hash.
	///
	///On success, returns `Ok(hashval)`, where `hashval` is the hash value.
	///
	///On failure, returns `Err(error)`, where `err` describes the error. All errors from this method can be
	///treated as internal errors, this method should never actually fail.
	///
	///[`HashInput`]: struct.HashInput.html
	pub fn oneshot<F>(&self, data_cb: F) -> Result<HashOutput, HashFunctionError>
		where F: Fn(&mut HashInput) -> ()
	{
		match self {
			&HashFunction::Sha256 => Self::sha256().oneshot(data_cb),
			&HashFunction::Sha384 => Self::sha384().oneshot(data_cb),
			&HashFunction::Sha512 => Self::sha512().oneshot(data_cb),
			&HashFunction::Sha3256 => Self::sha3_256().oneshot(data_cb),
			&HashFunction::Sha3384 => Self::sha3_384().oneshot(data_cb),
			&HashFunction::Sha3512 => Self::sha3_512().oneshot(data_cb),
		}
	}
	///Compute HMAC of data emitted by callback.
	///
	///Takes in:
	///
	/// * A hash function `self`.
	/// * A key `key`.
	/// * A callback `data_cb`. The callback has a single `&mut` [`HashInput`] parameter.
	///
	///This method calls `data_cb` and HMACs the concatenation of all data chunks emitted (using the `.input()`
	///method of the passed [`HashInput`] object) using the hash  and the key.
	///
	///On success, returns `Ok(hashval)`, where `hashval` is the hash value.
	///
	///On failure, returns `Err(error)`, where `err` describes the error.
	///
	///[`HashInput`]: struct.HashInput.html
	pub fn hmac<F>(&self, key: &[u8], data_cb: F) -> Result<HashOutput, HashFunctionError>
		where F: Fn(&mut HashInput) -> ()
	{
		match self {
			&HashFunction::Sha256 => Self::sha256().hmac(key, data_cb),
			&HashFunction::Sha384 => Self::sha384().hmac(key, data_cb),
			&HashFunction::Sha512 => Self::sha512().hmac(key, data_cb),
			&HashFunction::Sha3256 => Self::sha3_256().hmac(key, data_cb),
			&HashFunction::Sha3384 => Self::sha3_384().hmac(key, data_cb),
			&HashFunction::Sha3512 => Self::sha3_512().hmac(key, data_cb),
		}
	}
	///Calculate TLS 1.2 TLS-PRF.
	///
	///Takes in:
	///
	/// * A hash function `self`.
	/// * A key `key`.
	/// * A label `label`.
	/// * A callback `data_cb`. The callback has a single `&mut` [`HashInput`] parameter.
	/// * An output buffer `data`.
	///
	///This method calculates the TLS 1.2 TLS-PRF, using the key, the hash, the label, and context being the
	///concatenation of all data chunks emitted by the callback (using the `.input()` method of the passed
	///[`HashInput`] object).
	///
	///On success, returns `Ok(())`.
	///
	///On failure, returns `Err(error)`, where `error` describes the error.
	///
	///Note that this PRF is not collision-resistant: Trivial collisions happen if one label is prefix of
	///another. Also note that shorter outputs are prefixes of longer outputs with the same label and context.
	///
	///[`HashInput`]: struct.HashInput.html
	pub fn tls_prf<F>(&self, key: &[u8], label: &str, data_cb: F, data: &mut [u8]) ->
		Result<(), HashFunctionError> where F: Fn(&mut HashInput) -> ()
	{
		match self {
			&HashFunction::Sha256 => Self::sha256().tls_prf(key, label, data_cb, data),
			&HashFunction::Sha384 => Self::sha384().tls_prf(key, label, data_cb, data),
			&HashFunction::Sha512 => Self::sha512().tls_prf(key, label, data_cb, data),
			&HashFunction::Sha3256 => Self::sha3_256().tls_prf(key, label, data_cb, data),
			&HashFunction::Sha3384 => Self::sha3_384().tls_prf(key, label, data_cb, data),
			&HashFunction::Sha3512 => Self::sha3_512().tls_prf(key, label, data_cb, data),
		}
	}
	///Compute the HKDF-Expand function.
	///
	///Takes in:
	///
	/// * A hash function `self`.
	/// * A key `key`. The key must be exactly one hash function output in size.
	/// * A callback `data_cb`. The callback has a single `&mut` [`HashInput`] parameter.
	/// * An output buffer `data`. Must be at most 255 times the hash output length.
	///
	///This method computes the HKDF-Expand function using the key, the hash and info obtained by
	///concatenatenating all data chunks emitted by callback `data_cb` (using the `.input()` method of the
	///passed [`HashInput`] object). The output is written to the output buffer, filling it completely.
	///
	///On success, returns `Ok(())`.
	///
	///On failure, returns `Err(error)`, where `error` describes the error.
	///
	///Note that shorter outputs are prefixes of longer outputs with the same key and information.
	pub fn hkdf_expand<F>(&self, key: &[u8], data_cb: F, data: &mut [u8]) -> Result<(), HashFunctionError>
		where F: Fn(&mut HashInput) -> ()
	{
		match self {
			&HashFunction::Sha256 => Self::sha256().hkdf_expand(key, data_cb, data),
			&HashFunction::Sha384 => Self::sha384().hkdf_expand(key, data_cb, data),
			&HashFunction::Sha512 => Self::sha512().hkdf_expand(key, data_cb, data),
			&HashFunction::Sha3256 => Self::sha3_256().hkdf_expand(key, data_cb, data),
			&HashFunction::Sha3384 => Self::sha3_384().hkdf_expand(key, data_cb, data),
			&HashFunction::Sha3512 => Self::sha3_512().hkdf_expand(key, data_cb, data),
		}
	}
}

///A handle to hash calculation context, passed to callbacks.
///
///This structure represents a handle to hash calculation context. It can be used to append more data to be hashed.
pub struct HashInput<'a>(&'a mut HashFunctionContext);

impl<'a> HashInput<'a>
{
	///Append more data to be hashed.
	///
	///This method appends the data in `data` to hash computation described by `self`.
	pub fn input(&mut self, data: &[u8])
	{
		self.0.input(data);
	}
}

#[derive(Copy,Clone)]
struct LengthCounter(usize);

impl HashFunctionContext for LengthCounter
{
	fn input_length(&self) -> usize { 0 }
	fn output_length(&self) -> usize { 0 }
	fn reset(&mut self) { self.0 = 0; }
	fn input(&mut self, data: &[u8]) {
		//Hash functions can't fail.
		self.0 = self.0.wrapping_add(data.len());
	}
	fn output(&self) -> Result<HashOutput, HashFunctionError> {
		Err(HashFunctionError::DummyHash)
	}
	fn function(&self) -> HashFunction { HashFunction::Sha256 }	//Just something.
	fn clone_state(&self) -> Box<HashFunctionContext+Send> {
		Box::new(self.clone())
	}
}

mod nettlef
{
	use super::{HashFunction as HashFunc, HashFunctionContext as HashCtxTrait, HashFunctionContextExtended,
		HashFunctionError, HashOutput};
	pub use btls_aux_nettle::{Sha1Ctx, Sha256Ctx, Sha384Ctx, Sha512Ctx};
	use btls_aux_nettle::{Sha3256Ctx as _Sha3256Ctx, Sha3384Ctx as _Sha3384Ctx, Sha3512Ctx as _Sha3512Ctx};
	use btls_aux_securebuf::wipe_buffer;
	use std::cmp::min;
	use sha3::{Sha3_256, Sha3_384, Sha3_512};
	use digest::Input;
	use digest::FixedOutput;

	#[derive(Clone)]
	struct Sha3256Fallback(Sha3_256);
	#[derive(Clone)]
	struct Sha3384Fallback(Sha3_384);
	#[derive(Clone)]
	struct Sha3512Fallback(Sha3_512);

	macro_rules! low_fallback_sha3
	{
		($name:ident, $outlen:expr, $lowtype:ident) => {
			impl $name
			{
				fn new() -> $name
				{
					$name($lowtype::default())
				}
				fn input(&mut self, data: &[u8])
				{
					self.0.digest(data);
				}
				fn reset(&mut self)
				{
					self.0 = $lowtype::default();
				}
				fn output(&mut self) -> [u8;$outlen]
				{
					let mut buf = [0;$outlen];
					let copy = self.0.clone();
					let res = copy.fixed_result();
					let minspace = min(buf.len(), res.len());
					//minspace should be buf.len() = res.len().
					(&mut buf[..minspace]).copy_from_slice(&res[..minspace]);
					buf
				}
			}
		}
	}

	low_fallback_sha3!(Sha3256Fallback, 32, Sha3_256);
	low_fallback_sha3!(Sha3384Fallback, 48, Sha3_384);
	low_fallback_sha3!(Sha3512Fallback, 64, Sha3_512);

	#[derive(Clone)]
	pub struct Sha3256Ctx(Result<_Sha3256Ctx, Sha3256Fallback>);
	#[derive(Clone)]
	pub struct Sha3384Ctx(Result<_Sha3384Ctx, Sha3384Fallback>);
	#[derive(Clone)]
	pub struct Sha3512Ctx(Result<_Sha3512Ctx, Sha3512Fallback>);

	macro_rules! fallback_sha3
	{
		($name:ident, $native:ident, $outlen:expr, $fallback:ident) => {
			impl $name
			{
				pub fn new() -> $name {
					$name($native::new().map_err(|_|$fallback::new()))
				}
				pub fn reset(&mut self)
				{
					match &mut self.0 {
						&mut Ok(ref mut x) => x.reset(),
						&mut Err(ref mut x) => x.reset(),
					}
				}
				pub fn input(&mut self, data: &[u8])
				{
					match &mut self.0 {
						&mut Ok(ref mut x) => x.input(data),
						&mut Err(ref mut x) => x.input(data),
					}
				}
				pub fn output(&mut self) -> [u8; $outlen]
				{
					match &mut self.0 {
						&mut Ok(ref mut x) => x.output(),
						&mut Err(ref mut x) => x.output(),
					}
				}
			}
		}
	}

	fallback_sha3!(Sha3256Ctx, _Sha3256Ctx, 32, Sha3256Fallback);
	fallback_sha3!(Sha3384Ctx, _Sha3384Ctx, 48, Sha3384Fallback);
	fallback_sha3!(Sha3512Ctx, _Sha3512Ctx, 64, Sha3512Fallback);
	
	#[derive(Clone)]
	pub struct Sha256(Sha256Ctx);
	#[derive(Clone)]
	pub struct Sha384(Sha384Ctx);
	#[derive(Clone)]
	pub struct Sha512(Sha512Ctx);
	#[derive(Clone)]
	pub struct Sha3256(Sha3256Ctx);
	#[derive(Clone)]
	pub struct Sha3384(Sha3384Ctx);
	#[derive(Clone)]
	pub struct Sha3512(Sha3512Ctx);

	impl Sha256 { pub fn new() -> Sha256 { Sha256(Sha256Ctx::new()) } }
	impl Sha384 { pub fn new() -> Sha384 { Sha384(Sha384Ctx::new()) } }
	impl Sha512 { pub fn new() -> Sha512 { Sha512(Sha512Ctx::new()) } }
	impl Sha3256 { pub fn new() -> Sha3256 { Sha3256(Sha3256Ctx::new()) } }
	impl Sha3384 { pub fn new() -> Sha3384 { Sha3384(Sha3384Ctx::new()) } }
	impl Sha3512 { pub fn new() -> Sha3512 { Sha3512(Sha3512Ctx::new()) } }

	macro_rules! hash_impl
	{
		($name:ident, $func:expr) => {
			impl HashCtxTrait for $name
			{
				fn reset(&mut self) { self.0.reset() }
				fn input(&mut self, data: &[u8]) { self.0.input(data); }
				fn input_sg(&mut self, data: &[&[u8]]) { for i in data.iter() { self.0.input(i); } }
				fn function(&self) -> HashFunc { $func }
				fn output(&self) -> Result<HashOutput, HashFunctionError>
				{
					let sname = self.function().name();
					let mut digest = self.0.clone().output();
					let ret = HashOutput::from2(&digest).map_err(|(x, y)|
						HashFunctionError::OutputLengthTooBig(sname,x, y))?;
					wipe_buffer(&mut digest);
					Ok(ret)
				}
				fn clone_state(&self) -> Box<HashCtxTrait+Send> { Box::new(self.clone()) }
			}
			impl HashFunctionContextExtended for $name {}
		}
	}

	hash_impl!(Sha256, HashFunc::Sha256);
	hash_impl!(Sha384, HashFunc::Sha384);
	hash_impl!(Sha512, HashFunc::Sha512);
	hash_impl!(Sha3256, HashFunc::Sha3256);
	hash_impl!(Sha3384, HashFunc::Sha3384);
	hash_impl!(Sha3512, HashFunc::Sha3512);
}

///Checksum function.
///
///This enumeration describes a checksum function.
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub enum ChecksumFunction
{
	///SHA-1 (INSECURE).
	///
	///This is the now broken and insecure `SHA-1` function.
	Sha1Insecure,
	///SHA-256
	///
	///This is the `SHA-2` hash function with 256-bit output.
	Sha256,
	///SHA-384
	///
	///This is the `SHA-2` hash function with 384-bit output.
	Sha384,
	///SHA-512
	///
	///This is the `SHA-2` hash function with 512-bit output.
	Sha512,
	///SHA3-256
	///
	///This is the `SHA-3` hash function with 256-bit output.
	Sha3256,
	///SHA3-384
	///
	///This is the `SHA-3` hash function with 384-bit output.
	Sha3384,
	///SHA3-512.
	///
	///This is the `SHA-3` hash function with 512-bit output.
	///
	///Note, this hash function is slow to calculate.
	Sha3512,
}

macro_rules! hash_calculate
{
	($ctx:expr, $name:expr, $input:expr) => {{
		let mut ctx = $ctx;
		ctx.input($input);
		let mut digest = ctx.output();
		let ret = HashOutput::from2(&digest).map_err(|(x,y)|HashFunctionError::OutputLengthTooBig(
			$name, x, y));
		wipe_buffer(&mut digest);
		ret
	}}
}

impl ChecksumFunction
{
	///Make checksum function out of hash function.
	///
	///This method returns the checksum function corresponding to TLS PRF hash described by `hash'.
	pub fn from_hash(hash: HashFunction) -> ChecksumFunction
	{
		match hash {
			HashFunction::Sha256 => ChecksumFunction::Sha256,
			HashFunction::Sha384 => ChecksumFunction::Sha384,
			HashFunction::Sha512 => ChecksumFunction::Sha512,
			HashFunction::Sha3256 => ChecksumFunction::Sha3256,
			HashFunction::Sha3384 => ChecksumFunction::Sha3384,
			HashFunction::Sha3512 => ChecksumFunction::Sha3512,
		}
	}
	///Calculate a checksum value for data.
	///
	///Takes in:
	///
	/// * A checksum function `self`.
	/// * A piece of data `data`.
	///
	///This method calculates the checksum of the data.
	///
	///On success, returns `Ok(hashval)`, where `hashval` is the checksum value.
	///
	///On failure, returns `Err(error)`, where `err` describes the error. All errors from this method can be
	///treated as internal errors, this method should never actually fail.
	pub fn calculate(&self, input: &[u8]) -> Result<HashOutput, HashFunctionError>
	{
		match self {
			&ChecksumFunction::Sha1Insecure => hash_calculate!(nettlef::Sha1Ctx::new(), "SHA-1", input),
			&ChecksumFunction::Sha256 => hash_calculate!(nettlef::Sha256Ctx::new(), "SHA-256", input),
			&ChecksumFunction::Sha384 => hash_calculate!(nettlef::Sha384Ctx::new(), "SHA-384", input),
			&ChecksumFunction::Sha512 => hash_calculate!(nettlef::Sha512Ctx::new(), "SHA-512", input),
			&ChecksumFunction::Sha3256 => hash_calculate!(nettlef::Sha3256Ctx::new(), "SHA3-256", input),
			&ChecksumFunction::Sha3384 => hash_calculate!(nettlef::Sha3384Ctx::new(), "SHA3-384", input),
			&ChecksumFunction::Sha3512 => hash_calculate!(nettlef::Sha3512Ctx::new(), "SHA3-512", input),
		}
	}
}

///The type of checksum output.
///
///This type presents the output of the `checksum()` function.
///
///Note: The output type can change!
pub type ChecksumOutput = [u8; 32];

///Compute a checksum of data
///
///Takes in:
///
/// * A piece of data `input`.
///
///This function computes a checksum of the data using an unspecified strong hash function.
///
///Returns the hash value computed.
///
///The hash function used is guaranteed to remain stable as long as this library is loaded. It is intended to quickly
///compare pieces of data, not to be sent anywhere or stored in any sort of shared or non-volatile memory.
pub fn checksum(input: &[u8]) -> ChecksumOutput
{
	let mut ctx = nettlef::Sha256Ctx::new();
	ctx.input(input);
	ctx.output()
}

#[cfg(test)]
fn test_hash_function(func: HashFunction)
{
	let mut ctx = func.make_context();
	assert_eq!(func.input_length(), ctx.input_length());
	assert_eq!(func.output_length(), ctx.output_length());
	assert!(func.input_length() <= MAX_HASH_INPUT);
	assert!(func.output_length() <= MAX_HASH_OUTPUT);
	assert_eq!(ctx.function(), func);
	let emptyhash = ctx.output().unwrap();
	let emptyhash2 = ctx.output().unwrap();
	assert_eq!(emptyhash.as_ref().len(), func.output_length());
	assert_eq!(emptyhash.as_ref(), emptyhash2.as_ref());
	ctx.input("foo".as_bytes());
	ctx.input("bar".as_bytes());
	let foobarhash = ctx.output().unwrap();
	let mut ctx2 = ctx.clone_state();
	ctx.reset();
	ctx.input("foobar".as_bytes());
	let foobarhash2 = ctx.output().unwrap();
	assert_eq!(foobarhash.as_ref(), foobarhash2.as_ref());
	ctx.input("baz".as_bytes());
	ctx2.input("baz".as_bytes());
	let foobarbazhash = ctx.output().unwrap();
	let foobarbazhash2 = ctx2.output().unwrap();
	assert_eq!(foobarbazhash.as_ref(), foobarbazhash2.as_ref());
	ctx.reset();
	let emptyhash3 = ctx.output().unwrap();
	assert_eq!(emptyhash.as_ref(), emptyhash3.as_ref());
}


#[test]
fn test_sha256_hash()
{
	test_hash_function(HashFunction::Sha256);
	assert_eq!(HashFunction::Sha256.input_length(), 64);
	assert_eq!(HashFunction::Sha256.output_length(), 32);
}

#[test]
fn test_sha384_hash()
{
	test_hash_function(HashFunction::Sha384);
	assert_eq!(HashFunction::Sha384.input_length(), 128);
	assert_eq!(HashFunction::Sha384.output_length(), 48);
}

#[test]
fn test_sha512_hash()
{
	test_hash_function(HashFunction::Sha512);
	assert_eq!(HashFunction::Sha512.input_length(), 128);
	assert_eq!(HashFunction::Sha512.output_length(), 64);
}

#[test]
fn test_sha3_256_hash()
{
	test_hash_function(HashFunction::Sha3256);
	assert_eq!(HashFunction::Sha3256.input_length(), 200-2*32);
	assert_eq!(HashFunction::Sha3256.output_length(), 32);
}

#[test]
fn test_sha3_384_hash()
{
	test_hash_function(HashFunction::Sha3384);
	assert_eq!(HashFunction::Sha3384.input_length(), 200-2*48);
	assert_eq!(HashFunction::Sha3384.output_length(), 48);
}

#[test]
fn test_sha3_512_hash()
{
	test_hash_function(HashFunction::Sha3512);
	assert_eq!(HashFunction::Sha3512.input_length(), 200-2*64);
	assert_eq!(HashFunction::Sha3512.output_length(), 64);
}
