//!Time routines.
//!
//!This crate provodes time related methods, including:
//!
//! * Timestamp type.
//! * Trait for transporting time (including current)
//! * Get current time routine

#![no_std]
#![forbid(unsafe_code)]
#![forbid(missing_docs)]

use core::ops::Add;
use core::ops::Range;

///Timestamp type.
///
///This structure contains a second-resolution timestamp. The range of the timestamp goes from big bang to
///to over 290 billion years CE.
#[derive(Copy,Clone,PartialEq,PartialOrd,Eq,Ord,Debug)]
pub struct Timestamp(i64);

impl Timestamp
{
	///Create new timestamp.
	///
	///Takes in:
	///
	/// * A time offset from Unix epoch `ts`.
	///
	///This method creates a new timestamp from the offset.
	///
	///The offset is denoted as seconds since 1st Jaunuary 1970 midnight UTC, without leap seconds.
	pub fn new(ts: i64) -> Timestamp { Timestamp(ts) }
	///Get the timestamp as i64.
	///
	///Takes in:
	///
	/// * A timestamp `self`.
	///
	///This method casts the timestamp `self` into an integer. The returned integer is number of seconds since
	///1st Jaunuary 1970 midnight UTC, without leap seconds.
	pub fn into_inner(self) -> i64 { self.0 }
	///Negative infinity.
	///
	///This method returns the timestamp that is as far into the past as possible.
	pub fn negative_infinity() -> Timestamp { Timestamp(i64::min_value()) }
	///Positive infinity.
	///
	///This method returns the timestamp that is as far into the future as possible.
	pub fn positive_infinity() -> Timestamp { Timestamp(i64::max_value()) }
	///First of timestamps.
	///
	/// * A timestamp `self`.
	/// * An another timestamp `another`.
	///
	///This method returns the former of the two timestamps.
	pub fn first(self, another: Timestamp) -> Timestamp
	{
		if self < another { self } else { another }
	}
	///Last of timestamps.
	///
	/// * A timestamp `self`.
	/// * An another timestamp `another`.
	///
	///This method returns the latter of the two timestamps.
	pub fn last(self, another: Timestamp) -> Timestamp
	{
		if self > another { self } else { another }
	}
	///Get time difference between two timestamps.
	///
	///Takes in:
	///
	/// * The later timestamp `self`.
	/// * The former timestamp `another`.
	///
	///This method returns how much the later timestamp is after the former. If the later timestamp is actually
	///the former one, returns 0.
	pub fn delta(self, another: Timestamp) -> u64
	{
		if another.0 >= self.0 {
			let base = self.0;
			let baseoff = (!base).wrapping_add(1) as u64;
			baseoff.wrapping_add(another.0 as u64)
		} else {
			0
		}
	}
	///Is this timestamp in specified range?
	///
	///Takes in:
	///
	/// * A timestamp `self`.
	/// * A timestamp range `r`.
	///
	///This method returns `true` if the timestamp is in the range, `false` otherwise.
	pub fn in_range(self, r: &Range<Timestamp>) -> bool
	{
		self >= r.start && self < r.end
	}
}

///Interface: Get timestamp.
///
///This trait provodes an interface of getting a timestamp.
pub trait TimeInterface
{
	///Get the relevant timestamp.
	///
	///Takes in:
	///
	/// * A receiver `self`.
	///
	///This method returns the relevant timestamp for the receiver. This timestamp may be current time or some
	///fixed time represented by it.
	fn get_time(&self) -> Timestamp;
}

impl TimeInterface for Timestamp
{
	fn get_time(&self) -> Timestamp { *self }
}

impl Add<i64> for Timestamp
{
	type Output = Timestamp;
	fn add(self, x: i64) -> Timestamp
	{
		Timestamp(self.0 + x)
	}
}

impl Default for Timestamp
{
	fn default() -> Timestamp { Timestamp(0) }
}

///Get current time.
///
///This structure implements [`TimeInterface`] by returning the current wallclock time.
///
///[`TimeInterface`]: trait.TimeInterface.html
pub struct TimeNow;

impl TimeInterface for TimeNow
{
	fn get_time(&self) -> Timestamp
	{
		extern crate time;
		let now = time::get_time();
		Timestamp(now.sec)
	}
}


#[test]
fn timestamp_sub_normal()
{
	assert_eq!(Timestamp::new(123).delta(Timestamp::new(245)), 122);
}

#[test]
fn timestamp_sub_opposite()
{
	assert_eq!(Timestamp::new(245).delta(Timestamp::new(123)), 0);
}

#[test]
fn timestamp_sub_verylarge()
{
	assert_eq!(Timestamp::new(-0x6000000000000000).delta(Timestamp::new(0x6000000000000000)),
		0xC000000000000000);
}
