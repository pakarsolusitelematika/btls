//!Keypairs implemented in-process.
//!
//!This crate provodes an in-process implementation of the `KeyPair` trait from `btls-aux-keypair` crate.

#![deny(unsafe_code)]
#![forbid(missing_docs)]

use std::borrow::{Cow, ToOwned};
use std::convert::AsRef;
use std::fs::File;
use std::io::{Error as IoError, Read as IoRead};
use std::fmt::{Display, Error as FmtError, Formatter};
use std::ops::Deref;
use std::path::Path;
use std::sync::Arc;

#[macro_use]
extern crate btls_aux_fail;
extern crate btls_aux_ecdsa_sign;
extern crate btls_aux_ed25519_sign;
extern crate btls_aux_ed448_sign;
extern crate btls_aux_futures;
extern crate btls_aux_keyconvert;
extern crate btls_aux_keypair;
extern crate btls_aux_rsa_sign;
extern crate btls_aux_signature;
extern crate btls_aux_signature_algo;
extern crate libc;
use btls_aux_ed25519_sign::{Ed25519KeyLoadingError, Ed25519KeyPair};
use btls_aux_ed448_sign::{Ed448KeyLoadingError, Ed448KeyPair};
use btls_aux_futures::FutureReceiver;
use btls_aux_keyconvert::{convert_key_from_cwk, convert_key_from_jwk, convert_key_from_sexpr, DecodingError,
	EcdsaCurve, KEYFORMAT_ECDSA, KEYFORMAT_ED25519, KEYFORMAT_ED448, KEYFORMAT_RSA};
use btls_aux_keypair::{KeyPair, SubjectPublicKeyInfo, SupportedSchemes};
use btls_aux_rsa_sign::{RsaKeyLoadingError, RsaKeyPair};
use btls_aux_ecdsa_sign::{EcdsaKeyLoadingError, EcdsaKeyPair};
use btls_aux_signature_algo::{SIGN_ECDSA_P256_SCHEMES, SIGN_ED25519_SCHEMES, SIGN_ED448_SCHEMES, SIGN_RSA_SCHEMES,
	SignatureKeyPair};

#[cfg(not(unix))]
#[path="dummy_module.rs"]
mod module;
#[cfg(unix)]
#[path="module.rs"]
mod module;
pub use self::module::{ModuleError, ModuleKey};


//Error for loading a keypair. Hide this because it isn't reachable from anything documented.
#[doc(hidden)]
#[derive(Clone,Debug,PartialEq,Eq)]
pub enum KeyLoadingError
{
	#[doc(hidden)]
	Rsa(RsaKeyLoadingError),
	#[doc(hidden)]
	Ecdsa(EcdsaKeyLoadingError),
	#[doc(hidden)]
	Ed25519(Ed25519KeyLoadingError),
	#[doc(hidden)]
	Ed448(Ed448KeyLoadingError),
	#[doc(hidden)]
	KeyDecodeError(DecodingError),
	#[doc(hidden)]
	UnrecognizedPrivateKeyFormat,
	#[doc(hidden)]
	UnrecognizedKeyVersion,
	#[doc(hidden)]
	UnknownPrivateKeyType,
	#[doc(hidden)]
	Module(ModuleError),
	#[doc(hidden)]
	Hidden__
}


///Error reading key pair
///
///This enumeration describes an error arising in reading of a keypair.
#[derive(Debug)]
pub enum KeyReadError
{
	#[doc(hidden)]
	ParseError(String, KeyLoadingError),
	#[doc(hidden)]
	OpenError(String, IoError),
	#[doc(hidden)]
	ReadError(String, IoError),
	#[doc(hidden)]
	KeyGenerationFailed,
	#[doc(hidden)]
	Hidden__
}

impl Display for KeyLoadingError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::KeyLoadingError::*;
		match self {
			&Rsa(ref x) => fmt.write_fmt(format_args!("RSA loading: {}", x)),
			&Ecdsa(ref x) => fmt.write_fmt(format_args!("ECDSA loading: {}", x)),
			&Ed25519(ref x) => fmt.write_fmt(format_args!("Ed25519 loading: {}", x)),
			&Ed448(ref x) => fmt.write_fmt(format_args!("Ed448 loading: {}", x)),
			&KeyDecodeError(x) => fmt.write_fmt(format_args!("Error decoding key: {}", x)),
			&UnrecognizedPrivateKeyFormat => fmt.write_str("Unrecognized private key format"),
			&UnrecognizedKeyVersion => fmt.write_str("Unrecognized key format version"),
			&UnknownPrivateKeyType => fmt.write_str("Unknown private key type"),
			&Module(ref x) => fmt.write_fmt(format_args!("Can't load module key: {}", x)),
			&Hidden__ => fmt.write_str("Hidden__")
		}
	}
}

impl Display for KeyReadError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::KeyReadError::*;
		match self {
			&ParseError(ref n, ref x) => fmt.write_fmt(format_args!("Can't parse keypair '{}': {}", n,
				x)),
			&OpenError(ref n, ref x) => fmt.write_fmt(format_args!("Can't open keypair '{}': {}", n, x)),
			&ReadError(ref n, ref x) => fmt.write_fmt(format_args!("Can't read keypair '{}': {}", n, x)),
			&KeyGenerationFailed => fmt.write_str("Can't generate new key"),
			&Hidden__ => fmt.write_str("Hidden__")
		}
	}
}


//Don't put anything that isn't Send and Sync here!
enum LocalKeyPairStorage
{
	Rsa(RsaKeyPair, Vec<u8>),
	Ed25519(Ed25519KeyPair, Vec<u8>),
	Ed448(Ed448KeyPair, Vec<u8>),
	Ecdsa(EcdsaCurve, EcdsaKeyPair, Vec<u8>),
	Module(ModuleKey, Vec<u8>, Vec<u16>),
}


fn convert_key<'a>(key: &'a [u8]) -> Result<(usize, Cow<'a, [u8]>), KeyLoadingError>
{
	use self::KeyLoadingError::*;
	if key.get(0).map(|x|*x) == Some(40) {		//40 => ( from S-Expr.
		return Ok(convert_key_from_sexpr(key).map_err(|x|KeyDecodeError(x))?);
	} else if looks_like_json(key) {		//123 => { from JSON.
		return Ok(convert_key_from_jwk(key).map_err(|x|KeyDecodeError(x))?);
	} else if key.get(0).map(|x|*x&0xE0) == Some(160) {	//160-191 (CBOR major 5)
		return Ok(convert_key_from_cwk(key).map_err(|x|KeyDecodeError(x))?);
	}
	fail!(UnrecognizedPrivateKeyFormat);
}

fn aspace(b: u8) -> bool
{
	b == 9 || b == 10 || b == 13 || b == 32
}

fn looks_like_json(key: &[u8]) -> bool
{
	let mut idx = 0;
	//If the string starts with BOM, skip it.
	if key.len() > 3 && key[0] == 0xEF && key[1] == 0xBB && key[2] == 0xBF {
		idx = 3;
	}
	//Index, so stays in bounds.
	while idx < key.len() && aspace(key[idx]) { idx += 1; }
	//Read first non-ws, and check it is '{'.
	key.get(idx).map(|x|*x) == Some(123)
}


impl LocalKeyPairStorage
{
	fn get_key_type(&self) -> &'static str
	{
		match self {
			&LocalKeyPairStorage::Rsa(_, _) => "rsa",
			&LocalKeyPairStorage::Ed25519(_, _) => "ed25519",
			&LocalKeyPairStorage::Ed448(_, _) => "ed448",
			&LocalKeyPairStorage::Ecdsa(crv, _, _) => crv.get_ecdsa_name(),
			&LocalKeyPairStorage::Module(ref x, _, _) => x.get_key_type(),
		}
	}
	fn new(data: &[u8]) -> Result<LocalKeyPairStorage, KeyLoadingError>
	{
		use self::KeyLoadingError::*;
		if data.get(0).map(|x|*x) == Some(47) {		//47 => / from module.
			let mkey = ModuleKey::new(data).map_err(|x|KeyLoadingError::Module(x))?;
			let pubkey = mkey.get_public_key();
			let schemes = mkey.get_schemes();
			return Ok(LocalKeyPairStorage::Module(mkey, pubkey, schemes));
		}
		let (kind, data) = convert_key(data)?;
		if kind == KEYFORMAT_ED25519 {
			let (_, privkey, pubkey) = Ed25519KeyPair::keypair_load(data.deref()).map_err(|x|
				Ed25519(x))?;
			Ok(LocalKeyPairStorage::Ed25519(privkey, pubkey))
		} else if kind == KEYFORMAT_ED448 {
			let (_, privkey, pubkey) = Ed448KeyPair::keypair_load(data.deref()).map_err(|x|Ed448(x))?;
			Ok(LocalKeyPairStorage::Ed448(privkey, pubkey))
		} else if kind == KEYFORMAT_ECDSA {
			let (variant, privkey, pubkey) = EcdsaKeyPair::keypair_load(data.deref()).map_err(|x|Ecdsa(
				x))?;
			Ok(LocalKeyPairStorage::Ecdsa(variant, privkey, pubkey))
		} else if kind == KEYFORMAT_RSA {
			let (_, privkey, pubkey) = RsaKeyPair::keypair_load(data.deref()).map_err(|x|Rsa(x))?;
			Ok(LocalKeyPairStorage::Rsa(privkey, pubkey))
		} else {
			fail!(UnknownPrivateKeyType)
		}
	}
	fn sign(&self, data: &[u8], algorithm: u16) -> FutureReceiver<Result<Vec<u8>, ()>>
	{
		let mut output = [0;768];	//Should be big enough for RSA.
		FutureReceiver::from(match match self {
			&LocalKeyPairStorage::Rsa(ref key, _) =>
				//RSA
				key.keypair_sign(data, algorithm, &mut output),
			&LocalKeyPairStorage::Ed25519(ref key, _) =>
				//Ed25519
				key.keypair_sign(data, algorithm, &mut output),
			&LocalKeyPairStorage::Ed448(ref key, _) =>
				//Ed448
				key.keypair_sign(data, algorithm, &mut output),
			&LocalKeyPairStorage::Ecdsa(_, ref key, _) =>
				//ECDSA
				key.keypair_sign(data, algorithm, &mut output),
			&LocalKeyPairStorage::Module(ref key, _, _) => {
				//Module.
				return key.sign(data, algorithm)
			}
		} {
			Ok(siglen) => if siglen < output.len() { Ok((&output[..siglen]).to_owned()) } else {
				Err(()) },
			Err(_) => Err(())
		})
	}
	fn get_schemes(&self) -> Cow<'static, [u16]>
	{
		match self {
			&LocalKeyPairStorage::Rsa(_, _) => Cow::Borrowed(&SIGN_RSA_SCHEMES[..]),
			&LocalKeyPairStorage::Ed25519(_, _) => Cow::Borrowed(&SIGN_ED25519_SCHEMES[..]),
			&LocalKeyPairStorage::Ed448(_, _) => Cow::Borrowed(&SIGN_ED448_SCHEMES[..]),
			&LocalKeyPairStorage::Ecdsa(crv,_ , _) => Cow::Borrowed(crv.native_signature_schemes()),
			&LocalKeyPairStorage::Module(_, _, ref sch) => Cow::Owned(sch.clone()),
		}
	}
	fn get_pubkey(&self) -> Vec<u8>
	{
		match self {
			&LocalKeyPairStorage::Rsa(_, ref pkey) => pkey.clone(),
			&LocalKeyPairStorage::Ed25519(_, ref pkey) => pkey.clone(),
			&LocalKeyPairStorage::Ed448(_, ref pkey) => pkey.clone(),
			&LocalKeyPairStorage::Ecdsa(_, _, ref pkey) => pkey.clone(),
			&LocalKeyPairStorage::Module(_, ref pkey, _) => pkey.clone(),
		}
	}
}

struct _LocalKeyPair
{
	privkey: LocalKeyPairStorage,
	pubkey: SubjectPublicKeyInfo,
	schemes: SupportedSchemes,
	keytype: &'static str,
}

impl _LocalKeyPair
{
	fn new(data: &[u8]) -> Result<_LocalKeyPair, KeyLoadingError>
	{
		let privkey = LocalKeyPairStorage::new(data)?;
		let pubkey = privkey.get_pubkey();
		let schemes = privkey.get_schemes();
		let keytype = privkey.get_key_type();
		Ok(_LocalKeyPair{privkey:privkey, pubkey:SubjectPublicKeyInfo(Arc::new(pubkey)),
			schemes:SupportedSchemes(Arc::new(schemes)), keytype: keytype})
	}
	fn sign(&self, data: &[u8], algorithm: u16) -> FutureReceiver<Result<Vec<u8>, ()>>
	{
		self.privkey.sign(data, algorithm)
	}
	fn get_schemes(&self) -> SupportedSchemes
	{
		self.schemes.clone()
	}
	fn get_public_key(&self) -> SubjectPublicKeyInfo
	{
		self.pubkey.clone()
	}
	fn get_key_type(&self) -> &'static str
	{
		self.keytype
	}
}

///Key pair that is implemented by signing using a private key.
///
///This implements the `KeyPair` trait.
///
///Obviously, given the nature of this kind of signer, the private key is contained in the memory space of the
///process, and thus can be leaked with any sort of memory disclosure vulernability (not necressarily in the TLS
///implementation).
#[derive(Clone)]
pub struct LocalKeyPair(Arc<_LocalKeyPair>);

impl LocalKeyPair
{
	///Create a keypair out of something implementing `Read`.
	///
	///Takes in:
	///
	/// * A stream containing a keypair `privkey`.
	/// * A name for the stream `name`.
	///
	///This method creates a key pair by reading contents out of the stream. The name is used in error messages.
	///
	///The key formats currently supported are:
	///
	/// * COSE key format.
	/// * JWK format.
	/// * S-Expression format.
	/// * Internal module format.
	///
	///This method can be used to load keys from buffers since `&[u8]: Read` (but note the extra `&mut`
	///qualifier).
	///
	///On success, returns `Ok(keypair)` where `keypair` is the loaded keypair.
	///
	///On failure, returns `Err(error)` where `error` describes the error that occured.
	pub fn new<R1:IoRead>(privkey: &mut R1, name: &str) -> Result<LocalKeyPair, KeyReadError>
	{
		let mut _privkey = Vec::new();
		privkey.read_to_end(&mut _privkey).map_err(|x|KeyReadError::ReadError(name.to_owned(), x))?;
		Ok(LocalKeyPair(Arc::new(_LocalKeyPair::new(&_privkey).map_err(|x|KeyReadError::ParseError(
			name.to_owned(), x))?)))
	}
	///Create a keypair out of a file.
	///
	///Takes in:
	///
	/// * A name of file containing a keypair `privkey`.
	///
	///This method loads a keypair by reading the file.
	///
	///The key formats currently supported are:
	///
	/// * COSE key format.
	/// * JWK format.
	/// * S-Expression format.
	/// * Internal module format.
	///
	///On success, returns `Ok(keypair)` where `keypair` is the loaded keypair.
	///
	///On failure, returns `Err(error)` where `error` describes the error that occured.
	pub fn new_file<P1: AsRef<Path>>(privkey: P1) -> Result<LocalKeyPair, KeyReadError>
	{
		let privkey = privkey.as_ref();
		let call_as = format!("{}", privkey.display());
		let mut _privkey = File::open(privkey).map_err(|x|KeyReadError::OpenError(call_as.clone(), x))?;
		Self::new(&mut _privkey, &call_as)
	}
	///Create a fresh temporary ECDSA P-256 keypair.
	///
	///This method creates a transient ECDSA P-256 keypair. There is no way of saving the keypair anywhere, so
	///the method is only useful for keys used for a short time.
	///
	///On success, returns `Ok(keypair)` where `keypair` is the generated keypair.
	///
	///On failure, returns `Err(error)` where `error` describes the error that occured.
	pub fn new_transient_ecdsa_p256() -> Result<LocalKeyPair, KeyReadError>
	{
		use self::KeyReadError::*;
		let (kp, pubkey) = match EcdsaKeyPair::new_transient_ecdsa_p256() {
			Ok((x, y)) => (x, y),
			Err(_) => fail!(KeyGenerationFailed)
		};
		let privkey = LocalKeyPairStorage::Ecdsa(EcdsaCurve::NsaP256, kp, pubkey.clone());
		let schemes = &SIGN_ECDSA_P256_SCHEMES[..];
		let keytype = "ecdsa-p256";
		Ok(LocalKeyPair(Arc::new(_LocalKeyPair{privkey:privkey, pubkey:SubjectPublicKeyInfo(
			Arc::new(pubkey)), schemes:SupportedSchemes(Arc::new(Cow::Borrowed(schemes))),
			keytype:keytype})))
	}
}

impl KeyPair for LocalKeyPair
{
	fn sign(&self, data: &[u8], algorithm: u16) -> FutureReceiver<Result<Vec<u8>, ()>>
	{
		//Since signing is always synchronous, wrap the future here.
		self.0.sign(data, algorithm)
	}
	fn get_schemes(&self) -> SupportedSchemes
	{
		self.0.get_schemes()
	}
	fn get_public_key(&self) -> SubjectPublicKeyInfo
	{
		self.0.get_public_key()
	}
	fn get_key_type(&self) -> &'static str
	{
		self.0.get_key_type()
	}
}

#[test]
fn test_eddsa()
{
	use btls_aux_signature::SignatureBlock;
	use btls_aux_signature_algo::{SignatureType, VERIFY_FLAG_NO_RSA_PKCS1};
	let keypair = LocalKeyPair::new_file("src/testkey.raw").unwrap();
	let message = b"Hello, World!";
	assert_eq!(&(keypair.get_schemes().iter().cloned().collect::<Vec<_>>()), &[SIG_ED25519]);
	let s = keypair.sign(message, SIG_ED25519).read();
	let s = s.ok().unwrap();
	let sig = s.unwrap();
	assert_eq!(sig.len(), 64);
	let key = keypair.get_public_key();
	let signature = SignatureBlock::from_tls(SignatureType::Ed25519, &sig[..]);
	signature.verify(&key.0, message, 0). unwrap();
	signature.verify(&key.0, message, VERIFY_FLAG_NO_RSA_PKCS1).unwrap();
}

#[test]
fn test_eddsa_unknown()
{
	let keypair = LocalKeyPair::new_file("src/testkey.raw").unwrap();
	let message = b"Hello, World!";
	let s = keypair.sign(message, SIG_ECDSA_SHA256).read();
	let s = s.ok().unwrap();	//Future settled.
	s.unwrap_err();			//Signing failed.
}

#[test]
fn test_eddsa2()
{
	use btls_aux_signature::SignatureBlock;
	use btls_aux_signature_algo::{SignatureType, VERIFY_FLAG_NO_RSA_PKCS1};
	let refsig = include_bytes!("hellow.sig");
	let keypair = LocalKeyPair::new_file("src/testkey2.raw").unwrap();
	let message = b"Hello, World!";
	assert_eq!(&(keypair.get_schemes().iter().cloned().collect::<Vec<_>>()), &[SIG_ED448]);
	let s = keypair.sign(message, SIG_ED448).read();
	let s = s.ok().unwrap();
	let sig = s.unwrap();
	assert_eq!(sig.len(), 114);
	assert_eq!(&sig[..], &refsig[..]);
	let key = keypair.get_public_key();
	let signature = SignatureBlock::from_tls(SignatureType::Ed448, &sig[..]);
	signature.verify(&key.0, message, 0).unwrap();
	signature.verify(&key.0, message, VERIFY_FLAG_NO_RSA_PKCS1).unwrap();
}

#[test]
fn test_eddsa2_unknown()
{
	let keypair = LocalKeyPair::new_file("src/testkey2.raw").unwrap();
	let message = b"Hello, World!";
	let s = keypair.sign(message, SIG_ED25519).read();
	let s = s.ok().unwrap();	//Future settled.
	s.unwrap_err();			//Signing failed.
}

#[test]
fn test_rsa()
{
	use btls_aux_signature_algo::{SIG_RSA_PKCS1_SHA256, SIG_RSA_PKCS1_SHA384, SIG_RSA_PKCS1_SHA512,
		SIG_RSA_PSS_SHA256, SIG_RSA_PSS_SHA384, SIG_RSA_PSS_SHA512};
	use btls_aux_signature_algo::{SignatureType, VERIFY_FLAG_NO_RSA_PKCS1};
	use btls_aux_signature::SignatureBlock;
	let keypair = LocalKeyPair::new_file("src/testkey.der").unwrap();
	let message = b"Hello, World!";
	let schemes = match keypair.get_schemes().0.deref() {
		&Cow::Owned(ref x) => x.clone(),
		&Cow::Borrowed(x) => x.to_owned()
	};
	assert_eq!(schemes.deref(), &[SIG_RSA_PKCS1_SHA256,SIG_RSA_PKCS1_SHA384,SIG_RSA_PKCS1_SHA512,
		SIG_RSA_PSS_SHA256, SIG_RSA_PSS_SHA384, SIG_RSA_PSS_SHA512]);
	let s = keypair.sign(message, SIG_RSA_PKCS1_SHA256).read();
	let s = s.ok().unwrap();
	let sig = s.unwrap();
	assert_eq!(sig.len(), 256);
	let key = keypair.get_public_key();
	let signature = SignatureBlock::from_tls(SignatureType::RsaPkcs1Sha256, &sig[..]);
	signature.verify(&key.0, message, 0).unwrap();
	signature.verify(&key.0, message, VERIFY_FLAG_NO_RSA_PKCS1).unwrap_err();
}

#[test]
fn test_rsa_sha256()
{
	use btls_aux_signature_algo::SIG_RSA_PKCS1_SHA256;
	use btls_aux_signature_algo::{SignatureType, VERIFY_FLAG_NO_RSA_PKCS1};
	use btls_aux_signature::SignatureBlock;
	let keypair = LocalKeyPair::new_file("src/testkey.der").unwrap();
	let message = b"Hello, World!";
	let s = keypair.sign(message, SIG_RSA_PKCS1_SHA256).read();
	let s = s.ok().unwrap();
	let sig = s.unwrap();
	assert_eq!(sig.len(), 256);
	let key = keypair.get_public_key();
	let signature = SignatureBlock::from_tls(SignatureType::RsaPkcs1Sha256, &sig[..]);
	signature.verify(&key.0, message, 0).unwrap();
	signature.verify(&key.0, message, VERIFY_FLAG_NO_RSA_PKCS1).unwrap_err();
}

#[test]
fn test_rsa_sha384()
{
	use btls_aux_signature_algo::SIG_RSA_PKCS1_SHA384;
	use btls_aux_signature_algo::{SignatureType, VERIFY_FLAG_NO_RSA_PKCS1};
	use btls_aux_signature::SignatureBlock;
	let keypair = LocalKeyPair::new_file("src/testkey.der").unwrap();
	let message = b"Hello, World!";
	let s = keypair.sign(message, SIG_RSA_PKCS1_SHA384).read();
	let s = s.ok().unwrap();
	let sig = s.unwrap();
	assert_eq!(sig.len(), 256);
	let key = keypair.get_public_key();
	let signature = SignatureBlock::from_tls(SignatureType::RsaPkcs1Sha384, &sig[..]);
	signature.verify(&key.0, message, 0).unwrap();
	signature.verify(&key.0, message, VERIFY_FLAG_NO_RSA_PKCS1).unwrap_err();
}

#[test]
fn test_rsa_sha512()
{
	use btls_aux_signature_algo::SIG_RSA_PKCS1_SHA512;
	use btls_aux_signature_algo::{SignatureType, VERIFY_FLAG_NO_RSA_PKCS1};
	use btls_aux_signature::SignatureBlock;
	let keypair = LocalKeyPair::new_file("src/testkey.der").unwrap();
	let message = b"Hello, World!";
	let s = keypair.sign(message, SIG_RSA_PKCS1_SHA512).read();
	let s = s.ok().unwrap();
	let sig = s.unwrap();
	assert_eq!(sig.len(), 256);
	let key = keypair.get_public_key();
	let signature = SignatureBlock::from_tls(SignatureType::RsaPkcs1Sha512, &sig[..]);
	signature.verify(&key.0, message, 0).unwrap();
	signature.verify(&key.0, message, VERIFY_FLAG_NO_RSA_PKCS1).unwrap_err();
}

#[test]
fn test_rsa_sha256pss()
{
	use btls_aux_signature_algo::SIG_RSA_PSS_SHA256;
	use btls_aux_signature_algo::{SignatureType, VERIFY_FLAG_NO_RSA_PKCS1};
	use btls_aux_signature::SignatureBlock;
	let keypair = LocalKeyPair::new_file("src/testkey.der").unwrap();
	let message = b"Hello, World!";
	let s = keypair.sign(message, SIG_RSA_PSS_SHA256).read();
	let s = s.ok().unwrap();
	let sig = s.unwrap();
	assert_eq!(sig.len(), 256);
	let key = keypair.get_public_key();
	let signature = SignatureBlock::from_tls(SignatureType::RsaPssSha256, &sig[..]);
	signature.verify(&key.0, message, 0).unwrap();
	signature.verify(&key.0, message, VERIFY_FLAG_NO_RSA_PKCS1).unwrap();
}

#[test]
fn test_rsa_sha384pss()
{
	use btls_aux_signature_algo::SIG_RSA_PSS_SHA384;
	use btls_aux_signature_algo::{SignatureType, VERIFY_FLAG_NO_RSA_PKCS1};
	use btls_aux_signature::SignatureBlock;
	let keypair = LocalKeyPair::new_file("src/testkey.der").unwrap();
	let message = b"Hello, World!";
	let s = keypair.sign(message, SIG_RSA_PSS_SHA384).read();
	let s = s.ok().unwrap();
	let sig = s.unwrap();
	assert_eq!(sig.len(), 256);
	let key = keypair.get_public_key();
	let signature = SignatureBlock::from_tls(SignatureType::RsaPssSha384, &sig[..]);
	signature.verify(&key.0, message, 0).unwrap();
	signature.verify(&key.0, message, VERIFY_FLAG_NO_RSA_PKCS1).unwrap();
}

#[test]
fn test_rsa_sha512pss()
{
	use btls_aux_signature_algo::SIG_RSA_PSS_SHA512;
	use btls_aux_signature_algo::{SignatureType, VERIFY_FLAG_NO_RSA_PKCS1};
	use btls_aux_signature::SignatureBlock;
	let keypair = LocalKeyPair::new_file("src/testkey.der").unwrap();
	let message = b"Hello, World!";
	let s = keypair.sign(message, SIG_RSA_PSS_SHA512).read();
	let s = s.ok().unwrap();
	let sig = s.unwrap();
	assert_eq!(sig.len(), 256);
	let key = keypair.get_public_key();
	let signature = SignatureBlock::from_tls(SignatureType::RsaPssSha512, &sig[..]);
	signature.verify(&key.0, message, 0).unwrap();
	signature.verify(&key.0, message, VERIFY_FLAG_NO_RSA_PKCS1).unwrap();
}

/*
fn strip_leading_zero<'a>(x: &'a [u8]) -> &'a [u8]
{
	if x[0] == 0 { &x[1..] } else { x }
}

fn extend_sexpr(dest: &mut Vec<u8>, x: &[u8])
{
	dest.extend(format!("{}:", x.len()).as_bytes().iter());
	dest.extend(x.iter());
}

fn convert_file(name: &str)
{
	use std::io::Write;
	let mut contents = Vec::new();
	{
		let mut handle = File::open(name).unwrap();
		handle.read_to_end(&mut contents).unwrap();
	}
	let mut top = Source::new(&contents);
	let mut sub = top.read_asn1_value(ASN1_SEQUENCE, |_|"Error reading toplevel").unwrap().value;
	sub.read_asn1_value(ASN1_INTEGER, |_|"Error reading version").unwrap();	//Discard version.
	let mut dest = Vec::new();
	dest.push(b'(');
	dest.extend(b"3:rsa");
	extend_sexpr(&mut dest, strip_leading_zero(sub.read_asn1_value(ASN1_INTEGER,
		|_|"Error reading n").unwrap().raw_p));
	extend_sexpr(&mut dest, strip_leading_zero(sub.read_asn1_value(ASN1_INTEGER,
		|_|"Error reading e").unwrap().raw_p));
	extend_sexpr(&mut dest, strip_leading_zero(sub.read_asn1_value(ASN1_INTEGER,
		|_|"Error reading d").unwrap().raw_p));
	extend_sexpr(&mut dest, strip_leading_zero(sub.read_asn1_value(ASN1_INTEGER,
		|_|"Error reading p").unwrap().raw_p));
	extend_sexpr(&mut dest, strip_leading_zero(sub.read_asn1_value(ASN1_INTEGER,
		|_|"Error reading q").unwrap().raw_p));
	extend_sexpr(&mut dest, strip_leading_zero(sub.read_asn1_value(ASN1_INTEGER,
		|_|"Error reading dp").unwrap().raw_p));
	extend_sexpr(&mut dest, strip_leading_zero(sub.read_asn1_value(ASN1_INTEGER,
		|_|"Error reading dq").unwrap().raw_p));
	extend_sexpr(&mut dest, strip_leading_zero(sub.read_asn1_value(ASN1_INTEGER,
		|_|"Error reading qi").unwrap().raw_p));
	dest.push(b')');
	let mut handle = File::create(name).unwrap();
	handle.write_all(&dest).unwrap();
}

#[test]
fn convert_files()
{
	convert_file("src/rsa2040.der");
	convert_file("src/rsa2048.der");
	convert_file("src/rsa2055.der");
	convert_file("src/rsa2056.der");
	convert_file("src/rsa4095.der");
	convert_file("src/rsa4096.der");
	convert_file("src/rsa4097.der");
	convert_file("src/rsa4104.der");
}
*/

#[test]
fn test_rsa_badkeys()
{
	LocalKeyPair::new_file("src/rsa2048.der").unwrap();
	assert!(LocalKeyPair::new_file("src/rsa2040.der").is_err());
	assert!(LocalKeyPair::new_file("src/rsa2047.der").is_err());
	assert!(LocalKeyPair::new_file("src/rsa2055.der").is_err());
	assert!(LocalKeyPair::new_file("src/rsa4095.der").is_err());
	assert!(LocalKeyPair::new_file("src/rsa4097.der").is_err());
	assert!(LocalKeyPair::new_file("src/rsa4104.der").is_err());
}

#[test]
fn test_rsa_keysize_extreme()
{
	LocalKeyPair::new_file("src/rsa2048.der").unwrap();
	LocalKeyPair::new_file("src/rsa2056.der").unwrap();
	LocalKeyPair::new_file("src/rsa4096.der").unwrap();
}

#[test]
fn test_jose_boms()
{
	LocalKeyPair::new_file("src/test-boms.jose").unwrap();
}
