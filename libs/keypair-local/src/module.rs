//Module keys
//
//This module provodes an implementation of module keys (keys where operations are delegated to an module, in
//this case a in-process shared object).
//
//This can be used e.g. for implementing out-of-process (implying not vulernable to e.g. Heartbleed) keys. It Can
//also be used for having the keys reside on a HSM.

use std::borrow::ToOwned;
use std::collections::BTreeMap;
use std::ffi::CString;
use std::io::{stderr, Write as IoWrite};
use std::fmt::{Display, Error as FmtError, Formatter};
use std::str::from_utf8;
use std::sync::{Arc, RwLock};

use btls_aux_futures::{create_future, FutureReceiver, FutureSender};
use libc::{c_char, uid_t};

#[allow(unsafe_code)]
mod low {
	use std::ffi::{CStr, CString};
	use std::io::{Error as IoError};
	use std::mem::{transmute, zeroed};
	use std::slice::from_raw_parts;
	use std::sync::{Once, ONCE_INIT};
	use super::{_PubkeyRequest, _SignatureRequest, ModuleLibraryMap, PubkeyRequest, SignatureRequest};
	use libc::{c_char, c_void, dlerror, dlopen, dlsym, dlclose, RTLD_NOW};
	use libc::{getuid, geteuid, getgid, getegid, uid_t, gid_t};

	pub struct Uncallable(());
	pub unsafe trait ModuleSymbol
	{
		type FnType: Sized+Sync+Send+'static;
		fn name() -> &'static str;
		fn cast(raw: *mut c_void, u: Uncallable) -> <Self as ModuleSymbol>::FnType;
	}

	pub struct GetKeyHandle;
	pub struct GetKeyHandle2;
	pub struct RequestSign;
	pub struct RequestPubkey;

	unsafe impl ModuleSymbol for GetKeyHandle
	{
		type FnType = extern "C" fn(*const c_char, *mut u32) -> u64;
		fn name() -> &'static str { "btls_mod_get_key_handle" }
		fn cast(raw: *mut c_void, _: Uncallable) -> <Self as ModuleSymbol>::FnType
		{
			unsafe{transmute(raw)}
		}
	}

	unsafe impl ModuleSymbol for GetKeyHandle2
	{
		type FnType = extern "C" fn(*const u8, usize, *mut u32) -> u64;
		fn name() -> &'static str { "btls_mod_get_key_handle2" }
		fn cast(raw: *mut c_void, _: Uncallable) -> <Self as ModuleSymbol>::FnType
		{
			unsafe{transmute(raw)}
		}
	}

	unsafe impl ModuleSymbol for RequestSign
	{
		type FnType = extern "C" fn(u64, SignatureRequest, u16, *const u8, usize);
		fn name() -> &'static str { "btls_mod_request_sign" }
		fn cast(raw: *mut c_void, _: Uncallable) -> <Self as ModuleSymbol>::FnType
		{
			unsafe{transmute(raw)}
		}
	}

	unsafe impl ModuleSymbol for RequestPubkey
	{
		type FnType = extern "C" fn(u64, PubkeyRequest);
		fn name() -> &'static str { "btls_mod_request_pubkey" }
		fn cast(raw: *mut c_void, _: Uncallable) -> <Self as ModuleSymbol>::FnType
		{
			unsafe{transmute(raw)}
		}
	}

	pub fn s_getuid() -> uid_t { unsafe { getuid() } }
	pub fn s_geteuid() -> uid_t { unsafe { geteuid() } }
	pub fn s_getgid() -> gid_t { unsafe { getgid() } }
	pub fn s_getegid() -> gid_t { unsafe { getegid() } }
	pub fn s_stat(cname: &CStr) -> Result<::libc::stat, IoError>
	{
		unsafe {
			let mut st: ::libc::stat = zeroed();
			if ::libc::stat(cname.as_ptr(), &mut st as *mut _) < 0 {
				Err(IoError::last_os_error())
			} else {
				Ok(st)
			}
		}
	}
	pub struct _LibraryHandle(*mut c_void);
	unsafe impl Send for _LibraryHandle {}
	unsafe impl Sync for _LibraryHandle {}

	impl Drop for _LibraryHandle
	{
		fn drop(&mut self)
		{
			unsafe{dlclose(self.0)};
		}
	}

	impl _LibraryHandle
	{
		pub fn new(cname: &CStr) -> Result<_LibraryHandle, String>
		{
			unsafe {
				let handle = dlopen(cname.as_ptr(), RTLD_NOW);
				if handle.is_null() {
					let err = dlerror();
					Err(CStr::from_ptr(err).to_string_lossy().into_owned())
				} else {
					Ok(_LibraryHandle(handle))
				}
			}
		}
		pub fn symbol(&self, name: &CStr) -> Result<*mut c_void, ()>
		{
			unsafe {
				let ptr = dlsym(self.0, name.as_ptr());
				if ptr.is_null() {
					//This is strictly speaking wrong, but we don't need to handle NULL-valued
					//symbols.
					Err(())
				} else {
					Ok(ptr)
				}
			}
		}
		pub fn symbol2<MS:ModuleSymbol>(&self) -> Option<<MS as ModuleSymbol>::FnType>
		{
			self.symbol(&CString::new(MS::name()).unwrap()).ok().map(|x|MS::cast(x, Uncallable(())))
			//MS::cast(self.symbol(&CString::new(MS::name()).unwrap()), Uncallable(()))
		}
	}

	pub extern "C" fn signature_request_finish(sig: *mut _SignatureRequest, data: *const u8, datalen: usize)
	{
		unsafe {
			//Re-box it and drop.
			let sig = Box::from_raw(sig);
			if data.is_null() {
				sig.reject();
			} else {
				let slice = from_raw_parts(data, datalen);
				sig.finished(slice);
			}
		}
	}

	pub extern "C" fn signature_request_log(sig: *mut _SignatureRequest, msg: *const c_char)
	{
		unsafe {
			let sstr = CStr::from_ptr(msg).to_string_lossy();
			(*sig).log(sstr.as_ref());
		}
	}

	pub extern "C" fn pubkey_request_finish(pubk: *mut _PubkeyRequest, pubkey: *const u8, pubkeylen: usize,
		schemes: *const u16, schemescount: usize)
	{
		unsafe {
			let pubkey = from_raw_parts(pubkey, pubkeylen);
			let schemes = from_raw_parts(schemes, schemescount);
			(*pubk).finished(pubkey, schemes);
		}
	}

	pub fn get_global_modmap() -> &'static ModuleLibraryMap
	{
		static START: Once = ONCE_INIT;
		static mut MODULES: *mut ModuleLibraryMap = 0 as *mut _;
		START.call_once(|| {
			unsafe {
				MODULES = Box::into_raw(Box::new(ModuleLibraryMap::new()));
			}
		});
		unsafe {&*MODULES}
	}
}
use self::low::*;

fn get_module_load_privilege_uid() -> uid_t
{
	let r_uid = s_getuid();
	if r_uid != s_geteuid() || s_getgid() != s_getegid() { return 0; }
	return r_uid;
}

//Error from module. Hide this because it isn't reachable from anything documented.
#[doc(hidden)]
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum ModuleError
{
	#[doc(hidden)]
	CantLoadLibrary(String, String),
	#[doc(hidden)]
	NotValidModule,
	#[doc(hidden)]
	CantLockModules,
	#[doc(hidden)]
	NoSuchKey(u32),
	#[doc(hidden)]
	MalformedReference,
	#[doc(hidden)]
	OutOfLineRefsNotSupported,
	#[doc(hidden)]
	InLineRefsNotSupported,
	#[doc(hidden)]
	Hidden__
}

impl Display for ModuleError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::ModuleError::*;
		match self {
			&CantLoadLibrary(ref x, ref y) => fmt.write_fmt(format_args!("Can't load referenced \
				library '{}': {}", x, y)),
			&NotValidModule => fmt.write_str("Not a valid module"),
			&CantLockModules => fmt.write_str("Can't lock module storage"),
			&NoSuchKey(x) => fmt.write_fmt(format_args!("No such key found (code {})", x)),
			&MalformedReference => fmt.write_str("Malformed module reference file"),
			&OutOfLineRefsNotSupported => fmt.write_str("Out-of-line references not supported by \
				module"),
			&InLineRefsNotSupported => fmt.write_str("In-line references not supported by module"),
			&Hidden__ => fmt.write_str("Hidden__")
		}
	}
}

struct LibraryHandle(_LibraryHandle);

impl LibraryHandle
{
	fn new(name: &str) -> Result<LibraryHandle, ModuleError>
	{
		let privuid = get_module_load_privilege_uid();
		let cname = CString::new(name).map_err(|_|ModuleError::CantLoadLibrary(name.to_owned(),
			"Invalid name".to_owned()))?;
		let st = s_stat(&cname).map_err(|x|ModuleError::CantLoadLibrary(name.to_owned(),
			format!("Can't stat: {}", x)))?;
		fail_if!(st.st_uid != privuid && st.st_uid != 0, ModuleError::CantLoadLibrary(name.to_owned(),
			"Permission denied (bad owner)".to_owned()));
		fail_if!(st.st_mode & 64 != 64, ModuleError::CantLoadLibrary(name.to_owned(),
			"Permission denied (no +x)".to_owned()));
		Ok(LibraryHandle(_LibraryHandle::new(&cname).map_err(|x|ModuleError::CantLoadLibrary(
			name.to_owned(), x))?))
	}
	fn symbol2<MS:ModuleSymbol>(&self) -> Option<<MS as ModuleSymbol>::FnType>
	{
		self.0.symbol2::<MS>()
	}
}

//Has to be public due to private-in-public rules.
pub struct _SignatureRequest
{
	answer: FutureSender<Result<Vec<u8>, ()>>
}

impl _SignatureRequest
{
	fn finished(self, data: &[u8])
	{
		self.answer.settle(Ok(data.to_owned()));
	}
	fn reject(self)
	{
		self.answer.settle(Err(()));
	}
	fn log(&self, msg: &str)
	{
		//TODO: Better logging. Right now dump all this to stderr.
		let _ = writeln!(stderr(), "{}", msg);
		//Ignore errors.
	}
}

//Has to be public due to private-in-public rules.
pub struct _PubkeyRequest
{
	called: bool,
	pubkey: Vec<u8>,
	schemes: Vec<u16>,
}

impl _PubkeyRequest
{
	fn finished(&mut self, pubkey: &[u8], schemes: &[u16])
	{
		self.called = true;
		self.pubkey = pubkey.to_owned();
		self.schemes = schemes.to_owned();
	}
}

//Has to be public due to private-in-public rules.
#[repr(C)]
pub struct SignatureRequest
{
	context: *mut _SignatureRequest,
	finish: extern "C" fn(*mut _SignatureRequest, *const u8, usize),
	log: extern "C" fn(*mut _SignatureRequest, *const c_char)
}

//Has to be public due to private-in-public rules.
#[repr(C)]
pub struct PubkeyRequest
{
	context: *mut _PubkeyRequest,
	finish: extern "C" fn(*mut _PubkeyRequest, *const u8, usize, *const u16, usize),
}

impl SignatureRequest
{
	fn new() -> (SignatureRequest, FutureReceiver<Result<Vec<u8>, ()>>)
	{
		let (sender, receiver) = create_future();
		let req = Box::new(_SignatureRequest{answer:sender});
		(SignatureRequest{
			context: Box::into_raw(req),
			finish: signature_request_finish,
			log: signature_request_log
		}, receiver)
	}
}

#[derive(Copy,Clone)]
struct KeyHandle(u64);

struct ModuleLibraryHandle
{
	_library: LibraryHandle,
	_get_key_handle: Option<extern "C" fn(*const c_char, *mut u32) -> u64>,
	_get_key_handle2: Option<extern "C" fn(*const u8, usize, *mut u32) -> u64>,
	_request_sign: extern "C" fn(u64, SignatureRequest, u16, *const u8, usize),
	_request_pubkey: extern "C" fn(u64, PubkeyRequest),
}


impl ModuleLibraryHandle
{
	fn new(name: &str) -> Result<ModuleLibraryHandle, ModuleError>
	{
		let lib = LibraryHandle::new(name)?;
		//We need btls_mod_get_key_handle or btls_mod_get_key_handle2 or both.
		let _get_key_handle = lib.symbol2::<GetKeyHandle>();
		let _get_key_handle2 = lib.symbol2::<GetKeyHandle2>();
		fail_if!(_get_key_handle.is_none() && _get_key_handle2.is_none(), ModuleError::NotValidModule);
		let _request_sign = lib.symbol2::<RequestSign>().ok_or(ModuleError::NotValidModule)?;
		let _request_pubkey = lib.symbol2::<RequestPubkey>().ok_or(ModuleError::NotValidModule)?;
		Ok(ModuleLibraryHandle{
			_library: lib,
			_get_key_handle: _get_key_handle,
			_get_key_handle2: _get_key_handle2,
			_request_sign: _request_sign,
			_request_pubkey: _request_pubkey,
		})
	}
	fn has_get_key_handle(&self) -> bool
	{
		self._get_key_handle.is_some()
	}
	fn has_get_key_handle2(&self) -> bool
	{
		self._get_key_handle2.is_some()
	}
	fn get_key_handle(&self, name: &str, errcode: &mut u32) -> Result<KeyHandle, ()>
	{
		let n = CString::new(name).map_err(|_|())?;
		let h = (self._get_key_handle.ok_or(())?)(n.as_ptr(), errcode as *mut u32);
		Ok(KeyHandle(h))
	}
	fn get_key_handle2(&self, data: &[u8], errcode: &mut u32) -> Result<KeyHandle, ()>
	{
		let h = (self._get_key_handle2.ok_or(())?)(data.as_ptr(), data.len(), errcode as *mut u32);
		Ok(KeyHandle(h))
	}
	fn request_sign(&self, handle: KeyHandle, alg: u16, data: &[u8]) -> FutureReceiver<Result<Vec<u8>, ()>>
	{
		let _data = data.as_ptr();
		let _datalen = data.len();
		let (ctx, receiver) = SignatureRequest::new();
		(self._request_sign)(handle.0, ctx, alg, _data, _datalen);
		receiver
	}
	fn request_pubkey(&self, handle: KeyHandle) -> Result<(Vec<u8>, Vec<u16>), ()>
	{
		let mut lreq = _PubkeyRequest{called: false, pubkey:Vec::new(), schemes:Vec::new()};
		let req = PubkeyRequest{
			context: &mut lreq as *mut _PubkeyRequest,
			finish: pubkey_request_finish
		};
		(self._request_pubkey)(handle.0, req);
		if lreq.called { Ok((lreq.pubkey, lreq.schemes)) } else { Err(()) }
	}
}

#[derive(Clone)]
struct ModuleLibraryHandleA(Arc<ModuleLibraryHandle>);

impl ModuleLibraryHandleA
{
	fn new(name: &str) -> Result<ModuleLibraryHandleA, ModuleError>
	{
		Ok(ModuleLibraryHandleA(Arc::new(ModuleLibraryHandle::new(name)?)))
	}
	fn has_get_key_handle(&self) -> bool
	{
		self.0.has_get_key_handle()
	}
	fn has_get_key_handle2(&self) -> bool
	{
		self.0.has_get_key_handle2()
	}
	fn get_key_handle(&self, name: &str, errcode: &mut u32) -> Result<KeyHandle, ()>
	{
		self.0.get_key_handle(name, errcode)
	}
	fn get_key_handle2(&self, data: &[u8], errcode: &mut u32) -> Result<KeyHandle, ()>
	{
		self.0.get_key_handle2(data, errcode)
	}
	fn request_sign(&self, handle: KeyHandle, alg: u16, data: &[u8]) -> FutureReceiver<Result<Vec<u8>, ()>>
	{
		self.0.request_sign(handle, alg, data)
	}
	fn request_pubkey(&self, handle: KeyHandle) -> Result<(Vec<u8>, Vec<u16>), ()>
	{
		self.0.request_pubkey(handle)
	}
}

//Has to be public, by private in public rules.
#[derive(Clone)]
pub struct ModuleLibraryMap(Arc<RwLock<BTreeMap<String, ModuleLibraryHandleA>>>);

impl ModuleLibraryMap
{
	fn new() -> ModuleLibraryMap
	{
		ModuleLibraryMap(Arc::new(RwLock::new(BTreeMap::new())))
	}
	//Loads the module if it doesn't exist yet.
	fn lookup(&self, name: &str) -> Result<ModuleLibraryHandleA, ModuleError>
	{
		{
			let x = match self.0.read() {
				Ok(x) => x,
				Err(_) => fail!(ModuleError::CantLockModules)
			};
			match x.get(name) {
				Some(x) => return Ok(x.clone()),
				None => (),
			};
		}
		//The module is not in map, we need to add it.
		let mut x = match self.0.write() {
			Ok(x) => x,
			Err(_) => fail!(ModuleError::CantLockModules)
		};
		//Check again, race might have added it.
		match x.get(name) {
			Some(x) => return Ok(x.clone()),
			None => (),
		};
		//Nope, we need to load it.
		x.insert(name.to_owned(), ModuleLibraryHandleA::new(name)?);
		match x.get(name) {
			Some(x) => return Ok(x.clone()),
			None => fail!(ModuleError::CantLockModules)	//This should be unreachable.
		};
	}
}

fn guess_keytype(schemes: &[u16]) -> &'static str
{
	for i in schemes.iter() {
		match *i {
			0x807 => return "ed25519",
			0x808 => return "ed448",
			0x403 => return "ecdsa-p256",
			0x503 => return "ecdsa-p384",
			0x603 => return "ecdsa-p521",
			0x401 => return "rsa",
			0x501 => return "rsa",
			0x601 => return "rsa",
			0x804 => return "rsa",
			0x805 => return "rsa",
			0x806 => return "rsa",
			_ => (),
		};
	}
	"unknown"
}

//Module keys. Hide this because it is for some reason exported, even if it should not be.
#[doc(hidden)]
pub struct ModuleKey(ModuleLibraryHandleA, KeyHandle, Vec<u8>, Vec<u16>, &'static str);

impl ModuleKey
{
	fn new_inline(data: &[u8], lmap: &ModuleLibraryMap) -> Result<ModuleKey, ModuleError>
	{
		//Find the first unescaped \r or \n.
		let mut mendpos = data.len();
		let mut escape = false;
		for i in data.iter().enumerate() {
			if escape {
				escape = false;
			} else {
				escape = *i.1 == 92;
				if *i.1 == 13 || *i.1 == 10 { mendpos = i.0; break; }
			}
		}
		//mendpos is tied to length and thus can't be near overflow.
		let lfa = data.get(mendpos).map(|x|*x);
		let lfb = data.get(mendpos+1).map(|x|*x);
		let dpos = if lfa == Some(10) {
			mendpos + 1
		} else if lfa == Some(13) && lfb != Some(10) {
			mendpos + 1
		} else if lfa == Some(13) && lfb == Some(10) {
			mendpos + 2
		} else {
			fail!(ModuleError::MalformedReference);
		};
		fail_if!(dpos > data.len(), ModuleError::MalformedReference);
		let keydata = &data[dpos..];
		let lname = &data[..mendpos];	//mendpos is smaller than dpos.
		let lname = from_utf8(lname).map_err(|_|ModuleError::MalformedReference)?;
		let mut libname = String::new();
		escape = false;
		for i in lname.chars() {
			if escape {
				libname.push(i);
				escape = false;
			} else if i == '\\' {
				escape = true;
			} else {
				libname.push(i);
			}
		}
		let modulelib = lmap.lookup(&libname)?;
		let mut errcode = 0u32;
		fail_if!(!modulelib.has_get_key_handle2(), ModuleError::InLineRefsNotSupported);
		let handle = modulelib.get_key_handle2(keydata, &mut errcode).map_err(|_|
			ModuleError::NoSuchKey(0xFFFFFFFF))?;
		let (pubkey, schemes) = modulelib.request_pubkey(handle).map_err(|_|ModuleError::NoSuchKey(
			errcode))?;
		let keytype = guess_keytype(&schemes);
		Ok(ModuleKey(modulelib, handle, pubkey, schemes, keytype))
	}
	fn new_out_of_line(data: &[u8], lmap: &ModuleLibraryMap) -> Result<ModuleKey, ModuleError>
	{
		let mut libname = String::new();
		let mut keyname = String::new();
		let mut escape = false;
		let mut component = false;
		let mut end = false;
		let data = from_utf8(data).map_err(|_|ModuleError::MalformedReference)?;
		for i in data.chars() {
			fail_if!(end && i != '\r' && i != '\n', ModuleError::MalformedReference);
			let c = if component { &mut keyname } else { &mut libname };
			if escape {
				c.push(i);
				escape = false;
			} else {
				if i == '\r' {
					end = true;
				} else if i == '\n' {
					end = true;
				} else if i == '\\' {
					escape = true;
				} else if i == ' ' {
					//Expect 2 components.
					fail_if!(component, ModuleError::MalformedReference);
					component = true;
				} else {
					c.push(i);
				}
			}
		}
		fail_if!(keyname == "", ModuleError::MalformedReference);
		let modulelib = lmap.lookup(&libname)?;
		let mut errcode = 0u32;
		fail_if!(!modulelib.has_get_key_handle(), ModuleError::OutOfLineRefsNotSupported);
		let handle = modulelib.get_key_handle(&keyname, &mut errcode).map_err(|_|
			ModuleError::NoSuchKey(0xFFFFFFFF))?;
		let (pubkey, schemes) = modulelib.request_pubkey(handle).map_err(|_|ModuleError::NoSuchKey(
			errcode))?;
		let keytype = guess_keytype(&schemes);
		Ok(ModuleKey(modulelib, handle, pubkey, schemes, keytype))
	}
	///Create a new module key.
	pub fn new(data: &[u8]) -> Result<ModuleKey, ModuleError>
	{
		let module_map = get_global_modmap();
		//Check if the reference is inline or out-of-line. If first unescaped space is \r or \n, it is
		//in-line, If it is ' ' it is out-of-line.
		let mut is_inline = None;
		let mut escape = false;
		for i in data.iter() {
			if escape {
				escape = false;
			} else {
				escape = *i == 92;
				if *i == 13 || *i == 10 { is_inline = Some(true); break; }
				if *i == 32 { is_inline = Some(false); break; }
			}
		}
		match is_inline {
			Some(false) => Self::new_out_of_line(data, module_map),
			Some(true) => Self::new_inline(data, module_map),
			None => fail!(ModuleError::MalformedReference)
		}
	}
	///Sign with key.
	pub fn sign(&self, data: &[u8], algorithm: u16) -> FutureReceiver<Result<Vec<u8>, ()>>
	{
		self.0.request_sign(self.1, algorithm, data)
	}
	///Get list of schemes suppored.
	pub fn get_schemes(&self) -> Vec<u16>
	{
		self.3.clone()
	}
	///Get the public key as X.509 SPKI structure.
	pub fn get_public_key(&self) -> Vec<u8>
	{
		self.2.clone()
	}
	///Get the key type as string.
	pub fn get_key_type(&self) -> &'static str
	{
		self.4
	}
}
