//Dummy implementation of module key.
//
//This module provodes dummy implementations of module key interfaces, in order to use the same interfaces when
//module keys are supported as for not supported case.
use std::fmt::{Display, Error as FmtError, Formatter};

use btls_aux_futures::FutureReceiver;

//Error from module. Hide this because it isn't reachable from anything documented.
#[doc(hidden)]
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum ModuleError
{
	#[doc(hidden)]
	NotSupported,
	#[doc(hidden)]
	Hidden__
}

impl Display for ModuleError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::ModuleError::*;
		match self {
			&NotSupported => fmt.write_str("Module keys are not supported"),
			&Hidden__ => fmt.write_str("Hidden__")
		}
	}
}

//Dummy implementation of module key.
#[doc(hidden)]
pub struct ModuleKey(());

impl ModuleKey
{
	///This always fails.
	pub fn new(_data: &[u8]) -> Result<ModuleKey, ModuleError>
	{
		Err(ModuleError::NotSupported)
	}
	//No need for more elaborate definitions. ModuleKey is assumed unconstructable.
	pub fn sign(&self, _data: &[u8], _algorithm: u16) -> FutureReceiver<Result<Vec<u8>, ()>>
	{
		unreachable!();
	}
	pub fn get_schemes(&self) -> Vec<u16>
	{
		unreachable!();
	}
	pub fn get_public_key(&self) -> Vec<u8>
	{
		unreachable!();
	}
	pub fn get_key_type(&self) -> &'static str
	{
		unreachable!();
	}
}
