extern crate gcc;
use gcc::Config;
use std::panic::catch_unwind;

fn main()
{
	{
		//Try general nettle test.
		let mut config = Config::new();
		config.file("nettle-test.c");
		config.cargo_metadata(false);	//We do not need this!
		config.compile("libnettle-test.a");
	}
	//The UnwindSafe is retarded.
	let _ = catch_unwind(||{
		let mut config = Config::new();
		config.file("nettle-test-sha3.c");
		config.cargo_metadata(false);	//We do not need this!
		config.compile("libnettle-test-sha3.a");
		println!("cargo:rustc-cfg=nettle_sha3");
	});
	let _ = catch_unwind(||{
		let mut config = Config::new();
		config.file("nettle-test-rsa-tr.c");
		config.cargo_metadata(false);	//We do not need this!
		config.compile("libnettle-test-rsa-tr.a");
		println!("cargo:rustc-cfg=have_rsa_compute_root_tr");
	});
}
