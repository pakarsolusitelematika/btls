//!RSA signing
//!
//!This crate provodes routines for RSA signing.
#![forbid(unsafe_code)]
#![forbid(missing_docs)]

#[macro_use]
extern crate btls_aux_fail;
extern crate btls_aux_hash;
extern crate btls_aux_random;
extern crate btls_aux_keyconvert;
extern crate btls_aux_nettle;
extern crate btls_aux_securebuf;
extern crate btls_aux_serialization;
extern crate btls_aux_signature_algo;
extern crate btls_aux_signature_rsa;

use btls_aux_hash::{HashFunction, MAX_HASH_OUTPUT};
use btls_aux_random::secure_random;
use btls_aux_keyconvert::RsaKeyDecode;
use btls_aux_nettle::{generate_rsa_key, rsa_compute_root, RsaPrivate, RsaPublic};
use btls_aux_signature_algo::{MajorSignatureAlgorithm, SignatureKeyPair, SignatureType};
use btls_aux_securebuf::SecStackBuffer;
use btls_aux_serialization::{ASN1_OCTET_STRING, ASN1_SEQUENCE, Sink, SliceSink};
use std::cmp::min;
use std::ops::{Deref, DerefMut};
use std::fmt::{Display, Error as FmtError, Formatter};


///Error for RSA key pair
///
///This enumeration describes error arising in loading RSA key pair.
#[derive(Clone,Debug,PartialEq,Eq)]
pub enum RsaKeyLoadingError
{
	#[doc(hidden)]
	BadRsaKeySize(u64),
	#[doc(hidden)]
	CantConstructRsaSigningState,
	#[doc(hidden)]
	CantReadRsa,
	#[doc(hidden)]
	CantSerializeRsaKey,
	#[doc(hidden)]
	InvalidRsaE,
	#[doc(hidden)]
	InvalidRsaKeypair,
	#[doc(hidden)]
	UnknownPrivateKeyVersion,
	#[doc(hidden)]
	Hidden__
}

impl Display for RsaKeyLoadingError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use RsaKeyLoadingError::*;
		match self {
			&InvalidRsaE => fmt.write_str("RSA e is invalid"),
			&BadRsaKeySize(x) => fmt.write_fmt(format_args!("Unsupported RSA key bitlength {} (needs \
				to be 2048-4096 in multiples of 8)", x)),
			&UnknownPrivateKeyVersion => fmt.write_str("Unknown private key version"),
			&CantConstructRsaSigningState => fmt.write_str("Can't construct RSA signing state"),
			&InvalidRsaKeypair => fmt.write_str("Invalid RSA keypair"),
			&CantSerializeRsaKey => fmt.write_str("Can't serialize RSA key"),
			&CantReadRsa => fmt.write_str("Can't read RSA key"),
			&Hidden__ => fmt.write_str("Hidden__")
		}
	}
}

///A RSA key pair.
///
///This structure is an RSA key pair.
///
///The key generation parameter is the number of bits (2048-4096).
pub struct RsaKeyPair(RsaPrivate, RsaPublic, usize, u32);

impl RsaKeyPair
{
	///Deprecated, use `.SignatureKeyPair::keypair_sign()` instead.
	pub fn sign(&self, message: &[u8], alg: u16) -> Result<Vec<u8>, ()>
	{
		let mut tmp = [0; 520];	//Enough, since keyas are maximum 4096 bytes.
		match self.keypair_sign(message, alg, &mut tmp) {
			Ok(size) => {
				if size <= tmp.len() {
					Ok((&tmp[..size]).to_owned())
				} else {
					Err(())
				}
			}
			Err(_) => Err(())
		}
	}
	///Deprecated, use `.SignatureKeyPair::keypair_load()` instead.
	pub fn from_bytes(data: &[u8]) -> Result<(RsaKeyPair, Vec<u8>), RsaKeyLoadingError>
	{
		match RsaKeyPair::keypair_load(data) {
			Ok((_, x, y)) => Ok((x, y)),
			Err(x) => Err(x)
		}
	}
	///Deprecated, use `.SignatureKeyPair::keypair_generate()` instead.
	pub fn generate(bits: usize) -> Result<Vec<u8>, ()>
	{
		RsaKeyPair::keypair_generate(bits)
	}
}

impl SignatureKeyPair for RsaKeyPair
{
	type KeygenParameters = usize;
	type BadKeyError = RsaKeyLoadingError;
	type Variant = ();
	fn keypair_generate(bits: usize) -> Result<Vec<u8>, ()>
	{
		fail_if!(bits < 2048 || bits > 4096, ());
		//The biggest is 4096 bit key, so fits in 3072 bytes.
		let mut output = [0; 3072];
		let mut output = SecStackBuffer::new(&mut output, 3072).unwrap();	//Always fits.
		let rep_bytes = generate_rsa_key(bits, output.deref_mut())?;
		Ok((&(output.deref())[..rep_bytes]).to_owned())
	}
	fn keypair_load(data: &[u8]) -> Result<((), RsaKeyPair, Vec<u8>), RsaKeyLoadingError>
	{
		let p = RsaKeyDecode::new(data).map_err(|_|RsaKeyLoadingError::CantReadRsa)?;
		load_rsa_key(p.n, p.e, p.d, p.p, p.q, p.dp, p.dq, p.qi)
	}
	fn keypair_sign(&self, message: &[u8], scheme: u16, output: &mut [u8]) -> Result<usize, ()>
	{
		static BLANK: [u8;0] = [];
		let (pss, hashfunc, hashid) = match SignatureType::by_tls_id(scheme).map(|x|x.get_major_type()) {
			Some(MajorSignatureAlgorithm::RsaPkcs1(hash, oid, _, _)) => (false, hash, oid),
			Some(MajorSignatureAlgorithm::RsaPss(hash)) => (true, hash, &BLANK[..]),
			_ => fail!(())
		};
		let mut raw = [0;768];
		fail_if!(self.2 > raw.len(), ());
		fail_if!(self.2 > output.len(), ());
		let raw = &mut raw[..self.2];
		let output = &mut output[..self.2];
		if pss {
			let slen = hashfunc.output_length();
			let mut salt = [0;MAX_HASH_OUTPUT];
			fail_if!(slen > salt.len(), ());
			let salt = &mut salt[..slen];
			secure_random(salt);
			rsa_pad_pss(raw, message, salt, hashfunc, self.3)?;
		} else {
			rsa_pad_pkcs1_v1_5(raw, hashid, message, hashfunc)?;
		}
		rsa_compute_root(&self.1, &self.0, output, raw)?;
		Ok(output.len())
	}
}

fn load_rsa_key(n: &[u8], e: &[u8], d: &[u8], p: &[u8], q: &[u8], dp: &[u8], dq: &[u8], qi: &[u8]) ->
	Result<((), RsaKeyPair, Vec<u8>), RsaKeyLoadingError>
{
	use RsaKeyLoadingError::*;

	let trimbits = match n.get(0).map(|x|*x).unwrap_or(0) {
		0...1 => 8,
		2...3 => 7,
		4...7 => 6,
		8...15 => 5,
		16...31 => 4,
		32...63 => 3,
		64...127 => 2,
		_ => 1
	};
	
	//Checks.
	//The exponent must satisfy the following:
	//Must be at least 1 byte, Can't be longer than n.
	//Must be > 1.
	//Must be odd.
	fail_if!(e.len() > n.len(), InvalidRsaE);
	fail_if!(e.len() < 1 || e[0] == 0, InvalidRsaE);
	fail_if!(e.len() == 1 && e[0] == 1, InvalidRsaE);
	fail_if!(e[e.len()-1] & 1 == 0, InvalidRsaE);
	if n.len() < 256 || n.len() > 512 || n[0] < 128 {
		fail!(BadRsaKeySize(8 * (n.len() as u64) - (trimbits as u64) + 1));
	}

	//Construct headers.
	let mut publicparams = RsaPublic::new();
	let mut privateparams = RsaPrivate::new();
	publicparams.load(n, e).map_err(|_|InvalidRsaKeypair)?;
	privateparams.load(d, p, q, dp, dq, qi).map_err(|_|InvalidRsaKeypair)?;
	let pubkey = publicparams.as_rsa_spki().map_err(|_|CantSerializeRsaKey)?;
	
	Ok(((), RsaKeyPair(privateparams, publicparams, n.len(), trimbits), pubkey))
}

//RSA PKCS#1 v1.5 padding.
fn rsa_pad_pkcs1_v1_5(output: &mut [u8], hashid: &[u8], message: &[u8], h: HashFunction) -> Result<(), ()>
{
	let mut hctx = h.make_context();
	hctx.input(message);
	let hash = hctx.output().map_err(|_|())?;
	//Serialize the digestinfo, and move it to the end.
	//SEQUENCE { hashid OCTET STRING { hash }
	let dinfo_len = {
		let mut out = SliceSink::new(output);
		out.asn1_fn(ASN1_SEQUENCE, |x|{
			x.asn1_fn(ASN1_SEQUENCE, |y|{
				y.write_slice(hashid)
			})?;
			x.asn1_fn(ASN1_OCTET_STRING, |y|{
				y.write_slice(hash.as_ref())
			})
		})?;
		out.written()
	};
	//dinfo_len <= output.len(). Calculate prefix length and copy forward.
	let prefix_len = output.len().saturating_sub(dinfo_len);
	fail_if!(prefix_len < 11, ());		//RSA PKCS#1 v1.5 needs at least 11 byte prefix.
	for i in (0..dinfo_len).rev() {
		//prefix_len + i < prefix_len + dinfo_len = output.len(), so in-range.
		output[prefix_len + i] = output[i];
	}
	//Write the prefix.
	//00 01 n(>=8)*FF 00
	output[0] = 0;
	output[1] = 1;
	for i in 2..(prefix_len-1) { output[i] = 0xFF; }
	output[prefix_len-1] = 0;
	//Okay, done.
	Ok(())
}

//RSA PSS padding. trimbits is at most 8.
fn rsa_pad_pss(output: &mut [u8], message: &[u8], salt: &[u8], h: HashFunction, trimbits: u32) -> Result<(), ()>
{
	let mut hctx = h.make_context();
	hctx.input(message);
	let hash = hctx.output().map_err(|_|())?;
	fail_if!(trimbits > 8, ());
	fail_if!(hash.as_ref().len() != h.output_length(), ());
	let slen = salt.len();
	let hlen = h.output_length();
	let olen = output.len();
	fail_if!(output.len() < hlen.saturating_add(slen).saturating_add(2), ());

	//The padding octets. We checked that hlen + slen + 2 <= output.len().
	output[olen - hlen - slen - 2] = 1;
	output[olen - 1] = 0xbc;

	//Copy salt to its place. We checked that hlen + slen + 1 < output.len().
	let sstart = olen - hlen - slen - 1;
	let send = olen - hlen - 1;
	(&mut output[sstart..send]).copy_from_slice(salt.deref());	//send-sstart == salt.deref().len().

	//H, copy that as last bytes (minus one) of output. We checked hlen + 1 < output.len().
	hctx.reset();
	hctx.input(&[0,0,0,0,0,0,0,0]);
	hctx.input(hash.as_ref());
	hctx.input(salt);
	let hval = hctx.output().map_err(|_|())?;
	fail_if!(hval.as_ref().len() != hlen, ());		//Bad length.
	let hstart = send;
	let hend = olen-1;
	(&mut output[hstart..hend]).copy_from_slice(hval.as_ref());	//hend-hstart == hval.as_ref().len().

	//Now we have:
	//|--- zeroes ---| 01 |--- salt ---| |--- H ---| BC
	//We need to mask the part up to H with MGF, using H as seed.
	let mut maskfragstart = 0;
	let mut idx = 0;
	while maskfragstart < hstart {
		let counter = [(idx >> 24) as u8, (idx >> 16) as u8, (idx >> 8) as u8, idx as u8];
		hctx.reset();
		hctx.input(&output[hstart..hend]);
		hctx.input(&counter);
		let maskfrag = hctx.output().map_err(|_|())?;
		let maskfrag = maskfrag.as_ref();
		//Only pad up to hstart.
		let maskfrag = &maskfrag[..min(maskfrag.len(), hstart.saturating_sub(maskfragstart))];
		for i in maskfrag.iter().enumerate() {
			output[maskfragstart + i.0] ^= *i.1;
		}
		maskfragstart = maskfragstart.saturating_add(maskfrag.as_ref().len());
		idx += 1;
	}
	//Zero some leading bits.
	output[0] &= ((1 << (8 - trimbits)) - 1) as u8;
	//Ok.
	Ok(())
}

#[cfg(test)]
fn do_test(tlsid: u16)
{
	use self::btls_aux_keyconvert::convert_key_from_sexpr;
	use self::btls_aux_signature_algo::{SignatureAlgorithm, SignatureType, WrappedRecognizedSignatureAlgorithm};
	use self::btls_aux_signature_rsa::rsa_verify_pkix;
	let key_bytes = include_bytes!("testrsa.sexp");
	let (_, key_bytes) = convert_key_from_sexpr(&key_bytes[..]).unwrap();
	let (keypair, pubkey) = RsaKeyPair::from_bytes(&key_bytes).unwrap();
	let msg = "Hello, World!";
	let signature = keypair.sign(msg.as_bytes(), tlsid).unwrap();
	let algo = SignatureType::by_tls_id(tlsid).unwrap();
	let algo = WrappedRecognizedSignatureAlgorithm::from(SignatureAlgorithm::Tls(algo)).unwrap();
	rsa_verify_pkix(&pubkey, msg.as_bytes(), &algo, &signature, 0).unwrap();
}


#[test]
fn sign_rsa_pkcs1_sha256()
{
	do_test(0x401);
}

#[test]
fn sign_rsa_pkcs1_sha384()
{
	do_test(0x501);
}

#[test]
fn sign_rsa_pkcs1_sha512()
{
	do_test(0x601);
}

#[test]
fn sign_rsa_pss_sha256()
{
	do_test(0x804);
}

#[test]
fn sign_rsa_pss_sha384()
{
	do_test(0x805);
}

#[test]
fn sign_rsa_pss_sha512()
{
	do_test(0x806);
}
