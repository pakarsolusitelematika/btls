//!Ed448 signing
//!
//!This crate provodes routines for Ed448 signing.
#![forbid(unsafe_code)]
#![forbid(missing_docs)]
#[macro_use]
extern crate btls_aux_fail;
extern crate btls_aux_keyconvert;
extern crate btls_aux_random;
extern crate btls_aux_securebuf;
extern crate btls_aux_signature_algo;
extern crate btls_aux_xed448;
use btls_aux_keyconvert::Ed448KeyDecode;
use btls_aux_random::secure_random;
use btls_aux_securebuf::wipe_buffer;
use btls_aux_signature_algo::SignatureKeyPair;
use btls_aux_xed448::{ed448_pubkey, ed448_sign};

use std::fmt::{Display, Error as FmtError, Formatter};


///Error in loading Ed448 key pair
///
///This enumeration describes the error that occured in loading of Ed448 key pair.
#[derive(Clone,Debug,PartialEq,Eq)]
pub enum Ed448KeyLoadingError
{
	#[doc(hidden)]
	InvalidEd448DataLength(usize),
	#[doc(hidden)]
	InvalidEd448Keypair,
	#[doc(hidden)]
	Hidden__
}

impl Display for Ed448KeyLoadingError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use Ed448KeyLoadingError::*;
		match self {
			&InvalidEd448DataLength(x) => fmt.write_fmt(format_args!("Invalid Ed448 key length \
				{} (expected 114)", x)),
			&InvalidEd448Keypair => fmt.write_str("Inconsistent public/private keys in Ed448 key"),
			&Hidden__ => fmt.write_str("Hidden__")
		}
	}
}

///Ed448 keypair.
///
///This structure describes an Ed448 key pair.
pub struct Ed448KeyPair
{
	sk: [u8;57],
	pk: [u8;57],
}

impl Drop for Ed448KeyPair
{
	fn drop(&mut self)
	{
		wipe_buffer(&mut self.sk);
	}
}

const EDDSA_TLS_ID: u16 = 0x808;

impl Ed448KeyPair
{
	///Deprecated, use `.SignatureKeyPair::keypair_sign()` instead.
	pub fn sign(&self, message: &[u8], output: &mut [u8]) -> Result<(), ()>
	{
		fail_if!(output.len() != 114, ());
		self.keypair_sign(message, EDDSA_TLS_ID, output)?; 
		Ok(())
	}
	///Deprecated, use `.SignatureKeyPair::keypair_sign()` instead.
	pub fn sign2(&self, message: &[u8], output: &mut [u8], algo: u16) -> Result<usize, ()>
	{
		self.keypair_sign(message, algo, output)
	}
	///Deprecated, use `.SignatureKeyPair::keypair_load()` instead.
	pub fn from_bytes(data: &[u8]) -> Result<(Ed448KeyPair, Vec<u8>), Ed448KeyLoadingError>
	{
		match Ed448KeyPair::keypair_load(data) {
			Ok((_, x, y)) => Ok((x, y)),
			Err(x) => Err(x)
		}
	}
	///Deprecated, use `.SignatureKeyPair::keypair_generate()` instead.
	pub fn generate() -> Result<Vec<u8>, ()>
	{
		Ed448KeyPair::keypair_generate(())
	}
}

impl SignatureKeyPair for Ed448KeyPair
{
	type KeygenParameters = ();
	type BadKeyError = Ed448KeyLoadingError;
	type Variant = ();
	fn keypair_generate(_params: ()) -> Result<Vec<u8>, ()>
	{
		const SIZE: usize = 57;
		let mut privkey = [0; SIZE];
		let mut pubkey = [0; SIZE];
		let mut target = Vec::with_capacity(2*SIZE);
		target.resize(2*SIZE, 0);
		secure_random(&mut privkey);
		ed448_pubkey(&mut pubkey, &privkey);
		(&mut target[..SIZE]).copy_from_slice(&privkey);
		(&mut target[SIZE..]).copy_from_slice(&pubkey);
		wipe_buffer(&mut privkey);
		Ok(target)
	}
	fn keypair_load(data: &[u8]) -> Result<((), Ed448KeyPair, Vec<u8>), Ed448KeyLoadingError>
	{
		use Ed448KeyLoadingError::*;
		let keypair = Ed448KeyDecode::new(data).map_err(|_|InvalidEd448DataLength(data.len()))?;

		//Check the private and public keys correspond.
		let mut tmp = [0;57];
		let mut tmp2 = [0;57];
		fail_if!(keypair.private.len() != tmp.len(), InvalidEd448Keypair);	//Should be the same size.
		tmp.copy_from_slice(keypair.private);
		ed448_pubkey(&mut tmp2, &tmp);
		fail_if!(&tmp2[..] != keypair.public, InvalidEd448Keypair);

		//Serialize to internal representation and SPKI public key.
		let output = Ed448KeyPair{sk: tmp, pk: tmp2};
		wipe_buffer(&mut tmp);
		let ed448_header = b"\x30\x43\x30\x05\x06\x03\x2B\x65\x71\x03\x3A\x00";
		let mut out = Vec::new();
		out.extend_from_slice(ed448_header);
		out.extend_from_slice(&keypair.public);
		Ok(((), output, out))
	}
	fn keypair_sign(&self, message: &[u8], scheme: u16, output: &mut [u8]) -> Result<usize, ()>
	{
		fail_if!(scheme != EDDSA_TLS_ID, ());
		let mut signature = [0;114];
		fail_if!(signature.len() > output.len(), ());
		ed448_sign(&self.sk, &self.pk, message, &mut signature);
		(&mut output[..signature.len()]).copy_from_slice(&signature);
		Ok(signature.len())
	}
}
