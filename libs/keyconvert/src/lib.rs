//!Conversion of private keys from variety of formats into canonical standard form.
//!
//!This crate provodes routines for converting keypairs from a variety of formats into one internal format.
#![forbid(unsafe_code)]
#![forbid(missing_docs)]

use std::borrow::Cow;
use std::collections::BTreeMap;
use std::fmt::{Display, Error as FmtError, Formatter};
use std::io::{Result as IoResult, Write as IoWrite};
use std::str::from_utf8;

extern crate btls_aux_serialization;
use btls_aux_serialization::{is_single_asn1_structure, Sink, Source};
extern crate btls_aux_json;
use btls_aux_json::{CborNode, JsonNode};
extern crate btls_aux_sexp;
use btls_aux_sexp::SexprParseStream;
#[macro_use]
extern crate btls_aux_fail;

//We can't include signature-algo, due to cyclic dependency.
static SIGN_ECDSA_P256_SCHEMES: [u16;1] = [0x403];
static SIGN_ECDSA_P384_SCHEMES: [u16;1] = [0x503];
static SIGN_ECDSA_P521_SCHEMES: [u16;1] = [0x603];


///RSA key.
///
///This constant acts as an identifier for RSA keys.
///
///The internal format is as follows, in order:
///
/// * 2 bytes: Big-endian number of bytes in n (nlen).
/// * nlen bytes: Big-endian RSA n.
/// * nlen bytes: Big-endian RSA d.
/// * (nlen+1)/2 bytes: Big-endian RSA p
/// * (nlen+1)/2 bytes: Big-endian RSA q
/// * (nlen+1)/2 bytes: Big-endian RSA dp
/// * (nlen+1)/2 bytes: Big-endian RSA dq
/// * (nlen+1)/2 bytes: Big-endian RSA qi
/// * (remainder): Big endian RSA e.
pub const KEYFORMAT_RSA: usize = 0;
///ECDSA key.
///
///This constant acts as an identifier for ECDSA keys.
///
///The internal format is as follows, in order:
///
/// * 1 byte: Variant identifier (0 for NSA P-256, P-384 and P-521).
/// * c bytes: Big endian ECDSA d.
/// * c bytes: Big endian ECDSA x.
/// * c bytes: Big endian ECDSA y.
///
///Where c is 32 for P-256, 48 for P-384 and 66 for P-521.
pub const KEYFORMAT_ECDSA: usize = 1;
///Ed25519 key.
///
///This constant acts as an identifier for Ed25519 keys.
///
///The internal format is as follows, in order:
/// 
/// * 32 bytes: The private key.
/// * 32 bytes: The public key.
pub const KEYFORMAT_ED25519: usize = 2;
///Ed448 key.
///
///This constant acts as an identifier for Ed448 keys.
///
///The internal format is as follows, in order:
/// 
/// * 57 bytes: The private key.
/// * 57 bytes: The public key.
pub const KEYFORMAT_ED448: usize = 3;

const NSA_P256_PART_LEN: usize = 32;
const NSA_P384_PART_LEN: usize = 48;
const NSA_P521_PART_LEN: usize = 66;
const NSA_P256_SEXP_ID: &'static str = "P256";
const NSA_P384_SEXP_ID: &'static str = "P384";
const NSA_P521_SEXP_ID: &'static str = "P521";
const NSA_P256_JWK_ID: &'static str = "P-256";
const NSA_P384_JWK_ID: &'static str = "P-384";
const NSA_P521_JWK_ID: &'static str = "P-521";

///Sink that just discards all data.
///
///This structure implements `std::io::Write`, just discarding all the data written.
///
///This is useful to act as sink when some side information instead of the output matters.
pub struct DevNull;

impl IoWrite for DevNull
{
	fn write(&mut self, buf: &[u8]) -> IoResult<usize> { Ok(buf.len()) }
	fn flush(&mut self) -> IoResult<()> { Ok(()) }
}

///BASE64 decoder.
///
///This structure is a BASE64 decoder (note, not base64url).
pub struct Base64Decoder
{
	state: u32,
}

///Error in decoding.
///
///This enumeration contains the possible errors in decoding a keypair.
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub enum DecodingError
{
	#[doc(hidden)]
	InvalidBase64,
	#[doc(hidden)]
	NotValidUtf8,
	#[doc(hidden)]
	UnknownKeyType,
	#[doc(hidden)]
	RsaKeyTooSmall,
	#[doc(hidden)]
	RsaKeyBadSize,
	#[doc(hidden)]
	CantSerializeRsaKey,
	#[doc(hidden)]
	RsaElementsTooLarge,
	#[doc(hidden)]
	CantSerializeEcdsaKey,
	#[doc(hidden)]
	CwkInvalidCbor,
	#[doc(hidden)]
	JwkInvalidJson,
	#[doc(hidden)]
	NoKeyType,
	#[doc(hidden)]
	NoCurve,
	#[doc(hidden)]
	UnknownCurve,
	#[doc(hidden)]
	BadEd25519d,
	#[doc(hidden)]
	BadEd25519x,
	#[doc(hidden)]
	BadEd448d,
	#[doc(hidden)]
	BadEd448x,
	#[doc(hidden)]
	WkNoOkpSubtype,
	#[doc(hidden)]
	WkUnknownOkpSubtype,
	#[doc(hidden)]
	WkToplevelNotDictionary,
	#[doc(hidden)]
	SexpUnknownKeyType,
	#[doc(hidden)]
	SexpListTruncated,
	#[doc(hidden)]
	SexpListTooLong,
	#[doc(hidden)]
	WkNoParameter(&'static str, &'static str),
	#[doc(hidden)]
	WkBadParameter(&'static str, &'static str),
	#[doc(hidden)]
	IoError,
	#[doc(hidden)]
	ElementNotAsn1,
	#[doc(hidden)]
	ExpectedOne,
}

impl Display for DecodingError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::DecodingError::*;
		match self {
			&InvalidBase64 => fmt.write_str("Invalid Base64 encoding"),
			&NotValidUtf8 => fmt.write_str("Not valid UTF-8"),
			&RsaKeyTooSmall => fmt.write_str("RSA key needs to be 2048-4096 bits"),
			&RsaKeyBadSize => fmt.write_str("RSA key not multiple of 8 bits"),
			&CantSerializeRsaKey => fmt.write_str("Can't serialize RSA key"),
			&CantSerializeEcdsaKey => fmt.write_str("Can't serialize ECDSA key"),
			&RsaElementsTooLarge => fmt.write_str("An RSA element exceeds maximum size"),
			&CwkInvalidCbor => fmt.write_str("supposed CWK contains invalid CBOR"),
			&JwkInvalidJson => fmt.write_str("supposed JWK contains invalid JSON"),
			&NoKeyType => fmt.write_str("No key type in keypair"),
			&NoCurve => fmt.write_str("No curve in EC keypair"),
			&UnknownCurve => fmt.write_str("Unknown curve in EC key"),
			&BadEd25519d => fmt.write_str("Invalid length of d in Ed25519"),
			&BadEd25519x => fmt.write_str("Invalid length of x in Ed25519"),
			&BadEd448d => fmt.write_str("Invalid length of d in Ed448"),
			&BadEd448x => fmt.write_str("Invalid length of x in Ed448"),
			&WkNoOkpSubtype => fmt.write_str("CWK/JWK no OKP subtype"),
			&WkUnknownOkpSubtype => fmt.write_str("CWK/JWK unknown OKP subtype"),
			&UnknownKeyType => fmt.write_str("Unknown key type for keypair"),
			&WkToplevelNotDictionary => fmt.write_str("supposed CWK/JWK toplevel is not a dictionary"),
			&SexpUnknownKeyType => fmt.write_str("Unknown key type in S-Exp"),
			&SexpListTruncated => fmt.write_str("Not enough fields for key type in S-Exp"),
			&SexpListTooLong => fmt.write_str("Too many fields for key type in S-Exp"),
			&IoError => fmt.write_str("I/O error"),
			&WkNoParameter(x, y) => fmt.write_fmt(format_args!("CWK/JWK required parameter {} missing \
				in type {} key", x, y)),
			&WkBadParameter(x, y) => fmt.write_fmt(format_args!("CWK/JWK required parameter {} bad in \
				type {} key", x, y)),
			&ElementNotAsn1 => fmt.write_str("PEM element is not single ASN.1 value"),
			&ExpectedOne => fmt.write_str("Expected exactly one PEM element"),
		}
	}
}

impl Base64Decoder
{
	///Create a new BASE64 decoder.
	///
	///This method returns a new BASE64 decoder.
	pub fn new() -> Base64Decoder
	{
		Base64Decoder{state: 0}
	}
	///Decode BASE64 data.
	///
	///Takes in:
	///
	/// * A BASE64 decoder `self`.
	/// * Base64 encoded input `i`.
	/// * An output stream `output`.
	///
	///This method decodes the BASE64 input, writing the decode output the stream. The state is saved between
	///the calls into the encoder.
	///
	///The output runs at most 3 bytes behind the input.
	///
	///On success, returns `Ok(())`.
	///
	///On failure, returns `Err(err)`, where `err` describes the error that happened.
	pub fn data<Output:IoWrite>(&mut self, i: &str, output: &mut Output) -> Result<(), DecodingError>
	{
		use self::DecodingError::*;
		for j in i.chars() {
			if j == ' ' || j == '\t' { continue; }
			let x = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".find(j).
				unwrap_or(255);
			self.state = {
				let val = self.state & 0xFFFFFF;
				let pol = self.state >> 24;
				let mut seen_pad = (self.state >> 26) != 0;
				//Too much padding, or non-= after =, or invalid character.
				fail_if!(pol == 4 || (pol > 4 && x != 64) || x > 64, InvalidBase64);
				if x == 64 {
					if !seen_pad {
						if pol == 2 {
							let buf = [(val >> 4) as u8];
							output.write_all(&buf).map_err(|_|IoError)?;
						} else if pol == 3 {
							let buf = [(val >> 10) as u8, (val >> 2) as u8];
							output.write_all(&buf).map_err(|_|IoError)?;
						}
					}
					seen_pad = true;
				}				//Padding starts.
				let val = val << 6 | x as u32;
				let pol = pol.wrapping_add(1) & 3;
				if pol == 0 && !seen_pad {
					let buf = [(val >> 16) as u8, (val >> 8) as u8, val as u8];
					output.write_all(&buf).map_err(|_|IoError)?;
				}
				(seen_pad as u32) << 26 | pol << 24 | (val & 0xFFFFFF)
			};
		}
		Ok(())
	}
	///Decode end of BASE64 data.
	///
	///Takes in:
	///
	/// * A BASE64 decoder `self`.
	/// * An output stream `output`.
	///
	///This method decodes the end of BASE64 data, writing the remainder of the output into the stream.
	///
	///This produces at most 2 bytes of output.
	///
	///On success, returns `Ok(())`.
	///
	///On failure, returns `Err(err)`, where `err` describes the error that happened.
	pub fn end<Output:IoWrite>(&self, output: &mut Output) -> Result<(), DecodingError>
	{
		use self::DecodingError::*;
		let val = self.state & 0xFFFFFF;
		let pol = self.state >> 24;
		//1 mod 4 is not valid size. And if =s are present, there must be correct amount.
		fail_if!(pol == 1 || pol > 4, InvalidBase64);
		if pol == 2 {
			let buf = [(val >> 4) as u8];
			output.write_all(&buf).map_err(|_|IoError)?;
		} else if pol == 3 {
			let buf = [(val >> 10) as u8, (val >> 2) as u8];
			output.write_all(&buf).map_err(|_|IoError)?;
		}
		Ok(())
	}
}

///Determine if data looks like PEM textual encoding of given kind of object.
///
///Takes in:
///
/// * A candidate PEM textual encoding `data`.
/// * The kind of object to look for `kind`.
///
///This function determines if the candidate encoding looks to contain encoding of object of the specified kind.
///
///More specifically, the object matches kind if the end of the BEGIN/END lines minus the five trailing dashes
///matches. E.g. " FOOBAR" matches -----BEGIN RSA FOOBAR-----, -----BEGIN FOOBAR-----  and -----BEGIN EC FOOBAR-----.
///
///If the data looks to have object or objects of specified type, returns true, otherwise returns false.
pub fn looks_like_pem(data: &[u8], kind: &str) -> bool
{
	//This is supposed to be UTF-8.
	let data = match from_utf8(data) { Ok(x) => x, Err(_) => return false };
	let mut found_something = false;
	let mut open_label = None;
	let mut validator = Base64Decoder::new();
	for i in data.lines() {
		if i.starts_with("-----BEGIN ") && i.ends_with("-----") {
			let xlbl = &i[10..i.len()-5];	//10 to allow space at start of kind.
			if xlbl.ends_with(kind) { found_something = true; }
			if open_label.is_some() { return false; }
			open_label = Some(xlbl);
			validator = Base64Decoder::new();
		} else if i.starts_with("-----END ") && i.ends_with("-----") {
			if validator.end(&mut DevNull).is_err() { return false; }
			let xlbl = &i[8..i.len()-5];	//10 to allow space at start of kind.
			if open_label != Some(xlbl) { return false; }
			open_label = None;
		} else if open_label.is_some() {
			if validator.data(i, &mut DevNull).is_err() { return false; }
		}
	}
	if open_label.is_some() { return false; }
	found_something
}

///Decode objects of specified kind out of PEM textual encoding.
///
///Takes in:
///
/// * A PEM textual encoding `data`.
/// * The kind of object to look for `kind`.
/// * A multiple object flag `multi`.
///
///This function decodes objects of the specified kind out of the specified encooding. If the flag is set, all
///objects of the specified kind are decoded, and concatenation of the objects is returned.
///
///See [`looks_like_pem()`] for rules on matching object kinds. Each object must be a single ASN.1 structure.
///If the flag is false, then there may only be one object of the matching kind.
///
///On success, returns `Ok(content)`, where `content` is the decoded binary content.
///
///On failure, returns `Err(err)`, where `err` is object describing the error.
///
///[`looks_like_pem()`]: fn.looks_like_pem.html
pub fn decode_pem(data: &[u8], kind: &str, multi: bool) -> Result<Vec<u8>, DecodingError>
{
	use self::DecodingError::*;
	//This is supposed to be UTF-8.
	let data = from_utf8(data).map_err(|_|NotValidUtf8)?;
	let mut out = Vec::new();
	let mut interesting = false;
	let mut found_one = false;
	let mut found_count = 0usize;
	let mut decoder = Base64Decoder::new();
	let mut last_start = 0usize;
	//We skip everything we checked in looks_like_pem().
	for i in data.lines() {
		if i.starts_with("-----BEGIN ") && i.ends_with("-----") {
			let xlbl = &i[10..i.len()-5];	//10 to allow space at start of kind.
			//If multi is set, we concatenate the elements.
			if xlbl.ends_with(kind) {
				if multi || !found_one { interesting = true; }
				found_count = found_count.saturating_add(1);
			}
			found_one |= interesting;
			last_start = out.len();
			decoder = Base64Decoder::new();
		} else if i.starts_with("-----END ") && i.ends_with("-----") {
			//Only process if interesting. Ignore other blocks.
			if interesting {
				decoder.end(&mut out)?;
				let last_element = &out[last_start..];
				fail_if!(!is_single_asn1_structure(last_element), ElementNotAsn1);
			}
			interesting = false;
		} else if interesting {
			decoder.data(i, &mut out)?;
		}
	}
	fail_if!(!multi && found_count != 1, ExpectedOne);
	Ok(out)
}

fn decode_octets_cbor<'a>(x: &'a CborNode) -> Result<&'a [u8], ()>
{
	if let &CborNode::Octets(ref x) = x {
		Ok(&x[..])
	} else {
		Err(())
	}
}

fn decode_base64url_json(x: &JsonNode) -> Result<Vec<u8>, ()>
{
	if let &JsonNode::String(ref x) = x {
		let mut v = Vec::new();
		let mut base64_pol = 0u8;
		let mut base64_val = 0;
		for j in x.chars() {
			if j == ' ' || j == '\t' { continue; }
			let x = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_".find(j).
				unwrap_or(64);
			if x == 64 { return Err(()); }
			base64_val = base64_val << 6 | x;
			base64_pol = base64_pol.wrapping_add(1) & 3;
			if base64_pol == 0 {
				v.push((base64_val >> 16) as u8);
				v.push((base64_val >> 8) as u8);
				v.push(base64_val as u8);
			}
		}
		if base64_pol == 1 { return Err(()); }
		if base64_pol == 2 {
			v.push((base64_val >> 4) as u8);
		} else if base64_pol == 3 {
			v.push((base64_val >> 10) as u8);
			v.push((base64_val >> 2) as u8);
		}
		Ok(v)
	} else {
		Err(())
	}
}

fn serialize_rsa_key(n: &[u8], e: &[u8], d: &[u8], p: &[u8], q: &[u8], dp: &[u8], dq: &[u8], qi: &[u8]) ->
	Result<(usize, Cow<'static, [u8]>), DecodingError>
{
	use self::DecodingError::*;
	fail_if!(n.len() < 256 || n.len() > 512, RsaKeyTooSmall);
	fail_if!(n[0] < 128, RsaKeyBadSize);	//n[0] is valid, since n.len() is 256-512 by above.
	let largesize = n.len();
	let smallsize = (n.len() + 1) / 2;
	fail_if!(d.len() > largesize, RsaElementsTooLarge);
	fail_if!(p.len() > smallsize, RsaElementsTooLarge);
	fail_if!(q.len() > smallsize, RsaElementsTooLarge);
	fail_if!(dp.len() > smallsize, RsaElementsTooLarge);
	fail_if!(dq.len() > smallsize, RsaElementsTooLarge);
	fail_if!(qi.len() > smallsize, RsaElementsTooLarge);

	let mut output = Vec::new();
	output.write_u16(largesize as u16).map_err(|_|CantSerializeRsaKey)?;
	write_padded(&mut output, n, largesize).map_err(|_|CantSerializeRsaKey)?;
	write_padded(&mut output, d, largesize).map_err(|_|CantSerializeRsaKey)?;
	write_padded(&mut output, p, smallsize).map_err(|_|CantSerializeRsaKey)?;
	write_padded(&mut output, q, smallsize).map_err(|_|CantSerializeRsaKey)?;
	write_padded(&mut output, dp, smallsize).map_err(|_|CantSerializeRsaKey)?;
	write_padded(&mut output, dq, smallsize).map_err(|_|CantSerializeRsaKey)?;
	write_padded(&mut output, qi, smallsize).map_err(|_|CantSerializeRsaKey)?;
	output.write_slice(e).map_err(|_|CantSerializeRsaKey)?;
	return Ok((KEYFORMAT_RSA, Cow::Owned(output)));
}

///Decoding of RSA key format.
///
///This structure contains a decode of the internal RSA key format.
#[derive(Copy,Clone)]
pub struct RsaKeyDecode<'a>
{
	///RSA n
	pub n: &'a [u8],
	///RSA e
	pub e: &'a [u8],
	///RSA d
	pub d: &'a [u8],
	///RSA p
	pub p: &'a [u8],
	///RSA q
	pub q: &'a [u8],
	///RSA dp
	pub dp: &'a [u8],
	///RSA dq
	pub dq: &'a [u8],
	///RSA qi
	pub qi: &'a [u8],
}

impl<'a> RsaKeyDecode<'a>
{
	///Do the decoding.
	///
	///Takes in:
	///
	/// * The internal format RSA keypair representation `data`.
	///
	///This method decodes the representation.
	///
	///On success, returns `Ok(keypair)`, where `keypair` is the decoding of the specified RSA key.
	///
	///On failure, returns `Err(())`. The reasons for failing include:
	///
	/// * Invalid RSA key.
	pub fn new(data: &'a [u8]) -> Result<RsaKeyDecode<'a>, ()>
	{
		let mut input = Source::new(data);
		let largelen = input.read_u16(())? as usize;
		let smallen = (largelen + 1) / 2;
		let n = input.read_slice(largelen, ())?;
		let d = input.read_slice(largelen, ())?;
		let p = input.read_slice(smallen, ())?;
		let q = input.read_slice(smallen, ())?;
		let dp = input.read_slice(smallen, ())?;
		let dq = input.read_slice(smallen, ())?;
		let qi = input.read_slice(smallen, ())?;
		let e = input.read_remaining();
		Ok(RsaKeyDecode{n:n, e:e, d:d, p:p, q:q, dp:dp, dq:dq, qi:qi})
	}
}

fn serialize_ecdsa_key(oid: &[u8], d: &[u8], x: &[u8], y: &[u8], len: usize) ->
	Result<(usize, Cow<'static, [u8]>), DecodingError>
{
	use self::DecodingError::*;
	let mut output = Vec::new();
	output.write_slice(oid).map_err(|_|CantSerializeEcdsaKey)?;
	write_padded(&mut output, d, len).map_err(|_|CantSerializeEcdsaKey)?;
	write_padded(&mut output, x, len).map_err(|_|CantSerializeEcdsaKey)?;
	write_padded(&mut output, y, len).map_err(|_|CantSerializeEcdsaKey)?;
	return Ok((KEYFORMAT_ECDSA, Cow::Owned(output)));
}

///ECDSA curve.
///
///This enumeration represents curve ECDSA is computed over.
#[derive(Copy,Clone)]
pub enum EcdsaCurve
{
	///NSA P-256
	///
	///This is the NSA P-256 curve.
	NsaP256,
	///NSA P-384
	///
	///This is the NSA P-384 curve.
	NsaP384,
	///NSA P-521
	///
	///This is the NSA P-521 curve.
	NsaP521,
}

impl EcdsaCurve
{
	///Get the curve by ID number.
	///
	///Takes in:
	///
	/// * Elliptic curve ID number `id`.
	///
	///This method returns the curve with given ID.
	///
	///Note: This method is meant for legacy support, avoid using this method.
	///
	///If the specified curve exists, returns `Some(x)`, where `x` is the curve.
	///
	///If no such curve exists, returns `None`.
	pub fn by_id(id: u32) -> Option<EcdsaCurve>
	{
		match id {
			0 => Some(EcdsaCurve::NsaP256),
			1 => Some(EcdsaCurve::NsaP384),
			2 => Some(EcdsaCurve::NsaP521),
			_ => None
		}
	}
	///Get the curve ID number.
	///
	///Takes in:
	///
	/// * An elliptic curve `self`.
	///
	///This method returns the identification number for the curve.
	pub fn get_id(&self) -> u32
	{
		match *self {
			EcdsaCurve::NsaP256 => 0,
			EcdsaCurve::NsaP384 => 1,
			EcdsaCurve::NsaP521 => 2,
		}
	}
	///Get the major component bytes.
	///
	///Takes in:
	///
	/// * An elliptic curve `self`.
	///
	///This method returns the number of bytes in each major part (i.e., `d`, `x` and `y`) of the curve.
	///
	///For example, this returns `32` for `EcdsaCurve::P256`, because it takes 32 bytes to store, one of `d`,
	///`x` or `y`) for that curve.
	pub fn get_component_bytes(&self) -> usize
	{
		match *self {
			EcdsaCurve::NsaP256 => NSA_P256_PART_LEN,
			EcdsaCurve::NsaP384 => NSA_P384_PART_LEN,
			EcdsaCurve::NsaP521 => NSA_P521_PART_LEN,
		}
	}
	///Get the variant number.
	///
	///Takes in:
	///
	/// * An elliptic curve `self`.
	///
	///This method returns the variant number for the curve.
	pub fn get_variant(&self) -> u8
	{
		match *self {
			EcdsaCurve::NsaP256 => 0,
			EcdsaCurve::NsaP384 => 0,
			EcdsaCurve::NsaP521 => 0,
		}
	}
	///Get the S-Expr format curve name.
	///
	///Takes in:
	///
	/// * An elliptic curve `self`.
	///
	///This method returns the name of the curve used in S-Expression format for storing keypairs for the curve.
	pub fn get_sexp_curve_name(&self) -> &'static str
	{
		match *self {
			EcdsaCurve::NsaP256 => NSA_P256_SEXP_ID,
			EcdsaCurve::NsaP384 => NSA_P384_SEXP_ID,
			EcdsaCurve::NsaP521 => NSA_P521_SEXP_ID,
		}
	}
	///Get the JWK format curve name.
	///
	///Takes in:
	///
	/// * An elliptic curve `self`.
	///
	///This method returns the name of the curve used in JWK format for storing keypairs for the curve.
	pub fn get_jwk_curve_name(&self) -> &'static str
	{
		match *self {
			EcdsaCurve::NsaP256 => NSA_P256_JWK_ID,
			EcdsaCurve::NsaP384 => NSA_P384_JWK_ID,
			EcdsaCurve::NsaP521 => NSA_P521_JWK_ID,
		}
	}
	///Get the ECDSA key algorithm name for this curve
	///
	///Takes in:
	///
	/// * An elliptic curve `self`.
	///
	///This method returns the name of the ECDSA algorithm for the curve.
	pub fn get_ecdsa_name(&self) -> &'static str
	{
		match *self {
			EcdsaCurve::NsaP256 => "ecdsa-p256",
			EcdsaCurve::NsaP384 => "ecdsa-p384",
			EcdsaCurve::NsaP521 => "ecdsa-p521",
		}
	}
	///Get the native signature scheme list for this curve
	///
	///Takes in:
	///
	/// * An elliptic curve `self`.
	///
	///This method returns the native signature scheme list for the curve.
	pub fn native_signature_schemes(&self) -> &'static [u16]
	{
		match *self {
			EcdsaCurve::NsaP256 => &SIGN_ECDSA_P256_SCHEMES[..],
			EcdsaCurve::NsaP384 => &SIGN_ECDSA_P384_SCHEMES[..],
			EcdsaCurve::NsaP521 => &SIGN_ECDSA_P521_SCHEMES[..],
		}
	}
}

///Decoding of ECDSA key format.
///
///This structure contains a decode of the internal ECDSA key format.
#[derive(Copy,Clone)]
pub struct EcdsaKeyDecode<'a>
{
	///ECDSA curve.
	pub crv: EcdsaCurve,
	///ECDSA d
	pub d: &'a [u8],
	///ECDSA x
	pub x: &'a [u8],
	///ECDSA y
	pub y: &'a [u8],
}

impl<'a> EcdsaKeyDecode<'a>
{
	///Do the decoding.
	///
	///Takes in:
	///
	/// * The internal format ECDSA keypair representation `data`.
	///
	///This method decodes the representation.
	///
	///On success, returns `Ok(keypair)`, where `keypair` is the decoding of the specified ECDSA key.
	///
	///On failure, returns `Err(())`. The reasons for failing include:
	///
	/// * Invalid ECDSA key.
	pub fn new(data: &'a [u8]) -> Result<EcdsaKeyDecode<'a>, ()>
	{
		fail_if!(data.len() < 1 || data.len() % 3 != 1, ());
		let nominal_size = (data.len() - 1) / 3;
		let (id, data) = data.split_at(1);
		let (d, data) = data.split_at(nominal_size);
		let (x, y) = data.split_at(nominal_size);
		let crv = match (id[0], nominal_size) {
			(0, NSA_P256_PART_LEN) => EcdsaCurve::NsaP256,
			(0, NSA_P384_PART_LEN) => EcdsaCurve::NsaP384,
			(0, NSA_P521_PART_LEN) => EcdsaCurve::NsaP521,
			_ => fail!(())
		};
		Ok(EcdsaKeyDecode{crv:crv, d:d, x:x, y:y})
	}
}

///Decoding of Ed25519 key format.
///
///This structure contains a decode of the internal Ed25519 key format.
#[derive(Copy,Clone)]
pub struct Ed25519KeyDecode<'a>
{
	///Ed25519 private key (always 32 bytes)
	pub private: &'a [u8],
	///Ed25519 public key (always 32 bytes)
	pub public: &'a [u8],
}

impl<'a> Ed25519KeyDecode<'a>
{
	///Do the decoding.
	///
	///Takes in:
	///
	/// * The internal format Ed25519 keypair representation `data`.
	///
	///This method decodes the representation.
	///
	///On success, returns `Ok(keypair)`, where `keypair` is the decoding of the specified Ed25519 key.
	///
	///On failure, returns `Err(())`. The reasons for failing include:
	///
	/// * Invalid Ed25519 key.
	pub fn new(data: &'a [u8]) -> Result<Ed25519KeyDecode<'a>, ()>
	{
		fail_if!(data.len() != 64, ());
		let (privkey, pubkey) = data.split_at(32);
		Ok(Ed25519KeyDecode{private:privkey, public:pubkey})
	}
}

///Decoding of Ed448 key format.
///
///This structure contains a decode of the internal Ed448 key format.
#[derive(Copy,Clone)]
pub struct Ed448KeyDecode<'a>
{
	///Ed448 private key (always 57 bytes)
	pub private: &'a [u8],
	///Ed448 public key (always 57 bytes)
	pub public: &'a [u8],
}

impl<'a> Ed448KeyDecode<'a>
{
	///Do the decoding.
	///
	///Takes in:
	///
	/// * The internal format Ed448 keypair representation `data`.
	///
	///This method decodes the representation.
	///
	///On success, returns `Ok(keypair)`, where `keypair` is the decoding of the specified Ed25519 key.
	///
	///On failure, returns `Err(())`. The reasons for failing include:
	///
	/// * Invalid Ed448 key.
	pub fn new(data: &'a [u8]) -> Result<Ed448KeyDecode<'a>, ()>
	{
		fail_if!(data.len() != 114, ());
		let (privkey, pubkey) = data.split_at(57);
		Ok(Ed448KeyDecode{private:privkey, public:pubkey})
	}
}

///Convert a COSE key format key into internal format.
///
///Takes in:
///
/// * The keypair in COSE key format `data`.
///
///This function converts the keypair into the internal representation.
///
///On success, returns `Ok((format, intrep))`, where `format` is one of the `KEYFORMAT_*` constants, and
///`intrep` is the internal representation of the key.
///
///On failure, returns `Err(err)`, where `err` describes the error.
pub fn convert_key_from_cwk(data: &[u8]) -> Result<(usize, Cow<'static, [u8]>), DecodingError>
{
	use self::DecodingError::*;
	let cbor = CborNode::parse(data).map_err(|_|CwkInvalidCbor)?;
	if let CborNode::Dictionary(cbor) = cbor {
		let kty = cbor.get(&CborNode::Integer(1)).ok_or(NoKeyType)?.clone();
		if kty == CborNode::Integer(3) {
			//RSA key.
			let n = read_cose_keypart(&cbor, 0, "n", "RSA")?;
			let e = read_cose_keypart(&cbor, 1, "e", "RSA")?;
			let d = read_cose_keypart(&cbor, 2, "d", "RSA")?;
			let p = read_cose_keypart(&cbor, 3, "p", "RSA")?;
			let q = read_cose_keypart(&cbor, 4, "q", "RSA")?;
			let dp = read_cose_keypart(&cbor, 5, "dp", "RSA")?;
			let dq = read_cose_keypart(&cbor, 6, "dq", "RSA")?;
			let qi = read_cose_keypart(&cbor, 7, "qi", "RSA")?;
			return serialize_rsa_key(&n, &e, &d, &p, &q, &dp, &dq, &qi);
		} else if kty == CborNode::Integer(2) {
			//ECC key.
			static ZEROBYTE: [u8;1] = [0];
			let crv = cbor.get(&CborNode::NegInteger(0)).ok_or(NoCurve)?.clone();
			let (oid, len) = if crv == CborNode::Integer(1) {
				(&ZEROBYTE[..], NSA_P256_PART_LEN)
			} else if crv == CborNode::Integer(2) {
				(&ZEROBYTE[..], NSA_P384_PART_LEN)
			} else if crv == CborNode::Integer(3) {
				(&ZEROBYTE[..], NSA_P521_PART_LEN)
			} else {
				fail!(UnknownCurve);
			};
			let d = read_cose_keypart(&cbor, 3, "d", "EC")?;
			let x = read_cose_keypart(&cbor, 1, "x", "EC")?;
			let y = read_cose_keypart(&cbor, 2, "y", "EC")?;
			return serialize_ecdsa_key(oid, &d, &x, &y, len);
		} else if kty == CborNode::Integer(1) {
			//OKP key (Ed25519 or Ed448).
			let sub = cbor.get(&CborNode::NegInteger(0)).ok_or(WkNoOkpSubtype)?.clone();
			let d = read_cose_keypart(&cbor, 3, "d", "OKP")?;
			let x = read_cose_keypart(&cbor, 1, "x", "OKP")?;
			if sub == CborNode::Integer(6) {
				fail_if!(d.len() != 32, BadEd25519d);
				fail_if!(x.len() != 32, BadEd25519x);
				let mut res = Vec::new();
				res.extend_from_slice(&d);
				res.extend_from_slice(&x);
				return Ok((KEYFORMAT_ED25519, Cow::Owned(res)));
			} else if sub == CborNode::Integer(7) {
				fail_if!(d.len() != 57, BadEd448d);
				fail_if!(x.len() != 57, BadEd448x);
				let mut res = Vec::new();
				res.extend_from_slice(&d);
				res.extend_from_slice(&x);
				return Ok((KEYFORMAT_ED448, Cow::Owned(res)));
			} else {
				fail!(WkUnknownOkpSubtype);
			}
		} else {
			fail!(UnknownKeyType);
		}
	} else {
		fail!(WkToplevelNotDictionary);
	}
}

fn read_cose_keypart<'a>(dict: &'a BTreeMap<CborNode, CborNode>, key: u64, keyname: &'static str,
	cwktype: &'static str) -> Result<&'a [u8], DecodingError>
{
	use self::DecodingError::*;
	decode_octets_cbor(dict.get(&CborNode::NegInteger(key)).ok_or(WkNoParameter(keyname, cwktype))?).map_err(|_|
		WkBadParameter(keyname, cwktype))
}

fn read_jose_keypart(dict: &BTreeMap<String, JsonNode>, key: &'static str, jwktype: &'static str) ->
	Result<Vec<u8>, DecodingError>
{
	use self::DecodingError::*;
	decode_base64url_json(dict.get(key).ok_or(WkNoParameter(key, jwktype))?).map_err(|_|WkBadParameter(key,
		jwktype))
}

fn write_padded<S:Sink>(sink: &mut S, data: &[u8], datalen: usize) -> Result<(), ()>
{
	for _ in data.len()..datalen { sink.write_u8(0)?; }
	if data.len() > datalen {
		//data.len() is bigger than datalen by the check above.
		sink.write_slice(&data[data.len() - datalen..])
	} else {
		sink.write_slice(data)
	}?;
	Ok(())
}

///Convert a JWK into internal format.
///
///Takes in:
///
/// * The keypair in JWK format `data`.
///
///This function converts the keypair into the internal representation.
///
///On success, returns `Ok((format, intrep))`, where `format` is one of the `KEYFORMAT_*` constants, and
///`intrep` is the internal representation of the key.
///
///On failure, returns `Err(err)`, where `err` describes the error.
pub fn convert_key_from_jwk(data: &[u8]) -> Result<(usize, Cow<'static, [u8]>), DecodingError>
{
	use self::DecodingError::*;
	//This is supposed to be UTF-8.
	let mut data = from_utf8(data).map_err(|_|NotValidUtf8)?;
	let json = JsonNode::parse(&mut data).map_err(|_|JwkInvalidJson)?;
	if let &JsonNode::Dictionary(ref json) = &json {
		let kty = json.get("kty").ok_or(NoKeyType)?;
		if kty == "RSA" {
			//RSA key.
			let n = read_jose_keypart(&json, "n", "RSA")?;
			let e = read_jose_keypart(&json, "e", "RSA")?;
			let d = read_jose_keypart(&json, "d", "RSA")?;
			let p = read_jose_keypart(&json, "p", "RSA")?;
			let q = read_jose_keypart(&json, "q", "RSA")?;
			let dp = read_jose_keypart(&json, "dp", "RSA")?;
			let dq = read_jose_keypart(&json, "dq", "RSA")?;
			let qi = read_jose_keypart(&json, "qi", "RSA")?;
			return serialize_rsa_key(&n, &e, &d, &p, &q, &dp, &dq, &qi);
		} else if kty == "EC" {
			//ECC key.
			static ZEROBYTE: [u8;1] = [0];
			let crv = json.get("crv").ok_or(NoCurve)?;
			let (oid, len) = if crv == NSA_P256_JWK_ID {
				(&ZEROBYTE[..], NSA_P256_PART_LEN)
			} else if crv == NSA_P384_JWK_ID {
				(&ZEROBYTE[..], NSA_P384_PART_LEN)
			} else if crv == NSA_P521_JWK_ID {
				(&ZEROBYTE[..], NSA_P521_PART_LEN)
			} else {
				fail!(UnknownCurve);
			};
			let d = read_jose_keypart(&json, "d", "EC")?;
			let x = read_jose_keypart(&json, "x", "EC")?;
			let y = read_jose_keypart(&json, "y", "EC")?;
			return serialize_ecdsa_key(oid, &d, &x, &y, len);
		} else if kty == "OKP" {
			//OKP key (Ed25519 or Ed448).
			let sub = json.get("crv").ok_or(WkNoOkpSubtype)?;
			let d = read_jose_keypart(&json, "d", "OKP")?;
			let x = read_jose_keypart(&json, "x", "OKP")?;
			if sub == "Ed25519" {
				fail_if!(d.len() != 32, BadEd25519d);
				fail_if!(x.len() != 32, BadEd25519x);
				let mut res = Vec::new();
				res.extend_from_slice(&d);
				res.extend_from_slice(&x);
				return Ok((KEYFORMAT_ED25519, Cow::Owned(res)));
			} else if sub == "Ed448" {
				fail_if!(d.len() != 57, BadEd448d);
				fail_if!(x.len() != 57, BadEd448x);
				let mut res = Vec::new();
				res.extend_from_slice(&d);
				res.extend_from_slice(&x);
				return Ok((KEYFORMAT_ED448, Cow::Owned(res)));
			} else {
				fail!(WkUnknownOkpSubtype);
			}
		} else {
			fail!(UnknownKeyType);
		}
	} else {
		fail!(WkToplevelNotDictionary);
	}
}

///Convert a S-Expression key into internal format.
///
///Takes in:
///
/// * The keypair in S-Expression format `data`.
///
///This function converts the keypair into the internal representation.
///
///On success, returns `Ok((format, intrep))`, where `format` is one of the `KEYFORMAT_*` constants, and
///`intrep` is the internal representation of the key.
///
///On failure, returns `Err(err)`, where `err` describes the error.
pub fn convert_key_from_sexpr(data: &[u8]) -> Result<(usize, Cow<'static, [u8]>), DecodingError>
{
	use self::DecodingError::*;
	let mut stream = SexprParseStream::new(data);
	let ktype = stream.read_typed_str().map_err(|_|NoKeyType)?;
	if ktype == "rsa" {
		let n = stream.next_data().map_err(|_|SexpListTruncated)?;
		let e = stream.next_data().map_err(|_|SexpListTruncated)?;
		let d = stream.next_data().map_err(|_|SexpListTruncated)?;
		let p = stream.next_data().map_err(|_|SexpListTruncated)?;
		let q = stream.next_data().map_err(|_|SexpListTruncated)?;
		let dp = stream.next_data().map_err(|_|SexpListTruncated)?;
		let dq = stream.next_data().map_err(|_|SexpListTruncated)?;
		let qi = stream.next_data().map_err(|_|SexpListTruncated)?;
		stream.assert_eol().map_err(|_|SexpListTooLong)?;
		serialize_rsa_key(&n, &e, &d, &p, &q, &dp, &dq, &qi)
	} else if ktype == "ecdsa" {
		static ZEROBYTE: [u8;1] = [0];
		let crv = stream.next_str().map_err(|_|SexpListTruncated)?;
		let (oid, len) = if crv == NSA_P256_SEXP_ID {
			(&ZEROBYTE[..], NSA_P256_PART_LEN)
		} else if crv == NSA_P384_SEXP_ID {
			(&ZEROBYTE[..], NSA_P384_PART_LEN)
		} else if crv == NSA_P521_SEXP_ID {
			(&ZEROBYTE[..], NSA_P521_PART_LEN)
		} else {
			fail!(UnknownCurve);
		};
		let d = stream.next_data().map_err(|_|SexpListTruncated)?;
		let x = stream.next_data().map_err(|_|SexpListTruncated)?;
		let y = stream.next_data().map_err(|_|SexpListTruncated)?;
		stream.assert_eol().map_err(|_|SexpListTooLong)?;
		serialize_ecdsa_key(oid, d, x, y, len)
	} else if ktype == "ed25519" {
		let privkey = stream.next_data().map_err(|_|SexpListTruncated)?;
		let pubkey = stream.next_data().map_err(|_|SexpListTruncated)?;
		stream.assert_eol().map_err(|_|SexpListTooLong)?;
		fail_if!(privkey.len() != 32, BadEd25519d);
		fail_if!(pubkey.len() != 32, BadEd25519x);
		let mut res = Vec::new();
		res.extend_from_slice(privkey);
		res.extend_from_slice(pubkey);
		Ok((KEYFORMAT_ED25519, Cow::Owned(res)))
	} else if ktype == "ed448" {
		let privkey = stream.next_data().map_err(|_|SexpListTruncated)?;
		let pubkey = stream.next_data().map_err(|_|SexpListTruncated)?;
		stream.assert_eol().map_err(|_|SexpListTooLong)?;
		fail_if!(privkey.len() != 57, BadEd448d);
		fail_if!(pubkey.len() != 57, BadEd448x);
		let mut res = Vec::new();
		res.extend_from_slice(privkey);
		res.extend_from_slice(pubkey);
		Ok((KEYFORMAT_ED448, Cow::Owned(res)))
	} else {
		fail!(SexpUnknownKeyType);
	}
}

#[test]
fn ed25519_sexpr()
{
	let skey = include_bytes!("test-ed25519.sexp");
	let tkey = include_bytes!("test-ed25519.der");
	let (ktype, cmp) = convert_key_from_sexpr(&skey[..]).unwrap();
	assert_eq!(ktype, KEYFORMAT_ED25519);
	assert_eq!(cmp, &tkey[..]);
}

#[test]
fn ed25519_cose()
{
	let skey = include_bytes!("test-ed25519.cose");
	let tkey = include_bytes!("test-ed25519.der");
	let (ktype, cmp) = convert_key_from_cwk(&skey[..]).unwrap();
	assert_eq!(ktype, KEYFORMAT_ED25519);
	assert_eq!(cmp, &tkey[..]);
}

#[test]
fn ed25519_jose()
{
	let skey = include_bytes!("test-ed25519.jose");
	let tkey = include_bytes!("test-ed25519.der");
	let (ktype, cmp) = convert_key_from_jwk(&skey[..]).unwrap();
	assert_eq!(ktype, KEYFORMAT_ED25519);
	assert_eq!(cmp, &tkey[..]);
}

#[test]
fn ed448_sexpr()
{
	let skey = include_bytes!("test-ed448.sexp");
	let tkey = include_bytes!("test-ed448.der");
	let (ktype, cmp) = convert_key_from_sexpr(&skey[..]).unwrap();
	assert_eq!(ktype, KEYFORMAT_ED448);
	assert_eq!(cmp, &tkey[..]);
}

#[test]
fn ed448_cose()
{
	let skey = include_bytes!("test-ed448.cose");
	let tkey = include_bytes!("test-ed448.der");
	let (ktype, cmp) = convert_key_from_cwk(&skey[..]).unwrap();
	assert_eq!(ktype, KEYFORMAT_ED448);
	assert_eq!(cmp, &tkey[..]);
}

#[test]
fn ed448_jose()
{
	let skey = include_bytes!("test-ed448.jose");
	let tkey = include_bytes!("test-ed448.der");
	let (ktype, cmp) = convert_key_from_jwk(&skey[..]).unwrap();
	assert_eq!(ktype, KEYFORMAT_ED448);
	assert_eq!(cmp, &tkey[..]);
}

#[test]
fn p256_sexpr()
{
	let skey = include_bytes!("test-p256.sexp");
	let tkey = include_bytes!("test-p256.der");
	let (ktype, cmp) = convert_key_from_sexpr(&skey[..]).unwrap();
	assert_eq!(ktype, KEYFORMAT_ECDSA);
	assert_eq!(cmp, &tkey[..]);
}

#[test]
fn p256_cose()
{
	let skey = include_bytes!("test-p256.cose");
	let tkey = include_bytes!("test-p256.der");
	let (ktype, cmp) = convert_key_from_cwk(&skey[..]).unwrap();
	assert_eq!(ktype, KEYFORMAT_ECDSA);
	assert_eq!(cmp, &tkey[..]);
}

#[test]
fn p256_jose()
{
	let skey = include_bytes!("test-p256.jose");
	let tkey = include_bytes!("test-p256.der");
	let (ktype, cmp) = convert_key_from_jwk(&skey[..]).unwrap();
	assert_eq!(ktype, KEYFORMAT_ECDSA);
	assert_eq!(cmp, &tkey[..]);
}

#[test]
fn p384_sexpr()
{
	let skey = include_bytes!("test-p384.sexp");
	let tkey = include_bytes!("test-p384.der");
	let (ktype, cmp) = convert_key_from_sexpr(&skey[..]).unwrap();
	assert_eq!(ktype, KEYFORMAT_ECDSA);
	assert_eq!(cmp, &tkey[..]);
}

#[test]
fn p384_cose()
{
	let skey = include_bytes!("test-p384.cose");
	let tkey = include_bytes!("test-p384.der");
	let (ktype, cmp) = convert_key_from_cwk(&skey[..]).unwrap();
	assert_eq!(ktype, KEYFORMAT_ECDSA);
	assert_eq!(cmp, &tkey[..]);
}

#[test]
fn p384_jose()
{
	let skey = include_bytes!("test-p384.jose");
	let tkey = include_bytes!("test-p384.der");
	let (ktype, cmp) = convert_key_from_jwk(&skey[..]).unwrap();
	assert_eq!(ktype, KEYFORMAT_ECDSA);
	assert_eq!(cmp, &tkey[..]);
}

#[test]
fn rsa_sexpr()
{
	let skey = include_bytes!("test-rsa.sexp");
	let tkey = include_bytes!("test-rsa.der");
	let (ktype, cmp) = convert_key_from_sexpr(&skey[..]).unwrap();
	assert_eq!(ktype, KEYFORMAT_RSA);
	assert_eq!(cmp, &tkey[..]);
}

#[test]
fn rsa_cose()
{
	let skey = include_bytes!("test-rsa.cose");
	let tkey = include_bytes!("test-rsa.der");
	let (ktype, cmp) = convert_key_from_cwk(&skey[..]).unwrap();
	assert_eq!(ktype, KEYFORMAT_RSA);
	assert_eq!(cmp, &tkey[..]);
}

#[test]
fn rsa_jose()
{
	let skey = include_bytes!("test-rsa.jose");
	let tkey = include_bytes!("test-rsa.der");
	let (ktype, cmp) = convert_key_from_jwk(&skey[..]).unwrap();
	assert_eq!(ktype, KEYFORMAT_RSA);
	assert_eq!(cmp, &tkey[..]);
}
