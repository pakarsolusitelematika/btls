//!JSON and CBOR decoder
//!
//!This crate implements a JSON and CBOR decoders.

#![forbid(unsafe_code)]
#![forbid(missing_docs)]

use std::borrow::ToOwned;
use std::char::from_u32;
use std::cmp::{Ord, Ordering, PartialEq, PartialOrd};
use std::collections::BTreeMap;
#[cfg(test)]
use std::iter::FromIterator;
use std::str::{from_utf8, FromStr};
extern crate btls_aux_serialization;
use btls_aux_serialization::Source;
#[macro_use]
extern crate btls_aux_fail;

///A JSON value node.
///
///This enumeration represents a JSON value.
#[derive(Clone,Debug)]
pub enum JsonNode
{
	///NULL value.
	///
	///This is the JSON null type.
	Null,
	///Number (integer or float).
	///
	///This is the JSON number type. It takes the number value as the argument.
	Number(f64),
	///UTF-8 Text string
	///
	///This is the JSON string type. It takes the string as the argument.
	///
	///Note that only valid UTF-8 strings are supported as strings.
	String(String),
	///Boolean
	///
	///Tihs is the JSON boolean type. It takes the boolean value as the argument.
	Boolean(bool),
	///Array.
	///
	///This is the JSON array type. It takes a vector of JSON values as the argument.
	///
	///The argument vector stores all the values in JSON array, in order, without gaps.
	Array(Vec<JsonNode>),
	///Dictionary (map)
	///
	///This is the JSON object type. It takes a map from strings into JSON values as the argument.
	///
	///The argument map stores mapping from all the keys in object to all the values in the object, with no
	///extra or missing entries. Note that only valid UTF-8 strings are supported as keys.
	Dictionary(BTreeMap<String, JsonNode>),
}

fn decode_hex(x: char) -> Result<u32, ()>
{
	Ok(match x {
		'0' => 0,
		'1' => 1,
		'2' => 2,
		'3' => 3,
		'4' => 4,
		'5' => 5,
		'6' => 6,
		'7' => 7,
		'8' => 8,
		'9' => 9,
		'A' => 10, 'a' => 10,
		'B' => 11, 'b' => 11,
		'C' => 12, 'c' => 12,
		'D' => 13, 'd' => 13,
		'E' => 14, 'e' => 14,
		'F' => 15, 'f' => 15,
		_ => fail!(())
	})
}

impl JsonNode
{
	fn token_to_string(data: &str) -> Result<String, ()>
	{
		let mut state = 0;
		let mut surrogate_break = None;
		let mut val = 0u32;
		let mut res = String::with_capacity(data.len());
		for i in data.chars() {
			match state {
				0 => {		//The initial ".
					fail_if!(i != '\"', ());
					state = 1;
				},
				1 if i == '\"' => state = 7,	///Unescaped character, end.
				1 if i == '\\' => state = 2,	///Unescaped character, escape.
				1 if (i as u32) < 32 => fail!(()),  //Controls.
				//Delete is allowed for some reason!
				1 => res.push(i),
				2  if i == 'u' => state = 3,	//Hex Escape.
				2  => {				//Escape.
					if i == '\"' { res.push('\"'); }
					else if i == '\\' { res.push('\\'); }
					else if i == '/' { res.push('/'); }
					else if i == 'b' { res.push('\x08'); }
					else if i == 'f' { res.push('\x0c'); }
					else if i == 'n' { res.push('\x0a'); }
					else if i == 'r' { res.push('\x0d'); }
					else if i == 't' { res.push('\x09'); }
					else { fail!(()); }
					state = 1;
				},
				3 => {
					val = 16 * val + decode_hex(i)?;
					state = 4;
				},
				4 => {
					val = 16 * val + decode_hex(i)?;
					state = 5;
				},
				5 => {
					val = 16 * val + decode_hex(i)?;
					state = 6;
				},
				6 => {
					val = 16 * val + decode_hex(i)?;
					if val >> 11 != 27u32 {
						res.push(match from_u32(val) {
							Some(x) => x,
							None => fail!(())
						});
					} else if let Some((p, c)) = surrogate_break {
						fail_if!(c >> 10 != 54u32, ());
						fail_if!(val >> 10 != 55u32, ());
						fail_if!(p != res.len(), ());
						let rval = 65536 + (c - 0xD800) * 1024 + (val - 0xDC00);
						res.push(match from_u32(rval) {
							Some(x) => x,
							None => fail!(())
						});
						surrogate_break = None;
					} else {
						surrogate_break = Some((res.len(), val));
					}
					val = 0;
					state = 1;
				},
				_ => fail!(()),
			}
		}
		fail_if!(state != 7 || surrogate_break.is_some(), ());
		Ok(res)
	}
	fn pull_token<'a>(data: &mut &'a str) -> Result<&'a str, ()>
	{
		//Any whitespace will be skipped.
		//After that, if:
		// - It starts with ", it will be read as a string.
		// - Any 0-9A-Za-z, +, -, . will be read as token.
		// - Anything else will be returned as single char.
		let mut sidx = None;
		let mut fidx = None;
		let mut token = false;
		let mut string = false;
		let mut escaped = false;
		let mut token_end = false;
		for i in data.char_indices() {
			if token_end { fidx = Some(i.0); break; }
			if string {
				if escaped {
					//This is not strictly true, but " must be directly escaped.
					escaped = false;
				} else {
					escaped = i.1 == '\\';
					token_end = i.1 == '\"';
				}
				continue;
			}
			if (i.1 == '\t' || i.1 == ' ' || i.1 == '\r' || i.1 == '\n') && sidx.is_none() { continue; }
			if sidx.is_none() { sidx = Some(i.0); }
			if "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ+-.".find(i.1).is_some() {
				token = true;
				continue;
			}
			if i.1 == '\"' && !token {
				string = true;
				continue;
			}
			if token { fidx = Some(i.0); break; }
			token_end = true;
		}
		let (sidx, fidx) = if let Some(x) = sidx {
			(x, fidx.unwrap_or(data.len()))
		} else {
			fail!(());
		};
		fail_if!(!data.is_char_boundary(sidx) || !data.is_char_boundary(fidx), ());
		let ret = &(*data)[sidx..fidx];	//Sidx and fidx are are at char boundaries.
		*data = &(*data)[fidx..];
		Ok(ret)
	}
	///Parse a text string as JSON.
	///
	///Takes in:
	///
	/// * JSON text `data`.
	///
	///This method parses the text as JSON.
	///
	///If the text has UTF-8 BOM (U+0xFEFF) as its first codepoint, that codepoint is skipped.
	///
	///On success, returns `Ok(value)`, where `value` is the decoded JSON value at root level.
	///
	///On failure, returns `Err(())`. At least the following factors cause the method to fail:
	///
	/// * Not valid JSON.
	/// * Too deep nesting.
	/// * Surrogate escape present and not properly paired with another surrogate escape.
	pub fn parse(data: &str) -> Result<JsonNode, ()>
	{
		//Discard BOM (why on earth??)
		let mut data = if data.chars().next().map(|x|x as u32) == Some(0xFEFF) {
			//BOM is 3 bytes.
			let (_, tmp) = data.split_at(3);
			tmp
		} else {
			data
		};
		let res = Self::_parse(&mut data, 0);
		for i in data.chars() {
			fail_if!(i != ' ' && i != '\t' && i != '\r' && i != '\n', ());
		}
		res
	}
	fn _parse<'a>(data: &mut &'a str, depth: usize) -> Result<JsonNode, ()>
	{
		const MAX_DEPTH: usize = 500;
		fail_if!(depth > MAX_DEPTH, ());	//Stack overflow prevention.
		let next = JsonNode::pull_token(data)?;
		if next == "null" { return Ok(JsonNode::Null); }
		if next == "false" { return Ok(JsonNode::Boolean(false)); }
		if next == "true" { return Ok(JsonNode::Boolean(true)); }
		if next == "[" {
			let mut x = Vec::new();
			loop {
				//0-length array?
				let mut data2: &'a str = *data;
				let next = JsonNode::pull_token(&mut data2)?;
				if next == "]" && x.len() == 0 { *data = data2; break; }
				//Value
				x.push(JsonNode::_parse(data, depth.saturating_add(1))?);
				let next = JsonNode::pull_token(data)?;
				//, or ]
				if next == "]" { break; }
				fail_if!(next != ",", ());
			}
			return Ok(JsonNode::Array(x));
		}
		if next == "{" {
			let mut x = BTreeMap::new();
			loop {
				//0-length dictionary?
				let mut data2: &'a str = *data;
				let next = JsonNode::pull_token(&mut data2)?;
				if next == "}" && x.len() == 0 { *data = data2; break; }
				//Key
				let next = JsonNode::pull_token(data)?;
				let key = JsonNode::token_to_string(next)?;
				//:
				let next = JsonNode::pull_token(data)?;
				fail_if!(next != ":", ());
				//Value
				fail_if!(x.contains_key(&key), ());
				x.insert(key, JsonNode::_parse(data, depth.saturating_add(1))?);
				//, or }
				let next = JsonNode::pull_token(data)?;
				if next == "}" { break; }
				fail_if!(next != ",", ());
			}
			return Ok(JsonNode::Dictionary(x));
		}
		let fcode = next.chars().next().map(|x|x as u32).unwrap_or(0);
		if fcode >= 48 && fcode <= 57 || fcode == 45 {
			const MINUS: u8 = 1;
			const ZERO: u8 = 2;
			const NZERODIGIT: u8 = 4;
			const DOT: u8 = 8;
			const EXP: u8 = 16;
			const PLUS: u8 = 32;
			const STATE_INIT: u32 = 0;
			const STATE_MI: u32 = 1;
			const STATE_IN: u32 = 2;
			const STATE_ZE: u32 = 3;
			const STATE_REDFR: u32 = 4;
			const STATE_E1: u32 = 5;
			const STATE_FR: u32 = 6;
			const STATE_E2: u32 = 7;
			const STATE_E3: u32 = 8;
			//Numbers. -?(0|[1-9][0-9]*)(.[0-9]+)?([eE][+-]?[0-9]+)?
			let mut checkstate = STATE_INIT;
			for i in next.chars() {
				let charclass = match i {
					'-' => MINUS,
					'0' => ZERO,
					'1' => NZERODIGIT, '2' => NZERODIGIT, '3' => NZERODIGIT, '4' => NZERODIGIT,
					'5' => NZERODIGIT, '6' => NZERODIGIT, '7' => NZERODIGIT, '8' => NZERODIGIT,
					'9' => NZERODIGIT,
					'.' => DOT,
					'e' => EXP,	'E' => EXP,
					'+' => PLUS,
					_ => fail!(())		//Can't be valid.
				};
				checkstate = match checkstate {
					//Exponent sign is allowed in IN, ZE and FR.
					STATE_IN|STATE_ZE|STATE_FR if charclass & EXP != 0 => STATE_E1,
					//Initial minus sign.
					STATE_INIT if charclass & MINUS != 0 => STATE_MI,
					//After E, there can be + or -, transitions to E2.
					STATE_E1 if charclass & (MINUS|PLUS) != 0 => STATE_E2,
					//Any digit allowed in IN.
					STATE_IN if charclass & (ZERO|NZERODIGIT) != 0 => STATE_IN,
					//Any digit allowed in RedFR or FR. => Red forced away.
					STATE_REDFR|STATE_FR if charclass & (ZERO|NZERODIGIT) != 0 => STATE_FR,
					//Any digit allowed in E1, E2 or E3, but transitions to E3.
					STATE_E1|STATE_E2|STATE_E3 if charclass & (ZERO|NZERODIGIT) != 0 => STATE_E3,
					//Init and MI can have zero, transitions to ZE.
					STATE_INIT|STATE_MI if charclass & ZERO != 0 => STATE_ZE,
					//Init and MI can have nzero, transitions to IN.
					STATE_INIT|STATE_MI if charclass & NZERODIGIT != 0 => STATE_IN,
					//In and ZE can have dot, transitions to RedFR.
					STATE_IN|STATE_ZE if charclass & DOT != 0 => STATE_REDFR,
					//Others are errors.
					_ => fail!(())
				}
			}
			//Check end state.
			const ACCEPTS: u32 = (1 << STATE_ZE) | (1 << STATE_IN) | (1 << STATE_FR) | (1 << STATE_E3);
			fail_if!((1 << checkstate) & ACCEPTS == 0, ());
			return Ok(JsonNode::Number(f64::from_str(next).map_err(|_|())?));
		}
		if next.starts_with("\"") {
			//String.
			return Ok(JsonNode::String(JsonNode::token_to_string(next)?));
		}
		Err(())
	}
}

impl<'a> PartialEq<&'a str> for JsonNode
{
	fn eq(&self, other: &&'a str) -> bool
	{
		if let &JsonNode::String(ref x) = self {
			x == *other
		} else {
			false
		}
	}
}

impl PartialEq<str> for JsonNode
{
	fn eq(&self, other: &str) -> bool
	{
		if let &JsonNode::String(ref x) = self {
			x == other
		} else {
			false
		}
	}
}

impl PartialEq<f64> for JsonNode
{
	fn eq(&self, other: &f64) -> bool
	{
		if let &JsonNode::Number(x) = self {
			x == *other
		} else {
			false
		}
	}
}

impl PartialEq<bool> for JsonNode
{
	fn eq(&self, other: &bool) -> bool
	{
		if let &JsonNode::Boolean(x) = self {
			x == *other
		} else {
			false
		}
	}
}

impl PartialEq for JsonNode
{
	fn eq(&self, other: &Self) -> bool
	{
		match (self, other) {
			(&JsonNode::Null, &JsonNode::Null) => true,
			(&JsonNode::Boolean(x), &JsonNode::Boolean(y)) => x == y,
			(&JsonNode::Number(x), &JsonNode::Number(y)) => x == y,
			(&JsonNode::String(ref x), &JsonNode::String(ref y)) => x == y,
			(&JsonNode::Array(ref x), &JsonNode::Array(ref y)) => {
				let mut xtr = x.iter();
				let mut ytr = y.iter();
				loop {
					let (i, j) = (xtr.next(), ytr.next());
					if let (Some(ref i), Some(ref j)) = (i, j) {
						if i != j { return false; }
					} else if let (Some(_), None) = (i, j) {
						return false;
					} else if let (None, Some(_)) = (i, j) {
						return false;
					} else {
						return true;
					}
				}
			},
			(&JsonNode::Dictionary(ref x), &JsonNode::Dictionary(ref y)) => {
				let mut xtr = x.iter();
				let mut ytr = y.iter();
				loop {
					let (i, j) = (xtr.next(), ytr.next());
					if let (Some((ref k1, ref v1)), Some((ref k2, ref v2))) = (i, j) {
						if k1 != k2 { return false; }
						if v1 != v2 { return false; }
					} else if let (Some(_), None) = (i, j) {
						return false;
					} else if let (None, Some(_)) = (i, j) {
						return false;
					} else {
						return true;
					}
				}
			},
			(_, _) => false
		}
	}
}

impl Eq for JsonNode {}

///A CBOR value node.
///
///This enumeration represents a CBOR value.
///
///Through CBOR is mostly compatible with JSON, the CBOR has data types not present in JSON.
///
///Note that the `Ordering` implemented by this enumeration is undefined and does not equal the canonical CBOR
///ordering. However, the ordering is total and self-consistent, so BTreeMap is happy to have CBOR values as
///keys.
#[derive(Clone,Debug)]
pub enum CborNode
{
	///Nonnegative integer.
	///
	///This the CBOR nonnegative integer type. It takes the integer value as the argument.
	Integer(u64),
	///Negative integer.
	///
	///This is the CBOR negative integer type. It takes negative of the integer value, minus one as the argument.
	///
	///That is, 0 represents value of -1, and 2^64-1 represents vale of -2^64.
	NegInteger(u64),
	///Octet string.
	///
	///This is the CBOR octet string type. It takes the octet string as the argument.
	///
	///Note that this type is 8-bit clean, it can represent arbitrary octet string. JSON has no native octet
	///string type, in JSON, octet strings are commonly represented as base64url encoding of the octet string,
	///encoded as a text string.
	Octets(Vec<u8>),
	///UTF-8 string.
	///
	///This the CBOR text string type. It takes the text string as the argument.
	///
	///Note that CBOR requires text strings to be valid UTF-8, so arbitrary valid text string can be represented.
	String(String),
	///Array
	///
	///This is the CBOR array type. It takes a vector of CBOR values as the argument.
	///
	///The argument vector stores all the values in CBOR array, in order, without gaps.
	Array(Vec<CborNode>),
	///Dictionary (map).
	///
	///This is the CBOR object type. It takes a map from CBOR values into CBOR values as the argument.
	///
	///The argument map stores mapping from all the keys in object to all the values in the object, with no
	///extra or missing entries. Note that arbitrary CBOR values are supported as keys, including arrays or other
	///dictionaries. Note that JSON does not support keys that are not text strings.
	Dictionary(BTreeMap<CborNode, CborNode>),
	///A tagged value.
	///
	///This is the CBOR tag type. It takes the tag number as the first argument, and the CBOR object being
	///tagged in a box as the second argument.
	///
	///JSON does not have equivalent data type.
	Tag(u64, Box<CborNode>),
	///Simple value
	///
	///This is the CBOR simple value type. It takes the simple value number as an argument.
	///
	///The simple value number must either be between 0 and 23, inclusive or between 32 and 255 inclusive.
	///
	///The values, `false` and `true`, `null` and `undefined` (this is not in JSON) are represeted as simple
	///values, numbers 20, 21, 22 and 23, respectively.
	Simple(u8),
	///Half-precision (16 bit) floating-point value.
	///
	///This is the CBOR half-precision float type. It takes raw encoding of the floating-point number as an
	///argument.
	///
	///In JSON, this corresponds to subset of the number type.
	HalfFloat(u16),
	///Single-precision (32 bit) floating-point value.
	///
	///This is the CBOR single-precision float type. It takes raw encoding of the floating-point number as an
	///argument.
	///
	///In JSON, this corresponds to subset of the number type.
	Float(u32),
	///Double-precision (64 bit) floating-point value.
	///
	///This is the CBOR double-precision float type. It takes raw encoding of the floating-point number as an
	///argument.
	///
	///In JSON, this corresponds to the de-facto number type. In JSON, number type can be assumed to have the
	///same range and accuracy as double-precision floating point type.
	Double(u64),
}

///The CBOR FALSE simple value.
///
///This is CBOR value for `false`.
pub const CBOR_FALSE: CborNode = CborNode::Simple(20);
///The CBOR TRUE simple value.
///
///This is CBOR value for `true`.
pub const CBOR_TRUE: CborNode = CborNode::Simple(21);
///The CBOR NULL simple value.
///
///This is CBOR value for `null`.
pub const CBOR_NULL: CborNode = CborNode::Simple(22);
///The CBOR UNDEFINED simple value.
///
///This is CBOR value for `undefined`.
///
///There is no corresponding value in JSON.
pub const CBOR_UNDEFINED: CborNode = CborNode::Simple(23);

impl CborNode
{
	fn decode_header<'a>(data: &mut Source<'a>) -> Result<(u8, u8, Option<u64>), ()>
	{
		let cmdbyte = data.read_u8(())?;
		let major = cmdbyte >> 5;
		let minor = cmdbyte & 31;
		if minor < 24 {
			Ok((major, minor, Some(minor as u64)))
		} else if minor == 24 {
			Ok((major, minor, Some(data.read_u8(())? as u64)))
		} else if minor == 25 {
			Ok((major, minor, Some(data.read_u16(())? as u64)))
		} else if minor == 26 {
			Ok((major, minor, Some(data.read_u32(())? as u64)))
		} else if minor == 27 {
			Ok((major, minor, Some(data.read_u64(())?)))
		} else if minor < 31 {
			Err(())	//Invalid command byte.
		} else {
			Ok((major, minor, None))
		}
	}
	///Parse slice as CBOR structure.
	///
	///Takes in:
	///
	/// * The CBOR data `data`.
	///
	///This method parses the data as CBOR.
	///
	///On success, returns `Ok(value)`, where `value` is the decoded CBOR value at root level.
	///
	///On failure, returns `Err(())`. At least the following factors cause the method to fail:
	///
	/// * Not valid CBOR.
	/// * Too deep nesting.
	pub fn parse(data: &[u8]) -> Result<CborNode, ()>
	{
		let mut src = Source::new(data);
		let r = Self::_parse(&mut src, 0);
		fail_if!(!src.at_end(), ());
		r
	}
	fn _parse<'a>(data: &mut Source<'a>, depth: usize) -> Result<CborNode, ()>
	{
		const MAX_DEPTH: usize = 500;
		fail_if!(depth > MAX_DEPTH, ());	//Stack overflow prevention.
		let (major, minor, arg) = CborNode::decode_header(data)?;
		match (major, arg) {
			(0, Some(x)) => Ok(CborNode::Integer(x)),
			(1, Some(x)) => Ok(CborNode::NegInteger(x)),
			(2, Some(count)) => {
				fail_if!(count as usize as u64 != count, ());
				let x = data.read_slice(count as usize, ())?;
				Ok(CborNode::Octets(x.to_owned()))
			},
			(2, None) => {
				let mut x = Vec::new();
				loop {
					let (major, _, arg) = CborNode::decode_header(data)?;
					if major == 7 && arg == None { break; }
					match (major, arg) {
						(2, Some(count)) => {
							fail_if!(count as usize as u64 != count, ());
							let y = data.read_slice(count as usize, ())?;
							x.extend_from_slice(y);
						},
						(_, _) => fail!(())
					};
				}
				Ok(CborNode::Octets(x))
			},
			(3, Some(count)) => {
				fail_if!(count as usize as u64 != count, ());
				let x = data.read_slice(count as usize, ())?;
				let x = from_utf8(x).map_err(|_|())?;
				Ok(CborNode::String(x.to_owned()))
			},
			(3, None) => {
				let mut x = String::new();
				loop {
					let (major, _, arg) = CborNode::decode_header(data)?;
					if major == 7 && arg == None { break; }
					match (major, arg) {
						(3, Some(count)) => {
							fail_if!(count as usize as u64 != count, ());
							let y = data.read_slice(count as usize, ())?;
							let y = from_utf8(y).map_err(|_|())?;
							x.extend(y.chars());
						},
						(_, _) => fail!(())
					};
				}
				Ok(CborNode::String(x))
			},
			(4, Some(count)) => {
				let mut x = Vec::new();
				for _ in 0..count {
					x.push(CborNode::_parse(data, depth.saturating_add(1))?)
				}
				Ok(CborNode::Array(x))
			},
			(4, None) => {
				let mut x = Vec::new();
				loop {
					let mut data2 = data.clone();
					let (major, _, arg) = CborNode::decode_header(&mut data2)?;
					if major == 7 && arg == None { *data = data2; break; }
					x.push(CborNode::_parse(data, depth.saturating_add(1))?)
				}
				Ok(CborNode::Array(x))
			},
			(5, Some(count)) => {
				let mut x = BTreeMap::new();
				for _ in 0..count {
					let key = CborNode::_parse(data, depth.saturating_add(1))?;
					let value = CborNode::_parse(data, depth.saturating_add(1))?;
					fail_if!(x.contains_key(&key), ());
					x.insert(key, value);
				}
				Ok(CborNode::Dictionary(x))
			},
			(5, None) => {
				let mut x = BTreeMap::new();
				loop {
					let mut data2 = data.clone();
					let (major, _, arg) = CborNode::decode_header(&mut data2)?;
					if major == 7 && arg == None { *data = data2; break; }
					let key = CborNode::_parse(data, depth.saturating_add(1))?;
					let value = CborNode::_parse(data, depth.saturating_add(1))?;
					fail_if!(x.contains_key(&key), ());
					x.insert(key, value);
				}
				Ok(CborNode::Dictionary(x))
			},
			(6, Some(tag)) => {
				Ok(CborNode::Tag(tag, Box::new(CborNode::_parse(data, depth.saturating_add(1))?)))
			}
			(7, Some(tag)) if minor < 24 => {
				Ok(CborNode::Simple(tag as u8))
			}
			(7, Some(tag)) if minor == 24 => {
				if tag > 31 {
					Ok(CborNode::Simple(tag as u8))
				} else {
					Err(())
				}
			},
			(7, Some(x)) if minor == 25 => Ok(CborNode::HalfFloat(x as u16)),
			(7, Some(x)) if minor == 26 => Ok(CborNode::Float(x as u32)),
			(7, Some(x)) if minor == 27 => Ok(CborNode::Double(x)),
			(_, _) => Err(())
		}
	}
	fn priority(&self) -> usize
	{
		//Each discriminant must give a different value.
		match self {
			&CborNode::Integer(_) => 0,
			&CborNode::NegInteger(_) => 1,
			&CborNode::Octets(_) => 2,
			&CborNode::String(_) => 3,
			&CborNode::Array(_) => 4,
			&CborNode::Dictionary(_) => 5,
			&CborNode::Tag(_, _) => 6,
			&CborNode::Simple(_) => 7,
			&CborNode::HalfFloat(_) => 8,
			&CborNode::Float(_) => 9,
			&CborNode::Double(_) => 10,
		}
	}
}


impl PartialOrd for CborNode
{
	fn partial_cmp(&self, other: &Self) -> Option<Ordering>
	{
		//Just implement some consistent order so BTreeMap is happy.
		//We only need to compare matching discriminants, since the default case discrimnates on the
		//determinants themselves.
		match (self, other) {
			(&CborNode::Integer(ref x), &CborNode::Integer(ref y)) => x.partial_cmp(y),
			(&CborNode::NegInteger(ref x), &CborNode::NegInteger(ref y)) => x.partial_cmp(y),
			(&CborNode::Octets(ref x), &CborNode::Octets(ref y)) => x.partial_cmp(y),
			(&CborNode::String(ref x), &CborNode::String(ref y)) => x.partial_cmp(y),
			(&CborNode::Array(ref x), &CborNode::Array(ref y)) => {
				let mut xtr = x.iter();
				let mut ytr = y.iter();
				loop {
					let (i, j) = (xtr.next(), ytr.next());
					if let (Some(ref i), Some(ref j)) = (i, j) {
						let order = i.partial_cmp(j);
						if order != Some(Ordering::Equal) { return order; }
					} else if let (Some(_), None) = (i, j) {
						return Some(Ordering::Greater);
					} else if let (None, Some(_)) = (i, j) {
						return Some(Ordering::Less);
					} else {
						return Some(Ordering::Equal);
					}
				}
			},
			(&CborNode::Dictionary(ref x), &CborNode::Dictionary(ref y)) => {
				let mut xtr = x.iter();
				let mut ytr = y.iter();
				loop {
					let (i, j) = (xtr.next(), ytr.next());
					if let (Some((ref k1, ref v1)), Some((ref k2, ref v2))) = (i, j) {
						let order = k1.partial_cmp(k2);
						if order != Some(Ordering::Equal) { return order; }
						let order = v1.partial_cmp(v2);
						if order != Some(Ordering::Equal) { return order; }
					} else if let (Some(_), None) = (i, j) {
						return Some(Ordering::Greater);
					} else if let (None, Some(_)) = (i, j) {
						return Some(Ordering::Less);
					} else {
						return Some(Ordering::Equal);
					}
				}
			},
			(&CborNode::Tag(t1, ref x), &CborNode::Tag(t2, ref y)) => {
				if t1 > t2 { return Some(Ordering::Greater); }
				if t1 < t2 { return Some(Ordering::Less); }
				x.partial_cmp(y)
			},
			(&CborNode::Simple(ref x), &CborNode::Simple(ref y)) => x.partial_cmp(y),
			(&CborNode::HalfFloat(ref x), &CborNode::HalfFloat(ref y)) => x.partial_cmp(y),
			(&CborNode::Float(ref x), &CborNode::Float(ref y)) => x.partial_cmp(y),
			(&CborNode::Double(ref x), &CborNode::Double(ref y)) => x.partial_cmp(y),
			(ref x, ref y) => x.priority().partial_cmp(&y.priority())
		}
	}
}

impl PartialEq for CborNode
{
	fn eq(&self, other: &Self) -> bool
	{
		self.partial_cmp(other) == Some(Ordering::Equal)
	}
}

impl Ord for CborNode
{
	fn cmp(&self, other: &Self) -> Ordering
	{
		//Just arbitrarily assign the invalid case.
		self.partial_cmp(other).unwrap_or(Ordering::Less)
	}
}
impl Eq for CborNode {}


#[cfg(test)]
fn opposite(x: Ordering) -> Ordering
{
	match x {
		Ordering::Less => Ordering::Greater,
		Ordering::Greater => Ordering::Less,
		Ordering::Equal => Ordering::Equal,
	}
}

#[cfg(test)]
fn assert_cbor_rel(x: &CborNode, y: &CborNode, ordering: Ordering)
{
	assert_eq!(x.partial_cmp(y), Some(ordering));
	assert_eq!(y.partial_cmp(x), Some(opposite(ordering)));
	assert_eq!(x.cmp(y), ordering);
	assert_eq!(y.cmp(x), opposite(ordering));
	assert_eq!(x.eq(y), ordering == Ordering::Equal);
	assert_eq!(x.ne(y), ordering != Ordering::Equal);
}

#[test]
fn cbor_compares()
{
	//Various CBOR values in order, for testing.
	let list = [
		CborNode::Integer(0), CborNode::Integer(1), CborNode::Integer(6), CborNode::Integer(22),
		CborNode::Integer(0xFFFFFFFFFFFFFFFF), CborNode::NegInteger(0), CborNode::NegInteger(1),
		CborNode::NegInteger(15), CborNode::NegInteger(0xFFFFFFFFFFFFFFFF), CborNode::Octets(Vec::new()),
		CborNode::Octets(vec![0]), CborNode::Octets(vec![0,0]), CborNode::Octets(vec![0,1]),
		CborNode::Octets(vec![1]), CborNode::Octets(vec![1,0]), CborNode::Octets(vec![2]),
		CborNode::Octets(vec![255]), CborNode::Octets(vec![255,0]), CborNode::Octets(vec![255,255]),
		CborNode::Octets(vec![255,255,0]), CborNode::Octets(vec![255,255,255]),
		CborNode::String(format!("")), CborNode::String(format!(" ")), CborNode::String(format!("!")),
		CborNode::String(format!("0")), CborNode::String(format!("0x")), CborNode::String(format!("1")),
		CborNode::String(format!("A")), CborNode::String(format!("Z")), CborNode::String(format!("Z ")),
		CborNode::String(format!("ZZ")), CborNode::String(format!("ZZZZZZZ")), CborNode::Array(Vec::new()),
		CborNode::Array(vec![CborNode::Integer(0)]),
		CborNode::Array(vec![CborNode::Integer(1)]),
		CborNode::Array(vec![CborNode::Integer(1), CborNode::Integer(0)]),
		CborNode::Array(vec![CborNode::Integer(1), CborNode::Integer(1)]),
		CborNode::Array(vec![CborNode::Integer(1), CborNode::Double(0xFFFFFFFFFFFFFFFF)]),
		CborNode::Array(vec![CborNode::Integer(2)]),
		CborNode::Array(vec![CborNode::Integer(2), CborNode::Integer(0)]),
		CborNode::Array(vec![CborNode::Integer(2), CborNode::Integer(1), CborNode::Integer(6)]),
		CborNode::Array(vec![CborNode::Integer(2), CborNode::Integer(1), CborNode::Integer(7)]),
		CborNode::Array(vec![CborNode::Double(0xFFFFFFFFFFFFFFFF)]),
		CborNode::Dictionary(BTreeMap::new()),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Integer(0), CborNode::Integer(0))].iter().
			cloned())),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Integer(2), CborNode::Integer(0)),
			(CborNode::Integer(0), CborNode::Integer(0))].iter().cloned())),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Integer(0), CborNode::Integer(1))].iter().
			cloned())),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Integer(2), CborNode::Integer(0)),
			(CborNode::Integer(0), CborNode::Integer(1))].iter().cloned())),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Integer(2), CborNode::Integer(0)),
			(CborNode::Integer(0), CborNode::Integer(1)),
			(CborNode::Integer(7), CborNode::Integer(5))].iter().cloned())),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Integer(1), CborNode::Integer(0))].iter().
			cloned())),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Integer(2), CborNode::Integer(0)),
			(CborNode::Integer(1), CborNode::Integer(0)),
			(CborNode::Integer(6), CborNode::Integer(4))].iter().cloned())),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Integer(2), CborNode::Integer(0))].iter().
			cloned())),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Double(0), CborNode::Integer(0))].iter().
			cloned())),
		CborNode::Tag(0, Box::new(CborNode::Integer(0))),
		CborNode::Tag(0, Box::new(CborNode::Double(0xFFFFFFFFFFFFFFFF))),
		CborNode::Tag(1, Box::new(CborNode::Integer(0))),
		CborNode::Tag(1, Box::new(CborNode::Double(0xFFFFFFFFFFFFFFFF))),
		CborNode::Tag(2, Box::new(CborNode::Integer(0))),
		CborNode::Simple(0), CborNode::Simple(1), CborNode::Simple(2), CborNode::Simple(20),
		CborNode::Simple(21), CborNode::Simple(22), CborNode::Simple(23), CborNode::Simple(32),
		CborNode::Simple(254), CborNode::Simple(255), CborNode::HalfFloat(0), CborNode::HalfFloat(1),
		CborNode::HalfFloat(2), CborNode::HalfFloat(0xFFFF), CborNode::Float(0), CborNode::Float(1),
		CborNode::Float(2), CborNode::Float(0xFFFFFFFF), CborNode::Double(0), CborNode::Double(1),
		CborNode::Double(5), CborNode::Double(0xFFFFFFFFFFFFFFFF),
	];
	for i in 0..list.len() {
		for j in 0..i {
			assert_cbor_rel(&list[i], &list[j], Ordering::Greater);
		}
		assert_cbor_rel(&list[i], &list[i], Ordering::Equal);
		for j in i+1..list.len() {
			assert_cbor_rel(&list[i], &list[j], Ordering::Less);
		}
	}
}

#[test]
fn parse_cbor_abc_ind()
{
	assert_eq!(CborNode::parse(&[0x7F, 0x63, 65, 66, 67, 0xFF][..]).unwrap(), CborNode::String(format!("ABC")));
}

#[test]
fn parse_cbor()
{
	//Empty.
	CborNode::parse(&[]).unwrap_err();
	CborNode::parse(&[0x00, 0x01][..]).unwrap_err();
	//Positive integers.
	assert_eq!(CborNode::parse(&[0x00][..]).unwrap(), CborNode::Integer(0));
	assert_eq!(CborNode::parse(&[0x01][..]).unwrap(), CborNode::Integer(1));
	assert_eq!(CborNode::parse(&[0x17][..]).unwrap(), CborNode::Integer(23));
	CborNode::parse(&[0x18][..]).unwrap_err();
	assert_eq!(CborNode::parse(&[0x18, 0x00][..]).unwrap(), CborNode::Integer(0));
	assert_eq!(CborNode::parse(&[0x18, 0x17][..]).unwrap(), CborNode::Integer(23));
	assert_eq!(CborNode::parse(&[0x18, 0x18][..]).unwrap(), CborNode::Integer(24));
	assert_eq!(CborNode::parse(&[0x18, 0xFF][..]).unwrap(), CborNode::Integer(255));
	CborNode::parse(&[0x19, 0x01][..]).unwrap_err();
	assert_eq!(CborNode::parse(&[0x19, 0x00, 0x00][..]).unwrap(), CborNode::Integer(0));
	assert_eq!(CborNode::parse(&[0x19, 0x00, 0xFF][..]).unwrap(), CborNode::Integer(255));
	assert_eq!(CborNode::parse(&[0x19, 0x01, 0x00][..]).unwrap(), CborNode::Integer(256));
	assert_eq!(CborNode::parse(&[0x19, 0x01, 0x03][..]).unwrap(), CborNode::Integer(259));
	assert_eq!(CborNode::parse(&[0x19, 0xFF, 0xFE][..]).unwrap(), CborNode::Integer(0xFFFE));
	assert_eq!(CborNode::parse(&[0x19, 0xFF, 0xFF][..]).unwrap(), CborNode::Integer(0xFFFF));
	CborNode::parse(&[0x1A, 0x01, 0x00, 0x00][..]).unwrap_err();
	assert_eq!(CborNode::parse(&[0x1A, 0x00, 0x00, 0x00, 0x00][..]).unwrap(), CborNode::Integer(0));
	assert_eq!(CborNode::parse(&[0x1A, 0x00, 0x00, 0x00, 0x01][..]).unwrap(), CborNode::Integer(1));
	assert_eq!(CborNode::parse(&[0x1A, 0x00, 0x00, 0x00, 0xFF][..]).unwrap(), CborNode::Integer(255));
	assert_eq!(CborNode::parse(&[0x1A, 0x00, 0x00, 0x01, 0x00][..]).unwrap(), CborNode::Integer(256));
	assert_eq!(CborNode::parse(&[0x1A, 0x01, 0x05, 0x08, 0x14][..]).unwrap(), CborNode::Integer(0x01050814));
	assert_eq!(CborNode::parse(&[0x1A, 0xFF, 0xFE, 0xFD, 0xFC][..]).unwrap(), CborNode::Integer(0xFFFEFDFC));
	assert_eq!(CborNode::parse(&[0x1A, 0xFF, 0xFF, 0xFF, 0xFF][..]).unwrap(), CborNode::Integer(0xFFFFFFFF));
	CborNode::parse(&[0x1B, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,][..]).unwrap_err();
	assert_eq!(CborNode::parse(&[0x1B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00][..]).unwrap(),
		CborNode::Integer(0));
	assert_eq!(CborNode::parse(&[0x1B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01][..]).unwrap(),
		CborNode::Integer(1));
	assert_eq!(CborNode::parse(&[0x1B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF][..]).unwrap(),
		CborNode::Integer(255));
	assert_eq!(CborNode::parse(&[0x1B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00][..]).unwrap(),
		CborNode::Integer(256));
	assert_eq!(CborNode::parse(&[0x1B, 0x16, 0x22, 0x10, 0x02, 0x01, 0x05, 0x08, 0x14][..]).unwrap(),
		CborNode::Integer(0x1622100201050814));
	assert_eq!(CborNode::parse(&[0x1B, 0xFF, 0xFE, 0xFD, 0xFC, 0xFB, 0xFA, 0xF9, 0xF8][..]).unwrap(),
		CborNode::Integer(0xFFFEFDFCFBFAF9F8));
	assert_eq!(CborNode::parse(&[0x1B, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF][..]).unwrap(),
		CborNode::Integer(0xFFFFFFFFFFFFFFFF));
	CborNode::parse(&[0x1C][..]).unwrap_err();
	CborNode::parse(&[0x1D][..]).unwrap_err();
	CborNode::parse(&[0x1E][..]).unwrap_err();
	CborNode::parse(&[0x1F][..]).unwrap_err();
	CborNode::parse(&[0x1F, 0x00, 0xFF][..]).unwrap_err();
	//Negative integers.
	assert_eq!(CborNode::parse(&[0x20][..]).unwrap(), CborNode::NegInteger(0));
	assert_eq!(CborNode::parse(&[0x21][..]).unwrap(), CborNode::NegInteger(1));
	assert_eq!(CborNode::parse(&[0x37][..]).unwrap(), CborNode::NegInteger(23));
	assert_eq!(CborNode::parse(&[0x38, 0x01][..]).unwrap(), CborNode::NegInteger(0x01));
	assert_eq!(CborNode::parse(&[0x38, 0x18][..]).unwrap(), CborNode::NegInteger(0x18));
	assert_eq!(CborNode::parse(&[0x38, 0xFF][..]).unwrap(), CborNode::NegInteger(0xFF));
	assert_eq!(CborNode::parse(&[0x39, 0x01, 0x03][..]).unwrap(), CborNode::NegInteger(0x0103));
	assert_eq!(CborNode::parse(&[0x3A, 0x01, 0x03, 0x05, 0x07][..]).unwrap(), CborNode::NegInteger(0x01030507));
	assert_eq!(CborNode::parse(&[0x3B, 0x01, 0x03, 0x05, 0x07, 0x09, 0x0B, 0x0D, 0x0F][..]).unwrap(),
		CborNode::NegInteger(0x01030507090B0D0F));
	CborNode::parse(&[0x3C][..]).unwrap_err();
	CborNode::parse(&[0x3D][..]).unwrap_err();
	CborNode::parse(&[0x3E][..]).unwrap_err();
	CborNode::parse(&[0x3F][..]).unwrap_err();
	CborNode::parse(&[0x3F, 0x00, 0xFF][..]).unwrap_err();
	//Octet strings.
	assert_eq!(CborNode::parse(&[0x40][..]).unwrap(), CborNode::Octets(vec![]));
	assert_eq!(CborNode::parse(&[0x41, 0x05][..]).unwrap(), CborNode::Octets(vec![5]));
	CborNode::parse(&[0x42, 0x05][..]).unwrap_err();
	assert_eq!(CborNode::parse(&[0x42, 0x05, 0x09][..]).unwrap(), CborNode::Octets(vec![5, 9]));
	assert_eq!(CborNode::parse(&[0x44, 0x05, 0x09, 6, 2][..]).unwrap(), CborNode::Octets(vec![5, 9, 6, 2]));
	assert_eq!(CborNode::parse(&[0x58, 3, 0x09, 6, 2][..]).unwrap(), CborNode::Octets(vec![9, 6, 2]));
	assert_eq!(CborNode::parse(&[0x59, 0, 3, 0x09, 6, 2][..]).unwrap(), CborNode::Octets(vec![9, 6, 2]));
	assert_eq!(CborNode::parse(&[0x5A, 0, 0, 0, 3, 0x09, 6, 2][..]).unwrap(), CborNode::Octets(vec![9, 6, 2]));
	assert_eq!(CborNode::parse(&[0x5B, 0, 0, 0, 0, 0, 0, 0, 3, 0x09, 6, 2][..]).unwrap(),
		CborNode::Octets(vec![9, 6, 2]));
	CborNode::parse(&[0x5C][..]).unwrap_err();
	CborNode::parse(&[0x5D][..]).unwrap_err();
	CborNode::parse(&[0x5E][..]).unwrap_err();
	CborNode::parse(&[0x5F][..]).unwrap_err();
	assert_eq!(CborNode::parse(&[0x5F, 0xFF][..]).unwrap(), CborNode::Octets(vec![]));
	assert_eq!(CborNode::parse(&[0x5F, 0x40, 0xFF][..]).unwrap(), CborNode::Octets(vec![]));
	assert_eq!(CborNode::parse(&[0x5F, 0x41, 2, 0xFF][..]).unwrap(), CborNode::Octets(vec![2]));
	assert_eq!(CborNode::parse(&[0x5F, 0x42, 2, 6, 0xFF][..]).unwrap(), CborNode::Octets(vec![2, 6]));
	assert_eq!(CborNode::parse(&[0x5F, 0x42, 2, 6, 0x43, 1, 2, 3, 0xFF][..]).unwrap(),
		CborNode::Octets(vec![2, 6, 1, 2, 3]));
	assert_eq!(CborNode::parse(&[0x5F, 0x42, 2, 6, 0x40, 0x43, 1, 2, 3, 0xFF][..]).unwrap(),
		CborNode::Octets(vec![2, 6, 1, 2, 3]));
	CborNode::parse(&[0x5F, 0x42, 2, 6, 0x60, 0x43, 1, 2, 3, 0xFF][..]).unwrap_err();
	//Strings.
	assert_eq!(CborNode::parse(&[0x60][..]).unwrap(), CborNode::String(format!("")));
	assert_eq!(CborNode::parse(&[0x61, 0x41][..]).unwrap(), CborNode::String(format!("A")));
	CborNode::parse(&[0x62, 0x41][..]).unwrap_err();
	assert_eq!(CborNode::parse(&[0x62, 0x41, 0x44][..]).unwrap(), CborNode::String(format!("AD")));
	assert_eq!(CborNode::parse(&[0x78, 3, 65, 66, 67][..]).unwrap(), CborNode::String(format!("ABC")));
	assert_eq!(CborNode::parse(&[0x79, 0, 3, 65, 66, 67][..]).unwrap(), CborNode::String(format!("ABC")));
	assert_eq!(CborNode::parse(&[0x7A, 0, 0, 0, 3, 65, 66, 67][..]).unwrap(), CborNode::String(format!("ABC")));
	assert_eq!(CborNode::parse(&[0x7B, 0, 0, 0, 0, 0, 0, 0, 3, 65, 66, 67][..]).unwrap(),
		CborNode::String(format!("ABC")));
	assert_eq!(CborNode::parse(&[0x78, 3, 0xE2, 0xA2, 0x83][..]).unwrap(), CborNode::String(format!("{}",
		from_u32(0x2883).unwrap())));
	assert_eq!(CborNode::parse(&[0x78, 5, 33, 0xE2, 0xA2, 0x83, 36][..]).unwrap(),
		CborNode::String(format!("!{}$", from_u32(0x2883).unwrap())));
	CborNode::parse(&[0x7C][..]).unwrap_err();
	CborNode::parse(&[0x7D][..]).unwrap_err();
	CborNode::parse(&[0x7E][..]).unwrap_err();
	CborNode::parse(&[0x7F][..]).unwrap_err();
	assert_eq!(CborNode::parse(&[0x7F, 0xFF][..]).unwrap(), CborNode::String(format!("")));
	CborNode::parse(&[0x7F, 0x63, 65, 66, 67][..]).unwrap_err();
	assert_eq!(CborNode::parse(&[0x7F, 0x63, 65, 66, 67, 0xFF][..]).unwrap(), CborNode::String(format!("ABC")));
	assert_eq!(CborNode::parse(&[0x7F, 0x62, 65, 66, 0x61, 67, 0xFF][..]).unwrap(),
		CborNode::String(format!("ABC")));
	assert_eq!(CborNode::parse(&[0x7F, 0x62, 65, 66, 0x60, 0x61, 67, 0xFF][..]).unwrap(),
		CborNode::String(format!("ABC")));
	CborNode::parse(&[0x7F, 0x62, 65, 66, 0x40, 0x61, 67, 0xFF][..]).unwrap_err();
	CborNode::parse(&[0x7F, 0x62, 0xE2, 0xA2, 0x61, 0x83, 0xFF][..]).unwrap_err();
	//Arrays
	assert_eq!(CborNode::parse(&[0x80][..]).unwrap(), CborNode::Array(vec![]));
	assert_eq!(CborNode::parse(&[0x81, 0x04][..]).unwrap(), CborNode::Array(vec![CborNode::Integer(4)]));
	assert_eq!(CborNode::parse(&[0x81, 0xC2, 0x24][..]).unwrap(), CborNode::Array(vec![CborNode::Tag(2,
		Box::new(CborNode::NegInteger(4)))]));
	CborNode::parse(&[0x83, 0x04, 0x07, 0x18][..]).unwrap_err();
	assert_eq!(CborNode::parse(&[0x83, 0x04, 0x07, 0x02][..]).unwrap(),
		CborNode::Array(vec![CborNode::Integer(4), CborNode::Integer(7), CborNode::Integer(2)]));
	assert_eq!(CborNode::parse(&[0x83, 0x04, 0x07, 0x18, 0x19][..]).unwrap(),
		CborNode::Array(vec![CborNode::Integer(4), CborNode::Integer(7), CborNode::Integer(0x19)]));
	assert_eq!(CborNode::parse(&[0x98, 3, 0x04, 0x07, 0x18, 0x19][..]).unwrap(),
		CborNode::Array(vec![CborNode::Integer(4), CborNode::Integer(7), CborNode::Integer(0x19)]));
	assert_eq!(CborNode::parse(&[0x99, 0, 3, 0x04, 0x07, 0x18, 0x19][..]).unwrap(),
		CborNode::Array(vec![CborNode::Integer(4), CborNode::Integer(7), CborNode::Integer(0x19)]));
	assert_eq!(CborNode::parse(&[0x9A, 0, 0, 0, 3, 0x04, 0x07, 0x18, 0x19][..]).unwrap(),
		CborNode::Array(vec![CborNode::Integer(4), CborNode::Integer(7), CborNode::Integer(0x19)]));
	assert_eq!(CborNode::parse(&[0x9B, 0, 0, 0, 0, 0, 0, 0, 3, 0x04, 0x07, 0x18, 0x19][..]).unwrap(),
		CborNode::Array(vec![CborNode::Integer(4), CborNode::Integer(7), CborNode::Integer(0x19)]));
	CborNode::parse(&[0x9B, 1, 0, 0, 0, 0, 0, 0, 0][..]).unwrap_err();
	CborNode::parse(&[0x81, 1, 0][..]).unwrap_err();
	CborNode::parse(&[0x9C][..]).unwrap_err();
	CborNode::parse(&[0x9D][..]).unwrap_err();
	CborNode::parse(&[0x9E][..]).unwrap_err();
	CborNode::parse(&[0x9F][..]).unwrap_err();
	CborNode::parse(&[0x9F, 0x04, 0x07, 0x18, 0x19][..]).unwrap_err();
	assert_eq!(CborNode::parse(&[0x9F, 0x04, 0x07, 0x18, 0x19, 0xFF][..]).unwrap(),
		CborNode::Array(vec![CborNode::Integer(4), CborNode::Integer(7), CborNode::Integer(0x19)]));
	//Dictionaries
	assert_eq!(CborNode::parse(&[0xA0][..]).unwrap(),
		CborNode::Dictionary(BTreeMap::from_iter([].iter().cloned())));
	assert_eq!(CborNode::parse(&[0xA1, 0x03, 0x05][..]).unwrap(),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Integer(3), CborNode::Integer(5))].iter().
		cloned())));
	CborNode::parse(&[0xA2, 0x03, 0x05, 0x03, 0x05][..]).unwrap_err();
	assert_eq!(CborNode::parse(&[0xA2, 0x03, 0x05, 0x04, 0x05][..]).unwrap(),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Integer(3), CborNode::Integer(5)),
		(CborNode::Integer(4), CborNode::Integer(5))].iter().cloned())));
	assert_eq!(CborNode::parse(&[0xA2, 0xC2, 0x03, 0x05, 0xC1, 0x03, 0x06][..]).unwrap(),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Tag(1, Box::new(CborNode::Integer(3))),
		CborNode::Integer(6)), (CborNode::Tag(2, Box::new(CborNode::Integer(3))), CborNode::Integer(5))].
		iter().cloned())));
	assert_eq!(CborNode::parse(&[0xB8, 2, 0x03, 0x05, 0x04, 0x07][..]).unwrap(),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Integer(3), CborNode::Integer(5)),
		(CborNode::Integer(4), CborNode::Integer(7))].iter().cloned())));
	assert_eq!(CborNode::parse(&[0xB9, 0, 2, 0x03, 0x05, 0x04, 0x07][..]).unwrap(),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Integer(3), CborNode::Integer(5)),
		(CborNode::Integer(4), CborNode::Integer(7))].iter().cloned())));
	assert_eq!(CborNode::parse(&[0xBA, 0, 0, 0, 2, 0x03, 0x05, 0x04, 0x07][..]).unwrap(),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Integer(3), CborNode::Integer(5)),
		(CborNode::Integer(4), CborNode::Integer(7))].iter().cloned())));
	assert_eq!(CborNode::parse(&[0xBB, 0, 0, 0, 0, 0, 0, 0, 2, 0x03, 0x05, 0x04, 0x07][..]).unwrap(),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Integer(3), CborNode::Integer(5)),
		(CborNode::Integer(4), CborNode::Integer(7))].iter().cloned())));
	CborNode::parse(&[0xA1, 1][..]).unwrap_err();
	CborNode::parse(&[0xA1, 1, 0, 4][..]).unwrap_err();
	CborNode::parse(&[0xBC][..]).unwrap_err();
	CborNode::parse(&[0xBD][..]).unwrap_err();
	CborNode::parse(&[0xBE][..]).unwrap_err();
	CborNode::parse(&[0xBF][..]).unwrap_err();
	CborNode::parse(&[0xBF, 0xC2, 0x03, 0x05, 0xC1, 0x03, 0x06][..]).unwrap_err();
	assert_eq!(CborNode::parse(&[0xBF, 0xC2, 0x03, 0x05, 0xC1, 0x03, 0x06, 0xFF][..]).unwrap(),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Tag(1, Box::new(CborNode::Integer(3))),
		CborNode::Integer(6)), (CborNode::Tag(2, Box::new(CborNode::Integer(3))), CborNode::Integer(5))].
		iter().cloned())));
	//Tags
	assert_eq!(CborNode::parse(&[0xC0, 0x01][..]).unwrap(), CborNode::Tag(0, Box::new(CborNode::Integer(1))));
	assert_eq!(CborNode::parse(&[0xC1, 0x03][..]).unwrap(), CborNode::Tag(1, Box::new(CborNode::Integer(3))));
	assert_eq!(CborNode::parse(&[0xD7, 0x03][..]).unwrap(), CborNode::Tag(23, Box::new(CborNode::Integer(3))));
	assert_eq!(CborNode::parse(&[0xD8, 0x01, 0x03][..]).unwrap(), CborNode::Tag(1, Box::new(
		CborNode::Integer(3))));
	assert_eq!(CborNode::parse(&[0xD9, 0x01, 0x03, 0x04][..]).unwrap(), CborNode::Tag(0x0103, Box::new(
		CborNode::Integer(4))));
	assert_eq!(CborNode::parse(&[0xDA, 0x01, 0x03, 0x05, 0x07, 0x04][..]).unwrap(), CborNode::Tag(0x01030507,
		Box::new(CborNode::Integer(4))));
	assert_eq!(CborNode::parse(&[0xDB, 0x01, 0x03, 0x05, 0x07, 0x09, 0x0B, 0x0D, 0x0F, 0x04][..]).unwrap(),
		CborNode::Tag(0x01030507090B0D0F, Box::new(CborNode::Integer(4))));
	CborNode::parse(&[0xDC][..]).unwrap_err();
	CborNode::parse(&[0xDD][..]).unwrap_err();
	CborNode::parse(&[0xDE][..]).unwrap_err();
	CborNode::parse(&[0xDF][..]).unwrap_err();
	CborNode::parse(&[0xDF, 0x00, 0xFF][..]).unwrap_err();
	//Simples
	assert_eq!(CborNode::parse(&[0xE0][..]).unwrap(), CborNode::Simple(0));
	assert_eq!(CborNode::parse(&[0xE1][..]).unwrap(), CborNode::Simple(1));
	assert_eq!(CborNode::parse(&[0xF4][..]).unwrap(), CborNode::Simple(20));
	assert_eq!(CborNode::parse(&[0xF5][..]).unwrap(), CborNode::Simple(21));
	assert_eq!(CborNode::parse(&[0xF6][..]).unwrap(), CborNode::Simple(22));
	assert_eq!(CborNode::parse(&[0xF7][..]).unwrap(), CborNode::Simple(23));
	CborNode::parse(&[0xF8, 0x1F][..]).unwrap_err();
	assert_eq!(CborNode::parse(&[0xF8, 0x20][..]).unwrap(), CborNode::Simple(32));
	assert_eq!(CborNode::parse(&[0xF8, 0x21][..]).unwrap(), CborNode::Simple(33));
	assert_eq!(CborNode::parse(&[0xF8, 0xFF][..]).unwrap(), CborNode::Simple(255));
	//HalfFloats
	assert_eq!(CborNode::parse(&[0xF9, 0x00, 0x00][..]).unwrap(),
		CborNode::HalfFloat(0));
	assert_eq!(CborNode::parse(&[0xF9, 0x00, 0xFF][..]).unwrap(),
		CborNode::HalfFloat(0xFF));
	assert_eq!(CborNode::parse(&[0xF9, 0x01, 0x03][..]).unwrap(),
		CborNode::HalfFloat(0x0103));
	assert_eq!(CborNode::parse(&[0xF9, 0xFF, 0xFF][..]).unwrap(),
		CborNode::HalfFloat(0xFFFF));
	//Floats
	assert_eq!(CborNode::parse(&[0xFA, 0x00, 0x00, 0x00, 0x00][..]).unwrap(),
		CborNode::Float(0));
	assert_eq!(CborNode::parse(&[0xFA, 0x00, 0x00, 0xFF, 0xFF][..]).unwrap(),
		CborNode::Float(0xFFFF));
	assert_eq!(CborNode::parse(&[0xFA, 0x01, 0x03, 0x05, 0x07][..]).unwrap(),
		CborNode::Float(0x01030507));
	assert_eq!(CborNode::parse(&[0xFA, 0xFF, 0xFF, 0xFF, 0xFF][..]).unwrap(),
		CborNode::Float(0xFFFFFFFF));
	//Doubles
	assert_eq!(CborNode::parse(&[0xFB, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00][..]).unwrap(),
		CborNode::Double(0));
	assert_eq!(CborNode::parse(&[0xFB, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF][..]).unwrap(),
		CborNode::Double(0xFFFFFFFF));
	assert_eq!(CborNode::parse(&[0xFB, 0x01, 0x03, 0x05, 0x07, 0x09, 0x0B, 0x0D, 0x0F][..]).unwrap(),
		CborNode::Double(0x01030507090B0D0F));
	assert_eq!(CborNode::parse(&[0xFB, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF][..]).unwrap(),
		CborNode::Double(0xFFFFFFFFFFFFFFFF));
	//Invalid.
	CborNode::parse(&[0xFC][..]).unwrap_err();
	CborNode::parse(&[0xFD][..]).unwrap_err();
	CborNode::parse(&[0xFE][..]).unwrap_err();
	CborNode::parse(&[0xFF][..]).unwrap_err();
}

#[test]
fn test_json_string_unescape()
{
	JsonNode::token_to_string("").unwrap_err();
	JsonNode::token_to_string("foo").unwrap_err();
	JsonNode::token_to_string("\"foo").unwrap_err();
	JsonNode::token_to_string("foo\"").unwrap_err();
	JsonNode::token_to_string("\"f\\x006go\"").unwrap_err();
	assert_eq!(JsonNode::token_to_string("\"\"").unwrap(), format!(""));
	assert_eq!(JsonNode::token_to_string("\"foo\"").unwrap(), format!("foo"));
	assert_eq!(JsonNode::token_to_string("\"f\\\\o\"").unwrap(), format!("f\\o"));
	assert_eq!(JsonNode::token_to_string("\"f\\\"o\"").unwrap(), format!("f\"o"));
	assert_eq!(JsonNode::token_to_string("\"f\\bo\"").unwrap(), format!("f\x08o"));
	assert_eq!(JsonNode::token_to_string("\"f\\fo\"").unwrap(), format!("f\x0Co"));
	assert_eq!(JsonNode::token_to_string("\"f\\no\"").unwrap(), format!("f\no"));
	assert_eq!(JsonNode::token_to_string("\"f\\ro\"").unwrap(), format!("f\ro"));
	assert_eq!(JsonNode::token_to_string("\"f\\to\"").unwrap(), format!("f\to"));
	assert_eq!(JsonNode::token_to_string("\"f\\u006fo\"").unwrap(), format!("foo"));
	assert_eq!(JsonNode::token_to_string("\"f\\ud800\\udc00o\"").unwrap(), format!("f{}o",
		from_u32(0x10000).unwrap()));
	assert_eq!(JsonNode::token_to_string("\"f\\udbff\\udffdo\"").unwrap(), format!("f{}o",
		from_u32(0x10FFFD).unwrap()));
	JsonNode::token_to_string("\"\\s\"").unwrap_err();
	JsonNode::token_to_string("\"\\x6666\"").unwrap_err();
	JsonNode::token_to_string("\"\\ud800\"").unwrap_err();
	JsonNode::token_to_string("\"\\udc00\"").unwrap_err();
	JsonNode::token_to_string("\"\\ud800x\\udc00\"").unwrap_err();
}

#[test]
fn test_json_tokenization()
{
	let mut s = "";
	JsonNode::pull_token(&mut s).unwrap_err();
	let mut s = ",A";
	assert_eq!(JsonNode::pull_token(&mut s).unwrap(), ",");
	assert_eq!(s, "A");
	let mut s = "ABC";
	assert_eq!(JsonNode::pull_token(&mut s).unwrap(), "ABC");
	assert_eq!(s, "");
	let mut s = "ABC,Y";
	assert_eq!(JsonNode::pull_token(&mut s).unwrap(), "ABC");
	assert_eq!(s, ",Y");
	let mut s = "517ABC,X";
	assert_eq!(JsonNode::pull_token(&mut s).unwrap(), "517ABC");
	assert_eq!(s, ",X");
	let mut s = "FOO BAR";
	assert_eq!(JsonNode::pull_token(&mut s).unwrap(), "FOO");
	assert_eq!(s, " BAR");
	let mut s = "   \t\t\t   \t  FOO BAR";
	assert_eq!(JsonNode::pull_token(&mut s).unwrap(), "FOO");
	assert_eq!(s, " BAR");
	let mut s = "\"FOO BAR\"";
	assert_eq!(JsonNode::pull_token(&mut s).unwrap(), "\"FOO BAR\"");
	assert_eq!(s, "");
	let mut s = "\"FOO BAR\"X";
	assert_eq!(JsonNode::pull_token(&mut s).unwrap(), "\"FOO BAR\"");
	assert_eq!(s, "X");
	let mut s = "\"FOO\\\"BAR\"X";
	assert_eq!(JsonNode::pull_token(&mut s).unwrap(), "\"FOO\\\"BAR\"");
	assert_eq!(s, "X");
	let mut s = "\"FOO\\nBAR\"X";
	assert_eq!(JsonNode::pull_token(&mut s).unwrap(), "\"FOO\\nBAR\"");
	assert_eq!(s, "X");
	let mut s = "\"FOO\\nBAR\\\\\"X";
	assert_eq!(JsonNode::pull_token(&mut s).unwrap(), "\"FOO\\nBAR\\\\\"");
	assert_eq!(s, "X");
	let mut s = "\"FOOBAR\\u004f\"X";
	assert_eq!(JsonNode::pull_token(&mut s).unwrap(), "\"FOOBAR\\u004f\"");
	assert_eq!(s, "X");
}

#[test]
fn test_json_equals()
{
	assert!(JsonNode::parse("1").unwrap() == 1f64);
	assert!(JsonNode::parse("1").unwrap() != 2f64);
	assert!(JsonNode::parse("false").unwrap() == false);
	assert!(JsonNode::parse("false").unwrap() != true);
	assert!(JsonNode::parse("\"foo bar\"").unwrap() == "foo bar");
	assert!(JsonNode::parse("\"foo bar\"").unwrap() != "foo baz");
}

#[test]
fn test_json_parse()
{
	JsonNode::parse("").unwrap_err();
	JsonNode::parse("0,").unwrap_err();
	JsonNode::parse("True").unwrap_err();
	JsonNode::parse("true2").unwrap_err();
	JsonNode::parse("0f5").unwrap_err();
	JsonNode::parse("[,]").unwrap_err();
	JsonNode::parse("[1,3,7,]").unwrap_err();
	JsonNode::parse("{,}").unwrap_err();
	JsonNode::parse("{\"x\":0,}").unwrap_err();
	JsonNode::parse("{\"x\":0,\"x\":0}").unwrap_err();
	JsonNode::parse("{x:0}").unwrap_err();
	//Numbers.
	assert_eq!(JsonNode::parse("0").unwrap(), JsonNode::Number(0f64));
	assert_eq!(JsonNode::parse(" 0").unwrap(), JsonNode::Number(0f64));
	assert_eq!(JsonNode::parse("0 ").unwrap(), JsonNode::Number(0f64));
	assert_eq!(JsonNode::parse("160 ").unwrap(), JsonNode::Number(160f64));
	assert_eq!(JsonNode::parse("1.6e2").unwrap(), JsonNode::Number(160f64));
	assert_eq!(JsonNode::parse("-1.6e2").unwrap(), JsonNode::Number(-160f64));
	assert_eq!(JsonNode::parse("-1.635e2").unwrap(), JsonNode::Number(-163.5f64));
	assert_eq!(JsonNode::parse("7.5e-1").unwrap(), JsonNode::Number(0.75f64));
	//Null, False, True
	assert_eq!(JsonNode::parse("null").unwrap(), JsonNode::Null);
	assert_eq!(JsonNode::parse("false").unwrap(), JsonNode::Boolean(false));
	assert_eq!(JsonNode::parse("true").unwrap(), JsonNode::Boolean(true));
	assert_eq!(JsonNode::parse("true ").unwrap(), JsonNode::Boolean(true));
	//Strings.
	assert_eq!(JsonNode::parse("\"foobar\"").unwrap(), JsonNode::String(format!("foobar")));
	assert_eq!(JsonNode::parse("\"foo bar\"").unwrap(), JsonNode::String(format!("foo bar")));
	assert_eq!(JsonNode::parse("\"foo\\bbar\"").unwrap(), JsonNode::String(format!("foo\x08bar")));
	assert_eq!(JsonNode::parse("\"foo\\u00f6bar\"").unwrap(), JsonNode::String(format!("fooöbar")));
	assert_eq!(JsonNode::parse("\t  \"foobar\" \t ").unwrap(), JsonNode::String(format!("foobar")));
	//Arrays.
	assert_eq!(JsonNode::parse("[]").unwrap(), JsonNode::Array(vec![]));
	assert_eq!(JsonNode::parse("[null]").unwrap(), JsonNode::Array(vec![JsonNode::Null]));
	assert_eq!(JsonNode::parse("[1,3,7]").unwrap(), JsonNode::Array(vec![JsonNode::Number(1f64),
		JsonNode::Number(3f64), JsonNode::Number(7f64)]));
	assert_eq!(JsonNode::parse("[1  ,   3,  \t 7] ").unwrap(), JsonNode::Array(vec![JsonNode::Number(1f64),
		JsonNode::Number(3f64), JsonNode::Number(7f64)]));
	assert_eq!(JsonNode::parse("[1,3,7,\"qux\"]").unwrap(), JsonNode::Array(vec![JsonNode::Number(1f64),
		JsonNode::Number(3f64), JsonNode::Number(7f64), JsonNode::String(format!("qux"))]));
	//Dictionaries.
	assert_eq!(JsonNode::parse("{}").unwrap(), JsonNode::Dictionary(BTreeMap::new()));
	assert_eq!(JsonNode::parse("{\"foo\":false}").unwrap(), JsonNode::Dictionary(BTreeMap::from_iter([
		(format!("foo"), JsonNode::Boolean(false))].iter().cloned())));
	assert_eq!(JsonNode::parse("{\"foo\":false,\"bar\":null}").unwrap(), JsonNode::Dictionary(
		BTreeMap::from_iter([(format!("bar"), JsonNode::Null), (format!("foo"), JsonNode::Boolean(false))].
		iter().cloned())));
	assert_eq!(JsonNode::parse("  {\"foo\":false  ,  \t \"bar\":null}  ").unwrap(), JsonNode::Dictionary(
		BTreeMap::from_iter([(format!("bar"), JsonNode::Null), (format!("foo"), JsonNode::Boolean(false))].
		iter().cloned())));
}

#[test]
fn json_testcases()
{
	use std::io::Read;
	use std::fs::{File, read_dir};
	let iter = read_dir("src/json-tests").unwrap();
	let mut failed = 0;
	for i in iter {
		let j = i.unwrap();
		let p = j.path();
		let f = j.file_name().into_string().unwrap();
		let mut should_succeed = if f.starts_with("y_") {
			Some(true)
		} else if f.starts_with("n_") {
			Some(false)
		} else if f.starts_with("i_") {
			None
		} else {
			continue
		};
		let mut file = File::open(p).unwrap();
		let mut content = String::new();
		let mut successful = true;
		if successful {
			match file.read_to_string(&mut content) {
				Ok(_) => (),
				Err(_) => {
					successful = false;
					should_succeed = None;	//Don't care about tests that aren't valid UTF-8.
				}
			}
		}
		if successful {
			match JsonNode::parse(&content) {
				Ok(_) => (),
				Err(_) => successful = false
			}
		}
		let (status, delta) = match (successful, should_succeed) {
			(false, Some(false)) => ("PASS (failed as it should)", 0),
			(false, None) => ("IGNORE (got failed)", 0),
			(false, Some(true)) => ("FAILED (expect pass, but failed)", 1),
			(true, Some(false)) => ("FAILED (expect fail, but passed)", 1),
			(true, None) => ("IGNORE (got passed)", 0),
			(true, Some(true)) => ("PASS (passed as it should)", 0),
		};
		if delta > 0 {
			use std::io::{stdout, Write};
			stdout().write_all(format!("{}: {}", f, status).as_bytes()).unwrap();
		}
		failed += delta;
	}
	assert!(failed == 0);
}

#[test]
fn overlong_bom()
{
	use std::str::from_utf8;
	let input = [0xF0, 0x8F, 0xBB, 0xBF];
	assert!(from_utf8(&input).is_err());
}

#[test]
fn parse_json_bom()
{
	use std::str::from_utf8;
	let input = [0xEF, 0xBB, 0xBF, 0x22, 0x66, 0x6F, 0x6F, 0x22];
	assert_eq!(JsonNode::parse(from_utf8(&input).unwrap()), Ok(JsonNode::String("foo".to_owned())));
}
