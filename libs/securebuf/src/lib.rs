//!More secure buffers.
//!
//!This crate contains routines for zeroizing buffers and creating buffers that zero on drop.

#![no_std]
#![forbid(missing_docs)]
#![allow(unsafe_code)]
extern crate btls_aux_serialization;
use btls_aux_serialization::SlicingExt;
use core::ops::{Deref, DerefMut};
use core::ptr::write_volatile;

///Zeroize the contents of buffer.
///
///Takes in:
///
/// * A buffer `buf`.
///
///This function zeroizes the buffer.
///
///The zeroizating can not be optimized away, even if the buffer is not used afterwards.
#[allow(unsafe_code)]
pub fn wipe_buffer(buf: &mut [u8])
{
	for i in buf.iter_mut() {
		//This is safe since u8 is not Drop and the cast happens from reference (so the address is guaranteed
		//to be valid).
		unsafe{write_volatile(i as *mut u8, 0)};
	}
}

#[allow(unsafe_code)]
mod zerovalid
{
	///The type has a zero value.
	///
	///This trait denotes that the type has some sort of zero value. 
	///
	///Additionally, this trait impiles that the type is not `Drop`. Implementing this trait for any types
	///that are `Drop` causes undefined behavior.
	pub unsafe trait ZeroValid: Sized {
		///The all zeroes value.
		///
		///This method returns the zero value used when overwriting buffer contents.
		fn zero() -> Self;
	}
	//Implement ZeroValid for fundamential types.
	unsafe impl ZeroValid for bool { fn zero() -> Self { false }}
	unsafe impl ZeroValid for u8 { fn zero() -> Self { 0 }}
	unsafe impl ZeroValid for i8 { fn zero() -> Self { 0 }}
	unsafe impl ZeroValid for u16 { fn zero() -> Self { 0 }}
	unsafe impl ZeroValid for i16 { fn zero() -> Self { 0 }}
	unsafe impl ZeroValid for u32 { fn zero() -> Self { 0 }}
	unsafe impl ZeroValid for i32 { fn zero() -> Self { 0 }}
	unsafe impl ZeroValid for u64 { fn zero() -> Self { 0 }}
	unsafe impl ZeroValid for i64 { fn zero() -> Self { 0 }}
	unsafe impl ZeroValid for usize { fn zero() -> Self { 0 }}
	unsafe impl ZeroValid for isize { fn zero() -> Self { 0 }}
}
pub use zerovalid::ZeroValid;

///Zeroize the contents of buffer.
///
///Takes in:
///
/// * A buffer `buf`.
///
///This function zeroizes the buffer.
///
///The zeroizating can not be optimized away, even if the buffer is not used afterwards.
///
///This differs from [`wipe_buffer()`] in that the slice element type is bounded by trait [`ZeroValid`], instead
///fixed `u8` type.
///
///[`wipe_buffer()`]: fn.wipe_buffer.html
///[`ZeroValid`]: trait.ZeroValid.html
#[allow(unsafe_code)]
pub fn wipe_buffer2<T:ZeroValid>(buf: &mut [T])
{
	for i in buf.iter_mut() {
		//This is safe since T is not Drop and the cast happens from reference (so the address is guaranteed
		//to be valid).
		unsafe{write_volatile(i as *mut T, T::zero())};
	}
}

///Trait for "Behaves like a buffer".
///
///This trait models something that works like an array buffer.
pub trait Buffer: Deref+DerefMut
{
	///Return a reference to element at index.
	///
	///Takes in:
	///
	/// * An array `self`.
	/// * An index `index`.
	///
	///This method returns a constant reference to the index of the array.
	///
	///If the array index is in bounds, returns `Some(x)`, where `x` is reference to the element.
	///
	///If the array index is out of bounds, returns `None`.
	fn get(&self, index: usize) -> Option<&u8>;
	///Return a mutable reference to element at index.
	///
	///Takes in:
	///
	/// * An array `self`.
	/// * An index `index`.
	///
	///This method returns a mutable reference to the index of the array.
	///
	///If the array index is in bounds, returns `Some(x)`, where `x` is mutable reference to the element.
	///
	///If the array index is out of bounds, returns `None`.
	fn get_mut(&mut self, index: usize) -> Option<&mut u8>;
}

///A stack buffer that is wiped upon drop.
///
///This structure acts as stack buffer that is zeroized when it goes out of scope.
pub struct SecStackBuffer<'a>(&'a mut [u8]);

impl<'a> Drop for SecStackBuffer<'a>
{
	fn drop(&mut self)
	{
		wipe_buffer(self.0);
	}
}


impl<'a> Deref for SecStackBuffer<'a>
{
	type Target = [u8];
	fn deref(&self) -> &[u8]
	{
		self.0
	}
}

impl<'a> DerefMut for SecStackBuffer<'a>
{
	fn deref_mut(&mut self) -> &mut [u8]
	{
		self.0
	}
}

impl<'a> SecStackBuffer<'a>
{
	///Create a new buffer.
	///
	///Takes in:
	///
	/// * A backing buffer `backing`.
	/// * The size of the buffer `size`.
	///
	///This method creates a buffer that is backed by the buffer, and has the specified size.
	///
	///On success, returns `Ok(buffer)`, where `buffer` is the buffer created.
	///
	///On failure, returns `Err(())`. At least the following factors cause the method to fail:
	///
	/// * The size is bigger than length of the buffer.
	///method to fail.
	pub fn new<'b>(backing: &'b mut [u8], size: usize) -> Result<SecStackBuffer<'b>, ()>
	{
		Ok(SecStackBuffer(backing.slice_np_len_mut(0, size).ok_or(())?))
	}
}

impl<'a> Buffer for SecStackBuffer<'a>
{
	fn get(&self, index: usize) -> Option<&u8>
	{
		self.0.get(index)
	}
	fn get_mut(&mut self, index: usize) -> Option<&mut u8>
	{
		self.0.get_mut(index)
	}
}
