//!AEAD encryption/decryption.
//!
//!This crate provodes routines for encrypting and decrypting data using various AEAD (Authenticated Encryption
//!with Associated Data) encryption methods.
#![forbid(unsafe_code)]
#![forbid(missing_docs)]

#[macro_use]
extern crate btls_aux_fail;
extern crate btls_aux_chacha20poly1305aead;
extern crate btls_aux_nettle;
use btls_aux_chacha20poly1305aead::Chacha20Poly1305 as EChacha20Poly1305;
use btls_aux_nettle::GcmContext;


use std::sync::Mutex;

///AEAD algorithm.
///
///This enumeration specifies the algorithm used by AEAD (Authenticated Encryption with Associated Data) encryption
///and decryption.
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub enum ProtectorType
{
	///AES-128-GCM
	///
	///AES (Advanced Encryption Standard), with 128-bit key, in GCM (Galois Counter Mode).
	///
	///This algorithm is number 1 in IANA "Authenticated Encryption with Associated Data (AEAD) Parameters"
	///registry.
	Aes128Gcm,
	///AES-256-GCM
	///
	///AES (Advanced Encryption Standard), with 256-bit key, in GCM (Galois Counter Mode).
	///
	///This algorithm is number 2 in IANA "Authenticated Encryption with Associated Data (AEAD) Parameters"
	///registry.
	Aes256Gcm,
	///Chacha20-Poly1305
	///
	///Chacha20 (Chacha with 20 rounds) encryption combined with Poly1305 MAC (Message Authentication Code).
	///
	///This algorithm is number 29 in IANA "Authenticated Encryption with Associated Data (AEAD) Parameters"
	///registry.
	Chacha20Poly1305,
}

impl ProtectorType
{
	///Get bitmask constant for the algorithm.
	///
	///Takes in:
	///
	/// * An encryption algorithm `self`.
	///
	///This method returns the bitmask constant corresponding to the algorithm.
	///
	///This is useful for storing a set of algorithms (up to 64) in an u64. 
	pub fn algo_const(&self) -> u64
	{
		1u64 << match self {
			&ProtectorType::Aes128Gcm => 0,
			&ProtectorType::Aes256Gcm => 1,
			&ProtectorType::Chacha20Poly1305 => 2,
		}
	}
	///Get human-readable name for the algorithm.
	///
	///Takes in:
	///
	/// * An encryption algorithm `self`.
	///
	///This method returns the the human-readable name of the algorithm.
	pub fn as_string(&self) -> &'static str
	{
		match self {
			&ProtectorType::Aes128Gcm => "AES-128-GCM",
			&ProtectorType::Aes256Gcm => "AES-256-GCM",
			&ProtectorType::Chacha20Poly1305 => "Chacha20-Poly1305",
		}
	}
	///Create a new encryptor for the algorithm.
	///
	///Takes in:
	///
	/// * An encryption algorithm `self`.
	/// * A key `key`. The key must be of correct length for the algorithm.
	///
	///This method creates a encryption context using the algorithm and the key.
	///
	///The correct key lengths are:
	///
	/// * 16 bytes for AES-128-GCM
	/// * 32 bytes for AES-256-GCM and Chacha20-Poly1305.
	///
	///If successful, returns `Ok(econtext)`, where `econtext` is the created encryption context.
	///
	///On failure, returns `Err(())`. At least the following factors cause the method to fail:
	///
	/// * The key length is wrong.
	/// * The algorithm implementation fails its POST (Power On Self-Test) tests (buggy implementation).
	pub fn new_encryptor(&self, key: &[u8]) -> Result<EncryptionKey, ()>
	{
		match self {
			&ProtectorType::Aes128Gcm => Ok(EncryptionKey(_EncryptionKey::Gcm(Mutex::new(GcmContext::new(
				key)?)))),
			&ProtectorType::Aes256Gcm => Ok(EncryptionKey(_EncryptionKey::Gcm(Mutex::new(GcmContext::new(
				key)?)))),
			&ProtectorType::Chacha20Poly1305 => Ok(EncryptionKey(_EncryptionKey::Chacha2(Mutex::new(
				EChacha20Poly1305::new(key)?)))),
		}
	}
	///Create a new decryptor for the algorithm.
	///
	///Takes in:
	///
	/// * An encryption algorithm `self`.
	/// * A key `key`. The key must be of correct length for the algorithm.
	///
	///This method creates a decryption context using the algorithm and the key.
	///
	///The correct key lengths are:
	///
	/// * 16 bytes for AES-128-GCM
	/// * 32 bytes for AES-256-GCM and Chacha20-Poly1305.
	///
	///If successful, returns `Ok(dcontext)`, where `dcontext` is the created decryption context.
	///
	///On failure, returns `Err(())`. At least the following factors cause the method to fail:
	///
	/// * The key length is wrong.
	/// * The algorithm implementation fails its POST (Power On Self-Test) tests (buggy implementation).
	pub fn new_decryptor(&self, key: &[u8]) -> Result<DecryptionKey, ()>
	{
		match self {
			&ProtectorType::Aes128Gcm => Ok(DecryptionKey(_DecryptionKey::Gcm(Mutex::new(GcmContext::new(
				key)?)))),
			&ProtectorType::Aes256Gcm => Ok(DecryptionKey(_DecryptionKey::Gcm(Mutex::new(GcmContext::new(
				key)?)))),
			&ProtectorType::Chacha20Poly1305 => Ok(DecryptionKey(_DecryptionKey::Chacha2(Mutex::new(
				EChacha20Poly1305::new(key)?)))),
		}
	}
}

///An encryptor.
///
///This object encrypts data using the specified AEAD algorithm and the key.
pub struct EncryptionKey(_EncryptionKey);
enum _EncryptionKey
{
	Gcm(Mutex<GcmContext>),
	Chacha2(Mutex<EChacha20Poly1305>),
}

///A decryptor.
///
///This object decrypts data using the specified AEAD algorithm and the key.
pub struct DecryptionKey(_DecryptionKey);
enum _DecryptionKey
{
	Gcm(Mutex<GcmContext>),
	Chacha2(Mutex<EChacha20Poly1305>),
}

impl EncryptionKey
{
	///Encrypt a piece of data with the AEAD.
	///
	///Takes in:
	///
	/// * An encryption algorithm and key `self`.
	/// * A nonce `nonce`. The nonce must be unique and of correct length for the algorithm.
	/// * An associated data `ad`.
	/// * An input and output slice `in_out`.
	/// * The amount of plaintext padding `pad`. The padding must be sufficient to cover the plaintext
	///expansion during encryption.
	///
	///The plaintext to encrypt is given by the slice. However, the last amount of padding bytes of this slice
	///are not part of the plaintext, but are instead reserved for expansion of the plaintext while encrypting.
	///
	///The nonce has to be unique. Repeating the nonce even once CAUSES ALL SECURITY TO BE LOST. It is strongly
	///recommended to use counters, or similar methods that eliminate the possibility of nonce repeating. The
	///nonce MUST NOT be random, since there is insufficent space for entropy to ensure uniqueness (in context
	///of AES-GCM in TLS, this is known as "Nonce Disrespecting Adversaries attack). The nonce does not need to
	///be unpredictable nor secret. However, not randomizing the starting point of the nonce leads to loss of
	///security, which is a concern when used with 128-bit keys. However, this weakening is no concern with
	///256-bit keys due to large security margin.
	///
	///This method encrypts and authenticates the plaintext, and authenticates the associated data, using the
	///specified key and nonce. The result is written into the slice.
	///
	///The merging of plaintext input and ciphertext output, and padding the plaintext with some scratch space
	///is done in order to support in-place encryption, which is otherwise not possible, due to Rust forbidding
	///mutable buffer from aliasing non-mutable buffer.
	///
	///For all current algorithms, the nonce has to be 12 bytes in length, and the plaintext expansion is
	///16 bytes, so the amount of padding has to be at least 16 bytes.
	///
	///On success, returns `Ok(ciphertext_size)`, where `ciphertext_size` is the size of the produced
	///ciphertext in bytes.
	///
	///On failure, returns `Err(())`. At least the following factors cause the method to fail:
	///
	/// * The amount of padding is too small.
	/// * The amount of padding is larger than the slice.
	/// * The nonce is of wrong length for the algorithm.
	/// * The amount of plaintext, or associated data is larger than allowed by the the algorithm.
	/// * The internal implementation has at least once crashed, causing the context to be in undefined state.
	pub fn encrypt(&self, nonce: &[u8], ad: &[u8], in_out: &mut [u8], pad: usize) -> Result<usize, ()>
	{
		match &self.0 {
			&_EncryptionKey::Gcm(ref x) => match x.lock() {
				Ok(mut y) => y.encrypt(nonce, ad, in_out, pad),
				Err(_) => fail!(())
			},
			&_EncryptionKey::Chacha2(ref x) => match x.lock() {
				Ok(mut y) => y.encrypt(nonce, ad, in_out, pad),
				Err(_) => fail!(())
			},
		}
	}
}

impl DecryptionKey
{
	///Decrypt a piece of data with the AEAD.
	///
	///Takes in:
	///
	/// * An encryption algorithm and key `self`.
	/// * A nonce `nonce`. Must be the same as used when encrypting the plaintext.
	/// * Associated data `ad`. Must be the same as used when encrypting the plaintext.
	/// * A input and output slice `in_out`.
	///
	///This method authenticates the contents of the slice and the associated data using the given key and
	///nonce. If this authentication is successful, then decrypts the contents of the slice, writing the result
	///to the slice.
	///
	///If the nonce and associated data do not match what was used for the encryption, the decryption
	///will fail.
	///
	///The merging of ciphertext input and plaintext output, is done in order to support in-place decryption,
	///which is otherwise not possible, due to Rust forbidding mutable buffer from aliasing non-mutable buffer.
	///
	///On success, the method returns `Ok(plaintext_size)`, where `plaintext_size` is the size of the plaintext
	///produced in bytes.
	///
	///On failure, returns `Err(())`. At least the following factors cause the method to fail:
	///
	/// * The slice is too small to contain valid ciphertext.
	/// * The nonce has wrong length for the algorithm.
	/// * The key does not match what was used for encryption.
	/// * The nonce does not match what was used for encryption.
	/// * The associated data does not match what was used for encryption.
	/// * The ciphertext has been tampered with.
	/// * The internal implementation has at least once crashed, causing the context to be in undefined state.
	pub fn decrypt(&self, nonce: &[u8], ad: &[u8], in_out: &mut [u8]) -> Result<usize, ()>
	{
		match &self.0 {
			&_DecryptionKey::Gcm(ref x) => match x.lock() {
				Ok(mut y) => y.decrypt(nonce, ad, in_out),
				Err(_) => fail!(())
			},
			&_DecryptionKey::Chacha2(ref x) => match x.lock() {
				Ok(mut y) => y.decrypt(nonce, ad, in_out),
				Err(_) => fail!(())
			},
		}
	}
}
