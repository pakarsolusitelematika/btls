//!Assertion errors.
//!
//!This crate provodes types for handling assertion errors as failures. That is, without triggering unwinding, which
//!may not actually be supported by the underlying runtime.
#![forbid(unsafe_code)]
#![forbid(missing_docs)]

use std::error::Error;
use std::fmt::{Display, Error as FmtError, Formatter};

///Assertion failed error.
///
///This structure represents a failed assertion, with the assertion failure error message inside.
///
///This implements the standard `Display` and `Error` traits.
#[derive(Clone,Debug,PartialEq,Eq)]
pub struct AssertFailed(pub String);

impl Display for AssertFailed
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		fmt.write_str(&self.0)
	}
}

impl Error for AssertFailed
{
	fn description(&self) -> &str
	{
		&self.0
	}
}

///Create an unwrapped assertion failure with given message.
///
///Takes in:
///
/// * A message `$msg`, or format string `$fmt` and format arguments `$args` for the message.
///
///If there is only one message argument, it is interpretted as a raw string, and needs to be either a `&str`,
///`String` or `&String`. If there are multiple arguments, the first message argument is interpretted as format
///string, and the rest as format arguments, in the same style as the `println!()` macro.
///
///This macro constructs an `AssertFailed` object that has the specified message. If the message is given as a
///format string and arguments. If the message is given as format string and arguments, the given format string is
///interpolated and the result is used as the message. The created object is not wrapped in `Err()`.
///
///This is intended to be used in `.map_err()` methods, like:
///
///```no-run
///do_something().map_err(|(x,y)|assert_failure!("Foo is too big: {} is over limit {}", x, y))?;
///```
#[macro_export]
macro_rules! assert_failure
{
	($msg:expr) => {{
		use std::borrow::ToOwned;
		use $crate::AssertFailed;
		AssertFailed($msg.to_owned())
	}};
	($fmt:expr,$($args:tt)*) => {{
		use $crate::AssertFailed;
		AssertFailed(format!($fmt, $($args)*))
	}};
}

///If condition is false, fail current function with given an assertion failure.
///
///Takes in:
///
/// * A boolean condition `$cond`.
/// * A message or format string and format arguments for the message `$x`.
///
///If there is only one message argument, it is interpretted as a raw string, and needs to be either a `&str`,
///`String` or `&String`. If there are multiple arguments, the first message argument is interpretted as format
///string, and the rest as format arguments, in the same style as the `println!()` macro.
///
///This macro first checks the condition. If this condition is false, then the message given (or interpolation of
///the format string given) is then first wrapped in `AssertFailed` and then `Err()`. Then the result is immediately
///returned out of the current function using the `?` operator. Due to this, the return value underoes `From` trait
///conversions.
///
///This is intended to be used in checks that are expected to be true, howerver might not be:
///
///```no-run
///sanity_check!(source.len() < target.len(), "Target is too small ({}, need {})", target.len(), source.len());
///(&mut target[..source.len()]).copy_from_slice(source);
///```
#[macro_export]
macro_rules! sanity_check
{
	($cond:expr,$($x:tt)*) => {
		if !$cond { Err(assert_failure!($($x)*))?; }
	};
}

///Unconditionally fail the current function with given assertion failure.
///
///Takes in:
///
/// * A message or format string and format arguments for the message `$x`.
///
///If there is only one message argument, it is interpretted as a raw string, and needs to be either a `&str`,
///`String` or `&String`. If there are multiple arguments, the first message argument is interpretted as format
///string, and the rest as format arguments, in the same style as the `println!()` macro.
///
///This macro wraps the message given (or interpolation of the format string given) first in `AssertFailed` and then
///`Err()`. The then result is immediately returned out of the current function using the `?` operator. Due to this,
///the return value underoes `From` trait conversions.
///
///This macro never returns, all code after it is dead.
///
///This is intended to be used in match branches, like:
///
///```no-run
///match something {
///	...
///	Err(x) => sanity_failed!("something failed: {}", x)
///}
///```
#[macro_export]
macro_rules! sanity_failed
{
	($($x:tt)*) => {{
		Err(assert_failure!($($x)*))?;
		unreachable!();
	}};
}
