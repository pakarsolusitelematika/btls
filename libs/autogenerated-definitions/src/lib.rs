//!Definitions of binary representations of various OIDs.
//!
//!This crate contains binary representations of various OIDs (Object IDentifiers) that come accross in PKIX
//!certificates, compiled from the textual representations of said OIDs.
#![allow(dead_code)]
#![forbid(unsafe_code)]
#![forbid(missing_docs)]
include!(concat!(env!("OUT_DIR"), "/autogenerated.rs"));

///The canonical parameters block for RSA-PSS with SHA-256.
pub static RSA_PSS_SHA256_OID: &'static [u8; 66] = include_bytes!("rsa-pss-sha2-256.hdr");
///The canonical parameters block for RSA-PSS with SHA-384.
pub static RSA_PSS_SHA384_OID: &'static [u8; 66] = include_bytes!("rsa-pss-sha2-384.hdr");
///The canonical parameters block for RSA-PSS with SHA-512.
pub static RSA_PSS_SHA512_OID: &'static [u8; 66] = include_bytes!("rsa-pss-sha2-512.hdr");
///The canonical parameters block for RSA-PSS with SHA3-256.
pub static RSA_PSS_SHA3_256_OID: &'static [u8; 62] = include_bytes!("rsa-pss-sha3-256.hdr");
///The canonical parameters block for RSA-PSS with SHA3-384.
pub static RSA_PSS_SHA3_384_OID: &'static [u8; 62] = include_bytes!("rsa-pss-sha3-384.hdr");
///The canonical parameters block for RSA-PSS with SHA3-512.
pub static RSA_PSS_SHA3_512_OID: &'static [u8; 62] = include_bytes!("rsa-pss-sha3-512.hdr");
