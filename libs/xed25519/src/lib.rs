//!Low level implementation of `Ed25519` and `X25519`.
//!
//!This crate provodes a low-level implementations of the `Ed25519` signature algorithm, and the `X25519` key
//!agreement.
#![forbid(missing_docs)]

extern crate btls_aux_random;
use btls_aux_random::secure_random;
use std::slice::from_raw_parts_mut;

#[doc(hidden)]
#[no_mangle]
pub unsafe extern fn ed25519_randombytes_cb_xed25519(ptr: *mut u8, len: usize)
{
	let slice = from_raw_parts_mut(ptr, len);
	secure_random(slice);
}

///Generate an `X25519` public key from private key.
///
///Takes in:
///
/// * An output buffer to store the `X25519` public key `pubkey`.
/// * An input buffer containing the `X25519` private key `secret`.
///
///This function calculates the `X25519` public key corresponding to the private key, and stores the result to the
///output buffer.
pub fn x25519_generate(pubkey: &mut [u8;32], secret: &[u8;32])
{
	unsafe{curved25519_scalarmult_basepoint_xed25519(pubkey.as_mut_ptr(), secret.as_ptr())};
}

///Perform `X25519` key agreement.
///
///Takes in:
///
/// * An output buffer to store the `X25519` shared secret `shared`.
/// * An input buffer containing the `X25519` private key `secret`.
/// * An input buffer containing the `X25519` peer public key `peerpub`.
///
///This function calculates the `X25519` shared secret from the private key and the peer public key, and stores the
///result to the output buffer.
pub fn x25519_agree(shared: &mut [u8;32], secret: &[u8;32], peerpub: &[u8;32])
{
	unsafe{curved25519_scalarmult_xed25519(shared.as_mut_ptr(), secret.as_ptr(), peerpub.as_ptr())};
}

///Generate an `Ed25519` public key from private key.
///
///Takes in:
///
/// * An output buffer to store the `Ed25519` public key `pubkey`.
/// * An input buffer containing the `Ed25519` private key `secret`.
///
///This function calculates the `Ed25519` public key corresponding to the private key, and stores the result to the
///output buffer.
pub fn ed25519_pubkey(pubkey: &mut [u8; 32], key: &[u8; 32])
{
	unsafe{ed25519_publickey_xed25519(key.as_ptr(), pubkey.as_mut_ptr())};
}

///Sign data with `Ed25519`.
///
///Takes in:
///
/// * A `Ed25519` secret key `sk`.
/// * The `Ed25519` public key corresponding to the secret key `pk`.
/// * A message `tbs`.
/// * An output buffer for the signature `signature`.
///
///This function signs the message using the `Ed25519` private and public keys provoded, and stores the resulting
///signature to the output buffer.
pub fn ed25519_sign(sk: &[u8; 32], pk: &[u8; 32], tbs: &[u8], signature: &mut [u8;64])
{
	unsafe{ed25519_sign_xed25519(tbs.as_ptr(), tbs.len(), sk.as_ptr(), pk.as_ptr(), signature.as_mut_ptr())};
}

///Verify `Ed25519` signature.
///
///Takes in:
///
/// * A `Ed25519` public key `pk`.
/// * A message `tbs`.
/// * A signature `signature`.
///
///This function verifies the signature on the message using the specified public key.
///
///If the verification succeeds, returns `Ok(())`.
///
///If the verification fails, returns `Err(())`.
pub fn ed25519_verify(pubkey: &[u8; 32], tbs: &[u8], signature: &[u8;64]) -> Result<(), ()>
{
	let r = unsafe{ed25519_sign_open_xed25519(tbs.as_ptr(), tbs.len(), pubkey.as_ptr(), signature.as_ptr())};
	if r < 0 { Err(()) } else { Ok(()) }
}

extern
{
	fn curved25519_scalarmult_basepoint_xed25519(p: *mut u8, s: *const u8);
	fn curved25519_scalarmult_xed25519(o: *mut u8, s: *const u8, b: *const u8) -> i32;
	fn ed25519_publickey_xed25519(sk: *const u8, pk: *mut u8);
	fn ed25519_sign_xed25519(msg: *const u8, msglen: usize, sk: *const u8, pk: *const u8, sig: *mut u8);
	fn ed25519_sign_open_xed25519(msg: *const u8, msglen: usize, pk: *const u8, sig: *const u8) -> i32;
}
