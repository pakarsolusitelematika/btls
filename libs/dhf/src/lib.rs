//!Routines related to Diffie-Hellman Functions in TLS.
//!
//!This crate provodes routines for performing Diffie-Hellman key agreement in TLS.
#![forbid(unsafe_code)]
#![forbid(missing_docs)]
use std::fmt::{Debug, Display, Error as FmtError, Formatter};
extern crate btls_aux_nettle;
extern crate btls_aux_random;
extern crate btls_aux_securebuf;
use btls_aux_securebuf::wipe_buffer;
pub use btls_aux_random::secure_random;
#[macro_use]
extern crate btls_aux_fail;

macro_rules! make_bytes_wrapper
{
	($name:ident, $maxsize:expr) => {
		impl Clone for $name {
			fn clone(&self) -> Self
			{
				$name(self.0, self.1)
			}
		}
		impl Drop for $name { fn drop(&mut self) { wipe_buffer(&mut self.0); } }
		impl $name
		{
			///Convert an octet slice into type.
			///
			///Takes in:
			///
			/// * A data slice `y`.
			///
			///This method constructs an object of this type, initializing it with the contents of
			///the slice.
			///
			///On success, returns `Ok(obj)`, where `obj` is the newly constructed object.
			///
			///If the slice is larger than the maximum size, returns `Err((size, limit))`, where
			///`size` is the size of the slice passed and `limit` is the maximum number of octets
			///allowed.
			pub fn from2(y: &[u8]) -> Result<$name, (usize, usize)>
			{
				if y.len() > $maxsize { return Err((y.len(), $maxsize)); }
				let mut x = $name([0;$maxsize], y.len());
				(&mut x.0[..y.len()]).copy_from_slice(y);
				Ok(x)
			}
		}
		impl Debug for $name
		{
			fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
			{
				if self.1 > self.0.len() {
					fmt.write_fmt(format_args!("<Invalid data: {:?}, length={}>", &self.0[..],
						self.1))
				} else {
					fmt.write_fmt(format_args!("{:?}", &self.0[..self.1]))
				}
			}
		}
		impl AsRef<[u8]> for $name
		{
			fn as_ref(&self) -> &[u8]
			{
				//We outright assume length is valid.
				&self.0[..self.1]
			}
		}
	}
}

///TLS group identifier for `NSA P-256` group
pub const GRP_P256: u16 = 23;
///TLS group identifier for `NSA P-384` group
pub const GRP_P384: u16 = 24;
///TLS group identifier for `NSA P-521` group
pub const GRP_P521: u16 = 25;
///TLS group identifier for `X25519` pseudo-group
pub const GRP_X25519: u16 = 29;
///TLS group identifier for `X448` pseudo-group
pub const GRP_X448: u16 = 30;

///Error in key agreement.
///
///This enumeration describes the error that occured in key generation or key agreement.
#[derive(Clone,Debug,PartialEq,Eq)]
pub enum KeyAgreementError
{
	#[doc(hidden)]
	AttemptedDheReuse,
	#[doc(hidden)]
	PeerPublicKeyLengthInvalid,
	#[doc(hidden)]
	PeerPublicKeyInvalid,
	#[doc(hidden)]
	CantExportPublicKey,
	#[doc(hidden)]
	CurveNotSupported,
	#[doc(hidden)]
	CantGenerateDheKey,
	#[doc(hidden)]
	X448NotSupported,
	#[doc(hidden)]
	DummyPublicKey,
	#[doc(hidden)]
	OutputLengthTooBig(&'static str, usize, usize),
	#[doc(hidden)]
	PubkeyLengthTooBig(&'static str, usize, usize),
	#[doc(hidden)]
	Hidden__
}


impl Display for KeyAgreementError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::KeyAgreementError::*;
		match self {
			&AttemptedDheReuse => fmt.write_str("Tried to reuse DHE keypair"),
			&PeerPublicKeyLengthInvalid => fmt.write_str("Peer DHE public key length invalid"),
			&PeerPublicKeyInvalid => fmt.write_str("Peer DHE public key invalid"),
			&CurveNotSupported => fmt.write_str("Curve not supported"),
			&CantExportPublicKey => fmt.write_str("Can't export DHE public key"),
			&CantGenerateDheKey => fmt.write_str("Internal error: Failed to generate DH key"),
			&X448NotSupported => fmt.write_str("X448 not supported"),
			&DummyPublicKey => fmt.write_str("Dummy public key"),
			&OutputLengthTooBig(name,  x, y) => fmt.write_fmt(format_args!("DH output length too big \
				({}, limit {}) for {}", x, y, name)),
			&PubkeyLengthTooBig(name, x, y) => fmt.write_fmt(format_args!("DH public key length too big \
				({}, limit {}) for {}", x, y, name)),
			&Hidden__ => fmt.write_str("Hidden__")
		}
	}
}

///The maximum possible size of DHF output.
///
///This constant is the maximum size of Diffie-Hellamn key agreement output in bytes.
pub const MAX_DHF_OUTPUT: usize = 66;	//Assume DHF outputs are below 528 bits.
///The maximum possible size of DHF public key.
///
///This constant is the maximum size of Diffie-Hellamn public key in bytes.
pub const MAX_DHF_PUBKEY: usize = 136;	//Assume DHF public keys are below 1088 bits.

///Shared secret output from Diffie-Hellman Function.
///
///This structure represents the output from Diffie-Hellman key agreement. The structure acts as owned buffer with
///fixed maximum size ([`MAX_DHF_OUTPUT`]).
///
///[`MAX_DHF_OUTPUT`]: constant.MAX_DHF_OUTPUT.html
pub struct DhfOutput([u8; MAX_DHF_OUTPUT], usize);
make_bytes_wrapper!(DhfOutput, MAX_DHF_OUTPUT);
///Public key output from Diffie-Hellman Function.
///
///This structure represents a Diffie-Hellman public key. The structure acts as owned buffer with fixed maximum
///size ([`MAX_DHF_PUBKEY`]).
///
///[`MAX_DHF_PUBKEY`]: constant.MAX_DHF_PUBKEY.html
pub struct DhfPublicKey([u8; MAX_DHF_PUBKEY], usize);
make_bytes_wrapper!(DhfPublicKey, MAX_DHF_PUBKEY);

///A Diffie-Hellman Function.
///
///This enumeration specifies the variant of Diffie-Hellman (e.g., the curve) used for Diffie-Hellman key agreement.
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub enum Dhf
{
	///`ECDH NSA P-256`, TLS style.
	///
	///This is TLS-style ECDH (Elliptic-Curve Diffie-Hellman) using the `NSA P-256` curve.
	///
	///Public keys are 65 bytes, output is 32 bytes, estimated security level is 128 bits.
	NsaP256,
	///`ECDH NSA P-384`, TLS style.
	///
	///This is TLS-style ECDH (Elliptic-Curve Diffie-Hellman) using the `NSA P-384` curve.
	///
	///Public keys are 97 bytes, output is 48 bytes, estimated security level is 192 bits.
	NsaP384,
	///`ECDH NSA P-521`, TLS style.
	///
	///This is TLS-style ECDH (Elliptic-Curve Diffie-Hellman) using the `NSA P-521` curve.
	///
	///Public keys are 133 bytes, output is 66 bytes, estimated security level is 256 bits.
	NsaP521,
	///X25519.
	///
	///This is the IRTF X25519 function as key agreement.
	///
	///Public keys are 32 bytes, output is 32 bytes, estimated security level is 128 bits.
	X25519,
	///X448.
	///
	///This is the IRTF X448 function as key agreement.
	///
	///Public keys are 56 bytes, output is 56 bytes, estimated security level is 224 bits.
	X448,
}

mod x448
{
	use super::secure_random;
	use super::{Dhf, DhfOutput, DhfPublicKey, DiffieHellmanKey, KeyAgreementError};

	extern crate btls_aux_xed448;
	use self::btls_aux_xed448::{x448_agree, x448_generate};
	use btls_aux_securebuf::wipe_buffer;

	pub struct DhfX448(bool, [u8; 56], [u8; 56]);

	impl DhfX448
	{
		pub fn new() -> Result<DhfX448, KeyAgreementError>
		{
			let mut tmp = DhfX448(false, [0;56], [0;56]);
			secure_random(&mut tmp.1);
			x448_generate(&mut tmp.2, &tmp.1);
			Ok(tmp)
		}
	}

	impl DiffieHellmanKey for DhfX448
	{
		fn pubkey(&self) -> Result<DhfPublicKey, KeyAgreementError>
		{
			use super::KeyAgreementError::*;
			Ok(DhfPublicKey::from2(&self.2[..]).map_err(|(x, y)|PubkeyLengthTooBig("X448", x, y))?)
		}
		fn agree(&mut self, peer_pub: &[u8]) -> Result<DhfOutput, KeyAgreementError>
		{
			use super::KeyAgreementError::*;
			let mut ppub = [0; 56];
			let mut mat = [0; 56];
			fail_if!(ppub.len() != peer_pub.len(), PeerPublicKeyLengthInvalid);
			ppub.copy_from_slice(peer_pub);
			fail_if!(self.0, AttemptedDheReuse);
			x448_agree(&mut mat, &self.1, &ppub);
			let mut syndrome = 0;
			for i in mat.iter() { syndrome |= *i; }
			fail_if!(syndrome == 0, PeerPublicKeyInvalid);
			wipe_buffer(&mut self.1);
			self.0 = true;
			Ok(DhfOutput::from2(&mat[..]).map_err(|(x,y)|OutputLengthTooBig("X448", x, y))?)
		}
		fn tls_id(&self) -> u16 { self.group().to_tls_id() }
		fn group(&self) -> Dhf { Dhf::X448 }
	}
}

mod x25519
{
	use super::secure_random;
	use super::{Dhf, DhfOutput, DhfPublicKey, DiffieHellmanKey, KeyAgreementError};

	extern crate btls_aux_xed25519;
	use self::btls_aux_xed25519::{x25519_agree, x25519_generate};
	use btls_aux_securebuf::wipe_buffer;

	pub struct DhfX25519(bool, [u8; 32], [u8; 32]);

	impl DhfX25519
	{
		pub fn new() -> Result<DhfX25519, KeyAgreementError>
		{
			let mut tmp = DhfX25519(false, [0;32], [0;32]);
			secure_random(&mut tmp.1);
			x25519_generate(&mut tmp.2, &tmp.1);
			Ok(tmp)
		}
	}

	impl DiffieHellmanKey for DhfX25519
	{
		fn pubkey(&self) -> Result<DhfPublicKey, KeyAgreementError>
		{
			use super::KeyAgreementError::*;
			Ok(DhfPublicKey::from2(&self.2[..]).map_err(|(x, y)|PubkeyLengthTooBig("X25519", x, y))?)
		}
		fn agree(&mut self, peer_pub: &[u8]) -> Result<DhfOutput, KeyAgreementError>
		{
			use super::KeyAgreementError::*;
			let mut ppub = [0; 32];
			let mut mat = [0; 32];
			fail_if!(ppub.len() != peer_pub.len(), PeerPublicKeyLengthInvalid);
			ppub.copy_from_slice(peer_pub);
			fail_if!(self.0, AttemptedDheReuse);
			x25519_agree(&mut mat, &self.1, &ppub);
			let mut syndrome = 0;
			for i in mat.iter() { syndrome |= *i; }
			fail_if!(syndrome == 0, PeerPublicKeyInvalid);
			wipe_buffer(&mut self.1);
			self.0 = true;
			Ok(DhfOutput::from2(&mat[..]).map_err(|(x,y)|OutputLengthTooBig("X25519", x, y))?)
		}
		fn tls_id(&self) -> u16 { self.group().to_tls_id() }
		fn group(&self) -> Dhf { Dhf::X25519 }
	}
}

mod nettle
{
	use super::{Dhf, DhfOutput, DhfPublicKey, DiffieHellmanKey, KeyAgreementError, MAX_DHF_PUBKEY};
	use btls_aux_nettle::{EccCurve, NsaEcdhKey};
	use btls_aux_securebuf::wipe_buffer;
	pub struct DhfNsaNettle(Option<NsaEcdhKey>, DhfPublicKey, Dhf);

	impl DhfNsaNettle
	{
		pub fn new(crvid: u32, func: Dhf) -> Result<DhfNsaNettle, KeyAgreementError>
		{
			use super::KeyAgreementError::*;
			let mut tmp = [0; MAX_DHF_PUBKEY];	//Large enough for NIST P-521.
			let crv = EccCurve::new(crvid).map_err(|_|CurveNotSupported)?;
			let (key, publen) = NsaEcdhKey::new(crv, &mut tmp).map_err(|_|CantGenerateDheKey)?;
			let pubkey = DhfPublicKey::from2(&tmp[..publen]).map_err(|(x,y)|PubkeyLengthTooBig(
				func.as_string(), x, y))?;
			Ok(DhfNsaNettle(Some(key), pubkey, func))
		}
	}

	impl DiffieHellmanKey for DhfNsaNettle
	{
		fn pubkey(&self) -> Result<DhfPublicKey, KeyAgreementError>
		{
			Ok(self.1.clone())
		}
		fn agree(&mut self, peer_pub: &[u8]) -> Result<DhfOutput, KeyAgreementError>
		{
			use super::KeyAgreementError::*;
			let mut tmp = [0; MAX_DHF_PUBKEY];		//Large enough for NIST P-521.
			let xlen = if let &Some(ref x) = &self.0 {
				x.agree(&mut tmp, peer_pub).map_err(|_|PeerPublicKeyInvalid)?
			} else {
				fail!(AttemptedDheReuse);
			};
			fail_if!(xlen > peer_pub.len(), CantExportPublicKey);
			self.0 = None;		//Destroy the private key.
			let matlen = (xlen - 1) / 2;	//Only the x component.
			let out = {
				let mat = &tmp[1..][..matlen];
				DhfOutput::from2(mat).map_err(|(x,y)|OutputLengthTooBig(self.2.as_string(),
					x, y))?
			};
			wipe_buffer(&mut tmp);
			Ok(out)
		}
		fn tls_id(&self) -> u16 { self.group().to_tls_id() }
		fn group(&self) -> Dhf { self.2 }
	}
}

impl Dhf
{
	///Look up Diffie-Hellman function by TLS group identifier.
	///
	///Takes in:
	///
	/// * A TLS group identifier `id`.
	///
	///This method uses the identifier to look up corresponding Diffie-Hellman function.
	///
	///On success, returns `Some(dhf)`, where `dhf` is the Diffie-Hellman function object.
	///
	///If the specified TLS group identifier is unknown or not supported, returns `None`.
	pub fn by_tls_id(id: u16) -> Option<Dhf>
	{
		match id {
			GRP_P256 => Some(Dhf::NsaP256),
			GRP_P384 => Some(Dhf::NsaP384),
			GRP_P521 => Some(Dhf::NsaP384),
			GRP_X25519 => Some(Dhf::X25519),
			GRP_X448 => Some(Dhf::X448),
			_  => None
		}
	}
	///Generate a new keypair.
	///
	///Takes in:
	///
	/// * A Diffie-Hellman function `self`.
	///
	///This method generates a new keypair for the specified function.
	///
	///On success, returns `Ok(keypair)`, where `keypair` is the keypair generated.
	///
	///On failure, returns `Err(error)`, where `error` describes the error that occured.
	pub fn generate_key(&self) -> Result<Box<DiffieHellmanKey+Send>, KeyAgreementError>
	{
		Ok(match self {
			&Dhf::NsaP256 => Box::new(nettle::DhfNsaNettle::new(0, *self)?),
			&Dhf::NsaP384 => Box::new(nettle::DhfNsaNettle::new(1, *self)?),
			&Dhf::NsaP521 => Box::new(nettle::DhfNsaNettle::new(2, *self)?),
			&Dhf::X25519  => Box::new(x25519::DhfX25519::new()?),
			&Dhf::X448    => Box::new(x448::DhfX448::new()?),
		})
	}
	///Return the TLS group ID corresponding to this key agreement method.
	///
	///Takes in:
	///
	/// * A Diffie-Hellman function `self`.
	///
	///This method returns the TLS group identifier corresponding to the specified function.
	pub fn to_tls_id(&self) -> u16
	{
		match self {
			&Dhf::NsaP256 => GRP_P256,
			&Dhf::NsaP384 => GRP_P384,
			&Dhf::NsaP521 => GRP_P521,
			&Dhf::X25519  => GRP_X25519,
			&Dhf::X448    => GRP_X448,
		}
	}
	///Get bitmask constant for the function.
	///
	///Takes in:
	///
	/// * A Diffie-Hellman function `self`.
	///
	///This method returns the bitmask corresponding to the specified function.
	///
	///This is useful for storing a set of functions (up to 64) in an u64. 
	pub fn dhfalgo_const(&self) -> u64
	{
		1u64 << match self {
			&Dhf::NsaP256 => 0,
			&Dhf::NsaP384 => 1,
			&Dhf::X25519  => 2,
			&Dhf::X448    => 3,
			&Dhf::NsaP521 => 4,
		}
	}
	///Get human-readable name for the function.
	///
	///Takes in:
	///
	/// * A Diffie-Hellman function `self`.
	///
	///This method returns the the human-readable name of the specified function.
	pub fn as_string(&self) -> &'static str
	{
		match self {
			&Dhf::NsaP256 => "NSA P-256",
			&Dhf::NsaP384 => "NSA P-384",
			&Dhf::NsaP521 => "NSA P-521",
			&Dhf::X25519  => "X25519",
			&Dhf::X448    => "X448",
		}
	}
}

///Diffie-Hellman Function key pair.
///
///This trait is interface to Diffie-Hellman key pairs. It is object-safe, so objects of structures implementing
///this trait can be boxed as objects of this trait.
pub trait DiffieHellmanKey
{
	///Extract the public key from key pair.
	///
	///Takes in:
	///
	/// * A Diffie-Hellman keypair `self`.
	///
	///This method extracts the public key used in the keypair. This works even for invalidated keypairs.
	///
	///On success, returns `Ok(pubkey)`, where `pubkey` is the public key, in format TLS expects.
	///
	///On failure, returns `Err(error)`, where `error` describes the error that occured.
	fn pubkey(&self) -> Result<DhfPublicKey, KeyAgreementError>;
	///Perform key agreement with peer public key.
	///
	///Takes in:
	///
	/// * A Diffie-Hellman keypair `self`. The keypair must not be invalidated.
	/// * A peer public key `peer_pub`.
	///
	///This method performs key agreement between the keypair and the public key. The keypair is invalidated in
	///the process.
	///
	///On success, returns `Ok(shared)`, where `shared` is the shared key agreement output, in format TLS
	///expects.
	///
	///On failure, returns `Err(error)`, where `error` describes the error that occured. At least the following
	///factors cause the method to fail:
	///
	/// * All-zero output from `X25519` or `X448` functions.
	/// * The keypair has already been invalidated.
	fn agree(&mut self, peer_pub: &[u8]) -> Result<DhfOutput, KeyAgreementError>;
	///Get the TLS group id corresponding to the key.
	///
	///Takes in:
	///
	/// * A Diffie-Hellman keypair `self`.
	///
	///This method returns the TLS group identifier corresponding to the keypair.
	fn tls_id(&self) -> u16;
	///Get the Diffie-Hellman Function handle corresponding to the key.
	///
	///Takes in:
	///
	/// * A Diffie-Hellman keypair `self`.
	///
	///This method returns the Diffie-Hellman function corresponding to the key.
	fn group(&self) -> Dhf;
}

#[cfg(test)]
fn test_key_agreement(func: Dhf)
{
	//This will also test lengths are correct.
	let mut alice = func.generate_key().unwrap();
	let mut bob = func.generate_key().unwrap();
	let alicepub = alice.pubkey().unwrap();
	let bobpub = bob.pubkey().unwrap();
	assert!(alicepub.as_ref().len() <= MAX_DHF_PUBKEY);
	assert!(bobpub.as_ref().len() <= MAX_DHF_PUBKEY);
	let ab = alice.agree(bobpub.as_ref()).unwrap();
	let ba = bob.agree(alicepub.as_ref()).unwrap();
	assert!(ab.as_ref().len() <= MAX_DHF_OUTPUT);
	assert!(ba.as_ref().len() <= MAX_DHF_OUTPUT);
	alice.agree(bobpub.as_ref()).unwrap_err();
	assert_eq!(ab.as_ref(), ba.as_ref());
}

#[cfg(test)]
fn test_key_agreement_invalid(func: Dhf)
{
	let mut alice = func.generate_key().unwrap();
	let mallorypub = [4, 2];
	//This better fail.
	alice.agree(&mallorypub).unwrap_err();
}

#[test]
fn x25519_key_agreement()
{
	test_key_agreement(Dhf::X25519);
}

#[test]
fn x448_key_agreement()
{
	test_key_agreement(Dhf::X448);
}

#[test]
fn nsa_p256_key_agreement()
{
	test_key_agreement(Dhf::NsaP256);
}

#[test]
fn nsa_p384_key_agreement()
{
	test_key_agreement(Dhf::NsaP384);
}

#[test]
fn nsa_p521_key_agreement()
{
	test_key_agreement(Dhf::NsaP521);
}

#[test]
fn x25519_key_agreement_invalid()
{
	test_key_agreement_invalid(Dhf::X25519);
}

#[test]
fn x448_key_agreement_invalid()
{
	test_key_agreement_invalid(Dhf::X448);
}

#[test]
fn nsa_p256_key_agreement_invalid()
{
	test_key_agreement_invalid(Dhf::NsaP256);
}

#[test]
fn nsa_p384_key_agreement_invalid()
{
	test_key_agreement_invalid(Dhf::NsaP384);
}


#[test]
fn nsa_p521_key_agreement_invalid()
{
	test_key_agreement_invalid(Dhf::NsaP521);
}
